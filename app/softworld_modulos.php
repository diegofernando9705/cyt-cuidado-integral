<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class softworld_modulos extends Model
{
    use HasFactory;

    protected $fillable = [
    	'nombre_modulo',
    	'descripcion_modulo',
    	'estado_modulo',
    ];
}
