<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldContentTextHeaders extends Model
{
    use HasFactory;

    protected $fillable = [
    	'title',
    	'description',
    	'text_button',
    	'url_button',
    	'page_id',
    	'status',
    ];
}
