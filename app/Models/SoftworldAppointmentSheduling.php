<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldAppointmentSheduling extends Model
{
	use HasFactory;

	protected $table = "softworld_sheduling_appointments";
	protected $primaryKey = "code";
	public $incrementing = false;

	protected $fillable = [
		'code',
		'date',
		'start_hour',
		'end_hour',
		'names',
		'last_names',
		'celphone',
		'email',
		'profesional_id',
		'status',
		'date_deleted',
		'deleted_by',
	];

	public static function active()
	{
		return self::where([["date_deleted", NULL], ["deleted_by", NULL]])->get();
	}

	public function profesional()
	{
		return $this->hasOne(SoftworldPerson::class, 'id', 'profesional_id');
	}

	public function getStatus()
	{
		return $this->hasOne(SoftworldStatusPlatforms::class, 'id', 'status');
	}
}
