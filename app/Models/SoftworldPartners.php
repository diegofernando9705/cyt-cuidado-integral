<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldPartners extends Model
{
    use HasFactory;

	protected $primaryKey = "partner_code";
	public $incrementing = false;

    protected $fillable = [
		'partner_code',
		'partner_image',
		'partner_title',
		'partner_description',
		'person_contact_names',
		'person_contact_last_names',
		'person_contact_cellphone',
		'person_contact_email',
		'status',
		'date_deleted',
		'deleted_by',
	];

    protected static function active()
	{
		return self::where([["date_deleted", NULL], ["deleted_by", NULL]])->get();
	}

	public function getStatus()
	{
		return $this->hasOne(SoftworldStatusPlatforms::class, 'id', 'status');
	}
}
