<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldPlans extends Model
{
    use HasFactory;

    protected $primaryKey = "code";
    public $incrementing = false;

    protected $fillable = [
        'code',
        'image',
        'title',
        'description',
        'platform_code',
        'start_date',
        'end_date',
        'price',
        'status',
        'date_deleted',
        'deleted_by',
    ];

    protected static function activePlatform()
    {
        return self::where("date_deleted", NULL)->orderBy('created_at', 'DESC')->get();
    }

    protected static function activePublic()
    {
        return self::where([["start_date", "<=", NOW()], ["date_deleted", NULL]])->get();
    }

    public function getStatus()
    {
        return $this->hasOne(SoftworldStatusPlatforms::class, 'id', 'status');
    }

    public function memberships()
    {
        return $this->belongsToMany(SoftworldMembership::class, 'details_plans_memberships', 'plan_id', 'membership_id');
    }
}
