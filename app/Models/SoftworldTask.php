<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldTask extends Model
{
    use HasFactory;

    protected $primaryKey = "code";
    public $incrementing = false;

    protected $fillable = [
        'code',
        'title',
        'description',
        'start_task',
        'end_date',
        'priority',
        'status',
        'date_deleted',
        'deleted_by',
    ];

    public function getTaskAssigneesUser()
    {
        return $this->hasMany(DetailsTaskAssignees::class, 'task_id', 'code')->where('date_deleted', NULL);
    }

    public function getPriority()
    {
        return $this->hasOne(SoftworldPriority::class, 'id', 'priority');
    }

    public function getStatus()
    {
        return $this->hasOne(SoftworldStatusPlatforms::class, 'id', 'status');
    }

    protected static function searchAll($data)
    {
        $query = self::query();

        // Aplicar condición de fecha de eliminación nula
        $query->where('date_deleted', NULL);

        // Aplicar condición para buscar tareas asignadas a ti
        $query->whereHas('getTaskAssigneesUser', function ($query) {
            $query->where('user_id', auth()->user()->id);
        });

        // Aplicar búsqueda LIKE en los campos 'title' y 'description' si se proporcionan en los datos
        if (isset($data['title'])) {
            $query->where('title', 'LIKE', '%' . $data['title'] . '%');
        }

        if (isset($data['description'])) {
            $query->where('description', 'LIKE', '%' . $data['description'] . '%');
        }

        // Ordenar por fecha de creación de forma descendente
        $query->orderBy("created_at", "DESC");

        // Obtener los resultados
        return $query->get();
    }

    public function getFiles()
    {
        return $this->belongsToMany(SoftworldFiles::class, 'details_task_attachments', 'task_id', 'file_id')->where('date_deleted', NULL);
    }
}
