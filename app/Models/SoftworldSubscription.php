<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldSubscription extends Model
{
	use HasFactory;

	protected $fillable = [
		'user_id',
		'plan_id',
		'start_date',
		'end_date',
		'renewal',
		'normal_end_date',
		'date_renewal_cancelled',
		'code_subscription_pay',
	];

	protected static function getDataByUser()
	{
		return self::where("user_id", auth()->user()->id)->latest('created_at')->first();
	}

	public function getPlan()
	{
		return $this->hasOne(SoftworldPlans::class, 'code', 'plan_id');
	}
}
