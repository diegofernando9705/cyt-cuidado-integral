<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailsTaskAssignees extends Model
{
    use HasFactory;

    protected $fillable = [
        'task_id',
        'user_id',
        'assigned_by',
        'date_deleted',
        'deleted_by',
    ];

    public function getTask()
    {
        return $this->hasOne(SoftworldTask::class, 'code', 'task_id')->where('date_deleted', NULL);
    }

    public function getUser()
    {
        return $this->hasMany(User::class, 'id', 'user_id');
    }

    protected static function getTaskByUser($userId)
    {
        return self::where("user_id", $userId)->paginate(10);
    }
}
