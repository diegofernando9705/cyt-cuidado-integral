<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailsAnswersData extends Model
{
    use HasFactory;

    protected $fillable = [
    	'answer_id',
    	'data_id',
    ];
}
