<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailsFilesConcepts extends Model
{
    use HasFactory;

    protected $fillable = [
    	'file_id',
    	'concepts_id',
    ];
}
