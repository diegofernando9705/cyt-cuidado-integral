<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldLevels extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'date_deleted',
        'deleted_by',
    ];

    protected static function active()
	{
		return self::where([["date_deleted", NULL], ["deleted_by", NULL]])->get();
	}
}




