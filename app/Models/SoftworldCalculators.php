<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldCalculators extends Model
{
    use HasFactory;

    protected $primaryKey = "code";
    public $incrementing = false;

    protected $fillable = [
        'code',
        'image',
        'title',
        'description',
        'status',
        'date_deleted',
        'deleted_by',
    ];

    protected static function activePlatform()
    {
        return self::where('date_deleted', NULL)->get();
    }

    public function getStatus()
    {
        return $this->hasOne(SoftworldStatusPlatforms::class, 'id', 'status');
    }

    public function memberships()
    {
        return $this->belongsToMany(SoftworldMembership::class, 'details_calculators_memberships', 'calculator_id', 'membership_id');
    }
}
