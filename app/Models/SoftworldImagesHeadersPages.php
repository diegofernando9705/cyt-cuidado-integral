<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldImagesHeadersPages extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'url',
        'text_button',
        'link_button',
        'page',
        'status',
    ];
}
