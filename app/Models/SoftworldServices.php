<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldServices extends Model
{
	use HasFactory;
	protected $primaryKey = "code";
	public $incrementing = false;

	protected $fillable = [
		'code',
		'image',
		'title',
		'resena',
		'content',
		'url',
		'status',
	];

	protected static function active()
	{
		return self::whereIn("status", [1, 2])->get();
	}

	protected static function url($url)
	{
		return self::where([["url", $url], ["status", 1]])->first();
	}

	public function getStatus(){
		return $this->hasOne(SoftworldStatusPlatforms::class, 'id', 'status');
	}
}
