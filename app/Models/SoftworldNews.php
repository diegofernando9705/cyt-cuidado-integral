<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldNews extends Model
{
    use HasFactory;

    protected $fillable = [
        'image',
        'title',
        'summary',
        'description',
        'url',
        'type',
        'register_date',
        'date_end',
        'user_creator_id',
        'status',
    ];

    protected static function activeAdministrator()
    {
        return self::whereIn("status", [1, 2])->get();
    }

    protected static function active()
    {
        return self::where([["register_date", "<=", NOW()], ["status", 1]])->get();
    }

    protected static function url($url)
    {
        return self::where([["register_date", "<=", NOW()], ["status", 1], ["url", $url]])->first();
    }

    public function getStatus()
    {
        return $this->hasOne(SoftworldStatusPlatforms::class, 'id', 'status');
    }

    public function getUser()
    {
        return $this->hasOne(User::class, 'id', 'user_creator_id');
    }

    public function category()
    {
        return $this->belongsToMany(SoftworldCategory::class, 'details_category_news', 'new_id', 'category_id');
    }

    public function membership()
    {
        return $this->belongsToMany(SoftworldMembership::class, 'details_news_memberships', 'news_id', 'membership_id');
    }
}
