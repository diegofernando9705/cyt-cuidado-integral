<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailsCourseComments extends Model
{
    use HasFactory;

    protected $fillable = [
        'course_id',
        'user_id',
        'comment',
        'date_deleted',
        'deleted_by',
    ];

    public function getUser()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
