<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldHeadersTypes extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'slug',
    ];

    protected static function page($page)
    {
        return self::where("slug", $page)->first();
    }

    public function text()
    {
        return $this->hasOne(SoftworldContentTextHeaders::class, 'id', 'id');
    }

    public function images()
    {
        return $this->hasMany(SoftworldContentImagesHeaders::class, 'page_id', 'id');
    }
}
