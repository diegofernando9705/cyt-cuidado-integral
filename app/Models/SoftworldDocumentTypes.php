<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldDocumentTypes extends Model
{
    use HasFactory;
    protected $primaryKey = "code";
    public $incrementing = false;

    protected $fillable = [
        'code',
        'description',
        'status',
    ];

    protected static function active()
    {
        return self::where("status", 1)->get();
    }
}
