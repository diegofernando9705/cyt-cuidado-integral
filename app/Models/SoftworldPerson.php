<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldPerson extends Model
{
	use HasFactory;

	protected $fillable = [
		'document_type',
		'professional_card',
		'id',
		'first_name',
		'second_name',
		'first_last_name',
		'second_last_name',
		'cellphone',
		'email',
		'profession_id',
		'type',
		'user',
		'token_client',
		'status',
		'date_deleted',
		'deleted_by',
	];

	protected static function getOnlyUser($id)
	{
		$user = self::where("id", $id)->pluck('user')->first();
		return $user;
	}

	protected static function getDataByUser($userId)
	{
		return self::where([["user", $userId], ['date_deleted', NULL], ['status', 1]])->first();
	}

	public function documentType()
	{
		return $this->hasOne(SoftworldDocumentTypes::class, 'code', 'document_type');
	}

	protected static function getByUser()
	{
		return self::where("user", auth()->user()->id)->first();
	}

	protected static function getUsersOnly()
	{
		return self::where([["type", "user"], ['date_deleted', NULL], ['status', 1]])->get();
	}

	protected static function getSubscriberOnly()
	{
		return self::where("type", "subscriber")->get();
	}

	protected static function getUsersOnlyNotRegisterUser()
	{
		return self::where("type", "user")->whereNull('user')->get();
	}

	protected static function active()
	{
		return self::where([["date_deleted", NULL], ["deleted_by", NULL]])->get();
	}

	public function getStatus()
	{
		return $this->hasOne(SoftworldStatusPlatforms::class, 'id', 'status');
	}

	public function getUser()
	{
		return $this->hasOne(User::class, 'id', 'user');
	}

	public function getUnusedPoints()
	{
		return $this->hasMany(DetailsLitipointsPeople::class, 'people_id', 'id')->where('date_use', NULL);
	}

	public function getSubscription()
	{
		return $this->hasMany(SoftworldSubscription::class, 'user_id', 'user')->orderBy('id', 'DESC')->take(1);
	}

	public function getReferred()
	{
		return $this->belongsToMany(SoftworldPerson::class, 'details_person_referrals', 'major_id', 'referred_id');
	}
}
