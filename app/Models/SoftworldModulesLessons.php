<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldModulesLessons extends Model
{
	use HasFactory;

	protected $primaryKey = "code";
	public $incrementing = false;

	protected $fillable = [
		'code',
		'image',
		'title',
		'summary',
		'type',
		'status',
		'date_deleted',
		'deleted_by',
	];

	public function getModules()
	{
		return $this->belongsToMany(SoftworldCoursesModules::class, 'details_lessons_modules', 'lesson_id', 'module_id')->where('date_deleted', NULL);
	}

	public function getFiles()
	{
		return $this->belongsToMany(SoftworldFiles::class, 'details_files_lessons', 'lesson_id', 'file_id')->where('date_deleted', NULL);
	}

	public function getQualification()
	{
		return $this->hasMany(DetalsLessonQualification::class, 'lesson_id', 'code');
	}

	public function getUserQualification($user_id)
	{
		return $this->hasMany(DetailsProgressStudents::class, 'lesson_id', 'code')
			->where('user_id', $user_id)
			->first();
	}

	public function getComments()
	{
		return $this->hasMany(DetailsLessonsComments::class, 'lesson_id')
			->whereNull('date_deleted')
			->orderBy('created_at', 'DESC');
	}
}
