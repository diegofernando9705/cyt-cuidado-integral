<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldCourses extends Model
{
	use HasFactory;

	protected $primaryKey = "code";
	public $incrementing = false;

	protected $fillable = [
		'code',
		'title',
		'summary',
		'description',
		'cover_image',
		'video_image',
		'start_date',
		'end_date',
		'category_id',
		'instructor_id',
		'level',
		'student_limit',
		'status',
		'date_deleted',
		'deleted_by',
	];

	protected static function activePlatform()
	{
		return self::where("date_deleted", NULL)->get();
	}

	protected static function activeSubscriber()
	{
		return self::whereNull('date_deleted')
			->where('start_date', '<=', now())
			->where(function ($query) {
				$query->whereNull('end_date')
					->orWhere('end_date', '>=', now());
			})
			->get();
	}

	public function getInstructor()
	{
		return $this->hasOne(SoftworldPerson::class, 'id', 'instructor_id');
	}

	public function getStatus()
	{
		return $this->hasOne(SoftworldStatusPlatforms::class, 'id', 'status');
	}

	public function getCategory()
	{
		return $this->hasOne(SoftworldCategory::class, 'code', 'category_id');
	}

	public function getLevel()
	{
		return $this->hasOne(SoftworldLevels::class, 'id', 'level');
	}

	public function getArchives()
	{
		return $this->belongsToMany(SoftworldFiles::class, 'details_files_courses', 'course_id', 'file_id');
	}

	public function getModules()
	{
		return $this->belongsToMany(SoftworldCoursesModules::class, 'details_modules_courses', 'course_id', 'module_id')->where("date_deleted", NULL);
	}

	public function getMemberships()
	{
		return $this->belongsToMany(SoftworldMembership::class, 'details_courses_memberships', 'course_id', 'membership_id')->where("date_deleted", NULL);
	}

	public function getComments()
	{
		return $this->hasMany(DetailsCourseComments::class, 'course_id')
			->whereNull('date_deleted')
			->orderBy('created_at', 'DESC');
	}
}
