<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldEconomicIndicators extends Model
{
	use HasFactory;

	protected $fillable = [
		'date_start_indicator',
		'date_end_indicator',
		'value_start_indicator',
		'description_start_indicator',
		'percentage_start_indicator',
		'status_start_indicator',
		'date_deleted',
		'deleted_by'
	];

	protected static function getTRM($limit)
	{
		return self::where("description_start_indicator", 'TRM')->orderBy('date_end_indicator', 'DESC')->take($limit)->get();
	}

	protected static function getIPC($limit)
	{
		return self::where("description_start_indicator", 'IPC')->orderBy('date_end_indicator', 'DESC')->take($limit)->get();
	}

	protected static function getUVR($limit)
	{
		return self::where("description_start_indicator", 'UVR')->orderBy('date_end_indicator', 'DESC')->take($limit)->get();
	}

	protected static function getDTF($limit)
	{
		return self::where("description_start_indicator", 'DTF')->orderBy('date_end_indicator', 'DESC')->take($limit)->get();
	}

	protected static function active()
	{
		return self::where("date_deleted", NULL)->orderBy('id', 'DESC')->get();
	}
}
