<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldPriority extends Model
{
    use HasFactory;

    protected $fillable = [
		'description',
		'module',
		'date_deleted',
		'deleted_by',
	];

	protected static function active($module)
    {
        return self::where([["date_deleted", NULL], ['module', $module]])->get();
    }
}
