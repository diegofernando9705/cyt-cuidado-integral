<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailsTypeContent extends Model
{
    use HasFactory;

    protected $fillable = [
    	'slug_id',
    	'content_id',
    ];
}
