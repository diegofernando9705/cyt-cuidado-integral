<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailsConceptsMemberships extends Model
{
    use HasFactory;

    protected $fillable = [
        'concept_code',
        'membership_code',
    ];
}
