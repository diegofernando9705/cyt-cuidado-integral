<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailsCalculatorsMemberships extends Model
{
    use HasFactory;

    protected $fillable = [
        'calculator_id',
        'membership_id',
    ];

    public static function searchByMembership($membership_id)
    {
        return self::where('membership_id', $membership_id)->get();
    }

    public function getCalculator()
    {
        return $this->belongsTo(SoftworldCalculators::class, 'calculator_id')->where('date_deleted', NULL);
    }
}
