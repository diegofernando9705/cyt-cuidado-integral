<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldCoursesModules extends Model
{
    use HasFactory;

    protected $primaryKey = "code";
    public $incrementing = false;

	protected $fillable = [
		'code',
		'image',
		'title',
		'summary',
		'status',
		'date_deleted',
		'deleted_by',
	];

	public function getCourses()
	{
		return $this->belongsToMany(SoftworldCourses::class, 'details_modules_courses', 'module_id', 'course_id')->where("date_deleted", NULL);
	}

	public function lessons()
	{
		return $this->belongsToMany(SoftworldModulesLessons::class, 'details_lessons_modules', 'module_id', 'lesson_id')->where('date_deleted', NULL);
	}
}
