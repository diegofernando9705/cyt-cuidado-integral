<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailsPersonReferral extends Model
{
    use HasFactory;

    protected $fillable = [
        'major_id',
        'referred_id',
    ];


    public function referred()
    {
        return $this->belongsToMany(SoftworldPerson::class, 'details_person_referrals', 'major_id', 'referred_id');
    }

    public function referrers()
    {
        return $this->belongsToMany(SoftworldPerson::class, 'details_person_referrals', 'referred_id', 'major_id');
    }
}
