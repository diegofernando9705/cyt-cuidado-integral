<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailsFavoritesNews extends Model
{
    use HasFactory;

    protected $fillable = [
        'new_id',
        'user_id',
    ];


    protected static function allUserFavorite($id)
    {
        return self::where('user_id', $id)->get();
    }

    protected static function isUserFavorite($newId)
    {
        return self::where([['new_id', $newId], ['user_id', auth()->user()->id]])->first();
    }

    protected static function deleteFavorite($newId)
    {
        return self::where([['new_id', $newId], ['user_id', auth()->user()->id]])->delete();
    }

    public function getNews()
    {
        return $this->hasMany(SoftworldNews::class, 'id', 'new_id')->where('date_deleted', NULL);
    }
}
