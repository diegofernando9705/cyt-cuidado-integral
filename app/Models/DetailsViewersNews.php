<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailsViewersNews extends Model
{
    use HasFactory;

    protected $fillable = [
    	'new_id',
    	'date_vewer',
    ];

    public static function mostViewedNews()
    {
        return self::select('new_id')
            ->groupBy('new_id')
            ->orderByRaw('COUNT(*) DESC')
            ->take(10)
            ->pluck('new_id')
            ->toArray();
    }
}
