<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldCategory extends Model
{
    use HasFactory;
    protected $primaryKey = "code";
    public $incrementing = false;

    protected $fillable = [
        'code',
        'image',
        'title',
        'description',
        'status',
    ];

    protected static function active()
    {
        return self::whereIn("status", [1, 2])->get();
    }

    public function getStatus(){
		return $this->hasOne(SoftworldStatusPlatforms::class, 'id', 'status');
	}
}
