<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldConcepts extends Model
{
    use HasFactory;
    protected $primaryKey = "code";
    public $incrementing = false;

    protected $fillable = [
        'code',
        'title',
        'description',
        'register_date',
        'end_date',
        'privacy',
        'application_concept_id',
        'status',
        'created_by',
        'date_deleted',
        'deleted_by',
    ];

    protected static function activeAdministrative()
    {
        return self::where("date_deleted", NULL)->orderBy('created_at', 'DESC')->get();
    }

    public function getStatus()
    {
        return $this->hasOne(SoftworldStatusPlatforms::class, 'id', 'status');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    public function archives()
    {
        return $this->belongsToMany(SoftworldFiles::class, 'details_files_concepts', 'concepts_id', 'file_id');
    }

    public function answers()
    {
        return $this->belongsToMany(SoftworldAnswers::class, 'details_answers_data', 'data_id', 'answer_id')->where('date_deleted', NULL)->orderBy('created_at', 'DESC');
    }

    public function memberships()
    {
        return $this->belongsToMany(SoftworldMembership::class, 'details_concepts_memberships', 'concept_code', 'membership_code');
    }
}
