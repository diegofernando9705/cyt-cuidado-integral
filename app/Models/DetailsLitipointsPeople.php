<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailsLitipointsPeople extends Model
{
    use HasFactory;

    protected $fillable = [
        'description_point',
        'people_id',
        'register_date',
        'point',
        'date_use',
        'date_deleted',
        'deleted_by'
    ];
}
