<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldMembership extends Model
{
    use HasFactory;

    protected $primaryKey = "code";
    public $incrementing = false;

    protected $fillable = [
        'code',
        'image',
        'title',
        'description',
        'start_date',
        'end_date',
        'code_platform_subscription',
        'status',
        'date_deleted',
        'deleted_by',
    ];

    protected static function activePlatform()
    {
        return self::where("date_deleted", NULL)->get();
    }

    protected static function active()
    {
        return self::where([["start_date", "<=", NOW()], ["date_deleted", NULL]])->get();
    }

    public function plans()
    {
        return $this->belongsToMany(SoftworldPlans::class, 'details_plans_memberships', 'membership_id', 'plan_id')->where('date_deleted', NULL);
    }

    public function getStatus()
    {
        return $this->hasOne(SoftworldStatusPlatforms::class, 'id', 'status');
    }
}
