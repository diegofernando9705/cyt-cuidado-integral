<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailsAwardsPartner extends Model
{
    use HasFactory;

    protected $fillable = [
    	'award_code',
    	'partner_code',
    ];
}
