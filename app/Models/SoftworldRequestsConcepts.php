<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldRequestsConcepts extends Model
{
    use HasFactory;

    protected $primaryKey = "code";
    public $incrementing = false;

    protected $fillable = [
        'code',
        'title',
        'description',
        'date_register',
        'date_approved',
        'date_rejection',
        'created_by',
        'status',
        'date_deleted',
        'deleted_by',
    ];

    protected static function active()
    {
        return self::where([["date_deleted", NULL], ["deleted_by", NULL]])->get();
    }

    public function getUser()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    public function getStatus()
    {
        return $this->hasOne(SoftworldStatusPlatforms::class, 'id', 'status');
    }
}
