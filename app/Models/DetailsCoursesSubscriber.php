<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailsCoursesSubscriber extends Model
{
    use HasFactory;

    protected $fillable = [
        'course_id',
        'user_id',
        'qualification',
        'start_date',
        'end_date',
        'cancelled_date',
        'updated_date',
    ];

    protected static function getValidatedCourse($course_id, $user)
    {
        return self::where([['course_id', $course_id], ['user_id', $user]])->first();
    }
}
