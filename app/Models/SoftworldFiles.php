<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldFiles extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'type',
        'url',
        'status',
        'date_deleted',
        'deleted_by',
    ];

    protected static function activePlatform()
    {
        return self::where("date_deleted", NULL)->get();
    }

    public function getStatus()
    {
        return $this->hasOne(SoftworldStatusPlatforms::class, 'id', 'status');
    }
}
