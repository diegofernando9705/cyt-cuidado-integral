<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldAnswers extends Model
{
    use HasFactory;

    protected $fillable = [
        'description',
        'created_by',
        'date_deleted',
		'deleted_by',
    ];

    protected static function getData()
    {
        return self::where("date_deleted", NULL)->orderBy('created_at', 'DESC')->get();
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
