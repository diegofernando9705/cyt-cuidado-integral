<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldDescriptionsHeadersPages extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'slug',
    ];

    public static function searchBySlug($slug)
    {
        return self::where("slug", $slug)->first();
    }

    public function texto()
    {
        return $this->hasOne(SoftworldContentTextHeaders::class, 'id', 'id');
    }

    public function images()
	{
		return $this->hasMany(SoftworldContentImagesHeaders::class, 'id', 'id');
	}
}
