<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldQuoteOpinion extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombres_apellidos',    
        'correo_electronico',
        'telefono_celular',
        'ciudad',
        'area_derecho',
        'valor_pretensiones',
        'tipo_cliente',
        'opcion_pretensiones',
        'experiencia_litigio',
        'tribunal',
        'volumen_informacion',
        'servicios_adicionales',
        'area_dictamen',
        'resultado_cotizacion',
        'status',
        'date_deleted',
        'deleted_by'
    ];

    protected static function active()
	{
		return self::where([["date_deleted", NULL], ["deleted_by", NULL]])->get();
	}
}
