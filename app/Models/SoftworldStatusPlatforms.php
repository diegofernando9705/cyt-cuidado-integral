<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldStatusPlatforms extends Model
{
    use HasFactory;

    protected $fillable = [
        'description',
        'module'
    ];

    protected static function basic()
    {
        return self::whereIn("id", [1, 2])->get();
    }

    protected static function schedulingAppointments()
    {
        return self::where("module", "agendamiento-citas")->get();
    }

    protected static function getByModule($module)
    {
        return self::where([['module', $module], ['date_deleted', NULL]])->get();
    }
}
