<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailsFilesLessons extends Model
{
    use HasFactory;

    protected $fillable = [
        'lesson_id',
        'file_id',
    ];
}
