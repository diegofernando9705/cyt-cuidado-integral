<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoftworldAwards extends Model
{
	use HasFactory;

	protected $primaryKey = "award_code";
	public $incrementing = false;

	protected $fillable = [
		'award_code',
		'award_image',
		'award_title',
		'award_description',
		'litipoint_quantity',
		'availability',
		'start_date',
		'end_date',
		'status',
		'date_deleted',
		'deleted_by',
	];

	protected static function active()
	{
		return self::where([["date_deleted", NULL], ["deleted_by", NULL]])->get();
	}

	public function getStatus()
	{
		return $this->hasOne(SoftworldStatusPlatforms::class, 'id', 'status');
	}

	public function partners()
	{
		return $this->belongsToMany(SoftworldPartners::class, 'details_awards_partners', 'award_code', 'partner_code');
	}
	
	public function membership()
    {
        return $this->belongsToMany(SoftworldMembership::class, 'details_awards_memberships', 'award_code', 'membership_code');
    }
}
