<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\softworld_profesiones as Profesiones;
use App\softworld_doctores as Doctores;
use App\softworld_tipo_documento as TipoDocumentos;
use App\softworld_estados_plataforma as EstadosPlataforma;
use App\softworld_pacientes as Pacientes;
use Storage;
use File;
use DB;

class TalentohumanoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('administrativo.talento-humano.index');
    }

    public function listado()
    {

        $array_talento = [];
        $informacion = [];

        if (auth()->user()->id == 1) {
            $talentos_humanos = Doctores::all();
        } else {
            $talentos_humanos = Doctores::where("estado_doctor", 1)->get();
        }

        foreach ($talentos_humanos as $talento_humano) {
            $estado = EstadosPlataforma::where("codigo_estado", $talento_humano->estado_doctor)->get();
            $array_talento[] = [
                "vacio" => '',
                "tarjeta_profesional" => $talento_humano->tarjeta_profesional,
                "cedula_doctor" => $talento_humano->cedula_doctor,
                "primer_nombre" => $talento_humano->primer_nombre . " " . $talento_humano->segundo_nombre,
                "primer_apellido" => $talento_humano->primer_apellido . " " . $talento_humano->segundo_apellido,
                "celular_doctor" => $talento_humano->celular_doctor,
                "correo_doctor" => $talento_humano->correo_doctor,
                "codigo_estado" => $estado[0]->codigo_estado,
                "nombre_estado" => $estado[0]->descripcion_estado,
                "codigo_usuario" => auth()->user()->id,
            ];
        }


        $informacion["data"] = $array_talento;

        return json_encode($informacion);
    }

    /*
        ========================== FUNCIONES PARA EL MODULO DE DOCTORES ==========================
    */

    public function create()
    {
        $profesiones = Profesiones::where("estado_profesion", 1)->get();
        $tiposDocumentos = TipoDocumentos::where("estado_tipo_documento", 1)->get();
        $departamentos = DB::table("softworld_departamentos")->get();
        $estados = EstadosPlataforma::where("codigo_estado", "<", 3)->get();

        return view('administrativo.doctores.create', compact('profesiones', 'departamentos', 'tiposDocumentos', 'estados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'foto_firma' => 'required|mimes:jpeg,jpg,png',
            'tarjeta_profesional' => 'required|unique:softworld_doctores,tarjeta_profesional',
            'tipo_documento' => 'required',
            'cedula' => 'required|unique:softworld_doctores,cedula_doctor',
            'primer_nombre' => 'required',
            'primer_apellido' => 'required',
            'fecha_nacimiento' => 'required',
            'correo_electronico' => 'required|unique:softworld_doctores,correo_doctor',
            'celular' => 'required|unique:softworld_doctores,celular_doctor',
            'profesion' => 'required',
            'ciudad' => 'required',
            'genero' => 'required',
            'estado' => 'required',
        ];

        $messages = [
            'foto_firma.required' => 'La firma es importante',
            'foto_firma.mimes' => 'La firma deber ser un archivo de imagen.',
            'tarjeta_profesional.required' => 'La tarjeta profesional es importante',
            'tarjeta_profesional.unique' => 'La tarjeta profesional pertenece a otro doctor',

            'tipo_documento.required' => 'Selecciona un tipo de documento.',

            'cedula.required' => 'La cédula es importante',
            'cedula.unique' => 'Esta cédula ya se encuentra registrada',

            'primer_nombre.required' => 'El primer nombre es importante.',
            'primer_apellido.required' => 'El primer apellido es importante.',
            'fecha_nacimiento'  => 'Registre la fecha de nacimiento.',

            'correo_electronico.required' => 'El correo electronico es importante.',
            'correo_electronico.unique' => 'Éste correo electronico ya se encuentra registrado.',

            'celular.required' => 'El celular es importante.',
            'celular.unique' => 'Éste celular ya se encuentra registrado.',
            'departamento' => 'Selecciona un departamento.',
            'ciudad' => 'Ingresa una ciudad.',
            'estado' => 'El estado es importante.',
        ];

        $this->validate($request, $rules, $messages);

        if ($request->file('foto_doctor')) {
            $foto = $request->file('foto_doctor')->store("Doctores/" . $request->cedula, 's3');
        } else {
            $foto = "https://s3.amazonaws.com/cytcuidadointegral.com/CYT-LOGO.jpeg";
        }

        $firma = $request->file('foto_firma')->store("Doctores/" . $request->cedula, 's3');

        Doctores::create([
            'cedula_doctor' => $request->cedula,
            'tarjeta_profesional' => $request->tarjeta_profesional,
            'foto_doctor' => $foto,
            'firma_doctor' => $firma,
            'tipo_identificacion' => $request->tipo_documento,
            'primer_nombre' => $request->primer_nombre,
            'segundo_nombre' => $request->segundo_nombre,
            'primer_apellido' => $request->primer_apellido,
            'segundo_apellido' => $request->segundo_apellido,
            'telefono_doctor' => $request->telefono,
            'celular_doctor' => $request->celular,
            'correo_doctor' => $request->correo_electronico,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'id_profesion' => $request->profesion,
            'genero_doctor' => $request->genero,
            'departamento_doctor' => $request->departamento,
            'ciudad_doctor' => $request->ciudad,
            'direccion_doctor' => $request->direccion_doctor,
            'barrio_doctor' => $request->barrio,
            'zona_doctor' => $request->zona,
            'estado_doctor' => $request->estado,
        ]);

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profesiones = Profesiones::all();
        $doctor = Doctores::where('cedula_doctor', $id)->get();
        $departamentos = DB::table("softworld_departamentos")->where("estado_departamento", 1)->get();
        $tipos_documentos = TipoDocumentos::where("estado_tipo_documento", 1)->get();
        $estados = EstadosPlataforma::where("codigo_estado", "<", 3)->get();

        return view('administrativo.doctores.show', compact('doctor', 'profesiones', 'departamentos', 'tipos_documentos', 'estados'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profesiones = Profesiones::all();
        $doctor = Doctores::where('cedula_doctor', $id)->get();
        $departamentos = DB::table("softworld_departamentos")->where("estado_departamento", 1)->get();
        $tipos_documentos = TipoDocumentos::where("estado_tipo_documento", 1)->get();
        $estados = EstadosPlataforma::where("codigo_estado", "<", 3)->get();

        return view('administrativo.doctores.edit', compact('doctor', 'profesiones', 'departamentos', 'tipos_documentos', 'estados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $rules = [
            'cedula' => 'required',
            'primer_nombre' => 'required',
            'primer_apellido' => 'required',
            'correo_electronico' => 'required',
            'celular' => 'required',
            'profesion' => 'required',
            'ciudad' => 'required',
            'genero' => 'required',
            'estado' => 'required',
        ];

        $messages = [
            'tarjeta_profesional.required' => 'La tarjeta profesional es importante',
            'tarjeta_profesional.unique' => 'La tarjeta profesional pertenece a otro doctor',

            'tipo_documento.required' => 'Selecciona un tipo de documento.',

            'cedula.required' => 'La cédula del doctor es importante',
            'cedula.unique' => 'Esta cédula pertenece a otro doctor',

            'primer_nombre.required' => 'El primer nombre del doctor es importante.',
            'primer_apellido.required' => 'El primer apellido del doctor es importante.',
            'fecha_nacimiento'  => 'Registra la fecha de nacimiento del doctor.',

            'correo_electronico.required' => 'El correo electronico del doctor es importante.',
            'correo_electronico.unique' => 'Éste correo electronico, pertenece a otro doctor.',

            'celular.required' => 'El celular del doctor es importante.',
            'departamento' => 'Selecciona un departamento para el doctor.',
            'ciudad' => 'Ingresa una ciudad.',
            'estado' => 'El estado del doctor es importante.',
        ];

        $this->validate($request, $rules, $messages);


        Doctores::where('cedula_doctor', $request->cedula)->update([
            'primer_nombre' => $request->primer_nombre,
            'segundo_nombre' => $request->segundo_nombre,
            'primer_apellido' => $request->primer_apellido,
            'segundo_apellido' => $request->segundo_apellido,
            'telefono_doctor' => $request->telefono,
            'celular_doctor' => $request->celular,
            'correo_doctor' => $request->correo_electronico,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'id_profesion' => $request->profesion,
            'genero_doctor' => $request->genero,
            'departamento_doctor' => $request->departamento,
            'ciudad_doctor' => $request->ciudad,
            'direccion_doctor' => $request->direccion_doctor,
            'barrio_doctor' => $request->barrio,
            'zona_doctor' => $request->zona,
            'estado_doctor' => $request->estado,
        ]);

        if ($request->file('foto_doctor')) {
            $foto = $request->file('foto_doctor')->store("Doctores/" . $request->cedula, 's3');

            Doctores::where('cedula_doctor', $request->cedula)->update([
                'foto_doctor' => $foto,
            ]);
        }

        if ($request->file('foto_firma')) {
            $firma = $request->file('foto_firma')->store("Doctores/" . $request->cedula, 's3');
            Doctores::where('cedula_doctor', $request->cedula)->update([
                'firma_doctor' => $firma,
            ]);
        }

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Doctores::where("cedula_doctor", $id)->update([
            'estado_doctor' => 2,
        ]);

        return true;
    }

    /*
        ========================== FIN FUNCIONES PARA EL MODULO DE DOCTORES ==========================
    */

    /* ASIGNACION DE USUARIOS */
    public function asignacion_pacientes($identificacion)
    {
        $array_asignados = [];
        $pacientes = Pacientes::where("estado_paciente", 1)->get();
        $pacientes_asignados = DB::table("softworld_talentohumano_pacientes")
            ->where("fk_identificacion_talentohumano_tp", $identificacion)
            ->get();

        foreach ($pacientes_asignados as $paciente) {
            $array_asignados[] = $paciente->fk_identificacion_pacientes_tp;
        }

        return view("administrativo.pacientes.asignacion-pacientes", compact("pacientes", "array_asignados", "identificacion"));
    }

    public function asignacion_pacientes_store(Request $request, $cedula)
    {
        //se eliminan registros anteriores para actualizar
        $pacientes_asignados = DB::table("softworld_talentohumano_pacientes")
            ->where("fk_identificacion_talentohumano_tp", $cedula)
            ->delete();

        //se registrar la nueva informacion
        $pacientes = explode(",", $request->pacientes);

        foreach($pacientes as $paciente){
            DB::table("softworld_talentohumano_pacientes")->insert([
                "fk_identificacion_talentohumano_tp" => $cedula,
                "fk_identificacion_pacientes_tp" => $paciente,
            ]);
        }

        return true;
    }
    /* FIN ASIGNACIÓN DE USUARIOS */
}
