<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\softworld_ep AS EPS;
use App\softworld_estados_plataforma as Estados;

class EpsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('configuracion.eps.index');
    }

    public function listado()
    {
        $array_eps = [];

        if (auth()->user()->id == 1) {
            $informaciones_eps = EPS::all();
        } else {
            $informaciones_eps = EPS::where("estado_eps", 1)->get();
        }


        foreach ($informaciones_eps as $eps) {

            $estado = Estados::where("codigo_estado", $eps->estado_eps)->get();

            $array_eps[] = [
                "vacio" => "",
                "id_eps" => $eps->id_eps,
                "nombre_eps" => $eps->nombre_eps,
                "descripcion_eps" => $eps->descripcion_eps,
                "telefono_eps" => $eps->telefono_eps,
                "direccion_eps" => $eps->direccion_eps,
                "codigo_estados" => $estado[0]->codigo_estado,
                "nombre_estados" => $estado[0]->descripcion_estado,
            ];
        }

        $informacion["data"] = $array_eps;

        return json_encode($informacion);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configuracion.eps.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre_eps' => 'required',
            'telefono_eps' => 'required',
            'direccion_eps' => 'required',
            'estado_eps' => 'required',
        ];

        $messages = [
            'nombre_eps.required'       => 'El nombre de la EPS es importante.',
            'telefono_eps.required'       => 'El telefono de la EPS es importante.',
            'direccion_eps.required'       => 'Debe colocar la direccion de la EPS.',
            'estado_eps.required'       => 'Debe seleccionar un estado.',
        ];

        $this->validate($request, $rules, $messages);

        EPS::create([
            'nombre_eps' => $request->nombre_eps,
            'descripcion_eps' => $request->descripcion_eps,
            'telefono_eps' => $request->telefono_eps,
            'direccion_eps' => $request->direccion_eps,
            'estado_eps' => $request->estado_eps,
        ]);

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eps = EPS::where('id_eps', $id)->get();
        return view('configuracion.eps.edit', compact('eps'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'nombre_eps' => 'required',
            'telefono_eps' => 'required',
            'direccion_eps' => 'required',
            'estado_eps' => 'required',
        ];

        $messages = [
            'nombre_eps.required'       => 'El nombre de la EPS es importante.',
            'telefono_eps.required'       => 'El telefono de la EPS es importante.',
            'direccion_eps.required'       => 'Debe colocar la direccion de la EPS.',
            'estado_eps.required'       => 'Debe seleccionar un estado.',
        ];

        $this->validate($request, $rules, $messages);

        EPS::where('id_eps', $request->id_eps)->update([
            'nombre_eps' => $request->nombre_eps,
            'descripcion_eps' => $request->descripcion_eps,
            'telefono_eps' => $request->telefono_eps,
            'direccion_eps' => $request->direccion_eps,
            'estado_eps' => $request->estado_eps,
        ]);

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EPS::where('id_eps', $id)->update([
            'estado_eps' => 2,
        ]);

        return true;
    }
}
