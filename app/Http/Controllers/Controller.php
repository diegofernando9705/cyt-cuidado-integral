<?php

namespace App\Http\Controllers;

use App\Models\SoftworldSubscription;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function membershipCurrentUser()
    {
        $subscriptionCurrent = SoftworldSubscription::getDataByUser();
        $plans = $subscriptionCurrent->getPlan->memberships;

        foreach($plans as $membership){
            return $membership->code;
        }
    }
}
