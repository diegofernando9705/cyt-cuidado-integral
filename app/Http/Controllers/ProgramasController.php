<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\softworld_programa as Programas;
use App\softworld_estados_plataforma as Estados;
use DB;

class ProgramasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programas = Programas::all();
        return view('configuracion.programas.index', compact('programas'));
    }

    public function listado()
    {

        $array_programas = [];

        if (auth()->user()->id == 1) {
            $programas = Programas::all();
        } else {
            $programas = Programas::where("estado_programas", 1)->get();
        }

        foreach ($programas as $programa) {
            $estado = Estados::where("codigo_estado", $programa->estado_programas)->get();

            $array_programas[] = [
                "vacio" => "",
                "id_programadas" => $programa->id_programadas,
                "titulo_programas" => $programa->titulo_programas,
                "descripcion_programas" => $programa->descripcion_programas,
                "codigo_estados" => $estado[0]->codigo_estado,
                "nombre_estados" => $estado[0]->descripcion_estado,
            ];
        }

        $informacion["data"] = $array_programas;
        return json_encode($informacion);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configuracion.programas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre_programa' => 'required',
            'descripcion_programa' => 'required',
            'estado_programa' => 'required',
        ];

        $messages = [
            'nombre_programa.required'       => 'El nombre del programa es importante.',
            'descripcion_programa.required'       => 'La descripción del programa es importante.',
            'estado_programa.required'       => 'Debe seleccionar un estado.',
        ];

        $this->validate($request, $rules, $messages);

        $numerico = Programas::all()->count() + 1;
        $codigo = "CYT_PROG" . $numerico;

        Programas::create([
            'id_programadas' => $codigo,
            'titulo_programas' => $request->nombre_programa,
            'descripcion_programas' => $request->descripcion_programa,
            'estado_programas' => $request->estado_programa,
        ]);

        DB::table('softworld_procesos_acciones_plataforma')->insert([
            'titulo_proceso' => 'Creacion de programa',
            'descripcion_proceso' => 'El usuario ' . auth()->user()->name . ' hizo un registro de programa con id PROG-' . $codigo . " en la plataforma.",
            'codigo_usuario' =>  auth()->user()->id,
            'fecha_proceso' => date('Y-m-d H:i:s'),
        ]);

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $programa = Programas::where('id_programadas', $id)->get();
        return view('configuracion.programas.show', compact('programa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $programa = Programas::where('id_programadas', $id)->get();
        return view('configuracion.programas.edit', compact('programa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request)
    {
        $rules = [
            'titulo_programas' => 'required',
            'descripcion_programas' => 'required',
            'estado_programas' => 'required',
        ];

        $messages = [
            'titulo_programas.required'       => 'El nombre del procedimiento es importante.',
            'descripcion_programas.required'       => 'La descripción del procedimiento es importante.',
            'estado_programas.required'       => 'Debe seleccionar un estado.',
        ];

        $this->validate($request, $rules, $messages);

        Programas::where('id_programadas', $request->codigo)->update([
            'titulo_programas' => $request->titulo_programas,
            'descripcion_programas' => $request->descripcion_programas,
            'estado_programas' => $request->estado_programas,
        ]);

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Programas::where('id_programadas', $id)->update([
            'estado_programas' => 2,
        ]);

        return true;
    }
}
