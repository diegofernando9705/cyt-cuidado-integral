<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\softworld_procedimientos as Procedimientos;
use App\softworld_profesiones as Profesiones;
use App\softworld_estados_plataforma as EstadosPlataforma;


use DB;

class ProfesionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('configuracion.profesiones.index');
    }

    public function listado()
    {

        $array_profesion = [];
        $informacion = [];

        if (auth()->user()->id == 1) {
            $profesiones = Profesiones::all();
        } else {
            $profesiones = Profesiones::where("estado_profesion", 1)->get();
        }

        foreach ($profesiones as $profesion) {
            $estado = EstadosPlataforma::where("codigo_estado", $profesion->estado_profesion)->get();
            $array_profesion[] = [
                "codigo_profesion" => $profesion->codigo_profesion,
                "nombre_profesion" => $profesion->nombre_profesion,
                "descripcion_profesion" => $profesion->descripcion_profesion,
                "codigo_estado" => $estado[0]->codigo_estado,
                "nombre_estado" => $estado[0]->descripcion_estado,
            ];
        }

        $informacion["data"] = $array_profesion;

        return json_encode($informacion);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $procedimientos = Procedimientos::all();
        $codigo = Profesiones::count() + 1;
        $codigo_profesion = "CYT_PROF" . $codigo;

        return view('configuracion.profesiones.create', compact('procedimientos', 'codigo_profesion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre_profesion' => 'required',
            'descripcion_profesion' => 'required',
            'estado_profesion' => 'required',
        ];

        $messages = [
            'nombre_profesion.required' => 'Coloca el nombre de la profesion.',
            'descripcion_profesion.required' => 'La descripción de la profesión es importante.',
            'estado_profesion.required' => 'El estado de la profesión es importante.',
        ];

        $this->validate($request, $rules, $messages);

        $codigo = Profesiones::count() + 1;
        $codigoProfesion = "CYT_PROF" . $codigo;

        Profesiones::create([
            'codigo_profesion' => $codigoProfesion,
            'nombre_profesion' => $request->nombre_profesion,
            'descripcion_profesion' => $request->descripcion_profesion,
            'estado_profesion' => $request->estado_profesion,
        ]);

        if ($request->procedimientos) {
            foreach ($request->procedimientos as $value) {
                DB::table('detalle_profesiones_procedimientos')->insert([
                    'id_profesion' => $codigoProfesion,
                    'id_procedimiento' => $value,
                ]);
            }
        }

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profesion = Profesiones::where('codigo_profesion', $id)->get();
        $procedimientos = Procedimientos::all();
        $detalle_profesiones_procedimientos = [];

        $procedimientos_asignados = DB::table('detalle_profesiones_procedimientos')
            ->where('id_profesion', $id)
            ->get();

        foreach ($procedimientos_asignados as $procedimiento) {
            $detalle_profesiones_procedimientos[] = $procedimiento->id_procedimiento;
        }

        return view('configuracion.profesiones.show', compact('profesion', 'procedimientos', 'detalle_profesiones_procedimientos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {

        $profesion = Profesiones::where('codigo_profesion', $id)->get();
        $procedimientos = Procedimientos::all();
        $detalle_profesiones_procedimientos = [];
        $estados = EstadosPlataforma::where("codigo_estado", "<", 3)->get();

        $procedimientos_asignados = DB::table('detalle_profesiones_procedimientos')
            ->where('id_profesion', $id)
            ->get();

        foreach ($procedimientos_asignados as $procedimiento) {
            $detalle_profesiones_procedimientos[] = $procedimiento->id_procedimiento;
        }

        return view('configuracion.profesiones.edit', compact('profesion', 'procedimientos', 'detalle_profesiones_procedimientos', 'estados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request)
    {
        $rules = [
            'nombre_profesion' => 'required',
            'codigo' => 'required',
            'descripcion_profesion' => 'required',
            'estado_profesion' => 'required',
        ];

        $messages = [
            'nombre_profesion.required' => 'Coloca el nombre de la profesion.',
            'descripcion_profesion.required' => 'La descripción de la profesión es importante.',
            'estado_profesion.required' => 'El estado de la profesión es importante.',
        ];

        $this->validate($request, $rules, $messages);

        Profesiones::where('codigo_profesion', $request->codigo)->update([
            'nombre_profesion' => $request->nombre_profesion,
            'descripcion_profesion' => $request->descripcion_profesion,
            'estado_profesion' => $request->estado_profesion,
        ]);

        DB::table('detalle_profesiones_procedimientos')->where('id_profesion', $request->codigo)->delete();

        if ($request->procedimientos) {


            foreach ($request->procedimientos as $value) {
                DB::table('detalle_profesiones_procedimientos')->insert([
                    'id_profesion' => $request->codigo,
                    'id_procedimiento' => $value,
                ]);
            }
        }

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Profesiones::where('codigo_profesion', $id)->update([
            'estado_profesion' => 2,
        ]);

        return true;
    }
}
