<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\softworld_cie10 AS CIE10;
use App\softworld_estados_plataforma as Estados;
use DB;

class Cie10Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('configuracion.cie10.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listado()
    {

        $array_cie10 = [];
        $informacion = [];

        if (auth()->user()->id == 1) {
            $informacion_cie10 =  DB::table("softworld_cie10")->get();
        } else {
            $informacion_cie10 =  DB::table("softworld_cie10")->where("estado", 1)->get();
        }

        foreach ($informacion_cie10 as $cie10) {
            $estado = Estados::where("codigo_estado", $cie10->estado)->get();
            $array_cie10[] = [
                "vacio" => "",
                "codigo" => $cie10->codigo,
                "descripcion" => $cie10->descripcion,
                "estado" => $cie10->estado,
                "codigo_estado" => $estado[0]->codigo_estado,
                "nombre_estado" => $estado[0]->descripcion_estado,
            ];
        }

        $informacion["data"] = $array_cie10;

        return json_encode($informacion);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
