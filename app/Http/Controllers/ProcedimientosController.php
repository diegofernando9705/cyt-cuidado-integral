<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\softworld_procedimientos as Procedimiento;
use App\softworld_estados_plataforma as Estados;

class ProcedimientosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $procedimientos = Procedimiento::all();
        return view('configuracion.procedimientos.index', compact('procedimientos'));
    }

    public function listado()
    {
        $array_procedimientos = [];


        if (auth()->user()->id == 1) {
            $procedimientos = Procedimiento::all();
        } else {
            $procedimientos = Procedimiento::where("estado_procedimiento", 1)->get();
        }


        foreach ($procedimientos as $procedimiento) {

            $estado = Estados::where("codigo_estado", $procedimiento->estado_procedimiento)->get();

            $array_procedimientos[] = [
                "vacio" => "",
                "codigo_procedimiento" => $procedimiento->codigo_procedimiento,
                "nombre_procedimiento" => $procedimiento->nombre_procedimiento,
                "descripcion_procedimiento" => $procedimiento->descripcion_procedimiento,
                "codigo_estados" => $estado[0]->codigo_estado,
                "nombre_estados" => $estado[0]->descripcion_estado,
            ];
        }

        $informacion["data"] = $array_procedimientos;
        return json_encode($informacion);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configuracion.procedimientos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre_procedimiento' => 'required',
            'descripcion_procedimiento' => 'required',
            'estado_procedimiento' => 'required',
        ];

        $messages = [
            'nombre_procedimiento.required'       => 'El nombre del procedimiento es importante.',
            'descripcion_procedimiento.required'       => 'La descripción del procedimiento es importante.',
            'estado_procedimiento.required'       => 'Debe seleccionar un estado.',
        ];

        $this->validate($request, $rules, $messages);


        $numerico = Procedimiento::all()->count() + 1;
        $codigo = "CYT_PROC" . $numerico;

        Procedimiento::create([
            'codigo_procedimiento' => $codigo,
            'nombre_procedimiento' => $request->nombre_procedimiento,
            'descripcion_procedimiento' => $request->descripcion_procedimiento,
            'estado_procedimiento' => $request->estado_procedimiento,
        ]);

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $procedimiento = Procedimiento::where('codigo_procedimiento', $id)->get();
        return view('configuracion.procedimientos.show', compact('procedimiento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $procedimiento = Procedimiento::where('codigo_procedimiento', $id)->get();
        return view('configuracion.procedimientos.edit', compact('procedimiento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request)
    {
        $rules = [
            'nombre_procedimiento' => 'required',
            'descripcion_procedimiento' => 'required',
            'estado_procedimiento' => 'required',
        ];

        $messages = [
            'nombre_procedimiento.required'       => 'El nombre del procedimiento es importante.',
            'descripcion_procedimiento.required'       => 'La descripción del procedimiento es importante.',
            'estado_procedimiento.required'       => 'Debe seleccionar un estado.',
        ];

        $this->validate($request, $rules, $messages);

        Procedimiento::where('codigo_procedimiento', $request->codigo)->update([
            'nombre_procedimiento' => $request->nombre_procedimiento,
            'descripcion_procedimiento' => $request->descripcion_procedimiento,
            'estado_procedimiento' => $request->estado_procedimiento,
        ]);

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Procedimiento::where('codigo_procedimiento', $id)->update([
            'estado_procedimiento' => 2
        ]);

        return true;
    }
}
