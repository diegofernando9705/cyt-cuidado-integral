<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use App\softworld_doctores AS Doctores;

use DB;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('seguridad.usuarios.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        $doctores = Doctores::where('codigo_usuario', null)->get();

        return view('seguridad.usuarios.create', compact('roles', 'doctores'));
    }


    public function listado()
    {
        $usuarios = User::all();

        return User::paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'nombre_usuario' => 'required',
            'correo_usuario' => 'required|unique:users,email',
            'password' => 'required|required_with:confirmar_password|same:confirmar_password',
            'confirmar_password' => 'required',
            'rol_usuario' => 'required',
            'persona_asignada' => 'required',
            'estado_usuario' => 'required',
        ];

        $messages = [
            'nombre_usuario.required'       => 'El nombre del usuario es importante.',

            'correo_usuario.required'       => 'El correo electronico es importante.',
            'correo_usuario.unique'       => 'El correo electronico ya se encuentra registrado.',

            'persona_asignada.required'       => 'Debe seleccionar una persona para este usuario.',

            'password.required_with'       => 'Las contraseñas no coinciden.',
            'password.same'       => 'Las contraseñas no coinciden.',
        ];

        $this->validate($request, $rules, $messages);

        if($request->file('change-picture')){
            $foto = $request->file('change-picture')->store("Usuarios/".$request->persona_asignada, 's3');
        }else{
            $foto = 'vacio';
        }

        $usuario = User::create([
            'avatar' => $foto,
            'name' => $request->nombre_usuario,
            'email' => $request->correo_usuario,
            'password' => Hash::make($request->password),
            'condicion' =>  $request->estado_usuario,
        ])->assignRole($request->rol_usuario);


        Doctores::where('cedula_doctor', $request->persona_asignada)->update([
            'codigo_usuario' => $usuario->id
        ]);

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roles = Role::all();
        $usuario = DB::table('model_has_roles')
                    ->select('roles.*', 'model_has_roles.*', 'users.*', 'users.id AS id_usuario', 'roles.id AS id_rol', 'roles.name AS nombre_rol')
                    ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                    ->join('users', 'model_has_roles.model_id', '=', 'users.id')
                    ->where('users.id', $id)
                    ->get();

        $persona =  Doctores::where('codigo_usuario', $id)->get();
        
        User::where('id', $id)->get();
        return view('seguridad.usuarios.show', compact('usuario', 'roles', 'persona'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();
        $usuario = DB::table('model_has_roles')
                    ->select('roles.*', 'model_has_roles.*', 'users.*', 'users.id AS id_usuario', 'roles.id AS id_rol', 'roles.name AS nombre_rol')
                    ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                    ->join('users', 'model_has_roles.model_id', '=', 'users.id')
                    ->where('users.id', $id)
                    ->get();

        User::where('id', $id)->get();
        
         return view('seguridad.usuarios.edit', compact('usuario', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {


        $rules = [
            'nombre_usuario' => 'required|unique:users,name,'.$request->id_usuario,
            'correo_usuario' => 'required|unique:users,email,'.$request->id_usuario,
            'password' => 'required_with:confirmar_password|same:confirmar_password',
            'rol_usuario' => 'required',
            'estado_usuario' => 'required',
        ];

        $messages = [
            'nombre_usuario.required'       => 'El nombre del usuario es importante.',
            'nombre_usuario.unique'       => 'El nombre del usuario ya esta rgeistrado.',

            'correo_usuario.required'       => 'El correo electronico es importante.',
            'correo_usuario.unique'       => 'El correo electronico ya se encuentra registrado.',

            'password.required_with'       => 'Las contraseñas no coinciden.',
            'password.same'       => 'Las contraseñas no coinciden.',
        ];

        $this->validate($request, $rules, $messages);

        DB::table('model_has_roles')->where('model_id', $request->id_usuario)->delete();

        DB::table('model_has_roles')->insert(['model_id' => $request->id_usuario, 'model_type' => 'App\User', 'role_id' => $request->rol_usuario]);

        if($request->password){
            User::where('id', $request->id_usuario)->update([
                'name' => $request->nombre_usuario,
                'email' => $request->correo_usuario,
                'password' => Hash::make($request->password),
                'condicion' => $request->estado_usuario,
            ]);
        }else{
            $user = User::where('id', $request->id_usuario)->update([
                'name' => $request->nombre_usuario,
                'email' => $request->correo_usuario,
                'condicion' => $request->estado_usuario,
            ]);
        }

        return true;
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function seleccionar_correo($cedula){
        $correo = Doctores::where('cedula_doctor', $cedula)->get();
        return $correo[0]->correo_doctor;
    }

    public function perfil(){
        $usuario = User::where('id', auth()->user()->id)->get();
        return view('seguridad.usuarios.perfil', compact('usuario'));
    }
}
