<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\softworld_pacientes as Pacientes;
use App\softworld_doctores as Doctores;
use App\softworld_programa as Programas;
use App\softworld_profesiones as Profesiones;

use DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* === patologias en la IPS === */
        $array_patologias = [];
        $patologias = [];

        $pacientes = Pacientes::all();

        foreach ($pacientes as $paciente) {
            $temporaly = DB::table("detalle_formularios_pacientes")
                ->where("id_persona", $paciente->cedula_paciente)
                ->where("tipo_formulario", "formulario_admision")
                ->get();

            if (count($temporaly) >= 1) {
                $informacion_formulario = DB::table("softworld_formulario_admisiones")
                    ->where("id_formulario_admision", $temporaly[0]->id_formulario)
                    ->get();
            } else {
                $informacion_formulario = DB::table("softworld_formulario_admisiones")
                    ->where("id_formulario_admision", 1)
                    ->get();
            }

            foreach ($informacion_formulario as $data) {
                $data_cie10 = DB::table("softworld_cie10")
                    ->where("codigo", $data->registro_cie10)
                    ->get();

                foreach ($data_cie10 as $cie10) {
                    $array_patologias[$cie10->codigo][] = $cie10->descripcion;
                }
            }
        }


        foreach ($array_patologias as $key => $data_patologia) {
            $sumatoria = count($array_patologias[$key]);
            foreach ($data_patologia as $data) {
                $patologias[$key] = [
                    "nombre" => $data,
                    "total" => $sumatoria,
                ];
            }
        };

        /* === fin de patologias en la IPS === */

        /* == Numero de registros activos == */

        $pacientes = Pacientes::where("estado_paciente", 1)->count();
        $doctores = Doctores::where("estado_doctor", 1)->count();
        $programas = Programas::where("estado_programas", 1)->count();
        $profesiones = Profesiones::where("estado_profesion", 1)->count();
        $eps = Doctores::where("estado_doctor", 1)->count();
        /* == fin Numero de registros activos == */

        $pageConfigs = ['pageHeader' => false];

        return view('principal', ['pageConfigs' => $pageConfigs], compact("patologias", "pacientes", "doctores", "programas", "profesiones"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
