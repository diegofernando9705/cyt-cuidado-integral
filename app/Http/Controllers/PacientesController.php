<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use Barryvdh\DomPDF\Facade\Pdf;
use Storage;

use App\softworld_pacientes as Pacientes;
use App\softworld_doctores as Doctores;
use App\softworld_formulario_admisione as FormularioAdmision;
use App\softworld_estados_plataforma as EstadosPlataforma;
use App\softworld_historias_clinica as HistoriasClinicas;
use App\softworld_tipo_documento as TipoDocumentos;
use App\softworld_notas_paciente as NotasPacientes;
use App\softworld_acompanante as Acompanantes;
use App\softworld_programa as Programas;
use App\softworld_interconsulta as Interconsultas;
use App\softworld_profesiones as Profesiones;

use App\softworld_ep as EPS;


use DateTime;
use App\User;
use DB;

class PacientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('administrativo.pacientes.index');
    }


    public function listado()
    {
        $array_pacientes = [];
        $informacion = [];

        if (auth()->user()->id == 1) {
            $pacientes = Pacientes::all();

            foreach ($pacientes as $paciente) {
                $estado = EstadosPlataforma::where("codigo_estado", $paciente->estado_paciente)->get();
                $array_pacientes[] = [
                    "vacio" => '',
                    "notificacion" => "",
                    "cedula_paciente" => $paciente->cedula_paciente,
                    "primer_nombre" => $paciente->primer_nombre . " " . $paciente->segundo_nombre,
                    "primer_apellido" => $paciente->primer_apellido . " " . $paciente->segundo_apellido,
                    "celular_paciente" => $paciente->celular_paciente,
                    "correo_paciente" => $paciente->correo_paciente,
                    "codigo_estado" => $estado[0]->codigo_estado,
                    "nombre_estado" => $estado[0]->descripcion_estado,
                ];
            }
        } else {

            $informacion_talento_humano = Doctores::where("codigo_usuario", auth()->user()->id)
                ->get();

            //busco los pacientes asignados al de talento humano
            $pacientes = DB::table("softworld_talentohumano_pacientes")
                ->where("fk_identificacion_talentohumano_tp", $informacion_talento_humano[0]->cedula_doctor)
                ->get();

            foreach ($pacientes as $informacion_paciente) {

                $paciente = Pacientes::where(
                    [
                        ["cedula_paciente", $informacion_paciente->fk_identificacion_pacientes_tp],
                        ['estado_paciente', '<>', '2']
                    ]
                )->get();

                $estado = EstadosPlataforma::where("codigo_estado", $paciente[0]->estado_paciente)->get();

                $array_pacientes[] = [
                    "vacio" => '',
                    "notificacion" => "",
                    "cedula_paciente" => $paciente[0]->cedula_paciente,
                    "primer_nombre" => $paciente[0]->primer_nombre . " " . $paciente[0]->segundo_nombre,
                    "primer_apellido" => $paciente[0]->primer_apellido . " " . $paciente[0]->segundo_apellido,
                    "celular_paciente" => $paciente[0]->celular_paciente,
                    "correo_paciente" => $paciente[0]->correo_paciente,
                    "codigo_estado" => $estado[0]->codigo_estado,
                    "nombre_estado" => $estado[0]->descripcion_estado,
                ];
            }
        }

        $informacion["data"] = $array_pacientes;

        return json_encode($informacion);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departamentos = DB::table("softworld_departamentos")->get();
        $tiposDocumentos = TipoDocumentos::where("estado_tipo_documento", 1)->get();

        return view('administrativo.pacientes.create', compact("departamentos", "tiposDocumentos"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'tipo_documento' => 'required',
            'cedula' => 'required|unique:softworld_pacientes,cedula_paciente',
            'primer_nombre' => 'required',
            'primer_apellido' => 'required',
            'fecha_nacimiento' => 'required',
            'correo_electronico' => 'required|unique:softworld_pacientes,correo_paciente',
            'celular' => 'required',
            'ocupacion' => 'required',
            'genero' => 'required',
            'grupo_sanguineo' => 'required',
            'departamento' => 'required',
            'ciudad' => 'required',
            'direccion' => 'required',
            'barrio' => 'required',
            'cedula_acompanante' => 'required',
            'nombre_acompanante' => 'required',
            'numero_acompanante' => 'required',
            'parentesco_acompanante' => 'required',
        ];

        $messages = [
            'tipo_documento.required' => 'Selecciona un tipo de documento.',

            'cedula.required' => 'La cédula del paciente es importante',
            'cedula.unique' => 'Esta cédula pertenece a otro paciente',

            'primer_nombre.required' => 'El primer nombre del paciente es importante.',
            'primer_apellido.required' => 'El primer apellido del paciente es importante.',
            'fecha_nacimiento' => 'Registra la fecha de nacimiento del paciente.',

            'correo_electronico.required' => 'El correo electronico del paciente es importante.',
            'correo_electronico.unique' => 'Éste correo electronico, pertenece a otro paciente.',

            'celular.required' => 'El celular del paciente es importante.',
            'departamento' => 'Selecciona un departamento para el paciente.',
            'ciudad' => 'Ingresa una ciudad.',

            'cedula_acompanante' => 'La cedula acompanante es obligatorio.',
            'nombre_acompanante' => 'El nombre acompanante es obligatorio.',
            'numero_acompanante' => 'El numero acompanante es obligatorio.',
            'parentesco_acompanante' => 'El parentesco acompanante es obligatorio.',

        ];

        $this->validate($request, $rules, $messages);

        if ($request->file('change-picture')) {
            $foto = $request->file('change-picture')->store("Pacientes/" . $request->cedula, 's3');
        } else {
            $foto = 'CYT-LOGO.jpeg';
        }



        Pacientes::create([
            'foto_paciente' => $foto,
            'cedula_paciente' => $request->cedula,
            'tipo_identificacion' => $request->tipo_documento,
            'primer_nombre' => $request->primer_nombre,
            'segundo_nombre' => $request->segundo_nombre,
            'primer_apellido' => $request->primer_apellido,
            'segundo_apellido' => $request->segundo_apellido,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'correo_paciente' => $request->correo_electronico,
            'telefono_paciente' => $request->telefono,
            'celular_paciente' => $request->celular,
            'ocupacion_paciente' => $request->ocupacion,
            'genero_paciente' => $request->genero,
            'grupo_sanguineo' => $request->grupo_sanguineo,
            'departamento_paciente' => $request->departamento,
            'ciudad_paciente' => $request->ciudad,
            'direccion_paciente' => $request->direccion,
            'barrio_paciente' => $request->barrio,
            'comuna_paciente' => $request->comuna,
            'estado_paciente' => 3,
        ]);

        Acompanantes::create([
            'cedula_acompanante' => $request->cedula_acompanante,
            'nombre_completo_acompanante' => $request->nombre_acompanante,
            'numero_acompanante' => $request->numero_acompanante,
            'parentesco_acompanante' => $request->parentesco_acompanante,
        ]);

        DB::table('detalle_acompanante_paciente')->insert([
            'fk_cedula_paciente' => $request->cedula,
            'fk_cedula_acompanante' => $request->cedula_acompanante,
        ]);

        DB::table('softworld_procesos_acciones_plataforma')->insert([
            'titulo_proceso' => 'Creacion de paciente',
            'descripcion_proceso' => 'El usuario ' . auth()->user()->name . ' hizo un registro de paciente con identificacion ' . $request->cedula . " en la plataforma.",
            'codigo_usuario' =>  auth()->user()->id,
            'fecha_proceso' => date('Y-m-d H:i:s'),
        ]);

        DB::table('softworld_procesos_acciones_plataforma')->insert([
            'titulo_proceso' => 'Creacion de acompañante',
            'descripcion_proceso' => 'El usuario ' . auth()->user()->name . ' hizo un registro de acompañante con identificacion ' . $request->cedula_acompanante . " en la plataforma.",
            'codigo_usuario' =>  auth()->user()->id,
            'fecha_proceso' => date('Y-m-d H:i:s'),
        ]);

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paciente = Pacientes::where('cedula_paciente', $id)->get();
        $departamento = DB::table("softworld_departamentos")->where('codigo_dane', $paciente[0]->departamento_paciente)->get();
        return view('administrativo.pacientes.show', compact('paciente', 'departamento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tiposDocumentos = TipoDocumentos::where("estado_tipo_documento", 1)->get();
        $acompanante = DB::table('detalle_acompanante_paciente')
            ->select('detalle_acompanante_paciente.*', 'softworld_pacientes.*', 'softworld_acompanantes.*')
            ->join('softworld_pacientes', 'detalle_acompanante_paciente.fk_cedula_paciente', '=', 'softworld_pacientes.cedula_paciente')
            ->join('softworld_acompanantes', 'detalle_acompanante_paciente.fk_cedula_acompanante', '=', 'softworld_acompanantes.cedula_acompanante')
            ->get();
        $departamentos = DB::table("softworld_departamentos")->get();
        $paciente = Pacientes::where('cedula_paciente', $id)->get();
        return view('administrativo.pacientes.edit', compact('paciente', 'acompanante', 'departamentos', 'tiposDocumentos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'cedula' => 'required',
            'primer_nombre' => 'required',
            'primer_apellido' => 'required',
            'fecha_nacimiento' => 'required',
            'correo_electronico' => 'required',
            'celular_paciente' => 'required',

            'cedula_acompanante' => 'required',
            'nombre_acompanante' => 'required',
            'numero_acompanante' => 'required',
            'parentesco_acompanante' => 'required',
        ];

        $messages = [
            'cedula.required' => 'La cédula del paciente es importante',

            'primer_nombre.required' => 'El primer nombre del paciente es importante.',
            'primer_apellido.required' => 'El primer apellido del paciente es importante.',
            'fecha_nacimiento' => 'Registra la fecha de nacimiento del paciente.',

            'correo_electronico.required' => 'El correo electronico del paciente es importante.',

            'celular_paciente.required' => 'El celular del paciente es importante.',

            'ciudad' => 'Ingresa una ciudad.',

            'cedula_acompanante' => 'La cedula acompanante es obligatorio.',
            'nombre_acompanante' => 'El nombre acompanante es obligatorio.',
            'numero_acompanante' => 'El numero acompanante es obligatorio.',
            'parentesco_acompanante' => 'El parentesco acompanante es obligatorio.',
        ];

        $this->validate($request, $rules, $messages);


        Pacientes::where('cedula_paciente', $request->cedula)->update([
            'primer_nombre' => $request->primer_nombre,
            'segundo_nombre' => $request->segundo_nombre,
            'primer_apellido' => $request->primer_apellido,
            'segundo_apellido' => $request->segundo_apellido,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'correo_paciente' => $request->correo_electronico,
            'telefono_paciente' => $request->telefono,
            'celular_paciente' => $request->celular_paciente,
            'ocupacion_paciente' => $request->ocupacion,
            'genero_paciente' => $request->genero,
            'grupo_sanguineo' => $request->grupo_sanguineo,
            'departamento_paciente' => $request->departamento,
            'ciudad_paciente' => $request->ciudad,
            'direccion_paciente' => $request->direccion,
            'barrio_paciente' => $request->barrio,
            'comuna_paciente' => $request->comuna,
        ]);

        if ($request->file('foto_paciente')) {
            $foto = $request->file('foto_paciente')->store("Pacientes/" . $request->cedula, 's3');

            Pacientes::where('cedula_paciente', $request->cedula)->update([
                'foto_paciente' => $foto,
            ]);
        } else {
            $foto = '';
        }


        $verificar_acompanante = Acompanantes::where('cedula_acompanante', $request->cedula_acompanante)->get();

        if (count($verificar_acompanante) >= 1) {

            Acompanantes::where('cedula_acompanante', $request->cedula_acompanante)->update([
                'cedula_acompanante' => $request->cedula_acompanante,
                'nombre_completo_acompanante' => $request->nombre_acompanante,
                'numero_acompanante' => $request->numero_acompanante,
                'parentesco_acompanante' => $request->parentesco_acompanante,
            ]);
        } else {

            Acompanantes::create([
                'cedula_acompanante' => $request->cedula_acompanante,
                'nombre_completo_acompanante' => $request->nombre_acompanante,
                'numero_acompanante' => $request->numero_acompanante,
                'parentesco_acompanante' => $request->parentesco_acompanante,
            ]);

            DB::table('softworld_procesos_acciones_plataforma')->insert([
                'titulo_proceso' => 'Creacion de acompañante',
                'descripcion_proceso' => 'El usuario ' . auth()->user()->name . ' hizo un registro de acompañante con identificacion ' . $request->cedula_acompanante . " en la plataforma.",
                'codigo_usuario' =>  auth()->user()->id,
                'fecha_proceso' => date('Y-m-d H:i:s'),
            ]);
        }



        DB::table('softworld_procesos_acciones_plataforma')->insert([
            'titulo_proceso' => 'Actualizacion de paciente',
            'descripcion_proceso' => 'El usuario ' . auth()->user()->name . ' hizo una actualizacion informacion al paciente con identificacion ' . $request->cedula . " en la plataforma.",
            'codigo_usuario' =>  auth()->user()->id,
            'fecha_proceso' => date('Y-m-d H:i:s'),
        ]);

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pacientes::where('cedula_paciente', $id)->update([
            "estado_paciente" => 2
        ]);

        return true;
    }

    public function active($id)
    {
        Pacientes::where('cedula_paciente', $id)->update([
            "estado_paciente" => 1
        ]);

        return true;
    }

    public function formulario_admision($cedula)
    {

        $paciente = Pacientes::where('cedula_paciente', $cedula)->get();
        $eps = EPS::where('estado_eps', 1)->get();
        $programas = Programas::where("estado_programas", 1)->get();

        $acompanante = DB::table('detalle_acompanante_paciente')
            ->select('detalle_acompanante_paciente.*', 'softworld_pacientes.*', 'softworld_acompanantes.*')
            ->join('softworld_pacientes', 'detalle_acompanante_paciente.fk_cedula_paciente', '=', 'softworld_pacientes.cedula_paciente')
            ->join('softworld_acompanantes', 'detalle_acompanante_paciente.fk_cedula_acompanante', '=', 'softworld_acompanantes.cedula_acompanante')
            ->where('softworld_pacientes.cedula_paciente', $cedula)
            ->get();
        $departamento = DB::table("softworld_departamentos")->where("codigo_dane", $paciente[0]->departamento_paciente)->get();

        $fecha_nacimiento = new DateTime($paciente[0]->fecha_nacimiento);
        $hoy = new DateTime();
        $edad = $hoy->diff($fecha_nacimiento);
        $edad = $edad->y;
        $cie10 = DB::table('softworld_cie10')->get();
        $usuarios = User::all();

        return view('template.formulario_admision', compact('paciente', 'edad', 'usuarios', 'acompanante', 'eps', 'programas', 'cie10', 'departamento'));
    }


    public function registro_formulario_admision(Request $request)
    {


        $rules = [
            'cedula' => 'required',
            'ubicacion_vivienda' => 'required',
            'cie10_registro' => 'required',
            'zona_riesgo_accesibilidad' => 'required',
            'nombre_evaluador_accesibilidad' => 'required',
            'cargo_evaluador_accesibilidad' => 'required',
            'elementos_basicos_higiene' => 'required',
            'elementos_basicos_alimentacion' => 'required',
            'linea_telefono_casa' => 'required',
            'agua_potable' => 'required',
            'luz_electrica' => 'required',
            'nevera' => 'required',
            'nombre_evaluador_pad_casa' => 'required',
            'cargo_evaluador_pad_casa' => 'required',
            'estabilidad_pacientes' => 'required',
            'barthel_menor' => 'required',
            'karnofsky_menor' => 'required',
            'plan_manejo_medico' => 'required',
            'aceptacion_paciente_familia' => 'required',
            'criterio_pad' => 'required',
            'programa' => 'required',
            'observaciones' => 'required',
            'nombre_evaluador_pad_clinico' => 'required',
            'cargo_evaluador_pad_casa' => 'required',
        ];

        $messages = [
            'cedula.required' => 'La cédula del paciente es importante',

            'ubicacion_vivienda.required' => 'El campo "Vivienda Ubicada en área de cobertura" es importante',
            'cie10_registro.required' => 'El campo "registro CIE10" es importante',
            'zona_riesgo_accesibilidad.required' => 'El campo "Zona con riesgo de accesibilidad" es importante',
            'nombre_evaluador_accesibilidad.required' => 'El campo "Nombre del evaluador del criterio" es importante',
            'cargo_evaluador_accesibilidad.required' => 'El campo "Cargo" es importante',

            'elementos_basicos_higiene.required' => 'Esta información es importante',
            'elementos_basicos_alimentacion.required' => 'Esta información es importante',
            'linea_telefono_casa.required' => 'Esta información es importante',
            'agua_potable.required' => 'Esta información es importante',
            'luz_electrica.required' => 'Esta información es importante',
            'nevera.required' => 'Esta información es importante',
            'nombre_evaluador_pad_casa.required' => 'Esta información es importante',
            'cargo_evaluador_pad_casa.required' => 'Esta información es importante',
            'estabilidad_pacientes.required' => 'Esta información es importante',
            'barthel_menor.required' => 'Esta información es importante',
            'karnofsky_menor.required' => 'Esta información es importante',
            'plan_manejo_medico.required' => 'Esta información es importante',
            'aceptacion_paciente_familia.required' => 'Esta información es importante',
            'criterio_pad.required' => 'Esta información es importante',
            'programa.required' => 'Esta información es importante',
            'observaciones.required' => 'Esta información es importante',
            'nombre_evaluador_pad_clinico.required' => 'Esta información es importante',
            'cargo_evaluador_pad_casa.required' => 'Esta información es importante',
        ];

        $this->validate($request, $rules, $messages);

        $numerico = FormularioAdmision::all()->count() + 1;
        $codigo_formulario_admision = 'CYT-FA' . $numerico;

        date_default_timezone_set('America/Bogota');

        $formulario = FormularioAdmision::create([
            'id_formulario_admision' => $codigo_formulario_admision,
            'id_entidad_remitente' => $request->eps,
            'registro_cie10' => $request->cie10_registro,
            'medico_entidad_remitente' => $request->nombre_medico,
            'especialidad_medico_entidad_remitente' => $request->especialidad_medico,
            'ubicacion_vivienda' => $request->ubicacion_vivienda,
            'zona_riesgo_accesibilidad' => $request->zona_riesgo_accesibilidad,
            'nombre_evaluador_accesibilidad' => $request->nombre_evaluador_accesibilidad,
            'cargo_evaluador_accesibilidad' => $request->cargo_evaluador_accesibilidad,
            'elementos_basicos_higiene' => $request->elementos_basicos_higiene,
            'elementos_basicos_alimentacion' => $request->elementos_basicos_alimentacion,
            'linea_telefono_casa' => $request->linea_telefono_casa,
            'agua_potable' => $request->agua_potable,
            'luz_electrica' => $request->luz_electrica,
            'nevera' => $request->nevera,
            'nombre_evaluador_pad_casa' => $request->nombre_evaluador_pad_casa,
            'cargo_evaluador_pad_casa' => $request->cargo_evaluador_pad_casa,
            'estabilidad_pacientes' => $request->estabilidad_pacientes,
            'barthel_menor' => $request->barthel_menor,
            'karnofsky_menor' => $request->karnofsky_menor,
            'plan_manejo_medico' => $request->plan_manejo_medico,
            'aceptacion_paciente_familia' => $request->aceptacion_paciente_familia,
            'criterio_pad' => $request->criterio_pad,
            'programa' => $request->programa,
            'observaciones' => $request->observaciones,
            'nombre_evaluador_pad_clinico' => $request->nombre_evaluador_pad_clinico,
            'cargo_evaluador_pad_clinico' => $request->cargo_evaluador_pad_clinico,
        ]);

        DB::table('detalle_formularios_pacientes')->insert([
            'id_formulario' => $codigo_formulario_admision,
            'id_persona' => $request->cedula,
            'tipo_formulario' => 'formulario_admision',
        ]);

        Pacientes::where('cedula_paciente', $request->cedula)->update([
            'estado_paciente' => 1,
        ]);


        $historia_clinica = HistoriasClinicas::create([
            'codigo_historia_clinica' => 'HC-' . $request->cedula,
            'fecha_historia_clinica' => date('Y-m-d H:i:s'),
            'cedula_paciente' => $request->cedula,
            'doctor_genero' => auth()->user()->id,
            'estado_historia_clinica' => 4,
        ]);

        DB::table('softworld_procesos_acciones_plataforma')->insert([
            'titulo_proceso' => 'Formulario de admision',
            'descripcion_proceso' => 'El usuario ' . auth()->user()->name . ' hizo una admision al paciente con identificacion ' . $request->cedula . " en la plataforma.",
            'codigo_usuario' =>  auth()->user()->id,
            'fecha_proceso' => date('Y-m-d H:i:s'),
        ]);


        return true;
    }

    public function agregar_notas($id)
    {
        date_default_timezone_set('America/Bogota');

        $paciente = Pacientes::where('cedula_paciente', $id)->get();
        return view('template.agregar-notas', compact('paciente'));
    }

    public function historia_clinica($id)
    {

        $procesos = [];

        $historia_clinica = HistoriasClinicas::where('cedula_paciente', $id)->get();
        $formulario_admision = [];

        /* ============================== FORMULARIO ADMISION ============================== */

        $sql_formulario_admision = DB::table('detalle_formularios_pacientes')
            ->where('detalle_formularios_pacientes.id_persona', $id)
            ->where('detalle_formularios_pacientes.tipo_formulario', "formulario_admision")
            ->get();

        $form_admision = DB::table("softworld_formulario_admisiones")->where("id_formulario_admision", $sql_formulario_admision[0]->id_formulario)->get();

        foreach ($form_admision as $value) {
            $cie10 = DB::table("softworld_cie10")->where("codigo", $value->registro_cie10)->get();
            $eps = DB::table("softworld_eps")->where("id_eps", $value->id_entidad_remitente)->get();
            $user = DB::table("users")->where("id", $value->nombre_evaluador_accesibilidad)->get();
            $programa = DB::table("softworld_programas")->where("id_programadas", $value->programa)->get();

            $formulario_admision[] = array(
                'titulo_proceso' => "FORMULARIO ADMISION",
                'id_proceso_evolucion' => $value->id_formulario_admision,
                'id_nota_paciente' => $value->id_formulario_admision,
                'registro_cie10' => $cie10[0]->descripcion,
                'fecha_formulario' => $value->created_at,
                'nombre_entidad' => $eps[0]->nombre_eps,
                'telefono_entidad' => $eps[0]->telefono_eps,
                'direccion_entidad' => $eps[0]->direccion_eps,
                'nombre_medico' => $value->medico_entidad_remitente,
                'especialidad' => $value->especialidad_medico_entidad_remitente,
                'ubicacion_vivienda' => $value->ubicacion_vivienda,
                'zona_riesgo_accesibilidad' => $value->zona_riesgo_accesibilidad,
                'nombre_evaluador_accesibilidad' => $user[0]->name,
                'cargo_evaluador_accesibilidad' => $value->cargo_evaluador_accesibilidad,

                'elementos_basicos_higiene' => $value->elementos_basicos_higiene,
                'elementos_basicos_alimentacion' => $value->elementos_basicos_alimentacion,
                'linea_telefono_casa' => $value->linea_telefono_casa,
                'agua_potable' => $value->agua_potable,
                'luz_electrica' => $value->luz_electrica,
                'nevera' => $value->nevera,
                'nombre_evaluador_pad_casa' => $user[0]->name,
                'cargo_evaluador_pad_casa' => $value->cargo_evaluador_pad_casa,

                'estabilidad_pacientes' => $value->estabilidad_pacientes,
                'barthel_menor' => $value->barthel_menor,
                'karnofsky_menor' => $value->karnofsky_menor,
                'plan_manejo_medico' => $value->plan_manejo_medico,
                'aceptacion_paciente_familia' => $value->aceptacion_paciente_familia,
                'criterio_pad' => $value->criterio_pad,
                'programa' => $programa[0]->titulo_programas,
                'observaciones' => $value->observaciones,
                'nombre_evaluador_pad_clinico' => $user[0]->name,
                'cargo_evaluador_pad_clinico' => $value->cargo_evaluador_pad_clinico,


            );
        }

        /* ============================== FIN FORMULARIO ADMISION ============================== */





        /* ============================== FORMULARIO NOTAS RAPIDAS ============================== */
        $notas = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_notas_pacientes.*', 'softworld_historias_clinicas.*')
            ->join('softworld_notas_pacientes', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_notas_pacientes.id_nota_paciente')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($notas as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'id_nota_paciente' => $value->id_nota_paciente,
                'titulo_nota' => $value->titulo_nota,
                'descripcion_nota' => $value->descripcion_nota,
                'fecha_nota' => $value->fecha_nota,
                'usuario_responsable' => $value->usuario_responsable,
                'estado_nota' => $value->estado_nota
            );
        }
        /* ============================== FIN FORMULARIO NOTAS RAPIDAS ============================== */


        /* ============================== FORMULARIO FONOAUDILOGIA ============================== */
        $formularios = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_fonoaudiologias.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
            ->join('softworld_formulario_fonoaudiologias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_fonoaudiologias.id_form_fono')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->join('softworld_cie10', 'softworld_formulario_fonoaudiologias.cie10_registro', '=', 'softworld_cie10.codigo')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($formularios as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'id_form_fono' => $value->id_form_fono,
                'cie10_codigo' => $value->codigo,
                'cie10_name' => $value->descripcion,
                'cie10_registro' => $value->cie10_registro,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'ta_diagnostico' => $value->ta_diagnostico,
                'fc_diagnostico' => $value->fc_diagnostico,
                'fr_diagnostico' => $value->fr_diagnostico,
                'to_diagnostico' => $value->to_diagnostico,
                'diagnostico_registro' => $value->diagnostico_registro,
                'observacion_registro' => $value->observacion_registro,
                'habla_registro' => $value->habla_registro,
                'voz_registro' => $value->voz_registro,
                'articulacion_fonemas_registro' => $value->articulacion_fonemas_registro,
                'deglucion_registro' => $value->deglucion_registro,
                'respiracion_registro' => $value->respiracion_registro,
                'organos_fonatorios_registro' => $value->organos_fonatorios_registro,
                'audicion_registro' => $value->audicion_registro,
                'procesos_cognitivos_registro' => $value->procesos_cognitivos_registro,
                'analisis_registro' => $value->analisis_registro,
                'objetivos_tratamiento_registro' => $value->objetivos_tratamiento_registro,
                'plan_manejo_registro' => $value->plan_manejo_registro,
                'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                'interconsultas_registro' => $value->interconsultas_registro,
            );
        }
        /* ============================== FIN FORMULARIO FONOAUDILOGIA ============================== */


        /* ============================== FORMULARIO MEDICO GENERAL ============================== */

        $formulario_MG = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_medico_generals.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
            ->join('softworld_formulario_medico_generals', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_medico_generals.id_form_medico')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->join('softworld_cie10', 'softworld_formulario_medico_generals.cie10_registro', '=', 'softworld_cie10.codigo')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($formulario_MG as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'id_form_medico' => $value->id_form_medico,
                'cie10_codigo' => $value->codigo,
                'cie10_name' => $value->descripcion,
                'cie10_registro' => $value->cie10_registro,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'fc_diagnostico' => $value->fc_diagnostico,
                'fr_diagnostico' => $value->fr_diagnostico,
                'sato2' => $value->sato2,
                'peso' => $value->peso,
                'talla' => $value->talla,
                'ta_diagnostico' => $value->ta_diagnostico,
                'to_diagnostico' => $value->to_diagnostico,
                'imc' => $value->imc,
                'diagnostico_registro' => $value->diagnostico_registro,
                'uso_previo' => $value->uso_previo,
                'alergias' => $value->alergias,
                'analisis_registro' => $value->analisis_registro,
                'plan_manejo_registro' => $value->plan_manejo_registro,
                'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                'interconsultas_registro' => $value->interconsultas_registro,
            );
        }
        /* ============================== FIN FORMULARIO MEDICO GENERAL ============================== */


        /* ============================== FORMULARIO PSICOLOGIA ============================== */

        $formulario_PSI = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_psicologias.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
            ->join('softworld_formulario_psicologias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_psicologias.id_form_psi')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->join('softworld_cie10', 'softworld_formulario_psicologias.cie10_registro', '=', 'softworld_cie10.codigo')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($formulario_PSI as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'id_psicologia' => $value->id_form_psi,
                'cie10_codigo' => $value->codigo,
                'cie10_name' => $value->descripcion,
                'cie10_registro' => $value->cie10_registro,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'motivo_consulta' => $value->motivo_consulta,
                'condiciones_personales' => $value->condiciones_personales,
                'condiciones_familiares' => $value->condiciones_familiares,
                'analisis_registro' => $value->analisis_registro,
                'plan_manejo_registro' => $value->plan_manejo_registro,
                'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                'interconsultas_registro' => $value->interconsultas_registro,
            );
        }
        /* ============================== FIN FORMULARIO PSICOLOGIA ============================== */


        /* ============================== FORMULARIO NUTRICION ============================== */

        $formulario_nutricion = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_nutricionistas.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
            ->join('softworld_formulario_nutricionistas', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_nutricionistas.id_form_nutri')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->join('softworld_cie10', 'softworld_formulario_nutricionistas.cie10_registro', '=', 'softworld_cie10.codigo')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($formulario_nutricion as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'cie10_codigo' => $value->codigo,
                'cie10_name' => $value->descripcion,
                'id_form_nutri' => $value->id_form_nutri,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'fc_diagnostico' => $value->fc_diagnostico,
                'fr_diagnostico' => $value->fr_diagnostico,
                'sato2' => $value->sato2,
                'peso' => $value->peso,
                'talla' => $value->talla,
                'imc' => $value->imc,
                'to_diagnostico' => $value->to_diagnostico,
                'ta_diagnostico' => $value->ta_diagnostico,
                'diagnostico_registro' => $value->diagnostico_registro,
                'estado_nutricion_actual' => $value->estado_nutricion_actual,
                'via_alimentacion' => $value->via_alimentacion,
                'disfagia_valor' => $value->disfagia_valor,
                'disfagia_porque' => $value->disfagia_porque,
                'valoracion_nutricional' => $value->valoracion_nutricional,
                'uso_previo' => $value->uso_previo,
                'alergias' => $value->alergias,
                'analisis_registro' => $value->analisis_registro,
                'plan_manejo_registro' => $value->plan_manejo_registro,
                'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                'interconsultas_registro' => $value->interconsultas_registro,

            );
        }
        /* ============================== FIN FORMULARIO NUTRICION ============================== */


        /* ============================== FORMULARIO FISIOTERAPIA ============================== */

        $formulario_nutricion = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_fisioterapias.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
            ->join('softworld_formulario_fisioterapias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_fisioterapias.id_form_fisio')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->join('softworld_cie10', 'softworld_formulario_fisioterapias.cie10_registro', '=', 'softworld_cie10.codigo')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($formulario_nutricion as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'id_form_fisio' => $value->id_form_fisio,
                'cie10_codigo' => $value->codigo,
                'cie10_name' => $value->descripcion,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'fc_diagnostico' => $value->fc_diagnostico,
                'fr_diagnostico' => $value->fr_diagnostico,
                'sato2' => $value->sato2,
                'peso' => $value->peso,
                'talla' => $value->talla,
                'imc' => $value->imc,
                'to_diagnostico' => $value->to_diagnostico,
                'ta_diagnostico' => $value->ta_diagnostico,
                'diagnostico_registro' => $value->diagnostico_registro,
                'observacion' => $value->observacion,
                'dolor' => $value->dolor,
                'piel' => $value->piel,
                'inspeccion' => $value->inspeccion,
                'palpacion' => $value->palpacion,
                'auscultacion' => $value->auscultacion,
                'movilidad_articular' => $value->movilidad_articular,
                'fuerza_muscular' => $value->fuerza_muscular,
                'tono_muscular' => $value->tono_muscular,
                'sensibilidad' => $value->sensibilidad,
                'marcha' => $value->marcha,
                'analisis_registro' => $value->analisis_registro,
                'objetivos_tratamiento' => $value->objetivos_tratamiento,
                'plan_manejo_registro' => $value->plan_manejo_registro,
                'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                'interconsultas_registro' => $value->interconsultas_registro,

            );
        }
        /* ============================== FIN FORMULARIO FISIOTERAPIA ============================== */


        /* ============================== FORMULARIO ENTEROSTOMAL ============================== */

        $formulario_enterostomal = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_enterostomals.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
            ->join('softworld_formulario_enterostomals', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_enterostomals.id_form_enterostomal')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->join('softworld_cie10', 'softworld_formulario_enterostomals.cie10_registro', '=', 'softworld_cie10.codigo')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($formulario_enterostomal as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'id_form_enterostomal' => $value->id_form_enterostomal,
                'cie10_codigo' => $value->codigo,
                'cie10_name' => $value->descripcion,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'area_afectada' => $value->area_afectada,
                'clasificacion_lesion' => $value->clasificacion_lesion,
                'clasificacion_profundidad' => $value->clasificacion_profundidad,
                'grosor_parcial' => $value->grosor_parcial,
                'grosor_total' => $value->grosor_total,
                'lesion_largo' => $value->lesion_largo,
                'lesion_ancho' => $value->lesion_ancho,
                'lesion_profundidad' => $value->lesion_profundidad,
                'valoracion_inicial' => $value->valoracion_inicial,
                'nota_procedimiento' => $value->nota_procedimiento,
                'insumos_utilizados' => $value->insumos_utilizados,
                'plan_manejo' => $value->plan_manejo,
                'recomendaciones' => $value->recomendaciones,

            );
        }
        /* ============================== FIN FORMULARIO ENTEROSTOMAL ============================== */


        /* ============================== FORMULARIO TERAPIA OCUPACIONAL ============================== */

        $formulario_enterostomal = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_terapia_ocupacionals.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
            ->join('softworld_formulario_terapia_ocupacionals', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_terapia_ocupacionals.if_form_to')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->join('softworld_cie10', 'softworld_formulario_terapia_ocupacionals.cie10_registro', '=', 'softworld_cie10.codigo')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($formulario_enterostomal as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'if_form_to' => $value->if_form_to,
                'cie10_codigo' => $value->codigo,
                'cie10_name' => $value->descripcion,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'fc_diagnostico' => $value->fc_diagnostico,
                'fr_diagnostico' => $value->fr_diagnostico,
                'sato2' => $value->sato2,
                'peso' => $value->peso,
                'talla' => $value->talla,
                'imc' => $value->imc,
                'to_diagnostico' => $value->to_diagnostico,
                'ta_diagnostico' => $value->ta_diagnostico,
                'diagnostico' => $value->diagnostico,
                'observacion' => $value->observacion,
                'condiciones_motoras' => $value->condiciones_motoras,
                'componentes_sensiorales' => $value->componentes_sensiorales,
                'componentes_cognitivos' => $value->componentes_cognitivos,
                'funcionalidad' => $value->funcionalidad,
                'analisis' => $value->analisis,
                'objetivos_tratamiento' => $value->objetivos_tratamiento,
                'plan_manejo_registro' => $value->plan_manejo_registro,
                'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                'interconsultas_registro' => $value->interconsultas_registro,
            );
        }
        /* ============================== FIN FORMULARIO TERAPIA OCUPACIONAL ============================== */


        /* ============================== FORMULARIO TERAPIA RESPIRATORIA ============================== */

        $formulario_terapia_respiratoria = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_terapia_respiratorias.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
            ->join('softworld_formulario_terapia_respiratorias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_terapia_respiratorias.if_form_tr')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->join('softworld_cie10', 'softworld_formulario_terapia_respiratorias.cie10_registro', '=', 'softworld_cie10.codigo')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($formulario_terapia_respiratoria as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,

                'if_form_tr' => $value->if_form_tr,
                'cie10_codigo' => $value->codigo,
                'cie10_name' => $value->descripcion,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'fc_diagnostico' => $value->fc_diagnostico,
                'fr_diagnostico' => $value->fr_diagnostico,
                'sato2' => $value->sato2,
                'peso' => $value->peso,
                'talla' => $value->talla,
                'imc' => $value->imc,
                'to_diagnostico' => $value->to_diagnostico,
                'ta_diagnostico' => $value->ta_diagnostico,
                'diagnostico' => $value->diagnostico,
                'inspeccion' => $value->inspeccion,
                'tipo_respiratorio' => $value->tipo_respiratorio,
                'patron_respiratorio' => $value->patron_respiratorio,
                'signos_ira' => $value->signos_ira,
                'cianosis' => $value->cianosis,
                'disnea' => $value->disnea,
                'tos' => $value->tos,
                'expectoriaciones' => $value->expectoriaciones,
                'edema' => $value->edema,
                'asimetria_deformidades' => $value->asimetria_deformidades,
                'palpacion' => $value->palpacion,
                'auscultacion' => $value->auscultacion,
                'analisis' => $value->analisis,
                'objetivos_tratamiento' => $value->objetivos_tratamiento,
                'plan_manejo_registro' => $value->plan_manejo_registro,
                'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                'interconsultas_registro' => $value->interconsultas_registro,

            );
        }
        /* ============================== FIN FORMULARIO TERAPIA RESPIRATORIA ============================== */


        /* ============================== FORMULARIO EVOLUCIÓN TERAPEUTA ============================== */

        $evolucion_terapeuta = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_evolucion_terapeutas.*', 'softworld_historias_clinicas.*')
            ->join('softworld_evolucion_terapeutas', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_evolucion_terapeutas.id_evo_tera')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($evolucion_terapeuta as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'id_evo_tera' => $value->id_evo_tera,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'ta_diagnostico' => $value->ta_diagnostico,
                'fc_diagnostico' => $value->fc_diagnostico,
                'fr_diagnostico' => $value->fr_diagnostico,
                'sato2' => $value->sato2,
                'to_diagnostico' => $value->to_diagnostico,
                'evolucion' => $value->evolucion,
                'recomendaciones' => $value->recomendaciones,
            );
        }
        /* ============================== FIN FORMULARIO EVOLUCIÓN TERAPEUTA ============================== */


        /* ============================== FORMULARIO EVOLUCIÓN ENFERMERIA ============================== */

        $evolucion_terapeuta = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_evolucion_enfermerias.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
            ->join('softworld_evolucion_enfermerias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_evolucion_enfermerias.id_evo_enfer')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->join('softworld_cie10', 'softworld_evolucion_enfermerias.cie10_registro', '=', 'softworld_cie10.codigo')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($evolucion_terapeuta as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,

                'id_evo_enfer' => $value->id_evo_enfer,
                'cie10_codigo' => $value->codigo,
                'cie10_name' => $value->descripcion,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'ta_diagnostico' => $value->ta_diagnostico,
                'fc_diagnostico' => $value->fc_diagnostico,
                'fr_diagnostico' => $value->fr_diagnostico,
                'to_diagnostico' => $value->to_diagnostico,
                'sato2' => $value->sato2,
                'educacion_paciente' => $value->educacion_paciente,

            );
        }
        /* ============================== FIN FORMULARIO EVOLUCIÓN ENFERMERIA ============================== */


        /* ============================== DOCUMENTACION PACIENTES ============================== */

        $documentos = DB::table("softworld_documentos_pacientes")
            ->where("cedula_paciente_dopa", $id)
            ->get();


        foreach ($documentos as $value) {
            $procesos[] = array(
                'titulo_proceso' => "documentos_paciente",
                'id_proceso_evolucion' => "DOC".$value->id,
                'id_documento' => $value->id,
                'fecha' => $value->fecha,
                'documento' => $value->documento_paciente_dopa
            );
        }

        /* ============================== FIN DOCUMENTACION PACIENTES ============================== */

        foreach ($procesos as $key => $row) {
            $aux[$key] = $row['id_proceso_evolucion'];
        }

        if (count($historia_clinica) >= 1) {
            $paciente = Pacientes::where('cedula_paciente', $historia_clinica[0]->cedula_paciente)->get();
            return view('template.historia_clinica', compact('historia_clinica', 'paciente', 'procesos', 'formulario_admision'));
        } else {
            return "Sin Historia clinica";
        }
    }

    public function notas_store(Request $request)
    {

        $rules = [
            'titulo_nota' => 'required',
            'descripcion_nota' => 'required',
            'fecha_nota' => 'required',
            'usuario_responsable' => 'required',
        ];

        $messages = [
            'titulo_nota.required' => 'El titulo de la nota es importante',

            'descripcion_nota.required' => 'La descripción de la nota es importante',
            'fecha_nota.required' => 'La fecha de la nota es importante',
            'usuario_responsable.required' => 'El usuario de la nota es importante',
        ];

        $this->validate($request, $rules, $messages);

        $historia_clinica = HistoriasClinicas::where('cedula_paciente', $request->cedula_paciente)->get();

        $totalNR = NotasPacientes::count();
        $codigoNR = $totalNR + 1;

        NotasPacientes::create([
            'id_nota_paciente' => 'NR-' . $codigoNR,
            'titulo_nota' => $request->titulo_nota,
            'descripcion_nota' => $request->descripcion_nota,
            'fecha_nota' => $request->fecha_nota,
            'usuario_responsable' => $request->usuario_responsable,
            'estado_nota' => '1',
        ]);

        DB::table('softworld_procesos_evolucion')->insert([
            'titulo_proceso' => 'Creacion nota',
            'fk_historia_clinica' => $historia_clinica[0]->codigo_historia_clinica,
            'fk_proceso_evolucion_nota' => 'NR-' . $codigoNR,
        ]);

        return true;
    }

    public function generar_evaluacion($cedula)
    {
        return view('template.formularios_evaluacion', compact('cedula'));
    }

    public function generar_evolucion($cedula)
    {
        return view('template.formularios_evolucion', compact('cedula'));
    }

    public function abrir_formulario_evaluacion($tipo, $cedula)
    {

        $paciente = Pacientes::where('cedula_paciente', $cedula)->get();

        //formulario de admision para detectar cie10
        $formulario_admision = DB::table("detalle_formularios_pacientes")
            ->select("detalle_formularios_pacientes.*", "softworld_formulario_admisiones.*")
            ->join("softworld_formulario_admisiones", "detalle_formularios_pacientes.id_formulario", "=", "softworld_formulario_admisiones.id_formulario_admision")
            ->where([
                ["detalle_formularios_pacientes.id_persona", $cedula],
                ["detalle_formularios_pacientes.tipo_formulario", "formulario_admision"],
            ])->get();

        $cie10 = DB::table('softworld_cie10')->where("codigo", $formulario_admision[0]->registro_cie10)->get();

        $acompanante = DB::table('detalle_acompanante_paciente')
            ->select('detalle_acompanante_paciente.*', 'softworld_pacientes.*', 'softworld_acompanantes.*')
            ->join('softworld_pacientes', 'detalle_acompanante_paciente.fk_cedula_paciente', '=', 'softworld_pacientes.cedula_paciente')
            ->join('softworld_acompanantes', 'detalle_acompanante_paciente.fk_cedula_acompanante', '=', 'softworld_acompanantes.cedula_acompanante')
            ->where('softworld_pacientes.cedula_paciente', $cedula)
            ->get();

        $fecha_nacimiento = new DateTime($paciente[0]->fecha_nacimiento);
        $hoy = new DateTime();
        $edad = $hoy->diff($fecha_nacimiento);
        $edad = $edad->y;


        $historia_clinica = DB::table('softworld_historias_clinicas')
            ->select('softworld_historias_clinicas.codigo_historia_clinica', 'softworld_historias_clinicas.fecha_historia_clinica', 'softworld_historias_clinicas.estado_historia_clinica', 'softworld_historias_clinicas.cedula_paciente', 'softworld_pacientes.cedula_paciente', 'softworld_pacientes.primer_nombre', 'softworld_pacientes.primer_apellido')
            ->join('softworld_pacientes', 'softworld_historias_clinicas.cedula_paciente', '=', 'softworld_pacientes.cedula_paciente')
            ->where('softworld_pacientes.cedula_paciente', $cedula)
            ->get();

        if ($tipo == 'fonoaudiologia') {
            return view('template.formulario_fonoaudiologia', compact('paciente', 'acompanante', 'edad', 'historia_clinica', 'cie10'));
        } else if ($tipo == 'medico-general') {
            return view('template.formulario_medico_general', compact('paciente', 'acompanante', 'edad', 'historia_clinica', 'cie10'));
        } else if ($tipo == 'nutricionista') {
            return view('template.formulario_nutricionista', compact('paciente', 'acompanante', 'edad', 'historia_clinica', 'cie10'));
        } else if ($tipo == 'psicologia') {
            return view('template.formulario_psicologia', compact('paciente', 'acompanante', 'edad', 'historia_clinica', 'cie10'));
        } else if ($tipo == 'terapia-enterostomal') {
            return view('template.formulario_terapia_enterostomal', compact('paciente', 'acompanante', 'edad', 'historia_clinica', 'cie10'));
        } else if ($tipo == 'fisioterapia') {
            return view('template.formulario_fisioterapia', compact('paciente', 'acompanante', 'edad', 'historia_clinica', 'cie10'));
        } else if ($tipo == 'terapia-ocupacional') {
            return view('template.formulario_terapia_ocupacional', compact('paciente', 'acompanante', 'edad', 'historia_clinica', 'cie10'));
        } else if ($tipo == 'terapeutas') {
            return view('template.formulario_terapeutas');
        } else if ($tipo == 'terapia-respiratoria') {
            return view('template.formulario_terapia_respiratoria', compact('paciente', 'acompanante', 'edad', 'historia_clinica', 'cie10'));
        } else if ($tipo == 'evolucion-terapeutas') {
            return view('template.formulario_evolucion_terapeuta', compact('paciente', 'acompanante', 'edad', 'historia_clinica', 'cie10'));
        } else if ($tipo == 'evolucion-enfermeria') {
            return view('template.formulario_evolucion_enfermeria', compact('paciente', 'acompanante', 'edad', 'historia_clinica', 'cie10'));
        } else if ($tipo == 'evolucion-psicologia') {
            return view('template.formulario_evolucion_psicologia', compact('paciente', 'acompanante', 'edad', 'historia_clinica', 'cie10'));
        } else {
        }
    }

    public function busqueda_eps($id)
    {
        $eps = EPS::where('id_eps', $id)->get();
        return $eps;
    }

    public function registro_formulario_evaluacion(Request $request, $tipo)
    {

        if ($tipo == 'fonoaudiologia') {
        } else if ($tipo == 'medico-general') {
            return view('template.formulario_medico_general');
        } else if ($tipo == 'nutricionista') {
            return view('template.formulario_nutricionista');
        } else if ($tipo == 'psicologia') {
            return view('template.formulario_psicologia');
        } else if ($tipo == 'terapia-enterostomal') {
            return view('template.formulario_terapia_enterostomal');
        } else if ($tipo == 'terapia-fisica') {
            return view('template.formulario_terapia_fisica');
        } else if ($tipo == 'terapia-ocupacional') {
            return view('template.formulario_terapia_ocupacional');
        } else if ($tipo == 'terapeutas') {
            return view('template.formulario_terapeutas');
        } else {
        }
    }

    public function formulario_envio_mail_hc($cedula)
    {
        $historia_clinica = HistoriasClinicas::where('cedula_paciente', $cedula)->get();
        return view('template.formulario_envio_historia_clinica', compact('cedula', 'historia_clinica'));
    }

    public function envio_mail_hc(Request $request)
    {

        $procesos = [];

        $id = $request->cedula;

        date_default_timezone_set('America/Bogota');

        $MAIL_HOST = config('app.MAIL_HOST');
        $MAIL_PORT = config('app.MAIL_PORT');
        $MAIL_USERNAME = config('app.MAIL_USERNAME');
        $MAIL_PASSWORD = config('app.MAIL_PASSWORD');

        $historia_clinica = HistoriasClinicas::where('cedula_paciente', $id)->get();

        /* ============================== FORMULARIO NOTAS RAPIDAS ============================== */
        $notas = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_notas_pacientes.*', 'softworld_historias_clinicas.*')
            ->join('softworld_notas_pacientes', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_notas_pacientes.id_nota_paciente')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();
        foreach ($notas as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'titulo_nota' => $value->titulo_nota,
                'descripcion_nota' => $value->descripcion_nota,
                'fecha_nota' => $value->fecha_nota,
                'usuario_responsable' => $value->usuario_responsable,
                'estado_nota' => $value->estado_nota
            );
        }
        /* ============================== FIN FORMULARIO NOTAS RAPIDAS ============================== */


        /* ============================== FORMULARIO FONOAUDILOGIA ============================== */
        $formularios = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_fonoaudiologias.*', 'softworld_historias_clinicas.*')
            ->join('softworld_formulario_fonoaudiologias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_fonoaudiologias.id_form_fono')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();
        foreach ($formularios as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'cie10_registro' => $value->cie10_registro,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'ta_diagnostico' => $value->ta_diagnostico,
                'fc_diagnostico' => $value->fc_diagnostico,
                'fr_diagnostico' => $value->fr_diagnostico,
                'to_diagnostico' => $value->to_diagnostico,
                'diagnostico_registro' => $value->diagnostico_registro,
                'observacion_registro' => $value->observacion_registro,
                'habla_registro' => $value->habla_registro,
                'voz_registro' => $value->voz_registro,
                'articulacion_fonemas_registro' => $value->articulacion_fonemas_registro,
                'deglucion_registro' => $value->deglucion_registro,
                'respiracion_registro' => $value->respiracion_registro,
                'organos_fonatorios_registro' => $value->organos_fonatorios_registro,
                'audicion_registro' => $value->audicion_registro,
                'procesos_cognitivos_registro' => $value->procesos_cognitivos_registro,
                'analisis_registro' => $value->analisis_registro,
                'objetivos_tratamiento_registro' => $value->objetivos_tratamiento_registro,
                'plan_manejo_registro' => $value->plan_manejo_registro,
                'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                'interconsultas_registro' => $value->interconsultas_registro,
            );
        }
        /* ============================== FIN FORMULARIO FONOAUDILOGIA ============================== */


        /* ============================== FORMULARIO MEDICO GENERAL ============================== */

        $formulario_MG = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_medico_generals.*', 'softworld_historias_clinicas.*')
            ->join('softworld_formulario_medico_generals', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_medico_generals.id_form_medico')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($formulario_MG as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'cie10_registro' => $value->cie10_registro,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'fc_diagnostico' => $value->fc_diagnostico,
                'fr_diagnostico' => $value->fr_diagnostico,
                'sato2' => $value->sato2,
                'peso' => $value->peso,
                'talla' => $value->talla,
                'ta_diagnostico' => $value->ta_diagnostico,
                'to_diagnostico' => $value->to_diagnostico,
                'imc' => $value->imc,
                'diagnostico_registro' => $value->diagnostico_registro,
                'uso_previo' => $value->uso_previo,
                'alergias' => $value->alergias,
                'analisis_registro' => $value->analisis_registro,
                'plan_manejo_registro' => $value->plan_manejo_registro,
                'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                'interconsultas_registro' => $value->interconsultas_registro,
            );
        }
        /* ============================== FIN FORMULARIO MEDICO GENERAL ============================== */


        /* ============================== FORMULARIO PSICOLOGIA ============================== */

        $formulario_PSI = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_psicologias.*', 'softworld_historias_clinicas.*')
            ->join('softworld_formulario_psicologias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_psicologias.id_form_psi')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($formulario_PSI as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'cie10_registro' => $value->cie10_registro,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'motivo_consulta' => $value->motivo_consulta,
                'condiciones_personales' => $value->condiciones_personales,
                'condiciones_familiares' => $value->condiciones_familiares,
                'analisis_registro' => $value->analisis_registro,
                'plan_manejo_registro' => $value->plan_manejo_registro,
                'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                'interconsultas_registro' => $value->interconsultas_registro,
            );
        }
        /* ============================== FIN FORMULARIO PSICOLOGIA ============================== */


        /* ============================== FORMULARIO NUTRICION ============================== */

        $formulario_nutricion = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_nutricionistas.*', 'softworld_historias_clinicas.*')
            ->join('softworld_formulario_nutricionistas', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_nutricionistas.id_form_nutri')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($formulario_nutricion as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'cie10_registro' => $value->cie10_registro,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'fc_diagnostico' => $value->fc_diagnostico,
                'fr_diagnostico' => $value->fr_diagnostico,
                'sato2' => $value->sato2,
                'peso' => $value->peso,
                'talla' => $value->talla,
                'imc' => $value->imc,
                'to_diagnostico' => $value->to_diagnostico,
                'ta_diagnostico' => $value->ta_diagnostico,
                'diagnostico_registro' => $value->diagnostico_registro,
                'estado_nutricion_actual' => $value->estado_nutricion_actual,
                'via_alimentacion' => $value->via_alimentacion,
                'disfagia_valor' => $value->disfagia_valor,
                'disfagia_porque' => $value->disfagia_porque,
                'valoracion_nutricional' => $value->valoracion_nutricional,
                'uso_previo' => $value->uso_previo,
                'alergias' => $value->alergias,
                'analisis_registro' => $value->analisis_registro,
                'plan_manejo_registro' => $value->plan_manejo_registro,
                'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                'interconsultas_registro' => $value->interconsultas_registro,
            );
        }
        /* ============================== FIN FORMULARIO NUTRICION ============================== */


        /* ============================== FORMULARIO FISIOTERAPIA ============================== */

        $formulario_nutricion = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_fisioterapias.*', 'softworld_historias_clinicas.*')
            ->join('softworld_formulario_fisioterapias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_fisioterapias.id_form_fisio')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($formulario_nutricion as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'cie10_registro' => $value->cie10_registro,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'fc_diagnostico' => $value->fc_diagnostico,
                'fr_diagnostico' => $value->fr_diagnostico,
                'sato2' => $value->sato2,
                'peso' => $value->peso,
                'talla' => $value->talla,
                'imc' => $value->imc,
                'to_diagnostico' => $value->to_diagnostico,
                'ta_diagnostico' => $value->ta_diagnostico,
                'diagnostico_registro' => $value->diagnostico_registro,
                'observacion' => $value->observacion,
                'dolor' => $value->dolor,
                'piel' => $value->piel,
                'inspeccion' => $value->inspeccion,
                'palpacion' => $value->palpacion,
                'auscultacion' => $value->auscultacion,
                'movilidad_articular' => $value->movilidad_articular,
                'fuerza_muscular' => $value->fuerza_muscular,
                'tono_muscular' => $value->tono_muscular,
                'sensibilidad' => $value->sensibilidad,
                'marcha' => $value->marcha,
                'analisis_registro' => $value->analisis_registro,
                'objetivos_tratamiento' => $value->objetivos_tratamiento,
                'plan_manejo_registro' => $value->plan_manejo_registro,
                'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                'interconsultas_registro' => $value->interconsultas_registro,
            );
        }
        /* ============================== FIN FORMULARIO FISIOTERAPIA ============================== */


        /* ============================== FORMULARIO ENTEROSTOMAL ============================== */

        $formulario_enterostomal = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_enterostomals.*', 'softworld_historias_clinicas.*')
            ->join('softworld_formulario_enterostomals', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_enterostomals.id_form_enterostomal')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($formulario_enterostomal as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'cie10_registro' => $value->cie10_registro,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'area_afectada' => $value->area_afectada,
                'clasificacion_lesion' => $value->clasificacion_lesion,
                'clasificacion_profundidad' => $value->clasificacion_profundidad,
                'grosor_parcial' => $value->grosor_parcial,
                'grosor_total' => $value->grosor_total,
                'lesion_largo' => $value->lesion_largo,
                'lesion_ancho' => $value->lesion_ancho,
                'lesion_profundidad' => $value->lesion_profundidad,
                'valoracion_inicial' => $value->valoracion_inicial,
                'nota_procedimiento' => $value->nota_procedimiento,
                'insumos_utilizados' => $value->insumos_utilizados,
                'plan_manejo' => $value->plan_manejo,
                'recomendaciones' => $value->recomendaciones,
            );
        }
        /* ============================== FIN FORMULARIO ENTEROSTOMAL ============================== */


        /* ============================== FORMULARIO TERAPIA OCUPACIONAL ============================== */

        $formulario_enterostomal = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_terapia_ocupacionals.*', 'softworld_historias_clinicas.*')
            ->join('softworld_formulario_terapia_ocupacionals', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_terapia_ocupacionals.if_form_to')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($formulario_enterostomal as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'cie10_registro' => $value->cie10_registro,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'fc_diagnostico' => $value->fc_diagnostico,
                'fr_diagnostico' => $value->fr_diagnostico,
                'sato2' => $value->sato2,
                'peso' => $value->peso,
                'talla' => $value->talla,
                'imc' => $value->imc,
                'to_diagnostico' => $value->to_diagnostico,
                'ta_diagnostico' => $value->ta_diagnostico,
                'diagnostico' => $value->diagnostico,
                'observacion' => $value->observacion,
                'condiciones_motoras' => $value->condiciones_motoras,
                'componentes_sensiorales' => $value->componentes_sensiorales,
                'componentes_cognitivos' => $value->componentes_cognitivos,
                'funcionalidad' => $value->funcionalidad,
                'analisis' => $value->analisis,
                'objetivos_tratamiento' => $value->objetivos_tratamiento,
                'plan_manejo_registro' => $value->plan_manejo_registro,
                'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                'interconsultas_registro' => $value->interconsultas_registro,
            );
        }
        /* ============================== FIN FORMULARIO TERAPIA OCUPACIONAL ============================== */


        /* ============================== FORMULARIO TERAPIA RESPIRATORIA ============================== */

        $formulario_terapia_respiratoria = DB::table('softworld_procesos_evolucion')
            ->select('softworld_procesos_evolucion.*', 'softworld_formulario_terapia_respiratorias.*', 'softworld_historias_clinicas.*')
            ->join('softworld_formulario_terapia_respiratorias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_terapia_respiratorias.if_form_tr')
            ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
            ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
            ->get();

        foreach ($formulario_terapia_respiratoria as $value) {
            $procesos[] = array(
                'titulo_proceso' => $value->titulo_proceso,
                'id_proceso_evolucion' => $value->id_proceso_evolucion,
                'cie10_registro' => $value->cie10_registro,
                'fecha_registro' => $value->fecha_registro,
                'hora_registro' => $value->hora_registro,
                'fc_diagnostico' => $value->fc_diagnostico,
                'fr_diagnostico' => $value->fr_diagnostico,
                'sato2' => $value->sato2,
                'peso' => $value->peso,
                'talla' => $value->talla,
                'imc' => $value->imc,
                'to_diagnostico' => $value->to_diagnostico,
                'ta_diagnostico' => $value->ta_diagnostico,
                'diagnostico' => $value->diagnostico,
                'inspeccion' => $value->inspeccion,
                'tipo_respiratorio' => $value->tipo_respiratorio,
                'patron_respiratorio' => $value->patron_respiratorio,
                'signos_ira' => $value->signos_ira,
                'cianosis' => $value->cianosis,
                'disnea' => $value->disnea,
                'tos' => $value->tos,
                'expectoriaciones' => $value->expectoriaciones,
                'edema' => $value->edema,
                'asimetria_deformidades' => $value->asimetria_deformidades,
                'palpacion' => $value->palpacion,
                'auscultacion' => $value->auscultacion,
                'analisis' => $value->analisis,
                'objetivos_tratamiento' => $value->objetivos_tratamiento,
                'plan_manejo_registro' => $value->plan_manejo_registro,
                'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                'interconsultas_registro' => $value->interconsultas_registro,
            );
        }
        /* ============================== FIN FORMULARIO TERAPIA RESPIRATORIA ============================== */

        foreach ($procesos as $key => $row) {
            $aux[$key] = $row['id_proceso_evolucion'];
        }

        //array_multisort($aux, SORT_ASC, $procesos);


        if (count($historia_clinica) >= 1) {
            $paciente = Pacientes::where('cedula_paciente', $id)->get();

            $pdf = PDF::loadView('template.historia_clinica_pdf', compact('historia_clinica', 'paciente', 'procesos'))->setPaper('a4', 'portrait');

            $name = $historia_clinica[0]->codigo_historia_clinica . "-" . date('d-m-y') . "_" . date('H:i:s') . ".pdf";
            Storage::disk('s3')->put($name, $pdf->output());

            $link = "https://s3.amazonaws.com/cytcuidadointegral.com/" . $name;
        } else {
            return "Sin Historia clinica";
        }


        $correo = $request->email;

        $mail = new PHPMailer();
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = $MAIL_HOST;
        $mail->Port = $MAIL_PORT; // or 587
        $mail->IsHTML(true);
        $mail->Username = $MAIL_USERNAME;
        $mail->Password = $MAIL_PASSWORD;
        $mail->SetFrom("info@softworldcolombia.com");
        $mail->Subject = "[C&T Cuidado Integral] Historia clinica";
        $mail->Body = view('template.envio_hc', compact('correo', 'link'));
        $mail->AddAddress($correo);
        $mail->send();

        return true;
    }

    public function notificaciones($tipo, $cedula)
    {

        date_default_timezone_set('America/Bogota');

        $number = DB::table("softworld_visitas_fallidas")->count() + 1;
        $code = "CYT_VIFA" . $number;

        DB::table("softworld_visitas_fallidas")->insert([
            "code_visita_fallida" => $code,
            "descripcion_visita_fallida" => $tipo,
            "fecha_registro" => date("Y-m-d"),
        ]);

        DB::table("softworld_detalle_visita_usuario_status")->insert([
            "fk_visita_fallida_vus" => $code,
            "fk_usuario_vus" => $cedula,
            "usuario_registro_notificacion" => auth()->user()->name,
            "fecha_registro" => date("Y-m-d"),
        ]);

        return true;
    }

    /* Interconsultas */

    public function interconsultas_table()
    {
        $array_interconsultas = [];
        $array_pacientes = [];
        $informacion = [];

        if (auth()->user()->id == 1) {
            $interconsultas = Interconsultas::all();

            foreach ($interconsultas as $data) {
                $paciente = Pacientes::where("cedula_paciente", $data->cedula_paciente_interconsulta)->get();
                $talentohumano = Doctores::where("cedula_doctor", $data->cedula_talentohumano_interconsulta)->get();

                $data_cie10 = DB::table("softworld_cie10")->where("codigo", $data->diagnostico_cie10_interconsulta)->get();
                $registro_cie10 = $data_cie10[0]->descripcion;

                $array_interconsultas[] = [
                    "vacio" => '',
                    "codigo_interconsulta" => $data->codigo_interconsulta,
                    "cedula_paciente" => $paciente[0]->cedula_paciente,
                    "nombres_pacientes" => $paciente[0]->primer_nombre . " " . $paciente[0]->segundo_nombre . "" . $paciente[0]->primer_nombre . " " . $paciente[0]->primer_apellido . " " . $paciente[0]->segundo_apellido,
                    "fecha" => $data->fecha_interconsulta,
                    "elaborado" => $talentohumano[0]->primer_nombre . " " . $talentohumano[0]->segundo_nombre . " " . $talentohumano[0]->primer_apellido,
                ];
            }
        } else {

            $informacion_talento_humano = Doctores::where("codigo_usuario", auth()->user()->id)
                ->get();

            //busco los pacientes asignados al de talento humano
            $pacientes = DB::table("softworld_talentohumano_pacientes")
                ->where("fk_identificacion_talentohumano_tp", $informacion_talento_humano[0]->cedula_doctor)
                ->get();

            foreach ($pacientes as $informacion_paciente) {

                $paciente = Pacientes::where(
                    [
                        ["cedula_paciente", $informacion_paciente->fk_identificacion_pacientes_tp],
                        ['estado_paciente', '<>', '2']
                    ]
                )->get();

                $estado = EstadosPlataforma::where("codigo_estado", $paciente[0]->estado_paciente)->get();

                $array_pacientes[] = $paciente[0]->cedula_paciente;
            }

            $data_pacientes = array_unique($array_pacientes);

            foreach ($data_pacientes as $data) {
                $interconsultas = Interconsultas::where("cedula_paciente_interconsulta", $data)->get();

                foreach ($interconsultas as $data) {
                    $paciente = Pacientes::where("cedula_paciente", $data->cedula_paciente_interconsulta)->get();
                    $talentohumano = Doctores::where("cedula_doctor", $data->cedula_talentohumano_interconsulta)->get();

                    $data_cie10 = DB::table("softworld_cie10")->where("codigo", $data->diagnostico_cie10_interconsulta)->get();
                    $registro_cie10 = $data_cie10[0]->descripcion;

                    $array_interconsultas[] = [
                        "vacio" => '',
                        "codigo_interconsulta" => $data->codigo_interconsulta,
                        "cedula_paciente" => $paciente[0]->cedula_paciente,
                        "nombres_pacientes" => $paciente[0]->primer_nombre . " " . $paciente[0]->segundo_nombre . "" . $paciente[0]->primer_nombre . " " . $paciente[0]->primer_apellido . " " . $paciente[0]->segundo_apellido,
                        "fecha" => $data->fecha_interconsulta,
                        "elaborado" => $talentohumano[0]->primer_nombre . " " . $talentohumano[0]->segundo_nombre . " " . $talentohumano[0]->primer_apellido,
                    ];
                }
            }
        }

        $informacion["data"] = $array_interconsultas;

        return json_encode($informacion);
    }

    public function interconsultas_index()
    {
        return view('administrativo.pacientes.interconsultas.index');
    }

    public function interconsultas_create()
    {

        $eps = EPS::where("estado_eps", 1)->get();
        $registro_cie10 = DB::table("softworld_cie10")->get();

        $array_pacientes = [];
        $informacion = [];
        $array_talentohumano = [];

        if (auth()->user()->id == 1) {
            $pacientes = Pacientes::all();
            $talentohumano = Doctores::where("estado_doctor", 1)->get();

            foreach ($pacientes as $paciente) {
                $array_pacientes[] = [
                    "cedula_paciente" => $paciente->cedula_paciente,
                    "primer_nombre" => $paciente->primer_nombre . " " . $paciente->segundo_nombre,
                    "primer_apellido" => $paciente->primer_apellido . " " . $paciente->segundo_apellido,
                ];
            }

            foreach ($talentohumano as $data_talento) {
                $array_talentohumano[] = [
                    "cedula_doctor" => $data_talento->cedula_doctor,
                    "primer_nombre" => $data_talento->primer_nombre . " " . $data_talento->segundo_nombre,
                    "primer_apellido" => $data_talento->primer_apellido . " " . $data_talento->segundo_apellido,
                ];
            }
        } else {

            $informacion_talento_humano = Doctores::where("codigo_usuario", auth()->user()->id)
                ->get();

            //busco los pacientes asignados al de talento humano
            $pacientes = DB::table("softworld_talentohumano_pacientes")
                ->where("fk_identificacion_talentohumano_tp", $informacion_talento_humano[0]->cedula_doctor)
                ->get();

            foreach ($pacientes as $informacion_paciente) {

                $paciente = Pacientes::where(
                    [
                        ["cedula_paciente", $informacion_paciente->fk_identificacion_pacientes_tp],
                        ['estado_paciente', '<>', '2']
                    ]
                )->get();

                $array_pacientes[] = [
                    "cedula_paciente" => $paciente[0]->cedula_paciente,
                    "primer_nombre" => $paciente[0]->primer_nombre . " " . $paciente[0]->segundo_nombre,
                    "primer_apellido" => $paciente[0]->primer_apellido . " " . $paciente[0]->segundo_apellido,
                ];
            }

            foreach ($informacion_talento_humano as $data_talento) {
                $array_talentohumano[] = [
                    "cedula_doctor" => $data_talento->cedula_doctor,
                    "primer_nombre" => $data_talento->primer_nombre . " " . $data_talento->segundo_nombre,
                    "primer_apellido" => $data_talento->primer_apellido . " " . $data_talento->segundo_apellido,
                ];
            }
        }

        return view('administrativo.pacientes.interconsultas.create', compact("registro_cie10", "array_talentohumano", "array_pacientes", "eps"));
    }

    public function interconsultas_store(Request $request)
    {
        $rules = [
            'paciente' => 'required',
            'talentohumano' => 'required',
            'diagnostico_cie10' => 'required',
            'eps' => 'required',
            'fecha' => 'required',
            'interconsulta_por' => 'required',
            'observacion' => 'required',
        ];

        $messages = [
            'paciente.required' => 'Seleccione un paciente.',
            'talentohumano.required' => 'Seleccione una opción.',
            'diagnostico_cie10.required' => 'El diagnostico es importante.',
            'eps' => 'Seleccione una EPS.',
            'fecha.required' => 'Seleccione la fecha de registro.',
            'interconsulta_por.required' => 'Este campo es importante.',
            'observacion' => 'Este campo es importante.',

        ];

        $this->validate($request, $rules, $messages);

        $number = Interconsultas::all()->count() + 1;
        $codigo = "CYT_INCO" . $number;

        Interconsultas::create([
            'codigo_interconsulta' => $codigo,
            'fecha_interconsulta' => $request->fecha,
            'cedula_paciente_interconsulta' => $request->paciente,
            'cedula_talentohumano_interconsulta' => $request->talentohumano,
            'diagnostico_cie10_interconsulta' => $request->diagnostico_cie10,
            'eps' => $request->eps,
            'interconsulta_por' => $request->interconsulta_por,
            'observaciones_interconsulta' => $request->observacion,
        ]);

        return true;
    }

    public function interconsultas_show($codigo)
    {

        $array_interconsultas = [];

        $interconsultas = Interconsultas::where("codigo_interconsulta", $codigo)->get();

        foreach ($interconsultas as $data) {
            $paciente = Pacientes::where("cedula_paciente", $data->cedula_paciente_interconsulta)->get();
            $talentohumano = Doctores::where("cedula_doctor", $data->cedula_talentohumano_interconsulta)->get();
            $eps = EPS::where("id_eps", $data->eps)->get();

            $data_cie10 = DB::table("softworld_cie10")->where("codigo", $data->diagnostico_cie10_interconsulta)->get();

            $array_interconsultas[] = [
                "codigo_interconsulta" => $data->codigo_interconsulta,
                "cedula_paciente" => $paciente[0]->cedula_paciente,
                "nombres_pacientes" => $paciente[0]->primer_nombre . " " . $paciente[0]->segundo_nombre . "" . $paciente[0]->primer_nombre . " " . $paciente[0]->primer_apellido . " " . $paciente[0]->segundo_apellido,
                "eps" => $eps[0]->nombre_eps,
                "cie10" => "(" . $data_cie10[0]->codigo . ") " . $data_cie10[0]->descripcion,
                "interconsulta_por" => $data->interconsulta_por,
                "fecha" => $data->fecha_interconsulta,
                "elaborado" => $talentohumano[0]->primer_nombre . " " . $talentohumano[0]->segundo_nombre . " " . $talentohumano[0]->primer_apellido,
                "firma" => $talentohumano[0]->firma_doctor,
                "observaciones_interconsulta" => $data->observaciones_interconsulta,
            ];
        }

        return view('administrativo.pacientes.interconsultas.show', compact("array_interconsultas"));
    }

    public function interconsultas_exportar($codigo)
    {
        $array_interconsultas = [];

        $interconsultas = Interconsultas::where("codigo_interconsulta", $codigo)->get();

        foreach ($interconsultas as $data) {
            $paciente = Pacientes::where("cedula_paciente", $data->cedula_paciente_interconsulta)->get();
            $talentohumano = Doctores::where("cedula_doctor", $data->cedula_talentohumano_interconsulta)->get();
            $profesion = Profesiones::where("codigo_profesion", $talentohumano[0]->id_profesion)->get();
            $eps = EPS::where("id_eps", $data->eps)->get();

            $data_cie10 = DB::table("softworld_cie10")->where("codigo", $data->diagnostico_cie10_interconsulta)->get();

            $array_interconsultas[] = [
                "codigo_interconsulta" => $data->codigo_interconsulta,
                "cedula_paciente" => $paciente[0]->cedula_paciente,
                "nombres_pacientes" => $paciente[0]->primer_nombre . " " . $paciente[0]->segundo_nombre . "" . $paciente[0]->primer_nombre . " " . $paciente[0]->primer_apellido . " " . $paciente[0]->segundo_apellido,
                "eps" => $eps[0]->nombre_eps,
                "cie10" => "(" . $data_cie10[0]->codigo . ") " . $data_cie10[0]->descripcion,
                "interconsulta_por" => $data->interconsulta_por,
                "fecha" => $data->fecha_interconsulta,
                "elaborado" => $talentohumano[0]->primer_nombre . " " . $talentohumano[0]->segundo_nombre . " " . $talentohumano[0]->primer_apellido,
                "firma" => $talentohumano[0]->firma_doctor,
                "observaciones_interconsulta" => $data->observaciones_interconsulta,
                "profesion" => $profesion[0]->nombre_profesion,
                "tarjeta_profesional" => $talentohumano[0]->tarjeta_profesional,
            ];
        }

        $pdf = PDF::loadView("administrativo.pacientes.interconsultas.pdf", compact(
            'array_interconsultas'
        ));

        return $pdf->download('interconsulta-' . $codigo . '.pdf');
    }

    public function template_documentos($cedula)
    {
        $paciente = Pacientes::where("cedula_paciente", $cedula)->get();

        return view('administrativo.pacientes.documentos', compact("paciente"));
    }

    public function documentos_post(Request $request)
    {

        $rules = [
            'cedula' => 'required',
            'file' => 'required|mimes:jpeg,jpg,png,pdf'
        ];

        $messages = [
            'cedula.required' => 'La cédula del paciente es importante',
            'file.required' => 'El primer nombre del paciente es importante.',
            'file.mimes' => 'La extensión del documento no es permitida por el sistema. Debe ser: jpeg,jpg,png o pdf',

        ];

        $this->validate($request, $rules, $messages);

        date_default_timezone_set('America/Bogota');

        if ($request->file('file')) {
            $documento = $request->file('file')->store("Pacientes/" . $request->cedula, 's3');

            $insercion = DB::table("softworld_documentos_pacientes")->insert([
                "cedula_paciente_dopa" => $request->cedula,
                "documento_paciente_dopa" => $documento,
                "fecha" => date("d-m-Y H:i"),
            ]);
        }
        
        return true;
    }
}
