<?php

namespace App\Http\Controllers\page;

use App\Http\Controllers\Controller;
use App\Models\SoftworldMembership;
use App\Models\SoftworldPerson;
use App\Models\SoftworldPlans;
use App\Models\SoftworldSubscription;
use App\Services\EpaycoService;
use App\User;
use Carbon\Carbon;
use Epayco\Resources\Subscriptions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

class SubscriptionController extends Controller
{
    protected $epaycoService;

    public function __construct(EpaycoService $epaycoService)
    {
        $this->epaycoService = $epaycoService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function registerSubscriptor(Request $request)
    {
        $rules = [
            'identificacion_profesional' => 'required|unique:softworld_people,id',
            'tarjeta_profesional' => 'required|unique:softworld_people,professional_card',
            'tipo_de_documento_profesional' => 'required',
            'primer_nombre_profesional' => 'required',
            'primer_apellido_profesional' => 'required',
            'celular_profesional' => 'required',
            'correo_profesional' => 'required|email|required|unique:softworld_people,email',
            'correo_profesional_usuario' => 'required|email|required|unique:users,email',
            'password' => 'required|confirmed',
        ];

        $messages = [
            'identificacion_profesional.required' => 'Este campo es importante.',
            'identificacion_profesional.unique' => 'El número de identificación ya pertenece a otra persona.',
            'tarjeta_profesional.required' => 'Este campo es importante.',
            'tarjeta_profesional.unique' => 'La tarjeta profesional ingresada, ya pertenece a otra persona.',
            'tipo_de_documento_profesional.required' => 'Este campo es importante.',
            'primer_nombre_profesional.required' => 'Este campo es importante.',
            'primer_apellido_profesional.required' => 'Este campo es importante.',
            'celular_profesional.required' => 'Este campo es importante.',
            'correo_profesional.required' => 'Este campo es importante.',
            'correo_profesional.unique' => 'El correo electrónico ingresado ya pertenece a otra persona.',
            'password.required' => 'Ingrese una contraseña para su usuario.',
            'password.confirmed' => 'Las contraseñas no coinciden.',
        ];

        $this->validate($request, $rules, $messages);

        $user = User::create([
            'name' => $request->primer_nombre_profesional . " " . $request->primer_apellido_profesional,
            'email' => $request->correo_profesional,
            'condicion' => 1,
            'password' => Hash::make($request->password),
        ]);

        $rolSubscriber = Role::where("name", "subscriber")->first();
        $user->roles()->attach($rolSubscriber);

        SoftworldPerson::create([
            'document_type' => $request->tipo_de_documento_profesional,
            'professional_card' => $request->tarjeta_profesional,
            'id' => $request->identificacion_profesional,
            'first_name' => $request->primer_nombre_profesional,
            'second_name' => $request->segundo_nombre_profesional,
            'first_last_name' => $request->primer_apellido_profesional,
            'second_last_name' => $request->segundo_apellido_profesional,
            'cellphone' => $request->celular_profesional,
            'email' => $request->correo_profesional,
            'type' => 'subscriber',
            'user' => $user->id,
            'status' => 1,
        ]);

        if (Auth::attempt(['email' => $request->correo_profesional, 'password' => $request->password])) {
            // Si las credenciales son válidas, el usuario ha iniciado sesión correctamente
            $user = Auth::user();

            // Generar y devolver el token de acceso personal
            $tokenResult = $user->createToken('Personal Access Token');

            $token = $tokenResult->token;

            if ($request->remember_me) {
                $token->expires_at = Carbon::now()->addWeeks(1);
                $token->save();
            }

            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $token->expires_at
                )->toDateTimeString()
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function plansMembership(Request $request)
    {
        $rules = [
            'codigo_membresia' => 'required',
        ];

        $messages = [
            'codigo_membresia.required' => 'Por favlor, seleccione una membresia.',
        ];

        $this->validate($request, $rules, $messages);

        $membership = SoftworldMembership::find($request->codigo_membresia);

        return view('page-layouts.template.subscriptions.membership-plans', compact("membership"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function plansMembershipSelected(Request $request)
    {
        $plan = SoftworldPlans::find($request->codigo_plan);
        $person = SoftworldPerson::getByUser();
        return view('page-layouts.template.subscriptions.form-credit-card', compact("plan", "person"));
    }

    public function validacionTarjetaCredito(string $numeroTarjeta)
    {
        date_default_timezone_set("America/Bogota");

        $cards = array(
            "visa" => "(4\d{12}(?:\d{3})?)",
            "amex" => "(3[47]\d{13})",
            "jcb" => "(35[2-8][89]\d\d\d{10})",
            "maestro" => "((?:5020|5038|6304|6579|6761)\d{12}(?:\d\d)?)",
            "solo" => "((?:6334|6767)\d{12}(?:\d\d)?\d?)",
            "mastercard" => "(5[1-5]\d{14})",
            "switch" => "(?:(?:(?:4903|4905|4911|4936|6333|6759)\d{12})|(?:(?:564182|633110)\d{10})(\d\d)?\d?)",
        );

        $numero = str_replace(' ', '', $numeroTarjeta);

        $names = array("Visa", "American Express", "JCB", "Maestro", "Solo", "Mastercard", "Switch");
        $matches = array();
        $pattern = "#^(?:" . implode("|", $cards) . ")$#";
        $result = preg_match($pattern, str_replace(" ", "", $numero), $matches);

        return $result;
    }

    public function subscriptionPayment(Request $request)
    {
        $rules = [
            'nombre' => 'required',
            'numero' => 'required',
            'year' => 'required',
            'mes' => 'required',
            'ccv' => 'required',
        ];

        $messages = [
            'nombre.required' => 'Este campo es importante.',
            'numero.unique' => 'Este campo es importante.',
            'year.required' => 'Este campo es importante.',
            'mes.required' => 'Este campo es importante.',
            'ccv.unique' => 'Este campo es importante.',
        ];

        $this->validate($request, $rules, $messages);

        $tokenCard = "";
        $tokenCustomer = "";

        /* Valido que la tarjeta de credito, sea correcta */
        $validateCreditCard = $this->validacionTarjetaCredito($request->numero);

        if (!$validateCreditCard) {
            return "CREDIT_CARD_INCORRECT";
        }

        /* Identifico información del usuario actual */
        $person = SoftworldPerson::getByUser();

        if (empty($person->token_client)) {
            $tokenCard = $this->createTokenCreditCard($request->all());
            $tokenCustomer = $this->createTokenClientEpayco($tokenCard, $person);
            $person->token_client = $tokenCustomer;
            $person->update();
        } else {
            $tokenCard = $this->createTokenCreditCard($request->all());
            $tokenCustomer = $person->token_client;
        }

        /* Verifico si no hay suscripciones activas */
        $codePlan = SoftworldPlans::find($request->code_plan);
        $subscriptionActive = SoftworldSubscription::getDataByUser();

        if (empty($subscriptionActive)) {
            $subscription = $this->createSubscription($codePlan->platform_code, $tokenCard, $tokenCustomer, $person->id);
            $chargeSubscription = $this->chargeSubscription($codePlan->platform_code, $tokenCard, $tokenCustomer, $person->id, $person->cellphone);
            $store = $this->storeSubscription($codePlan->code, $chargeSubscription);
        } else {
            $respuestaCancelacion = $this->cancelSubscription($subscriptionActive->code_subscription_pay);

            if ($respuestaCancelacion->status == true) {
                $subscriptionActive->date_renewal_cancelled = date("y-m-d H:i");
                $subscriptionActive->renewal = false;
                $subscriptionActive->update();

                $subscription = $this->createSubscription($codePlan->platform_code, $tokenCard, $tokenCustomer, $person->id);
                $chargeSubscription = $this->chargeSubscription($codePlan->platform_code, $tokenCard, $tokenCustomer, $person->id, $person->cellphone);
                $store = $this->storeSubscription($codePlan->code, $chargeSubscription);
            }
        }

        return true;
    }

    public function createTokenClientEpayco($tokenCard, $person)
    {
        $customer = $this->epaycoService->createEpaycoCustomer([
            "token_card" => $tokenCard,
            "name" => $person->first_name . " " . $person->second_name,
            "last_name" => $person->first_last_name . " " . $person->second_last_name,
            "email" => $person->email,
            "cell_phone" => $person->cellphone,
        ]);

        return $customer->data->customerId;
    }

    public function createTokenCreditCard($dataCreditCard)
    {
        $token = $this->epaycoService->createCreditCardToken([
            "card[number]" => str_replace(' ', '', $dataCreditCard["numero"]),
            "card[exp_year]" => $dataCreditCard["year"],
            "card[exp_month]" => $dataCreditCard["mes"],
            "card[cvc]" => $dataCreditCard["ccv"]
        ]);

        return $token->id;
    }

    public function createSubscription($codePlan, $cardToken, $customerId, $personId)
    {
        $subscription = $this->epaycoService->createEpaycoSubscription([
            "id_plan" => $codePlan,
            "customer" => $customerId,
            "token_card" => $cardToken,
            "doc_type" => "CC",
            "doc_number" => '"' . $personId . '"',
            "url_confirmation" => "https://ejemplo.com/confirmacion",
            "method_confirmation" => "GET"
        ]);

        return $subscription;
    }

    public function chargeSubscription($codePlan, $cardToken, $customerId, $personId, $personCellphone)
    {
        $chargeSubscription = $this->epaycoService->createEpaycoSubscription([
            "id_plan" => $codePlan,
            "customer" => $customerId,
            "token_card" => $cardToken,
            "doc_type" => "CC",
            "doc_number" =>  '"' . $personId . '"',
            "phone" => $personCellphone,
            "cell_phone" => $personCellphone,
            "ip" => request()->ip()
        ]);

        return $chargeSubscription;
    }

    public function storeSubscription($codePlan, $epayco)
    {
        $subscription = new SoftworldSubscription();
        $subscription->user_id = auth()->user()->id;
        $subscription->plan_id = $codePlan;
        $subscription->start_date = date("y-m-d", strtotime($epayco->current_period_start));
        $subscription->end_date = date("y-m-d", strtotime($epayco->current_period_end));
        $subscription->renewal = 1;
        $subscription->code_subscription_pay = $epayco->id;
        $subscription->save();

        return true;
    }

    public function cancelSubscription($id)
    {
        $respuestaCancelacion = $this->epaycoService->cancelEpaycoSubscription($id);
        return $respuestaCancelacion;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
