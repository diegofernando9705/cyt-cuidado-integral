<?php

namespace App\Http\Controllers\page;

use App\Http\Controllers\Controller;
use App\Models\SoftworldDocumentTypes;
use App\Models\SoftworldMembership;
use App\Models\SoftworldNews;
use App\Models\SoftworldServices;
use Illuminate\Http\Request;

class GlobalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function services()
    {
        $services = SoftworldServices::active();
        return response()->json($services);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function membership()
    {
        $membersip = SoftworldMembership::active();
        return response()->json($membersip);
    }

    public function news()
    {
        $news = SoftworldNews::active();
        return response()->json($news);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function formSubscription(Request $request)
    {
        $documentType = SoftworldDocumentTypes::active();
        $membership = SoftworldMembership::find($request->codigo_membresia);
        return view("page-layouts.subscriptions.form", compact("membership", "documentType"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
