<?php

namespace App\Http\Controllers\page;

use App\Http\Controllers\Controller;
use App\Models\DetailsFavoritesNews;
use App\Models\DetailsViewersNews;
use App\Models\SoftworldAboutUs;
use App\Models\SoftworldAppointmentSheduling;
use App\Models\SoftworldContactForm;
use App\Models\SoftworldFrequentQuestions;
use App\Models\SoftworldHeadersTypes;
use App\Models\SoftworldNews;
use App\Models\SoftworldPerson;
use App\Models\SoftworldQuoteOpinion;
use App\Models\SoftworldServices;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use PHPMailer\PHPMailer\PHPMailer;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $typeHeaders = SoftworldHeadersTypes::page("index");

        if ($typeHeaders->type == "text") {
            $content = $typeHeaders->text;
            $subview = view('page-layouts.template.headers.text', compact('content'))->render();
        } else {
            $content = $typeHeaders->images;
            $subview = view('page-layouts.template.headers.slider', compact('content'))->render();
        }

        return view('page.index', compact('subview'));
    }

    public function about()
    {
        $typeHeaders = SoftworldHeadersTypes::page("about_us");

        if ($typeHeaders->type == "text") {
            $content = $typeHeaders->text;
            $subview = view('page-layouts.template.headers.text', compact('content'))->render();
        } else {
            $content = $typeHeaders->images;
            $subview = view('page-layouts.template.headers.slider', compact('content'))->render();
        }

        $data = SoftworldAboutUs::first();

        return view('page.about_us', compact('subview', 'data'));
    }

    public function services()
    {
        $typeHeaders = SoftworldHeadersTypes::page("services");

        if ($typeHeaders->type == "text") {
            $content = $typeHeaders->text;
            $subview = view('page-layouts.template.headers.text', compact('content'))->render();
        } else {
            $content = $typeHeaders->images;
            $subview = view('page-layouts.template.headers.slider', compact('content'))->render();
        }

        $data = SoftworldAboutUs::first();

        return view('page.services', compact('subview', 'data'));
    }

    public function service($url)
    {
        $data = SoftworldServices::url($url);
        return view('page.service', compact('data'));
    }

    public function news()
    {
        $typeHeaders = SoftworldHeadersTypes::page("news");

        if ($typeHeaders->type == "text") {
            $content = $typeHeaders->text;
            $subview = view('page-layouts.template.headers.text', compact('content'))->render();
        } else {
            $content = $typeHeaders->images;
            $subview = view('page-layouts.template.headers.slider', compact('content'))->render();
        }

        return view('page.news', compact('subview'));
    }

    public function validateMembershipNewsAndUser($id)
    {

        $new = SoftworldNews::find($id);

        if ($new->membership()->count() === 0) {
            return false;
        }

        $array_memberships = [];

        foreach ($new->membership as $membership) {
            $array_memberships[] = $membership->code;
        }

        $data_profesional = SoftworldPerson::getDataByUser(auth()->user()->id);

        foreach ($data_profesional->getSubscription as $subscription) {
            foreach ($subscription->getPlan->memberships as $membership) {
                $membership_user = $membership->code;
                // Validar si $membership_user está dentro de las opciones de $new->membership
                if (in_array($membership_user, $array_memberships)) {
                    return true; // El usuario tiene la membresía adecuada para ver la noticia
                }
            }
        }

        return false;
    }

    public function mostViewedNews($idNew = NULL)
    {
        $query = DetailsViewersNews::mostViewedNews();
        $news = SoftworldNews::whereIn('id', $query)->where('id', '<>', $idNew)->get();
        return $news;
    }

    public function new($url)
    {
        date_default_timezone_set("America/Bogota");

        $favorite = "";
        $data = SoftworldNews::url($url);

        DetailsViewersNews::create([
            'new_id' => $data->id,
            'date_vewer' => date("Y-m-d H:m:s"),
        ]);

        $mostViewedNews = $this->mostViewedNews($data->id);

        if ($data->type == "subscribers") {
            if (Auth::check()) {
                if ($this->validateMembershipNewsAndUser($data->id)) {
                    $favorite = DetailsFavoritesNews::isUserFavorite($data->id);
                    return view('page-layouts.template.news.new', compact('data', 'mostViewedNews', 'favorite'));
                } else {
                    $alert = "No tienes acceso al contenido de esta noticia con tu membresía actual.";
                }
            } else {
                $alert = "Suscríbete para ver el contenido de esta noticia";
            }

            $title = $data->title;
            return view('page-layouts.template.alert.new-subscribers', compact('title', 'mostViewedNews', 'alert', 'favorite'));
        } else {
            return view('page-layouts.template.news.new', compact('data', 'mostViewedNews', 'favorite'));
        }
    }

    public function frequentQuestions()
    {
        $typeHeaders = SoftworldHeadersTypes::page("frequent_questions");

        if ($typeHeaders->type == "text") {
            $content = $typeHeaders->text;
            $subview = view('page-layouts.template.headers.text', compact('content'))->render();
        } else {
            $content = $typeHeaders->images;
            $subview = view('page-layouts.template.headers.slider', compact('content'))->render();
        }

        $questions = SoftworldFrequentQuestions::active();

        return view('page.frequent-questions', compact('subview', 'questions'));
    }

    public function contact()
    {
        $typeHeaders = SoftworldHeadersTypes::page("contact");

        if ($typeHeaders->type == "text") {
            $content = $typeHeaders->text;
            $subview = view('page-layouts.template.headers.text', compact('content'))->render();
        } else {
            $content = $typeHeaders->images;
            $subview = view('page-layouts.template.headers.slider', compact('content'))->render();
        }

        return view('page.contact', compact('subview'));
    }

    public function contactStore(Request $request)
    {
        $rules = [
            'nombres' => 'required',
            'apellidos' => 'required',
            'correos' => 'required',
            'celular' => 'required',
            'mensaje' => 'required',
        ];

        $messages = [
            'nombres.required' => 'Este campo es importante',
            'apellidos.required' => 'Este campo es importante.',
            'correos.required' => 'Este campo es importante.',
            'celular.required' => 'Este campo es importante.',
            'mensaje.required' => 'Este campo es importante.',
        ];

        $this->validate($request, $rules, $messages);

        SoftworldContactForm::create([
            'names' => $request->nombres,
            'last_names' => $request->apellidos,
            'email' => $request->correos,
            'cellphone' => $request->celular,
            'message' => $request->mensaje,
        ]);

        return true;
    }


    public function quoteOpinion(Request $request)
    {

        $valor_capital  = $request->valor_pretensiones;

        /* ==================== PORCENTAJE DE VALOR PARA EL VOLUMEN DE INFORMACIÓN ==================== */

        if ($request->tipo_cliente == "juridica_natural" && $request->volumen_informacion == 'opcion_uno') {
            $volumen_informacion = -0.05;
        } else if ($request->tipo_cliente == "juridica_normal" && $request->volumen_informacion == 'opcion_uno') {
            $volumen_informacion = -0.07;
        } else if ($request->tipo_cliente == "juridica_multinacional" && $request->volumen_informacion == 'opcion_uno') {
            $volumen_informacion = -0.03;
        } else if ($request->tipo_cliente == "derecho_publico" && $request->volumen_informacion == 'opcion_uno') {
            $volumen_informacion = -0.04;
        } else if ($request->tipo_cliente == "juridica_natural" && $request->volumen_informacion == 'opcion_dos') {
            $volumen_informacion = 0.06;
        } else if ($request->tipo_cliente == "juridica_normal" && $request->volumen_informacion == 'opcion_dos') {
            $volumen_informacion = 0.07;
        } else if ($request->tipo_cliente == "juridica_multinacional" && $request->volumen_informacion == 'opcion_dos') {
            $volumen_informacion = 0.08;
        } else if ($request->tipo_cliente == "derecho_publico" && $request->volumen_informacion == 'opcion_dos') {
            $volumen_informacion = 0.09;
        } else if ($request->tipo_cliente == "juridica_natural" && $request->volumen_informacion == 'opcion_tres') {
            $volumen_informacion = 0.10;
        } else if ($request->tipo_cliente == "juridica_normal" && $request->volumen_informacion == 'opcion_tres') {
            $volumen_informacion = 0.11;
        } else if ($request->tipo_cliente == "juridica_multinacional" && $request->volumen_informacion == 'opcion_tres') {
            $volumen_informacion = 0.12;
        } else if ($request->tipo_cliente == "derecho_publico" && $request->volumen_informacion == 'opcion_tres') {
            $volumen_informacion = 0.15;
        }

        /* ==================== PORCENTAJE DE VALOR PARA EL PORCENTAJE DE PRETENSIONES ==================== */

        if ($request->tipo_cliente == "juridica_natural" && $request->pretensiones == 'opcion_uno') {
            $porcentaje_pretension = 0.20;
        } else if ($request->tipo_cliente == "juridica_normal" && $request->pretensiones == 'opcion_uno') {
            $porcentaje_pretension = 0.23;
        } else if ($request->tipo_cliente == "juridica_multinacional" && $request->pretensiones == 'opcion_uno') {
            $porcentaje_pretension = 0.28;
        } else if ($request->tipo_cliente == "derecho_publico" && $request->pretensiones == 'opcion_uno') {
            $porcentaje_pretension = 0.30;
        } else if ($request->tipo_cliente == "juridica_natural" && $request->pretensiones == 'opcion_dos') {
            $porcentaje_pretension = 0.23;
        } else if ($request->tipo_cliente == "juridica_normal" && $request->pretensiones == 'opcion_dos') {
            $porcentaje_pretension = 0.20;
        } else if ($request->tipo_cliente == "juridica_multinacional" && $request->pretensiones == 'opcion_dos') {
            $porcentaje_pretension = 0.32;
        } else if ($request->tipo_cliente == "derecho_publico" && $request->pretensiones == 'opcion_dos') {
            $porcentaje_pretension = 0.35;
        } else if ($request->tipo_cliente == "juridica_natural" && $request->pretensiones == 'opcion_tres') {
            $porcentaje_pretension = 0.26;
        } else if ($request->tipo_cliente == "juridica_normal" && $request->pretensiones == 'opcion_tres') {
            $porcentaje_pretension = 0.15;
        } else if ($request->tipo_cliente == "juridica_multinacional" && $request->pretensiones == 'opcion_tres') {
            $porcentaje_pretension = 0.30;
        } else if ($request->tipo_cliente == "derecho_publico" && $request->pretensiones == 'opcion_tres') {
            $porcentaje_pretension = 0.30;
        } else if ($request->tipo_cliente == "juridica_natural" && $request->pretensiones == 'opcion_cuatro') {
            $porcentaje_pretension = 0.13;
        } else if ($request->tipo_cliente == "juridica_normal" && $request->pretensiones == 'opcion_cuatro') {
            $porcentaje_pretension = 0.12;
        } else if ($request->tipo_cliente == "juridica_multinacional" && $request->pretensiones == 'opcion_cuatro') {
            $porcentaje_pretension = 0.15;
        } else if ($request->tipo_cliente == "derecho_publico" && $request->pretensiones == 'opcion_cuatro') {
            $porcentaje_pretension = 0.22;
        }

        /* ==================== PORCENTAJE DE VALOR PARA EL TIPO DE CLIENTE ==================== */

        if ($request->tipo_cliente == "juridica_natural") {
            $porcentaje_tipo_cliente = 0.07;
        } else if ($request->tipo_cliente == "juridica_normal") {
            $porcentaje_tipo_cliente = 0.05;
        } else if ($request->tipo_cliente == "juridica_multinacional") {
            $porcentaje_tipo_cliente = 0.25;
        } else if ($request->tipo_cliente == "derecho_publico") {
            $porcentaje_tipo_cliente = 0.40;
        }

        /* ==================== PORCENTAJE DE VALOR PARA EL TIEMPO DE DEDICACION ==================== */

        //No aplica

        /* ==================== PORCENTAJE DE VALOR PARA EL INGRESO ==================== */
        //No aplica

        /* ==================== PORCENTAJE DE VALOR DE SERVICIOS REQUERIDOS O ADICIONALES ==================== */

        if ($request->servicios_adicionales) {
            $valor_preparacion_audiencia = $valor_capital * (0.02 / 100);
            $valor_sustentacion_audiencia = $valor_capital * (0.03 / 100);
            $elementos_tecnicos_probatorio = $valor_capital * (0.02 / 100);

            $valor_servicios_adicionales = $valor_preparacion_audiencia + $valor_sustentacion_audiencia + $elementos_tecnicos_probatorio;
        }

        /*
        if ($request->servicios_adicionales == "opcion_uno") {
            $valor_servicios_adicionales = 0.02;
        } else if ($request->servicios_adicionales == "opcion_dos") {
            $valor_servicios_adicionales = 0.03;
        } else if ($request->servicios_adicionales == "opcion_tres") {
            $valor_servicios_adicionales = 0.02;
        } else {
            $valor_servicios_adicionales = 0;
        }*/

        /* ==================== PORCENTAJE DE VALOR DE SERVICIOS REQUERIDOS O ADICIONALES ==================== */

        if ($request->area_dictamen == "opcion_uno") {
            $valor_area_dictamen = 5000000;
        } else if ($request->area_dictamen == "opcion_dos") {
            $valor_area_dictamen = 5000000;
        } else {
            $valor_area_dictamen = 2000000;
        }

        /* ==================== PORCENTAJE DE VALOR SEGUN EXPERIENCIA ABOGADO ==================== */

        if ($request->experiencia_litigio == "opcion_uno") {
            $valor_experiencia_litigio = 0.05;
        } else if ($request->experiencia_litigio == "opcion_dos") {
            $valor_experiencia_litigio = 0.08;
        } else if ($request->experiencia_litigio == "opcion_tres") {
            $valor_experiencia_litigio = 0.10;
        } else {
            $valor_experiencia_litigio = 0.15;
        }

        /* ==================== PORCENTAJE DE VALOR SEGUN TRIBUNAL ==================== */

        if ($request->tribunal == "privado") {
            $valor_tribunal = 0.30;
        } else {
            $valor_tribunal = 0.15;
        }


        /* ==================== OPERACION TIPO DE CLIENTE ==================== */

        $operacion_tipo_cliente = $valor_capital * ($porcentaje_tipo_cliente / 100);
        $operacion_pretensiones = $valor_capital * ($porcentaje_pretension / 100);
        $operacion_expriencia_abogado = $valor_capital * ($valor_experiencia_litigio / 100);
        $operacion_tipo_dictamen = $valor_capital * ($valor_tribunal / 100);
        $operacion_preparacion_audiencia = $valor_servicios_adicionales;
        $operacion_volumen_informacion = $valor_capital * ($volumen_informacion / 100);
        $valor_area_dictamen = $valor_area_dictamen;


        $valor_final_dictamen = $operacion_tipo_cliente + $operacion_pretensiones + $operacion_expriencia_abogado + $operacion_tipo_dictamen + $operacion_volumen_informacion + $valor_area_dictamen + $operacion_preparacion_audiencia;

        $register = SoftworldQuoteOpinion::create([
            "nombres_apellidos" => $request->nombres_apellidos_persona,
            "correo_electronico" => $request->correo_persona,
            "telefono_celular" => $request->celular_persona,
            "ciudad" => $request->ciudad_persona,
            "area_derecho" => $request->area_derecho_persona,
            "valor_pretensiones" => $request->valor_pretensiones,
            "tipo_cliente" => $request->tipo_cliente,
            "opcion_pretensiones" => $request->pretensiones,
            "experiencia_litigio" => $request->experiencia_litigio,
            "tribunal" => $request->tribunal,
            "volumen_informacion" => $request->volumen_informacion,
            "servicios_adicionales" => $request->servicios_adicionales,
            "area_dictamen" => $request->area_dictamen,
            "resultado_cotizacion" => $valor_final_dictamen,
        ]);

        $array_informacion = SoftworldQuoteOpinion::find($register->id);

        date_default_timezone_set('America/Bogota');

        $MAIL_HOST = config('app.MAIL_HOST');
        $MAIL_PORT = config('app.MAIL_PORT');
        $MAIL_USERNAME = config('app.MAIL_USERNAME');
        $MAIL_PASSWORD = config('app.MAIL_PASSWORD');
        $setFrom = config("app.MAIL_USERNAME");

        $correo = $request->correo_persona;

        $mail = new PHPMailer();
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = $MAIL_HOST;
        $mail->Port = $MAIL_PORT; // or 587
        $mail->IsHTML(true);
        $mail->Username = $MAIL_USERNAME;
        $mail->Password = $MAIL_PASSWORD;
        $mail->SetFrom($setFrom);
        $mail->Subject = "[CNP Centro Nacional de Pruebas] Resumen cotizador dictamenes";
        $mail->Body = view('page-layouts.template.email.cotizador-dictamen', compact("array_informacion"));
        $mail->AddAddress($correo);
        $mail->send();

        return true;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function membership()
    {
        return "Hola";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function registerAppointmentScheduling(Request $request)
    {
        $rules = [
            'fecha_seleccionada' => 'required|date|after_or_equal:today',
            'hora_inicial' => 'required|date_format:H:i',
            'hora_final' => 'required|date_format:H:i|after:hora_inicial',
            'nombre' => 'required',
            'apellidos' => 'required',
            'celular' => 'required',
            'correo' => 'required|email',
        ];

        $messages = [
            'fecha_seleccionada.required' => 'El campo fecha seleccionada es requerido.',
            'fecha_seleccionada.date' => 'El campo fecha seleccionada debe ser una fecha válida.',
            'fecha_seleccionada.after_or_equal' => 'El campo fecha seleccionada debe ser igual o inferior a la fecha actual.',
            'hora_inicial.required' => 'El campo hora inicial es requerido.',
            'hora_inicial.date_format' => 'El campo hora inicial debe tener el formato HH:MM.',
            'hora_final.required' => 'El campo hora final es requerido.',
            'hora_final.date_format' => 'El campo hora final debe tener el formato HH:MM.',
            'hora_final.after' => 'El campo hora final debe ser posterior a la hora inicial.',
            'nombre.required' => 'El campo nombre es requerido.',
            'apellidos.required' => 'El campo apellidos es requerido.',
            'celular.required' => 'El campo celular es requerido.',
            'correo.required' => 'El campo correo es requerido.',
            'correo.email' => 'El campo correo debe ser una dirección de correo electrónico válida.',
        ];

        $this->validate($request, $rules, $messages);

        $count = SoftworldAppointmentSheduling::count() + 1;
        $code = "CNP-AC" . $count;

        SoftworldAppointmentSheduling::create([
            'code' => $code,
            'date' => $request->fecha_seleccionada,
            'start_hour' => $request->hora_inicial,
            'end_hour' => $request->hora_final,
            'names' => $request->nombre,
            'last_names' => $request->apellidos,
            'celphone' => $request->celular,
            'email' => $request->correo,
            'status' => 5,
        ]);

        return true;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function loginForm()
    {
        return view("page-layouts.template.login.form-login");
    }

    public function resetForm()
    {
        return view("page-layouts.template.login.form-reset");
    }

    public function loginPost(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Por favor, verifica tus credenciales y vuelve a intentarlo.'
            ], 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function accessDenied()
    {
        return "No tiene permitido";
    }

    public function loginLogout(Request $request)
    {
        Auth::logout();
        return redirect()->route('index');
    }


    /* RECOVERY PASSWORD */

    public function sendRecoveryPassword(Request $request)
    {
        $rules = [
            'email' => 'required|email'
        ];

        $messages = [
            'email.required' => 'Correo electronico no valido',
            'email.email' => 'Correo electronico no valido.',
        ];

        $this->validate($request, $rules, $messages);


        $usuario = DB::table('users')->where('email', '=', $request->email)->first();


        if ($usuario) {

            date_default_timezone_set('America/Bogota');

            $MAIL_HOST = config('app.MAIL_HOST');
            $MAIL_PORT = config('app.MAIL_PORT');
            $MAIL_USERNAME = config('app.MAIL_USERNAME');
            $MAIL_PASSWORD = config('app.MAIL_PASSWORD');


            $token = Str::random(50);

            DB::table('password_resets')->where('email', $request->email)->delete();

            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => $token,
                'created_at' => date('Y-m-d h:i'),
            ]);


            $link = route("reset_password_form", [$token]);

            $correo = $request->email;
            $mail = new PHPMailer();
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
            $mail->IsSMTP(); // enable SMTP
            $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
            $mail->SMTPAuth = true; // authentication enabled
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = $MAIL_HOST;
            $mail->Port = $MAIL_PORT; // or 587
            $mail->IsHTML(true);
            $mail->Username = $MAIL_USERNAME;
            $mail->Password = $MAIL_PASSWORD;
            $mail->SetFrom("no-replay@cnp.com.co");
            $mail->Subject = "[Centro Nacional de Pruebas] Restablecer contraseña";
            $mail->Body = view('page-layouts.template.email.recovery-password', compact('correo', 'link', 'usuario'));
            $mail->AddAddress($correo);
            $mail->send();

            return true;
        } else {
            return false;
        }
    }

    public function formRecoveryPassword($token)
    {
        date_default_timezone_set('America/Bogota');

        $datos = DB::table('password_resets')->where('token', $token)->first();

        if ($datos) {
            $sql_token = DB::table('password_resets')->where('token', $token)->get();

            $fecha = $sql_token['0']->created_at;
            $fecha_hora = explode(" ", $fecha);

            if ($fecha_hora[0] == date('Y-m-d')) {

                $horas_bd = explode(":", $fecha_hora[1]);
                $minutos_registrado_BD = ($horas_bd[0] * 60) + $horas_bd[1];

                $horas_hoy = explode(":", date('h:i'));
                $minutos_hoy = ($horas_hoy[0] * 60) + $horas_hoy[1];

                $minutos_comparacion = $minutos_registrado_BD - $minutos_hoy;
                //devolverá 600 que serán los minutos totales.

                if (abs($minutos_comparacion) > 60) {
                    $pageConfigs = ['blankPage' => true];
                    return view('/content/miscellaneous/error', ['pageConfigs' => $pageConfigs]);
                } else {
                    $pageConfigs = ['blankPage' => true];

                    return view('page-layouts.template.login.form-reset-password', ['pageConfigs' => $pageConfigs, 'codigo' => $token]);
                }
            } else {
                return "No";
            }
        } else {
            $pageConfigs = ['blankPage' => true];

            return view('/content/miscellaneous/error', ['pageConfigs' => $pageConfigs]);
        }
    }

    function updateRecoveryPassword(Request $request)
    {
        $rules = [
            'password' => 'required|string|',
            'c_password' => 'required|same:password',
        ];

        $messages = [
            'password.required' => 'Coloque la nueva contraseña',
            'password.string' => 'La contraseña debe ser cadena de texto.',
            'c_password.required' => 'Verifique las contraseñas nuevamente.',
            'c_password.same' => 'Las contraseñas no coinciden.',
        ];

        $this->validate($request, $rules, $messages);

        $datos = DB::table('password_resets')->where('token', $request->codigo)->first();

        if ($datos) {
            User::where('email', $datos->email)->update([
                'password' => bcrypt($request->password)
            ]);

            DB::table('password_resets')->where('token', $request->codigo)->delete();
            return false;
        }

        return false;
    }
}
