<?php

namespace App\Http\Controllers\Subscriber;

use App\Http\Controllers\Controller;
use App\Models\DetailsTaskAssignees;
use App\Models\DetailsTaskAttachments;
use App\Models\SoftworldFiles;
use App\Models\SoftworldPerson;
use App\Models\SoftworldPriority;
use App\Models\SoftworldStatusPlatforms;
use App\Models\SoftworldTask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public function listadoByUser()
    {
        $tasks = DetailsTaskAssignees::getTaskByUser(auth()->user()->id);
        return $tasks;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = SoftworldPerson::getDataByUser(auth()->user()->id);
        $referrers = $data->getReferred;

        $priorities = SoftworldPriority::active('task');
        $status = SoftworldStatusPlatforms::getByModule('task');

        return view('suscriber.template.task.create', compact('priorities', 'referrers', 'status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'end_task' => 'nullable|date',
            'priority' => 'required',
            'referred' => 'required',
            'status' => 'required',
        ];

        if ($request->filled('end_task')) {
            $rules['start_task'] = 'required|date|before_or_equal:end_task';
        }

        $messages = [
            'title.required' => 'Este campo es obligatorio.',
            'start_task.required' => 'El campo de fecha de inicio es obligatorio.',
            'start_task.before_or_equal' => 'La fecha de inicio no debe ser posterior a la fecha de finalización de la tarea.',
            'end_task.date' => 'El campo de fecha de finalización debe ser una fecha válida.',
            'priority.required' => 'El campo de prioridad es obligatorio.',
            'referred.required' => 'El campo de referidos es obligatorio.',
            'status.required' => 'El campo de estado es obligatorio.',
        ];

        $this->validate($request, $rules, $messages);

        $count = SoftworldTask::count() + 1;
        $code = "CNP-TASK" . $count;

        $task = new SoftworldTask();
        $task->code = $code;
        $task->title = $request->title;
        $task->description = $request->description;
        $task->start_task = $request->start_task;
        $task->end_date = $request->end_task;
        $task->priority = $request->priority;
        $task->status = $request->status;
        $task->save();

        if ($request->file("files")) {
            $rules = [
                'files' => 'array',
                "files.*" => "mimes:pdf|max:3000",
            ];

            $messages = [
                'files.*' => 'Solo se permiten archivos PDF',
            ];

            $this->validate($request, $rules, $messages);

            foreach ($request->file("files") as $file) {
                $archive = $file->store("todo", 's3');

                $fileData = new SoftworldFiles();
                $fileData->title = $file->getClientOriginalName();
                $fileData->type = $file->getClientOriginalExtension();
                $fileData->url = $archive;
                $fileData->status = 1;
                $fileData->save();

                DetailsTaskAttachments::create([
                    'task_id' => $code,
                    'file_id' => $fileData->id,
                ]);
            }
        }

        $referreds = explode(",", $request->referred);

        foreach ($referreds as $referred) {
            if ($referred == "for_my") {
                DetailsTaskAssignees::create([
                    'task_id' => $code,
                    'user_id' => auth()->user()->id,
                    'assigned_by' => 1,
                ]);
            } else {
                DetailsTaskAssignees::create([
                    'task_id' => $code,
                    'user_id' => SoftworldPerson::getOnlyUser($referred),
                    'assigned_by' => auth()->user()->id,
                ]);
            }
        }

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $array_assigness = [];

        $task = SoftworldTask::find($id);
        $data = SoftworldPerson::getDataByUser(auth()->user()->id);
        $referrers = $data->getReferred;

        foreach ($task->getTaskAssigneesUser as $assign) {
            $array_assigness[] = $assign->user_id;
        }

        $priorities = SoftworldPriority::active('task');
        $status = SoftworldStatusPlatforms::getByModule('task');

        return view('suscriber.template.task.edit', compact('priorities', 'referrers', 'status', 'task', 'array_assigness'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'title' => 'required',
            'end_task' => 'nullable|date',
            'priority' => 'required',
            'referred' => 'required',
            'status' => 'required',
        ];

        if ($request->filled('end_task')) {
            $rules['start_task'] = 'required|date|before_or_equal:end_task';
        }

        $messages = [
            'title.required' => 'Este campo es obligatorio.',
            'start_task.required' => 'El campo de fecha de inicio es obligatorio.',
            'start_task.before_or_equal' => 'La fecha de inicio no debe ser posterior a la fecha de finalización de la tarea.',
            'end_task.date' => 'El campo de fecha de finalización debe ser una fecha válida.',
            'priority.required' => 'El campo de prioridad es obligatorio.',
            'referred.required' => 'El campo de referidos es obligatorio.',
            'status.required' => 'El campo de estado es obligatorio.',
        ];

        $this->validate($request, $rules, $messages);

        $code = $request->code;

        $task = SoftworldTask::find($code);
        $task->title = $request->title;
        $task->description = $request->description;
        $task->start_task = $request->start_task;
        $task->end_date = $request->end_task;
        $task->priority = $request->priority;
        $task->status = $request->status;
        $task->update();


        if ($request->file("files")) {
            $rules = [
                'files' => 'array',
                "files.*" => "mimes:pdf|max:3000",
            ];

            $messages = [
                'files.*' => 'Solo se permiten archivos PDF',
            ];

            $this->validate($request, $rules, $messages);

            foreach ($request->file("files") as $file) {
                $archive = $file->store("todo", 's3');

                $fileData = new SoftworldFiles();
                $fileData->title = $file->getClientOriginalName();
                $fileData->type = $file->getClientOriginalExtension();
                $fileData->url = $archive;
                $fileData->status = 1;
                $fileData->save();

                DetailsTaskAttachments::create([
                    'task_id' => $code,
                    'file_id' => $fileData->id,
                ]);
            }
        }

        $referreds = explode(",", $request->referred);

        foreach ($referreds as $referred) {
            if ($referred == "for_my") {
                DetailsTaskAssignees::create([
                    'task_id' => $code,
                    'user_id' => auth()->user()->id,
                    'assigned_by' => 1,
                ]);
            } else {
                DetailsTaskAssignees::create([
                    'task_id' => $code,
                    'user_id' => SoftworldPerson::getOnlyUser($referred),
                    'assigned_by' => auth()->user()->id,
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search($data = NULL)
    {
        $tasks = [];

        if ($data) {
            // Buscar tareas asociadas al usuario logueado por título y descripción
            $tasks = DetailsTaskAssignees::where('user_id', auth()->user()->id)
                ->whereHas('getTask', function ($query) use ($data) {
                    $query->where('title', 'like', "%$data%")
                        ->orWhere('description', 'like', "%$data%")
                        ->orWhere('end_date', 'like', "%$data%");
                })->paginate(10);
        } else {
            $tasks = DetailsTaskAssignees::getTaskByUser(auth()->user()->id);
        }

        return view('suscriber.template.task.list', compact('tasks'));
    }
}
