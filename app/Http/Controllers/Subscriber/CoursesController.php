<?php

namespace App\Http\Controllers\Subscriber;

use App\Http\Controllers\Controller;
use App\Models\DetailsCourseComments;
use App\Models\DetailsCoursesSubscriber;
use App\Models\DetailsLessonsComments;
use App\Models\DetailsProgressStudents;
use App\Models\DetailsQualificationCourse;
use App\Models\DetalsLessonQualification;
use App\Models\SoftworldCourses;
use App\Models\SoftworldModulesLessons;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = SoftworldCourses::activeSubscriber();
        return view('suscriber.courses.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        date_default_timezone_set("America/Bogota");

        // Obtiene el curso de la base de datos
        $course = SoftworldCourses::find($id);

        $start_date = DateTime::createFromFormat('Y-m-d H:i:s', $course->start_date);

        if ($start_date !== false) {
            // Formatea la fecha de inicio del curso
            $start_date_formatted = $start_date->format('Y-m-d');

            // Establece el idioma de Carbon a español
            Carbon::setLocale('es');

            // Crea un objeto Carbon a partir del objeto DateTime
            $start_date_carbon = Carbon::instance($start_date);

            // Formatea la fecha en el formato deseado usando Carbon
            $start_date = $start_date_carbon->isoFormat('dddd, DD [de] MMMM [del] YYYY');

            // Convierte la primera letra en mayúscula
            $start_date = ucfirst(mb_convert_case($start_date, MB_CASE_LOWER));

            // Ahora puedes usar $start_date en tu código
        } else {
            $start_date = "";
        }



        $end_date = DateTime::createFromFormat('Y-m-d H:i:s', $course->end_date);

        if ($end_date !== false) {
            $end_date_formatted = $end_date->format('Y-m-d');
            Carbon::setLocale('es');
            $end_date_carbon = Carbon::instance($end_date);
            $end_date = $end_date_carbon->isoFormat('dddd, DD [de] MMMM [del] YYYY');
            $end_date = ucfirst(mb_convert_case($end_date, MB_CASE_LOWER));
        } else {
            $end_date = "";
        }

        return view('suscriber.courses.show', compact('course', 'start_date', 'end_date'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function start(Request $request)
    {
        $rules = [
            'code' => 'required',
        ];

        $messages = [
            'code.required' => 'Este campo es importante.',
        ];

        $this->validate($request, $rules, $messages);

        date_default_timezone_set("America/Bogota");

        $validate_course = DetailsCoursesSubscriber::getValidatedCourse($request->code, auth()->user()->id);

        if (!$validate_course) {
            DetailsCoursesSubscriber::create([
                'course_id' => $request->code,
                'user_id' => auth()->user()->id,
                'qualification' => 0,
                'start_date' => now(),
            ]);

            $course_id = $request->code;
        } else {
            $course_id = $validate_course->course_id;
        }

        $course = SoftworldCourses::find($course_id);
        return view('suscriber.courses.progress', compact('course'));
    }

    public function course($course_id)
    {
        $validate_course = DetailsCoursesSubscriber::getValidatedCourse($course_id, auth()->user()->id);

        if (!$validate_course) {
            DetailsCoursesSubscriber::create([
                'course_id' => $course_id,
                'user_id' => auth()->user()->id,
                'qualification' => 0,
                'start_date' => now(),
            ]);

            $course_id = $course_id;
        } else {
            $course_id = $validate_course->course_id;
        }

        $course = SoftworldCourses::find($course_id);
        return view('suscriber.courses.progress_course', compact('course'));
    }

    public function comments($course_id)
    {
        $course = SoftworldCourses::find($course_id);
        return view('suscriber.courses.comments_course', compact('course'));
    }

    public function commentsStoreCourse(Request $request)
    {
        $rules = [
            'comment' => 'required'
        ];

        $messages = [
            'comment.required' => 'Este campo es importante.',
        ];

        $this->validate($request, $rules, $messages);

        DetailsCourseComments::create([
            'course_id' => $request->course,
            'user_id' => auth()->user()->id,
            'comment' => $request->comment,
        ]);

        return true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lesson($id)
    {
        $array_points = [];

        $lesson = SoftworldModulesLessons::find($id);

        foreach ($lesson->getQualification as $qualification) {
            $array_points[] = $qualification->point;
        }

        $suma = array_sum($array_points);

        if ($suma >= 1) {
            $promedio = $suma / count($array_points);
        } else {
            $promedio = 0;
        }

        return view('suscriber.courses.lesson', compact('lesson', 'promedio'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lessonFinished($id)
    {

        $validate = DetailsProgressStudents::where([['user_id', auth()->user()->id], ['lesson_id', $id]])->count();

        if ($validate == 0) {
            DetailsProgressStudents::create([
                'user_id' => auth()->user()->id,
                'lesson_id' => $id,
                'register_date' => NOW(),
            ]);
        }

        return true;
    }

    public function commentsLesson($lesson_id)
    {
        $lesson = SoftworldModulesLessons::find($lesson_id);
        return view('suscriber.courses.comments_lesson', compact('lesson'));
    }

    public function commentsStoreLesson(Request $request)
    {
        $rules = [
            'comment' => 'required'
        ];

        $messages = [
            'comment.required' => 'Este campo es importante.',
        ];

        $this->validate($request, $rules, $messages);

        DetailsLessonsComments::create([
            'lesson_id' => $request->lesson,
            'user_id' => auth()->user()->id,
            'comment' => $request->comment,
        ]);

        return true;
    }

    public function assessmentLesson($value, $lesson_id)
    {
        $userId = auth()->user()->id;

        // Verificar si existe un registro para la lección y el usuario actual
        $existingRecord = DetalsLessonQualification::where('lesson_id', $lesson_id)
            ->where('user_id', $userId)
            ->first();

        if ($existingRecord) {
            // Si existe un registro, eliminarlo
            $existingRecord->delete();
        }

        // Crear un nuevo registro con el valor proporcionado
        $newRecord = new DetalsLessonQualification();
        $newRecord->lesson_id = $lesson_id;
        $newRecord->user_id = $userId;
        $newRecord->point = $value;
        $newRecord->save();

        return true;
    }
}
