<?php

namespace App\Http\Controllers\Subscriber;

use App\Http\Controllers\Controller;
use App\Models\DetailsCalculatorsMemberships;
use Illuminate\Http\Request;

class CalculatorsController extends Controller
{
    protected $array_calculators = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $calculators = DetailsCalculatorsMemberships::searchByMembership($this->membershipCurrentUser());

        $this->array_calculators = [];

        foreach ($calculators as $calculator) {
            $this->array_calculators[] = $calculator->getCalculator->code;
        }

        return view("suscriber.calculators.index", compact('calculators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $results = "";

        if ($id === "calculadora_indexacion") {
            $results = $this->storeCalculadoraIndexacion(new Request($request->all()));
        }

        return $results;
    }

    public function storeCalculadoraIndexacion(Request $request)
    {
        $rules = [
            'capital' => 'required'
        ];

        $messages = [
            'capital.required' => 'Este campo es importante.',
        ];

        $this->validate($request, $rules, $messages);

        return $request["capital"];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->index();

        if (!empty($this->array_calculators)) {
            if (!in_array($id, $this->array_calculators)) {
                return "forbidden_calculator";
            }

            return view("suscriber.calculators.template.$id");
        } else {
            return "empty_calculator";
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
