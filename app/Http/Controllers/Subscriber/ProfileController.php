<?php

namespace App\Http\Controllers\Subscriber;

use App\Http\Controllers\Controller;
use App\Models\DetailsValidationsUser;
use App\Models\SoftworldPerson;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use PHPMailer\PHPMailer\PHPMailer;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = SoftworldPerson::getDataByUser(auth()->user()->id);
        return view('suscriber.my-profile.index', compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $type)
    {

        if ($type == "user") {
            $rules = [
                'correo_usuario' => 'required|email|required|unique:users,email,' . auth()->user()->id . ',id',
                'name' => 'required|required|unique:users,name,' . auth()->user()->id . ',id',
                'password' => 'nullable|confirmed',
            ];

            $messages = [
                'name.required' => 'Este campo es importante.',
                'name.unique' => 'El nombre del usuario ingresado ya pertenece a otra persona.',
                'correo_usuario.required' => 'Este campo es importante.',
                'correo_usuario.unique' => 'El correo electrónico ingresado ya pertenece a otra persona.',
                //'password.required' => 'Ingrese una contraseña para su usuario.',
                'password.confirmed' => 'Las contraseñas no coinciden.',
            ];

            $this->validate($request, $rules, $messages);

            $user = User::find(auth()->user()->id);

            if ($user->email != $request->correo_usuario) {
                $user->email_verified_at = NULL;
            }

            $user->name = $request->name;
            $user->email = $request->correo_usuario;
            $user->password = Hash::make($request->password);
            $user->update();
        } else if ($type == "professional") {

            $rules = [
                'id' => 'required',
                'first_name' => 'required',
                'first_last_name' => 'required',
                'cellphone' => 'required|unique:softworld_people,cellphone,' . $request->id . ',id',
                'email' => 'required|unique:softworld_people,email,' . $request->id . ',id',
            ];

            $messages = [
                'id.required' => 'Este campo es importante.',
                'first_name.required' => 'Este campo es importante.',
                'first_last_name.required' => 'Este campo es importante.',
                'cellphone.required' => 'Este campo es importante.',
                'cellphone.unique' => 'El celular ingresado ya pertenece a otra persona.',
                'email.required' => 'Este campo es importante.',
                'email.unique' => 'El correo electrónico ya pertenece a otra persona.',
            ];

            $this->validate($request, $rules, $messages);

            $professional = SoftworldPerson::find($request->id);
            $professional->first_name = $request->first_name;
            $professional->second_name = $request->second_name;
            $professional->first_last_name = $request->first_last_name;
            $professional->second_last_name = $request->second_last_name;
            $professional->cellphone = $request->cellphone;
            $professional->email = $request->email;
            $professional->update();
        } else {
            $rules = [
                'image_data' => 'required|mimes:jpg,jpeg,png',
            ];

            $messages = [
                'image_data.required' => 'Seleccione una imagen.',
                'image_data.mimes' => 'Seleccione archivos de imagen: jpg, jpeg, png',
            ];

            $this->validate($request, $rules, $messages);

            $image = $request->file("image_data")->store("usuarios", 's3');

            $user = User::find(auth()->user()->id);
            $user->avatar = $image;
            $user->update();
            return true;
        }

        return true;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validation()
    {

        $usuario = User::find(auth()->user()->id);

        if ($usuario->email_verified_at == NULL) {

            date_default_timezone_set('America/Bogota');

            $MAIL_HOST = config('app.MAIL_HOST');
            $MAIL_PORT = config('app.MAIL_PORT');
            $MAIL_USERNAME = config('app.MAIL_USERNAME');
            $MAIL_PASSWORD = config('app.MAIL_PASSWORD');


            $token = Str::random(50);

            DetailsValidationsUser::create([
                'user_id' => auth()->user()->id,
                'token' => $token,
            ]);

            $link = route("profile-subscriber-validation-token", [$token]);

            $correo = $usuario->email;
            $mail = new PHPMailer();
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
            $mail->IsSMTP(); // enable SMTP
            $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
            $mail->SMTPAuth = true; // authentication enabled
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = $MAIL_HOST;
            $mail->Port = $MAIL_PORT; // or 587
            $mail->IsHTML(true);
            $mail->Username = $MAIL_USERNAME;
            $mail->Password = $MAIL_PASSWORD;
            $mail->SetFrom("no-replay@cnp.com.co");
            $mail->Subject = "[Centro Nacional de Pruebas] Validación de correo electrónico";
            $mail->Body = view('page-layouts.template.email.validation-email', compact('correo', 'link', 'usuario'));
            $mail->AddAddress($correo);
            $mail->send();

            return true;
        }

        return false;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validationConfirmed($token)
    {
        $validation = DetailsValidationsUser::validatedByToken($token);

        if ($validation) {

            date_default_timezone_set("America/Bogota");

            $user = User::find($validation->user_id);
            $user->email_verified_at = NOW();
            $user->update();

            $validation->delete();
            return redirect(route('profile-subscriber'));
        } else {
            $title = "Error de validación";
            $alert = "La página que usted intenta ingresar no existe o ya se encuentra vencida.";
            return view('page-layouts.template.alert.token-invalid', compact('title', 'alert'));
        }

        return $token;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
