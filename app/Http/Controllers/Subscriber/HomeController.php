<?php

namespace App\Http\Controllers\Subscriber;

use App\Http\Controllers\Controller;
use App\Http\Controllers\page\HomeController as PageHomeController;
use App\Models\SoftworldEconomicIndicators;
use App\Models\SoftworldNews;
use App\Models\SoftworldPerson;
use App\Models\SoftworldTask;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        date_default_timezone_set("America/Bogota");

        $dia_hoy = date("Y-m-d");
        $pageConfigs = ['pageHeader' => false];
        $arrayIndicators = [];

        $data_professional = SoftworldPerson::getDataByUser(auth()->user()->id);

        if (!$data_professional) {
            return view("suscriber.template.error.profesional_not_found");
        }

        date_default_timezone_set("America/Bogota");

        $current_time = Carbon::now();

        if ($current_time->hour >= 5 && $current_time->hour < 12) {
            $texto = "Buenos días";
        } elseif ($current_time->hour >= 12 && $current_time->hour < 19) {
            $texto = "Buenas tardes";
        } else {
            $texto = "Buenas noches";
        }

        $current_time = Carbon::now();
        Carbon::setLocale(config('app.locale'));
        setlocale(LC_ALL, 'es_MX', 'es', 'ES', 'es_MX.utf8');


        /* =========== BUSQUEDA DE LOS ÚLTIMOS INDICADORES REGISTRADOS */

        $arrayIndicators = [
            'TRM' => SoftworldEconomicIndicators::getTRM(2),
            'IPC' => SoftworldEconomicIndicators::getIPC(2),
            'UVR' => SoftworldEconomicIndicators::getUVR(2),
            'DTF' => SoftworldEconomicIndicators::getDTF(2),
        ];


        /* =========== BUSQUEDA DE LAS TAREAS ASIGNADAS A MI =========== */

        $controllerTask = new TaskController;
        $tasks = $controllerTask->listadoByUser();

        Carbon::setLocale('es');
        $fecha_hoy =  Carbon::now()->isoFormat('dddd, DD [de] MMMM [del] YYYY');
        $fecha_hoy = ucfirst(mb_convert_case($fecha_hoy, MB_CASE_LOWER));

        
        
        /* =========== BUSQUEDA DE LOS FAVORITOS DEL USUARIO =========== */

        $controller = new NewsController;
        $favorites = $controller->allfavoritesOfUser(auth()->user()->id);

        /* =========== FIN BUSQUEDA DE LOS FAVORITOS DEL USUARIO =========== */


        $controllerNews = new PageHomeController();
        $mostViewedNews = $controllerNews->mostViewedNews();


        return view('suscriber.home', [
            'pageConfigs' => $pageConfigs,
            "data_profesional" => $data_professional,
            "array_indicators" => $arrayIndicators,
            'fecha_hoy' => $fecha_hoy,
            'jornada' => $texto,
            'tasks' => $tasks,
            'favorites' => $favorites,
            'mostViewedNews' => $mostViewedNews,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
