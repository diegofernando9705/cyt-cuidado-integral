<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;
use PHPMailer\PHPMailer\PHPMailer;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;


use Carbon\Carbon;
use App\User;
use App\Mail\PsswordReset;

use Validator;
use DB;

class AuthController extends Controller
{

  /**
   * Create user
   *
   * @param  [string] name
   * @param  [string] email
   * @param  [string] password
   * @param  [string] password_confirmation
   * @return [string] message
   */
  public function register(Request $request)
  {
    $request->validate([
      'name' => 'required|string',
      'email' => 'required|string|email|unique:users',
      'password' => 'required|string|',
      'c_password' => 'required|same:password',
    ]);

    $user = new User([
      'name' => $request->name,
      'email' => $request->email,
      'password' => bcrypt($request->password)
    ]);
    if ($user->save()) {
      return response()->json([
        'message' => 'Successfully created user!'
      ], 201);
    } else {
      return response()->json(['error' => 'Provide proper details']);
    }
  }

  /**
   * Login user and create token
   *
   * @param  [string] email
   * @param  [string] password
   * @param  [boolean] remember_me
   * @return [string] access_token
   * @return [string] token_type
   * @return [string] expires_at
   */
  public function login(Request $request)
  {
    $request->validate([
      'email' => 'required|string|email',
      'password' => 'required|string',
      'remember_me' => 'boolean'
    ]);

    $credentials = request(['email', 'password']);
    if (!Auth::attempt($credentials))
      return response()->json([
        'message' => 'Credenciales incorrectas'
      ], 401);

    $user = $request->user();
    $tokenResult = $user->createToken('Personal Access Token');
    $token = $tokenResult->token;
    if ($request->remember_me)
      $token->expires_at = Carbon::now()->addWeeks(1);
    $token->save();
    return response()->json([
      'access_token' => $tokenResult->accessToken,
      'token_type' => 'Bearer',
      'expires_at' => Carbon::parse(
        $tokenResult->token->expires_at
      )->toDateTimeString()
    ]);
  }

  /**
   * Logout user (Revoke the token)
   *
   * @return [string] message
   */
  public function logout(Request $request)
  {
    $request->user()->token()->revoke();
    return response()->json([
      'message' => 'Successfully logged out'
    ]);
  }

  public function cierre()
  {
    Auth::logout();
    return redirect()->route('login')->with('message', 'State saved correctly!!!');;
  }
  /**
   * Get the authenticated User
   *
   * @return [json] user object
   */
  public function user(Request $request)
  {
    return response()->json($request->user());
  }

  public function envioPassword(Request $request)
  {

    $rules = [
      'email' => 'required|email|'
    ];

    $messages = [
      'email.required' => 'Coloque la nueva contraseña',
      'email.email' => 'Correo electronico no valido.',
    ];

    $this->validate($request, $rules, $messages);


    $usuario = DB::table('users')->where('email', '=', $request->email)->count();


    if ($usuario > 0) {
      date_default_timezone_set('America/Bogota');

      $MAIL_HOST = config('app.MAIL_HOST');
      $MAIL_PORT = config('app.MAIL_PORT');
      $MAIL_USERNAME = config('app.MAIL_USERNAME');
      $MAIL_PASSWORD = config('app.MAIL_PASSWORD');


      $token = Str::random(50);

      DB::table('password_resets')->where('email', $request->email)->delete();

      DB::table('password_resets')->insert([
        'email' => $request->email,
        'token' => $token,
        'created_at' => date('Y-m-d h:i'),
      ]);


      $link = route("reset_password_form", [$token]);

      $correo = $request->email;
      $mail = new PHPMailer();
      $mail->IsSMTP(); // enable SMTP
      $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
      $mail->SMTPAuth = true; // authentication enabled
      $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
      $mail->Host = $MAIL_HOST;
      $mail->Port = $MAIL_PORT; // or 587
      $mail->IsHTML(true);
      $mail->Username = $MAIL_USERNAME;
      $mail->Password = $MAIL_PASSWORD;
      $mail->SetFrom("info@softworldcolombia.com");
      $mail->Subject = "[C&T Cuidado Integral] Restablecer contrasena";
      $mail->Body = view('template.mail', compact('correo', 'link'));
      $mail->AddAddress($correo);
      $mail->send();

      $pageConfigs = ['blankPage' => true];
      return redirect()->route('login', ['pageConfigs' => $pageConfigs, 'status' => 'email-send']);
    } else {
      $pageConfigs = ['blankPage' => true];
      return redirect()->route('login', ['pageConfigs' => $pageConfigs, 'status' => 'email-no-existe']);
    }
  }

  public function form_password($token)
  {

    date_default_timezone_set('America/Bogota');

    $datos = DB::table('password_resets')->where('token', $token)->count();

    if ($datos > 0) {

      $sql_token = DB::table('password_resets')->where('token', $token)->get();

      $fecha = $sql_token['0']->created_at;
      $fecha_hora = explode(" ", $fecha);

      if ($fecha_hora[0] == date('Y-m-d')) {

        $horas_bd = explode(":", $fecha_hora[1]);
        $minutos_registrado_BD = ($horas_bd[0] * 60) + $horas_bd[1];

        $horas_hoy = explode(":", date('h:i'));
        $minutos_hoy = ($horas_hoy[0] * 60) + $horas_hoy[1];

        $minutos_comparacion = $minutos_registrado_BD - $minutos_hoy;
        //devolverá 600 que serán los minutos totales.

        if (abs($minutos_comparacion) > 60) {
          $pageConfigs = ['blankPage' => true];

          return view('/content/miscellaneous/error', ['pageConfigs' => $pageConfigs]);
        } else {
          $pageConfigs = ['blankPage' => true];

          return view('/content/authentication/auth-reset-password-v2', ['pageConfigs' => $pageConfigs, 'token' => $token]);
        }
      } else {
        return "No";
      }
    } else {
      $pageConfigs = ['blankPage' => true];

      return view('/content/miscellaneous/error', ['pageConfigs' => $pageConfigs]);
    }
  }

  public function new_password(Request $request)
  {

    $rules = [
      'password' => 'required|string|',
      'c_password' => 'required|same:password',
    ];

    $messages = [
      'password.required' => 'Coloque la nueva contraseña',
      'password.string' => 'La contraseña debe ser cadena de texto.',
      'c_password.required' => 'Verifique las contraseñas nuevamente.',
      'c_password.same' => 'Las contraseñas no coinciden.',
    ];

    $this->validate($request, $rules, $messages);

    $datos = DB::table('password_resets')->where('token', $request->codigo)->get();

    User::where('email', $datos[0]->email)->update([
      'password' => bcrypt($request->password)
    ]);

    DB::table('password_resets')->where('token', $request->codigo)->delete();
    $pageConfigs = ['blankPage' => true];
    return redirect()->route('login', ['pageConfigs' => $pageConfigs, 'status' => 'true']);
  }
}
