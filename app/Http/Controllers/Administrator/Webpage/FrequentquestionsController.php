<?php

namespace App\Http\Controllers\Administrator\Webpage;

use App\Http\Controllers\Controller;
use App\Models\SoftworldFrequentQuestions;
use App\Models\SoftworldStatusPlatforms;
use Illuminate\Http\Request;

class FrequentquestionsController extends Controller
{
    public function tabla()
    {
        $array_preguntas_frecuentes = [];

        $preguntasfrecuentes = SoftworldFrequentQuestions::active();

        foreach ($preguntasfrecuentes as $preguntafrecuente) {
            $array_preguntas_frecuentes[] = [
                "vacio" => "",
                "codigo_pregunta" => $preguntafrecuente->code,
                "titulo_pregunta" => $preguntafrecuente->question,
                "respuesta_pregunta" => $preguntafrecuente->answer,
                "codigo_estado" => $preguntafrecuente->getStatus->id,
                "nombre_estado" => $preguntafrecuente->getStatus->description,
            ];
        }

        $informacion["data"] = $array_preguntas_frecuentes;

        return json_encode($informacion);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("administrator.webpage.frequent-questions.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("administrator.webpage.frequent-questions.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'titulo_pregunta' => 'required',
            'respuesta_pregunta' => 'required',
        ];

        $messages = [
            'titulo_pregunta.required' => 'Este campo es importante',
            'respuesta_pregunta.required' => 'Este campor es importante',
        ];

        $this->validate($request, $rules, $messages);

        $count = SoftworldFrequentQuestions::all()->count() + 1;
        $codigo_pregunta = "SW-PF" . $count;

        $data = new SoftworldFrequentQuestions();
        $data->code = $codigo_pregunta;
        $data->question = $request->titulo_pregunta;
        $data->answer = $request->respuesta_pregunta;
        $data->status = 1;
        $data->save();

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SoftworldFrequentQuestions::find($id);
        return view("administrator.webpage.frequent-questions.show", compact("data"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = SoftworldFrequentQuestions::find($id);
        $status = SoftworldStatusPlatforms::basic();
        return view("administrator.webpage.frequent-questions.edit", compact("data", "status"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'titulo_pregunta' => 'required',
            'respuesta_pregunta' => 'required',
            'estado_pregunta' => 'required',
        ];

        $messages = [
            'titulo_pregunta.required' => 'Este campo es importante',
            'respuesta_pregunta.required' => 'Este campor es importante',
            'estado_pregunta.required' => 'Este campor es importante',
        ];

        $this->validate($request, $rules, $messages);

        $data = SoftworldFrequentQuestions::find($id);
        $data->question = $request->titulo_pregunta;
        $data->answer = $request->respuesta_pregunta;
        $data->status = $request->estado_pregunta;
        $data->update();

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        PreguntasFrecuentes::where("codigo_pregunta", $id)->update([
            "estado_pregunta" => 2,
        ]);
        return true;
    }
}
