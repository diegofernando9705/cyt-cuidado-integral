<?php

namespace App\Http\Controllers\Administrator\Webpage;

use App\Http\Controllers\Controller;
use App\Models\SoftworldQuoteOpinion;
use Illuminate\Http\Request;

class QuoteOpinionsController extends Controller
{
    public function tabla()
    {
        $array_cotizadores_dictamenes = [];

        $cotizadores = SoftworldQuoteOpinion::active();

        foreach ($cotizadores as $cotizador) {

            $array_cotizadores_dictamenes[] = [
                "vacio" => "",
                "codigo_cotizacion_dictamen" => $cotizador->id,
                "nombres_apellidos" => $cotizador->nombres_apellidos,
                "correo_electronico" => $cotizador->correo_electronico,
                "telefono_celular" => $cotizador->telefono_celular,
                "ciudad" => $cotizador->ciudad,
                "resultado" => $cotizador->resultado_cotizacion,

            ];
        }

        $informacion["data"] = $array_cotizadores_dictamenes;

        return json_encode($informacion);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("administrator.webpage.quote-opinions.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $array_informacion = SoftworldQuoteOpinion::find($id);
        return view("administrator.webpage.quote-opinions.show", compact("array_informacion"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
