<?php

namespace App\Http\Controllers\Administrator\Webpage;

use App\Http\Controllers\Controller;
use App\Models\SoftworldCategory;
use App\Models\SoftworldMembership;
use App\Models\SoftworldNews;
use App\Models\SoftworldStatusPlatforms;
use Illuminate\Http\Request;

class NewsController extends Controller
{

    public function tabla()
    {
        $array_noticias = [];

        $noticias = SoftworldNews::activeAdministrator();

        foreach ($noticias as $noticia) {
            $array_noticias[] = [
                "vacio" => "",
                "codigo_noticia" => $noticia->id,
                "imagen_noticia" => config("app.AWS_BUCKET_URL") . $noticia->image,
                "titulo_noticia" => $noticia->title,
                "fecha_registro" => $noticia->register_date,
                "url_noticia" => $noticia->url,
                "codigo_estado" => $noticia->getStatus->id,
                "nombre_estado" => $noticia->getStatus->description,
            ];
        }

        $informacion["data"] = $array_noticias;

        return json_encode($informacion);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("administrator.webpage.news.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = SoftworldCategory::active();
        $status = SoftworldStatusPlatforms::basic();
        return view("administrator.webpage.news.create", compact("categorias", "status"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'imagen_noticia' => 'required|mimes:jpg,jpeg,png',
            'fecha_registro' => 'required',
            'fecha_terminacion' => 'required|after_or_equal:fecha_registro',
            'titulo_noticia' => 'required',
            'resumen_noticia' => 'required',
            'descripcion_noticia' => 'required',
            'url_noticia' => 'required|unique:softworld_news,url',
            'categorias_noticia' => 'required',
        ];

        $messages = [
            'imagen_noticia.required' => 'Este campo es importante',
            'imagen_noticia.mimes' => 'Seleccione un tipo de archivo válido (jpg,jpeg,png)',

            'fecha_registro.unique' => 'Ya existe una categoria con este titulo',

            'fecha_terminacion.required' => 'Este campo es importante',
            'fecha_terminacion.after_or_equal' => 'No debe ser inferior a la fecha de publicación.',

            'titulo_noticia.required' => 'Este campo es importante',
            'resumen_noticia.required' => 'Este campo es importante',
            'descripcion_noticia.required' => 'Este campo es importante',

            'url_noticia.required' => 'Este campo es importante',
            'url_noticia.unique' => 'La URL ya se encuentra registrada.',

            'categorias_noticia.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        $new = new SoftworldNews();

        $new->title = $request->titulo_noticia;
        $new->summary = $request->resumen_noticia;
        $new->description = $request->descripcion_noticia;
        $new->url = $request->url_noticia;
        $new->type = $request->type;
        $new->register_date = $request->fecha_registro;
        $new->date_end = $request->fecha_terminacion;
        $new->user_creator_id = auth()->user()->id;
        $new->status = 1;

        $file_portada = $request->file("imagen_noticia")->store("noticias", 's3');
        $new->image = $file_portada;

        $new->save();

        $new->category()->sync(explode(",", $request->categorias_noticia));

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SoftworldNews::find($id);
        return view("administrator.webpage.news.show", compact("data"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorias_noticia = [];
        $array_tags = [];

        $data = SoftworldNews::find($id);
        $categorias = SoftworldCategory::active();
        $status = SoftworldStatusPlatforms::basic();

        foreach ($data->category as $resultado) {
            $categorias_noticia[] = $resultado->code;
        }

        return view("administrator.webpage.news.edit", compact("data", "categorias", "categorias_noticia", "status"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'fecha_registro' => 'required',
            'fecha_terminacion' => 'required|after_or_equal:fecha_registro',
            'titulo_noticia' => 'required',
            'resumen_noticia' => 'required',
            'descripcion_noticia' => 'required',
            'url_noticia' => 'required|unique:softworld_news,url,' . $id . ",id",
            'type' => 'required',
            'categorias_noticia' => 'required',
            'estado_noticia' => 'required',
        ];

        $messages = [
            'fecha_registro.unique' => 'Ya existe una categoria con este titulo',

            'fecha_terminacion.required' => 'Este campo es importante',
            'fecha_terminacion.after_or_equal' => 'No debe ser superior a la fecha de publicación.',

            'titulo_noticia.required' => 'Este campo es importante',
            'resumen_noticia.required' => 'Este campo es importante',
            'descripcion_noticia.required' => 'Este campo es importante',

            'url_noticia.required' => 'Este campo es importante',
            'url_noticia.unique' => 'La URL ya se encuentra registrada.',

            'type.required' => 'Este campo es importante',
            'categorias_noticia.required' => 'Este campo es importante',
            'estado_noticia.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        $new = SoftworldNews::find($id);

        $new->title = $request->titulo_noticia;
        $new->summary = $request->resumen_noticia;
        $new->description = $request->descripcion_noticia;
        $new->url = $request->url_noticia;
        $new->type = $request->type;
        $new->register_date = $request->fecha_registro;
        $new->date_end = $request->fecha_terminacion;
        $new->status =  $request->estado_noticia;

        if ($request->hasFile("imagen_noticia")) {
            $rules = [
                'imagen_noticia' => 'required|mimes:jpg,jpeg,png',
            ];

            $messages = [
                'imagen_noticia.required' => 'Este campo es importante',
                'imagen_noticia.mimes' => 'Seleccione un tipo de archivo válido (jpg,jpeg,png)',
            ];

            $this->validate($request, $rules, $messages);

            $file_portada = $request->file("imagen_noticia")->store("noticias", 's3');
            $new->image = $file_portada;
        }
        $new->update();
        
        $new->category()->sync(explode(",", $request->categorias_noticia));

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $new = SoftworldNews::find($id);
        $new->status = 3;
        $new->update();

        return true;
    }

    /* APartado donde se le asignara una noticia a la membresia */
    public function form_membresia_noticia($codigo_noticia)
    {
        $array_membresias_seleccionadas = [];

        $new = SoftworldNews::find($codigo_noticia);
        $membresias = SoftworldMembership::active();
        
        foreach ($new->membership as $membership) {
            $array_membresias_seleccionadas[] = $membership->code;
        }

        return view("administrator.webpage.news.asignar_membresia", compact("membresias", "array_membresias_seleccionadas", "codigo_noticia"));
    }

    public function post_membresia_noticia(Request $request, $codigo_noticia)
    {
        $new = SoftworldNews::find($codigo_noticia);
        $new->membership()->sync(explode(",", $request->membership));
        return true;
    }
}
