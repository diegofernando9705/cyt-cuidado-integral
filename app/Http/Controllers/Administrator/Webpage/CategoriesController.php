<?php

namespace App\Http\Controllers\Administrator\Webpage;

use App\Http\Controllers\Controller;
use App\Models\SoftworldCategory;
use App\Models\SoftworldStatusPlatforms;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function tabla()
    {
        $array_categorias = [];

        $categorias = SoftworldCategory::active();

        foreach ($categorias as $categoria) {
            $array_categorias[] = [
                "vacio" => "",
                "codigo_categoria" => $categoria->code,
                "imagen_categoria" => $categoria->image,
                "titulo_categoria" => $categoria->title,
                "descripcion_categoria" => $categoria->description,
                "codigo_estado" => $categoria->getStatus->id,
                "nombre_estado" => $categoria->getStatus->description,
            ];
        }

        $informacion["data"] = $array_categorias;

        return json_encode($informacion);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("administrator.webpage.category.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("administrator.webpage.category.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'titulo_categoria' => 'required|unique:softworld_categories,title',

        ];

        $messages = [
            'titulo_categoria.required' => 'Este campo es importante',
            'titulo_categoria.unique' => 'Ya existe una categoria con este titulo',
        ];

        $this->validate($request, $rules, $messages);

        $count = SoftworldCategory::count() + 1;
        $code = "SW-CAT" . $count;

        $category = new SoftworldCategory();
        $category->code = $code;
        $category->image = $request->data;
        $category->title = $request->titulo_categoria;
        $category->description = $request->descripcion_categoria;
        $category->status = 1;

        if ($request->hasFile("imagen_categoria")) {
            $rules = [
                'imagen_categoria' => 'mimes:jpg,jpeg,png',
            ];

            $messages = [
                'imagen_categoria.mimes' => 'Seleccione archivos de imagen: jpg, jpeg, png',
            ];

            $this->validate($request, $rules, $messages);

            $category->image = $request->file("imagen_categoria")->store("categorias", 's3');
        }

        $category->save();

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $array_categorias_diarios = [];

        $data = SoftworldCategory::find($id);
        $estados = SoftworldStatusPlatforms::basic();

        return view("administrator.webpage.category.edit", compact("data", "estados"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'titulo_categoria' => 'required|unique:softworld_categories,title,' . $id . ",code",
            'estado_categoria' => 'required',
        ];

        $messages = [
            'titulo_categoria.required' => 'Este campo es importante',
            'titulo_categoria.unique' => 'Ya existe una categoria con este titulo',
            'estado_categoria.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        $category = SoftworldCategory::find($id);
        $category->title = $request->titulo_categoria;
        $category->description = $request->descripcion_categoria;
        $category->status = $request->estado_categoria;

        if ($request->hasFile("imagen_categoria")) {
            $rules = [
                'imagen_categoria' => 'mimes:jpg,jpeg,png',
            ];

            $messages = [
                'imagen_categoria.mimes' => 'Seleccione archivos de imagen: jpg, jpeg, png',
            ];

            $this->validate($request, $rules, $messages);

            $imagen_categoria = $request->file("imagen_categoria")->store("categorias", 's3');
            $category->image = $request->estado_categoria;
        }
        
        $category->update();

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $category = SoftworldCategory::find($id);
        $category->status = 3;
        $category->update();

        return true;
    }
}
