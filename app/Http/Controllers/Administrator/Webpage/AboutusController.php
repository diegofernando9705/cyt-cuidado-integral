<?php

namespace App\Http\Controllers\Administrator\Webpage;

use App\Http\Controllers\Controller;
use App\Models\SoftworldAboutUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class AboutusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function formUpdate()
    {
        $data = SoftworldAboutUs::first();
        return view("administrator.webpage.about-us.form", compact("data"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'titulo_nosotros' => 'required',
        ];

        $messages = [
            'titulo_nosotros.required' => 'Esta información es importante.',
        ];

        $this->validate($request, $rules, $messages);

        SoftworldAboutUs::where("id", 1)->update([
            "title" => $request->titulo_nosotros,
            "description" => $request->body_nosotros,
        ]);

        Session::flash('message', "La información se ha actualizado correctamente.");
        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
