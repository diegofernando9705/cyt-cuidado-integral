<?php

namespace App\Http\Controllers\Administrator\Webpage;

use App\Http\Controllers\Controller;
use App\Models\SoftworldContentImagesHeaders;
use App\Models\SoftworldHeadersTypes;
use Illuminate\Http\Request;

class BannersController extends Controller
{
    protected function banners_general()
    {
        return view("administrator.webpage.banners.index");
    }

    public function banners_general_slug($slug)
    {
        $arrayImages = [];
        $data = SoftworldHeadersTypes::page($slug);
        
        if ($data->type == "text") {
            $data_header = $data->text;
            return view("administrator.webpage.banners.texto", compact("data_header", "data"));
        } else {
            return view("administrator.webpage.banners.banners", compact("data"));
        }
    }

    public function listBannersHeader($slug)
    {
        $arrayImages = [];
        $data = SoftworldHeadersTypes::page($slug);

        foreach ($data->images as $image) {
            $arrayImages[] = [
                "vacio" => "",
                "id" => $image->id,
                "url" => config("app.AWS_BUCKET_URL") . $image->url,
                "title" => $image->title,
            ];
        }

        $informacion["data"] = $arrayImages;

        return json_encode($informacion);
    }

    public function update(Request $request, $id)
    {
        $data = SoftworldHeadersTypes::find($id);

        if ($data->type == "text") {
            $data_header = $data->texto;
            $data_header->title = $request->title;
            $data_header->description = $request->description;
            $data_header->update();
        } else if ($data->type == "banner") {
            $image = new SoftworldContentImagesHeaders();
            $file_banner = $request->file("imagen")->store("banners", 's3');

            $image->title = $request->title;
            $image->description = $request->description;
            $image->url = $file_banner;
            $image->type = $data->type;
            $image->text_button = $request->text_button;
            $image->url_button = $request->link_button;
            $image->page_id = $id;
            $image->status = 1;
            $image->save();
        }

        return true;
    }


    public function updateBannerType($slug, $type)
    {
        $data = SoftworldHeadersTypes::page($slug);
        $data->type = $type;
        $data->save();

        if ($type == "text") {
            $data_header = $data->text;
            return view("administrator.webpage.banners.texto", compact("data_header", "data"));
        } else {
            $data_header = $data->images;
            return view("administrator.webpage.banners.banners", compact("data_header", "data"));
        }
    }

    public function formAddImageToBanner($slug)
    {
        $data = SoftworldHeadersTypes::page($slug);
        return view("administrator.webpage.banners.forms.add-images-to-banner", compact("data"));
    }

    public function formEditImageBanner($code)
    {
        $data = SoftworldContentImagesHeaders::find($code);
        return view("administrator.webpage.banners.forms.edit-images-to-banner", compact("data"));
    }


    public function updateImageBanner(Request $request, $code)
    {
        $rules = [
            'title' => 'required',
            'text_button' => 'required',
            'link_button' => 'required',

        ];

        $messages = [
            'title.required' => 'Este campo es importante',
            'description.unique' => 'Este campo es importante',
            'diario_general.required' => 'Este campo es importante',
            'estado_categoria.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        $data = SoftworldContentImagesHeaders::find($code);

        if ($request->file("imagen")) {
            $rules = [
                'imagen' => 'required|mimes:jpg,jpeg,png',
            ];

            $messages = [
                'imagen.required' => 'Este campo es importante',
                'imagen.mimes' => 'Seleccione archivos de imagenes con extension: jpg, jpeg o png',
            ];

            $this->validate($request, $rules, $messages);

            $file_banner = $request->file("imagen")->store("banners", 's3');
            $data->url = $file_banner;
        }


        $data->title = $request->title;
        $data->description = $request->description;
        $data->text_button = $request->text_button;
        $data->url_button = $request->link_button;
        $data->update();

        return true;
    }

    public function deleteImageBanner($code)
    {
        $data = SoftworldContentImagesHeaders::find($code);
        $data->delete();
        return false;
    }
}
