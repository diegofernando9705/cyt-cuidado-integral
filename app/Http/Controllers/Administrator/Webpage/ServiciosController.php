<?php

namespace App\Http\Controllers\Administrator\Webpage;

use App\Http\Controllers\Controller;
use App\Models\SoftworldServices;
use App\Models\SoftworldStatusPlatforms;
use Illuminate\Http\Request;

class ServiciosController extends Controller
{
    public function tabla()
    {
        $array_servicios = [];

        $servicios = SoftworldServices::active();

        foreach ($servicios as $servicio) {
            $array_servicios[] = [
                "vacio" => "",
                "code_servicios" => $servicio->code,
                "image_service" => $servicio->image,
                "titulo_service" => $servicio->title,
                "resena_service" => substr($servicio->resena, 0, 40) . "(...)",
                "body_service" => $servicio->content,
                "url_unica" => $servicio->url,
                "codigo_estado" => $servicio->getStatus->id,
                "nombre_estado" => $servicio->getStatus->description,
            ];
        }

        $informacion["data"] = $array_servicios;

        return json_encode($informacion);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("administrator.webpage.servicios.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("administrator.webpage.servicios.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'change-picture' => 'required|mimes:jpg,jpeg,png',
            'url_unica' => 'required|unique:softworld_services,url',
            'titulo_service' => 'required',
            'resena_service' => 'required',
            'body_service' => 'required',
        ];

        $messages = [
            'change-picture.required' => 'Seleccione una imagen de portada.',
            'change-picture.mimes' => 'Seleccione un archivo de imagen: jpg, jpeg, png.',
            'url_unica.required' => 'Ingrese el titulo del servicio.',
            'url_unica.unique' => 'La URL ingresada ya se encuentra en la base de datos.',
            'titulo_service.required' => 'Ingrese el titulo del servicio.',
            'resena_service.required' => 'Ingrese la reseña del servicio.',
            'body_service.required' => 'Ingrese la descripción del servicio.',
        ];

        $this->validate($request, $rules, $messages);

        $file_portada = $request->file("change-picture")->store("servicios", 's3');

        $count = SoftworldServices::count() + 1;
        $codigo = "SW-S" . $count;

        SoftworldServices::create([
            'code' => $codigo,
            'image' => $file_portada,
            'title' => $request->titulo_service,
            'resena' => $request->resena_service,
            'content' => $request->body_service,
            'url' => $request->url_unica,
            'status' => 1,
        ]);

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data_services = SoftworldServices::find($id);
        return view("administrator.webpage.servicios.show", compact("data_services"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_services = SoftworldServices::find($id);
        $status = SoftworldStatusPlatforms::basic();

        return view("administrator.webpage.servicios.edit", compact("data_services", "status"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'titulo_service' => 'required',
            'resena_service' => 'required',
            'body_service' => 'required',
            'estado_service' => 'required',
        ];

        $messages = [
            'titulo_service.required' => 'Este campo es importante',
            'resena_service.required' => 'Este campo es importante',
            'body_service.required' => 'Este campo es importante',
            'estado_service.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        $service = SoftworldServices::find($id);

        if ($request->hasFile('change-picture')) {
            $rules = [
                'change-picture' => 'mimes:jpg,jpeg,png',
            ];

            $messages = [
                'change-picture.mimes' => 'El tipo de archivo no es válido. Debe ser: jpg, png, jpeg',
            ];

            $this->validate($request, $rules, $messages);

            $service->image = $request->file("change-picture")->store("servicios", 's3');
        }

        $service->title = $request->titulo_service;
        $service->resena = $request->resena_service;
        $service->content = $request->body_service;
        $service->status = $request->estado_service;
        $service->update();

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $service = SoftworldServices::find($id);
        $service->status = 3;
        $service->update();

        return true;
    }
}
