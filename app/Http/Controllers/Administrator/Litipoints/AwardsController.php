<?php

namespace App\Http\Controllers\Administrator\Litipoints;

use App\Http\Controllers\Controller;
use App\Models\SoftworldAwards;
use App\Models\SoftworldMembership;
use App\Models\SoftworldPartners;
use Illuminate\Http\Request;

class AwardsController extends Controller
{

    public function listado()
    {
        $array_premios = [];

        $premios = SoftworldAwards::active();

        foreach ($premios as $premio) {
            $array_premios[] = [
                "vacio" => "",
                "codigo_premio" => $premio->award_code,
                "titulo_premio" => $premio->award_title,
                "cantidad_litipuntos" => $premio->litipoint_quantity . " litipuntos",
                "disponibles_litipuntos" => $premio->availability,
                "fecha_inicio_premio" => $premio->start_date,
                "fecha_fin_premio" => $premio->end_date,
                "codigo_estado" => $premio->getStatus->id,
                "nombre_estado" => $premio->getStatus->description,
            ];
        }

        $informacion["data"] = $array_premios;

        return json_encode($informacion);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("administrator.litipoints.awards.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $socios = SoftworldPartners::active();
        return view("administrator.litipoints.awards.create", compact("socios"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'imagen_premio' => 'required|mimes:jpg,jpeg,png',
            'start_date' => 'required|date|after_or_equal:today',
            'end_date' => 'required|date|after:start_date',
            'award_title' => 'required',
            'award_description' => 'required',
        ];

        $messages = [
            'imagen_premio.required' => 'Este campo es importante.',
            'imagen_premio.mimes' => 'Por favor seleccione un fórmato de imagen válido.',

            'start_date.required' => 'Este campo es importante.',
            'start_date.date' => 'Formato de fecha incorrecto.',
            'start_date.after_or_equal' => 'La fecha de inicio no debe ser inferior a la actual.',

            'end_date.required' => 'Este campo es importante.',
            'end_date.date' => 'Formato de fecha incorrecto.',
            'end_date.after' => 'La fecha de fin debe ser superior a la fecha inicial.',

            'award_title.required' => 'Este campo es importante.',
            'award_description.required' => 'Este campo es importante.',
        ];

        $this->validate($request, $rules, $messages);

        $count = SoftworldAwards::count() + 1;
        $code = "CNP-AW" . $count;

        $image = $request->file("imagen_premio")->store("awards", 's3');

        $award = new SoftworldAwards();
        $award->award_code = $code;
        $award->award_image = $image;
        $award->award_title = $request->award_title;
        $award->award_description = $request->award_description;
        $award->litipoint_quantity = $request->litipoint_quantity;
        $award->availability = $request->availability;
        $award->start_date = $request->start_date;
        $award->end_date = $request->end_date;
        $award->status = 1;

        if ($request->partner) {
        }

        $award->save();

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SoftworldAwards::find($id);
        return view("administrator.litipoints.awards.show", compact("data"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = SoftworldAwards::find($id);
        $socios = SoftworldPartners::active();
        $detalles_socios_premios =  $data->partners;

        $array_socios_premios = [];
        foreach ($detalles_socios_premios as $socio) {
            $array_socios_premios[] = $socio->partner_code;
        }

        return view("administrator.litipoints.awards.edit", compact("socios", "array_socios_premios", "data"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'award_title' => 'required',
            'award_description' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'start_date.required' => 'Este campo es importante.',
            'start_date.date' => 'Formato de fecha incorrecto.',
            'start_date.after_or_equal' => 'La fecha de inicio no debe ser inferior a la actual.',

            'end_date.required' => 'Este campo es importante.',
            'end_date.date' => 'Formato de fecha incorrecto.',
            'end_date.after' => 'La fecha de fin debe ser superior a la fecha inicial.',

            'award_title.required' => 'Este campo es importante.',
            'award_description.required' => 'Este campo es importante.',
            'status.required' => 'Este campo es importante.',
        ];

        $this->validate($request, $rules, $messages);

        $award = SoftworldAwards::find($id);
        $award->award_title = $request->award_title;
        $award->award_description = $request->award_description;
        $award->litipoint_quantity = $request->litipoint_quantity;
        $award->availability = $request->availability;
        $award->end_date = $request->end_date;
        $award->status = $request->status;

        if ($request->hasFile("imagen_premio")) {
            $rules = [
                'imagen_premio' => 'required|mimes:jpg,jpeg,png',
            ];

            $messages = [
                'imagen_premio.required' => 'Este campo es importante',
                'imagen_premio.mimes' => 'Seleccione un tipo de archivo válido (jpg,jpeg,png)',
            ];

            $this->validate($request, $rules, $messages);

            $award->award_image = $request->file("imagen_premio")->store("awards", 's3');
        }

        if ($request->partner) {
            $award->partners()->sync(explode(",", $request->partner));
        }

        $award->update();

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        date_default_timezone_set("America/Bogota");

        $award = SoftworldAwards::find($id);
        $award->date_deleted = now();
        $award->deleted_by = auth()->user()->id;
        $award->update();

        return true;
    }


    public function form_membresia_premio($codigo_premio)
    {
        $array_membresias_seleccionadas = [];

        $membresias = SoftworldMembership::active();
        $award = SoftworldAwards::find($codigo_premio);

        foreach ($award->membership as $data) {
            $array_membresias_seleccionadas[] = $data->code;
        }

        return view("administrator.litipoints.awards.form-membership", compact("membresias", "array_membresias_seleccionadas", "codigo_premio"));
    }


    public function updateMembershipAward(Request $request, $id)
    {
        $award = SoftworldAwards::find($id);
        $award->membership()->sync(explode(",", $request->membership));
        
        return true;
    }
}
