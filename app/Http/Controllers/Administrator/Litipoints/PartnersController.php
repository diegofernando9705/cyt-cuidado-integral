<?php

namespace App\Http\Controllers\Administrator\Litipoints;

use App\Http\Controllers\Controller;
use App\Models\SoftworldPartners;
use App\Models\SoftworldStatusPlatforms;
use Illuminate\Http\Request;

class PartnersController extends Controller
{

    public function listado()
    {
        $array_socios = [];

        $socios = SoftworldPartners::active();

        foreach ($socios as $socio) {
            $array_socios[] = [
                "vacio" => "",
                "partner_code" => $socio->partner_code,
                "partner_title" => $socio->partner_title,
                "person_contact_names" => $socio->person_contact_names,
                "person_contact_last_names" => $socio->person_contact_last_names,
                "codigo_estado" => $socio->getStatus->id,
                "nombre_estado" => $socio->getStatus->description,
            ];
        }

        $informacion["data"] = $array_socios;

        return json_encode($informacion);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("administrator.litipoints.partners.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("administrator.litipoints.partners.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'logo_socio' => 'required|mimes:jpg,jpeg,png',
            'partner_title' => 'required',
            'person_contact_names' => 'required',
            'person_contact_last_names' => 'required',
            'person_contact_cellphone' => 'required',
            'person_contact_email' => 'required|email',
        ];

        $messages = [
            'logo_socio.required' => 'Este campo es importante.',
            'logo_socio.mimes' => 'Por favor seleccione un fórmato de imagen válido.',

            'person_contact_names.required' => 'Este campo es importante.',
            'person_contact_last_names.required' => 'Este campo es importante.',
            'person_contact_cellphone.required' => 'Este campo es importante.',
            'person_contact_email.required' => 'Este campo es importante.',
            'person_contact_email.email' => 'Ingrese un correo electrónico válido.',
        ];

        $this->validate($request, $rules, $messages);

        $count = SoftworldPartners::count() + 1;
        $code = "CNP-PARTNER" . $count;
    
        $image = $request->file("logo_socio")->store("partners", 's3');

        $partner = new SoftworldPartners();
        $partner->partner_code = $code;
        $partner->partner_image = $image;
        $partner->partner_title = $request->partner_title;
        $partner->partner_description = $request->partner_description;
        $partner->person_contact_names = $request->person_contact_names;
        $partner->person_contact_last_names = $request->person_contact_last_names;
        $partner->person_contact_cellphone = $request->person_contact_cellphone;
        $partner->person_contact_email = $request->person_contact_email;
        $partner->status = 1;
        $partner->save();

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SoftworldPartners::find($id);
        return view("administrator.litipoints.partners.show", compact("data"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = SoftworldPartners::find($id);
        $status = SoftworldStatusPlatforms::basic();
        return view("administrator.litipoints.partners.edit", compact("data", "status"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'partner_title' => 'required',
            'person_contact_names' => 'required',
            'person_contact_last_names' => 'required',
            'person_contact_cellphone' => 'required',
            'person_contact_email' => 'required|email',
        ];

        $messages = [
            'person_contact_names.required' => 'Este campo es importante.',
            'person_contact_last_names.required' => 'Este campo es importante.',
            'person_contact_cellphone.required' => 'Este campo es importante.',
            'person_contact_email.required' => 'Este campo es importante.',
            'person_contact_email.email' => 'Ingrese un correo electrónico válido.',
        ];

        $this->validate($request, $rules, $messages);


        $partner = SoftworldPartners::find($id);
        
        $partner->partner_title = $request->partner_title;
        $partner->partner_description = $request->partner_description;
        $partner->person_contact_names = $request->person_contact_names;
        $partner->person_contact_last_names = $request->person_contact_last_names;
        $partner->person_contact_cellphone = $request->person_contact_cellphone;
        $partner->person_contact_email = $request->person_contact_email;
       

        if($request->hasFile("logo_socio")){
            $rules = [
                'logo_socio' => 'required|mimes:jpg,jpeg,png',
            ];
    
            $messages = [
                'logo_socio.required' => 'Este campo es importante.',
                'logo_socio.mimes' => 'Por favor seleccione un fórmato de imagen válido.',
            ];
    
            $this->validate($request, $rules, $messages);

            $image = $request->file("logo_socio")->store("partners", 's3');
            $partner->partner_image = $image;
        }

        $partner->status = 1;
        $partner->update();

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        date_default_timezone_set("America/Bogota");

        $partner = SoftworldPartners::find($id);
        $partner->date_deleted = now();
        $partner->deleted_by = auth()->user()->id;
        $partner->status = false;
        $partner->update();

        return true;
    }
}
