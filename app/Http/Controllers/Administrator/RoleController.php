<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\softworld_modulos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function listado()
    {
        return Role::orderBy('created_at', 'ASC')->paginate(20);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::simplePaginate(2);
        return view('administrator.administrador.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permisos = Permission::all();
        $modulos = softworld_modulos::all();

        return view('administrator.administrador.roles.create', compact('permisos', 'modulos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $rules = [
            'nombre_rol' => 'required|unique:roles,name',
        ];

        $messages = [
            'nombre_rol.required'       => 'El nombre del Rol es importante.',
            'nombre_rol.unique'       => 'El nombre del Rol debe ser unico.',
        ];

        $this->validate($request, $rules, $messages);

        $rol = Role::create([
            'name' => $request->nombre_rol
        ]);

        if ($request->permisos) {
            foreach ($request->permisos as $value) {
                $rol->givePermissionTo($value);
            }
        }

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permisos_asignados = [];

        $permisos = Permission::all();
        $modulos = softworld_modulos::all();

        $sql_permisos_asignados = DB::table('role_has_permissions')
            ->select('permissions.*', 'role_has_permissions.*')
            ->join('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
            ->where('role_id', $id)->get();

        foreach ($sql_permisos_asignados as $value) {
            $permisos_asignados[] = $value->name;
        }

        $rol = Role::where('id', $id)->get();
        return view('administrator.administrador.roles.show', compact('rol', 'permisos', 'modulos', 'permisos_asignados'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permisos_asignados = [];

        $permisos = Permission::all();
        $modulos = softworld_modulos::all();

        $sql_permisos_asignados = DB::table('role_has_permissions')
            ->select('permissions.*', 'role_has_permissions.*')
            ->join('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
            ->where('role_id', $id)->get();

        foreach ($sql_permisos_asignados as $value) {
            $permisos_asignados[] = $value->name;
        }

        $rol = Role::where('id', $id)->get();
        return view('administrator.administrador.roles.edit', compact('rol', 'permisos', 'modulos', 'permisos_asignados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'nombre_rol' => 'required',
        ];

        $messages = [
            'nombre_rol.required' => 'El nombre del Rol es importante.',
        ];

        $this->validate($request, $rules, $messages);

        Role::where('id', $request->codigo)->update([
            'name' => $request->nombre_rol
        ]);

        DB::table('role_has_permissions')->where('role_id', $request->codigo)->delete();

        $roles = Role::where('id', $request->codigo)->get();

        foreach ($roles as $value) {
            $rol = $value;
        }

        if ($request->permisos) {
            foreach ($request->permisos as $value) {
                $rol->givePermissionTo($value);
            }
        }

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
