<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\SoftworldPerson;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function listado()
    {
        return User::where([["email", "<>", "info@softworldcolombia.com"], ["condicion", "<>", 3]])->paginate();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('administrator.administrador.usuarios.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $array_profesionales = [];
        $roles = Role::whereNotIn("name", ["administrador", "subscriber"])->get();
        $profesionales = SoftworldPerson::getUsersOnlyNotRegisterUser();

        return view('administrator.administrador.usuarios.create', compact("roles", "profesionales"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre_usuario' => 'required',
            'correo_usuario' => 'required|email',
            'password' => 'required|required_with:confirmar_password|same:confirmar_password',
            'confirmar_password' => 'required',
            'rol_usuario' => 'required',
            'persona_asignada' => 'required',
        ];

        $messages = [
            'nombre_usuario.required' => 'El nombre del usuario es importante.',
            'correo_usuario.required' => 'El correo del usuario es importante.',
            'correo_usuario.required' => 'El correo del usuario ya existe.',
            'persona_asignada.required' => 'Debe seleccionar una persona para este usuario.',
            'password.required_with' => 'Las contraseñas no coinciden.',
            'password.same' => 'Las contraseñas no coinciden.',
        ];

        $this->validate($request, $rules, $messages);

        if ($request->file('change-picture')) {
            $foto = $request->file('change-picture')->store("Usuarios/" . $request->persona_asignada, 's3');
        } else {
            $foto = 'vacio';
        }

        $usuario = User::create([
            'avatar' => $foto,
            'name' => $request->nombre_usuario,
            'email' => $request->correo_usuario,
            'password' => Hash::make($request->password),
            'condicion' =>  1,
        ])->assignRole($request->rol_usuario);

        $data_profesional = SoftworldPerson::find($request->persona_asignada);
        $data_profesional->user = $usuario->id;
        $data_profesional->update();

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::where("id", "<>", 1)->get();
        $usuario = DB::table('model_has_roles')
            ->select('roles.*', 'model_has_roles.*', 'users.*', 'users.id AS id_usuario', 'roles.id AS id_rol', 'roles.name AS nombre_rol')
            ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
            ->join('users', 'model_has_roles.model_id', '=', 'users.id')
            ->where('users.id', $id)
            ->get();

        User::where('id', $id)->get();

        return view('administrator.administrador.usuarios.edit', compact('usuario', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'nombre_usuario' => 'required',
            'correo_usuario' => 'required|unique:users,email,' . $request->id_usuario, ',id',
            'password' => 'required_with:confirmar_password|same:confirmar_password',
            'rol_usuario' => 'required',
            'estado_usuario' => 'required',
        ];

        $messages = [
            'nombre_usuario.required'       => 'El nombre del usuario es importante.',
            'nombre_usuario.unique'       => 'El nombre del usuario ya esta registrado.',

            'correo_usuario.required'       => 'El correo electronico es importante.',
            'correo_usuario.unique'       => 'El correo electronico ya se encuentra registrado.',

            'password.required_with'       => 'Las contraseñas no coinciden.',
            'password.same'       => 'Las contraseñas no coinciden.',
        ];

        $this->validate($request, $rules, $messages);

        DB::table('model_has_roles')->where('model_id', $request->id_usuario)->delete();

        DB::table('model_has_roles')->insert(['model_id' => $request->id_usuario, 'model_type' => 'App\User', 'role_id' => $request->rol_usuario]);

        if ($request->password) {
            User::where('id', $request->id_usuario)->update([
                'name' => $request->nombre_usuario,
                'email' => $request->correo_usuario,
                'password' => Hash::make($request->password),
                'condicion' => $request->estado_usuario,
            ]);
        } else {
            User::where('id', $request->id_usuario)->update([
                'name' => $request->nombre_usuario,
                'email' => $request->correo_usuario,
                'condicion' => $request->estado_usuario,
            ]);
        }

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->condicion = 3;
        $user->update();

        return true;
    }
}
