<?php

namespace App\Http\Controllers\Administrator\Administrative;

use App\Http\Controllers\Controller;
use App\Models\DetailsFilesConcepts;
use App\Models\SoftworldConcepts;
use App\Models\SoftworldFiles;
use App\Models\SoftworldMembership;
use App\Models\SoftworldPerson;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

class ConceptsController extends Controller
{
    public function listado()
    {
        $array_conceptos = [];

        $conceptos = SoftworldConcepts::activeAdministrative();

        foreach ($conceptos as $concepto) {
            $array_conceptos[] = [
                "vacio" => "",
                "codigo_concepto" => $concepto->code,
                "titulo_concepto" => $concepto->title,
                "fecha_registro" => $concepto->register_date,
                "fecha_caducidad" => $concepto->end_date,
                "usuario_creador_concepto" => $concepto->user->name,
                "codigo_estado" => $concepto->getStatus->id,
                "nombre_estado" => $concepto->getStatus->description,
            ];
        }

        $informacion["data"] = $array_conceptos;

        return json_encode($informacion);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("administrator.administrative.concepts.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $humanTalent = SoftworldPerson::getUsersOnly();
        return view("administrator.administrative.concepts.create", compact('humanTalent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'register_date' => 'required',
            'privacy' => 'required',
        ];

        $messages = [
            'title.unique' => 'Este campo es importante',
            'description.unique' => 'Este campo es importante',
            'register_date.required' => 'Este campo es importante',
            'privacy.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        if ($request->file("archivos")) {
            $rules = [
                'archivos' => 'array',
                "archivos.*" => "mimes:pdf|max:3000",
            ];

            $messages = [
                'archivos.*' => 'Solo se permiten archivos PDF',
            ];

            $this->validate($request, $rules, $messages);
        }

        $count = SoftworldConcepts::all()->count() + 1;
        $code = "CNP-CPTO" . $count;

        $concept = new SoftworldConcepts();
        $concept->code = $code;
        $concept->title = $request->title;
        $concept->description = $request->description;
        $concept->register_date = $request->register_date;
        $concept->end_date = $request->end_date;
        $concept->privacy = $request->privacy;
        $concept->application_concept_id = $request->application_concept;
        $concept->status = 1;
        $concept->created_by = auth()->user()->id;
        $concept->save();

        if ($request->file("archivos")) {
            foreach ($request->file("archivos") as $file) {
                $archive = $file->store("conceptos", 's3');

                $fileData = new SoftworldFiles();
                $fileData->title = $file->getClientOriginalName();
                $fileData->type = $file->getClientOriginalExtension();
                $fileData->url = $archive;
                $fileData->status = 1;
                $fileData->save();

                DetailsFilesConcepts::create([
                    'file_id' => $fileData->id,
                    'concepts_id' => $code,
                ]);
            }
        }

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $concept = SoftworldConcepts::find($id);
        return view("administrator.administrative.concepts.show", compact('concept'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pdf($id)
    {
        $array_respuestas = [];

        $data = SoftworldConcepts::find($id);

        $pdf = Pdf::loadView("administrator.administrative.concepts.pdf",  compact("data"));

        return $pdf->download('concepto-' . $id . '.pdf');
    }

    public function formMembresia($id)
    {
        $array_membresias = [];

        $membresias = SoftworldMembership::activePlatform();
        $concept = SoftworldConcepts::find($id);

        foreach ($concept->memberships as $data) {
            $array_membresias[] = $data->code;
        }

        return view("administrator.administrative.concepts.memberships", compact('membresias', 'array_membresias', 'id'));
    }

    public function postMembresia(Request $request, $id)
    {
        $concept = SoftworldConcepts::find($id);
        $concept->memberships()->sync(explode(",", $request->membership));

        return true;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        date_default_timezone_set("America/Bogota");

        $concept = SoftworldConcepts::find($id);
        $concept->date_deleted = now();
        $concept->deleted_by = auth()->user()->id;
        $concept->update();

        return true;
    }
}
