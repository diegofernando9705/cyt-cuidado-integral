<?php

namespace App\Http\Controllers\Administrator\Administrative;

use App\Http\Controllers\Controller;
use App\Models\SoftworldMembership;
use App\Models\SoftworldStatusPlatforms;
use Illuminate\Http\Request;

class MembershipController extends Controller
{
    public function listado()
    {
        $array_membresias = [];

        $membresias = SoftworldMembership::activePlatform();

        foreach ($membresias as $membresia) {
            $array_membresias[] = [
                "vacio" => "",
                "codigo_membresia" => $membresia->code,
                "imagen_membresia" => $membresia->image,
                "titulo_membresia" => $membresia->title,
                "fecha_inicio" => $membresia->start_date,
                "fecha_fin" => $membresia->end_date,
                "codigo_estado" => $membresia->getStatus->id,
                "nombre_estado" => $membresia->getStatus->description,
            ];
        }

        $informacion["data"] = $array_membresias;

        return json_encode($informacion);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("administrator.administrative.membership.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $membership = SoftworldMembership::find($id);
        return view("administrator.administrative.membership.show", compact('membership'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $membership = SoftworldMembership::find($id);
        $estados = SoftworldStatusPlatforms::basic();
        return view("administrator.administrative.membership.edit", compact('membership', 'estados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'start_date' => 'required',
            'end_date' => 'nullable|after_or_equal:start_date',
            'title' => 'required|unique:softworld_memberships,title,' . $request->code . ',code',
            'description' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'start_date.unique' => 'Ya existe una categoria con este titulo',

            'end_date.required' => 'Este campo es importante',
            'end_date.after_or_equal' => 'No debe ser superior a la fecha de publicación.',

            'title.required' => 'Este campo es importante',
            'title.unique' => 'Ya hay una membresia con este nombre.',
            'description.required' => 'Este campo es importante',

            'status.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        $membership = SoftworldMembership::find($request->code);


        if ($request->hasFile('image')) {
            $rules = [
                'image' => 'mimes:jpg,jpeg,png',
            ];

            $messages = [
                'image.mimes' => 'Seleccione archivos de imagen: jpg, jpeg, png',
            ];

            $this->validate($request, $rules, $messages);

            $image = $request->file("image")->store("membresias", 's3');
            $membership->image = $image;
        }

        $membership->title = $request->title;
        $membership->description = $request->description;
        $membership->start_date = $request->start_date;
        $membership->end_date = $request->end_date;
        $membership->status = $request->status;
        $membership->update();

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        date_default_timezone_set("America/Bogota");

        $people = SoftworldMembership::find($id);
        $people->date_deleted = now();
        $people->deleted_by = auth()->user()->id;
        $people->update();

        return true;
    }
}
