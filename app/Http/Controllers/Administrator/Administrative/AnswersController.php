<?php

namespace App\Http\Controllers\Administrator\Administrative;

use App\Http\Controllers\Controller;
use App\Models\DetailsAnswersData;
use App\Models\SoftworldAnswers;
use App\Models\SoftworldAwards;
use App\Models\SoftworldConcepts;
use Illuminate\Http\Request;

class AnswersController extends Controller
{
    public function listado($code)
    {
        $concept = SoftworldConcepts::find($code);

        if ($concept) {
            $all_answers = $concept->answers;
        }

        return view("administrator.administrative.answers.listado", compact('all_answers'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'descripcion_respuesta' => 'required',
            'data' => 'required',
        ];

        $messages = [
            'descripcion_respuesta.required' => 'Este campo es importante',
            'data.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        date_default_timezone_set("America/Bogota");

        $answer = new SoftworldAnswers();
        $answer->description = $request->descripcion_respuesta;
        $answer->created_by = auth()->user()->name;
        $answer->save();

        $details = new DetailsAnswersData();
        $details->answer_id = $answer->id;
        $details->data_id = $request->data;
        $details->save();

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        date_default_timezone_set("America/Bogota");

        $answer = SoftworldAnswers::find($id);
        $answer->date_deleted = now();
        $answer->deleted_by = auth()->user()->id;
        $answer->update();
        
        return true;
    }
}
