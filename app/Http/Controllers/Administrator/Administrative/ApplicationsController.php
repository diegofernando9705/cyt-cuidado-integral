<?php

namespace App\Http\Controllers\Administrator\Administrative;

use App\Http\Controllers\Controller;
use App\Models\SoftworldApplicationsConcepts;
use App\Models\SoftworldRequestsConcepts;
use Illuminate\Http\Request;

class ApplicationsController extends Controller
{
    public function listado()
    {
        $array_solicitudes = [];
        $solicitudes_concepto = SoftworldRequestsConcepts::active();

        foreach ($solicitudes_concepto as $solicitud) {
            $array_solicitudes[] = [
                "vacio" => "",
                "codigo_solicitud" => $solicitud->code,
                "titulo_solicitud" => $solicitud->title,
                "fecha_solicitud" => $solicitud->date_register,
                "usuario_solicitud" => $solicitud->getUser->name,
                "codigo_estado" => $solicitud->getStatus->id,
                "nombre_estado" => $solicitud->getStatus->description,
            ];
        }

        $informacion["data"] = $array_solicitudes;

        return json_encode($informacion);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("administrator.administrative.application-concepts.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
