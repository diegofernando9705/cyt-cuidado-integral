<?php

namespace App\Http\Controllers\Administrator\Administrative;

use App\Http\Controllers\Controller;
use App\Models\SoftworldAppointmentSheduling;
use App\Models\SoftworldPerson;
use App\Models\SoftworldStatusPlatforms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SchedulingAppointmentsController extends Controller
{
    public function listado()
    {
        $array_agendamientos = [];

        $agendamientos = SoftworldAppointmentSheduling::active();

        foreach ($agendamientos as $agendamiento) {
            $array_agendamientos[] = [
                "vacio" => "",
                "code" => $agendamiento->code,
                "names" => $agendamiento->names,
                "last_names" => $agendamiento->last_names,
                "date" => $agendamiento->date . " | " . $agendamiento->start_hour . " - " . $agendamiento->end_hour,
                "codigo_estado" => $agendamiento->getStatus->id,
                "nombre_estado" => $agendamiento->getStatus->description,
            ];
        }

        $informacion["data"] = $array_agendamientos;

        return json_encode($informacion);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("administrator.administrative.scheduling-appointments.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agendamiento = SoftworldAppointmentSheduling::find($id);
        return view("administrator.administrative.scheduling-appointments.show", compact("agendamiento"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agendamiento = SoftworldAppointmentSheduling::find($id);
        $profesionales = SoftworldPerson::getUsersOnly();
        $status = SoftworldStatusPlatforms::schedulingAppointments();

        return view("administrator.administrative.scheduling-appointments.edit", compact("agendamiento", "profesionales", "status"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'date' => 'required|date|after_or_equal:today',
            'start_hour' => 'required|date_format:H:i',
            'end_hour' => 'required|date_format:H:i|after:start_hour',
            'profesional_id' => 'required',
        ];

        $messages = [
            'date.required' => 'El campo fecha seleccionada es requerido.',
            'date.date' => 'El campo fecha seleccionada debe ser una fecha válida.',
            'date.after_or_equal' => 'El campo fecha seleccionada debe ser igual o superior a la fecha actual.',
            'start_hour.required' => 'El campo hora inicial es requerido.',
            'start_hour.date_format' => 'El campo hora inicial debe tener el formato HH:MM.',
            'end_hour.required' => 'El campo hora final es requerido.',
            'end_hour.date_format' => 'El campo hora final debe tener el formato HH:MM.',
            'end_hour.after' => 'El campo hora final debe ser posterior a la hora inicial.',
            'profesional_id.required' => 'Seleccione un profesional.',
        ];

        $this->validate($request, $rules, $messages);

        $start_hour = $request->start_hour;
        $end_hour = $request->end_hour;

        $existingAppointments = SoftworldAppointmentSheduling::where([['date', $request->date], ['profesional_id', $request->profesional_id]])
            ->where('code', '!=', $request->code)
            ->where(function ($query) use ($start_hour, $end_hour) {
                $query->whereBetween('start_hour', [$start_hour, $end_hour])
                    ->orWhereBetween('end_hour', [$start_hour, $end_hour]);
            })
            ->where("status", 4)
            ->where([["date_deleted", NULL], ["deleted_by", NULL]])
            ->exists();

        if ($existingAppointments) {
            return "preview_register";
        }

        $appointment = SoftworldAppointmentSheduling::find($request->code);
        $appointment->date = $request->date;
        $appointment->start_hour = $request->start_hour;
        $appointment->end_hour = $request->end_hour;
        $appointment->profesional_id = $request->profesional_id;
        $appointment->status = $request->status;
        $appointment->update();

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        date_default_timezone_set("America/Bogota");

        $agendamiento = SoftworldAppointmentSheduling::find($id);
        $agendamiento->date_deleted = now();
        $agendamiento->deleted_by = auth()->user()->id;
        $agendamiento->update();

        return true;
    }
}
