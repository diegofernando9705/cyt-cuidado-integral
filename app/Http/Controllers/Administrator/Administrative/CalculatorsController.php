<?php

namespace App\Http\Controllers\Administrator\Administrative;

use App\Http\Controllers\Controller;
use App\Models\SoftworldCalculators;
use App\Models\SoftworldMembership;
use App\Models\SoftworldStatusPlatforms;
use Illuminate\Http\Request;

class CalculatorsController extends Controller
{
    public function listado()
    {
        $array_calculadoras = [];

        $calculadoras = SoftworldCalculators::activePlatform();

        foreach ($calculadoras as $calculadora) {
            $array_calculadoras[] = [
                "vacio" => "",
                "pk_calculadora" => $calculadora->code,
                "imagen_calculadora" => $calculadora->image,
                "titulo_calculadora" => $calculadora->title,
                "descripcion_calculadora" => $calculadora->description,
                "codigo_estado" => $calculadora->getStatus->id,
                "nombre_estado" => $calculadora->getStatus->description,
            ];
        }

        $informacion["data"] = $array_calculadoras;

        return json_encode($informacion);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("administrator.administrative.calculators.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $array_membresias = [];
        $calculator = SoftworldCalculators::find($id);
        $membresias = SoftworldMembership::activePlatform();
        $estados = SoftworldStatusPlatforms::basic();

        foreach ($calculator->memberships as $data) {
            $array_membresias[] =  $data->code;
        }


        return view("administrator.administrative.calculators.edit", compact('calculator', 'estados', 'membresias', 'array_membresias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'title' => 'required|unique:softworld_calculators,title,' . $request->code . ',code',
            'status' => 'required',
        ];

        $messages = [
            'title.required' => 'Este campo es importante.',
            'title.unique' => 'Ya existe una calculadora con este titulo.',
            'status.required' =>  'Este campo es importante.',
        ];

        $this->validate($request, $rules, $messages);

        $calculator = SoftworldCalculators::find($request->code);

        if ($request->hasFile('image')) {
            $rules = [
                'image' => 'mimes:jpg,jpeg,png',
            ];

            $messages = [
                'image.mimes' => 'Seleccione archivos de imagen: jpg, jpeg, png',
            ];

            $this->validate($request, $rules, $messages);

            $image = $request->file("image")->store("calculadoras", 's3');
            $calculator->image = $image;
        }

        $calculator->title = $request->title;
        $calculator->description = $request->description;
        $calculator->status = $request->status;
        $calculator->update();

        if ($request->membresias) {
            $calculator->memberships()->sync(explode(",", $request->membresias));
        }

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        date_default_timezone_set("America/Bogota");

        $calculator = SoftworldCalculators::find($id);
        $calculator->date_deleted = now();
        $calculator->deleted_by = auth()->user()->id;
        $calculator->update();

        return true;
    }
}
