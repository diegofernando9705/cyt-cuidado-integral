<?php

namespace App\Http\Controllers\Administrator\Administrative;

use App\Http\Controllers\Controller;
use App\Models\DetailsCoursesMemberships;
use App\Models\DetailsFilesCourses;
use App\Models\DetailsFilesLessons;
use App\Models\DetailsLessonsModules;
use App\Models\DetailsModulesCourses;
use App\Models\SoftworldCategory;
use App\Models\SoftworldCourses;
use App\Models\SoftworldCoursesModules;
use App\Models\SoftworldFiles;
use App\Models\SoftworldLevels;
use App\Models\SoftworldMembership;
use App\Models\SoftworldModulesLessons;
use App\Models\SoftworldPerson;
use App\Models\SoftworldStatusPlatforms;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    public function listado()
    {
        $array_cursos = [];

        $courses = SoftworldCourses::activePlatform();

        foreach ($courses as $course) {
            $array_cursos[] = [
                "vacio" => "",
                "codigo_curso" => $course->code,
                "imagen_curso" => $course->cover_image,
                "titulo_curso" => $course->title,
                "fecha_inicio_curso" => $course->start_date,
                "fecha_fin_curso" => $course->end_date,
                "profesional" => $course->getInstructor->first_name . " " . $course->getInstructor->second_name . " " . $course->getInstructor->first_last_name,
                "codigo_estado" =>  $course->getStatus->id,
                "nombre_estado" =>  $course->getStatus->description,
            ];
        }

        $informacion["data"] = $array_cursos;

        return json_encode($informacion);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('administrator.learning.courses.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = SoftworldCategory::active();
        $professionals = SoftworldPerson::getUsersOnly();
        $levels = SoftworldLevels::active();

        return view('administrator.learning.courses.create', compact('categories', 'professionals', 'levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'imagen_curso' => 'required|mimes:jpg,jpeg,png',
            'start_date' => 'required|date|after_or_equal:today',
            'end_date' =>  'nullable|date|after_or_equal:start_date',
            'title' => 'required',
            'summary' => 'required',
            'description' => 'required',
            'video_image' => 'nullable|mimes:mp4',
            'category_id' => 'required',
            'professional_id' => 'required',
            'nivel_curso' => 'required',
        ];

        $messages = [
            'imagen_curso.required' => 'Este campo es importante',
            'imagen_curso.mimes' => 'Solo se permiten archivos con extensión: jpg, jpeg, png',

            'start_date.required' => 'Este campo es importante',
            'start_date.after_or_equal' => 'La fecha de inicio no debe ser inferior a la fecha actual',

            'title.required' => 'Este campo es importante',
            'summary.required' => 'Este campo es importante',
            'description.required' => 'Este campo es importante',
            'video_image.mimes' => 'Solo se permiten archivos de video',

            'category_id.unique' => 'Este campo es importante',
            'professional_id.required' => 'Este campo es importante',
            'nivel_curso.unique' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        $count = SoftworldCourses::all()->count() + 1;
        $code = "CNP-CUR" . $count;

        $image = $request->file("imagen_curso")->store("cursos" . "/" . $code, 's3');

        if ($request->file("video_image")) {
            $rules = [
                "video_image" => "mimes:mp4",
            ];

            $messages = [
                'video_image.mimes' => 'Solo se permiten archivos Mp4',
            ];

            $this->validate($request, $rules, $messages);
            $file_video = $request->file("video_image")->store("cursos" . "/" . $code, 's3');
        } else {
            $file_video = NULL;
        }

        $course = new SoftworldCourses();
        $course->code = $code;
        $course->title = $request->title;
        $course->summary = $request->summary;
        $course->description = $request->description;
        $course->cover_image = $image;
        $course->video_image = $file_video;
        $course->start_date = $request->start_date;
        $course->end_date = $request->end_date;
        $course->category_id = $request->category_id;
        $course->instructor_id = $request->professional_id;
        $course->level = $request->nivel_curso;
        $course->student_limit = $request->student_limit;
        $course->status = 1;
        $course->save();

        if ($request->file("archivos")) {
            foreach ($request->file("archivos") as $file) {
                $archive =  $file->store("cursos" . "/" . $code, 's3');

                $fileData = new SoftworldFiles();
                $fileData->title = $file->getClientOriginalName();
                $fileData->type = $file->getClientOriginalExtension();
                $fileData->url = $archive;
                $fileData->status = 1;
                $fileData->save();

                DetailsFilesCourses::create([
                    'course_id' => $request->code,
                    'file_id' => $fileData->id,
                ]);
            }
        }

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = SoftworldCourses::find($id);
        return view('administrator.learning.courses.show', compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = SoftworldCourses::find($id);
        $categories = SoftworldCategory::active();
        $professionals = SoftworldPerson::getUsersOnly();
        $levels = SoftworldLevels::active();
        $status = SoftworldStatusPlatforms::getByModule('courses');

        return view('administrator.learning.courses.edit', compact('course', 'categories', 'professionals', 'levels', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'start_date' => 'required|date',
            'end_date' =>  'nullable|date|after_or_equal:start_date',
            'title' => 'required',
            'summary' => 'required|max:191',
            'description' => 'required',
            'video_image' => 'nullable|mimes:mp4',
            'category_id' => 'required',
            'professional_id' => 'required',
            'level' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'start_date.required' => 'Este campo es importante',
            'start_date.after_or_equal' => 'La fecha de inicio no debe ser inferior a la fecha actual',

            'title.required' => 'Este campo es importante',
            'summary.required' => 'Este campo es importante',
            'summary.max' => 'Resumen demasiado largo',
            'description.required' => 'Este campo es importante',
            'video_image.mimes' => 'Solo se permiten archivos de video',
            'end_date.after_or_equal' => 'La fecha final no debe ser inferior a la fecha de inicio',
            'category_id.unique' => 'Este campo es importante',
            'professional_id.required' => 'Este campo es importante',
            'level.unique' => 'Este campo es importante',
            'status.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        $course = SoftworldCourses::find($request->code);
        $course->title = $request->title;
        $course->summary = $request->summary;
        $course->description = $request->description;

        $course->start_date = $request->start_date;
        $course->end_date = $request->end_date;
        $course->category_id = $request->category_id;
        $course->instructor_id = $request->professional_id;
        $course->level = $request->level;
        $course->student_limit = $request->student_limit;
        $course->status = $request->status;
        $course->update();

        if ($request->hasFile("imagen_curso")) {
            $rules = [
                'imagen_curso' => 'required|mimes:jpg,jpeg,png',
            ];

            $messages = [
                'imagen_curso.mimes' => 'Solo se permiten archivos con extensión: jpg, jpeg, png',
            ];

            $this->validate($request, $rules, $messages);

            $image = $request->file("imagen_curso")->store("cursos" . "/" . $request->code, 's3');
            $course->cover_image = $image;
            $course->update();
        }

        if ($request->hasFile("video_image")) {
            $rules = [
                "video_image" => "mimes:mp4",
            ];

            $messages = [
                'video_image.mimes' => 'Solo se permiten archivos Mp4',
            ];

            $this->validate($request, $rules, $messages);
            $file_video = $request->file("video_image")->store("cursos" . "/" . $request->code, 's3');
            $course->video_image = $file_video;
            $course->update();
        }

        if ($request->file("archivos")) {
            foreach ($request->file("archivos") as $file) {
                $archive =  $file->store("cursos" . "/" . $request->code, 's3');

                $fileData = new SoftworldFiles();
                $fileData->title = $file->getClientOriginalName();
                $fileData->type = $file->getClientOriginalExtension();
                $fileData->url = $archive;
                $fileData->status = 1;
                $fileData->save();

                DetailsFilesCourses::create([
                    'course_id' => $request->code,
                    'file_id' => $fileData->id,
                ]);
            }
        }

        return true;
    }

    public function form_membership_course($id)
    {

        $array_memberships_selected = [];

        $memberships = SoftworldMembership::activePlatform();

        $course = SoftworldCourses::find($id);
        $memberships_courses = $course->getMemberships;
        
        foreach ($memberships_courses as $membership) {
            $array_memberships_selected[] = $membership->code;
        }

        return view("administrator.learning.courses.assign_memberships", compact("memberships", "array_memberships_selected", "id"));
    }

    public function store_membership_course(Request $request, $id){
        $course = SoftworldCourses::find($id);
        if ($request->membership) {
            $course->getMemberships()->sync(explode(",", $request->membership));
        }

        return true;
    }

    public function modules($courseId)
    {
        $course = SoftworldCourses::find($courseId);
        return view('administrator.learning.courses.modules.list', compact('course'));
    }

    public function moduleStore(Request $request, $courseId)
    {

        $rules = [
            'file' => 'required|mimes:jpg,jpeg,png',
            'text' => 'required',
            'textarea' => 'required',
        ];

        $messages = [
            'file.required' => 'Este campo es importante',
            'file.mimes' => 'Solo se permiten archivos con extensión: jpg, jpeg, png',

            'text.required' => 'Este campo es importante',
            'textarea.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);


        $count = SoftworldCoursesModules::all()->count() + 1;
        $code = "CNP-CMODUL" . $count;

        $image = $request->file("file")->store("cursos" . "/" . $courseId . "/modules/" . $code, 's3');

        $module = new SoftworldCoursesModules();
        $module->code = $code;
        $module->image = $image;
        $module->title = $request->text;
        $module->summary = $request->textarea;
        $module->status = 13;
        $module->save();

        DetailsModulesCourses::create([
            'course_id' => $courseId,
            'module_id' => $code,
        ]);

        return true;
    }

    public function moduleEdit($id)
    {
        $module = SoftworldCoursesModules::find($id);
        return response($module);
    }

    public function moduleUpdate(Request $request, $moduleId)
    {
        $rules = [
            'text' => 'required',
            'textarea' => 'required',
        ];

        $messages = [
            'text.required' => 'Este campo es importante',
            'textarea.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        $module = SoftworldCoursesModules::find($moduleId);
        $module->title = $request->text;
        $module->summary = $request->textarea;

        if ($request->hasFile('file')) {

            $rules = [
                'file' => 'required|mimes:jpg,jpeg,png',
            ];

            $messages = [
                'file.required' => 'Este campo es importante',
                'file.mimes' => 'Solo se permiten archivos con extensión: jpg, jpeg, png',
            ];

            $this->validate($request, $rules, $messages);
            $image = $request->file("file")->store("cursos" . "/" . $request->code_course . "/modules/" . $moduleId, 's3');
            $module->image = $image;
        }

        $module->update();

        return true;
    }


    public function moduleDelete($id)
    {
        date_default_timezone_set("America/Bogota");

        $module = SoftworldCoursesModules::find($id);
        $module->date_deleted = NOW();
        $module->deleted_by = auth()->user()->id;
        $module->update();

        return true;
    }

    public function lessonStore(Request $request, $moduleId)
    {
        $rules = [
            'image' => 'required|mimes:jpg,jpeg,png',
            'title' => 'required',
            'summary' => 'required',
            'type' => 'required',
        ];

        $messages = [
            'image.required' => 'Este campo es importante',
            'image.mimes' => 'Por favor seleccione imagenes de formato: jpg, jpeg y png',
            'title.required' => 'Este campo es importante',
            'summary.required' => 'Este campo es importante',
            'summary.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        $count = SoftworldModulesLessons::all()->count() + 1;
        $code = "CNP-MLE" . $count;

        $module = SoftworldCoursesModules::find($moduleId);
        foreach ($module->getCourses as $course) {
            $code_course = $course->code;
        }

        foreach ($request->file('archivos') as $file) {
            $mime_type = $file->getClientMimeType();
            $type = explode('/', $mime_type);

            if ($request->type != $type[1]) {
                return [false, "file_error"];
            }

            $archive = $file->store("cursos" . "/" . $code_course . "/modules/lessons/" . $code, 's3');
            
            $fileData = new SoftworldFiles();
            $fileData->title = $file->getClientOriginalName();
            $fileData->type = $file->getClientOriginalExtension();
            $fileData->url = $archive;
            $fileData->status = 1;
            $fileData->save();

            DetailsFilesLessons::create([
                'lesson_id' => $code,
                'file_id' => $fileData->id,
            ]);
        }

        $image = $request->file("image")->store("cursos" . "/" . $code_course . "/modules/" . $moduleId . "/lessons/" . $code, 's3');

        SoftworldModulesLessons::create([
            'code' => $code,
            'image' => $image,
            'title' => $request->title,
            'summary' => $request->summary,
            'type' => $request->type,
            'status' => 13,
        ]);

        DetailsLessonsModules::create([
            'module_id' => $moduleId,
            'lesson_id' => $code,
        ]);

        return [true, $code_course];
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lessonEdit($id)
    {
        $data = SoftworldModulesLessons::find($id);
        return view("administrator.template.forms.new_lesson", compact("data"));
    }

    public function lessonUpdate(Request $request, $id)
    {
        $rules = [
            'title' => 'required',
            'summary' => 'required',
            'type' => 'required',
        ];

        $messages = [
            'title.required' => 'Este campo es importante',
            'summary.required' => 'Este campo es importante',
            'summary.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        $lesson = SoftworldModulesLessons::find($id);

        foreach ($lesson->getModules as $module) {
            $code_module = $module->code;
            foreach ($module->getCourses as $course) {
                $code_course = $course->code;
            }
        }

        if ($request->hasFile('image')) {
            $rules = [
                'image' => 'required|mimes:jpg,jpeg,png',
            ];

            $messages = [
                'image.required' => 'Este campo es importante',
                'image.mimes' => 'Por favor seleccione imagenes de formato: jpg, jpeg y png',
            ];

            $this->validate($request, $rules, $messages);

            $image = $request->file("image")->store("cursos" . "/" . $code_course . "/modules/" . $code_module . "/lessons/" . $id, 's3');
        }

        $lesson->title = $request->title;
        $lesson->summary = $request->summary;
        $lesson->type = $request->type;
        $lesson->update();

        return [true, $code_course];
    }

    public function lessonDelete($id)
    {
        date_default_timezone_set("America/Bogota");

        $lesson = SoftworldModulesLessons::find($id);
        $lesson->date_deleted = NOW();
        $lesson->deleted_by = auth()->user()->id;
        $lesson->update();

        foreach ($lesson->getModules as $module) {
            foreach ($module->getCourses as $course) {
                $code_course = $course->code;
            }
        }
        
        return [true, $code_course];
    }
}
