<?php

namespace App\Http\Controllers\Administrator\Administrative;

use App\Http\Controllers\Controller;
use App\Imports\EconomicIndicatorImport;
use App\Models\SoftworldEconomicIndicators;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class EconomicIndicatorsController extends Controller
{
    public function listado()
    {
        $array_indicadores = [];
        $indicadores = SoftworldEconomicIndicators::active();

        foreach ($indicadores as $indicador) {
            $array_indicadores[] = [
                'vacio' => "",
                'id' => $indicador->id,
                'fecha_indicador_inicio' => $indicador->date_start_indicator,
                'fecha_indicador_fin' => $indicador->date_end_indicator,
                'valor_indicador' => $indicador->value_start_indicator,
                'porcentaje' => $indicador->percentage_start_indicator,
                'descripcion_indicador' => $indicador->description_start_indicator,
            ];
        }

        $informacion["data"] = $array_indicadores;
        return json_encode($informacion);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("administrator.administrative.economic-indicators.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("administrator.administrative.economic-indicators.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'date_start_indicator'          => 'required|date|before_or_equal:date_end_indicator',
            'date_end_indicator'            => 'required|date',
            'value_start_indicator'         => 'required',
            'percentage_start_indicator'    => 'required',
            'description_start_indicator'   => 'required',
        ];

        $messages = [
            'date_start_indicator.required'         => 'Este campo  es importante.',
            'date_start_indicator.before_or_equal'  => 'La fecha de inicio no puede ser superior a la fecha de fin.',
            'date_end_indicator.required'           => 'Este campo  es importante.',
            'value_start_indicator.required'        => 'Este campo es importante.',
            'percentage_start_indicator.required'   => 'Este campo es importante.',
            'description_start_indicator.required'  => 'Este campo es importante.',
        ];

        $this->validate($request, $rules, $messages);

        SoftworldEconomicIndicators::create([
            'date_start_indicator' => $request->date_start_indicator,
            'date_end_indicator' => $request->date_end_indicator,
            'value_start_indicator' => $request->value_start_indicator,
            'description_start_indicator' => $request->description_start_indicator,
            'percentage_start_indicator' => $request->percentage_start_indicator,
            'status_start_indicator' => $request->status_start_indicator,
            'status_start_indicator' => 1,
        ]);

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $indicador = SoftworldEconomicIndicators::find($id);
        $indicador->date_deleted = NOW();
        $indicador->deleted_by = 1;
        $indicador->update();

        return true;
    }

    public function formImport()
    {
        return view("administrator.administrative.economic-indicators.import-form");
    }

    public function updateImportEconomicIndicators(Request $request)
    {
        $rules = [
            'file' => 'required',
            'indicador_economico' => 'required',
        ];

        $messages = [
            'file.required'       => 'Este campo  es importante.',
            'indicador_economico.required'       => 'Este campo  es importante.',
        ];

        $this->validate($request, $rules, $messages);

        SoftworldEconomicIndicators::where("description_start_indicator", $request->indicador_economico)->delete();
        $archivo = $request->file('file');
        Excel::import(new EconomicIndicatorImport($request->indicador_economico), $archivo);

        return true;
    }
}
