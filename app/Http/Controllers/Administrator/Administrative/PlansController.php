<?php

namespace App\Http\Controllers\Administrator\Administrative;

use App\Http\Controllers\Controller;
use App\Models\SoftworldMembership;
use App\Models\SoftworldPlans;
use App\Models\SoftworldStatusPlatforms;
use Illuminate\Http\Request;

class PlansController extends Controller
{

    public function listado()
    {
        $array_planes = [];

        $planes = SoftworldPlans::activePlatform();

        foreach ($planes as $plan) {
            $array_planes[] = [
                "vacio" => "",
                "codigo_plan" => $plan->code,
                "nombre_plan" => $plan->title,
                "fecha_inicio" => $plan->start_date,
                "fecha_fin" => $plan->end_date,
                "valor_del_cobro" => '$ ' . number_format($plan->price),
                "codigo_estado" => $plan->getStatus->id,
                "nombre_estado" => $plan->getStatus->description,
            ];
        }

        $informacion["data"] = $array_planes;

        return json_encode($informacion);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("administrator.administrative.plans.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $plan = SoftworldPlans::find($id);
        return view("administrator.administrative.plans.show", compact('plan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $array_membresias = [];
        $plan = SoftworldPlans::find($id);
        $membresias = SoftworldMembership::activePlatform();
        $estados = SoftworldStatusPlatforms::basic();

        foreach ($plan->memberships as $data) {
            $array_membresias[] =  $data->code;
        }

        return view("administrator.administrative.plans.edit", compact('membresias', 'array_membresias', 'plan', 'estados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'start_date' => 'required',
            'end_date' => 'nullable|after_or_equal:start_date',
            'title' => 'required|unique:softworld_plans,title,' . $request->code . ',code',
            'price' => 'required',
            'description' => 'required',
            'status' => 'required',
        ];

        $messages = [
            'start_date.unique' => 'Ya existe una categoria con este titulo',

            'end_date.required' => 'Este campo es importante',
            'end_date.after_or_equal' => 'No debe ser superior a la fecha de inicio.',

            'title.required' => 'Este campo es importante',
            'price.required' => 'Este campo es importante',
            'title.unique' => 'Ya hay una membresia con este nombre.',
            'description.required' => 'Este campo es importante',

            'status.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        $plan = SoftworldPlans::find($request->code);


        if ($request->hasFile('image')) {
            $rules = [
                'image' => 'mimes:jpg,jpeg,png',
            ];

            $messages = [
                'image.mimes' => 'Seleccione archivos de imagen: jpg, jpeg, png',
            ];

            $this->validate($request, $rules, $messages);

            $image = $request->file("image")->store("planes", 's3');
            $plan->image = $image;
        }

        $plan->title = $request->title;
        $plan->description = $request->description;
        $plan->platform_code = $request->platform_code;
        $plan->start_date = $request->start_date;
        $plan->end_date = $request->end_date;
        $plan->price = $request->price;
        $plan->status = $request->status;
        $plan->update();

        if ($request->membresias) {
            $plan->memberships()->sync(explode(",", $request->membresias));
        }

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        date_default_timezone_set("America/Bogota");

        $plan = SoftworldPlans::find($id);
        $plan->date_deleted = now();
        $plan->deleted_by = auth()->user()->id;
        $plan->update();

        return true;
    }
}
