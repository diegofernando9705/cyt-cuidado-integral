<?php

namespace App\Http\Controllers\Administrator\Administrative;

use App\Http\Controllers\Controller;
use App\Models\SoftworldDocumentTypes;
use App\Models\SoftworldPerson;
use App\Models\SoftworldStatusPlatforms;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PeopleController extends Controller
{
    public function listado()
    {
        $array_personas = [];

        $person = SoftworldPerson::active();

        foreach ($person as $people) {
            if ($people->type == "subscriber") {
                $type = "Suscriptor | Abogado";
            } else {
                $type = "Talento humano";
            }

            $array_personas[] = [
                "vacio" => "",
                "id" => $people->id,
                "nombres" => $people->first_name . " " . $people->second_name,
                "apellidos" => $people->first_last_name . " " . $people->second_last_name,
                "email" => $people->email,
                "type" => $type,
                "codigo_estado" => $people->getStatus->id,
                "nombre_estado" => $people->getStatus->description,
            ];
        }

        $informacion["data"] = $array_personas;

        return json_encode($informacion);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("administrator.administrative.people.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipos_documentos = SoftworldDocumentTypes::active();
        return view("administrator.administrative.people.create", compact("tipos_documentos"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'identificacion_profesional' => 'required|unique:softworld_people,id',
            'tarjeta_profesional' => 'required|unique:softworld_people,professional_card',
            'tipo_de_documento_profesional' => 'required',
            'primer_nombre_profesional' => 'required',
            'primer_apellido_profesional' => 'required',
            'celular_profesional' => 'required|numeric|digits:10|unique:softworld_people,cellphone',
            'correo_profesional' => 'required|email|unique:softworld_people,email',
            'type' => 'required',
        ];

        $messages = [
            'identificacion_profesional.required' => 'Este campo es importante',
            'identificacion_profesional.unique' => 'Ya existe un registro con este número de identificación',

            'tarjeta_profesional.required' => 'Este campo es importante, si no posee, por favor coloque el Número de identificación',
            'tarjeta_profesional.unique' => 'Ya existe un registro con este número de tarjeta profesional',

            'tipo_de_documento_profesional.required' => 'Este campo es importante',
            'primer_nombre_profesional.required' => 'Este campo es importante',
            'primer_apellido_profesional.required' => 'Este campo es importante',

            'celular_profesional.required' => 'Este campo es importante',
            'celular_profesional.unique' => 'Ya existe un registro con este número de celular',
            'celular_profesional.numeric' => 'Ingrese un número de celular válido',
            'celular_profesional.digits' => 'Ingrese un número de celular válido',

            'correo_profesional.required' => 'Este campo es importante',
            'correo_profesional.unique' => 'Ya existe un registro con este correo electrónico',
            'correo_profesional.email' => 'Ingrese un correo electrónico válido',

            'type.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        $person = new SoftworldPerson();
        $person->document_type = $request->tipo_de_documento_profesional;
        $person->professional_card = $request->tarjeta_profesional;
        $person->id = $request->identificacion_profesional;
        $person->first_name = $request->primer_nombre_profesional;
        $person->second_name = $request->segundo_nombre_profesional;
        $person->first_last_name = $request->primer_apellido_profesional;
        $person->second_last_name = $request->segundo_apellido_profesional;
        $person->cellphone = $request->celular_profesional;
        $person->email = $request->correo_profesional;
        $person->type = $request->type;
        $person->status = 1;

        $user = new User();
        $user->name = $request->primer_nombre_profesional . " " . $request->primer_apellido_profesional;
        $user->email = $request->correo_profesional;
        $user->password = Hash::make($request->identificacion_profesional);
        $user->condicion = 1;

        if ($request->hasFile("user_avatar")) {
            $rules = [
                'user_avatar' => 'mimes:jpg,jpeg,png',
            ];

            $messages = [
                'user_avatar.mimes' => 'Seleccione archivos de imagen: jpg, jpeg, png',
            ];

            $this->validate($request, $rules, $messages);

            $image = $request->file("user_avatar")->store("people", 's3');
            $user->avatar = $image;
        }

        $user->save();

        $person->user = $user->id;
        $person->save();

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SoftworldPerson::find($id);
        return view("administrator.administrative.people.show", compact("data"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = SoftworldPerson::find($id);
        $status = SoftworldStatusPlatforms::basic();

        return view("administrator.administrative.people.edit", compact("data", "status"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'tarjeta_profesional' => 'required|unique:softworld_people,professional_card,' . $request->identificacion_profesional, "id",
            'primer_nombre_profesional' => 'required',
            'primer_apellido_profesional' => 'required',
            'celular_profesional' => 'required|numeric|digits:10|unique:softworld_people,cellphone,' . $request->identificacion_profesional, "id",
            'correo_profesional' => 'required|email|unique:softworld_people,email,' . $request->identificacion_profesional, "id",
            'type' => 'required',
            'estado' => 'required',
        ];

        $messages = [
            'tarjeta_profesional.required' => 'Este campo es importante, si no posee, por favor coloque el Número de identificación',
            'tarjeta_profesional.unique' => 'Ya existe un registro con este número de tarjeta profesional',

            'primer_nombre_profesional.required' => 'Este campo es importante',
            'primer_apellido_profesional.required' => 'Este campo es importante',

            'celular_profesional.required' => 'Este campo es importante',
            'celular_profesional.unique' => 'Ya existe un registro con este número de celular',
            'celular_profesional.numeric' => 'Ingrese un número de celular válido',
            'celular_profesional.digits' => 'Ingrese un número de celular válido',

            'correo_profesional.required' => 'Este campo es importante',
            'correo_profesional.unique' => 'Ya existe un registro con este correo electrónico',
            'correo_profesional.email' => 'Ingrese un correo electrónico válido',

            'type.required' => 'Este campo es importante',
            'estado.required' => 'Este campo es importante',
        ];

        $this->validate($request, $rules, $messages);

        $people = SoftworldPerson::find($request->identificacion_profesional);
        $people->professional_card = $request->tarjeta_profesional;
        $people->first_name = $request->primer_nombre_profesional;
        $people->second_name = $request->segundo_nombre_profesional;
        $people->first_last_name = $request->primer_apellido_profesional;
        $people->second_last_name = $request->segundo_apellido_profesional;
        $people->cellphone = $request->celular_profesional;
        $people->email = $request->correo_profesional;
        $people->type = $request->type;
        $people->status = $request->estado;
        $people->update();

        if ($request->hasFile("user_avatar")) {
            $rules = [
                'user_avatar' => 'mimes:jpg,jpeg,png',
            ];

            $messages = [
                'user_avatar.mimes' => 'Seleccione archivos de imagen: jpg, jpeg, png',
            ];

            $this->validate($request, $rules, $messages);

            $user =  User::find($people->user);
            $user->avatar = $request->file("user_avatar")->store("people", 's3');
            $user->update();
        }

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        date_default_timezone_set("America/Bogota");

        $people = SoftworldPerson::find($id);
        $people->status = 3;
        $people->date_deleted = now();
        $people->deleted_by = auth()->user()->id;
        $people->update();

        return true;
    }
}
