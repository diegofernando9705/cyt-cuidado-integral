<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\softworld_pacientes AS Pacientes;
use App\softworld_historias_clinica AS HistoriasClinicas;
use App\softworld_formulario_fonoaudiologia AS Fonoaudiologia;
use App\softworld_formulario_medico_general AS MedicoGeneral;
use App\softworld_formulario_nutricionista AS Nutricionista;
use App\softworld_formulario_psicologia AS Psicologia;
use App\softworld_formulario_enterostomal AS Enterostomal;
use App\softworld_formulario_fisioterapia AS Fisioterapia;
use App\softworld_formulario_terapia_ocupacional AS TerapiaOcupacional;
use App\softworld_formulario_terapia_respiratoria AS TerapiaRespiratoria;
use App\softworld_evolucion_terapeuta AS EvolucionTerapeuta;
use App\softworld_evolucion_enfermeria AS EvolucionEnfermeria;
use App\softworld_estados_plataforma as EstadosPlataforma;

use Barryvdh\DomPDF\Facade as PDF;
use Storage;
use DB;

class HistoriasclinicasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('administrativo.historias-clinicas.index');
    }


    public function listado(){

        $array_historias = [];

        if (auth()->user()->id == 1) {
            $informacion_historias = HistoriasClinicas::all();
        } else {
            $informacion_historias = HistoriasClinicas::where("estado_historia_clinica", 4)->get();
        }


        foreach ($informacion_historias as $historia) {

            $estado = EstadosPlataforma::where("codigo_estado", $historia->estado_historia_clinica)->get();
            $paciente = Pacientes::where("cedula_paciente", $historia->cedula_paciente)->get();

            $array_historias[] = [
                "vacio" => "",
                "codigo_historia_clinica" => $historia->estado_historia_clinica,
                "cedula_paciente" => $historia->cedula_paciente ,
                "primer_nombre" => $paciente[0]->primer_nombre." ".$paciente[0]->segundo_nombre,
                "primer_apellido" => $paciente[0]->primer_apellido." ".$paciente[0]->segundo_apellido,
                "fecha_historia_clinica" => $historia->fecha_historia_clinica,
                "estado_historia_clinica" => $estado[0]->codigo_estado,
                "nombre_estados" => $estado[0]->descripcion_estado,
            ];
        }

        $informacion["data"] = $array_historias;
        
        return json_encode($informacion);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $procesos = [];
        date_default_timezone_set('America/Bogota');
        $historia_clinica = HistoriasClinicas::where('cedula_paciente', $id)->get();
        $formulario_admision = [];

        /* ============================== FORMULARIO ADMISION ============================== */

        $sql_formulario_admision = DB::table('detalle_formularios_pacientes')
                    ->select('detalle_formularios_pacientes.*', 'softworld_pacientes.*', 'softworld_formulario_admisiones.*')
                    ->join('softworld_pacientes', 'detalle_formularios_pacientes.id_persona', '=', 'softworld_pacientes.cedula_paciente')
                    ->join('detalle_formularios_pacientes', 'detalle_formularios_pacientes.id_formulario', '=', 'softworld_formulario_admisiones.id_formulario_admision')
                    ->where('detalle_formularios_pacientes.id_persona', $id)
                    ->get();

            foreach ($notas as $value) {
                $formulario_admision[] = array(
                                    'titulo_proceso' => "Hola mundo", 
                                    'id_proceso_evolucion' => "Hola mundo",
                                    'id_nota_paciente' => "Hola mundo",
                                    'titulo_nota' => "Hola mundo",
                                    'descripcion_nota' => "Hola mundo",
                                    'fecha_nota' => "Hola mundo",
                                    'usuario_responsable' => "Hola mundo",
                                    'estado_nota' => "Hola mundo"
                                    );
            }
            
        /* ============================== FIN FORMULARIO ADMISION ============================== */


        /* ============================== FORMULARIO NOTAS RAPIDAS ============================== */
            $notas = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_notas_pacientes.*', 'softworld_historias_clinicas.*')
                    ->join('softworld_notas_pacientes', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_notas_pacientes.id_nota_paciente')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();
            foreach ($notas as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'id_nota_paciente' => $value->id_nota_paciente,
                                    'titulo_nota' => $value->titulo_nota,
                                    'descripcion_nota' => $value->descripcion_nota,
                                    'fecha_nota' => $value->fecha_nota,
                                    'usuario_responsable' => $value->usuario_responsable,
                                    'estado_nota' => $value->estado_nota);
            }
        /* ============================== FIN FORMULARIO NOTAS RAPIDAS ============================== */


        /* ============================== FORMULARIO FONOAUDILOGIA ============================== */
            $formularios = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_fonoaudiologias.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
                    ->join('softworld_formulario_fonoaudiologias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_fonoaudiologias.id_form_fono')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->join('softworld_cie10', 'softworld_formulario_fonoaudiologias.cie10_registro', '=', 'softworld_cie10.codigo')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($formularios as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'id_form_fono' => $value->id_form_fono,
                                    'cie10_codigo' => $value->codigo,
                                    'cie10_name' => $value->descripcion,
                                    'cie10_registro' => $value->cie10_registro,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'ta_diagnostico' => $value->ta_diagnostico,
                                    'fc_diagnostico' => $value->fc_diagnostico,
                                    'fr_diagnostico' => $value->fr_diagnostico,
                                    'to_diagnostico' => $value->to_diagnostico,
                                    'diagnostico_registro' => $value->diagnostico_registro,
                                    'observacion_registro' => $value->observacion_registro,
                                    'habla_registro' => $value->habla_registro,
                                    'voz_registro' => $value->voz_registro,
                                    'articulacion_fonemas_registro' => $value->articulacion_fonemas_registro,
                                    'deglucion_registro' => $value->deglucion_registro,
                                    'respiracion_registro' => $value->respiracion_registro,
                                    'organos_fonatorios_registro' => $value->organos_fonatorios_registro,
                                    'audicion_registro' => $value->audicion_registro,
                                    'procesos_cognitivos_registro' => $value->procesos_cognitivos_registro,
                                    'analisis_registro' => $value->analisis_registro,
                                    'objetivos_tratamiento_registro' => $value->objetivos_tratamiento_registro,
                                    'plan_manejo_registro' => $value->plan_manejo_registro,
                                    'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                                    'interconsultas_registro' => $value->interconsultas_registro,
                                );
            }
        /* ============================== FIN FORMULARIO FONOAUDILOGIA ============================== */


        /* ============================== FORMULARIO MEDICO GENERAL ============================== */

            $formulario_MG = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_medico_generals.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
                    ->join('softworld_formulario_medico_generals', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_medico_generals.id_form_medico')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->join('softworld_cie10', 'softworld_formulario_medico_generals.cie10_registro', '=', 'softworld_cie10.codigo')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($formulario_MG as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'id_form_medico' => $value->id_form_medico,
                                    'cie10_codigo' => $value->codigo,
                                    'cie10_name' => $value->descripcion,
                                    'cie10_registro' => $value->cie10_registro,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'fc_diagnostico' => $value->fc_diagnostico,
                                    'fr_diagnostico' => $value->fr_diagnostico,
                                    'sato2' => $value->sato2,
                                    'peso' => $value->peso,
                                    'talla' => $value->talla,
                                    'ta_diagnostico' => $value->ta_diagnostico,
                                    'to_diagnostico' => $value->to_diagnostico,
                                    'imc' => $value->imc,
                                    'diagnostico_registro' => $value->diagnostico_registro,
                                    'uso_previo' => $value->uso_previo,
                                    'alergias' => $value->alergias,
                                    'analisis_registro' => $value->analisis_registro,
                                    'plan_manejo_registro' => $value->plan_manejo_registro,
                                    'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                                    'interconsultas_registro' => $value->interconsultas_registro,
                                );
            }
        /* ============================== FIN FORMULARIO MEDICO GENERAL ============================== */


        /* ============================== FORMULARIO PSICOLOGIA ============================== */

            $formulario_PSI = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_psicologias.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
                    ->join('softworld_formulario_psicologias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_psicologias.id_form_psi')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->join('softworld_cie10', 'softworld_formulario_psicologias.cie10_registro', '=', 'softworld_cie10.codigo')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($formulario_PSI as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'id_psicologia' => $value->id_form_psi,
                                    'cie10_codigo' => $value->codigo,
                                    'cie10_name' => $value->descripcion,
                                    'cie10_registro' => $value->cie10_registro,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'motivo_consulta' => $value->motivo_consulta,
                                    'condiciones_personales' => $value->condiciones_personales,
                                    'condiciones_familiares' => $value->condiciones_familiares,
                                    'analisis_registro' => $value->analisis_registro,
                                    'plan_manejo_registro' => $value->plan_manejo_registro,
                                    'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                                    'interconsultas_registro' => $value->interconsultas_registro,
                                );
            }
        /* ============================== FIN FORMULARIO PSICOLOGIA ============================== */


        /* ============================== FORMULARIO NUTRICION ============================== */

            $formulario_nutricion = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_nutricionistas.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
                    ->join('softworld_formulario_nutricionistas', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_nutricionistas.id_form_nutri')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->join('softworld_cie10', 'softworld_formulario_nutricionistas.cie10_registro', '=', 'softworld_cie10.codigo')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($formulario_nutricion as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'cie10_codigo' => $value->codigo,
                                    'cie10_name' => $value->descripcion,
                                    'id_form_nutri' => $value->id_form_nutri,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'fc_diagnostico' => $value->fc_diagnostico,
                                    'fr_diagnostico' => $value->fr_diagnostico,
                                    'sato2' => $value->sato2,
                                    'peso' => $value->peso,
                                    'talla' => $value->talla,
                                    'imc' => $value->imc,
                                    'to_diagnostico' => $value->to_diagnostico,
                                    'ta_diagnostico' => $value->ta_diagnostico,
                                    'diagnostico_registro' => $value->diagnostico_registro,
                                    'estado_nutricion_actual' => $value->estado_nutricion_actual,
                                    'via_alimentacion' => $value->via_alimentacion,
                                    'disfagia_valor' => $value->disfagia_valor,
                                    'disfagia_porque' => $value->disfagia_porque,
                                    'valoracion_nutricional' => $value->valoracion_nutricional,
                                    'uso_previo' => $value->uso_previo,
                                    'alergias' => $value->alergias,
                                    'analisis_registro' => $value->analisis_registro,
                                    'plan_manejo_registro' => $value->plan_manejo_registro,
                                    'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                                    'interconsultas_registro' => $value->interconsultas_registro,

                                );
            }

        /* ============================== FIN FORMULARIO NUTRICION ============================== */


        /* ============================== FORMULARIO FISIOTERAPIA ============================== */

            $formulario_nutricion = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_fisioterapias.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
                    ->join('softworld_formulario_fisioterapias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_fisioterapias.id_form_fisio')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->join('softworld_cie10', 'softworld_formulario_fisioterapias.cie10_registro', '=', 'softworld_cie10.codigo')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($formulario_nutricion as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'id_form_fisio' => $value->id_form_fisio,
                                    'cie10_codigo' => $value->codigo,
                                    'cie10_name' => $value->descripcion,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'fc_diagnostico' => $value->fc_diagnostico,
                                    'fr_diagnostico' => $value->fr_diagnostico,
                                    'sato2' => $value->sato2,
                                    'peso' => $value->peso,
                                    'talla' => $value->talla,
                                    'imc' => $value->imc,
                                    'to_diagnostico' => $value->to_diagnostico,
                                    'ta_diagnostico' => $value->ta_diagnostico,
                                    'diagnostico_registro' => $value->diagnostico_registro,
                                    'observacion' => $value->observacion,
                                    'dolor' => $value->dolor,
                                    'piel' => $value->piel,
                                    'inspeccion' => $value->inspeccion,
                                    'palpacion' => $value->palpacion,
                                    'auscultacion' => $value->auscultacion,
                                    'movilidad_articular' => $value->movilidad_articular,
                                    'fuerza_muscular' => $value->fuerza_muscular,
                                    'tono_muscular' => $value->tono_muscular,
                                    'sensibilidad' => $value->sensibilidad,
                                    'marcha' => $value->marcha,
                                    'analisis_registro' => $value->analisis_registro,
                                    'objetivos_tratamiento' => $value->objetivos_tratamiento,
                                    'plan_manejo_registro' => $value->plan_manejo_registro,
                                    'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                                    'interconsultas_registro' => $value->interconsultas_registro,

                                );
            }

        /* ============================== FIN FORMULARIO FISIOTERAPIA ============================== */


        /* ============================== FORMULARIO ENTEROSTOMAL ============================== */

            $formulario_enterostomal = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_enterostomals.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
                    ->join('softworld_formulario_enterostomals', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_enterostomals.id_form_enterostomal')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->join('softworld_cie10', 'softworld_formulario_enterostomals.cie10_registro', '=', 'softworld_cie10.codigo')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($formulario_enterostomal as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'id_form_enterostomal' => $value->id_form_enterostomal,
                                    'cie10_codigo' => $value->codigo,
                                    'cie10_name' => $value->descripcion,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'area_afectada' => $value->area_afectada,
                                    'clasificacion_lesion' => $value->clasificacion_lesion,
                                    'clasificacion_profundidad' => $value->clasificacion_profundidad,
                                    'grosor_parcial' => $value->grosor_parcial,
                                    'grosor_total' => $value->grosor_total,
                                    'lesion_largo' => $value->lesion_largo,
                                    'lesion_ancho' => $value->lesion_ancho,
                                    'lesion_profundidad' => $value->lesion_profundidad,
                                    'valoracion_inicial' => $value->valoracion_inicial,
                                    'nota_procedimiento' => $value->nota_procedimiento,
                                    'insumos_utilizados' => $value->insumos_utilizados,
                                    'plan_manejo' => $value->plan_manejo,
                                    'recomendaciones' => $value->recomendaciones,

                                );
            }
        /* ============================== FIN FORMULARIO ENTEROSTOMAL ============================== */


        /* ============================== FORMULARIO TERAPIA OCUPACIONAL ============================== */

            $formulario_enterostomal = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_terapia_ocupacionals.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
                    ->join('softworld_formulario_terapia_ocupacionals', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_terapia_ocupacionals.if_form_to')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->join('softworld_cie10', 'softworld_formulario_terapia_ocupacionals.cie10_registro', '=', 'softworld_cie10.codigo')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($formulario_enterostomal as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'if_form_to' => $value->if_form_to,
                                    'cie10_codigo' => $value->codigo,
                                    'cie10_name' => $value->descripcion,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'fc_diagnostico' => $value->fc_diagnostico,
                                    'fr_diagnostico' => $value->fr_diagnostico,
                                    'sato2' => $value->sato2,
                                    'peso' => $value->peso,
                                    'talla' => $value->talla,
                                    'imc' => $value->imc,
                                    'to_diagnostico' => $value->to_diagnostico,
                                    'ta_diagnostico' => $value->ta_diagnostico,
                                    'diagnostico' => $value->diagnostico,
                                    'observacion' => $value->observacion,
                                    'condiciones_motoras' => $value->condiciones_motoras,
                                    'componentes_sensiorales' => $value->componentes_sensiorales,
                                    'componentes_cognitivos' => $value->componentes_cognitivos,
                                    'funcionalidad' => $value->funcionalidad,
                                    'analisis' => $value->analisis,
                                    'objetivos_tratamiento' => $value->objetivos_tratamiento,
                                    'plan_manejo_registro' => $value->plan_manejo_registro,
                                    'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                                    'interconsultas_registro' => $value->interconsultas_registro,
                                );
            }
        /* ============================== FIN FORMULARIO TERAPIA OCUPACIONAL ============================== */


        /* ============================== FORMULARIO TERAPIA RESPIRATORIA ============================== */

            $formulario_terapia_respiratoria = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_terapia_respiratorias.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
                    ->join('softworld_formulario_terapia_respiratorias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_terapia_respiratorias.if_form_tr')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->join('softworld_cie10', 'softworld_formulario_terapia_respiratorias.cie10_registro', '=', 'softworld_cie10.codigo')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($formulario_terapia_respiratoria as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,

                                    'if_form_tr' => $value->if_form_tr,
                                    'cie10_codigo' => $value->codigo,
                                    'cie10_name' => $value->descripcion,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'fc_diagnostico' => $value->fc_diagnostico,
                                    'fr_diagnostico' => $value->fr_diagnostico,
                                    'sato2' => $value->sato2,
                                    'peso' => $value->peso,
                                    'talla' => $value->talla,
                                    'imc' => $value->imc,
                                    'to_diagnostico' => $value->to_diagnostico,
                                    'ta_diagnostico' => $value->ta_diagnostico,
                                    'diagnostico' => $value->diagnostico,
                                    'inspeccion' => $value->inspeccion,
                                    'tipo_respiratorio' => $value->tipo_respiratorio,
                                    'patron_respiratorio' => $value->patron_respiratorio,
                                    'signos_ira' => $value->signos_ira,
                                    'cianosis' => $value->cianosis,
                                    'disnea' => $value->disnea,
                                    'tos' => $value->tos,
                                    'expectoriaciones' => $value->expectoriaciones,
                                    'edema' => $value->edema,
                                    'asimetria_deformidades' => $value->asimetria_deformidades,
                                    'palpacion' => $value->palpacion,
                                    'auscultacion' => $value->auscultacion,
                                    'analisis' => $value->analisis,
                                    'objetivos_tratamiento' => $value->objetivos_tratamiento,
                                    'plan_manejo_registro' => $value->plan_manejo_registro,
                                    'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                                    'interconsultas_registro' => $value->interconsultas_registro,

                                );
            }
        /* ============================== FIN FORMULARIO TERAPIA RESPIRATORIA ============================== */
        

        /* ============================== FORMULARIO EVOLUCIÓN TERAPEUTA ============================== */

            $evolucion_terapeuta = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_evolucion_terapeutas.*', 'softworld_historias_clinicas.*')
                    ->join('softworld_evolucion_terapeutas', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_evolucion_terapeutas.id_evo_tera')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($evolucion_terapeuta as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'id_evo_tera' => $value->id_evo_tera,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'ta_diagnostico' => $value->ta_diagnostico,
                                    'fc_diagnostico' => $value->fc_diagnostico,
                                    'fr_diagnostico' => $value->fr_diagnostico,
                                    'sato2' => $value->sato2,
                                    'to_diagnostico' => $value->to_diagnostico,
                                    'evolucion' => $value->evolucion,
                                    'recomendaciones' => $value->recomendaciones,
                                );
            }
        /* ============================== FIN FORMULARIO EVOLUCIÓN TERAPEUTA ============================== */


        /* ============================== FORMULARIO EVOLUCIÓN ENFERMERIA ============================== */

            $evolucion_terapeuta = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_evolucion_enfermerias.*', 'softworld_historias_clinicas.*', 'softworld_cie10.*')
                    ->join('softworld_evolucion_enfermerias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_evolucion_enfermerias.id_evo_enfer')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->join('softworld_cie10', 'softworld_evolucion_enfermerias.cie10_registro', '=', 'softworld_cie10.codigo')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($evolucion_terapeuta as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,

                                    'id_evo_enfer' => $value->id_evo_enfer,
                                    'cie10_codigo' => $value->codigo,
                                    'cie10_name' => $value->descripcion,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'ta_diagnostico' => $value->ta_diagnostico,
                                    'fc_diagnostico' => $value->fc_diagnostico,
                                    'fr_diagnostico' => $value->fr_diagnostico,
                                    'to_diagnostico' => $value->to_diagnostico,
                                    'sato2' => $value->sato2,
                                    'educacion_paciente' => $value->educacion_paciente,

                                );
            }
        /* ============================== FIN FORMULARIO EVOLUCIÓN ENFERMERIA ============================== */

        foreach ($procesos as $key => $row) {
            $aux[$key] = $row['id_proceso_evolucion'];
        }


        if(count($historia_clinica) >= 1){
            $paciente = Pacientes::where('cedula_paciente', $historia_clinica[0]->cedula_paciente)->get();
            return view('template.historia_clinica', compact('historia_clinica', 'paciente', 'procesos'));
        }else{
            return "Sin Historia clinica";
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function formulario_envio_mail_hc($cedula){
         $historia_clinica = HistoriasClinicas::where('cedula_paciente', $cedula)->get();
        return view('template.formulario_envio_historia_clinica', compact('cedula', 'historia_clinica'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function registro_formulario(Request $request, $formulario){
        
        if($formulario === 'fonoaudiologia'){
            $rules = [
                'cie10_registro' => 'required',
                'fecha_registro' => 'required',
                'hora_registro' => 'required',
                'ta_diagnostico' => 'required',
                'fc_diagnostico' => 'required',
                'fr_diagnostico' => 'required',
                'to_diagnostico' => 'required',
                'diagnostico_registro' => 'required',
                'observacion_registro' => 'required',
                'habla_registro' => 'required',
                'voz_registro' => 'required',
                'articulacion_fonemas_registro' => 'required',
                'deglucion_registro' => 'required',
                'respiracion_registro' => 'required',
                'organos_fonatorios_registro' => 'required',
                'audicion_registro' => 'required',
                'procesos_cognitivos_registro' => 'required',
                'analisis_registro' => 'required',
                'objetivos_tratamiento_registro' => 'required',
                'plan_manejo_registro' => 'required',
                'notas_aclaratorias_registro' => 'required',
                'interconsultas_registro' => 'required',
            ];

            $messages = [
                'cie10_registro' => 'campo requerido para el registro',
                'fecha_registro' => 'campo requerido para el registro',
                'hora_registro' => 'campo requerido para el registro',
                'ta_diagnostico' => 'campo requerido para el registro',
                'fc_diagnostico' => 'campo requerido para el registro',
                'fr_diagnostico' => 'campo requerido para el registro',
                'to_diagnostico' => 'campo requerido para el registro',
                'diagnostico_registro' => 'campo requerido para el registro',
                'observacion_registro' => 'campo requerido para el registro',
                'habla_registro' => 'campo requerido para el registro',
                'voz_registro' => 'campo requerido para el registro',
                'articulacion_fonemas_registro' => 'campo requerido para el registro',
                'deglucion_registro' => 'campo requerido para el registro',
                'respiracion_registro' => 'campo requerido para el registro',
                'organos_fonatorios_registro' => 'campo requerido para el registro',
                'audicion_registro' => 'campo requerido para el registro',
                'procesos_cognitivos_registro' => 'campo requerido para el registro',
                'analisis_registro' => 'campo requerido para el registro',
                'objetivos_tratamiento_registro' => 'campo requerido para el registro',
                'plan_manejo_registro' => 'campo requerido para el registro',
                'notas_aclaratorias_registro' => 'campo requerido para el registro',
                'interconsultas_registro' => 'campo requerido para el registro',
            ];

            $this->validate($request, $rules, $messages);

            $total = Fonoaudiologia::count();

            $codigoFFONO = $total+1;

            Fonoaudiologia::create([
                'id_form_fono' => 'FFON-'.$codigoFFONO,
                'cie10_registro' => $request->cie10_registro,
                'fecha_registro' => $request->fecha_registro,
                'hora_registro' => $request->hora_registro,
                'ta_diagnostico' => $request->ta_diagnostico,
                'fc_diagnostico' => $request->fc_diagnostico,
                'fr_diagnostico' => $request->fr_diagnostico,
                'to_diagnostico' => $request->to_diagnostico,
                'diagnostico_registro' => $request->diagnostico_registro,
                'observacion_registro' => $request->observacion_registro,
                'habla_registro' => $request->habla_registro,
                'voz_registro' => $request->voz_registro,
                'articulacion_fonemas_registro' => $request->articulacion_fonemas_registro,
                'deglucion_registro' => $request->deglucion_registro,
                'respiracion_registro' => $request->respiracion_registro,
                'organos_fonatorios_registro' => $request->organos_fonatorios_registro,
                'audicion_registro' => $request->audicion_registro,
                'procesos_cognitivos_registro' => $request->procesos_cognitivos_registro,
                'analisis_registro' => $request->analisis_registro,
                'objetivos_tratamiento_registro' => $request->objetivos_tratamiento_registro,
                'plan_manejo_registro' => $request->plan_manejo_registro,
                'notas_aclaratorias_registro' => $request->notas_aclaratorias_registro,
                'interconsultas_registro' => $request->interconsultas_registro,               
            ]);

            DB::table('detalle_formularios_pacientes')->insert([
                'id_formulario' => 'FFON-'.$codigoFFONO,
                'id_persona' => $request->cc,
                'tipo_formulario' => 'formulario_fonoaudiologia',
            ]);

            DB::table('softworld_procesos_evolucion')->insert([
                'titulo_proceso' => 'formulario_fonoaudiologia',
                'fk_historia_clinica' => $request->code_hc,
                'fk_proceso_evolucion_nota' => 'FFON-'.$codigoFFONO,
            ]);

            DB::table('softworld_procesos_acciones_plataforma')->insert([
                'titulo_proceso' => 'Registro formulario Fonoaudiologia',
                'descripcion_proceso' => 'El usuario '.auth()->user()->name. ' hizo un registro de un formulario de fonoaudiologia al paciente '.$request->cc." en la plataforma.",
                'codigo_usuario' =>  auth()->user()->id,
                'fecha_proceso' => date('Y-m-d H:i:s'),
            ]);
            
            return true;   
        }else if($formulario === 'medico-general'){
            $rules = [
                'cie10_registro' => 'required',
                'fecha_registro' => 'required',
                'hora_registro' => 'required',
                'fc_diagnostico' => 'required',
                'fr_diagnostico' => 'required',
                'sato2' => 'required',
                'peso' => 'required',
                'talla' => 'required',
                'ta_diagnostico' => 'required',
                'to_diagnostico' => 'required',
                'imc' => 'required',
                'diagnostico_registro' => 'required',
                'uso_previo' => 'required',
                'alergias' => 'required',
                'analisis_registro' => 'required',
                'plan_manejo_registro' => 'required',
                'notas_aclaratorias_registro' => 'required',
                'interconsultas_registro' => 'required',
            ];

            $messages = [
                'cie10_registro' => 'campo requerido para el registro',
                'fecha_registro' => 'campo requerido para el registro',
                'hora_registro' => 'campo requerido para el registro',
                'fc_diagnostico' => 'campo requerido para el registro',
                'fr_diagnostico' => 'campo requerido para el registro',
                'sato2' => 'campo requerido para el registro',
                'peso' => 'campo requerido para el registro',
                'talla' => 'campo requerido para el registro',
                'ta_diagnostico' => 'campo requerido para el registro',
                'to_diagnostico' => 'campo requerido para el registro',
                'imc' => 'campo requerido para el registro',
                'diagnostico_registro' => 'campo requerido para el registro',
                'uso_previo' => 'campo requerido para el registro',
                'alergias' => 'campo requerido para el registro',
                'analisis_registro' => 'campo requerido para el registro',
                'plan_manejo_registro' => 'campo requerido para el registro',
                'notas_aclaratorias_registro' => 'campo requerido para el registro',
                'interconsultas_registro' => 'campo requerido para el registro',
            ];

            $this->validate($request, $rules, $messages);

            $total = MedicoGeneral::count();

            $codigoMG = $total+1;

            MedicoGeneral::create([
                'id_form_medico' => 'FMG-'.$codigoMG,
                'cie10_registro' => $request->cie10_registro,
                'fecha_registro' => $request->fecha_registro,
                'hora_registro' => $request->hora_registro,
                'fc_diagnostico' => $request->fc_diagnostico,
                'fr_diagnostico' => $request->fr_diagnostico,
                'sato2' => $request->sato2,
                'peso' => $request->peso,
                'talla' => $request->talla,
                'ta_diagnostico' => $request->ta_diagnostico,
                'to_diagnostico' => $request->to_diagnostico,
                'imc' => $request->imc,
                'diagnostico_registro' => $request->diagnostico_registro,
                'uso_previo' => $request->uso_previo,
                'alergias' => $request->alergias,
                'analisis_registro' => $request->analisis_registro,
                'plan_manejo_registro' => $request->plan_manejo_registro,
                'notas_aclaratorias_registro' => $request->notas_aclaratorias_registro,
                'interconsultas_registro' => $request->interconsultas_registro,            
            ]);

            DB::table('detalle_formularios_pacientes')->insert([
                'id_formulario' => 'FMG-'.$codigoMG,
                'id_persona' => $request->cc,
                'tipo_formulario' => 'formulario_medico_general',
            ]);

            DB::table('softworld_procesos_evolucion')->insert([
                'titulo_proceso' => 'formulario_medico_general',
                'fk_historia_clinica' => $request->code_hc,
                'fk_proceso_evolucion_nota' => 'FMG-'.$codigoMG,
            ]);

            DB::table('softworld_procesos_acciones_plataforma')->insert([
                'titulo_proceso' => 'Registro formulario Medico general',
                'descripcion_proceso' => 'El usuario '.auth()->user()->name. ' hizo un registro de un formulario de Medico general al paciente '.$request->cc." en la plataforma.",
                'codigo_usuario' =>  auth()->user()->id,
                'fecha_proceso' => date('Y-m-d H:i:s'),
            ]);
            
            return true;
        }else if($formulario === 'nutricionista'){
            $rules = [
                'cie10_registro' => 'required',
                'fecha_registro' => 'required',
                'hora_registro' => 'required',
                'fc_diagnostico' => 'required',
                'fr_diagnostico' => 'required',
                'sato2' => 'required',
                'peso' => 'required',
                'talla' => 'required',
                'imc' => 'required',
                'to_diagnostico' => 'required',
                'ta_diagnostico' => 'required',
                'diagnostico_registro' => 'required',
                'estado_nutricion_actual' => 'required',
                'via_alimentacion' => 'required',
                'disfagia_valor' => 'required',
                'disfagia_porque' => 'required',
                'valoracion_nutricional' => 'required',
                'uso_previo' => 'required',
                'alergias' => 'required',
                'analisis_registro' => 'required',
                'plan_manejo_registro' => 'required',
                'notas_aclaratorias_registro' => 'required',
                'interconsultas_registro' => 'required',
            ];

            $messages = [
                'cie10_registro' => 'campo requerido para el registro',
                'fecha_registro' => 'campo requerido para el registro',
                'hora_registro' => 'campo requerido para el registro',
                'fc_diagnostico' => 'campo requerido para el registro',
                'fr_diagnostico' => 'campo requerido para el registro',
                'sato2' => 'campo requerido para el registro',
                'peso' => 'campo requerido para el registro',
                'talla' => 'campo requerido para el registro',
                'imc' => 'campo requerido para el registro',
                'to_diagnostico' => 'campo requerido para el registro',
                'ta_diagnostico' => 'campo requerido para el registro',
                'diagnostico_registro' => 'campo requerido para el registro',
                'estado_nutricion_actual' => 'campo requerido para el registro',
                'via_alimentacion' => 'campo requerido para el registro',
                'disfagia_valor' => 'campo requerido para el registro',
                'disfagia_porque' => 'campo requerido para el registro',
                'valoracion_nutricional' => 'campo requerido para el registro',
                'uso_previo' => 'campo requerido para el registro',
                'alergias' => 'campo requerido para el registro',
                'analisis_registro' => 'campo requerido para el registro',
                'plan_manejo_registro' => 'campo requerido para el registro',
                'notas_aclaratorias_registro' => 'campo requerido para el registro',
                'interconsultas_registro' => 'campo requerido para el registro',
            ];

            $this->validate($request, $rules, $messages);

            $total = Nutricionista::count();

            $codigoNutricion = $total+1;

            Nutricionista::create([
                'id_form_nutri' => 'FN-'.$codigoNutricion,
                'cie10_registro' => $request->cie10_registro,
                'fecha_registro' => $request->fecha_registro,
                'hora_registro' => $request->hora_registro,
                'fc_diagnostico' => $request->fc_diagnostico,
                'fr_diagnostico' => $request->fr_diagnostico,
                'sato2' => $request->sato2,
                'peso' => $request->peso,
                'talla' => $request->talla,
                'imc' => $request->imc,
                'to_diagnostico' => $request->to_diagnostico,
                'ta_diagnostico' => $request->ta_diagnostico,
                'diagnostico_registro' => $request->diagnostico_registro,
                'estado_nutricion_actual' => $request->estado_nutricion_actual,
                'via_alimentacion' => $request->via_alimentacion,
                'disfagia_valor' => $request->disfagia_valor,
                'disfagia_porque' => $request->disfagia_porque,
                'valoracion_nutricional' => $request->valoracion_nutricional,
                'uso_previo' => $request->uso_previo,            
                'alergias' => $request->alergias,            
                'analisis_registro' => $request->analisis_registro,            
                'plan_manejo_registro' => $request->plan_manejo_registro,            
                'notas_aclaratorias_registro' => $request->notas_aclaratorias_registro,            
                'interconsultas_registro' => $request->interconsultas_registro,            
            ]);

            DB::table('detalle_formularios_pacientes')->insert([
                'id_formulario' => 'FN-'.$codigoNutricion,
                'id_persona' => $request->cc,
                'tipo_formulario' => 'formulario_nutricion',
            ]);

            DB::table('softworld_procesos_evolucion')->insert([
                'titulo_proceso' => 'formulario_nutricion',
                'fk_historia_clinica' => $request->code_hc,
                'fk_proceso_evolucion_nota' => 'FN-'.$codigoNutricion,
            ]);

            DB::table('softworld_procesos_acciones_plataforma')->insert([
                'titulo_proceso' => 'Registro formulario Nutricion',
                'descripcion_proceso' => 'El usuario '.auth()->user()->name. ' hizo un registro de un formulario de Nutricionista al paciente '.$request->cc." en la plataforma.",
                'codigo_usuario' =>  auth()->user()->id,
                'fecha_proceso' => date('Y-m-d H:i:s'),
            ]);
            
            return true;
        }else if($formulario === 'psicologia'){
            $rules = [
                'cie10_registro' => 'required',
                'fecha_registro' => 'required',
                'hora_registro' => 'required',
                'motivo_consulta' => 'required',
                'condiciones_personales' => 'required',
                'condiciones_familiares' => 'required',
                'analisis_registro' => 'required',
                'plan_manejo_registro' => 'required',
                'notas_aclaratorias_registro' => 'required',
                'interconsultas_registro' => 'required',
            ];

            $messages = [
                'cie10_registro' => 'campo requerido para el registro',
                'fecha_registro' => 'campo requerido para el registro',
                'hora_registro' => 'campo requerido para el registro',
                'motivo_consulta' => 'campo requerido para el registro',
                'condiciones_personales' => 'campo requerido para el registro',
                'condiciones_familiares' => 'campo requerido para el registro',
                'analisis_registro' => 'campo requerido para el registro',
                'plan_manejo_registro' => 'campo requerido para el registro',
                'notas_aclaratorias_registro' => 'campo requerido para el registro',
                'interconsultas_registro' => 'campo requerido para el registro',
            ];

            $this->validate($request, $rules, $messages);

            $total = Psicologia::count();

            $codigoPsicologia = $total+1;

            Psicologia::create([
                'id_form_psi' => 'FPS-'.$codigoPsicologia,
                'cie10_registro' => $request->cie10_registro,
                'fecha_registro' => $request->fecha_registro,
                'hora_registro' => $request->hora_registro,
                'motivo_consulta' => $request->motivo_consulta,
                'condiciones_personales' => $request->condiciones_personales,
                'condiciones_familiares' => $request->condiciones_familiares,
                'analisis_registro' => $request->analisis_registro,
                'plan_manejo_registro' => $request->plan_manejo_registro,
                'notas_aclaratorias_registro' => $request->notas_aclaratorias_registro,
                'interconsultas_registro' => $request->interconsultas_registro,
            ]);

            DB::table('detalle_formularios_pacientes')->insert([
                'id_formulario' => 'FPS-'.$codigoPsicologia,
                'id_persona' => $request->cc,
                'tipo_formulario' => 'formulario_psicologia',
            ]);

            DB::table('softworld_procesos_evolucion')->insert([
                'titulo_proceso' => 'formulario_psicologia',
                'fk_historia_clinica' => $request->code_hc,
                'fk_proceso_evolucion_nota' => 'FPS-'.$codigoPsicologia,
            ]);

            DB::table('softworld_procesos_acciones_plataforma')->insert([
                'titulo_proceso' => 'Registro formulario Psicologia',
                'descripcion_proceso' => 'El usuario '.auth()->user()->name. ' hizo un registro de un formulario de Psicologia al paciente '.$request->cc." en la plataforma.",
                'codigo_usuario' =>  auth()->user()->id,
                'fecha_proceso' => date('Y-m-d H:i:s'),
            ]);
            
            return true;
        }else if($formulario === 'enterostomal'){
            $rules = [
                'cie10_registro' => 'required',
                'fecha_registro' => 'required',
                'hora_registro' => 'required',
                'area_afectada' => 'required',
                'clasificacion_lesion' => 'required',
                'clasificacion_profundidad' => 'required',
                'grosor_parcial' => 'required',
                'grosor_total' => 'required',
                'lesion_largo' => 'required',
                'lesion_ancho' => 'required',
                'lesion_profundidad' => 'required',
                'valoracion_inicial' => 'required',
                'nota_procedimiento' => 'required',
                'insumos_utilizados' => 'required',
                'plan_manejo' => 'required',
                'recomendaciones' => 'required',
            ];

            $messages = [
                'cie10_registro' => 'campo requerido para el registro',
                'fecha_registro' => 'campo requerido para el registro',
                'hora_registro' => 'campo requerido para el registro',
                'area_afectada' => 'campo requerido para el registro',
                'clasificacion_lesion' => 'campo requerido para el registro',
                'clasificacion_profundidad' => 'campo requerido para el registro',
                'grosor_parcial' => 'campo requerido para el registro',
                'grosor_total' => 'campo requerido para el registro',
                'lesion_largo' => 'campo requerido para el registro',
                'lesion_ancho' => 'campo requerido para el registro',
                'lesion_profundidad' => 'campo requerido para el registro',
                'valoracion_inicial' => 'campo requerido para el registro',
                'nota_procedimiento' => 'campo requerido para el registro',
                'insumos_utilizados' => 'campo requerido para el registro',
                'plan_manejo' => 'campo requerido para el registro',
                'recomendaciones' => 'campo requerido para el registro',
            ];

            $this->validate($request, $rules, $messages);

            $total = Enterostomal::count();

            $codigoEnterostomal = $total+1;

            Enterostomal::create([
                'id_form_enterostomal' => 'FE-'.$codigoEnterostomal,
                'cie10_registro' => $request->cie10_registro,
                'fecha_registro' => $request->fecha_registro,
                'hora_registro' => $request->hora_registro,
                'area_afectada' => $request->area_afectada,
                'clasificacion_lesion' => $request->clasificacion_lesion,
                'clasificacion_profundidad' => $request->clasificacion_profundidad,
                'grosor_parcial' => $request->grosor_parcial,
                'grosor_total' => $request->grosor_total,
                'lesion_largo' => $request->lesion_largo,
                'lesion_ancho' => $request->lesion_ancho,
                'lesion_profundidad' => $request->lesion_profundidad,
                'valoracion_inicial' => $request->valoracion_inicial,
                'nota_procedimiento' => $request->nota_procedimiento,
                'insumos_utilizados' => $request->insumos_utilizados,
                'plan_manejo' => $request->plan_manejo,
                'recomendaciones' => $request->recomendaciones,
            ]);

            DB::table('detalle_formularios_pacientes')->insert([
                'id_formulario' => 'FE-'.$codigoEnterostomal,
                'id_persona' => $request->cc,
                'tipo_formulario' => 'formulario_enterostomal',
            ]);

            DB::table('softworld_procesos_evolucion')->insert([
                'titulo_proceso' => 'formulario_enterostomal',
                'fk_historia_clinica' => $request->code_hc,
                'fk_proceso_evolucion_nota' => 'FE-'.$codigoEnterostomal,
            ]);

            DB::table('softworld_procesos_acciones_plataforma')->insert([
                'titulo_proceso' => 'Registro formulario Psicologia',
                'descripcion_proceso' => 'El usuario '.auth()->user()->name. ' hizo un registro de un formulario de Enterostomal al paciente '.$request->cc." en la plataforma.",
                'codigo_usuario' =>  auth()->user()->id,
                'fecha_proceso' => date('Y-m-d H:i:s'),
            ]);
            
            return true;
        }else if($formulario === 'fisioterapia'){
            $rules = [
                'cie10_registro' => 'required',
                'fecha_registro' => 'required',
                'hora_registro' => 'required',
                'fc_diagnostico' => 'required',
                'fr_diagnostico' => 'required',
                'sato2' => 'required',
                'peso' => 'required',
                'talla' => 'required',
                'imc' => 'required',
                'to_diagnostico' => 'required',
                'ta_diagnostico' => 'required',
                'diagnostico_registro' => 'required',
                'observacion' => 'required',
                'dolor' => 'required',
                'piel' => 'required',
                'inspeccion' => 'required',
                'palpacion' => 'required',
                'auscultacion' => 'required',
                'movilidad_articular' => 'required',
                'fuerza_muscular' => 'required',
                'tono_muscular' => 'required',
                'sensibilidad' => 'required',
                'marcha' => 'required',
                'analisis_registro' => 'required',
                'objetivos_tratamiento' => 'required',
                'plan_manejo_registro' => 'required',
                'notas_aclaratorias_registro' => 'required',
                'interconsultas_registro' => 'required',

            ];



            $messages = [
                'cie10_registro' => 'campo requerido para el registro',
                'fecha_registro' => 'campo requerido para el registro',
                'hora_registro' => 'campo requerido para el registro',
                'fc_diagnostico' => 'campo requerido para el registro',
                'fr_diagnostico' => 'campo requerido para el registro',
                'sato2' => 'campo requerido para el registro',
                'peso' => 'campo requerido para el registro',
                'talla' => 'campo requerido para el registro',
                'imc' => 'campo requerido para el registro',
                'to_diagnostico' => 'campo requerido para el registro',
                'ta_diagnostico' => 'campo requerido para el registro',
                'diagnostico_registro' => 'campo requerido para el registro',
                'observacion' => 'campo requerido para el registro',
                'dolor' => 'campo requerido para el registro',
                'piel' => 'campo requerido para el registro',
                'inspeccion' => 'campo requerido para el registro',
                'palpacion' => 'campo requerido para el registro',
                'auscultacion' => 'campo requerido para el registro',
                'movilidad_articular' => 'campo requerido para el registro',
                'fuerza_muscular' => 'campo requerido para el registro',
                'tono_muscular' => 'campo requerido para el registro',
                'sensibilidad' => 'campo requerido para el registro',
                'marcha' => 'campo requerido para el registro',
                'analisis_registro' => 'campo requerido para el registro',
                'objetivos_tratamiento' => 'campo requerido para el registro',
                'plan_manejo_registro' => 'campo requerido para el registro',
                'notas_aclaratorias_registro' => 'campo requerido para el registro',
                'interconsultas_registro' => 'campo requerido para el registro',
            ];

            $this->validate($request, $rules, $messages);

            $total = Fisioterapia::count();

            $codigoFisioterapia = $total+1;

            Fisioterapia::create([
                'id_form_fisio' => 'FFT-'.$codigoFisioterapia,
                'cie10_registro' => $request->cie10_registro,
                'fecha_registro' => $request->fecha_registro,
                'hora_registro' => $request->hora_registro,
                'fc_diagnostico' => $request->fc_diagnostico,
                'fr_diagnostico' => $request->fr_diagnostico,
                'sato2' => $request->sato2,
                'peso' => $request->peso,
                'talla' => $request->talla,
                'imc' => $request->imc,
                'to_diagnostico' => $request->to_diagnostico,
                'ta_diagnostico' => $request->ta_diagnostico,
                'diagnostico_registro' => $request->diagnostico_registro,
                'observacion' => $request->observacion,
                'dolor' => $request->dolor,
                'piel' => $request->piel,
                'inspeccion' => $request->inspeccion,
                'palpacion' => $request->palpacion,
                'auscultacion' => $request->auscultacion,
                'movilidad_articular' => $request->movilidad_articular,
                'fuerza_muscular' => $request->fuerza_muscular,
                'tono_muscular' => $request->tono_muscular,
                'sensibilidad' => $request->sensibilidad,
                'marcha' => $request->marcha,
                'analisis_registro' => $request->analisis_registro,
                'objetivos_tratamiento' => $request->objetivos_tratamiento,
                'plan_manejo_registro' => $request->plan_manejo_registro,
                'notas_aclaratorias_registro' => $request->notas_aclaratorias_registro,
                'interconsultas_registro' => $request->interconsultas_registro,

            ]);

            DB::table('detalle_formularios_pacientes')->insert([
                'id_formulario' => 'FFT-'.$codigoFisioterapia,
                'id_persona' => $request->cc,
                'tipo_formulario' => 'formulario_fisioterapia',
            ]);

            DB::table('softworld_procesos_evolucion')->insert([
                'titulo_proceso' => 'formulario_fisioterapia',
                'fk_historia_clinica' => $request->code_hc,
                'fk_proceso_evolucion_nota' => 'FFT-'.$codigoFisioterapia,
            ]);

            DB::table('softworld_procesos_acciones_plataforma')->insert([
                'titulo_proceso' => 'Registro formulario Psicologia',
                'descripcion_proceso' => 'El usuario '.auth()->user()->name. ' hizo un registro de un formulario de Fisioterapia al paciente '.$request->cc." en la plataforma.",
                'codigo_usuario' =>  auth()->user()->id,
                'fecha_proceso' => date('Y-m-d H:i:s'),
            ]);
            
            return true;
        }else if($formulario === 'terapia-ocupacional'){
            $rules = [
                'cie10_registro' => 'required',
                'fecha_registro' => 'required',
                'hora_registro' => 'required',
                'fc_diagnostico' => 'required',
                'fr_diagnostico' => 'required',
                'sato2' => 'required',
                'peso' => 'required',
                'talla' => 'required',
                'imc' => 'required',
                'to_diagnostico' => 'required',
                'ta_diagnostico' => 'required',
                'diagnostico' => 'required',
                'observacion' => 'required',
                'condiciones_motoras' => 'required',
                'componentes_sensiorales' => 'required',
                'componentes_cognitivos' => 'required',
                'funcionalidad' => 'required',
                'analisis' => 'required',
                'objetivos_tratamiento' => 'required',
                'plan_manejo_registro' => 'required',
                'notas_aclaratorias_registro' => 'required',
                'interconsultas_registro' => 'required',
            ];

            $messages = [
                'cie10_registro' => 'campo requerido para el registro',
                'fecha_registro' => 'campo requerido para el registro',
                'hora_registro' => 'campo requerido para el registro',
                'fc_diagnostico' => 'campo requerido para el registro',
                'fr_diagnostico' => 'campo requerido para el registro',
                'sato2' => 'campo requerido para el registro',
                'peso' => 'campo requerido para el registro',
                'talla' => 'campo requerido para el registro',
                'imc' => 'campo requerido para el registro',
                'to_diagnostico' => 'campo requerido para el registro',
                'ta_diagnostico' => 'campo requerido para el registro',
                'diagnostico' => 'campo requerido para el registro',
                'observacion' => 'campo requerido para el registro',
                'condiciones_motoras' => 'campo requerido para el registro',
                'componentes_sensiorales' => 'campo requerido para el registro',
                'componentes_cognitivos' => 'campo requerido para el registro',
                'funcionalidad' => 'campo requerido para el registro',
                'analisis' => 'campo requerido para el registro',
                'objetivos_tratamiento' => 'campo requerido para el registro',
                'plan_manejo_registro' => 'campo requerido para el registro',
                'notas_aclaratorias_registro' => 'campo requerido para el registro',
                'interconsultas_registro' => 'campo requerido para el registro',
            ];

            $this->validate($request, $rules, $messages);

            $total = TerapiaOcupacional::count();

            $codigoTerapiaOcupacional = $total+1;

            TerapiaOcupacional::create([
                'if_form_to' => 'FTO-'.$codigoTerapiaOcupacional,
                'cie10_registro' => $request->cie10_registro,
                'fecha_registro' => $request->fecha_registro,
                'hora_registro' => $request->hora_registro,
                'fc_diagnostico' => $request->fc_diagnostico,
                'fr_diagnostico' => $request->fr_diagnostico,
                'sato2' => $request->sato2,
                'peso' => $request->peso,
                'talla' => $request->talla,
                'imc' => $request->imc,
                'to_diagnostico' => $request->to_diagnostico,
                'ta_diagnostico' => $request->ta_diagnostico,
                'diagnostico' => $request->diagnostico,
                'observacion' => $request->observacion,
                'condiciones_motoras' => $request->condiciones_motoras,
                'componentes_sensiorales' => $request->componentes_sensiorales,
                'componentes_cognitivos' => $request->componentes_cognitivos,
                'funcionalidad' => $request->funcionalidad,
                'analisis' => $request->analisis,
                'objetivos_tratamiento' => $request->objetivos_tratamiento,
                'plan_manejo_registro' => $request->plan_manejo_registro,
                'notas_aclaratorias_registro' => $request->notas_aclaratorias_registro,
                'interconsultas_registro' => $request->interconsultas_registro,
            ]);

            DB::table('detalle_formularios_pacientes')->insert([
                'id_formulario' => 'FTO-'.$codigoTerapiaOcupacional,
                'id_persona' => $request->cc,
                'tipo_formulario' => 'formulario_terapia_ocupacional',
            ]);

            DB::table('softworld_procesos_evolucion')->insert([
                'titulo_proceso' => 'formulario_terapia_ocupacional',
                'fk_historia_clinica' => $request->code_hc,
                'fk_proceso_evolucion_nota' => 'FTO-'.$codigoTerapiaOcupacional,
            ]);

            DB::table('softworld_procesos_acciones_plataforma')->insert([
                'titulo_proceso' => 'Registro formulario terapia ocupacional',
                'descripcion_proceso' => 'El usuario '.auth()->user()->name. ' hizo un registro de un formulario de terapia ocupacional al paciente '.$request->cc." en la plataforma.",
                'codigo_usuario' =>  auth()->user()->id,
                'fecha_proceso' => date('Y-m-d H:i:s'),
            ]);
            
            return true;
        }else if($formulario === 'terapia-respiratoria'){
            $rules = [
                'cie10_registro' => 'required',
                'fecha_registro' => 'required',
                'hora_registro' => 'required',
                'fc_diagnostico' => 'required',
                'fr_diagnostico' => 'required',
                'sato2' => 'required',
                'peso' => 'required',
                'talla' => 'required',
                'imc' => 'required',
                'to_diagnostico' => 'required',
                'ta_diagnostico' => 'required',
                'diagnostico' => 'required',
                'inspeccion' => 'required',
                'tipo_respiratorio' => 'required',
                'patron_respiratorio' => 'required',
                'signos_ira' => 'required',
                'cianosis' => 'required',
                'disnea' => 'required',
                'tos' => 'required',
                'expectoriaciones' => 'required',
                'edema' => 'required',
                'asimetria_deformidades' => 'required',
                'palpacion' => 'required',
                'auscultacion' => 'required',
                'analisis' => 'required',
                'objetivos_tratamiento' => 'required',
                'plan_manejo_registro' => 'required',
                'notas_aclaratorias_registro' => 'required',
                'interconsultas_registro' => 'required',
            ];

            $messages = [
                'cie10_registro' => 'campo requerido para el registro',
                'fecha_registro' => 'campo requerido para el registro',
                'hora_registro' => 'campo requerido para el registro',
                'fc_diagnostico' => 'campo requerido para el registro',
                'fr_diagnostico' => 'campo requerido para el registro',
                'sato2' => 'campo requerido para el registro',
                'peso' => 'campo requerido para el registro',
                'talla' => 'campo requerido para el registro',
                'imc' => 'campo requerido para el registro',
                'to_diagnostico' => 'campo requerido para el registro',
                'ta_diagnostico' => 'campo requerido para el registro',
                'diagnostico' => 'campo requerido para el registro',
                'inspeccion' => 'campo requerido para el registro',
                'tipo_respiratorio' => 'campo requerido para el registro',
                'patron_respiratorio' => 'campo requerido para el registro',
                'signos_ira' => 'campo requerido para el registro',
                'cianosis' => 'campo requerido para el registro',
                'disnea' => 'campo requerido para el registro',
                'tos' => 'campo requerido para el registro',
                'expectoriaciones' => 'campo requerido para el registro',
                'edema' => 'campo requerido para el registro',
                'asimetria_deformidades' => 'campo requerido para el registro',
                'palpacion' => 'campo requerido para el registro',
                'auscultacion' => 'campo requerido para el registro',
                'analisis' => 'campo requerido para el registro',
                'objetivos_tratamiento' => 'campo requerido para el registro',
                'plan_manejo_registro' => 'campo requerido para el registro',
                'notas_aclaratorias_registro' => 'campo requerido para el registro',
                'interconsultas_registro' => 'campo requerido para el registro',
            ];

            $this->validate($request, $rules, $messages);

            $total = TerapiaRespiratoria::count();

            $codigoTerapiaRespiratoria = $total+1;

            TerapiaRespiratoria::create([
                'if_form_tr' => 'FTR-'.$codigoTerapiaRespiratoria,
                'cie10_registro' => $request->cie10_registro,
                'fecha_registro' => $request->fecha_registro,
                'hora_registro' => $request->hora_registro,
                'fc_diagnostico' => $request->fc_diagnostico,
                'fr_diagnostico' => $request->fr_diagnostico,
                'sato2' => $request->sato2,
                'peso' => $request->peso,
                'talla' => $request->talla,
                'imc' => $request->imc,
                'to_diagnostico' => $request->to_diagnostico,
                'ta_diagnostico' => $request->ta_diagnostico,
                'diagnostico' => $request->diagnostico,
                'inspeccion' => $request->inspeccion,
                'tipo_respiratorio' => $request->tipo_respiratorio,
                'patron_respiratorio' => $request->patron_respiratorio,
                'signos_ira' => $request->signos_ira,
                'cianosis' => $request->cianosis,
                'disnea' => $request->disnea,
                'tos' => $request->tos,
                'expectoriaciones' => $request->expectoriaciones,
                'edema' => $request->edema,
                'asimetria_deformidades' => $request->asimetria_deformidades,
                'palpacion' => $request->palpacion,
                'auscultacion' => $request->auscultacion,
                'analisis' => $request->analisis,
                'objetivos_tratamiento' => $request->objetivos_tratamiento,
                'plan_manejo_registro' => $request->plan_manejo_registro,
                'notas_aclaratorias_registro' => $request->notas_aclaratorias_registro,
                'interconsultas_registro' => $request->interconsultas_registro,
            ]);

            DB::table('detalle_formularios_pacientes')->insert([
                'id_formulario' => 'FTR-'.$codigoTerapiaRespiratoria,
                'id_persona' => $request->cc,
                'tipo_formulario' => 'formulario_terapia_respiratoria',
            ]);

            DB::table('softworld_procesos_evolucion')->insert([
                'titulo_proceso' => 'formulario_terapia_respiratoria',
                'fk_historia_clinica' => $request->code_hc,
                'fk_proceso_evolucion_nota' => 'FTR-'.$codigoTerapiaRespiratoria,
            ]);

            DB::table('softworld_procesos_acciones_plataforma')->insert([
                'titulo_proceso' => 'Registro formulario terapia respiratoria',
                'descripcion_proceso' => 'El usuario '.auth()->user()->name. ' hizo un registro de un formulario de terapia respiratoria al paciente '.$request->cc." en la plataforma.",
                'codigo_usuario' =>  auth()->user()->id,
                'fecha_proceso' => date('Y-m-d H:i:s'),
            ]);
            
            return true;
        }else if($formulario === 'evolucion_terapeuta'){
            $rules = [
                'fecha' => 'required',
                'hora' => 'required',
                'ta' => 'required',
                'fc' => 'required',
                'fr' => 'required',
                'sato2' => 'required',
                'to' => 'required',
                'evolucion' => 'required',
                'recomendaciones' => 'required',
            ];

            $messages = [
                'fecha' => 'campo requerido para el registro',
                'hora' => 'campo requerido para el registro',
                'ta' => 'campo requerido para el registro',
                'fc' => 'campo requerido para el registro',
                'fr' => 'campo requerido para el registro',
                'sato2' => 'campo requerido para el registro',
                'to' => 'campo requerido para el registro',
                'evolucion' => 'campo requerido para el registro',
                'recomendaciones' => 'campo requerido para el registro',
            ];

            $this->validate($request, $rules, $messages);

            $total = EvolucionTerapeuta::count();

            $codigoEvoTera = $total+1;

            EvolucionTerapeuta::create([
                'id_evo_tera' => 'EVOTE-'.$codigoEvoTera,
                'fecha_registro' => $request->fecha,
                'hora_registro' => $request->hora,
                'ta_diagnostico' => $request->ta,
                'fc_diagnostico' => $request->fc,
                'fr_diagnostico' => $request->fr,
                'sato2' => $request->sato2,
                'to_diagnostico' => $request->to,
                'evolucion' => $request->evolucion,
                'recomendaciones' => $request->recomendaciones,
            ]);

            DB::table('detalle_formularios_pacientes')->insert([
                'id_formulario' => 'EVOTE-'.$codigoEvoTera,
                'id_persona' => $request->cc,
                'tipo_formulario' => 'evolucion_terapeuta',
            ]);

            DB::table('softworld_procesos_evolucion')->insert([
                'titulo_proceso' => 'evolucion_terapeuta',
                'fk_historia_clinica' => $request->code_hc,
                'fk_proceso_evolucion_nota' => 'EVOTE-'.$codigoEvoTera,
            ]);

            DB::table('softworld_procesos_acciones_plataforma')->insert([
                'titulo_proceso' => 'Registro formulario evolucion terapeuta',
                'descripcion_proceso' => 'El usuario '.auth()->user()->name. ' hizo un registro de un formulario de evolucion terapeuta al paciente '.$request->cc." en la plataforma.",
                'codigo_usuario' =>  auth()->user()->id,
                'fecha_proceso' => date('Y-m-d H:i:s'),
            ]);
            
            return true;
        }else if($formulario === 'evolucion_enfermeria'){
            $rules = [
                'cie10_registro' => 'required',
                'fecha_registro' => 'required',
                'hora_registro' => 'required',
                'ta_diagnostico' => 'required',
                'fc_diagnostico' => 'required',
                'fr_diagnostico' => 'required',
                'to_diagnostico' => 'required',
                'sato2' => 'required',
                'educacion_paciente' => 'required',
            ];

            $messages = [
                'cie10_registro' => 'campo requerido para el registro',
                'fecha_registro' => 'campo requerido para el registro',
                'hora_registro' => 'campo requerido para el registro',
                'ta_diagnostico' => 'campo requerido para el registro',
                'fc_diagnostico' => 'campo requerido para el registro',
                'fr_diagnostico' => 'campo requerido para el registro',
                'to_diagnostico' => 'campo requerido para el registro',
                'sato2' => 'campo requerido para el registro',
                'educacion_paciente' => 'campo requerido para el registro',
            ];

            $this->validate($request, $rules, $messages);

            $total = EvolucionEnfermeria::count();

            $codigoEvoEnfer = $total+1;

            EvolucionEnfermeria::create([
                'id_evo_enfer' => 'EVOENF-'.$codigoEvoEnfer,
                'cie10_registro' => $request->cie10_registro,
                'fecha_registro' => $request->fecha_registro,
                'hora_registro' => $request->hora_registro,
                'ta_diagnostico' => $request->ta_diagnostico,
                'fc_diagnostico' => $request->fc_diagnostico,
                'fr_diagnostico' => $request->fr_diagnostico,
                'to_diagnostico' => $request->to_diagnostico,
                'sato2' => $request->sato2,
                'educacion_paciente' => $request->educacion_paciente,
            ]);

            DB::table('detalle_formularios_pacientes')->insert([
                'id_formulario' => 'EVOENF-'.$codigoEvoEnfer,
                'id_persona' => $request->cc,
                'tipo_formulario' => 'evolucion_enfermeria',
            ]);

            DB::table('softworld_procesos_evolucion')->insert([
                'titulo_proceso' => 'evolucion_enfermeria',
                'fk_historia_clinica' => $request->code_hc,
                'fk_proceso_evolucion_nota' => 'EVOENF-'.$codigoEvoEnfer,
            ]);

            DB::table('softworld_procesos_acciones_plataforma')->insert([
                'titulo_proceso' => 'Registro formulario evolucion enfermeria',
                'descripcion_proceso' => 'El usuario '.auth()->user()->name. ' hizo un registro de un formulario de evolucion enfermeria al paciente '.$request->cc." en la plataforma.",
                'codigo_usuario' =>  auth()->user()->id,
                'fecha_proceso' => date('Y-m-d H:i:s'),
            ]);
            
            return true;
        }else{

        }
    }

    public function exportar_pdf($id){
        
        $procesos = [];
        date_default_timezone_set('America/Bogota');
        $historia_clinica = HistoriasClinicas::where('cedula_paciente', $id)->get();

        /* ============================== FORMULARIO NOTAS RAPIDAS ============================== */
            $notas = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_notas_pacientes.*', 'softworld_historias_clinicas.*')
                    ->join('softworld_notas_pacientes', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_notas_pacientes.id_nota_paciente')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();
            foreach ($notas as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'titulo_nota' => $value->titulo_nota,
                                    'descripcion_nota' => $value->descripcion_nota,
                                    'fecha_nota' => $value->fecha_nota,
                                    'usuario_responsable' => $value->usuario_responsable,
                                    'estado_nota' => $value->estado_nota);
            }
        /* ============================== FIN FORMULARIO NOTAS RAPIDAS ============================== */


        /* ============================== FORMULARIO FONOAUDILOGIA ============================== */
            $formularios = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_fonoaudiologias.*', 'softworld_historias_clinicas.*')
                    ->join('softworld_formulario_fonoaudiologias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_fonoaudiologias.id_form_fono')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();
            foreach ($formularios as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'cie10_registro' => $value->cie10_registro,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'ta_diagnostico' => $value->ta_diagnostico,
                                    'fc_diagnostico' => $value->fc_diagnostico,
                                    'fr_diagnostico' => $value->fr_diagnostico,
                                    'to_diagnostico' => $value->to_diagnostico,
                                    'diagnostico_registro' => $value->diagnostico_registro,
                                    'observacion_registro' => $value->observacion_registro,
                                    'habla_registro' => $value->habla_registro,
                                    'voz_registro' => $value->voz_registro,
                                    'articulacion_fonemas_registro' => $value->articulacion_fonemas_registro,
                                    'deglucion_registro' => $value->deglucion_registro,
                                    'respiracion_registro' => $value->respiracion_registro,
                                    'organos_fonatorios_registro' => $value->organos_fonatorios_registro,
                                    'audicion_registro' => $value->audicion_registro,
                                    'procesos_cognitivos_registro' => $value->procesos_cognitivos_registro,
                                    'analisis_registro' => $value->analisis_registro,
                                    'objetivos_tratamiento_registro' => $value->objetivos_tratamiento_registro,
                                    'plan_manejo_registro' => $value->plan_manejo_registro,
                                    'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                                    'interconsultas_registro' => $value->interconsultas_registro,
                                );
            }
        /* ============================== FIN FORMULARIO FONOAUDILOGIA ============================== */


        /* ============================== FORMULARIO MEDICO GENERAL ============================== */

            $formulario_MG = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_medico_generals.*', 'softworld_historias_clinicas.*')
                    ->join('softworld_formulario_medico_generals', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_medico_generals.id_form_medico')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($formulario_MG as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'cie10_registro' => $value->cie10_registro,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'fc_diagnostico' => $value->fc_diagnostico,
                                    'fr_diagnostico' => $value->fr_diagnostico,
                                    'sato2' => $value->sato2,
                                    'peso' => $value->peso,
                                    'talla' => $value->talla,
                                    'ta_diagnostico' => $value->ta_diagnostico,
                                    'to_diagnostico' => $value->to_diagnostico,
                                    'imc' => $value->imc,
                                    'diagnostico_registro' => $value->diagnostico_registro,
                                    'uso_previo' => $value->uso_previo,
                                    'alergias' => $value->alergias,
                                    'analisis_registro' => $value->analisis_registro,
                                    'plan_manejo_registro' => $value->plan_manejo_registro,
                                    'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                                    'interconsultas_registro' => $value->interconsultas_registro,
                                );
            }
        /* ============================== FIN FORMULARIO MEDICO GENERAL ============================== */


        /* ============================== FORMULARIO PSICOLOGIA ============================== */

            $formulario_PSI = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_psicologias.*', 'softworld_historias_clinicas.*')
                    ->join('softworld_formulario_psicologias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_psicologias.id_form_psi')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($formulario_PSI as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'cie10_registro' => $value->cie10_registro,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'motivo_consulta' => $value->motivo_consulta,
                                    'condiciones_personales' => $value->condiciones_personales,
                                    'condiciones_familiares' => $value->condiciones_familiares,
                                    'analisis_registro' => $value->analisis_registro,
                                    'plan_manejo_registro' => $value->plan_manejo_registro,
                                    'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                                    'interconsultas_registro' => $value->interconsultas_registro,
                                );
            }
        /* ============================== FIN FORMULARIO PSICOLOGIA ============================== */


        /* ============================== FORMULARIO NUTRICION ============================== */

            $formulario_nutricion = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_nutricionistas.*', 'softworld_historias_clinicas.*')
                    ->join('softworld_formulario_nutricionistas', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_nutricionistas.id_form_nutri')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($formulario_nutricion as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'cie10_registro' => $value->cie10_registro,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'fc_diagnostico' => $value->fc_diagnostico,
                                    'fr_diagnostico' => $value->fr_diagnostico,
                                    'sato2' => $value->sato2,
                                    'peso' => $value->peso,
                                    'talla' => $value->talla,
                                    'imc' => $value->imc,
                                    'to_diagnostico' => $value->to_diagnostico,
                                    'ta_diagnostico' => $value->ta_diagnostico,
                                    'diagnostico_registro' => $value->diagnostico_registro,
                                    'estado_nutricion_actual' => $value->estado_nutricion_actual,
                                    'via_alimentacion' => $value->via_alimentacion,
                                    'disfagia_valor' => $value->disfagia_valor,
                                    'disfagia_porque' => $value->disfagia_porque,
                                    'valoracion_nutricional' => $value->valoracion_nutricional,
                                    'uso_previo' => $value->uso_previo,
                                    'alergias' => $value->alergias,
                                    'analisis_registro' => $value->analisis_registro,
                                    'plan_manejo_registro' => $value->plan_manejo_registro,
                                    'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                                    'interconsultas_registro' => $value->interconsultas_registro,
                                );
            }
        /* ============================== FIN FORMULARIO NUTRICION ============================== */


        /* ============================== FORMULARIO FISIOTERAPIA ============================== */

            $formulario_nutricion = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_fisioterapias.*', 'softworld_historias_clinicas.*')
                    ->join('softworld_formulario_fisioterapias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_fisioterapias.id_form_fisio')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($formulario_nutricion as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'cie10_registro' => $value->cie10_registro,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'fc_diagnostico' => $value->fc_diagnostico,
                                    'fr_diagnostico' => $value->fr_diagnostico,
                                    'sato2' => $value->sato2,
                                    'peso' => $value->peso,
                                    'talla' => $value->talla,
                                    'imc' => $value->imc,
                                    'to_diagnostico' => $value->to_diagnostico,
                                    'ta_diagnostico' => $value->ta_diagnostico,
                                    'diagnostico_registro' => $value->diagnostico_registro,
                                    'observacion' => $value->observacion,
                                    'dolor' => $value->dolor,
                                    'piel' => $value->piel,
                                    'inspeccion' => $value->inspeccion,
                                    'palpacion' => $value->palpacion,
                                    'auscultacion' => $value->auscultacion,
                                    'movilidad_articular' => $value->movilidad_articular,
                                    'fuerza_muscular' => $value->fuerza_muscular,
                                    'tono_muscular' => $value->tono_muscular,
                                    'sensibilidad' => $value->sensibilidad,
                                    'marcha' => $value->marcha,
                                    'analisis_registro' => $value->analisis_registro,
                                    'objetivos_tratamiento' => $value->objetivos_tratamiento,
                                    'plan_manejo_registro' => $value->plan_manejo_registro,
                                    'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                                    'interconsultas_registro' => $value->interconsultas_registro,
                                );
            }
        /* ============================== FIN FORMULARIO FISIOTERAPIA ============================== */


        /* ============================== FORMULARIO ENTEROSTOMAL ============================== */

            $formulario_enterostomal = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_enterostomals.*', 'softworld_historias_clinicas.*')
                    ->join('softworld_formulario_enterostomals', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_enterostomals.id_form_enterostomal')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($formulario_enterostomal as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'cie10_registro' => $value->cie10_registro,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'area_afectada' => $value->area_afectada,
                                    'clasificacion_lesion' => $value->clasificacion_lesion,
                                    'clasificacion_profundidad' => $value->clasificacion_profundidad,
                                    'grosor_parcial' => $value->grosor_parcial,
                                    'grosor_total' => $value->grosor_total,
                                    'lesion_largo' => $value->lesion_largo,
                                    'lesion_ancho' => $value->lesion_ancho,
                                    'lesion_profundidad' => $value->lesion_profundidad,
                                    'valoracion_inicial' => $value->valoracion_inicial,
                                    'nota_procedimiento' => $value->nota_procedimiento,
                                    'insumos_utilizados' => $value->insumos_utilizados,
                                    'plan_manejo' => $value->plan_manejo,
                                    'recomendaciones' => $value->recomendaciones,
                                );
            }
        /* ============================== FIN FORMULARIO ENTEROSTOMAL ============================== */


        /* ============================== FORMULARIO TERAPIA OCUPACIONAL ============================== */

            $formulario_enterostomal = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_terapia_ocupacionals.*', 'softworld_historias_clinicas.*')
                    ->join('softworld_formulario_terapia_ocupacionals', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_terapia_ocupacionals.if_form_to')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($formulario_enterostomal as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'cie10_registro' => $value->cie10_registro,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'fc_diagnostico' => $value->fc_diagnostico,
                                    'fr_diagnostico' => $value->fr_diagnostico,
                                    'sato2' => $value->sato2,
                                    'peso' => $value->peso,
                                    'talla' => $value->talla,
                                    'imc' => $value->imc,
                                    'to_diagnostico' => $value->to_diagnostico,
                                    'ta_diagnostico' => $value->ta_diagnostico,
                                    'diagnostico' => $value->diagnostico,
                                    'observacion' => $value->observacion,
                                    'condiciones_motoras' => $value->condiciones_motoras,
                                    'componentes_sensiorales' => $value->componentes_sensiorales,
                                    'componentes_cognitivos' => $value->componentes_cognitivos,
                                    'funcionalidad' => $value->funcionalidad,
                                    'analisis' => $value->analisis,
                                    'objetivos_tratamiento' => $value->objetivos_tratamiento,
                                    'plan_manejo_registro' => $value->plan_manejo_registro,
                                    'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                                    'interconsultas_registro' => $value->interconsultas_registro,
                                );
            }
        /* ============================== FIN FORMULARIO TERAPIA OCUPACIONAL ============================== */


        /* ============================== FORMULARIO TERAPIA RESPIRATORIA ============================== */

            $formulario_terapia_respiratoria = DB::table('softworld_procesos_evolucion')
                    ->select('softworld_procesos_evolucion.*', 'softworld_formulario_terapia_respiratorias.*', 'softworld_historias_clinicas.*')
                    ->join('softworld_formulario_terapia_respiratorias', 'softworld_procesos_evolucion.fk_proceso_evolucion_nota', '=', 'softworld_formulario_terapia_respiratorias.if_form_tr')
                    ->join('softworld_historias_clinicas', 'softworld_procesos_evolucion.fk_historia_clinica', '=', 'softworld_historias_clinicas.codigo_historia_clinica')
                    ->where('softworld_historias_clinicas.cedula_paciente', '=', $id)
                    ->get();

            foreach ($formulario_terapia_respiratoria as $value) {
                $procesos[] = array('titulo_proceso' => $value->titulo_proceso, 
                                    'id_proceso_evolucion' => $value->id_proceso_evolucion,
                                    'cie10_registro' => $value->cie10_registro,
                                    'fecha_registro' => $value->fecha_registro,
                                    'hora_registro' => $value->hora_registro,
                                    'fc_diagnostico' => $value->fc_diagnostico,
                                    'fr_diagnostico' => $value->fr_diagnostico,
                                    'sato2' => $value->sato2,
                                    'peso' => $value->peso,
                                    'talla' => $value->talla,
                                    'imc' => $value->imc,
                                    'to_diagnostico' => $value->to_diagnostico,
                                    'ta_diagnostico' => $value->ta_diagnostico,
                                    'diagnostico' => $value->diagnostico,
                                    'inspeccion' => $value->inspeccion,
                                    'tipo_respiratorio' => $value->tipo_respiratorio,
                                    'patron_respiratorio' => $value->patron_respiratorio,
                                    'signos_ira' => $value->signos_ira,
                                    'cianosis' => $value->cianosis,
                                    'disnea' => $value->disnea,
                                    'tos' => $value->tos,
                                    'expectoriaciones' => $value->expectoriaciones,
                                    'edema' => $value->edema,
                                    'asimetria_deformidades' => $value->asimetria_deformidades,
                                    'palpacion' => $value->palpacion,
                                    'auscultacion' => $value->auscultacion,
                                    'analisis' => $value->analisis,
                                    'objetivos_tratamiento' => $value->objetivos_tratamiento,
                                    'plan_manejo_registro' => $value->plan_manejo_registro,
                                    'notas_aclaratorias_registro' => $value->notas_aclaratorias_registro,
                                    'interconsultas_registro' => $value->interconsultas_registro,
                                );
            }
        /* ============================== FIN FORMULARIO TERAPIA RESPIRATORIA ============================== */
        
        foreach ($procesos as $key => $row) {
            $aux[$key] = $row['id_proceso_evolucion'];
        }

        //array_multisort($aux, SORT_ASC, $procesos);


        if(count($historia_clinica) >= 1){
            $paciente = Pacientes::where('cedula_paciente', $id)->get();

            $pdf = PDF::loadView('template.historia_clinica_pdf', compact('historia_clinica', 'paciente', 'procesos'))->setPaper('a4', 'portrait');
                
            $name = $historia_clinica[0]->codigo_historia_clinica."-".date('d-m-y H:i:s').".pdf";
            Storage::disk('s3')->put($name, $pdf->output());
            
            return "https://s3.amazonaws.com/cytcuidadointegral.com/".$name; 

        }else{
            return "Sin Historia clinica";
        }

    }
}
