<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\softworld_modulos AS Modulos;
use DB;

class RoleController extends Controller
{
    public function index(){

        $roles = Role::simplePaginate(2);
    	return view('seguridad.roles.index', compact('roles'));
    }

    public function listado(){
    	return Role::paginate();
    }

    public function create(){
    	$permisos = Permission::all();
    	$modulos = Modulos::all();

    	return view('seguridad.roles.create', compact('permisos', 'modulos'));
    }

    public function store(Request $request){

    	$rules = [
            'nombre_rol' => 'required|unique:roles,name',
        ];

        $messages = [
            'nombre_rol.required'       => 'El nombre del Rol es importante.',
            'nombre_rol.unique'       => 'El nombre del Rol debe ser unico.',
        ];

        $this->validate($request, $rules, $messages);

        $rol = Role::create([
        	'name' => $request->nombre_rol
        ]);

        if($request->permisos){
        	foreach ($request->permisos as $value) {
        		$rol->givePermissionTo($value);
        	}
        }

    	return true;
    }

    public function edit($id){

        $permisos_asignados = [];

        $permisos = Permission::all();
        $modulos = Modulos::all();

        $sql_permisos_asignados = DB::table('role_has_permissions')
                                        ->select('permissions.*', 'role_has_permissions.*')
                                        ->join('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
                                        ->where('role_id', $id)->get();

        foreach ($sql_permisos_asignados as $value) {
            $permisos_asignados[] = $value->name;
        }

        $rol = Role::where('id', $id)->get();
        return view('seguridad.roles.edit', compact('rol', 'permisos', 'modulos', 'permisos_asignados'));
    }

    public function update (Request $request){
        
        //{"name":"dddddddddddddd","guard_name":"web","updated_at":"2021-08-12T11:14:19.000000Z","created_at":"2021-08-12T11:14:19.000000Z","id":4}

        $rules = [
            'nombre_rol' => 'required',
        ];

        $messages = [
            'nombre_rol.required' => 'El nombre del Rol es importante.',
        ];

        $this->validate($request, $rules, $messages);

        Role::where('id', $request->codigo)->update([
            'name' => $request->nombre_rol
        ]);

        DB::table('role_has_permissions')->where('role_id', $request->codigo)->delete();

        $roles = Role::where('id', $request->codigo)->get();

        foreach ($roles as $value) {
            $rol = $value; 
        }
        
        if($request->permisos){
            foreach ($request->permisos as $value) {
                $rol->givePermissionTo($value);
            }
        }

        return true;
    }

    public function show($id){
        $permisos_asignados = [];

        $permisos = Permission::all();
        $modulos = Modulos::all();

        $sql_permisos_asignados = DB::table('role_has_permissions')
                                        ->select('permissions.*', 'role_has_permissions.*')
                                        ->join('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
                                        ->where('role_id', $id)->get();

        foreach ($sql_permisos_asignados as $value) {
            $permisos_asignados[] = $value->name;
        }

        $rol = Role::where('id', $id)->get();
        return view('seguridad.roles.show', compact('rol', 'permisos', 'modulos', 'permisos_asignados'));
    }
}
