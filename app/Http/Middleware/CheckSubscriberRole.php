<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckSubscriberRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->hasRole('subscriber')) {
            // Si el usuario tiene el rol "subscriber", redirigir a una página de acceso denegado o mostrar un mensaje de error
            return redirect()->route('access-denied');
        }

        // Si el usuario no tiene el rol "subscriber", continuar con la solicitud
        return $next($request);
    }
}
