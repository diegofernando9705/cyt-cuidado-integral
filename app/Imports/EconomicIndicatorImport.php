<?php

namespace App\Imports;

use App\Models\SoftworldEconomicIndicators;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Carbon\Carbon;

class EconomicIndicatorImport implements ToCollection
{
    protected $indicadorEconomico;

    public function __construct($indicadorEconomico)
    {
        $this->indicadorEconomico = $indicadorEconomico;
    }

    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        foreach ($collection as $row) {
            $fechaInicial =  Carbon::parse($row[0])->format('Y-m-d');
            $fechaFinal =  Carbon::parse($row[1])->format('Y-m-d');
            $valorIndicador =  $row[2];
            $porcentajeIndicador =  $row[3];

            SoftworldEconomicIndicators::create([
                'date_start_indicator' => $fechaInicial,
                'date_end_indicator' => $fechaFinal,
                'value_start_indicator' => $valorIndicador,
                'description_start_indicator' => $this->indicadorEconomico,
                'percentage_start_indicator' => $porcentajeIndicador,
                'status_start_indicator' => 1,
            ]);
        }
    }
}
