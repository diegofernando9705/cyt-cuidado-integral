<?php

namespace App\Services;

use Epayco\Epayco;

class EpaycoService
{
    protected $epayco;

    public function __construct()
    {
        $this->epayco = new Epayco(array(
            "apiKey" => config("app.EPAYCO_PUBLIC_KEY"),
            "privateKey" => config("app.EPAYCO_PRIVATE_KEY"),
            "lenguage" => "ES",
            "test" => true
        ));
    }

    public function getEpaycoInstance()
    {
        return $this->epayco;
    }

    public function createCreditCardToken($data)
    {
        return $this->epayco->token->create($data);
    }

    public function createEpaycoCustomer($data)
    {
        return $this->epayco->customer->create($data);
    }

    public function createEpaycoSubscription($data)
    {
        return $this->epayco->subscriptions->create($data);
    }

    public function chargeSubscription($data)
    {
        return $this->epayco->subscriptions->charge($data);
    }

    public function cancelEpaycoSubscription($subscriptionId)
    {
        return $this->epayco->subscriptions->cancel($subscriptionId);
    }
}
