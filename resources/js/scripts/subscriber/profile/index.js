$(document).on("click", ".habilitar_campos_form_usuario", function () {
    const inputs = document.querySelectorAll('.input_form_usuario, #actualizar_informacion_button_usuario, .inhabilitar_campos_form_usuario');

    // Iterar sobre cada elemento y quitar el atributo "disabled"
    inputs.forEach(input => {
        input.removeAttribute('disabled');
    });
});

$(document).on("click", ".inhabilitar_campos_form_usuario", function () {
    const inputs = document.querySelectorAll('.input_form_usuario, #actualizar_informacion_button_usuario, .inhabilitar_campos_form_usuario');

    // Iterar sobre cada elemento y quitar el atributo "disabled"
    inputs.forEach(input => {
        $(input).prop('disabled', true);
    });
});

const formulario_usuario = document.querySelectorAll('.formulario_usuario');

// Loop over them and prevent submission
Array.prototype.slice.call(formulario_usuario).forEach((form) => {
    form.addEventListener('submit', (event) => {

        if (!form.checkValidity()) {
            event.stopPropagation();
        } else {
            document.getElementById("actualizar_informacion_button_usuario").disabled = true;
            document.getElementById("inhabilitar_campos_form_usuario").disabled = true;

            document.getElementById("name").disabled = true;
            document.getElementById("correo_usuario").disabled = true;
            document.getElementById("password").disabled = true;
            document.getElementById("password_confirmation").disabled = true;


            var paqueteDeDatos = new FormData();

            paqueteDeDatos.append('name', $('#name').val());
            paqueteDeDatos.append('correo_usuario', $('#correo_usuario').val());
            paqueteDeDatos.append('password', $('#password').val());
            paqueteDeDatos.append('password_confirmation', $('#password_confirmation').val());

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                }
            });

            $.ajax({
                type: 'POST',
                url: "/subscriber/profile/edit/update/user",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: paqueteDeDatos,
                processData: false,
                contentType: false,
                success: function (data) {
                    Swal.fire({
                        title: "Información actualizada",
                        text: "Se actualizó la información del usuario",
                        icon: "success",
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                    });

                    setTimeout(() => {
                        $("#password").val();
                        $("#password_confirmation").val();
                    }, 1000);
                },
                error: function (err) {
                    document.getElementById("actualizar_informacion_button_usuario").disabled = false;

                    document.getElementById("name").disabled = false;
                    document.getElementById("correo_usuario").disabled = false;
                    document.getElementById("password").disabled = false;
                    document.getElementById("password_confirmation").disabled = false;

                    errorHttp(err.status);
                    if (err.status == 422) {
                        $(".errores").hide();
                        console.log(err.responseJSON);
                        $('#success_message').fadeIn().html(err.responseJSON
                            .message);
                        console.warn(err.responseJSON.errors);
                        $.each(err.responseJSON.errors, function (i, error) {
                            var el = $(document).find('[name="' +
                                i + '"]');
                            el.after($('<span class="errores" style="color: red;">' +
                                error[0] + '</span>'));
                        });
                    }
                },
            });
            event.preventDefault();
        }
        event.preventDefault();
        form.classList.add('was-validated');
    }, false);
});



$(document).on("click", ".habilitar_campos_form_professional", function () {
    const inputs = document.querySelectorAll('.input_form, #actualizar_informacion_button_professional, #inhabilitar_campos_form_professional');

    // Iterar sobre cada elemento y quitar el atributo "disabled"
    inputs.forEach(input => {
        input.removeAttribute('disabled');

    });
});


$(document).on("click", ".inhabilitar_campos_form_professional", function () {
    const inputs = document.querySelectorAll('.input_form, #actualizar_informacion_button_professional, #inhabilitar_campos_form_professional');

    // Iterar sobre cada elemento y quitar el atributo "disabled"
    inputs.forEach(input => {
        $(input).prop('disabled', true);
    });
});

const formulario_profesional = document.querySelectorAll('.formulario_profesional');

// Loop over them and prevent submission
Array.prototype.slice.call(formulario_profesional).forEach((form) => {
    form.addEventListener('submit', (event) => {

        if (!form.checkValidity()) {
            event.stopPropagation();
        } else {
            document.getElementById("inhabilitar_campos_form_usuario").disabled = true;

            document.getElementById("first_name").disabled = true;
            document.getElementById("second_name").disabled = true;
            document.getElementById("first_last_name").disabled = true;
            document.getElementById("second_last_name").disabled = true;
            document.getElementById("cellphone").disabled = true;
            document.getElementById("email").disabled = true;



            var paqueteDeDatos = new FormData();

            paqueteDeDatos.append('id', $('#id').val());
            paqueteDeDatos.append('first_name', $('#first_name').val());
            paqueteDeDatos.append('second_name', $('#second_name').val());
            paqueteDeDatos.append('first_last_name', $('#first_last_name').val());
            paqueteDeDatos.append('second_last_name', $('#second_last_name').val());
            paqueteDeDatos.append('cellphone', $('#cellphone').val());
            paqueteDeDatos.append('email', $('#email').val());

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                }
            });

            $.ajax({
                type: 'POST',
                url: "/subscriber/profile/edit/update/professional",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: paqueteDeDatos,
                processData: false,
                contentType: false,
                success: function (data) {
                    Swal.fire({
                        title: "Información actualizada",
                        text: "Se actualizó la información de contacto",
                        icon: "success",
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                    });
                },
                error: function (err) {
                    document.getElementById("first_name").disabled = false;
                    document.getElementById("second_name").disabled = false;
                    document.getElementById("first_last_name").disabled = false;
                    document.getElementById("second_last_name").disabled = false;
                    document.getElementById("cellphone").disabled = false;
                    document.getElementById("email").disabled = false;
                    errorHttp(err.status);
                    if (err.status == 422) {
                        $(".errores").hide();
                        console.log(err.responseJSON);
                        $('#success_message').fadeIn().html(err.responseJSON
                            .message);
                        console.warn(err.responseJSON.errors);
                        $.each(err.responseJSON.errors, function (i, error) {
                            var el = $(document).find('[name="' +
                                i + '"]');
                            el.after($('<span class="errores" style="color: red;">' +
                                error[0] + '</span>'));
                        });
                    }
                },
            });
            event.preventDefault();
        }
        event.preventDefault();
        form.classList.add('was-validated');
    }, false);
});


$(document).on("click", ".btn-validated", function () {
    Swal.fire({
        icon: "question",
        title: "¿Esta seguro que desea validar el correo electrónico?",
        html: "Este proceso se hará solo una vez",
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonColor: "#003f48",
        confirmButtonText: "Si, validar",
        denyButtonText: `Cancelar`,
        allowOutsideClick: false,
        allowEscapeKey: false,
        focusConfirm: true,
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            animationLoading("", "Generando y enviando token...");
            $.ajax({
                type: "GET",
                url: "/subscriber/profile/validation/email",
                success: function (data) {
                    Swal.fire("Verificación enviada", "", "success");
                },
                error: function (error) {
                    errorHttp(error.status);
                },
            });
        } else if (result.isDenied) {
            Swal.fire("Verificación cancelada", "", "info");
        }
    });
});

var fileToUpload;

document.addEventListener('DOMContentLoaded', () => {

    // Input File
    const inputImage = document.querySelector('#image');
    // Nodo donde estará el editor
    const editor = document.querySelector('#editor');
    // Ruta de la imagen seleccionada
    let urlImage = undefined;
    // Evento disparado cuando se adjunte una imagen
    inputImage.addEventListener('change', abrirEditor, false);

    /**
    * Método que abre el editor con la imagen seleccionada
    */
    function abrirEditor(e) {
        $("#imageModal").modal("show");
        // Obtiene la imagen
        urlImage = URL.createObjectURL(e.target.files[0]);

        // Borra editor en caso que existiera una imagen previa
        editor.innerHTML = '';
        let cropprImg = document.createElement('img');
        cropprImg.setAttribute('id', 'croppr');
        editor.appendChild(cropprImg);

        // Envia la imagen al editor para su recorte
        document.querySelector('#croppr').setAttribute('src', urlImage);

        // Crea el editor
        new Croppr('#croppr', {
            aspectRatio: 1,
            startSize: [70, 70],
            onCropEnd: recortarImagen
        });
    }

    /**
    * Método que recorta la imagen con las coordenadas proporcionadas con croppr.js
    */
    function recortarImagen(data) {
        // Variables de recorte
        const inicioX = data.x;
        const inicioY = data.y;
        const nuevoAncho = data.width;
        const nuevaAltura = data.height;
        const zoom = 1;

        // Canvas para el recorte
        const canvas = document.createElement('canvas');
        const contexto = canvas.getContext('2d');

        // Tamaño del canvas
        canvas.width = nuevoAncho;
        canvas.height = nuevaAltura;

        // Cargar la imagen original
        const image = new Image();
        image.onload = function () {
            // Dibujar la parte recortada de la imagen original en el canvas
            contexto.drawImage(image, inicioX, inicioY, nuevoAncho * zoom, nuevaAltura * zoom, 0, 0, nuevoAncho, nuevaAltura);

            // Convertir el canvas a un archivo Blob
            canvas.toBlob(function (blob) {
                // Crear un objeto File desde el Blob
                fileToUpload = new File([blob], "recorte.jpg", { type: "image/jpeg" });
            }, "image/jpeg");
        };
        image.src = urlImage;
    }
});

$(document).on("click", "#cropImage", function () {
    $("#imageModal").modal("hide");
    animationLoading("", "Recortando y subiendo imagen...");

    var formData = new FormData();
    formData.append('image_data', fileToUpload);

    var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

    $.ajax({
        type: 'POST',
        url: '/subscriber/profile/edit/update/photo',
        data: formData,
        processData: false,
        contentType: false,
        headers: {
            'X-CSRF-TOKEN': token
        },
        success: function (response) {
            if (response == true) {
                updateSuccess();
                
                setTimeout(() => {
                    location.reload();
                }, 1500);
            }
        },
        error: function (xhr, status, error) {
            // Manejar errores de la solicitud AJAX
            console.error('Error al enviar la imagen recortada:', error);
        }
    });
});
