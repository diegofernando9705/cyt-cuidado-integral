var forms = document.querySelectorAll(".store_calculator");

// Loop over them and prevent submission
Array.prototype.slice.call(forms).forEach((form) => {
    form.addEventListener(
        "submit",
        (event) => {
            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {

                animationLoading("", "Generando cálculo según valores...");

                var paqueteDeDatos = new FormData();

                paqueteDeDatos.append("capital", $("#capital").prop("value"));
                paqueteDeDatos.append("fecha_actual", $("#fecha_actual").val());
                paqueteDeDatos.append("fecha_inicial", $("#fecha_inicial").val());

                $.ajax({
                    type: 'POST',
                    url: '/subscriber/calculators/store/' + $("#code_calculator").val(),
                    headers: {
                        "X-CSRF-Token": $('meta[name="csrf-token"]').attr(
                            "content"),
                    },
                    data: paqueteDeDatos,
                    contentType: false,
                    processData: false,
                    cache: false,
                    success: function (data) {

                        /* Indexacion IPC */
                        var ipc_actual = data[0][0];
                        var ipc_inicial = data[0][1];
                        var ipc_indexado = data[0][2];

                        $("#ipc_actual").html(ipc_actual);
                        $("#ipc_inicial").html(ipc_inicial);
                        $("#ipc_indexado").html(ipc_indexado);


                        /* Indexacion UVR */
                        var uvr_actual = data[1][0];
                        var uvr_inicial = data[1][1];
                        var uvr_indexado = data[1][2];
                        var uvr_capital = data[1][3];

                        $("#uvr_actual").html(uvr_actual);
                        $("#uvr_inicial").html(uvr_inicial);
                        $("#uvr_indexado").html(uvr_indexado);
                        $("#uvr_capital").html(uvr_capital);



                        /* Indexacion IBC */
                        var ibc_actual = data[2][0];
                        var ibc_inicial = data[2][1];
                        var ibc_indexado = data[2][2];

                        $("#ibc_actual").html(ibc_actual);
                        $("#ibc_inicial").html(ibc_inicial);
                        $("#ibc_indexado").html(ibc_indexado);


                        /* Indexacion USURA */
                        var usura_actual = data[3][0];
                        var usura_inicial = data[3][1];
                        var usura_indexado = data[3][2];

                        $("#usura_actual").html(usura_actual);
                        $("#usura_inicial").html(usura_inicial);
                        $("#usura_indexado").html(usura_indexado);


                        /* Indexacion TRM */
                        var trm_actual = data[4][0];
                        var trm_inicial = data[4][1];
                        var trm_indexado = data[4][2];

                        $("#trm_actual").html(trm_actual);
                        $("#trm_inicial").html(trm_inicial);
                        $("#trm_indexado").html(trm_indexado);

                    },
                    error: function (err) {
                        $(".errores").hide();
                        errorHttp(err.status);
                        
                        if (err.status == 422) {
                            console.log(err.responseJSON);
                            $("#success_message").fadeIn().html(err.responseJSON.message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' + i + '"]');
                                el.after(
                                    $(
                                        '<span class="errores" style="color: red;">' +
                                        error[0] +
                                        "</span>"
                                    )
                                );
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add("was-validated");
        },
        false
    );
});

function formatNumber(n) {
    n = String(n).replace(/\D/g, "");
    return n === '' ? n : Number(n).toLocaleString();
}

$(document).on("keyup", "#capital", function () {
    const value = $(this).val();
    $(this).val(formatNumber(value));

    $(".td_valor_reintegral").html($(this).val());
});