
$(document).ready(function () {
    $(".image-hover").fadeOut();
});

$('.calculator-div').hover(
    function () {
        $(this).find('.image-hover').fadeIn();
    },
    function () {
        $(this).find('.image-hover').fadeOut();
    }
);

$(document).on("click", ".calculator-div", function () {
    var code = $(this).attr("data-id");
    animationLoading("", "Cargando calculadora...");

    $.ajax({
        type: 'GET',
        url: "/subscriber/calculators/show/" + code,
        success: function (data) {

            Swal.close();

            if (data === "empty_calculator") {
                Swal.fire({
                    allowOutsideClick: false,
                    confirmButtonText: "Cerrar",
                    confirmButtonColor: "#05555b",
                    icon: "error",
                    title: "Sin disponibilidad",
                    html: "La membresia actual no cuenta con calculadoras disponibles en este momento.",
                    footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico.</i>",
                });
            } else if (data === "forbidden_calculator") {
                Swal.fire({
                    allowOutsideClick: false,
                    confirmButtonText: "Cerrar",
                    confirmButtonColor: "#05555b",
                    icon: "error",
                    title: "Sin permisos",
                    html: "La calculadora a la que usted intenta ingresar no esta asignada a la membresia actual.",
                    footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico.</i>",
                });
             } else {
                $("#modal").modal("show");
                $("#modal-content").html(data);
            }
        },
        error: function (err) {
            errorHttp(err.status);
        },
    });
});

$(document).on("click", ".btn-closed-modal", function () {
    $(".modal").modal("hide");
});
