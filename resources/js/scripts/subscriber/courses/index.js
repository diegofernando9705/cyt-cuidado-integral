
$(document).ready(function () {
    $(".image-hover").fadeOut();
});

$('.course-div').hover(
    function () {
        $(this).find('.image-hover').fadeIn();
    },
    function () {
        $(this).find('.image-hover').fadeOut();
    }
);


$(document).on("click", ".course-div", function () {
    animationLoading("", "Cargando información...");

    var id = $(this).attr("data-id");

    $.ajax({
        type: 'GET',
        url: "/subscriber/courses/show/" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            $("#modal").modal("show");
            $("#modal-title").html("Información del curso");

            var csrfToken = $('meta[name="csrf-token"]').attr('content');
            // Construyendo el formulario con el token CSRF incluido
            var formHtml = "<button type='button' class='btn btn-danger btn-closed-modal btn-sn'>Cancelar</button><form action='courses/progress' method='POST'><input type='hidden' name='_token' value='" + csrfToken + "'><input type='hidden' name='code' value='" + id + "'><button type='submit' class='btn btn-application btn-sn'>Tomar curso</button></form>";

            $("#modal-footer").html(formHtml);
            $("#modal-content").html(data);
            Swal.close();
        },
        error: function (err) {
            errorHttp(err.status);
        },
    });
});


$(document).on("click", ".btn-closed-modal", function () {
    $(".modal").modal("hide");
});


$(document).on("click", ".btn-take-course", function () {
    var id = $(this).attr("data-id");

    Swal.fire({
        icon: "question",
        title: "¿Esta seguro que desea tomar este curso?",
        html: "Una vez iniciado, se registrará su proceso",
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonColor: "#003f48",
        confirmButtonText: "Si, iniciar ahora",
        denyButtonText: `Cancelar`,
        allowOutsideClick: false,
        allowEscapeKey: false,
        focusConfirm: true,
    }).then((result) => {

    });
});

$(document).on("click", "#major_course", function () {
    var id = $(this).attr("data-id");
    animationLoading("", "Cargando curso...");
    window.location.hash = "major";

    document.querySelectorAll(".options_menu_course").forEach(function (element) {
        element.classList.remove("active_li");
    });
    $(this).addClass("active_li");

    setTimeout(() => {
        $.ajax({
            type: 'GET',
            url: "/subscriber/course/selected/" + id,
            success: function (data) {
                $("#content-data").html(data);
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
        Swal.close();
    }, 1000);
});

$(document).on("click", "#comments_lesson", function () {
    var id = $(this).attr("data-id");
    animationLoading("", "Cargando comentarios de la lección...");
    window.location.hash = "comments";

    document.querySelectorAll(".options_menu_course").forEach(function (element) {
        element.classList.remove("active_li");
    });

    $(this).addClass("active_li");

    setTimeout(() => {
        $.ajax({
            type: 'GET',
            url: "/subscriber/course/lesson/selected/comments/" + id,
            success: function (data) {
                $("#general-store").html(data);
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
        Swal.close();
    }, 1000);
});


$(document).on("click", ".container-lesson", function () {
    animationLoading("", "Cargando lección...");

    document.querySelectorAll(".active-lesson").forEach(function (element) {
        element.classList.remove("active-lesson");
    });

    $(this).addClass('active-lesson');

    getLesson($(this).attr("data-code"));
});

$(document).on("click", "#major_lesson", function () {
    window.location.hash = "major_lesson";

    document.querySelectorAll(".options_menu_course").forEach(function (element) {
        element.classList.remove("active_li");
    });
    $(this).addClass("active_li");

    getLesson($(this).attr("data-id"));
});

function getLesson(code) {
    $.ajax({
        type: 'GET',
        url: "/subscriber/course/lesson/selected/" + code,
        success: function (data) {
            $("#comments_course").attr("id", "comments_lesson");
            $("#comments_lesson").attr("data-id", code);

            $("#major_course").attr("id", "major_lesson");
            $("#major_lesson").attr("data-id", code);
            $("#major_lesson").removeClass("major_course");

            $("#general-store").html(data);
            Swal.close();
        },
        error: function (error) {
            errorHttp(error.status);
        },
    });
}

$(document).on("click", ".button-qualification", function () {
    Swal.fire({
        title: "Califica esta lección",
        html: '<div class="valoracion">' +
            '<input id="radio1" class="star_valoracion" type="radio" name="estrellas" value="5">' +
            '<label for="radio1">★</label>' +
            '<input id="radio2" class="star_valoracion" type="radio" name="estrellas" value="4">' +
            '<label for="radio2">★</label>' +
            '<input id="radio3" class="star_valoracion" type="radio" name="estrellas" value="3">' +
            '<label for="radio3">★</label>' +
            '<input id="radio4" class="star_valoracion" type="radio" name="estrellas" value="2">' +
            '<label for="radio4">★</label>' +
            '<input id="radio5" class="star_valoracion" type="radio" name="estrellas" value="1">' +
            '<label for="radio5">★</label>' +
            '</div>',
        showConfirmButton: false,
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonColor: "#003f48",
        confirmButtonText: "Si, iniciar ahora",
        denyButtonText: `Cancelar`,
        allowOutsideClick: false,
        allowEscapeKey: false,
        focusConfirm: true,
    });
});


$(document).on("click", ".comments_course", function () {
    var id = $(this).attr("data-id");
    animationLoading("", "Cargando comentarios del curso...");
    window.location.hash = "comments";

    document.querySelectorAll(".options_menu_course").forEach(function (element) {
        element.classList.remove("active_li");
    });

    $(this).addClass("active_li");

    setTimeout(() => {
        $.ajax({
            type: 'GET',
            url: "/subscriber/course/selected/comments/" + id,
            success: function (data) {
                $("#content-data").html(data);
            },
            error: function (error) {
               // errorHttp(error.status);
            },
        });
        Swal.close();
    }, 1000);
});

$(document).on("click", ".star_valoracion", function () {
    var valor = $(this).val();
    var code_lesson = $("#code_lesson_index").val();

    $.ajax({
        type: 'GET',
        url: "/subscriber/course/lesson/assessment/" + valor + "/" + code_lesson,
        success: function (data) {
            setTimeout(() => {
                if (data == true) {
                    Swal.fire({
                        allowOutsideClick: false,
                        confirmButtonText: "Cerrar",
                        confirmButtonColor: "#05555b",
                        icon: "success",
                        title: "Calificación realizada",
                        html: "Se ha registrado su calificación para esta lección.",
                    });
                }
            }, 1000);
            
            setTimeout(() => {
                getLesson(code_lesson);
            }, 2000);
        },
        error: function (error) {
            errorHttp(error.status);
        },
    });
});