$('.selector').select2();

$(function () {
    'use strict';

    const forms_task = document.querySelectorAll('.create-task');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms_task).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {
                document.getElementById("submit").disabled = true;

                document.getElementById("title").disabled = true;
                document.getElementById("files").disabled = true;
                document.getElementById("start_task").disabled = true;
                document.getElementById("end_task").disabled = true;
                document.getElementById("priority").disabled = true;
                document.getElementById("referred").disabled = true;
                document.getElementById("status").disabled = true;


                var paqueteDeDatos = new FormData();
                var inpFiles = document.getElementById("files");
                for (const file of inpFiles.files) {
                    paqueteDeDatos.append("files[]", file);
                }

                paqueteDeDatos.append('title', $('#title').val());
                paqueteDeDatos.append('start_task', $('#start_task').val());
                paqueteDeDatos.append('end_task', $('#end_task').val());
                paqueteDeDatos.append('description', CKEDITOR.instances["description"].getData());
                paqueteDeDatos.append('priority', $('#priority').val());
                paqueteDeDatos.append('referred', $('#referred').val());
                paqueteDeDatos.append('status', $('#status').val());

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: "/subscriber/myTask/create/store",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        Swal.fire({
                            title: "Tarea registrada",
                            text: "Se registro la información en la plataforma",
                            icon: "success",
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                        });

                        setTimeout(() => {
                            $.ajax({
                                type: "GET",
                                url: "/subscriber/myTask/search/",
                                success: function (data) {
                                    $("#table-task-tbody").html(data);
                                },
                                error: function (error) {
                                    //errorHttp(error.status);
                                },
                            });
                        }, 1000);
                    },
                    error: function (err) {
                        document.getElementById("submit").disabled = false;

                        document.getElementById("title").disabled = false;
                        document.getElementById("files").disabled = false;
                        document.getElementById("start_task").disabled = false;
                        document.getElementById("end_task").disabled = false;
                        document.getElementById("priority").disabled = false;
                        document.getElementById("referred").disabled = false;
                        document.getElementById("status").disabled = false;

                        errorHttp(err.status);
                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });



    const forms_update_task = document.querySelectorAll('.update-task');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms_update_task).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {
                document.getElementById("submit").disabled = true;

                document.getElementById("title").disabled = true;
                document.getElementById("files").disabled = true;
                document.getElementById("start_task").disabled = true;
                document.getElementById("end_task").disabled = true;
                document.getElementById("priority").disabled = true;
                document.getElementById("referred").disabled = true;
                document.getElementById("status").disabled = true;


                var paqueteDeDatos = new FormData();
                var inpFiles = document.getElementById("files");
                for (const file of inpFiles.files) {
                    paqueteDeDatos.append("files[]", file);
                }

                paqueteDeDatos.append('code', $('#code').val());
                paqueteDeDatos.append('title', $('#title').val());
                paqueteDeDatos.append('start_task', $('#start_task').val());
                paqueteDeDatos.append('end_task', $('#end_task').val());
                paqueteDeDatos.append('description', CKEDITOR.instances["description"].getData());
                paqueteDeDatos.append('priority', $('#priority').val());
                paqueteDeDatos.append('referred', $('#referred').val());
                paqueteDeDatos.append('status', $('#status').val());

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: "/subscriber/myTask/edit/update",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        Swal.fire({
                            title: "Tarea actualizada",
                            text: "Se actualizó la información en la plataforma",
                            icon: "success",
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                        });

                        setTimeout(() => {
                            $.ajax({
                                type: "GET",
                                url: "/subscriber/myTask/search/",
                                success: function (data) {
                                    $("#table-task-tbody").html(data);
                                },
                                error: function (error) {
                                    //errorHttp(error.status);
                                },
                            });
                        }, 1000);
                    },
                    error: function (err) {
                        document.getElementById("submit").disabled = false;

                        document.getElementById("title").disabled = false;
                        document.getElementById("files").disabled = false;
                        document.getElementById("start_task").disabled = false;
                        document.getElementById("end_task").disabled = false;
                        document.getElementById("priority").disabled = false;
                        document.getElementById("referred").disabled = false;
                        document.getElementById("status").disabled = false;

                        errorHttp(err.status);
                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});

$(document).on("click", ".delete-file", function () {
    var id = $(this).attr("data-id");

    Swal.fire({
        icon: "question",
        title: "¿Esta seguro que desea eliminar el archivo?",
        html: "Una vez eliminado, no se podrá recuperar",
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonColor: "#003f48",
        confirmButtonText: "Si, eliminar",
        denyButtonText: `Cancelar`,
        allowOutsideClick: false,
        allowEscapeKey: false,
        focusConfirm: true,
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            $.ajax({
                type: "GET",
                url: "/subscriber/myFiles/delete/" + id,
                success: function (data) {
                    Swal.fire("Archivo eliminado", "", "info");
                    setTimeout(() => {
                        var containerFile = document.getElementById('file-' + id);
                        if (containerFile) {
                            containerFile.remove();
                        }
                    }, 1000);
                },
                error: function (error) {
                    errorHttp(error.status);
                },
            });
        } else if (result.isDenied) {
            Swal.fire("Eliminación cancelada", "", "info");
        }
    });
});