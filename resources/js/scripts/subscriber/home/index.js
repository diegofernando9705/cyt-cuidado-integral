$('.indicadores-slider').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 3000,
    smartSpeed: 450,
    responsiveClass: true,
    responsive: {
        0: {
            items: 2
        },
        768: {
            items: 2
        },
        991: {
            items: 5
        },
        1200: {
            items: 5
        },
        1920: {
            items: 5
        }
    }
});

$('.hacer_hoy-slider').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 40000,
    smartSpeed: 450,
    responsiveClass: true,
    responsive: {
        0: {
            items: 2
        },
        768: {
            items: 3
        },
        991: {
            items: 5
        },
        1200: {
            items: 5
        },
        1920: {
            items: 5
        }
    }
});

$('.dictamenes-slider').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 5000,
    smartSpeed: 450,
    responsiveClass: true,
    responsive: {
        0: {
            items: 2
        },
        768: {
            items: 2
        },
        991: {
            items: 3
        },
        1200: {
            items: 4
        },
        1920: {
            items: 4
        }
    }
});

$('.favoritos-slider').owlCarousel({
    loop: false,
    autoplay: true,
    autoplayTimeout: 5000,
    smartSpeed: 450,
    responsiveClass: true,
    responsive: {
        0: {
            items: 2
        },
        768: {
            items: 2
        },
        991: {
            items: 2
        },
        1200: {
            items: 2
        },
        1920: {
            items: 2
        }
    }
});

$(document).on("click", ".icono_agregar", function () {
    animationLoading('', ' Cargando información...');

    $.ajax({
        type: "GET",
        url: "/subscriber/myTask/create",
        success: function (data) {
            $("#modal").modal("show");
            $("#modal-title").html("Crear una nueva tarea");
            $("#modal-content").html(data);
            CKEDITOR.replace('description');
            Swal.close();
        },
        error: function (error) {
            errorHttp(error.status);
        },
    });
});

$(document).on("keyup", "#input-task-search", function () {
    //animationLoading('', ' Buscando...');
    $.ajax({
        type: "GET",
        url: "/subscriber/myTask/search/" + $(this).val(),
        success: function (data) {
            $("#table-task-tbody").html(data);
            Swal.close();
        },
        error: function (error) {
            //errorHttp(error.status);
        },
    });
});

$(document).on("click", ".edit-task", function () {
    animationLoading('', ' Cargando información...');

    $.ajax({
        type: "GET",
        url: "/subscriber/myTask/edit/" + $(this).attr("data-id"),
        success: function (data) {
            $("#modal").modal("show");
            $("#modal-title").html("Editar tarea");
            $("#modal-content").html(data);
            CKEDITOR.replace('description');
            Swal.close();
        },
        error: function (error) {
            errorHttp(error.status);
        },
    });
});