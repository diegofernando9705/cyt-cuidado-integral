
$(document).ready(function () {
    $(".image-hover").fadeOut();
});

$('.diary-div').hover(
    function () {
        $(this).find('.image-hover').fadeIn();
    },
    function () {
        $(this).find('.image-hover').fadeOut();
    }
);