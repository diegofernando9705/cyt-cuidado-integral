/*=========================================================================================
  File Name: form-validation.js
  Description: jquery bootstrap validation js
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: PIXINVENT
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(function () {
  'use strict';

  var bootstrapForm = $('.needs-validation'),
    jqForm = $('#jquery-val-form'),
    picker = $('#dob'),
    dtPicker = $('#dob-bootstrap-val'),
    select = $('.select2');

  // select2
  select.each(function () {
    var $this = $(this);
    $this.wrap('<div class="position-relative"></div>');
    $this
      .select2({
        placeholder: 'Select value',
        dropdownParent: $this.parent()
      })
      .change(function () {
        $(this).valid();
      });
  });

  // Pickerta
  if (picker.length) {
    picker.flatpickr({
      onReady: function (selectedDates, dateStr, instance) {
        if (instance.isMobile) {
          $(instance.mobileInput).attr('step', null);
        }
      }
    });
  }

  if (dtPicker.length) {
    dtPicker.flatpickr({
      onReady: function (selectedDates, dateStr, instance) {
        if (instance.isMobile) {
          $(instance.mobileInput).attr('step', null);
        }
      }
    });
  }

  // Bootstrap Validation
  // --------------------------------------------------------------------
  if (bootstrapForm.length) {
    Array.prototype.filter.call(bootstrapForm, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          form.classList.add('invalid');
        }
        form.classList.add('was-validated');
        event.preventDefault();
        // if (inputGroupValidation) {
        //   inputGroupValidation(form);
        // }
      });
      // bootstrapForm.find('input, textarea').on('focusout', function () {
      //   $(this)
      //     .removeClass('is-valid is-invalid')
      //     .addClass(this.checkValidity() ? 'is-valid' : 'is-invalid');
      //   if (inputGroupValidation) {
      //     inputGroupValidation(this);
      //   }
      // });
    });
  }

  // jQuery Validation
  // --------------------------------------------------------------------
  if (jqForm.length) {
    jqForm.validate({
      rules: {
        'basic-default-name': {
          required: true
        },
        'basic-default-email': {
          required: true,
          email: true
        },
        'basic-default-password': {
          required: true
        },
        'confirm-password': {
          required: true,
          equalTo: '#basic-default-password'
        },
        'select-country': {
          required: true
        },
        dob: {
          required: true
        },
        customFile: {
          required: true
        },
        validationRadiojq: {
          required: true
        },
        validationBiojq: {
          required: true
        },
        validationCheck: {
          required: true
        }
      }
    });
  }
});


// ******************************************* FORMULARIO  ROL *******************************************

// ===================== validacion del formulario rol registro ====================================  

// =================== fin validacion del formulario rol registro ====================================

// ===================== validacion del formulario rol edicion ====================================  

// =================== fin validacion del formulario rol actualizacion ====================================
// ***************************************** FIN FORMULARIO  ROL *******************************************



// ************************************* FORMULARIO MODULO DE USUARIO *************************************



// ================================ formulario para un nuevo registro ================================


// ================================ formulario para un nuevo registro ================================
// ===================== FIN VALIDACION DEL FORMULARIO REGISTRO DE USUARIO ====================================


// ===================== VALIDACION DEL FORMULARIO REGISTRO DE PACIENTES ====================================

$(function () {
  'use strict';

  const formulario_pacientes = document.querySelectorAll('.registro-pacientes');
  console.log(formulario_pacientes);
  // Loop over them and prevent submission
  Array.prototype.slice.call(formulario_pacientes).forEach((form) => {
    form.addEventListener('submit', (event) => {
      console.log(form.checkValidity());
      if (!form.checkValidity()) {
        event.stopPropagation();
      } else {

        var paqueteDeDatos = new FormData();
        paqueteDeDatos.append('change-picture', $('#change-picture')[0].files[0]);

        paqueteDeDatos.append('tipo_documento', $('#tipo_documento').prop('value'));
        paqueteDeDatos.append('cedula', $('#cedula').prop('value'));
        paqueteDeDatos.append('primer_nombre', $('#primer_nombre').prop('value'));
        paqueteDeDatos.append('primer_apellido', $('#primer_apellido').prop('value'));
        paqueteDeDatos.append('segundo_nombre', $('#segundo_nombre').prop('value'));
        paqueteDeDatos.append('segundo_apellido', $('#segundo_apellido').prop('value'));
        paqueteDeDatos.append('fecha_nacimiento', $('#fecha_nacimiento').prop('value'));
        paqueteDeDatos.append('correo_electronico', $('#correo_electronico').prop('value'));
        paqueteDeDatos.append('telefono', $('#telefono').prop('value'));
        paqueteDeDatos.append('celular', $('#celular').prop('value'));
        paqueteDeDatos.append('ocupacion', $('#ocupacion').prop('value'));
        paqueteDeDatos.append('genero', $('#genero').prop('value'));
        paqueteDeDatos.append('grupo_sanguineo', $('#grupo_sanguineo').prop('value'));
        paqueteDeDatos.append('departamento', $('#departamento').prop('value'));
        paqueteDeDatos.append('ciudad', $('#ciudad').prop('value'));
        paqueteDeDatos.append('direccion', $('#direccion').prop('value'));
        paqueteDeDatos.append('barrio', $('#barrio').prop('value'));
        paqueteDeDatos.append('comuna', $('#comuna').prop('value'));
        paqueteDeDatos.append('cedula_acompanante', $('#cedula_acompanante').prop('value'));
        paqueteDeDatos.append('nombre_acompanante', $('#nombre_acompanante').prop('value'));
        paqueteDeDatos.append('numero_acompanante', $('#numero_acompanante').prop('value'));
        paqueteDeDatos.append('parentesco_acompanante', $('#parentesco_acompanante').prop('value'));

        $.ajax({
          type: 'POST',
          contentType: false,
          url: '/administrativo/pacientes/create/store',
          headers: { 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') },
          data: paqueteDeDatos, // serializes the form's elements.
          processData: false,
          cache: false,
          success: function (data) {

            document.getElementById("change-picture").disabled = true;
            document.getElementById("tipo_documento").disabled = true;
            document.getElementById("cedula").disabled = true;
            document.getElementById("primer_nombre").disabled = true;
            document.getElementById("primer_apellido").disabled = true;
            document.getElementById("segundo_nombre").disabled = true;
            document.getElementById("segundo_apellido").disabled = true;
            document.getElementById("fecha_nacimiento").disabled = true;
            document.getElementById("correo_electronico").disabled = true;
            document.getElementById("telefono").disabled = true;
            document.getElementById("celular").disabled = true;
            document.getElementById("ocupacion").disabled = true;
            document.getElementById("genero").disabled = true;
            document.getElementById("grupo_sanguineo").disabled = true;
            document.getElementById("departamento").disabled = true;
            document.getElementById("ciudad").disabled = true;
            document.getElementById("direccion").disabled = true;
            document.getElementById("barrio").disabled = true;
            document.getElementById("comuna").disabled = true;

            if (data == true) {
              $("#registro-exitoso").toast('show');

              setTimeout(function () {
                window.location = "/administrativo/pacientes";
              }, 2000);
            }


          },
          error: function (err) {
            if (err.status == 422) {
              console.log(err.responseJSON);
              $('#success_message').fadeIn().html(err.responseJSON.message);
              console.warn(err.responseJSON.errors);
              $.each(err.responseJSON.errors, function (i, error) {
                var el = $(document).find('[name="' + i + '"]');
                el.after($('<span style="color: red;">' + error[0] + '</span>'));
              });
            }
          },
        });
        event.preventDefault();


        event.preventDefault();
      }
      event.preventDefault();
      form.classList.add('was-validated');
    }, false);
  });


  // jQuery Validation
  // --------------------------------------------------------------------
  if (formulario_pacientes.length) {
    formulario_pacientes.validate({

      rules: {
        'tipo_documento': {
          required: true
        },
        'cedula': {
          required: true,
          digits: true,
          maxlength: 12,
          minlength: 5
        },
        'primer_nombre': {
          required: true
        },
        'primer_apellido': {
          required: true
        },
        'fecha_nacimiento': {
          required: true
        },
        'correo_electronico': {
          required: true,
          email: true
        },
        'celular': {
          required: true
        },
        'departamento': {
          required: true
        },
        'ciudad': {
          required: true
        },
        'estado': {
          required: true
        }
      }

    });
  }

});
// ===================== FIN VALIDACION DEL FORMULARIO REGISTRO DE USUARIO ====================================


// ===================== VALIDACION DEL FORMULARIO REGISTRO DE PROFESION ====================================

$(function () {
  'use strict';

  const formulario_profesion = document.querySelectorAll('.registro-profesiones');

  Array.prototype.slice.call(formulario_profesion).forEach((form) => {
    form.addEventListener('submit', (event) => {
      console.log(form.checkValidity());
      if (!form.checkValidity()) {
        event.stopPropagation();
      } else {

        var formulario = $(".registro-profesiones").serialize();

        $.ajax({
          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
          type: 'POST',
          url: '/configuracion/profesiones/create/store',
          data: formulario,
          success: function (data) {

            document.getElementById("nombre_profesion").disabled = true;
            document.getElementById("descripcion_profesion").disabled = true;
            document.getElementById("estado_profesion").disabled = true;
            document.getElementById("submit").disabled = true;

            $("#mensajes").html("<div class='alert alert-success' style='padding:10px'>Verificando informacion</div>");

            $("#registro-exitoso").toast('show');

            setTimeout(function () {
              window.location = "/configuracion/profesiones";
            }, 2000);
          },
          error: function (err) {
            $(".error-ajax").attr("style", "display:none;");
            if (err.status == 422) {
              $('#success_message').fadeIn().html(err.responseJSON.message);
              console.warn(err.responseJSON.errors);
              $.each(err.responseJSON.errors, function (i, error) {
                var el = $(document).find('[name="' + i + '"]');
                el.after($('<span style="color: red;" class="error-ajax">' + error[0] + '</span>'));
              });
            }
          },
        });
        event.preventDefault();


        event.preventDefault();
      }
      event.preventDefault();
      form.classList.add('was-validated');
    }, false);
  });


  // jQuery Validation
  // --------------------------------------------------------------------
  if (formulario_profesion.length) {
    formulario_profesion.validate({

      rules: {
        'nombre_profesion': {
          required: true
        },
        'descripcion_profesion': {
          required: true
        },
        'estado_profesion': {
          required: true
        }
      }

    });
  }
});
// ===================== FIN VALIDACION DEL FORMULARIO REGISTRO DE PROFESION ====================================


// ******************************************* FORMULARIO  DOCTORES *******************************************
$(function () {
  'use strict';

  const forms = document.querySelectorAll('.registro-doctores');

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms).forEach((form) => {
    form.addEventListener('submit', (event) => {


      if (!form.checkValidity()) {
        event.stopPropagation();
      } else {

        var paqueteDeDatos = new FormData();
        paqueteDeDatos.append('foto_doctor', $('#change-picture')[0].files[0]);
        paqueteDeDatos.append('foto_firma', $('#change-firma')[0].files[0]);

        paqueteDeDatos.append('tarjeta_profesional', $('#tarjeta_profesional').prop('value'));
        paqueteDeDatos.append('tipo_documento', $('#tipo_documento').prop('value'));
        paqueteDeDatos.append('cedula', $('#cedula').prop('value'));
        paqueteDeDatos.append('primer_nombre', $('#primer_nombre').prop('value'));
        paqueteDeDatos.append('primer_apellido', $('#primer_apellido').prop('value'));
        paqueteDeDatos.append('segundo_nombre', $('#segundo_nombre').prop('value'));
        paqueteDeDatos.append('segundo_apellido', $('#segundo_apellido').prop('value'));
        paqueteDeDatos.append('fecha_nacimiento', $('#fecha_nacimiento').prop('value'));
        paqueteDeDatos.append('correo_electronico', $('#correo_electronico').prop('value'));
        paqueteDeDatos.append('telefono', $('#telefono').prop('value'));
        paqueteDeDatos.append('celular', $('#celular').prop('value'));
        paqueteDeDatos.append('profesion', $('#profesion').prop('value'));
        paqueteDeDatos.append('genero', $('#genero').prop('value'));
        paqueteDeDatos.append('departamento', $('#departamento').prop('value'));
        paqueteDeDatos.append('ciudad', $('#ciudad').prop('value'));
        paqueteDeDatos.append('barrio', $('#barrio').prop('value'));
        paqueteDeDatos.append('zona', $('#zona').prop('value'));
        paqueteDeDatos.append('genero', $('#genero').prop('value'));
        paqueteDeDatos.append('estado', $('#estado').prop('value'));



        document.getElementById('tarjeta_profesional').disabled = true;
        document.getElementById('tipo_documento').disabled = true;
        document.getElementById('cedula').disabled = true;
        document.getElementById('primer_nombre').disabled = true;
        document.getElementById('primer_apellido').disabled = true;
        document.getElementById('segundo_nombre').disabled = true;
        document.getElementById('segundo_apellido').disabled = true;
        document.getElementById('fecha_nacimiento').disabled = true;
        document.getElementById('correo_electronico').disabled = true;
        document.getElementById('telefono').disabled = true;
        document.getElementById('celular').disabled = true;
        document.getElementById('profesion').disabled = true;
        document.getElementById('genero').disabled = true;
        document.getElementById('departamento').disabled = true;
        document.getElementById('ciudad').disabled = true;
        document.getElementById('barrio').disabled = true;
        document.getElementById('zona').disabled = true;
        document.getElementById('estado').disabled = true;
        document.getElementById('submit').disabled = true;

        $.ajax({
          type: 'POST',
          contentType: false,
          url: '/administrativo/talentohumano/create/store',
          headers: { 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') },
          data: paqueteDeDatos, // serializes the form's elements.
          processData: false,
          cache: false,
          success: function (data) {
            $("#mensajes").html("<div class='alert alert-success' style='padding:10px'>Verificando informacion</div>");

            if (data == true) {
              $("#registro-exitoso").toast('show');

              setTimeout(function () {
                window.location = "/administrativo/talentohumano";
              }, 2000);
            }
          },
          error: function (err) {
            document.getElementById('tarjeta_profesional').disabled = false;
            document.getElementById('tipo_documento').disabled = false;
            document.getElementById('cedula').disabled = false;
            document.getElementById('primer_nombre').disabled = false;
            document.getElementById('primer_apellido').disabled = false;
            document.getElementById('segundo_nombre').disabled = false;
            document.getElementById('segundo_apellido').disabled = false;
            document.getElementById('fecha_nacimiento').disabled = false;
            document.getElementById('correo_electronico').disabled = false;
            document.getElementById('telefono').disabled = false;
            document.getElementById('celular').disabled = false;
            document.getElementById('profesion').disabled = false;
            document.getElementById('genero').disabled = false;
            document.getElementById('departamento').disabled = false;
            document.getElementById('ciudad').disabled = false;
            document.getElementById('barrio').disabled = false;
            document.getElementById('zona').disabled = false;
            document.getElementById('estado').disabled = false;
            document.getElementById('submit').disabled = false;

            if (err.status == 422) {
              $(".errores").attr("style", "display:none;");
              $('#success_message').fadeIn().html(err.responseJSON.message);

              $.each(err.responseJSON.errors, function (i, error) {
                var el = $(document).find('[name="' + i + '"]');
                el.after($('<span class="errores" style="color: red;">' + error[0] + '</span>'));
              });
            }
          },
        });

        event.preventDefault();

      }
      event.preventDefault();
      form.classList.add('was-validated');
    }, false);
  });


  // jQuery Validation
  // --------------------------------------------------------------------
  if (forms.length) {
    forms.validate({
      /*
      * ? To enable validation onkeyup
      onkeyup: function (element) {
        $(element).valid();
      },*/
      /*
      * ? To enable validation on focusout
      onfocusout: function (element) {
        $(element).valid();
      }, */
      rules: {
        'foto_doctor': {
          required: true
        },
        'foto_firma': {
          required: true
        },
        'tarjeta_profesional': {
          required: true
        },
        'tipo_documento': {
          required: true
        },
        'cedula': {
          required: true
        },
        'primer_nombre': {
          required: true
        },
        'primer_apellido': {
          required: true
        },
        'fecha_nacimiento': {
          required: true
        },
        'correo_electronico': {
          required: true
        },
        'celular': {
          required: true
        },
        'profesion': {
          required: true
        },
        'genero': {
          required: true
        },
        'estado': {
          required: true
        }

      }
    });
  }
});
// ******************************************* FIN FORMULARIO  DOCTORES *******************************************






// ===================== validacion del formulario rol edicion ====================================  

$(function () {
  'use strict';

  const forms = document.querySelectorAll('.actualizar-doctores');

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms).forEach((form) => {
    form.addEventListener('submit', (event) => {

      if (!form.checkValidity()) {
        event.stopPropagation();
      } else {

        var formulario = $(".actualizar-doctores").serialize();

        $.ajax({
          type: 'POST',
          url: '/seguridad/roles/create/update',
          data: formulario, // serializes the form's elements.
          success: function (data) {
            document.getElementById("registro-btn").disabled = true;
            document.getElementById("regreso-btn").disabled = true;

            $("#actualizacion-exitosa").toast('show');

            setTimeout(function () {
              window.location = "/seguridad/roles/listado";
            }, 2000);
          },
          error: function (error) {
            $("#mensajes").html("<div class='alert alert-danger' style='padding:10px'>" + error.responseJSON.errors.nombre_rol + "</div>");
          },
        });
        event.preventDefault();

      }
      event.preventDefault();
      form.classList.add('was-validated');
    }, false);
  });


  // jQuery Validation
  // --------------------------------------------------------------------
  if (forms.length) {
    forms.validate({
      /*
      * ? To enable validation onkeyup
      onkeyup: function (element) {
        $(element).valid();
      },*/
      /*
      * ? To enable validation on focusout
      onfocusout: function (element) {
        $(element).valid();
      }, */
      rules: {
        'codigo': {
          required: true
        },
        'nombre_rol': {
          required: true
        }
      }
    });
  }
});
// =================== fin validacion del formulario rol actualizacion ====================================

// ***************************************** FIN FORMULARIO  DOCTORES *******************************************



// ******************************************* FORMULARIO  PROCEDIMIENTOS *******************************************


// ===================== validacion del formulario procedimiento registro ====================================  
$(function () {
  'use strict';

  const forms = document.querySelectorAll('.registro-procedimiento');

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms).forEach((form) => {
    form.addEventListener('submit', (event) => {

      if (!form.checkValidity()) {
        event.stopPropagation();
      } else {

        var formulario = $(".registro-procedimiento").serialize();

        $.ajax({
          type: 'POST',
          url: '/configuracion/procedimientos/create/store',
          data: formulario,
          success: function (data) {
            document.getElementById("nombre_procedimiento").disabled = true;
            document.getElementById("descripcion_procedimiento").disabled = true;
            document.getElementById("estado_procedimiento").disabled = true;
            document.getElementById("submit").disabled = true;

            $("#mensajes").html("<div class='alert alert-success' style='padding:10px'>Verificando informacion</div>");

            if (data == true) {
              $("#registro-exitoso").toast('show');

              setTimeout(function () {
                window.location = "/configuracion/procedimientos";
              }, 2000);
            }
          },
          error: function (err) {
            $(".error-ajax").attr("style", "display:none;");
            if (err.status == 422) {
              $('#success_message').fadeIn().html(err.responseJSON.message);
              console.warn(err.responseJSON.errors);
              $.each(err.responseJSON.errors, function (i, error) {
                var el = $(document).find('[name="' + i + '"]');
                el.after($('<span style="color: red;" class="error-ajax">' + error[0] + '</span>'));
              });
            }
          },
        });
        event.preventDefault();

      }
      event.preventDefault();
      form.classList.add('was-validated');
    }, false);
  });


  // jQuery Validation
  // --------------------------------------------------------------------
  if (forms.length) {
    forms.validate({
      rules: {
        'nombre_procedimiento': {
          required: true
        },
        'descripcion_procedimiento': {
          required: true
        },
        'estado_procedimiento': {
          required: true
        }
      }
    });
  }
});
// =================== fin validacion del formulario rol registro ====================================
// ***************************************** FIN FORMULARIO  PROCEDIMIENTOS *******************************************


// ******************************************* FORMULARIO  PROGRAMAS *******************************************

$(function () {
  'use strict';

  const forms = document.querySelectorAll('.registro-programa');

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms).forEach((form) => {
    form.addEventListener('submit', (event) => {

      if (!form.checkValidity()) {
        event.stopPropagation();
      } else {

        var formulario = $(".registro-programa").serialize();

        $.ajax({
          type: 'POST',
          url: '/configuracion/programas/create/store',
          data: formulario,
          success: function (data) {
            document.getElementById("nombre_programa").disabled = true;
            document.getElementById("descripcion_programa").disabled = true;
            document.getElementById("estado_programa").disabled = true;
            document.getElementById("submit").disabled = true;

            $("#mensajes").html("<div class='alert alert-success' style='padding:10px'>Verificando informacion</div>");

            if (data == true) {
              $("#registro-exitoso").toast('show');

              setTimeout(function () {
                window.location = "/configuracion/programas";
              }, 2000);
            }
          },
          error: function (err) {
            $(".error-ajax").attr("style", "display:none;");
            if (err.status == 422) {
              $('#success_message').fadeIn().html(err.responseJSON.message);
              console.warn(err.responseJSON.errors);
              $.each(err.responseJSON.errors, function (i, error) {
                var el = $(document).find('[name="' + i + '"]');
                el.after($('<span style="color: red;" class="error-ajax">' + error[0] + '</span>'));
              });
            }
          },
        });
        event.preventDefault();

      }
      event.preventDefault();
      form.classList.add('was-validated');
    }, false);
  });


  // jQuery Validation
  // --------------------------------------------------------------------
  if (forms.length) {
    forms.validate({
      rules: {
        'nombre_programa': {
          required: true
        },
        'descripcion_programa': {
          required: true
        },
        'estado_programa': {
          required: true
        }
      }
    });
  }
});
// ***************************************** FIN FORMULARIO  PROGRAMAS *******************************************


// ******************************************* FORMULARIO  EPS *******************************************


// ===================== validacion del formulario procedimiento registro ====================================  
$(function () {
  'use strict';

  const forms = document.querySelectorAll('.registro-eps');

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms).forEach((form) => {
    form.addEventListener('submit', (event) => {

      if (!form.checkValidity()) {
        event.stopPropagation();
      } else {

        var formulario = $(".registro-eps").serialize();

        $.ajax({
          type: 'POST',
          url: '/configuracion/eps/create/store',
          data: formulario,
          success: function (data) {
            document.getElementById("nombre_eps").disabled = true;
            document.getElementById("descripcion_eps").disabled = true;
            document.getElementById("telefono_eps").disabled = true;
            document.getElementById("direccion_eps").disabled = true;
            document.getElementById("estado_eps").disabled = true;
            document.getElementById("submit").disabled = true;

            $("#mensajes").html("<div class='alert alert-success' style='padding:10px'>Verificando informacion</div>");

            if (data == true) {
              $("#registro-exitoso").toast('show');

              setTimeout(function () {
                window.location = "/configuracion/eps/listado";
              }, 2000);
            }
          },
          error: function (err) {
            $(".error-ajax").attr("style", "display:none;");
            if (err.status == 422) {
              $('#success_message').fadeIn().html(err.responseJSON.message);
              console.warn(err.responseJSON.errors);
              $.each(err.responseJSON.errors, function (i, error) {
                var el = $(document).find('[name="' + i + '"]');
                el.after($('<span style="color: red;" class="error-ajax">' + error[0] + '</span>'));
              });
            }
          },
        });
        event.preventDefault();

      }
      event.preventDefault();
      form.classList.add('was-validated');
    }, false);
  });


  // jQuery Validation
  // --------------------------------------------------------------------
  if (forms.length) {
    forms.validate({
      rules: {
        'nombre_eps': {
          required: true
        },
        'telefono_eps': {
          required: true
        },
        'direccion_eps': {
          required: true
        },
        'estado_eps': {
          required: true
        }
      }
    });
  }
});
  // =================== fin validacion del formulario rol registro ====================================
// ***************************************** FIN FORMULARIO  EPS *******************************************