/**
 * DataTables Basic
 */

$(function () {
  'use strict';

  var tabla_roles = $('.datatables-basic'),
    tabla_pacientes = $('.datatables-pacientes'),
    tabla_interconsultas = $('.datatables-interconsultas'),
    tabla_profesiones = $('.datatables-profesiones'),
    tabla_procedimientos = $('.datatables-procedimientos'),
    tabla_eps = $('.datatables-eps'),
    tabla_programas = $('.datatables-programas'),

    tabla_doctores = $('.datatables-doctores'),
    tabla_usuarios = $('.datatables-usuarios'),
    tabla_cie10 = $('.datatables-cie10'),
    tabla_historiasclinicas = $('.datatables-historiasclinicas'),
    tabla_historiasclinicasprofile = $('.datatables-historiasclinicasprofile'),
    dt_date_table = $('.dt-date'),
    dt_complex_header_table = $('.dt-complex-header'),
    dt_row_grouping_table = $('.dt-row-grouping'),
    dt_multilingual_table = $('.dt-multilingual'),
    assetPath = '../../../app-assets/';


  if ($('body').attr('data-framework') === 'laravel') {
    assetPath = $('body').attr('data-asset-path');
  }


  //  -------------------------------------- TABLA TALENTO HUMANO --------------------------------------

  if (tabla_doctores.length) {
    var dt_basic = tabla_doctores.DataTable({
      ajax: '/administrativo/talentohumano/table/listado',
      columns: [
        // used for sorting so will hide this column
        // used for sorting so will hide this column
        { data: 'vacio' },
        { data: 'cedula_doctor' },
        { data: 'primer_nombre' },
        { data: 'primer_apellido' },
        { data: 'celular_doctor' },
        { data: 'correo_doctor' },
        { data: 'nombre_estado' },
        { data: '' }
      ],
      columnDefs: [
        {
          className: 'control',
          orderable: false,
          responsivePriority: 3,
          targets: 0
        },
        {
          responsivePriority: 3,
          targets: 0
        },
        {
          // Actions
          targets: -1,
          title: 'Acciones',
          orderable: false,
          render: function (data, type, full, meta) {

            if (full['codigo_estado'] == 1) {
              return (
                '<button class="btn btn-success ver-doctor btn-sm" title="Ver Información" data-id="' + full['cedula_doctor'] + '">'
                + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
                '</button>'
                +
                ' <button class="btn btn-info editar-doctor btn-sm" title="Editar Información" data-id="' + full['cedula_doctor'] + '">'
                + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) +
                '</button> '
                +
                '<button class="btn btn-primary asignar-pacientes btn-sm" title="Asignar pacientes" data-id="' + full['cedula_doctor'] + '">'
                + feather.icons['user-plus'].toSvg({ class: 'font-small-4' }) +
                '</button>'
                +
                ' <button class="btn btn-danger eliminar-doctor btn-sm" title="Eliminar Información" data-id="' + full['cedula_doctor'] + '">'
                + feather.icons['trash-2'].toSvg({ class: 'font-small-4' }) +
                '</button>'
              );
            } else {
              return (
                '<button class="btn btn-success ver-doctor btn-sm" title="Ver Información" data-id="' + full['cedula_doctor'] + '">'
                + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
                '</button>'
                +
                ' <button class="btn btn-info editar-doctor btn-sm" title="Editar Información" data-id="' + full['cedula_doctor'] + '">'
                + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) +
                '</button>'
              );
            }


          }
        }
      ],
      order: [[1, 'asc']],
      responsive: {
        details: {
          display: $.fn.dataTable.Responsive.display.modal({
            header: function (row) {
              var data = row.data();
              return 'Informacion completa para ' + data['primer_nombre'];
            }
          }),
          type: 'column',
          renderer: function (api, rowIdx, columns) {
            var data = $.map(columns, function (col, i) {

              return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                ? '<tr data-dt-row="' +
                col.rowIndex +
                '" data-dt-column="' +
                col.columnIndex +
                '">' +
                '<td>' +
                col.title +
                ':' +
                '</td> ' +
                '<td>' +
                col.data +
                '</td>' +
                '</tr>'
                : '';
            }).join('');

            return data ? $('<table class="table"/>').append(data) : false;
          }
        }
      },
      language: {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
    $('div.head-label').html('<h6 class="mb-0">DataTable with Buttons</h6>');
  }

  $(document).on("click", ".ver-doctor", function () {
    $("#modal-doctor").modal("show");
    $(".dtr-bs-modal").modal("hide");

    $.ajax({
      type: 'GET',
      url: '/administrativo/talentohumano/show/' + $(this).attr("data-id"),
      success: function (data) {
        $("#exampleModalCenterTitle").html('Descripción  de Información');
        $("#contenido-doctor").html(data);
      },
      error: function (error) {
        $("#modal-doctor").modal("hide");
        if (error.status == 403) {
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Usted no tiene permisos para ver la información. Comunicate con soporte.',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        }
      },
    });
  });

  $(document).on("click", ".editar-doctor", function () {
    $(".dtr-bs-modal").modal("hide");
    $("#modal-doctor").modal("show");
    $.ajax({
      type: 'GET',
      url: '/administrativo/talentohumano/edit/' + $(this).attr("data-id"),
      success: function (data) {
        $("#exampleModalCenterTitle").html('Actualizacion de Información');
        $("#contenido-doctor").html(data);
      },
      error: function (error) {
        if (error.status == 403) {
          $("#modal-doctor").modal("hide");
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Usted no tiene permisos para editar la información. Comunicate con soporte.',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        } else {
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Comunicate con soporte. <b>CÓD. ERROR: ' + error.status + '</b>',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        }
      },
    });
  });


  $(document).on("click", ".asignar-pacientes", function () {
    $("#modal-doctor").modal("show");
    $(".dtr-bs-modal").modal("hide");
    $("#tituloModalTalento").html("Asignacion de pacientes");
    $.ajax({
      type: 'GET',
      url: '/administrativo/talentohumano/asignacion/pacientes/' + $(this).attr("data-id"),
      success: function (data) {
        $("#exampleModalCenterTitle").html('Descripción  de Información');
        $("#contenido-doctor").html(data);
      },
      error: function (error) {
        if (error.status == 403) {
          $("#modal-doctor").modal("hide");
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Usted no tiene permisos para asignar pacientes. Comunicate con soporte.',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        } else {
          $("#modal-doctor").modal("hide");
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Comunicate con soporte.',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        }

      },
    });
  });

  $(document).on("click", ".eliminar-doctor", function () {
    $(".dtr-bs-modal").modal("hide");
    Swal.fire({
      title: '¿Desea eliminar el registro?',
      text: "No podrás revertir esto",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          type: 'GET',
          url: '/administrativo/talentohumano/destroy/' + $(this).attr("data-id"),
          processData: false,
          contentType: false,
          success: function (data) {
            console.log(data);
            if (data == true) {
              Swal.fire(
                'Eliminado!',
                'La información ha sido eliminada.',
                'success'
              )

              setTimeout(function () {
                window.location = "/administrativo/talentohumano";
              }, 2000);

            } else {

            }

          },
          error: function (err) {
            if (err.status == 422) {

              $(".errores").hide();
              console.log(err.responseJSON);
              $('#success_message').fadeIn().html(err.responseJSON.message);
              console.warn(err.responseJSON.errors);
              $.each(err.responseJSON.errors, function (i, error) {
                var el = $(document).find('[name="' + i + '"]');
                el.after($('<span class="errores" style="color: red;">' + error[0] + '</span>'));
              });
            } else if (err.status == 403) {
              $("#modal-doctor").modal("hide");
              Swal.fire({
                allowOutsideClick: false,
                icon: 'error',
                title: 'Oops...',
                text: 'Usted no tiene permisos para eliminar información. Comunicate con soporte.',
                footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
              });
            }
          },
        });
      }
    })
  });




  // -------------------------------------- FIN TABLA DOCTORES --------------------------------------


  //  -------------------------------------- TABLA HISTORIAS CLINICAS --------------------------------------

  if (tabla_historiasclinicas.length) {
    var dt_basic = tabla_historiasclinicas.DataTable({
      ajax: '/administrativo/historiasclinicas/table/listado',
      columns: [
        // used for sorting so will hide this column
        { data: 'vacio' }, // used for sorting so will hide this column
        { data: 'codigo_historia_clinica' }, // used for sorting so will hide this column
        { data: 'cedula_paciente' },
        { data: 'primer_nombre' },
        { data: 'primer_apellido' },
        { data: 'fecha_historia_clinica' },
        { data: 'nombre_estados' },
        { data: '' }
      ],
      columnDefs: [
        {
          className: 'control',
          orderable: false,
          responsivePriority: 2,
          targets: 0
        },
        {
          responsivePriority: 1,
          targets: 4
        },
        {
          // Actions
          targets: -1,
          title: 'Acciones',
          orderable: false,
          render: function (data, type, full, meta) {
            return (
              '<button class="btn btn-success ver-historiaclinica" data-id="' + full['cedula_paciente'] + '" title="Ver Información">'
              + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
              ' INFORMACIÓN</button>'
              +
              ' <button class="btn btn-secondary agregar-nota" data-id="' + full['cedula_paciente'] + '" title="Agregar Nota">'
              + feather.icons['plus-square'].toSvg({ class: 'font-small-4' }) +
              ' NOTA</button>'
              +
              '<br><button class="btn btn-warning generar-evaluacion" data-id="' + full['cedula_paciente'] + '" style="margin-top:5px;">'
              + feather.icons['book'].toSvg({ class: 'font-small-4' }) +
              ' EVALUACIÓN</button>'
              +
              ' <button class="btn btn-info generar-evolucion" data-id="' + full['cedula_paciente'] + '" style="margin-top:5px;">'
              + feather.icons['trending-up'].toSvg({ class: 'font-small-4' }) +
              ' EVOLUCIÓN </button>'
            );
          }
        }
      ],
      order: [[1, 'asc']],
      responsive: {
        details: {
          display: $.fn.dataTable.Responsive.display.modal({
            header: function (row) {
              var data = row.data();
              return 'Informacion completa para ' + data['primer_nombre'];
            }
          }),
          type: 'column',
          renderer: function (api, rowIdx, columns) {
            var data = $.map(columns, function (col, i) {

              return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                ? '<tr data-dt-row="' +
                col.rowIndex +
                '" data-dt-column="' +
                col.columnIndex +
                '">' +
                '<td>' +
                col.title +
                ':' +
                '</td> ' +
                '<td>' +
                col.data +
                '</td>' +
                '</tr>'
                : '';
            }).join('');

            return data ? $('<table class="table"/>').append(data) : false;
          }
        }
      },
      language: {
        paginate: {
          // remove previous & next text from pagination
          previous: '&nbsp;',
          next: '&nbsp;'
        }
      }
    });
    $('div.head-label').html('<h6 class="mb-0">Historias Clinicas</h6>');
  }

  $(document).on("click", ".ver-historiaclinica", function () {
    $("#modal-historia-clinica").modal("show");
    $("#generar-pdf").attr("data-id", $(this).attr("data-id"));
    $.ajax({
      type: 'GET',
      url: '/administrativo/pacientes/historia_clinica/' + $(this).attr("data-id"),
      success: function (data) {
        $("#exampleModalCenterTitle").html('Descripción  de Información');
        $("#contenido-historia-clinica").html(data);
      },
      error: function (error) {
        $("#mensajes").html("<div class='alert alert-danger' style='padding:10px'>" + error.responseJSON.errors.nombre_rol + "</div>");
      },
    });
  });

  $(document).on("click", ".editar-historiaclinica", function () {
    $("#modal-doctor").modal("show");
    $.ajax({
      type: 'GET',
      url: '/administrativo/historiasclinicas/edit/' + $(this).attr("data-id"),
      success: function (data) {
        $("#exampleModalCenterTitle").html('Actualizacion de Información');
        $("#contenido-doctor").html(data);
      },
      error: function (error) {
        $("#mensajes").html("<div class='alert alert-danger' style='padding:10px'>" + error.responseJSON.errors.nombre_rol + "</div>");
      },
    });
  });

  $(document).on("click", ".agregar-nota", function () {
    $("#modal-pacientes").modal("show");
    $.ajax({
      type: 'GET',
      url: '/administrativo/pacientes/notas/' + $(this).attr("data-id"),
      success: function (data) {
        $(".tituloModal").html('Agregar nota');
        $("#contenido-pacientes").html(data);
      },
      error: function (error) {
        $("#mensajes").html("<div class='alert alert-danger' style='padding:10px'>" + error.responseJSON.errors.nombre_rol + "</div>");
      },
    });
  });


  // -------------------------------------- FIN TABLA HISTORIAS CLINICAS --------------------------------------


  //  -------------------------------------- TABLA HISTORIAS CLINICAS PERFIL USUARIO --------------------------------------

  if (tabla_historiasclinicasprofile.length) {
    var dt_basic = tabla_historiasclinicasprofile.DataTable({
      ajax: '/administrativo/historiasclinicas/table/listado',
      columns: [
        // used for sorting so will hide this column
        { data: 'codigo_historia_clinica' }, // used for sorting so will hide this column
        { data: 'codigo_historia_clinica' }, // used for sorting so will hide this column
        { data: 'cedula_paciente' },
        { data: 'primer_nombre' },
        { data: 'primer_apellido' },
        { data: 'estado_historia_clinica' },
        { data: '' }
      ],
      columnDefs: [
        {
          className: 'control',
          orderable: false,
          responsivePriority: 2,
          targets: 0
        },
        {
          responsivePriority: 1,
          targets: 4
        },
        {
          // Actions
          targets: -1,
          title: 'Acciones',
          orderable: false,
          render: function (data, type, full, meta) {
            return (
              '<button class="btn btn-success ver-historiaclinica" data-id="' + full['cedula_paciente'] + '" title="Ver Información">'
              + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
              '</button>'
            );
          }
        }
      ],
      order: [[1, 'asc']],
      responsive: {
        details: {
          display: $.fn.dataTable.Responsive.display.modal({
            header: function (row) {
              var data = row.data();
              return 'Informacion completa para ' + data['primer_nombre'];
            }
          }),
          type: 'column',
          renderer: function (api, rowIdx, columns) {
            var data = $.map(columns, function (col, i) {

              return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                ? '<tr data-dt-row="' +
                col.rowIndex +
                '" data-dt-column="' +
                col.columnIndex +
                '">' +
                '<td>' +
                col.title +
                ':' +
                '</td> ' +
                '<td>' +
                col.data +
                '</td>' +
                '</tr>'
                : '';
            }).join('');

            return data ? $('<table class="table"/>').append(data) : false;
          }
        }
      },
      language: {
        paginate: {
          // remove previous & next text from pagination
          previous: '&nbsp;',
          next: '&nbsp;'
        }
      }
    });
    $('div.head-label').html('<h6 class="mb-0">Historias Clinicas</h6>');
  }

  $(document).on("click", ".ver-historiaclinica", function () {
    $("#modal-historia-clinica").modal("show");
    $.ajax({
      type: 'GET',
      url: '/administrativo/historiasclinicas/show/' + $(this).attr("data-id"),
      success: function (data) {
        $("#exampleModalCenterTitle").html('Descripción  de Información');
        $("#contenido-historia-clinica").html(data);
      },
      error: function (error) {
        $("#mensajes").html("<div class='alert alert-danger' style='padding:10px'>" + error.responseJSON.errors.nombre_rol + "</div>");
      },
    });
  });
  // -------------------------------------- FIN TABLA HISTORIAS CLINICAS PERFIL USUARIO --------------------------------------


  //  -------------------------------------- TABLA ROLES --------------------------------------

  if (tabla_roles.length) {
    var dt_basic = tabla_roles.DataTable({
      ajax: '/seguridad/roles/table/listado',
      columns: [
        { data: 'id' }, // used for sorting so will hide this column
        { data: 'id' }, // used for sorting so will hide this column
        { data: 'name' },
        { data: 'guard_name' },
        { data: 'created_at' },
        { data: '' }
      ],
      columnDefs: [
        {
          className: 'control',
          orderable: false,
          responsivePriority: 2,
          targets: 0
        },
        {
          responsivePriority: 1,
          targets: 4
        },
        {
          // Actions
          targets: -1,
          title: 'Acciones',
          orderable: false,
          render: function (data, type, full, meta) {

            return (
              '<button class="btn btn-success ver-rol btn-sm" title="Ver información" data-id="' + full['id'] + '">'
              + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
              '</button>'
              +
              ' <a href="/seguridad/roles/edit/' + full['id'] + '" title="Editar información" style="color:white;">'
              + '<button class="btn btn-info btn-sm"">' + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) + '</button>' +
              ' </a>'
              +
              '<button class="btn btn-danger eliminar-rol btn-sm" title="Eliminar información" data-id="' + full['id'] + '">'
              + feather.icons['trash-2'].toSvg({ class: 'font-small-4' }) +
              '</button>'

            );
          }
        }
      ],
      order: [[2, 'asc']],
      responsive: {
        details: {
          display: $.fn.dataTable.Responsive.display.modal({
            header: function (row) {
              var data = row.data();
              return 'Details of ' + data['name'];
            }
          }),
          type: 'column',
          renderer: function (api, rowIdx, columns) {
            var data = $.map(columns, function (col, i) {

              return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                ? '<tr data-dt-row="' +
                col.rowIndex +
                '" data-dt-column="' +
                col.columnIndex +
                '">' +
                '<td>' +
                col.title +
                ':' +
                '</td> ' +
                '<td>' +
                col.data +
                '</td>' +
                '</tr>'
                : '';
            }).join('');

            return data ? $('<table class="table"/>').append(data) : false;
          }
        }
      },
      language: {
        paginate: {
          // remove previous & next text from pagination
          previous: '&nbsp;',
          next: '&nbsp;'
        }
      }
    });
    $('div.head-label').html('<h6 class="mb-0">DataTable with Buttons</h6>');
  }

  $(document).on("click", ".ver-rol", function () {
    $("#modal-ver-rol").modal("show");
    $.ajax({
      type: 'GET',
      url: '/seguridad/roles/show/' + $(this).attr("data-id"),
      success: function (data) {
        console.log(data);
        $("#contenido-rol").html(data);
      },
      error: function (error) {
        console.log(error);
      },
    });
  });
  // -------------------------------------- FIN TABLA ROLES --------------------------------------


  //  -------------------------------------- TABLA PACIENTES --------------------------------------

  if (tabla_pacientes.length) {
    var dt_basic = tabla_pacientes.DataTable({
      ajax: '/administrativo/pacientes/table/listado',
      columns: [
        { data: 'vacio' }, // used for sorting so will hide this column
        {
          "data": 'notificacion',
          "render": function (data, data2, type, row, meta) {
            return feather.icons['bell'].toSvg({ class: 'notifications_pacientes', datacode: type["cedula_paciente"], style: "width:20px; height:20px;" });
          }
        }, // used for sorting so will hide this column
        { data: 'cedula_paciente' },
        { data: 'primer_nombre' },
        { data: 'primer_apellido' },
        { data: 'celular_paciente' },
        { data: 'correo_paciente' },
        { data: 'nombre_estado' },
        { data: '' }
      ],
      columnDefs: [
        {
          className: 'control',
          orderable: false,
          responsivePriority: 1,
          targets: 0
        },
        {
          responsivePriority: 1,
          targets: 2
        },
        {
          // Actions
          targets: -1,
          title: 'Acciones',
          orderable: false,
          render: function (data, type, full, meta) {
            if (full['codigo_estado'] == 3) {
              return (
                ' <button class="btn btn-info editar-pacientes" data-id="' + full['cedula_paciente'] + '">'
                + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) +
                '</button> '
                +
                '<button class="btn btn-danger formulario-pacientes" data-id="' + full['cedula_paciente'] + '">'
                + 'Admisión' +
                '</button>'
              );
            } else {
              return (
                '<button class="btn btn-success ver-pacientes" data-id="' + full['cedula_paciente'] + '" title="Ver Información">'
                + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
                '</button>'
                +
                ' <button class="btn btn-info editar-pacientes" data-id="' + full['cedula_paciente'] + '" title="Editar Información">'
                + feather.icons['edit'].toSvg({ class: 'font-small-4' }) +
                '</button> '
                +
                ' <button class="btn btn-secondary agregar-nota" data-id="' + full['cedula_paciente'] + '" title="Agregar Nota">'
                + feather.icons['plus-square'].toSvg({ class: 'font-small-4' }) +
                '</button>'
                +
                ' <button class="btn btn-dark historia_clinica" data-id="' + full['cedula_paciente'] + '" title="Historia clinica">'
                + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) +
                '</button>'
                +
                ' <button class="btn btn-danger adjuntar_documentos" data-id="' + full['cedula_paciente'] + '" title="Adjntar PDF" style="margin-top:5px;">Subir documentos</button>'
                +
                '<br><button class="btn btn-warning generar-evaluacion" data-id="' + full['cedula_paciente'] + '" style="margin-top:5px;">'
                + feather.icons['book'].toSvg({ class: 'font-small-4' }) +
                ' EVALUACIÓN</button>'
                +
                ' <button class="btn btn-info generar-evolucion" data-id="' + full['cedula_paciente'] + '" style="margin-top:5px;">'
                + feather.icons['trending-up'].toSvg({ class: 'font-small-4' }) +
                ' EVOLUCIÓN </button>'
              );
            }

          }
        }
      ],
      order: [[1, 'asc']],
      responsive: {
        details: {
          display: $.fn.dataTable.Responsive.display.modal({
            header: function (row) {
              var data = row.data();
              return 'Informacion completa para ' + data['primer_nombre'];
            }
          }),
          type: 'column',
          renderer: function (api, rowIdx, columns) {
            var data = $.map(columns, function (col, i) {

              return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                ? '<tr data-dt-row="' +
                col.rowIndex +
                '" data-dt-column="' +
                col.columnIndex +
                '">' +
                '<td>' +
                col.title +
                ':' +
                '</td> ' +
                '<td>' +
                col.data +
                '</td>' +
                '</tr>'
                : '';
            }).join('');

            return data ? $('<table class="table"/>').append(data) : false;
          }
        }
      },
      language: {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
    $('div.head-label').html('<h6 class="mb-0">DataTable with Buttons</h6>');
  }


  $(document).on("click", ".adjuntar_documentos", function () {
    $("#modal-pacientes").modal("show");
    $(".dtr-bs-modal").modal("hide");
    $(".tituloModal").html("Subir documentos");
    $.ajax({
      type: 'GET',
      url: '/administrativo/pacientes/documentos/' + $(this).attr("data-id"),
      success: function (data) {
        $("#contenido-pacientes").html(data);
      },
      error: function (error) {
        $(".modal").modal("hide");
        if (error.status == 403) {
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Usted no tiene permisos para esta acción. Comunicate con soporte.',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        } else {
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Comunicate con soporte.  <b>COD. ERROR:' + error.status + '</b>',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        }
      },
    });
  });


  $(document).on("click", ".ver-pacientes", function () {
    $("#modal-pacientes").modal("show");
    $(".dtr-bs-modal").modal("hide");
    $(".tituloModal").html("Información del paciente");

    $.ajax({
      type: 'GET',
      url: '/administrativo/pacientes/show/' + $(this).attr("data-id"),
      success: function (data) {
        $("#exampleModalCenterTitle").html('Descripción  de Información');
        $("#contenido-pacientes").html(data);
      },
      error: function (error) {
        $(".modal").modal("hide");
        if (error.status == 403) {
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Usted no tiene permisos para esta acción. Comunicate con soporte.',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        } else {
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Comunicate con soporte.  <b>COD. ERROR:' + error.status + '</b>',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        }
      },
    });
  });

  $(document).on("click", ".editar-pacientes", function () {
    $("#modal-pacientes").modal("show");
    $(".dtr-bs-modal").modal("hide");
    $(".tituloModal").html("Actualización de información");

    $.ajax({
      type: 'GET',
      url: '/administrativo/pacientes/edit/' + $(this).attr("data-id"),
      success: function (data) {
        $(".tituloModal").html('Actualizacion de Información');
        $("#contenido-pacientes").html(data);
      },
      error: function (error) {
        $(".modal").modal("hide");
        if (error.status == 403) {
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Usted no tiene permisos para esta acción. Comunicate con soporte.',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        } else {
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Comunicate con soporte.  <b>COD. ERROR:' + error.status + '</b>',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        }
      },
    });
  });

  $(document).on("click", ".agregar-nota", function () {
    $("#modal-pacientes").modal("show");
    $(".dtr-bs-modal").modal("hide");
    $(".tituloModal").html("Agregar nota");

    $.ajax({
      type: 'GET',
      url: '/administrativo/pacientes/notas/' + $(this).attr("data-id"),
      success: function (data) {
        $(".tituloModal").html('Agregar nota');
        $("#contenido-pacientes").html(data);
      },
      error: function (error) {
        $(".modal").modal("hide");
        if (error.status == 403) {
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Usted no tiene permisos para esta acción. Comunicate con soporte.',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        } else {
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Comunicate con soporte.  <b>COD. ERROR:' + error.status + '</b>',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        }
      },
    });
  });

  $(document).on("click", ".formulario-pacientes", function () {
    $("#modal-pacientes-formulario-admision").modal("show");
    $(".dtr-bs-modal").modal("hide");

    $.ajax({
      type: 'GET',
      url: '/administrativo/formulario/pacientes/' + $(this).attr("data-id"),
      success: function (data) {
        $("#exampleModalCenterTitle").html('Formulario de admisión');
        $("#contenido-pacientes-formulario-admision").html(data);
      },
      error: function (error) {
        $(".modal").modal("hide");
        if (error.status == 403) {
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Usted no tiene permisos para esta acción. Comunicate con soporte.',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        } else {
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Comunicate con soporte.  <b>COD. ERROR:' + error.status + '</b>',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        }
      },
    });
  });

  $(document).on("click", ".historia_clinica", function () {
    $("#modal-historia-clinica").modal("show");
    $(".dtr-bs-modal").modal("hide");

    $("#generar-pdf").attr("data-id", $(this).attr("data-id"));
    $.ajax({
      type: 'GET',
      url: '/administrativo/pacientes/historia_clinica/' + $(this).attr("data-id"),
      success: function (data) {
        $(".tituloModal").html('HISTORIA CLINICA');
        $("#contenido-historia-clinica").html(data);
      },
      error: function (error) {
        $(".modal").modal("hide");
        if (error.status == 403) {
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Usted no tiene permisos para esta acción. Comunicate con soporte.',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        } else {
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Oops...',
            text: 'Comunicate con soporte.  <b>COD. ERROR:' + error.status + '</b>',
            footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
          });
        }
      },
    });
  });

  $(document).on("click", ".generar-evaluacion", function () {
    $("#modal-pacientes").modal("show");
    $(".dtr-bs-modal").modal("hide");

    $.ajax({
      type: 'GET',
      url: '/administrativo/pacientes/generar_evaluacion/' + $(this).attr("data-id"),
      success: function (data) {
        $(".tituloModal").html('GENERAR EVALUACIÓN DEL PACIENTE');
        $("#contenido-pacientes").html(data);
      },
      error: function (error) {
        console.log(error);
      },
    });
  });

  $(document).on("click", ".generar-evolucion", function () {
    $("#modal-pacientes").modal("show");
    $(".dtr-bs-modal").modal("hide");

    $.ajax({
      type: 'GET',
      url: '/administrativo/pacientes/generar_evolucion/' + $(this).attr("data-id"),
      success: function (data) {
        $(".tituloModal").html('GENERAR EVOLUCIÓN DEL PACIENTE');
        $("#contenido-pacientes").html(data);
      },
      error: function (error) {
        console.log(error);
      },
    });
  });


  $(document).on("click", ".notifications_pacientes", function () {
    $(".dtr-bs-modal").modal("hide");
    Swal.fire({
      title: 'Realizar notificación',
      text: "Seleccione la notificación que desea realizar para el paciente",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Visita fallida',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          type: 'GET',
          url: '/administrativo/pacientes/registro_notificacion/visita_fallida/' + $(this).attr("datacode"),
          success: function (data) {
            console.log(data);
            Swal.fire(
              'Registrada',
              'La notificación ha sido registrada.',
              'success'
            )
          },
          error: function (error) {
            if (error.status == 403) {
              Swal.fire({
                allowOutsideClick: false,
                icon: 'error',
                title: 'Oops...',
                text: 'Usted no tiene permisos para esta acción. Comunicate con soporte.',
                footer: "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>"
              });
            }
          },
        });
      } else {
        Swal.fire(
          'Cancelado',
          'No se hizo registro previo.',
          'warning'
        )
      }
    })
  });
  // -------------------------------------- FIN TABLA PACIENTES --------------------------------------


  //  -------------------------------------- TABLA PROFESIONES --------------------------------------

  if (tabla_profesiones.length) {
    var dt_basic = tabla_profesiones.DataTable({
      ajax: '/configuracion/profesiones/table/listado',
      columns: [
        { data: 'codigo_profesion' }, // used for sorting so will hide this column
        { data: 'codigo_profesion' }, // used for sorting so will hide this column
        { data: 'nombre_profesion' },
        { data: 'descripcion_profesion' },
        { data: 'nombre_estado' },
        { data: '' }
      ],
      columnDefs: [
        {
          className: 'control',
          orderable: false,
          responsivePriority: 2,
          targets: 0
        },
        {
          responsivePriority: 1,
          targets: 4
        },
        {
          // Actions
          targets: -1,
          title: 'Acciones',
          orderable: false,
          render: function (data, type, full, meta) {
            if (full['codigo_estado'] == 1) {
              return (
                '<button class="btn btn-success ver-profesion btn-sm" title="Ver información" data-id="' + full['codigo_profesion'] + '">'
                + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
                '</button>'
                +
                ' <button class="btn btn-info editar-profesion btn-sm" title="Editar información" data-id="' + full['codigo_profesion'] + '">'
                + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) +
                '</button> '
                +
                '<button class="btn btn-danger eliminar-profesion btn-sm" title="Eliminar información" data-id="' + full['codigo_profesion'] + '">'
                + feather.icons['trash-2'].toSvg({ class: 'font-small-4' }) +
                '</button>'
              );
            } else {
              return (
                '<button class="btn btn-success ver-profesion btn-sm" title="Ver información" data-id="' + full['codigo_profesion'] + '">'
                + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
                '</button>'
                +
                ' <button class="btn btn-info editar-profesion btn-sm" title="Editar información" data-id="' + full['codigo_profesion'] + '">'
                + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) +
                '</button> '
              );
            }

          }
        }
      ],
      order: [[2, 'asc']],
      responsive: {
        details: {
          display: $.fn.dataTable.Responsive.display.modal({
            header: function (row) {
              var data = row.data();
              return 'Detalles para ' + data['nombre_profesion'];
            }
          }),
          type: 'column',
          renderer: function (api, rowIdx, columns) {
            var data = $.map(columns, function (col, i) {

              return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                ? '<tr data-dt-row="' +
                col.rowIndex +
                '" data-dt-column="' +
                col.columnIndex +
                '">' +
                '<td>' +
                col.title +
                ':' +
                '</td> ' +
                '<td>' +
                col.data +
                '</td>' +
                '</tr>'
                : '';
            }).join('');

            return data ? $('<table class="table"/>').append(data) : false;
          }
        }
      },
      language: {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
    $('div.head-label').html('<h6 class="mb-0"></h6>');
  }

  $(document).on("click", ".ver-profesion", function () {
    $(".modal").modal("hide");
    $("#modal-profesion").modal("show");
    $.ajax({
      type: 'GET',
      url: '/configuracion/profesiones/show/' + $(this).attr("data-id"),
      success: function (data) {
        $("#exampleModalCenterTitle").html('Descripción  de Información');
        $("#contenido-profesion").html(data);
      },
      error: function (error) {
        console.log(error);
      },
    });
  });

  $(document).on("click", ".editar-profesion", function () {
    $(".modal").modal("hide");
    $("#modal-profesion").modal("show");
    $.ajax({
      type: 'GET',
      url: '/configuracion/profesiones/edit/' + $(this).attr("data-id"),
      success: function (data) {
        $("#exampleModalCenterTitle").html('Actualizacion de Información');
        $("#contenido-profesion").html(data);
      },
      error: function (error) {
        console.log(error);
      },
    });
  });


  $(document).on("click", ".eliminar-profesion", function () {
    $(".dtr-bs-modal").modal("hide");
    Swal.fire({
      title: '¿Desea eliminar el registro?',
      text: "No podrás revertir esto",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar.',
      cancelButtonText: 'No, cancelar.',
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          type: 'GET',
          url: '/configuracion/profesiones/destroy/' + $(this).attr("data-id"),
          processData: false,
          contentType: false,
          success: function (data) {
            console.log(data);
            if (data == true) {
              setTimeout(function () {
                var opciones = tabla_profesiones.DataTable();
                opciones.ajax.reload();
              }, 2000);

            } else {

            }

          },
          error: function (err) {
            if (err.status == 422) {

              $(".errores").hide();
              console.log(err.responseJSON);
              $('#success_message').fadeIn().html(err.responseJSON.message);
              console.warn(err.responseJSON.errors);
              $.each(err.responseJSON.errors, function (i, error) {
                var el = $(document).find('[name="' + i + '"]');
                el.after($('<span class="errores" style="color: red;">' + error[0] + '</span>'));
              });
            }
          },
        });
        Swal.fire(
          'Eliminado!',
          'La información ha sido eliminada.',
          'success'
        )
      }
    })
  });
  // -------------------------------------- FIN TABLA PROFESIONES --------------------------------------


  //  -------------------------------------- TABLA USUARIOS --------------------------------------

  // -------------------------------------- FIN TABLA USUARIOS --------------------------------------


  //  -------------------------------------- TABLA PROCEDIMIENTOS --------------------------------------

  if (tabla_procedimientos.length) {
    var dt_basic = tabla_procedimientos.DataTable({
      ajax: '/configuracion/procedimientos/table/listado',
      columns: [
        { data: 'vacio' }, // used for sorting so will hide this column
        { data: 'codigo_procedimiento' }, // used for sorting so will hide this column
        { data: 'nombre_procedimiento' },
        { data: 'descripcion_procedimiento' },
        { data: 'nombre_estados' },
        { data: '' }
      ],
      columnDefs: [
        {
          className: 'control',
          orderable: false,
          responsivePriority: 2,
          targets: 0
        },
        {
          responsivePriority: 1,
          targets: 4
        },
        {
          // Actions
          targets: -1,
          title: 'Acciones',
          orderable: false,
          render: function (data, type, full, meta) {
            if (full['codigo_estados'] == 1) {
              return (
                '<button class="btn btn-success btn-sm ver-procedimiento" title="Ver información" data-id="' + full['codigo_procedimiento'] + '">'
                + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
                '</button>'
                +
                ' <button class="btn btn-info btn-sm editar-procedimiento" title="Editar información" data-id="' + full['codigo_procedimiento'] + '">'
                + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) +
                '</button> '
                +
                '<button class="btn btn-danger btn-sm eliminar-procedimiento" title="Eliminar información" data-id="' + full['codigo_procedimiento'] + '">'
                + feather.icons['trash-2'].toSvg({ class: 'font-small-4' }) +
                '</button>'

              );
            } else {
              return (
                '<button class="btn btn-success btn-sm ver-procedimiento" title="Ver Informacion" data-id="' + full['codigo_procedimiento'] + '">'
                + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
                '</button>'
                +
                ' <button class="btn btn-info btn-sm editar-procedimiento" title="Editar Informacion" data-id="' + full['codigo_procedimiento'] + '">'
                + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) +
                '</button> '

              );
            }

          }
        }
      ],
      order: [[2, 'asc']],
      responsive: {
        details: {
          display: $.fn.dataTable.Responsive.display.modal({
            header: function (row) {
              var data = row.data();
              return 'Detalles para ' + data['nombre_procedimiento'];
            }
          }),
          type: 'column',
          renderer: function (api, rowIdx, columns) {
            var data = $.map(columns, function (col, i) {

              return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                ? '<tr data-dt-row="' +
                col.rowIndex +
                '" data-dt-column="' +
                col.columnIndex +
                '">' +
                '<td>' +
                col.title +
                ':' +
                '</td> ' +
                '<td>' +
                col.data +
                '</td>' +
                '</tr>'
                : '';
            }).join('');

            return data ? $('<table class="table"/>').append(data) : false;
          }
        }
      },
      language: {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
    $('div.head-label').html('<h6 class="mb-0"></h6>');
  }


  $(document).on("click", ".ver-procedimiento", function () {
    $("#modal-procedimiento").modal("show");
    $.ajax({
      type: 'GET',
      url: '/configuracion/procedimientos/show/' + $(this).attr("data-id"),
      success: function (data) {
        $("#exampleModalCenterTitle").html('Descripción  de Información');
        $("#contenido-procedimiento").html(data);
      },
      error: function (error) {
        console.log(error);
      },
    });
  });

  $(document).on("click", ".editar-procedimiento", function () {
    $("#modal-procedimiento").modal("show");
    $.ajax({
      type: 'GET',
      url: '/configuracion/procedimientos/edit/' + $(this).attr("data-id"),
      success: function (data) {
        $("#exampleModalCenterTitle").html('Actualizacion de Información');
        $("#contenido-procedimiento").html(data);
      },
      error: function (error) {
        console.log(error);
      },
    });
  });

  $(document).on("click", ".eliminar-procedimiento", function () {
    $(".dtr-bs-modal").modal("hide");
    Swal.fire({
      title: '¿Desea eliminar el registro?',
      text: "No podrás revertir esto",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar.',
      cancelButtonText: 'No, cancelar.',
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          type: 'GET',
          url: '/configuracion/procedimientos/destroy/' + $(this).attr("data-id"),
          processData: false,
          contentType: false,
          success: function (data) {
            console.log(data);
            if (data == true) {
              setTimeout(function () {
                var opciones = tabla_procedimientos.DataTable();
                opciones.ajax.reload();
              }, 1000);

            } else {

            }

          },
          error: function (err) {
            if (err.status == 422) {

              $(".errores").hide();
              console.log(err.responseJSON);
              $('#success_message').fadeIn().html(err.responseJSON.message);
              console.warn(err.responseJSON.errors);
              $.each(err.responseJSON.errors, function (i, error) {
                var el = $(document).find('[name="' + i + '"]');
                el.after($('<span class="errores" style="color: red;">' + error[0] + '</span>'));
              });
            }
          },
        });
        Swal.fire(
          'Eliminado!',
          'La información ha sido eliminada.',
          'success'
        )
      }
    })
  });
  // -------------------------------------- FIN TABLA PROCEDIMIENTOS --------------------------------------


  //  -------------------------------------- TABLA PROGRAMAS --------------------------------------

  if (tabla_programas.length) {
    var dt_basic = tabla_programas.DataTable({
      ajax: '/configuracion/programas/table/listado',
      columns: [
        { data: 'vacio' }, // used for sorting so will hide this column
        { data: 'id_programadas' }, // used for sorting so will hide this column
        { data: 'titulo_programas' },
        { data: 'descripcion_programas' },
        { data: 'nombre_estados' },
        { data: '' }
      ],
      columnDefs: [
        {
          className: 'control',
          orderable: false,
          responsivePriority: 2,
          targets: 0
        },
        {
          responsivePriority: 1,
          targets: 4
        },
        {
          // Actions
          targets: -1,
          title: 'Acciones',
          orderable: false,
          render: function (data, type, full, meta) {
            if (full['codigo_estados'] == 1) {
              return (
                '<button class="btn btn-success btn-sm ver-programa" title="Ver información" data-id="' + full['id_programadas'] + '">'
                + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
                '</button>'
                +
                ' <button class="btn btn-info btn-sm editar-programa" title="Editar información" data-id="' + full['id_programadas'] + '">'
                + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) +
                '</button> '
                +
                '<button class="btn btn-danger btn-sm eliminar-programa" title="Eliminar información" data-id="' + full['id_programadas'] + '">'
                + feather.icons['trash-2'].toSvg({ class: 'font-small-4' }) +
                '</button>'
              );
            } else {
              return (
                '<button class="btn btn-success btn-sm ver-programa" title="Ver Información" data-id="' + full['id_programadas'] + '">'
                + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
                '</button>'
                +
                ' <button class="btn btn-info btn-sm editar-programa" title="Editar Información" data-id="' + full['id_programadas'] + '">'
                + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) +
                '</button> '
              );
            }

          }
        }
      ],
      order: [[2, 'asc']],
      responsive: {
        details: {
          display: $.fn.dataTable.Responsive.display.modal({
            header: function (row) {
              var data = row.data();
              return 'Detalles para ' + data['nombre_procedimiento'];
            }
          }),
          type: 'column',
          renderer: function (api, rowIdx, columns) {
            var data = $.map(columns, function (col, i) {

              return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                ? '<tr data-dt-row="' +
                col.rowIndex +
                '" data-dt-column="' +
                col.columnIndex +
                '">' +
                '<td>' +
                col.title +
                ':' +
                '</td> ' +
                '<td>' +
                col.data +
                '</td>' +
                '</tr>'
                : '';
            }).join('');

            return data ? $('<table class="table"/>').append(data) : false;
          }
        }
      },
      language: {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
    $('div.head-label').html('<h6 class="mb-0"></h6>');
  }


  $(document).on("click", ".ver-programa", function () {
    $("#modal-procedimiento").modal("show");
    $.ajax({
      type: 'GET',
      url: '/configuracion/programas/show/' + $(this).attr("data-id"),
      success: function (data) {
        $("#exampleModalCenterTitle").html('Descripción  de Información');
        $("#contenido-procedimiento").html(data);
      },
      error: function (error) {
        console.log(error);
      },
    });
  });

  $(document).on("click", ".editar-programa", function () {
    $("#modal-procedimiento").modal("show");
    $.ajax({
      type: 'GET',
      url: '/configuracion/programas/edit/' + $(this).attr("data-id"),
      success: function (data) {
        $("#exampleModalCenterTitle").html('Actualizacion de Información');
        $("#contenido-procedimiento").html(data);
      },
      error: function (error) {
        console.log(error);
      },
    });
  });


  $(document).on("click", ".eliminar-programa", function () {
    $(".dtr-bs-modal").modal("hide");
    Swal.fire({
      title: '¿Desea eliminar el registro?',
      text: "No podrás revertir esto",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar.',
      cancelButtonText: 'No, cancelar.',
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          type: 'GET',
          url: '/configuracion/programas/destroy/' + $(this).attr("data-id"),
          processData: false,
          contentType: false,
          success: function (data) {
            console.log(data);
            if (data == true) {
              setTimeout(function () {
                var opciones = tabla_programas.DataTable();
                opciones.ajax.reload();
              }, 1000);

            } else {

            }

          },
          error: function (err) {
            if (err.status == 422) {

              $(".errores").hide();
              console.log(err.responseJSON);
              $('#success_message').fadeIn().html(err.responseJSON.message);
              console.warn(err.responseJSON.errors);
              $.each(err.responseJSON.errors, function (i, error) {
                var el = $(document).find('[name="' + i + '"]');
                el.after($('<span class="errores" style="color: red;">' + error[0] + '</span>'));
              });
            }
          },
        });
        Swal.fire(
          'Eliminado!',
          'La información ha sido eliminada.',
          'success'
        )
      }
    })
  });


  // -------------------------------------- FIN TABLA PROGRAMAS --------------------------------------


  //  -------------------------------------- TABLA EPS --------------------------------------

  if (tabla_eps.length) {
    var dt_basic = tabla_eps.DataTable({
      ajax: '/configuracion/eps/table/listado',
      columns: [
        { data: 'vacio' }, // used for sorting so will hide this column
        { data: 'nombre_eps' }, // used for sorting so will hide this column
        { data: 'descripcion_eps' },
        { data: 'telefono_eps' },
        { data: 'direccion_eps' },
        { data: 'nombre_estados' },
        { data: '' }
      ],
      columnDefs: [
        {
          className: 'control',
          orderable: false,
          responsivePriority: 2,
          targets: 4
        },
        {
          responsivePriority: 1,
          targets: 4
        },
        {
          // Actions
          targets: -1,
          title: 'Acciones',
          orderable: false,
          render: function (data, type, full, meta) {
            if (full['codigo_estados'] == 1) {
              return (
                ' <button class="btn btn-info btn-sm editar-eps" title="Editar Información" data-id="' + full['id_eps'] + '">'
                + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) +
                '</button> '
                +
                '<button class="btn btn-danger btn-sm eliminar-eps" title="Eliminar Información" data-id="' + full['id_eps'] + '">'
                + feather.icons['trash-2'].toSvg({ class: 'font-small-4' }) +
                '</button>'

              );
            } else {
              return (
                ' <button class="btn btn-info btn-sm editar-eps" data-id="' + full['id_eps'] + '">'
                + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) +
                '</button> '

              );
            }

          }
        }
      ],
      order: [[2, 'asc']],
      responsive: {
        details: {
          display: $.fn.dataTable.Responsive.display.modal({
            header: function (row) {
              var data = row.data();
              return 'Detalles para ' + data['nombre_eos'];
            }
          }),
          type: 'column',
          renderer: function (api, rowIdx, columns) {
            var data = $.map(columns, function (col, i) {

              return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                ? '<tr data-dt-row="' +
                col.rowIndex +
                '" data-dt-column="' +
                col.columnIndex +
                '">' +
                '<td>' +
                col.title +
                ':' +
                '</td> ' +
                '<td>' +
                col.data +
                '</td>' +
                '</tr>'
                : '';
            }).join('');

            return data ? $('<table class="table"/>').append(data) : false;
          }
        }
      },
      language: {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
    $('div.head-label').html('<h6 class="mb-0"></h6>');
  }


  $(document).on("click", ".editar-eps", function () {
    $("#modal-eps").modal("show");
    $.ajax({
      type: 'GET',
      url: '/configuracion/eps/edit/' + $(this).attr("data-id"),
      success: function (data) {
        $(".titulo_modal").html('Actualizacion de Información');
        $("#contenido-eps").html(data);
      },
      error: function (error) {
        console.log(error);
      },
    });
  });


  $(document).on("click", ".eliminar-eps", function () {
    $(".dtr-bs-modal").modal("hide");
    Swal.fire({
      title: '¿Desea eliminar el registro?',
      text: "No podrás revertir esto",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar.',
      cancelButtonText: 'No, cancelar.',
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          type: 'GET',
          url: '/configuracion/eps/destroy/' + $(this).attr("data-id"),
          processData: false,
          contentType: false,
          success: function (data) {
            console.log(data);
            if (data == true) {
              setTimeout(function () {
                var opciones = tabla_eps.DataTable();
                opciones.ajax.reload();
              }, 1000);

            } else {

            }

          },
          error: function (err) {
            if (err.status == 422) {

              $(".errores").hide();
              console.log(err.responseJSON);
              $('#success_message').fadeIn().html(err.responseJSON.message);
              console.warn(err.responseJSON.errors);
              $.each(err.responseJSON.errors, function (i, error) {
                var el = $(document).find('[name="' + i + '"]');
                el.after($('<span class="errores" style="color: red;">' + error[0] + '</span>'));
              });
            }
          },
        });
        Swal.fire(
          'Eliminado!',
          'La información ha sido eliminada.',
          'success'
        )
      }
    })
  });

  // -------------------------------------- FIN TABLA EPS --------------------------------------


  //  -------------------------------------- TABLA CIE10 --------------------------------------

  if (tabla_cie10.length) {
    var dt_basic = tabla_cie10.DataTable({
      ajax: '/configuracion/cie10/table/listado',
      columns: [
        { data: 'codigo' }, // used for sorting so will hide this column
        { data: 'descripcion' }, // used for sorting so will hide this column
        { data: 'nombre_estado' }
      ],
      order: [[2, 'asc']],
      responsive: {
        details: {
          display: $.fn.dataTable.Responsive.display.modal({
            header: function (row) {
              var data = row.data();
              return 'Detalles para ' + data['nombre_eos'];
            }
          }),
          type: 'column',
          renderer: function (api, rowIdx, columns) {
            var data = $.map(columns, function (col, i) {

              return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                ? '<tr data-dt-row="' +
                col.rowIndex +
                '" data-dt-column="' +
                col.columnIndex +
                '">' +
                '<td>' +
                col.title +
                ':' +
                '</td> ' +
                '<td>' +
                col.data +
                '</td>' +
                '</tr>'
                : '';
            }).join('');

            return data ? $('<table class="table"/>').append(data) : false;
          }
        }
      },
      language: {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
    $('div.head-label').html('<h6 class="mb-0"></h6>');
  }


  $(document).on("click", ".editar-eps", function () {
    $("#modal-eps").modal("show");
    $.ajax({
      type: 'GET',
      url: '/configuracion/eps/edit/' + $(this).attr("data-id"),
      success: function (data) {
        $(".titulo_modal").html('Actualizacion de Información');
        $("#contenido-eps").html(data);
      },
      error: function (error) {
        console.log(error);
      },
    });
  });
  // -------------------------------------- FIN TABLA CIE10 --------------------------------------



  //  -------------------------------------- TABLA INTERCONSULTAS --------------------------------------

  if (tabla_interconsultas.length) {
    var dt_basic = tabla_interconsultas.DataTable({
      ajax: '/administrativo/pacientes/interconsultas/table/listado',
      columns: [
        { data: 'vacio' }, // used for sorting so will hide this column
        { data: 'codigo_interconsulta' },
        { data: 'cedula_paciente' },
        { data: 'nombres_pacientes' },
        { data: 'fecha' },
        { data: 'elaborado' },
        { data: '' }
      ],
      columnDefs: [
        {
          className: 'control',
          orderable: false,
          responsivePriority: 1,
          targets: 0
        },
        {
          responsivePriority: 1,
          targets: 0
        },
        {
          // Actions
          targets: -1,
          title: 'Acciones',
          orderable: false,
          render: function (data, type, full, meta) {
            return (
              '<button class="btn btn-success btn-sm ver-interconsulta" data-id="' + full['codigo_interconsulta'] + '" title="Ver Información">'
              + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
              '</button>'
              +
              ' <a class="btn btn-danger btn-sm " href="interconsultas/pdf/' + full['codigo_interconsulta'] + '" title="Exportar a PDF">'
              + feather.icons['file'].toSvg({ class: 'font-small-4' }) +
              '</a> '
            );

          }
        }
      ],
      order: [[1, 'asc']],
      responsive: {
        details: {
          display: $.fn.dataTable.Responsive.display.modal({
            header: function (row) {
              var data = row.data();
              return 'Informacion completa para ' + data['primer_nombre'];
            }
          }),
          type: 'column',
          renderer: function (api, rowIdx, columns) {
            var data = $.map(columns, function (col, i) {

              return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                ? '<tr data-dt-row="' +
                col.rowIndex +
                '" data-dt-column="' +
                col.columnIndex +
                '">' +
                '<td>' +
                col.title +
                ':' +
                '</td> ' +
                '<td>' +
                col.data +
                '</td>' +
                '</tr>'
                : '';
            }).join('');

            return data ? $('<table class="table"/>').append(data) : false;
          }
        }
      },
      language: {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
    $('div.head-label').html('<h6 class="mb-0">DataTable with Buttons</h6>');
  }


  $(document).on("click", ".ver-interconsulta", function () {
    $("#modal-interconsulta").modal("show");
    $.ajax({
      type: 'GET',
      url: '/administrativo/pacientes/interconsultas/show/' + $(this).attr("data-id"),
      success: function (data) {
        $("#tituloModal").html('Detalles de interconsulta');
        $("#contenido-interconsulta").html(data);
      },
      error: function (error) {
        console.log(error);
      },
    });
  });

});
