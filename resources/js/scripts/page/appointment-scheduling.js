$(function () {
    'use strict';

    const forms = document.querySelectorAll('.form_agenda_cita');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {

                var paqueteDeDatos = new FormData();

                paqueteDeDatos.append('fecha_seleccionada', $('#fecha_seleccionada').prop('value'));
                paqueteDeDatos.append('hora_inicial', $('#hora_inicial').prop('value'));
                paqueteDeDatos.append('hora_final', $('#hora_final').val());
                paqueteDeDatos.append('nombre', $('#nombre').prop('value'));
                paqueteDeDatos.append('apellidos', $('#apellidos').prop('value'));
                paqueteDeDatos.append('celular', $('#celular').prop('value'));
                paqueteDeDatos.append('correo', $('#correo').prop('value'));


                document.getElementById("fecha_seleccionada").disabled = true;
                document.getElementById("hora_inicial").disabled = true;
                document.getElementById("hora_final").disabled = true;
                document.getElementById("nombre").disabled = true;
                document.getElementById("apellidos").disabled = true;
                document.getElementById("celular").disabled = true;
                document.getElementById("correo").disabled = true;

                document.getElementById("button_agendar").disabled = true;
                document.getElementById("cancelar_agenda").disabled = true;

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "POST",
                    contentType: false,
                    url: "/register-appointment-scheduling",
                    headers: {
                        "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content"),
                    },
                    data: paqueteDeDatos, // serializes the form's elements.
                    processData: false,
                    cache: false,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire(
                                "Registro exitoso!",
                                "El registro ha sido exitoso!",
                                "success"
                            );

                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        }
                    },
                    error: function (err) {
                        document.getElementById("fecha_seleccionada").disabled = false;
                        document.getElementById("hora_inicial").disabled = false;
                        document.getElementById("hora_final").disabled = false;
                        document.getElementById("nombre").disabled = false;
                        document.getElementById("apellidos").disabled = false;
                        document.getElementById("celular").disabled = false;
                        document.getElementById("correo").disabled = false;

                        document.getElementById("button_agendar").disabled = false;
                        document.getElementById("cancelar_agenda").disabled = false;

                        errorHttp(err.status);
                        if (err.status == 422) {
                            $(".errores").attr("style", "display:none;");
                            $("#success_message").fadeIn().html(err.responseJSON.message);

                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' + i + '"]');
                                el.after(
                                    $(
                                        '<span class="errores" style="color: red;">' +
                                        error[0] +
                                        "</span>"
                                    )
                                );
                            });
                        }
                    },
                });

                event.preventDefault();

            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});