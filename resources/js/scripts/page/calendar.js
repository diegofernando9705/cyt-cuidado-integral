
    /* Calendario */

    // Obtener los elementos del DOM necesarios
    const calendarBody = document.getElementById('calendar-body');
    const currentMonth = document.getElementById('current-month');
    const prevMonthBtn = document.getElementById('prev-month');
    const nextMonthBtn = document.getElementById('next-month');

    // Obtener la fecha actual
    let date = new Date();

    // Función para generar el calendario
    function generateCalendar() {
        // Obtener el número de días en el mes actual y el día de la semana del primer día del mes
        let daysInMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
        let firstDayOfMonth = new Date(date.getFullYear(), date.getMonth(), 1).getDay();

        // Crear una cadena de caracteres con las celdas de los días del calendario
        let days = '';
        for (let i = 0; i < firstDayOfMonth; i++) {
            days += '<td></td>';
        }

        for (let i = 1; i <= daysInMonth; i++) {
            let dayClass = '';
            if (i === new Date().getDate() && date.getMonth() === new Date().getMonth()) {
                dayClass = 'current-day';
            }
            days += `<td class='dia_mes_seleccion ${dayClass}'><a>${i}</a></td>`;
            if ((firstDayOfMonth + i) % 7 === 0) {
                days += '</tr><tr>';
            }
        }

        // Insertar la cadena de caracteres en el cuerpo de la tabla del calendario
        calendarBody.innerHTML = '<tr>' + days + '</tr>';

        // Actualizar el mes y el año en el encabezado del calendario
        currentMonth.textContent = date.toLocaleDateString('es-ES', {
            month: 'long',
            year: 'numeric'
        });
    }

    // Generar el calendario inicial
    generateCalendar();

    // Agregar event listeners a los botones de cambiar de mes
    prevMonthBtn.addEventListener('click', () => {
        date.setMonth(date.getMonth() - 1);
        generateCalendar();
    });
    nextMonthBtn.addEventListener('click', () => {
        date.setMonth(date.getMonth() + 1);
        generateCalendar();
    });

    // Función para mostrar eventos en el calendario
    function showEvent(eventDate) {
        // Obtener el día del evento
        let eventDay = new Date(eventDate).getDate();

        // Obtener todas las celdas de la tabla del calendario
        let calendarCells = calendarBody.getElementsByTagName('td');

        // Recorrer todas las celdas y resaltar las que corresponden al día del evento
        for (let i = 0; i < calendarCells.length; i++) {
            let cell = calendarCells[i];
            if (cell.textContent === String(eventDay)) {
                cell.classList.add('event');
            }
        }
    }

    /* Fin calendario */

    $(document).on("click", ".dia_mes_seleccion", function() {
        document.getElementById("nombre").disabled = false;
        document.getElementById("apellidos").disabled = false;
        document.getElementById("celular").disabled = false;
        document.getElementById("correo").disabled = false;
        document.getElementById("fecha_seleccionada").disabled = false;
        document.getElementById("hora_inicial").disabled = false;
        document.getElementById("hora_final").disabled = false;
        document.getElementById("button_agendar").disabled = false;
        document.getElementById("cancelar_agenda").disabled = false;

        $('#nombre').val("");
        $('#apellidos').val("");
        $('#celular').val("");
        $('#correo').val("");

        $('#fecha_seleccionada').val("");
        $('#hora_inicial').val("08:00");
        $('#hora_final').val("08:00");

        $("#modal_calendar").modal("show");
    });
