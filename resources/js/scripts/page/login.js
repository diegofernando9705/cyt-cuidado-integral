$(document).on("click", "#togglePassword", function () {
    var passwordInput = document.getElementById('password');
    var togglePasswordButton = document.getElementById('togglePassword');

    var type = passwordInput.getAttribute('type') === 'password' ? 'text' : 'password';

    passwordInput.setAttribute('type', type);
    togglePasswordButton.classList.toggle('fa-eye-slash');
    togglePasswordButton.classList.toggle('fa-eye');
});

$(function () {
    'use strict';

    const forms = document.querySelectorAll('.formulario-login');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {

                var paqueteDeDatos = new FormData();

                paqueteDeDatos.append('_token', $('#csrf_token').prop('value'));
                paqueteDeDatos.append('email', $('#email').prop('value'));
                paqueteDeDatos.append('email', $('#email').prop('value'));
                paqueteDeDatos.append('password', $('#password').prop('value'));

                document.getElementById("email").disabled = true;
                document.getElementById("password").disabled = true;

                document.getElementById("submit").disabled = true;

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "POST",
                    contentType: false,
                    url: "/login-post",
                    headers: {
                        "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content"),
                    },
                    data: paqueteDeDatos, // serializes the form's elements.
                    processData: false,
                    cache: false,
                    success: function (data) {
                        if (data) {
                            $("#alertDanger").removeClass("alert-danger");
                            $("#alertDanger").addClass("alert-info");

                            $("#alertDanger").show();
                            $("#alertDanger").html("Ingreso exitoso. Bienvenido.");

                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        }
                    },
                    error: function (err) {
                        document.getElementById("email").disabled = false;
                        document.getElementById("password").disabled = false;

                        document.getElementById("submit").disabled = false;

                        if (err.status == 422) {
                            $(".errores").attr("style", "display:none;");
                            $("#success_message").fadeIn().html(err.responseJSON.message);

                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' + i + '"]');
                                el.after(
                                    $(
                                        '<span class="errores" style="color: red;">' +
                                        error[0] +
                                        "</span>"
                                    )
                                );
                            });
                        } else if (err.status == 401) {
                            $("#alertDanger").show();
                            $("#alertDanger").html(err.responseJSON.message);

                        }
                    },
                });

                event.preventDefault();

            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });



    const fogot_password = document.querySelectorAll('.auth-forgot-password');

    // Loop over them and prevent submission
    Array.prototype.slice.call(fogot_password).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {

                var paqueteDeDatos = new FormData();

                paqueteDeDatos.append('email', $('#email').prop('value'));
                document.getElementById("email").disabled = true;

                document.getElementById("submit").disabled = true;

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "POST",
                    contentType: false,
                    url: "/send-recovery-password",
                    headers: {
                        "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content"),
                    },
                    data: paqueteDeDatos, // serializes the form's elements.
                    processData: false,
                    cache: false,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire({
                                title: "Instrucciones enviadas!",
                                text: "Se envió un correo electrónico con instrucciones para restablecer la contraseña",
                                icon: "success"
                            });

                            setTimeout(() => {
                                $("#modalLogin").modal("hide");
                            }, 1000);
                        } else {
                            Swal.fire({
                                title: "Error",
                                text: "Lo siento, no se puedo realizar el envio de contraseña.",
                                footer: 'Estamos trabajando para dar solución al inconveniente presentado',
                                icon: "error"
                            });

                            setTimeout(() => {
                                $("#modalLogin").modal("hide");
                            }, 1000);
                        }
                    },
                    error: function (err) {
                        document.getElementById("email").disabled = false;
                        document.getElementById("password").disabled = false;

                        document.getElementById("submit").disabled = false;

                        if (err.status == 422) {
                            $(".errores").attr("style", "display:none;");
                            $("#success_message").fadeIn().html(err.responseJSON.message);

                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' + i + '"]');
                                el.after(
                                    $(
                                        '<span class="errores" style="color: red;">' +
                                        error[0] +
                                        "</span>"
                                    )
                                );
                            });
                        } else if (err.status == 401) {
                            $("#alertDanger").show();
                            $("#alertDanger").html(err.responseJSON.message);

                        }
                    },
                });

                event.preventDefault();

            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });

});


$(document).on("click", ".boton_ir_membresia", function () {
    $(".modal").modal("hide");
});