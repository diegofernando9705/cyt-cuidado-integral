/*=========================================================================================
  File Name: table-datatables-basic.js
  Description: jquery datatable js
  ----------------------------------------------------------------------------------------
  Item Name: SOFTWORLD COLOMBIA - DATATABLES
  Author: SOFTWORLD COLOMBIA
  Project: CENTRO NACIONAL DE PRUEBAS
  Author URL: https://softworldcolombia.com/
==========================================================================================*/

$(function () {
  "use strict";

  var tabla_cursos = $(".datatables-courses"),
    assetPath = "../../../app-assets/";

  if ($("body").attr("data-framework") === "laravel") {
    assetPath = $("body").attr("data-asset-path");
  }

  /* |||||||||||||||| CURSOS |||||||||||||||| */

  if (tabla_cursos.length) {
    var dt_basic = tabla_cursos.DataTable({
      ajax: "/app/learning/courses/listado",
      columns: [
        { data: "vacio" },
        { data: "codigo_curso" },
        {
          data: "imagen_curso",
          render: function (data, type, row, meta) {
            if (data) {
              return (
                "<img src='https://s3.amazonaws.com/cnp.com.co/" +
                data +
                "' width='80px'>"
              );
            } else {
              return (
                "<img src='https://s3.amazonaws.com/cnp.com.co/" +
                data +
                "' width='80px'>"
              );
            }
          },
        },
        { data: "titulo_curso" },
        { data: "fecha_inicio_curso" },
        { data: "fecha_fin_curso" },
        { data: "profesional" },
        { data: "nombre_estado" },
        { data: "" },
      ],
      columnDefs: [
        {
          className: "control",
          orderable: false,
        },
        {
          // Actions
          targets: -1,
          title: "Acciones",
          orderable: false,
          render: function (data, type, full, meta) {
            return (
              ' <button class="btn btn-success ver-curso btn-sm" title="Ver curso" data-id="' +
              full["codigo_curso"] +
              '">' +
              feather.icons["eye"].toSvg({ class: "font-small-4" }) +
              "</button> " +
              ' <button class="btn btn-info editar-curso btn-sm" title="Editar Información" data-id="' +
              full["codigo_curso"] +
              '">' +
              feather.icons["file-text"].toSvg({ class: "font-small-4" }) +
              "</button> " +
              ' <button class="btn btn-danger eliminar-curso btn-sm" title="Eliminar Información" data-id="' +
              full["codigo_curso"] +
              '">' +
              feather.icons["trash-2"].toSvg({ class: "font-small-4" }) +
              "</button>" +
              ' <button class="btn btn-primary modules btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top:5px;" title="Secciones y lecciones del curso" data-id="' +
              full["codigo_curso"] +
              '"> Módulos </button>' +
              ' <button class="btn btn-primary asignar-membresia-cursos btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top:5px;" title="Asignar a membresia" data-id="' +
              full["codigo_curso"] +
              '"> Asignar membresia </button>'
            );
          },
        },
      ],
      order: [[1, "asc"]],
      responsive: {
        details: {
          display: $.fn.dataTable.Responsive.display.modal({
            header: function (row) {
              var data = row.data();
              return "Informacion completa para " + data["titulo_noticia"];
            },
          }),
          type: "column",
          renderer: function (api, rowIdx, columns) {
            var data = $.map(columns, function (col, i) {
              return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                ? '<tr data-dt-row="' +
                col.rowIndex +
                '" data-dt-column="' +
                col.columnIndex +
                '">' +
                "<td>" +
                col.title +
                ":" +
                "</td> " +
                "<td>" +
                col.data +
                "</td>" +
                "</tr>"
                : "";
            }).join("");

            return data ? $('<table class="table"/>').append(data) : false;
          },
        },
      },
      language: {
        url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
      },
    });
    $("div.head-label").html('<h6 class="mb-0">DataTable with Buttons</h6>');
  }

  $(document).on("click", ".ver-curso", function () {
    animationLoading("", "Cargando información...")

    $.ajax({
      type: "GET",
      url: "/app/learning/courses/show/" + $(this).attr("data-id"),
      success: function (data) {
        $("#modal").modal("show");
        $("#modal-title").html("Información detallada");
        $("#modal-content").html(data);
        Swal.close();
      },
      error: function (error) {
        errorHttp(error.status);
      },
    });
  });

  $(document).on("click", ".editar-curso", function () {
    animationLoading("", "Cargando información...")

    $.ajax({
      type: "GET",
      url: "/app/learning/courses/edit/" + $(this).attr("data-id"),
      success: function (data) {
        $("#modal").modal("show");
        $("#modal-title").html("Editar curso");
        $("#modal-content").html(data);
        Swal.close();
      },
      error: function (error) {
        errorHttp(error.status);
      },
    });
  });

  $(document).on("click", ".eliminar-curso", function () {
    $(".modal").modal("hide");
    Swal.fire({
      title: "¿Desea eliminar el registro?",
      text: "No podrás revertir esto",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, eliminar!",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          type: "GET",
          url: "/app/aprendizaje/cursos/delete/" + $(this).attr("data-id"),
          processData: false,
          contentType: false,
          success: function (data) {
            if (data == true) {
              Swal.fire(
                "Eliminado!",
                "La información ha sido eliminada.",
                "success"
              );

              setTimeout(() => {
                var opciones = tabla_cursos.DataTable();
                opciones.ajax.reload();
              }, 2000);
            }
          },
          error: function (err) {
            if (err.status == 422) {
              $(".errores").hide();
              console.log(err.responseJSON);
              $("#success_message").fadeIn().html(err.responseJSON.message);
              console.warn(err.responseJSON.errors);
              $.each(err.responseJSON.errors, function (i, error) {
                var el = $(document).find('[name="' + i + '"]');
                el.after(
                  $(
                    '<span class="errores" style="color: red;">' +
                    error[0] +
                    "</span>"
                  )
                );
              });
            } else {
              $("#modal-services").modal("hide");
              if (err.status == 403) {
                Swal.fire({
                  allowOutsideClick: false,
                  icon: "error",
                  title: "Acción restringida",
                  html:
                    "Usted no tiene permisos para esta acción. Comuniquese con administración. <br><br> <b>CÓD. ERROR: " +
                    err.status +
                    "</b>",
                  footer:
                    "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>",
                });
              } else {
                Swal.fire({
                  allowOutsideClick: false,
                  icon: "error",
                  title: "Error desconocido",
                  html:
                    "Comunicate con soporte. <br> <b>CÓD. ERROR: " +
                    err.status +
                    "</b>",
                  footer:
                    "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>",
                });
              }
            }
          },
        });
      }
    });
  });

  $(document).on("click", ".modules", function () {
    animationLoading("", "Cargando módulos del curso...")

    $.ajax({
      type: "GET",
      url: "/app/learning/courses/edit/modules/" + $(this).attr("data-id"),
      success: function (data) {
        $("#modal").modal("show");

        $(document).off("focusin.modal");
        $.fn.modal.Constructor.prototype._enforceFocus = function () { };

        const containerClaimReqModal = document.getElementById("modal");
        const claimReqModal = new bootstrap.Modal(containerClaimReqModal, {
          focus: false,
        });

        $("#modal-title").html("Membresias del curso");
        $("#modal-content").html(data);
        Swal.close();
      },
      error: function (error) {
        errorHttp(error.status);
      },
    });
  });

  $(document).on("click", ".asignar-membresia-cursos", function () {
    animationLoading("", "Cargando módulos del curso...")

    $.ajax({
      type: "GET",
      url: "/app/learning/courses/memberships/" + $(this).attr("data-id"),
      success: function (data) {
        $("#modal").modal("show");

        $(document).off("focusin.modal");
        $.fn.modal.Constructor.prototype._enforceFocus = function () { };

        const containerClaimReqModal = document.getElementById("modal");
        const claimReqModal = new bootstrap.Modal(containerClaimReqModal, {
          focus: false,
        });

        $("#modal-title").html("Módulos asignados al curso");
        $("#modal-content").html(data);
        Swal.close();
      },
      error: function (error) {
        errorHttp(error.status);
      },
    });
  });

  /* |||||||||||||||| FIN CURSOS |||||||||||||||| */

});
