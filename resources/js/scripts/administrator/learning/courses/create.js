$(document).ready(function () {
    $('.selector').select2();
});

var changePicture = $('#imagen_curso'),
    userAvatar = $('.user-avatar');

// Change user profile picture
if (changePicture.length) {
    $(changePicture).on('change', function (e) {
        var reader = new FileReader(),
            files = e.target.files;
        reader.onload = function () {
            if (userAvatar.length) {
                userAvatar.attr('src', reader.result);
            }
        };
        reader.readAsDataURL(files[0]);
    });
};


var tabla_servicios = $('.datatables-servicios');

CKEDITOR.replace('summary');
CKEDITOR.replace('description');

$(function () {
    'use strict';

    const forms = document.querySelectorAll('.store-cursos');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {

                var paqueteDeDatos = new FormData();

                paqueteDeDatos.append('imagen_curso', $('#imagen_curso')[0].files[0]);
                paqueteDeDatos.append('start_date', $("#start_date").prop('value'));
                paqueteDeDatos.append('end_date', $("#end_date").prop('value'));
                paqueteDeDatos.append('title', $("#title").prop('value'));

                paqueteDeDatos.append('summary', CKEDITOR.instances["summary"].getData());
                paqueteDeDatos.append('description', CKEDITOR.instances["description"].getData());

                paqueteDeDatos.append('video_image', $("#video_image").prop('value'));
                paqueteDeDatos.append('category_id', $("#category_id").prop('value'));
                paqueteDeDatos.append('professional_id', $("#professional_id").prop('value'));

                paqueteDeDatos.append('nivel_curso', $("#nivel_curso").prop('value'));
                paqueteDeDatos.append('student_limit', $("#student_limit").prop('value'));

                var inpFiles = document.getElementById("archivos");
                for (const file of inpFiles.files) {
                    paqueteDeDatos.append("archivos[]", file);
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '/app/learning/courses/create/store',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        storeSuccess();

                        setTimeout(() => {
                            window.location = '/app/learning/courses';
                        }, 1000);
                    },
                    error: function (err) {
                        errorHttp(err.status);
                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});