$(document).ready(function () {
    $('.selector').select2();
});

var changePicture = $('#imagen_curso'),
    userAvatar = $('.user-avatar');

// Change user profile picture
if (changePicture.length) {
    $(changePicture).on('change', function (e) {
        var reader = new FileReader(),
            files = e.target.files;
        reader.onload = function () {
            if (userAvatar.length) {
                userAvatar.attr('src', reader.result);
            }
        };
        reader.readAsDataURL(files[0]);
    });
};


var table_courses = $('.datatables-courses');

var description = document.getElementById("description");

if (description) {
    CKEDITOR.replace('description');
}


$(function () {
    'use strict';

    const forms = document.querySelectorAll('.update-course');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {

                var paqueteDeDatos = new FormData();
                paqueteDeDatos.append('code', $("#code").prop('value'));

                paqueteDeDatos.append('imagen_curso', $('#imagen_curso')[0].files[0]);
                paqueteDeDatos.append('start_date', $("#start_date").prop('value'));
                paqueteDeDatos.append('end_date', $("#end_date").prop('value'));
                paqueteDeDatos.append('title', $("#title").prop('value'));

                paqueteDeDatos.append('summary', $("#summary").prop('value'));
                paqueteDeDatos.append('description', CKEDITOR.instances["description"].getData());

                paqueteDeDatos.append('video_image', $("#video_image").prop('value'));
                paqueteDeDatos.append('category_id', $("#category_id").prop('value'));
                paqueteDeDatos.append('professional_id', $("#professional_id").prop('value'));

                paqueteDeDatos.append('level', $("#level").prop('value'));
                paqueteDeDatos.append('student_limit', $("#student_limit").prop('value'));
                paqueteDeDatos.append('status', $("#status").prop('value'));

                var inpFiles = document.getElementById("archivos");
                for (const file of inpFiles.files) {
                    paqueteDeDatos.append("archivos[]", file);
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '/app/learning/courses/edit/update',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        updateSuccess();

                        setTimeout(() => {
                            var tabla = table_courses.DataTable();
                            tabla.ajax.reload();
                        }, 1000);
                    },
                    error: function (err) {
                        errorHttp(err.status);
                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});



$('#add_module').click(function () {
    Swal.fire({
        title: 'Confirmar',
        text: '¿Estás seguro de enviar el formulario?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, enviar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            mostrarFormulario();
        }
    });
});

function mostrarFormulario() {
    Swal.fire({
        title: 'Nuevo módulo',
        html: `
                <form id="formulario">
                    <div class="form-group">
                        <label>(*) Imagen del módulo</label>
                        <input type="file" class="form-control" id="inputFile" accept="image/*" required>
                    </div>    
                    <div class="form-group">
                        <label>(*) Titulo del módulo</label>
                        <input type="text" class="form-control" id="inputText" required>
                    </div>
                    <div class="form-group">
                        <label>(*) Descripción del módulo</label>
                        <textarea id="textarea" class="form-control" required></textarea>
                    </div>
                        <button class="btn btn-primary btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" type="submit" id="submit-module">Registrar</button>                
                </form>
            `,
        showCancelButton: true,
        cancelButtonText: 'Cancelar registro',
        showConfirmButton: false,
        showCloseButton: false,
        focusConfirm: false,
        focusCancel: false,
        allowOutsideClick: false,
        allowEscapeKey: false
    });
}

// Manejar el envío del formulario a través de AJAX
$(document).on('submit', '#formulario', function (e) {
    e.preventDefault();

    var code_course = $("#id_course").val();
    var csrfToken = $('meta[name="csrf-token"]').attr('content');

    document.getElementById('inputText').disabled = true;
    document.getElementById('inputFile').disabled = true;
    document.getElementById('textarea').disabled = true;
    document.getElementById('submit-module').disabled = true;


    // Obtener los datos del formulario
    var formData = new FormData();

    formData.append('_token', csrfToken);
    formData.append('text', $('#inputText').val());
    formData.append('file', $('#inputFile')[0].files[0]);
    formData.append('textarea', $('#textarea').val());

    // Enviar la solicitud AJAX
    $.ajax({
        url: '/app/learning/courses/edit/modules/' + code_course + '/save',
        method: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            storeSuccess();

            setTimeout(() => {
                animationLoading("", "Cargando módulos del curso...")
                loadingModules(courseId);
            }, 1000);
        },
        error: function (error) {
            errorHttp(error.status);
        }
    });
});


$(document).on("click", ".delete_option", function () {
    var code_course = $(this).attr("code-course");

    Swal.fire({
        icon: "question",
        title: "¿Esta seguro que desea eliminarlo?",
        html: "Una vez eliminado, no se podrá recuperar",
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonColor: "#003f48",
        confirmButtonText: "Si, eliminar",
        denyButtonText: `Cancelar`,
        allowOutsideClick: false,
        allowEscapeKey: false,
        focusConfirm: true,
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            $.ajax({
                type: "GET",
                url: "/app/learning/courses/edit/modules/" + $(this).attr("data-id") + "/delete",
                success: function (data) {
                    deleteSuccess();
                    setTimeout(function () {
                        loadingModules(code_course);
                    }, 1000);
                },
                error: function (error) {
                    errorHttp(error.status);
                },
            });
        } else if (result.isDenied) {
            Swal.fire("Eliminación cancelada", "", "info");
        }
    });
});


$(document).on("click", ".edit_option", function () {

    var code_module = $(this).attr("data-id");
    var code_course = $(this).attr("code-course");

    $.ajax({
        url: '/app/learning/courses/edit/module/' + code_module,
        method: 'GET',
        success: function (response) {
            Swal.fire({
                title: 'Edición del módulo - ' + response.title,
                html: `
                        <form id="formulario-update-module">
                            <input type="hidden" id="code_module" name="code_module" value="`+ response.code + `">
                            <div class="form-group">
                                <label>(*) Imagen del módulo</label>
                                <input type="file" class="form-control" id="inputFile-update" accept="image/*">
                            </div>    
                            <div class="form-group">
                                <label>(*) Titulo del módulo</label>
                                <input type="text" class="form-control" id="inputText-update" value="`+ response.title + `" required>
                            </div>
                            <div class="form-group">
                                <label>(*) Descripción del módulo</label>
                                <textarea id="textarea-update" class="form-control" required>`+ response.summary + `</textarea>
                            </div>
                                <button class="btn btn-primary btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" type="submit" code-course="`+ code_course + `" id="submit-module-update">Actualizar</button>                
                        </form>
                    `,
                showCancelButton: true,
                cancelButtonText: 'Cancelar registro',
                showConfirmButton: false,
                showCloseButton: false,
                focusConfirm: false,
                focusCancel: false,
                allowOutsideClick: false,
                allowEscapeKey: false
            });
        },
        error: function (error) {
            errorHttp(error.status);
        }
    });
});

$(document).on('submit', '#formulario-update-module', function (e) {
    e.preventDefault();

    var code_course = $("#submit-module-update").attr("code-course");
    var code_module = $("#code_module").val();

    var csrfToken = $('meta[name="csrf-token"]').attr('content');

    document.getElementById('inputText-update').disabled = true;
    document.getElementById('inputFile-update').disabled = true;
    document.getElementById('textarea-update').disabled = true;
    document.getElementById('submit-module-update').disabled = true;


    // Obtener los datos del formulario
    var formData = new FormData();

    formData.append('_token', csrfToken);
    formData.append('code_course', code_course);
    formData.append('text', $('#inputText-update').val());
    formData.append('file', $('#inputFile-update')[0].files[0]);
    formData.append('textarea', $('#textarea-update').val());

    // Enviar la solicitud AJAX
    $.ajax({
        url: '/app/learning/courses/edit/module/update/' + code_module,
        method: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            console.log(code_course);
            updateSuccess();

            setTimeout(() => {
                animationLoading("", "Cargando módulos del curso...")
                loadingModules(code_course);
            }, 1500);
        },
        error: function (error) {
            errorHttp(error.status);
        }
    });
});

$(document).on("click", ".button_add_new_lessons", function () {
    animationLoading("", "Cargando formulario...");

    var module = $(this).attr("data-module");

    $.ajax({
        type: "GET",
        url: "/app/templates/form/new_lesson/" + module,
        success: function (data) {
            $("#modal-two").modal("show");

            $("#modal-content-two").html("");

            $(document).off("focusin.modal");
            $.fn.modal.Constructor.prototype._enforceFocus = function () { };

            const containerClaimReqModal = document.getElementById("modal");
            const claimReqModal = new bootstrap.Modal(containerClaimReqModal, {
                focus: false,
            });

            $("#modal-title-two").html("Agregar una nueva lección");
            $("#modal-content-two").html(data);
            Swal.close();
        },
        error: function (error) {
            errorHttp(error.status);
        },
    });
});


$(document).on("click", ".edit_lesson", function () {
    animationLoading("", "Cargando formulario...");

    var code = $(this).attr("data-code");

    $.ajax({
        type: "GET",
        url: "/app/learning/courses/module/lessons/edit/" + code,
        success: function (data) {
            $("#modal-two").modal("show");
            $("#modal-content-two").html("");

            $(document).off("focusin.modal");
            $.fn.modal.Constructor.prototype._enforceFocus = function () { };

            const containerClaimReqModal = document.getElementById("modal");
            const claimReqModal = new bootstrap.Modal(containerClaimReqModal, {
                focus: false,
            });

            $("#modal-title-two").html("Editar lección");
            $("#modal-content-two").html(data);
            Swal.close();
        },
        error: function (error) {
            errorHttp(error.status);
        },
    });
});

$(document).on("click", ".delete_lesson", function () {
    Swal.fire({
        icon: "question",
        title: "¿Esta seguro que desea eliminarlo?",
        html: "Una vez eliminado, no se podrá recuperar",
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonColor: "#003f48",
        confirmButtonText: "Si, eliminar",
        denyButtonText: `Cancelar`,
        allowOutsideClick: false,
        allowEscapeKey: false,
        focusConfirm: true,
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            $.ajax({
                type: "GET",
                url: "/app/learning/courses/module/lessons/delete/" + $(this).attr("data-code"),
                success: function (data) {
                    Swal.fire("Información eliminada", "", "info");

                    setTimeout(() => {
                        loadingModules(data[1]);
                    }, 1000);
                },
                error: function (error) {
                    errorHttp(error.status);
                },
            });
        } else if (result.isDenied) {
            Swal.fire("Eliminación cancelada", "", "info");
        }
    });
});

function loadingModules(courseId) {
    Swal.close();

    $.ajax({
        type: "GET",
        url: "/app/learning/courses/edit/modules/" + courseId,
        success: function (data) {
            $("#modal-content").html(data);
            Swal.close();
        },
        error: function (error) {
            errorHttp(error.status);
        },
    });
}

/* Fomulario de lección */

