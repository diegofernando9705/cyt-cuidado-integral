/*=========================================================================================
  File Name: table-datatables-basic.js
  Description: jquery datatable js
  ----------------------------------------------------------------------------------------
  Item Name: SOFTWORLD COLOMBIA - DATATABLES
  Author: SOFTWORLD COLOMBIA
  Project: CENTRO NACIONAL DE PRUEBAS
  Author URL: https://softworldcolombia.com/
==========================================================================================*/

$(function () {
    "use strict";

    var tabla_socios = $(".datatables-socios"),
        assetPath = "../../../app-assets/";

    if ($("body").attr("data-framework") === "laravel") {
        assetPath = $("body").attr("data-asset-path");
    }

    /* |||||||||||||||| partnerS |||||||||||||||| */

    if (tabla_socios.length) {
        var dt_basic = tabla_socios.DataTable({
            ajax: "/app/litipoints/partners/listado",
            columns: [
                { data: "vacio" },
                { data: "partner_code" },
                { data: "partner_title" },
                { data: "person_contact_names" },
                { data: "person_contact_last_names" },
                { data: "nombre_estado" },
                { data: "" },
            ],
            columnDefs: [
                {
                    className: "control",
                    orderable: false,
                },
                {
                    // Actions
                    targets: -1,
                    title: "Acciones",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return (
                            ' <button class="btn btn-success ver-partner btn-sm" title="Ver Información" data-id="' +
                            full["partner_code"] +
                            '">' +
                            feather.icons["eye"].toSvg({ class: "font-small-4" }) +
                            "</button> " +
                            ' <button class="btn btn-info editar-partner btn-sm" title="Editar Información" data-id="' +
                            full["partner_code"] +
                            '">' +
                            feather.icons["file-text"].toSvg({ class: "font-small-4" }) +
                            "</button> " +
                            ' <button class="btn btn-danger eliminar-partner btn-sm" title="Eliminar Información" data-id="' +
                            full["partner_code"] +
                            '">' +
                            feather.icons["trash-2"].toSvg({ class: "font-small-4" }) +
                            "</button>" 
                        );
                    },
                },
            ],
            order: [[1, "asc"]],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return "Informacion completa para " + data["primer_nombre"];
                        },
                    }),
                    type: "column",
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                "<td>" +
                                col.title +
                                ":" +
                                "</td> " +
                                "<td>" +
                                col.data +
                                "</td>" +
                                "</tr>"
                                : "";
                        }).join("");

                        return data ? $('<table class="table"/>').append(data) : false;
                    },
                },
            },
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
            },
        });
        $("div.head-label").html('<h6 class="mb-0">DataTable with Buttons</h6>');
    }

    $(document).on("click", ".ver-partner", function () {
        animationLoading("", "Cargando información...");

        $.ajax({
            type: "GET",
            url: "/app/litipoints/partners/show/" + $(this).attr("data-id"),
            success: function (data) {
                $(".dtr-bs-modal").modal("hide");
                $("#modal-partners").modal("show");

                $("#titulo_modal").html("Información");
                $("#contenido_modal").html(data);
                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".editar-partner", function () {
        animationLoading("", "Cargando información...");

        $.ajax({
            type: "GET",
            url: "/app/litipoints/partners/edit/" + $(this).attr("data-id"),
            success: function (data) {
                $(".dtr-bs-modal").modal("hide");
                $("#modal-partners").modal("show");

                $("#titulo_modal").html("Información");
                $("#contenido_modal").html(data);
                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".eliminar-partner", function () {
        $(".modal").modal("hide");
        Swal.fire({
            icon: "question",
            title: "¿Esta seguro que desea eliminarlo?",
            html: "Una vez eliminado, no se podrá recuperar",
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonColor: "#003f48",
            confirmButtonText: "Si, eliminar",
            denyButtonText: `Cancelar`,
            allowOutsideClick: false,
            allowEscapeKey: false,
            focusConfirm: true,
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/app/litipoints/partners/delete/" + $(this).attr("data-id"),
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire(
                                "Eliminado!",
                                "La información ha sido eliminada.",
                                "success"
                            );

                            setTimeout(() => {
                                var opciones = tabla_socios.DataTable();
                                opciones.ajax.reload();
                            }, 2000);
                        }
                    },
                    error: function (err) {
                        errorHttp(err.status);
                    },
                });
            }
        });
    });

    $(document).on("click", ".asignar-membresia-partner", function () {
        animationLoading("", "Cargando información...");

        $.ajax({
            type: "GET",
            url: "/app/litipoints/partners/form/membership/" + $(this).attr("data-id"),
            success: function (data) {
                $(".dtr-bs-modal").modal("hide");
                $("#modal-partners").modal("show");

                $("#titulo_modal").html("Asignación de membresia");
                $("#contenido_modal").html(data);

                Swal.close();
            },
            error: function (error) {
                $(".modal").modal("hide");

                errorHttp(error.status);
            },
        });
    });

    /* |||||||||||||||| FIN partnerS |||||||||||||||| */

});
