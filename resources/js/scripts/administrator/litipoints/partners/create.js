$(document).ready(function () {
    $('.selector').select2();
});


var tabla_servicios = $('.datatables-servicios');
var changePicture = $('#logo_socio'),
    userAvatar = $('.user-avatar');

// Change user profile picture
if (changePicture.length) {
    $(changePicture).on('change', function (e) {
        var reader = new FileReader(),
            files = e.target.files;
        reader.onload = function () {
            if (userAvatar.length) {
                userAvatar.attr('src', reader.result);
            }
        };
        reader.readAsDataURL(files[0]);
    });
};


// Actualizacion de servicio
$(function () {
    'use strict';

    const forms = document.querySelectorAll('.store-socios-premios');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {
                document.getElementById("submit").disabled = true;

                var paqueteDeDatos = new FormData();
                paqueteDeDatos.append('logo_socio', $('#logo_socio')[0].files[0]);

                paqueteDeDatos.append('partner_title', $('#partner_title').prop('value'));
                paqueteDeDatos.append('partner_description', $('#partner_description').prop('value'));
                paqueteDeDatos.append('person_contact_names', $('#person_contact_names').prop('value'));
                paqueteDeDatos.append('person_contact_last_names', $('#person_contact_last_names').prop('value'));
                paqueteDeDatos.append('person_contact_cellphone', $('#person_contact_cellphone').prop('value'));
                paqueteDeDatos.append('person_contact_email', $('#person_contact_email').prop('value'));


                document.getElementById("partner_title").disabled = true;
                document.getElementById("partner_description").disabled = true;
                document.getElementById("person_contact_names").disabled = true;
                document.getElementById("person_contact_last_names").disabled = true;
                document.getElementById("person_contact_cellphone").disabled = true;
                document.getElementById("person_contact_email").disabled = true;
                document.getElementById("submit").disabled = true;


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '/app/litipoints/partners/create/store',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire({
                                title: "Registro exitoso",
                                text: "La información se ha registrado correctamente",
                                icon: "success"
                            });

                            setTimeout(function () {
                                window.location = "/app/litipoints/partners";
                            }, 2000);

                        }
                    },
                    error: function (err) {

                        document.getElementById("partner_title").disabled = false;
                        document.getElementById("partner_description").disabled = false;
                        document.getElementById("person_contact_names").disabled = false;
                        document.getElementById("person_contact_last_names").disabled = false;
                        document.getElementById("person_contact_cellphone").disabled = false;
                        document.getElementById("person_contact_email").disabled = false;
                        document.getElementById("submit").disabled = false;

                        errorHttp(err.status);

                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});