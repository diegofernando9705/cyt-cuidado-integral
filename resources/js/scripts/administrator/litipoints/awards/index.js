/*=========================================================================================
  File Name: table-datatables-basic.js
  Description: jquery datatable js
  ----------------------------------------------------------------------------------------
  Item Name: SOFTWORLD COLOMBIA - DATATABLES
  Author: SOFTWORLD COLOMBIA
  Project: CENTRO NACIONAL DE PRUEBAS
  Author URL: https://softworldcolombia.com/
==========================================================================================*/

$(function () {
    "use strict";

    var tabla_premios = $(".datatables-premios"),
        assetPath = "../../../app-assets/";

    if ($("body").attr("data-framework") === "laravel") {
        assetPath = $("body").attr("data-asset-path");
    }

    /* |||||||||||||||| PREMIOS |||||||||||||||| */

    if (tabla_premios.length) {
        var dt_basic = tabla_premios.DataTable({
            ajax: "/app/litipoints/awards/listado",
            columns: [
                { data: "vacio" },
                { data: "codigo_premio" },
                { data: "titulo_premio" },
                { data: "cantidad_litipuntos" },
                { data: "disponibles_litipuntos" },
                { data: "fecha_inicio_premio" },
                { data: "fecha_fin_premio" },
                { data: "nombre_estado" },
                { data: "" },
            ],
            columnDefs: [
                {
                    className: "control",
                    orderable: false,
                },
                {
                    // Actions
                    targets: -1,
                    title: "Acciones",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        if (full["codigo_estado"] == 1) {
                            return (
                                ' <button class="btn btn-success ver-premio btn-sm" title="Ver Información" data-id="' +
                                full["codigo_premio"] +
                                '">' +
                                feather.icons["eye"].toSvg({ class: "font-small-4" }) +
                                "</button> " +
                                ' <button class="btn btn-info editar-premio btn-sm" title="Editar Información" data-id="' +
                                full["codigo_premio"] +
                                '">' +
                                feather.icons["file-text"].toSvg({ class: "font-small-4" }) +
                                "</button> " +
                                ' <button class="btn btn-danger eliminar-premio btn-sm" title="Eliminar Información" data-id="' +
                                full["codigo_premio"] +
                                '">' +
                                feather.icons["trash-2"].toSvg({ class: "font-small-4" }) +
                                "</button>" +
                                ' <button class="btn btn-primary asignar-membresia-premio btn-sm" title="Asignar a membresia" style="margin-top:5px;" data-id="' +
                                full["codigo_premio"] +
                                '"> Asignar membresia </button>'
                            );
                        } else {
                            return (
                                '<button class="btn btn-success ver-premio btn-sm" title="Ver Información" data-id="' +
                                full["codigo_premio"] +
                                '">' +
                                feather.icons["eye"].toSvg({ class: "font-small-4" }) +
                                "</button>"
                            );
                        }
                    },
                },
            ],
            order: [[1, "asc"]],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return "Informacion completa para " + data["primer_nombre"];
                        },
                    }),
                    type: "column",
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                "<td>" +
                                col.title +
                                ":" +
                                "</td> " +
                                "<td>" +
                                col.data +
                                "</td>" +
                                "</tr>"
                                : "";
                        }).join("");

                        return data ? $('<table class="table"/>').append(data) : false;
                    },
                },
            },
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
            },
        });
        $("div.head-label").html('<h6 class="mb-0">DataTable with Buttons</h6>');
    }

    $(document).on("click", ".ver-premio", function () {
        animationLoading("", "Cargando información...");

        $.ajax({
            type: "GET",
            url: "/app/litipoints/awards/show/" + $(this).attr("data-id"),
            success: function (data) {
                $(".dtr-bs-modal").modal("hide");
                $("#modal-premios").modal("show");

                $("#titulo_modal").html("Información");
                $("#contenido_modal").html(data);
                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".editar-premio", function () {
        animationLoading("", "Cargando información...");

        $.ajax({
            type: "GET",
            url: "/app/litipoints/awards/edit/" + $(this).attr("data-id"),
            success: function (data) {
                $(".dtr-bs-modal").modal("hide");
                $("#modal-premios").modal("show");

                $("#titulo_modal").html("Información");
                $("#contenido_modal").html(data);
                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".eliminar-premio", function () {
        $(".modal").modal("hide");
        Swal.fire({
            icon: "question",
            title: "¿Esta seguro que desea eliminarlo?",
            html: "Una vez eliminado, no se podrá recuperar",
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonColor: "#003f48",
            confirmButtonText: "Si, eliminar",
            denyButtonText: `Cancelar`,
            allowOutsideClick: false,
            allowEscapeKey: false,
            focusConfirm: true,
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/app/litipoints/awards/delete/" + $(this).attr("data-id"),
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire(
                                "Eliminado!",
                                "La información ha sido eliminada.",
                                "success"
                            );

                            setTimeout(() => {
                                var opciones = tabla_premios.DataTable();
                                opciones.ajax.reload();
                            }, 2000);
                        }
                    },
                    error: function (err) {
                        errorHttp(err.status);
                    },
                });
            }
        });
    });

    $(document).on("click", ".asignar-membresia-premio", function () {
        animationLoading("", "Cargando información...");

        $.ajax({
            type: "GET",
            url: "/app/litipoints/awards/form/membership/" + $(this).attr("data-id"),
            success: function (data) {
                $(".dtr-bs-modal").modal("hide");
                $("#modal-premios").modal("show");
               
                $("#titulo_modal").html("Asignación de membresia");
                $("#contenido_modal").html(data);

                Swal.close();
            },
            error: function (error) {
                $(".modal").modal("hide");
                
                errorHttp(error.status);
            },
        });
    });

    /* |||||||||||||||| FIN PREMIOS |||||||||||||||| */

});
