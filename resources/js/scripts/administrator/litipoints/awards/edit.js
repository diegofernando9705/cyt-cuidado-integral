$(document).ready(function() {
    $('.selector').select2();
});

var changePicture = $('#imagen_premio'),
    userAvatar = $('.user-avatar');

// Change user profile picture
if (changePicture.length) {
    $(changePicture).on('change', function(e) {
        var reader = new FileReader(),
            files = e.target.files;
        reader.onload = function() {
            if (userAvatar.length) {
                userAvatar.attr('src', reader.result);
            }
        };
        reader.readAsDataURL(files[0]);
    });
};

tabla_premios = $('.datatables-premios');


$(function() {
    'use strict';

    const forms = document.querySelectorAll('.update-premios');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {
                var paqueteDeDatos = new FormData();

                paqueteDeDatos.append('imagen_premio', $('#imagen_premio')[0].files[0]);


                paqueteDeDatos.append('start_date', $('#start_date').prop("value"));
                paqueteDeDatos.append('end_date', $('#end_date').prop('value'));
                paqueteDeDatos.append('award_title', $('#award_title').prop('value'));
                paqueteDeDatos.append('award_description', $('#award_description').prop('value'));
                paqueteDeDatos.append('litipoint_quantity', $('#litipoint_quantity').prop('value'));
                paqueteDeDatos.append('availability', $('#availability').prop('value'));
                paqueteDeDatos.append('partner', $('#partner').val());
                paqueteDeDatos.append('status', $('#status').prop('value'));



                document.getElementById("imagen_premio").disabled = true;
                document.getElementById("start_date").disabled = true;
                document.getElementById("end_date").disabled = true;
                document.getElementById("award_title").disabled = true;
                document.getElementById("award_description").disabled = true;
                document.getElementById("litipoint_quantity").disabled = true;
                document.getElementById("availability").disabled = true;
                document.getElementById("partner").disabled = true;
                document.getElementById("status").disabled = true;
                document.getElementById("submit").disabled = true;



                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: "/app/litipoints/awards/edit/update/"+$("#award_code").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data == true) {
                            Swal.fire({
                                icon: "success",
                                title: "Actualización exitosa",
                                text: "La información se ha actualizado correctamente.",
                            });


                            setTimeout(function() {
                                $("#modal-premios").modal('hide');
                                var opciones = tabla_premios.DataTable();
                                opciones.ajax.reload();
                            }, 2000);

                        }
                    },
                    error: function(err) {
                        document.getElementById("imagen_premio").disabled = false;
                        document.getElementById("end_date").disabled = false;
                        document.getElementById("award_title").disabled = false;
                        document.getElementById("award_description").disabled = false;
                        document.getElementById("litipoint_quantity").disabled = false;
                        document.getElementById("availability").disabled = false;
                        document.getElementById("partner").disabled = false;
                        document.getElementById("status").disabled = false;
                        document.getElementById("submit").disabled = false;

                        errorHttp(err.status);

                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function(i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});