/*=========================================================================================
  File Name: table-datatables-basic.js
  Description: jquery datatable js
  ----------------------------------------------------------------------------------------
  Item Name: SOFTWORLD COLOMBIA - DATATABLES
  Author: SOFTWORLD COLOMBIA
  Project: CENTRO NACIONAL DE PRUEBAS
  Author URL: https://softworldcolombia.com/
==========================================================================================*/

$(function () {
    "use strict";

    var tabla_conceptos = $(".datatables-conceptos"),
        assetPath = "../../../app-assets/";

    if ($("body").attr("data-framework") === "laravel") {
        assetPath = $("body").attr("data-asset-path");
    }


    /* |||||||||||||||| CONCEPTOS |||||||||||||||| */

    if (tabla_conceptos.length) {
        var dt_basic = tabla_conceptos.DataTable({
            ajax: "/app/administrative/concepts/listado",
            columns: [
                { data: "vacio" },
                { data: "codigo_concepto" },
                { data: "titulo_concepto" },
                { data: "fecha_registro" },
                { data: "fecha_caducidad" },
                { data: "usuario_creador_concepto" },
                { data: "nombre_estado" },
                { data: "" },
            ],
            columnDefs: [
                {
                    className: "control",
                    orderable: false,
                },
                {
                    // Actions
                    targets: -1,
                    title: "Acciones",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        if (full["codigo_estado"] == 1) {
                            return (
                                ' <button class="btn btn-success ver-concepto btn-sm" title="Ver noticia" data-id="' +
                                full["codigo_concepto"] +
                                '">' +
                                feather.icons["eye"].toSvg({ class: "font-small-4" }) +
                                "</button> " +
                                ' <button class="btn btn-danger eliminar-concepto btn-sm" title="Eliminar Información" data-id="' +
                                full["codigo_concepto"] +
                                '">' +
                                feather.icons["trash-2"].toSvg({ class: "font-small-4" }) +
                                "</button>" +
                                ' <a href="/app/administrative/concepts/pdf/' +
                                full["codigo_concepto"] +
                                '" class="btn btn-danger btn-sm" title="Descargar PDF"> PDF </a>' +
                                ' <button class="btn btn-primary asignar-membresia-conceptos btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top:5px;" title="Asignar a membresia" data-id="' +
                                full["codigo_concepto"] +
                                '"> Asignar membresia </button>'
                            );
                        } else {
                            return (
                                '<button class="btn btn-success ver-concepto btn-sm" title="Ver Información" disabled data-id="' +
                                full["codigo_concepto"] +
                                '">' +
                                feather.icons["eye"].toSvg({ class: "font-small-4" }) +
                                "</button>"
                            );
                        }
                    },
                },
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return "Informacion completa para " + data["titulo_concepto"];
                        },
                    }),
                    type: "column",
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                "<td>" +
                                col.title +
                                ":" +
                                "</td> " +
                                "<td>" +
                                col.data +
                                "</td>" +
                                "</tr>"
                                : "";
                        }).join("");

                        return data ? $('<table class="table"/>').append(data) : false;
                    },
                },
            },
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
            },
        });
        $("div.head-label").html('<h6 class="mb-0">DataTable with Buttons</h6>');
    }

    $(document).on("click", ".ver-concepto", function () {
        animationLoading('', ' Cargando información...');

        $.ajax({
            type: "GET",
            url: "/app/administrative/concepts/show/" + $(this).attr("data-id"),
            success: function (data) {
                $("#modal-conceptos").modal("show");
                $("#titulo_modal").html("Ver Información");
                $("#contenido_modal").html(data);
                CKEDITOR.replace("descripcion_respuesta");
                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".eliminar-concepto", function () {
        Swal.fire({
            icon: "question",
            title: "¿Esta seguro que desea eliminarlo?",
            html: "Una vez eliminado, no se podrá recuperar",
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonColor: "#003f48",
            confirmButtonText: "Si, eliminar",
            denyButtonText: `Cancelar`,
            allowOutsideClick: false,
            allowEscapeKey: false,
            focusConfirm: true,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/app/administrative/concepts/delete/" + $(this).attr("data-id"),
                    success: function (data) {
                        deleteSuccess();
                        setTimeout(function () {
                            var tabla = tabla_conceptos.DataTable();
                            tabla.ajax.reload();
                        }, 1500);
                    },
                    error: function (error) {
                        errorHttp(error.status);
                    },
                });
            } else if (result.isDenied) {
                Swal.fire("Eliminación cancelada", "", "info");
            }
        });
    });

    $(document).on("click", ".asignar-membresia-conceptos", function () {
        animationLoading('', ' Cargando información...');

        $.ajax({
            type: "GET",
            url:"/app/administrative/concepts/form/membership/" + $(this).attr("data-id"),
            success: function (data) {
                $("#modal-conceptos").modal("show");
                $("#titulo_modal").html("Asignación de membresia");
                $("#contenido_modal").html(data);
                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    /* |||||||||||||||| FIN CONCEPTOS |||||||||||||||| */

});
