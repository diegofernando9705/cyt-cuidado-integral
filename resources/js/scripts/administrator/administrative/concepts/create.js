$(document).ready(function () {
    $('.selector').select2();
});


var tabla_servicios = $('.datatables-servicios');


$(document).on("change", "#privacy", function () {
    var application_concept = document.getElementById('application_concept');

    if ($(this).prop("value") == "public") {
        $("#div_profesionales").attr("style", "display:none;");
        application_concept.removeAttribute('required');
    } else {
        $("#div_profesionales").attr("style", "display:block;");
        application_concept.setAttribute('required', '');
    }
});

CKEDITOR.replace('description');


// Actualizacion de servicio
$(function () {
    'use strict';

    const forms = document.querySelectorAll('.store-conceptos');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {
                document.getElementById("title").disabled = true;
                document.getElementById("description").disabled = true;
                document.getElementById("register_date").disabled = true;
                document.getElementById("end_date").disabled = true;
                document.getElementById("privacy").disabled = true;
                document.getElementById("application_concept").disabled = true;
                document.getElementById("archivos").disabled = true;
                document.getElementById("submit").disabled = true;

                var paqueteDeDatos = new FormData();

                var inpFiles = document.getElementById("archivos");
                for (const file of inpFiles.files) {
                    paqueteDeDatos.append("archivos[]", file);
                }

                paqueteDeDatos.append('title', $("#title").prop('value'));
                paqueteDeDatos.append('description', CKEDITOR.instances["description"].getData());
                paqueteDeDatos.append('register_date', $("#register_date").prop('value'));
                paqueteDeDatos.append('end_date', $("#end_date").val());
                paqueteDeDatos.append('privacy', $("#privacy").prop('value'));
                paqueteDeDatos.append('application_concept', $("#application_concept").prop('value'));

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '/app/administrative/concepts/create/store',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data == true) {
                            storeSuccess();

                            setTimeout(function () {
                                window.location = "/app/administrative/concepts";
                            }, 1800);
                        }
                    },
                    error: function (err) {
                        document.getElementById("title").disabled = false;
                        document.getElementById("description").disabled = false;
                        document.getElementById("register_date").disabled = false;
                        document.getElementById("end_date").disabled = false;
                        document.getElementById("privacy").disabled = false;
                        document.getElementById("application_concept").disabled = false;
                        document.getElementById("archivos").disabled = false;
                        document.getElementById("submit").disabled = false;

                        errorHttp(err.status);

                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});