$(function () {
    'use strict';

    var tabla_human_talent = $('.datatables-human-talent'),
        assetPath = '../../../app-assets/';


    if ($('body').attr('data-framework') === 'laravel') {
        assetPath = $('body').attr('data-asset-path');
    }

    if (tabla_human_talent.length) {
        var dt_basic = tabla_human_talent.DataTable({
            ajax: '/app/administrative/people/listado',
            columns: [
                { data: 'vacio' },
                { data: 'id' },
                { data: 'nombres' },
                { data: 'apellidos' },
                { data: 'email' },
                { data: 'type' },
                { data: 'nombre_estado' },
                { data: '' }
            ],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0
                },
                {
                    responsivePriority: 1,
                    targets: 4
                },
                {
                    targets: -1,
                    title: 'Acciones',
                    orderable: false,
                    render: function (data, type, full, meta) {

                        return (
                            '<button class="btn btn-success show-people btn-sm" title="Ver información" data-id="' + full['id'] + '">'
                            + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
                            '</button>'
                            +
                            ' <button class="btn btn-info edit-people btn-sm" title="Editar información" data-id="' + full['id'] + '">'
                            + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) +
                            '</button>'
                            +
                            ' <button class="btn btn-danger delete-people btn-sm" title="Eliminar información" data-id="' + full['id'] + '">'
                            + feather.icons['trash-2'].toSvg({ class: 'font-small-4' }) +
                            '</button>'

                        );
                    }
                }
            ],
            order: [[1, 'asc']],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Detalles del usuario ' + data['name'];
                        }
                    }),
                    type: 'column',
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {

                            return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                '<td>' +
                                col.title +
                                ':' +
                                '</td> ' +
                                '<td>' +
                                col.data +
                                '</td>' +
                                '</tr>'
                                : '';
                        }).join('');

                        return data ? $('<table class="table"/>').append(data) : false;
                    }
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">DataTable with Buttons</h6>');
    }

    $(document).on("click", ".show-people", function () {
        animationLoading('', ' Cargando información...');

        $.ajax({
            type: 'GET',
            url: '/app/administrative/people/show/' + $(this).attr("data-id"),
            success: function (data) {
                Swal.close();
                $("#modal").modal("show");
                $("#content-modal").html(data);
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".edit-people", function () {
        animationLoading('', ' Cargando información...');
        
        $.ajax({
            type: 'GET',
            url: '/app/administrative/people/edit/' + $(this).attr("data-id"),
            success: function (data) {
                Swal.close();
                $("#modal").modal("show");
                $("#content-modal").html(data);
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".delete-people", function () {
        Swal.fire({
            icon: "question",
            title: "¿Esta seguro que desea eliminarlo?",
            html: "Una vez eliminado, no se podrá recuperar",
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonColor: "#003f48",
            confirmButtonText: "Si, eliminar",
            denyButtonText: `Cancelar`,
            allowOutsideClick: false,
            allowEscapeKey: false,
            focusConfirm: true,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/app/administrative/people/delete/" + $(this).attr("data-id"),
                    success: function (data) {
                        Swal.fire("Información eliminada", "", "info");
                        setTimeout(function () {
                            var tabla = tabla_human_talent.DataTable();
                            tabla.ajax.reload();
                        }, 1000);
                    },
                    error: function (error) {
                        errorHttp(error.status);
                    },
                });
            } else if (result.isDenied) {
                Swal.fire("Eliminación cancelada", "", "info");
            }
        });
    });


});