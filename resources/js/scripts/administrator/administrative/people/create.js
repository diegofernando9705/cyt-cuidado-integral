$(function () {
    "use strict";

    var changePicture = $('#user-avatar'),
        userAvatar = $('.user-avatar');

    // Change user profile picture
    if (changePicture.length) {
        $(changePicture).on('change', function (e) {
            var reader = new FileReader(),
                files = e.target.files;
            reader.onload = function () {
                if (userAvatar.length) {
                    userAvatar.attr('src', reader.result);
                }
            };
            reader.readAsDataURL(files[0]);
        });
    }

    $('.selector').select2();

    const forms = document.querySelectorAll(".store-human-talent");

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener(
            "submit",
            (event) => {
                if (!form.checkValidity()) {
                    event.stopPropagation();
                } else {
                    var paqueteDeDatos = new FormData();

                    paqueteDeDatos.append('user_avatar', $('#user-avatar')[0].files[0]);
                    paqueteDeDatos.append('tarjeta_profesional', $('#tarjeta_profesional').prop('value'));
                    paqueteDeDatos.append('tipo_de_documento_profesional', $('#tipo_de_documento_profesional').prop('value'));
                    paqueteDeDatos.append('identificacion_profesional', $('#identificacion_profesional').prop('value'));
                    paqueteDeDatos.append('primer_nombre_profesional', $('#primer_nombre_profesional').prop('value'));
                    paqueteDeDatos.append('segundo_nombre_profesional', $('#segundo_nombre_profesional').prop('value'));
                    paqueteDeDatos.append('primer_apellido_profesional', $('#primer_apellido_profesional').prop('value'));
                    paqueteDeDatos.append('segundo_apellido_profesional', $('#segundo_apellido_profesional').prop('value'));
                    paqueteDeDatos.append('correo_profesional', $('#correo_profesional').prop('value'));
                    paqueteDeDatos.append('celular_profesional', $('#celular_profesional').prop('value'));
                    paqueteDeDatos.append('type', $('#type').prop('value'));



                    document.getElementById("tarjeta_profesional").disabled = true;
                    document.getElementById("tipo_de_documento_profesional").disabled = true;
                    document.getElementById("identificacion_profesional").disabled = true;
                    document.getElementById("primer_nombre_profesional").disabled = true;
                    document.getElementById("segundo_nombre_profesional").disabled = true;
                    document.getElementById("primer_apellido_profesional").disabled = true;
                    document.getElementById("segundo_apellido_profesional").disabled = true;
                    document.getElementById("correo_profesional").disabled = true;
                    document.getElementById("celular_profesional").disabled = true;
                    document.getElementById("type").disabled = true;

                    document.getElementById("submit").disabled = true;

                    $.ajax({
                        type: "POST",
                        url: "/app/administrative/people/create/store",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: paqueteDeDatos,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            Swal.fire({
                                icon: "success",
                                title: "Registro exitosa",
                                text: "La información se ha registrado correctamente.",
                            });

                            if (data == true) {
                                setTimeout(function () {
                                    Swal.close();
                                    window.location = "/app/administrative/people";
                                }, 1000);
                            }
                        },
                        error: function (err) {
                            document.getElementById("tarjeta_profesional").disabled = false;
                            document.getElementById("tipo_de_documento_profesional").disabled = false;
                            document.getElementById("identificacion_profesional").disabled = false;
                            document.getElementById("primer_nombre_profesional").disabled = false;
                            document.getElementById("segundo_nombre_profesional").disabled = false;
                            document.getElementById("primer_apellido_profesional").disabled = false;
                            document.getElementById("segundo_apellido_profesional").disabled = false;
                            document.getElementById("correo_profesional").disabled = false;
                            document.getElementById("celular_profesional").disabled = false;
                            document.getElementById("type").disabled = false;

                            document.getElementById("submit").disabled = false;
                            errorHttp(err.status);
                            if (err.status == 422) {

                                $(".errores").hide();
                                console.log(err.responseJSON);
                                $("#success_message").fadeIn().html(err.responseJSON.message);
                                console.warn(err.responseJSON.errors);
                                $.each(err.responseJSON.errors, function (i, error) {
                                    var el = $(document).find('[name="' + i + '"]');
                                    el.after(
                                        $(
                                            '<span class="errores" style="color: red;">' +
                                            error[0] +
                                            "</span>"
                                        )
                                    );
                                });
                            }
                        },
                    });
                    event.preventDefault();
                }
                event.preventDefault();
                form.classList.add("was-validated");
            },
            false
        );
    });
});