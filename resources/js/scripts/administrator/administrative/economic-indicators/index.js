$(function () {
  "use strict";

  var datatables_indicadores_economicos = $(
      ".datatables-indicadores-economicos"
    ),
    assetPath = "../../../app-assets/";

  if ($("body").attr("data-framework") === "laravel") {
    assetPath = $("body").attr("data-asset-path");
  }

  /* |||||||||||||||| TABLA INDICADORES ECONOMICOS |||||||||||||||| */
  if (datatables_indicadores_economicos.length) {
    var dt_basic = datatables_indicadores_economicos.DataTable({
      ajax: "/app/administrative/economic-indicator/listado",
      columns: [
        { data: "vacio" },
        { data: "fecha_indicador_inicio" },
        { data: "fecha_indicador_fin" },
        { data: "valor_indicador" },
        { data: "porcentaje" },
        { data: "descripcion_indicador" },
        { data: "" },
      ],
      columnDefs: [
        {
          className: "control",
          orderable: false,
        },
        {
          // Actions
          targets: -1,
          title: "Acciones",
          orderable: false,
          render: function (data, type, full, meta) {
            return (
              '<button class="btn btn-danger eliminar-indicador-economico btn-sm" title="Eliminar indicador económico" data-id="' +
              full["id"] +
              '">' +
              feather.icons["trash-2"].toSvg({ class: "font-small-4" }) +
              "</button>"
            );
          },
        },
      ],
      order: [],
      responsive: {
        details: {
          display: $.fn.dataTable.Responsive.display.modal({
            header: function (row) {
              var data = row.data();
              return "Informacion completa para " + data["primer_nombre"];
            },
          }),
          type: "column",
          renderer: function (api, rowIdx, columns) {
            var data = $.map(columns, function (col, i) {
              return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                ? '<tr data-dt-row="' +
                    col.rowIndex +
                    '" data-dt-column="' +
                    col.columnIndex +
                    '">' +
                    "<td>" +
                    col.title +
                    ":" +
                    "</td> " +
                    "<td>" +
                    col.data +
                    "</td>" +
                    "</tr>"
                : "";
            }).join("");

            return data ? $('<table class="table"/>').append(data) : false;
          },
        },
      },
      language: {
        url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
      },
    });
    $("div.head-label").html('<h6 class="mb-0">DataTable with Buttons</h6>');
  }

  $(document).on("click", ".eliminar-indicador-economico", function () {
    $(".modal").modal("hide");
    Swal.fire({
      title: "¿Desea eliminar el registro?",
      text: "No podrás revertir esto",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, eliminar!",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          type: "GET",
          url:
            "/app/administrative/economic-indicator/delete/" +
            $(this).attr("data-id"),
          processData: false,
          contentType: false,
          success: function (data) {
            if (data == true) {
              Swal.fire(
                "Eliminado!",
                "La información ha sido eliminada.",
                "success"
              );

              setTimeout(() => {
                var opciones = datatables_indicadores_economicos.DataTable();
                opciones.ajax.reload();
              }, 2000);
            }
          },
          error: function (err) {
            if (err.status == 422) {
              $(".errores").hide();
              console.log(err.responseJSON);
              $("#success_message").fadeIn().html(err.responseJSON.message);
              console.warn(err.responseJSON.errors);
              $.each(err.responseJSON.errors, function (i, error) {
                var el = $(document).find('[name="' + i + '"]');
                el.after(
                  $(
                    '<span class="errores" style="color: red;">' +
                      error[0] +
                      "</span>"
                  )
                );
              });
            } else {
              $("#modal-services").modal("hide");
              if (err.status == 403) {
                Swal.fire({
                  allowOutsideClick: false,
                  icon: "error",
                  title: "Acción restringida",
                  html:
                    "Usted no tiene permisos para esta acción. Comuniquese con administración. <br><br> <b>CÓD. ERROR: " +
                    err.status +
                    "</b>",
                  footer:
                    "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>",
                });
              } else {
                Swal.fire({
                  allowOutsideClick: false,
                  icon: "error",
                  title: "Error desconocido",
                  html:
                    "Comunicate con soporte. <br> <b>CÓD. ERROR: " +
                    err.status +
                    "</b>",
                  footer:
                    "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>",
                });
              }
            }
          },
        });
      }
    });
  });
  /* |||||||||||||||| FIN TABLA INDICADORES ECONOMICOS |||||||||||||||| */

  $(document).on("click", "#actualizar_indicadores_excel", function () {
    $(".modal").modal("show");
    $("#titulo_modal").html("Actualización de indicadores");

    $.ajax({
      type: "GET",
      url: "/app/administrative/economic-indicator/update/formulario",
      processData: false,
      contentType: false,
      success: function (data) {
        $("#contenido_modal").html(data);
      },
      error: function (err) {
        if (err.status == 422) {
          $(".errores").hide();
          console.log(err.responseJSON);
          $("#success_message").fadeIn().html(err.responseJSON.message);
          console.warn(err.responseJSON.errors);
          $.each(err.responseJSON.errors, function (i, error) {
            var el = $(document).find('[name="' + i + '"]');
            el.after(
              $(
                '<span class="errores" style="color: red;">' +
                  error[0] +
                  "</span>"
              )
            );
          });
        } else {
          $("#modal-services").modal("hide");
          if (err.status == 403) {
            Swal.fire({
              allowOutsideClick: false,
              icon: "error",
              title: "Acción restringida",
              html:
                "Usted no tiene permisos para esta acción. Comuniquese con administración. <br><br> <b>CÓD. ERROR: " +
                err.status +
                "</b>",
              footer:
                "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>",
            });
          } else {
            Swal.fire({
              allowOutsideClick: false,
              icon: "error",
              title: "Error desconocido",
              html:
                "Comunicate con soporte. <br> <b>CÓD. ERROR: " +
                err.status +
                "</b>",
              footer:
                "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>",
            });
          }
        }
      },
    });
  });
});
