$('.selector').select2();

/* |||||||||||||||| FORMULARIO REGISTRO INDICADORES ECONOMICOS |||||||||||||||| */
$(function () {
  "use strict";

  const forms = document.querySelectorAll(".store-indicador-economico");

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms).forEach((form) => {
    form.addEventListener(
      "submit",
      (event) => {
        if (!form.checkValidity()) {
          event.stopPropagation();
        } else {
          var formulario = $(".store-indicador-economico").serialize();

          document.getElementById("date_start_indicator").disabled = true;
          document.getElementById("date_end_indicator").disabled = true;
          document.getElementById("value_start_indicator").disabled = true;
          document.getElementById("percentage_start_indicator").disabled = true;
          document.getElementById("description_start_indicator").disabled = true;

          document.getElementById("submit").disabled = true;

          $.ajax({
            type: "POST",
            url: "/app/administrative/economic-indicator/create/store",
            data: formulario, // serializes the form's elements.
            success: function (data) {

              $("#registro-exitoso").toast("show");

              setTimeout(function () {
                window.location = "/app/administrative/economic-indicator";
              }, 1000);

            },
            error: function (err) {
              document.getElementById("date_start_indicator").disabled = false;
              document.getElementById("date_end_indicator").disabled = false;
              document.getElementById("value_start_indicator").disabled = false;
              document.getElementById("percentage_start_indicator").disabled = false;
              document.getElementById("description_start_indicator").disabled = false;

              document.getElementById("submit").disabled = false;

              if (err.status == 403) {
                Swal.fire({
                  title: "<strong>ACCIÓN RESTRINGIDA</strong>",
                  icon: "error",
                  html:
                    "Usted no tiene permisos para esta acción. Comuniquese con soporte. <br>" +
                    "<b>CÓD. ERROR: " +
                    err.status +
                    "</b> <br>" +
                    "<small>" +
                    err.statusText +
                    "</small> <br>",
                  showCloseButton: false,
                  showCancelButton: false,
                  showConfirmButton: false,
                  focusConfirm: false,
                });
              } else {
                Swal.fire({
                  title: "<strong>ERROR</strong>",
                  icon: "error",
                  html:
                    "Ocurrio un error en el proceso. <br>" +
                    "<b>CÓD. ERROR: " +
                    err.status +
                    "</b> <br>" +
                    "<small>" +
                    err.statusText +
                    "</small> <br>",
                  showCloseButton: false,
                  showCancelButton: false,
                  showConfirmButton: false,
                  focusConfirm: false,
                });

                $(".errores").hide();
                console.log(err.responseJSON);
                $("#success_message").fadeIn().html(err.responseJSON.message);
                console.warn(err.responseJSON.errors);
                $.each(err.responseJSON.errors, function (i, error) {
                  var el = $(document).find('[name="' + i + '"]');
                  el.after(
                    $(
                      '<span class="errores" style="color: red;">' +
                      error[0] +
                      "</span>"
                    )
                  );
                });
              }
            },
          });
          event.preventDefault();
        }
        event.preventDefault();
        form.classList.add("was-validated");
      },
      false
    );
  });
});
/* |||||||||||||||| FIN FORMULARIO REGISTRO INDICADORES ECONOMICOS |||||||||||||||| */

/* |||||||||||||||| FORMULARIO REGISTRO INDICADORES ECONOMICOS DESDE EXCEL |||||||||||||||| */
$(function () {
  "use strict";

  const forms = document.querySelectorAll(".formulario_importacion");

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms).forEach((form) => {
    form.addEventListener(
      "submit",
      (event) => {
        if (!form.checkValidity()) {
          event.stopPropagation();
        } else {
          var paqueteDeDatos = new FormData();

          paqueteDeDatos.append("file", $("#file")[0].files[0]);
          paqueteDeDatos.append(
            "indicador_economico",
            $("#indicador_economico").prop("value")
          );

          document.getElementById("file").disabled = true;
          document.getElementById("indicador_economico").disabled = true;

          document.getElementById("submit").disabled = true;

          var timerInterval;
          Swal.fire({
            title: "Importando información",
            html: "Esta ventana se cerrará automáticamente al terminar el proceso.",
            allowOutsideClick: false,
            allowEscapeKey: false,
            timer: 10000,
            timerProgressBar: true,
            didOpen: () => {
              Swal.showLoading();
              const b = Swal.getHtmlContainer().querySelector("b");
              timerInterval = setInterval(() => {
                b.textContent = Swal.getTimerLeft();
              }, 100);
            },
            willClose: () => {
              clearInterval(timerInterval);
            },
          }).then((result) => { });

          $.ajax({
            type: "POST",
            contentType: false,
            url: "/app/administrative/economic-indicator/update/formulario/data",
            headers: {
              "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content"),
            },
            data: paqueteDeDatos,
            processData: false,
            cache: false,
            success: function (data) {
              if (data == true) {
                setTimeout(() => {
                  document.getElementById("file").disabled = false;
                  document.getElementById(
                    "indicador_economico"
                  ).disabled = false;
                  document.getElementById("submit").disabled = false;

                  Swal.close();
                }, 3000);

                $(".modal").modal("hide");

                Swal.fire(
                  "Importación exitosa",
                  "La información se ha actualizado correctamente",
                  "success"
                );

                setTimeout(function () {
                  location.reload();
                }, 1000);
              }
            },
            error: function (err) {
              document.getElementById("file").disabled = false;
              document.getElementById("indicador_economico").disabled = false;

              document.getElementById("submit").disabled = false;
              if (err.status == 403) {
                Swal.fire({
                  title: "<strong>ACCIÓN RESTRINGIDA</strong>",
                  icon: "error",
                  html:
                    "Usted no tiene permisos para esta acción. Comuniquese con soporte. <br>" +
                    "<b>CÓD. ERROR: " +
                    err.status +
                    "</b> <br>" +
                    "<small>" +
                    err.statusText +
                    "</small> <br>",
                  showCloseButton: false,
                  showCancelButton: false,
                  showConfirmButton: false,
                  focusConfirm: false,
                });
              } else {
                Swal.fire({
                  title: "<strong>ERROR</strong>",
                  icon: "error",
                  html:
                    "Ocurrio un error en el proceso. <br>" +
                    "<b>CÓD. ERROR: " +
                    err.status +
                    "</b> <br>" +
                    "<small>" +
                    err.statusText +
                    "</small> <br>",
                  showCloseButton: false,
                  showCancelButton: false,
                  showConfirmButton: false,
                  focusConfirm: false,
                });

                $(".errores").hide();
                console.log(err.responseJSON);
                $("#success_message").fadeIn().html(err.responseJSON.message);
                console.warn(err.responseJSON.errors);
                $.each(err.responseJSON.errors, function (i, error) {
                  var el = $(document).find('[name="' + i + '"]');
                  el.after(
                    $(
                      '<span class="errores" style="color: red;">' +
                      error[0] +
                      "</span>"
                    )
                  );
                });
              }
            },
          });
          event.preventDefault();
        }
        event.preventDefault();
        form.classList.add("was-validated");
      },
      false
    );
  });
});
/* |||||||||||||||| FIN FORMULARIO REGISTRO INDICADORES ECONOMICOS |||||||||||||||| */
