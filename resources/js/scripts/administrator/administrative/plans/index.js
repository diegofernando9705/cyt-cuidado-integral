/*=========================================================================================
  File Name: table-datatables-basic.js
  Description: jquery datatable js
  ----------------------------------------------------------------------------------------
  Item Name: SOFTWORLD COLOMBIA - DATATABLES
  Author: SOFTWORLD COLOMBIA
  Project: CENTRO NACIONAL DE PRUEBAS
  Author URL: https://softworldcolombia.com/
==========================================================================================*/

$(function () {
    "use strict";

    var tabla_planes = $(".datatables-planes"),
        assetPath = "../../../app-assets/";

    if ($("body").attr("data-framework") === "laravel") {
        assetPath = $("body").attr("data-asset-path");
    }

    /* |||||||||||||||| PLANES |||||||||||||||| */

    if (tabla_planes.length) {
        var dt_basic = tabla_planes.DataTable({
            ajax: "/app/administrative/plans/listado",
            columns: [
                { data: "vacio" },
                { data: "codigo_plan" },
                { data: "nombre_plan" },
                { data: "fecha_inicio" },
                { data: "fecha_fin" },
                { data: "valor_del_cobro" },
                { data: "nombre_estado" },
                { data: "" },
            ],
            columnDefs: [
                {
                    className: "control",
                    orderable: false,
                },
                {
                    // Actions
                    targets: -1,
                    title: "Acciones",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        if (full["codigo_estado"] == 1) {
                            return (
                                ' <button class="btn btn-success ver-plan btn-sm" title="Ver noticia" data-id="' +
                                full["codigo_plan"] +
                                '">' +
                                feather.icons["eye"].toSvg({ class: "font-small-4" }) +
                                "</button> " +
                                ' <button class="btn btn-info editar-plan btn-sm" title="Eliminar Información" data-id="' +
                                full["codigo_plan"] +
                                '">' +
                                feather.icons["edit"].toSvg({ class: "font-small-4" }) +
                                "</button>" +
                                ' <button class="btn btn-danger eliminar-plan btn-sm" title="Eliminar Información" data-id="' +
                                full["codigo_plan"] +
                                '">' +
                                feather.icons["trash-2"].toSvg({ class: "font-small-4" }) +
                                "</button>"
                            );
                        } else {
                            return (
                                '<button class="btn btn-success ver-plan btn-sm" title="Ver Información" disabled data-id="' +
                                full["codigo_plan"] +
                                '">' +
                                feather.icons["eye"].toSvg({ class: "font-small-4" }) +
                                "</button>"
                            );
                        }
                    },
                },
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return "Informacion completa para " + data["titulo_membresia"];
                        },
                    }),
                    type: "column",
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                "<td>" +
                                col.title +
                                ":" +
                                "</td> " +
                                "<td>" +
                                col.data +
                                "</td>" +
                                "</tr>"
                                : "";
                        }).join("");

                        return data ? $('<table class="table"/>').append(data) : false;
                    },
                },
            },
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
            },
        });
        $("div.head-label").html('<h6 class="mb-0">DataTable with Buttons</h6>');
    }

    $(document).on("click", ".ver-plan", function () {
        animationLoading('', ' Cargando información...');

        $.ajax({
            type: "GET",
            url: "/app/administrative/plans/show/" + $(this).attr("data-id"),
            success: function (data) {
                $("#modal-planes").modal("show");
                $("#titulo_modal").html("Ver Información");
                $("#contenido_modal").html(data);
                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".editar-plan", function () {
        animationLoading('', ' Cargando información...');

        $.ajax({
            type: "GET",
            url: "/app/administrative/plans/edit/" + $(this).attr("data-id"),
            success: function (data) {
                $("#modal-planes").modal("show");
                $("#titulo_modal").html("Editar Información");
                $("#contenido_modal").html(data);
                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".eliminar-plan", function () {
        Swal.fire({
            icon: "question",
            title: "¿Esta seguro que desea eliminarlo?",
            html: "Una vez eliminado, no se podrá recuperar",
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonColor: "#003f48",
            confirmButtonText: "Si, eliminar",
            denyButtonText: `Cancelar`,
            allowOutsideClick: false,
            allowEscapeKey: false,
            focusConfirm: true,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/app/administrative/plans/delete/" + $(this).attr("data-id"),
                    success: function (data) {
                        Swal.fire("Información eliminada", "", "info");
                        setTimeout(function () {
                            var tabla = tabla_planes.DataTable();
                            tabla.ajax.reload();
                        }, 1000);
                    },
                    error: function (error) {
                        errorHttp(error.status);
                    },
                });
            } else if (result.isDenied) {
                Swal.fire("Eliminación cancelada", "", "info");
            }
        });
    });

    /* |||||||||||||||| FIN PLANES |||||||||||||||| */

});
