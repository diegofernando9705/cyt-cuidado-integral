$(document).ready(function () {
    $('.selector').select2();
});


var changePicture = $('#image'),
    userAvatar = $('.user-avatar');

// Change user profile picture
if (changePicture.length) {
    $(changePicture).on('change', function (e) {
        var reader = new FileReader(),
            files = e.target.files;
        reader.onload = function () {
            if (userAvatar.length) {
                userAvatar.attr('src', reader.result);
            }
        };
        reader.readAsDataURL(files[0]);
    });
};

var tabla_planes = $('.datatables-planes');

// Actualizacion de servicio
$(function () {
    'use strict';

    const forms = document.querySelectorAll('.update-planes');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {
                document.getElementById("submit").disabled = true;

                document.getElementById("start_date").disabled = true;
                document.getElementById("end_date").disabled = true;
                document.getElementById("title").disabled = true;
                document.getElementById("description").disabled = true;
                document.getElementById("price").disabled = true;
                document.getElementById("membresias").disabled = true;
                document.getElementById("status").disabled = true;

                var paqueteDeDatos = new FormData();
                paqueteDeDatos.append('image', $('#image')[0].files[0]);

                paqueteDeDatos.append('code', $('#code').val());
                paqueteDeDatos.append('start_date', $('#start_date').val());
                paqueteDeDatos.append('end_date', $('#end_date').val());
                paqueteDeDatos.append('title', $('#title').val());
                paqueteDeDatos.append('description', $('#description').val());
                paqueteDeDatos.append('price', $('#price').val());
                paqueteDeDatos.append('membresias', $('#membresias').val());
                paqueteDeDatos.append('status', $('#status').val());

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: "/app/administrative/plans/edit/update",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        Swal.fire({
                            title: "Actualización exitosa",
                            text: "Se actualizó la información en la plataforma",
                            icon: "success",
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                        });

                        setTimeout(() => {
                            $("#modal-planes").modal('hide');
                            var opciones = tabla_planes.DataTable();
                            opciones.ajax.reload();
                        }, 2000);
                    },
                    error: function (err) {
                        document.getElementById("submit").disabled = false;

                        document.getElementById("start_date").disabled = false;
                        document.getElementById("end_date").disabled = false;
                        document.getElementById("title").disabled = false;
                        document.getElementById("description").disabled = false;
                        document.getElementById("price").disabled = false;
                        document.getElementById("membresias").disabled = false;
                        document.getElementById("status").disabled = false;

                        errorHttp(err.status);
                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});