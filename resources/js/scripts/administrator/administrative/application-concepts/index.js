/*=========================================================================================
  File Name: table-datatables-basic.js
  Description: jquery datatable js
  ----------------------------------------------------------------------------------------
  Item Name: SOFTWORLD COLOMBIA - DATATABLES
  Author: SOFTWORLD COLOMBIA
  Project: CENTRO NACIONAL DE PRUEBAS
  Author URL: https://softworldcolombia.com/
==========================================================================================*/

$(function () {
    "use strict";
  
    var tabla_solicitudes_conceptos = $(".datatables-solicitudes-conceptos"),
      assetPath = "../../../app-assets/";
  
    if ($("body").attr("data-framework") === "laravel") {
      assetPath = $("body").attr("data-asset-path");
    }
  
    /* =========================== CENTRO NACIONAL DE PRUEBAS ===========================*/
  
    /* |||||||||||||||| SOLICITUDES CONCEPTOS |||||||||||||||| */
  
    if (tabla_solicitudes_conceptos.length) {
      var dt_basic = tabla_solicitudes_conceptos.DataTable({
        ajax: "/app/administrative/application-concepts/listado",
        columns: [
          { data: "vacio" },
          { data: "codigo_solicitud" },
          { data: "titulo_solicitud" },
          { data: "fecha_solicitud" },
          { data: "usuario_solicitud" },
          { data: "nombre_estado" },
          { data: "" },
        ],
        columnDefs: [
          {
            className: "control",
            orderable: false,
          },
          {
            // Actions
            targets: -1,
            title: "Acciones",
            orderable: false,
            render: function (data, type, full, meta) {
              return (
                ' <button class="btn btn-success ver-solicitud-concepto btn-sm" title="Ver noticia" data-id="' +
                full["codigo_solicitud"] +
                '">' +
                feather.icons["eye"].toSvg({ class: "font-small-4" }) +
                "</button> "
              );
            },
          },
        ],
        responsive: {
          details: {
            display: $.fn.dataTable.Responsive.display.modal({
              header: function (row) {
                var data = row.data();
                return "Informacion completa para " + data["titulo_concepto"];
              },
            }),
            type: "column",
            renderer: function (api, rowIdx, columns) {
              var data = $.map(columns, function (col, i) {
                return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                  ? '<tr data-dt-row="' +
                      col.rowIndex +
                      '" data-dt-column="' +
                      col.columnIndex +
                      '">' +
                      "<td>" +
                      col.title +
                      ":" +
                      "</td> " +
                      "<td>" +
                      col.data +
                      "</td>" +
                      "</tr>"
                  : "";
              }).join("");
  
              return data ? $('<table class="table"/>').append(data) : false;
            },
          },
        },
        language: {
          url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
        },
      });
      $("div.head-label").html('<h6 class="mb-0">DataTable with Buttons</h6>');
    }
  
    $(document).on("click", ".ver-solicitud-concepto", function () {
      $(".dtr-bs-modal").modal("hide");
      $("#modal-conceptos").modal("show");
      $("#contenido_modal").html(
        "<div class='loading_modal col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'><img src='https://s3.amazonaws.com/cnp.com.co/loading.gif'><p>Recibiendo datos...</p></div>"
      );
  
      $.fn.modal.Constructor.prototype._enforceFocus = function () {};
  
      const containerClaimReqModal = document.getElementById("modal-conceptos");
      const claimReqModal = new bootstrap.Modal(containerClaimReqModal, {
        focus: false,
      });
  
      $.ajax({
        type: "GET",
        url:
          "/app/administrativo/solicitudesconceptos/show/" +
          $(this).attr("data-id"),
        success: function (data) {
          $("#titulo_modal").html("Ver Información");
          $("#contenido_modal").html(data);
        },
        error: function (error) {
          $("#modal-conceptos").modal("hide");
          if (error.status == 403) {
            Swal.fire({
              allowOutsideClick: false,
              icon: "error",
              title: "Acción restringida",
              html:
                "Usted no tiene permisos para esta acción. Comuniquese con administración. <br><br> <b>CÓD. ERROR: " +
                error.status +
                "</b> ",
              footer:
                "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>",
            });
          } else {
            Swal.fire({
              allowOutsideClick: false,
              icon: "error",
              title: "Error desconocido",
              html:
                "Comunicate con soporte. <br> <b>CÓD. ERROR: " +
                error.status +
                "</b>",
              footer:
                "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>",
            });
          }
        },
      });
    });
  
    /* |||||||||||||||| FIN SOLICITUDES CONCEPTOS |||||||||||||||| */
    
  });
  