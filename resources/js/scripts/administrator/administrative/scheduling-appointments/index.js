$(function () {
    'use strict';

    var tabla_agendamiento_citas = $('.datatables-agendamiento-citas'),
        assetPath = '../../../app-assets/';


    if ($('body').attr('data-framework') === 'laravel') {
        assetPath = $('body').attr('data-asset-path');
    }

    if (tabla_agendamiento_citas.length) {
        var dt_basic = tabla_agendamiento_citas.DataTable({
            ajax: '/app/administrative/scheduling-appointments/listado',
            columns: [
                { data: 'vacio' }, // used for sorting so will hide this column
                { data: 'code' }, // used for sorting so will hide this column
                { data: 'names' },
                { data: 'last_names' },
                { data: 'date' },
                { data: 'nombre_estado' },
                { data: '' }
            ],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0
                },
                {
                    responsivePriority: 1,
                    targets: 4
                },
                {
                    // Actions
                    targets: -1,
                    title: 'Acciones',
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return (
                            '<button class="btn btn-success ver-agendamiento btn-sm" title="Ver información" data-id="' + full['code'] + '">'
                            + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
                            '</button>'
                            +
                            ' <button class="btn btn-info editar-agendamiento btn-sm" title="Editar información" data-id="' + full['code'] + '">'
                            + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) +
                            '</button>'
                            +
                            ' <button class="btn btn-danger eliminar-agendamiento btn-sm" title="Eliminar información" data-id="' + full['code'] + '">'
                            + feather.icons['trash-2'].toSvg({ class: 'font-small-4' }) +
                            '</button>'
                        );
                    }
                }
            ],
            order: [[1, 'asc']],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Detalles del usuario ' + data['name'];
                        }
                    }),
                    type: 'column',
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {

                            return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                '<td>' +
                                col.title +
                                ':' +
                                '</td> ' +
                                '<td>' +
                                col.data +
                                '</td>' +
                                '</tr>'
                                : '';
                        }).join('');

                        return data ? $('<table class="table"/>').append(data) : false;
                    }
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">DataTable with Buttons</h6>');
    }

    $(document).on("click", ".ver-agendamiento", function () {
        animationLoading('', ' Cargando información...');

        $.ajax({
            type: 'GET',
            url: '/app/administrative/scheduling-appointments/show/' + $(this).attr("data-id"),
            success: function (data) {
                Swal.close();
                $("#modal").modal("show");
                $("#contenido").html(data);
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".editar-agendamiento", function () {
        animationLoading('', ' Cargando información...');

        $.ajax({
            type: 'GET',
            url: '/app/administrative/scheduling-appointments/edit/' + $(this).attr("data-id"),
            success: function (data) {
                Swal.close();
                $("#modal").modal("show");
                $("#titulo").html("Editar agendamiento");
                $("#contenido").html(data);
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".eliminar-agendamiento", function () {
        Swal.fire({
            icon: "question",
            title: "¿Esta seguro que desea eliminarlo?",
            html: "Una vez eliminado, no se podrá recuperar",
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonColor: "#003f48",
            confirmButtonText: "Si, eliminar",
            denyButtonText: `Cancelar`,
            allowOutsideClick: false,
            allowEscapeKey: false,
            focusConfirm: true,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/app/administrative/scheduling-appointments/delete/" + $(this).attr("data-id"),
                    success: function (data) {
                        if (data == true) {
                            Swal.fire("Información eliminada", "", "info");
                            setTimeout(function () {
                                var tabla = tabla_agendamiento_citas.DataTable();
                                tabla.ajax.reload();
                            }, 2000);
                        } else {
                            Swal.fire("Error en el proceso", "", "info");
                        }
                    },
                    error: function (error) {
                        errorHttp(error.status);
                    },
                });
            } else if (result.isDenied) {
                Swal.fire("Eliminación cancelada", "", "info");
            }
        });
    });


});