$(function () {
    'use strict';

    const forms = document.querySelectorAll('.update-agendamiento');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {

                var paqueteDeDatos = new FormData();

                paqueteDeDatos.append('code', $('#code').prop('value'));
                paqueteDeDatos.append('date', $('#date').prop('value'));
                paqueteDeDatos.append('start_hour', $('#start_hour').prop('value'));
                paqueteDeDatos.append('end_hour', $('#end_hour').val());
                paqueteDeDatos.append('profesional_id', $('#profesional_id').prop('value'));
                paqueteDeDatos.append('status', $('#status').prop('value'));



                document.getElementById("date").disabled = true;
                document.getElementById("start_hour").disabled = true;
                document.getElementById("end_hour").disabled = true;
                document.getElementById("profesional_id").disabled = true;
                document.getElementById("status").disabled = true;

                document.getElementById("submit").disabled = true;

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "POST",
                    contentType: false,
                    url: "/app/administrative/scheduling-appointments/edit/update",
                    headers: {
                        "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content"),
                    },
                    data: paqueteDeDatos, // serializes the form's elements.
                    processData: false,
                    cache: false,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire(
                                "Registro exitoso!",
                                "El registro ha sido exitoso!",
                                "success"
                            );

                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        } else if (data == "preview_register") {
                            document.getElementById("date").disabled = false;
                            document.getElementById("start_hour").disabled = false;
                            document.getElementById("end_hour").disabled = false;
                            document.getElementById("profesional_id").disabled = false;
                            document.getElementById("status").disabled = false;

                            document.getElementById("submit").disabled = false;

                            Swal.fire(
                                "No hubo registro!",
                                "Ya hay un agendamiento registrado con esta información",
                                "error"
                            );
                        }
                    },
                    error: function (err) {

                        document.getElementById("date").disabled = false;
                        document.getElementById("start_hour").disabled = false;
                        document.getElementById("end_hour").disabled = false;
                        document.getElementById("profesional_id").disabled = false;
                        document.getElementById("status").disabled = false;

                        document.getElementById("submit").disabled = false;

                        errorHttp(err.status);

                        if (err.status == 422) {
                            $(".errores").attr("style", "display:none;");
                            $("#success_message").fadeIn().html(err.responseJSON.message);

                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' + i + '"]');
                                el.after(
                                    $(
                                        '<span class="errores" style="color: red;">' +
                                        error[0] +
                                        "</span>"
                                    )
                                );
                            });
                        }
                    },
                });

                event.preventDefault();

            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});