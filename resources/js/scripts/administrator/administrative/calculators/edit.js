$(document).ready(function () {
    $('.selector').select2();
});


var changePicture = $('#image'),
    userAvatar = $('.user-avatar');

// Change user profile picture
if (changePicture.length) {
    $(changePicture).on('change', function (e) {
        var reader = new FileReader(),
            files = e.target.files;
        reader.onload = function () {
            if (userAvatar.length) {
                userAvatar.attr('src', reader.result);
            }
        };
        reader.readAsDataURL(files[0]);
    });
};

var tabla_calculadoras = $('.datatables-calculadoras');

// Actualizacion de servicio
$(function () {
    'use strict';

    const forms = document.querySelectorAll('.update-calculadoras');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {
                document.getElementById("membresias").disabled = true;
                document.getElementById("code").disabled = true;
                document.getElementById("title").disabled = true;
                document.getElementById("description").disabled = true;
                document.getElementById("status").disabled = true;
                document.getElementById("submit").disabled = true;

                var paqueteDeDatos = new FormData();
                paqueteDeDatos.append('image', $('#image')[0].files[0]);
                paqueteDeDatos.append('membresias', $('#membresias').val());
                paqueteDeDatos.append('code', $('#code').val());
                paqueteDeDatos.append('title', $('#title').val());
                paqueteDeDatos.append('description', $('#description').val());
                paqueteDeDatos.append('status', $('#status').val());

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: "/app/administrative/calculators/edit/update",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        updateSuccess();

                        setTimeout(() => {
                            $("#modal-calculadoras").modal('hide');
                            var opciones = tabla_calculadoras.DataTable();
                            opciones.ajax.reload();
                        }, 1000);
                    },
                    error: function (err) {
                        errorHttp(err.status);
                        document.getElementById("membresias").disabled = false;
                        document.getElementById("code").disabled = false;
                        document.getElementById("title").disabled = false;
                        document.getElementById("description").disabled = false;
                        document.getElementById("status").disabled = false;
                        document.getElementById("submit").disabled = false;

                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});