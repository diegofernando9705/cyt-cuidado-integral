/*=========================================================================================
  File Name: table-datatables-basic.js
  Description: jquery datatable js
  ----------------------------------------------------------------------------------------
  Item Name: SOFTWORLD COLOMBIA - DATATABLES
  Author: SOFTWORLD COLOMBIA
  Project: CENTRO NACIONAL DE PRUEBAS
  Author URL: https://softworldcolombia.com/
==========================================================================================*/

$(function () {
    "use strict";

    var tabla_calculadoras = $(".datatables-calculadoras"),
        assetPath = "../../../app-assets/";

    if ($("body").attr("data-framework") === "laravel") {
        assetPath = $("body").attr("data-asset-path");
    }

    /* =========================== CENTRO NACIONAL DE PRUEBAS ===========================*/

    /* |||||||||||||||| CALCULADORAS |||||||||||||||| */

    if (tabla_calculadoras.length) {
        var dt_basic = tabla_calculadoras.DataTable({
            ajax: "/app/administrative/calculators/listado",
            columns: [
                { data: "vacio" },
                { data: "pk_calculadora" },
                {
                    data: "imagen_calculadora",
                    render: function (data, type, row, meta) {
                        if (data) {
                            return (
                                "<img src='https://s3.amazonaws.com/cnp.com.co/" +
                                data +
                                "' width='80px'>"
                            );
                        } else {
                            return (
                                "<img src='https://s3.amazonaws.com/cnp.com.co/" +
                                data +
                                "' width='80px'>"
                            );
                        }
                    },
                },
                { data: "titulo_calculadora" },
                { data: "descripcion_calculadora" },
                { data: "nombre_estado" },
                { data: "" },
            ],
            columnDefs: [
                {
                    className: "control",
                    orderable: false,
                },
                {
                    // Actions
                    targets: -1,
                    title: "Acciones",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        if (full["codigo_estado"] == 1) {
                            return (
                                ' <button class="btn btn-info editar-calculadora btn-sm" title="Editar calculadora" data-id="' +
                                full["pk_calculadora"] +
                                '">' +
                                feather.icons["edit"].toSvg({ class: "font-small-4" }) +
                                "</button>" +
                                ' <button class="btn btn-danger eliminar-calculadora btn-sm" title="Eliminar calculadora" data-id="' +
                                full["pk_calculadora"] +
                                '">' +
                                feather.icons["trash-2"].toSvg({ class: "font-small-4" }) +
                                "</button>"
                            );
                        } else {
                            return (
                                '<button class="btn btn-success ver-calculadora btn-sm" title="Ver Información" disabled data-id="' +
                                full["pk_calculadora"] +
                                '">' +
                                feather.icons["eye"].toSvg({ class: "font-small-4" }) +
                                "</button>"
                            );
                        }
                    },
                },
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return "Informacion completa para " + data["titulo_membresia"];
                        },
                    }),
                    type: "column",
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                "<td>" +
                                col.title +
                                ":" +
                                "</td> " +
                                "<td>" +
                                col.data +
                                "</td>" +
                                "</tr>"
                                : "";
                        }).join("");

                        return data ? $('<table class="table"/>').append(data) : false;
                    },
                },
            },
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
            },
        });
        $("div.head-label").html('<h6 class="mb-0">DataTable with Buttons</h6>');
    }

    $(document).on("click", ".editar-calculadora", function () {
        animationLoading('', ' Cargando información...');

        $.ajax({
            type: "GET",
            url: "/app/administrative/calculators/edit/" + $(this).attr("data-id"),
            success: function (data) {
                $("#modal-calculadoras").modal("show");
                $("#titulo_modal").html("Ver Información");
                $("#contenido_modal").html(data);
                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".eliminar-calculadora", function () {
        $(".modal").modal("hide");
        Swal.fire({
            icon: "question",
            title: "¿Esta seguro que desea eliminarlo?",
            html: "Una vez eliminado, no se podrá recuperar",
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonColor: "#003f48",
            confirmButtonText: "Si, eliminar",
            denyButtonText: `Cancelar`,
            allowOutsideClick: false,
            allowEscapeKey: false,
            focusConfirm: true,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/app/administrative/calculators/delete/" + $(this).attr("data-id"),
                    success: function (data) {
                        deleteSuccess();
                        setTimeout(function () {
                            var tabla = tabla_calculadoras.DataTable();
                            tabla.ajax.reload();
                        }, 1000);
                    },
                    error: function (error) {
                        errorHttp(error.status);
                    },
                });
            } else if (result.isDenied) {
                Swal.fire("Eliminación cancelada", "", "info");
            }
        });
    });

    /* |||||||||||||||| FIN CALCULADORAS |||||||||||||||| */

});
