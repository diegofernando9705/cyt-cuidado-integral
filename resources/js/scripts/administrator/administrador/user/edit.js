$(function () {
    'use strict';
    var table_user = $('.datatables-usuarios');

    const formulario_usuario_edicion = document.querySelectorAll('.edicion-usuario');

    Array.prototype.slice.call(formulario_usuario_edicion).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {
                var formulario = $(".edicion-usuario").serialize();

                document.getElementById("nombre_usuario").disabled = true;
                document.getElementById("correo_usuario").disabled = true;
                document.getElementById("password").disabled = true;
                document.getElementById("confirmar_password").disabled = true;
                document.getElementById("rol_usuario").disabled = true;
                document.getElementById("estado_usuario").disabled = true;
                document.getElementById("submit").disabled = true;

                $.ajax({
                    type: 'POST',
                    url: '/app/administrador/usuarios/edit/update',
                    data: formulario,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire({
                                icon: "success",
                                title: "Actualización exitosa",
                                text: "La información se ha actualizado correctamente.",
                            });

                            var tabla = table_user.DataTable();
                            tabla.ajax.reload();

                            setTimeout(function () {
                                $("#modal-edicion-usuario").modal("hide");
                            }, 500);
                        }
                    },
                    error: function (err) {
                        document.getElementById("nombre_usuario").disabled = false;
                        document.getElementById("correo_usuario").disabled = false;
                        document.getElementById("password").disabled = false;
                        document.getElementById("confirmar_password").disabled = false;
                        document.getElementById("rol_usuario").disabled = false;
                        document.getElementById("estado_usuario").disabled = false;
                        document.getElementById("submit").disabled = false;

                        
                        errorHttp(err.status);
                        $(".error-ajax").attr("style", "display:none;");
                        if (err.status == 422) {
                            $('#success_message').fadeIn().html(err.responseJSON.message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' + i + '"]');
                                el.after($('<span style="color: red;" class="error-ajax">' + error[0] + '</span>'));
                            });
                        }
                    },
                });

                event.preventDefault();
            }

            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});
