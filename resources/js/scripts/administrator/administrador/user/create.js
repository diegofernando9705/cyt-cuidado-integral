
$(function () {
    'use strict';

    const formulario_usuario = document.querySelectorAll('.registro-usuario');


    Array.prototype.slice.call(formulario_usuario).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {


                var paqueteDeDatos = new FormData();
                paqueteDeDatos.append('change-picture', $('#change-picture')[0].files[0]);

                paqueteDeDatos.append('nombre_usuario', $('#nombre_usuario').prop('value'));
                paqueteDeDatos.append('correo_usuario', $('#correo_usuario').prop('value'));
                paqueteDeDatos.append('password', $('#password').prop('value'));
                paqueteDeDatos.append('confirmar_password', $('#confirmar_password').prop('value'));
                paqueteDeDatos.append('rol_usuario', $('#rol_usuario').prop('value'));
                paqueteDeDatos.append('persona_asignada', $('#persona_asignada').prop('value'));


                document.getElementById("nombre_usuario").disabled = true;
                document.getElementById("correo_usuario").disabled = true;
                document.getElementById("password").disabled = true;
                document.getElementById("confirmar_password").disabled = true;
                document.getElementById("rol_usuario").disabled = true;
                document.getElementById("submit").disabled = true;
                document.getElementById("cancelar").disabled = true;
                document.getElementById("persona_asignada").disabled = true;


                $.ajax({
                    type: 'POST',
                    contentType: false,
                    url: '/app/administrador/usuarios/create/store',
                    headers: { 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') },
                    data: paqueteDeDatos,
                    processData: false,
                    cache: false,
                    success: function (data) {

                        if (data == true) {
                            Swal.fire({
                                icon: "success",
                                title: "Registero exitoso",
                                text: "La información se ha registrado correctamente.",
                            });

                            
                            setTimeout(function () {
                                window.location = "/app/administrador/usuario";
                            }, 2000);
                        } else if (data == 'asignacion_duplicada') {
                            $("#mensajes").html("<div class='alert alert-danger' style='padding:10px'>Ya hay un usuario asignado para la persona seleccionada.</div>");

                            document.getElementById("nombre_usuario").disabled = false;
                            document.getElementById("correo_usuario").disabled = false;
                            document.getElementById("password").disabled = false;
                            document.getElementById("confirmar_password").disabled = false;
                            document.getElementById("rol_usuario").disabled = false;
                            document.getElementById("submit").disabled = false;
                            document.getElementById("cancelar").disabled = false;
                            document.getElementById("persona_asignada").disabled = false;
                        } else {
                            $("#mensajes").html("<div class='alert alert-danger' style='padding:10px'>Error del servidor, verifica e intenta nuevamente.</div>");
                        }
                    },
                    error: function (err) {
                        document.getElementById("nombre_usuario").disabled = false;
                        document.getElementById("correo_usuario").disabled = false;
                        document.getElementById("password").disabled = false;
                        document.getElementById("confirmar_password").disabled = false;
                        document.getElementById("rol_usuario").disabled = false;
                        document.getElementById("submit").disabled = false;
                        document.getElementById("cancelar").disabled = false;
                        document.getElementById("persona_asignada").disabled = false;

                        errorHttp(err.status);
                        if (err.status == 422) {
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON.message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' + i + '"]');
                                el.after($('<span style="color: red;">' + error[0] + '</span>'));
                            });
                        }
                    },
                });


                event.preventDefault();
            }

            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });

});
