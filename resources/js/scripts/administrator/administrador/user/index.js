$(function () {
    'use strict';

    var tabla_usuarios = $('.datatables-usuarios'),
        assetPath = '../../../app-assets/';


    if ($('body').attr('data-framework') === 'laravel') {
        assetPath = $('body').attr('data-asset-path');
    }

    if (tabla_usuarios.length) {
        var dt_basic = tabla_usuarios.DataTable({
            ajax: '/app/administrador/usuarios/listado',
            columns: [
                { data: 'id' }, // used for sorting so will hide this column
                { data: 'id' }, // used for sorting so will hide this column
                { data: 'name' },
                { data: 'email' },
                { data: '' }
            ],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0
                },
                {
                    responsivePriority: 1,
                    targets: 4
                },
                {
                    // Actions
                    targets: -1,
                    title: 'Acciones',
                    orderable: false,
                    render: function (data, type, full, meta) {

                        return (
                            '<button class="btn btn-info editar-usuario btn-sm" title="Editar información" data-id="' + full['id'] + '">'
                            + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) +
                            '</button>'
                            +
                            ' <button class="btn btn-danger eliminar-usuario btn-sm" title="Eliminar información" data-id="' + full['id'] + '">'
                            + feather.icons['trash-2'].toSvg({ class: 'font-small-4' }) +
                            '</button>'

                        );
                    }
                }
            ],
            order: [[1, 'asc']],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Detalles del usuario ' + data['name'];
                        }
                    }),
                    type: 'column',
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {

                            return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                '<td>' +
                                col.title +
                                ':' +
                                '</td> ' +
                                '<td>' +
                                col.data +
                                '</td>' +
                                '</tr>'
                                : '';
                        }).join('');

                        return data ? $('<table class="table"/>').append(data) : false;
                    }
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">DataTable with Buttons</h6>');
    }

    $(document).on("click", ".editar-usuario", function () {
        animationLoading('',' Cargando información...');

        $.ajax({
            type: 'GET',
            url: '/app/administrador/usuarios/edit/' + $(this).attr("data-id"),
            success: function (data) {
                Swal.close();
                $("#modal-edicion-usuario").modal("show");
                $("#contenido-rol-edicion").html(data);
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".eliminar-usuario", function () {
        Swal.fire({
            icon: "question",
            title: "¿Esta seguro que desea eliminarlo?",
            html: "Una vez eliminado, no se podrá recuperar",
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonColor: "#003f48",
            confirmButtonText: "Si, eliminar",
            denyButtonText: `Cancelar`,
            allowOutsideClick: false,
            allowEscapeKey: false,
            focusConfirm: true,
          }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
              $.ajax({
                type: "GET",
                url: "/app/administrador/usuarios/delete/" + $(this).attr("data-id"),
                success: function (data) {
                  if (data == true) {
                    Swal.fire("Información eliminada", "", "info");
                    setTimeout(function () {
                      var tabla = tabla_usuarios.DataTable();
                      tabla.ajax.reload();
                    }, 2000);
                  }else{
                    Swal.fire("Error en el proceso", "", "info");
                  }
                },
                error: function (error) {
                  errorHttp(error.status);
                },
              });
            } else if (result.isDenied) {
              Swal.fire("Eliminación cancelada", "", "info");
            }
          });
    });

    
});