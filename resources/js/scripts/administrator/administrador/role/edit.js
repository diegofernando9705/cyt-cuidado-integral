
$(function () {
    'use strict';

    const forms = document.querySelectorAll('.actualizar-rol');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {
            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {

                var formulario = $(".actualizar-rol").serialize();

                $.ajax({
                    type: 'POST',
                    url: '/app/administrador/roles/edit/update',
                    data: formulario, // serializes the form's elements.
                    success: function (data) {
                        document.getElementById("registro-btn").disabled = true;
                        document.getElementById("regreso-btn").disabled = true;

                        Swal.fire({
                            icon: "success",
                            title: "Actualización exitosa",
                            text: "La información se ha actualizado correctamente.",
                        });

                        setTimeout(function () {
                            window.location = "/app/administrador/roles";
                        }, 2000);
                    },
                    error: function (error) {
                        errorHttp(error.status);
                    },
                });
                event.preventDefault();

            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});