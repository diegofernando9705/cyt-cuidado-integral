$(function () {
    'use strict';

    const forms = document.querySelectorAll('.registro-rol');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {

                var formulario = $(".registro-rol").serialize();

                $.ajax({
                    type: 'POST',
                    url: '/app/administrador/roles/create/store',
                    data: formulario, // serializes the form's elements.
                    success: function (data) {
                        document.getElementById("nombre_rol").disabled = true;
                        document.getElementById("registro-btn").disabled = true;

                        Swal.fire({
                            icon: "success",
                            title: "Registo exitoso",
                            text: "La información se ha registrado correctamente.",
                        });

                        if (data == true) {
                            setTimeout(function () {
                                window.location = "/app/administrador/roles";
                            }, 2000);

                        }

                    },
                    error: function (error) {
                        errorHttp(error.status);
                    },
                });
                event.preventDefault();

            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });


    // jQuery Validation
    // --------------------------------------------------------------------
    if (forms.length) {
        forms.validate({
            /*
            * ? To enable validation onkeyup
            onkeyup: function (element) {
              $(element).valid();
            },*/
            /*
            * ? To enable validation on focusout
            onfocusout: function (element) {
              $(element).valid();
            }, */
            rules: {
                'nombre_rol': {
                    required: true
                }
            }
        });
    }
});