
$(function () {
    'use strict';

    var tabla_roles = $('.datatables-basic'),
        assetPath = '../../../app-assets/';

    if ($('body').attr('data-framework') === 'laravel') {
        assetPath = $('body').attr('data-asset-path');
    }

    if (tabla_roles.length) {
        var dt_basic = tabla_roles.DataTable({
            ajax: '/app/administrador/roles/listado',
            columns: [
                { data: 'id' }, // used for sorting so will hide this column
                { data: 'id' }, // used for sorting so will hide this column
                { data: 'name' },
                { data: 'guard_name' },
                { data: 'created_at' },
                { data: '' }
            ],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0
                },
                {
                    responsivePriority: 1,
                    targets: 4
                },
                {
                    // Actions
                    targets: -1,
                    title: 'Acciones',
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return (
                            '<button class="btn btn-success ver-rol btn-sm" title="Ver información" data-id="' + full['id'] + '">'
                            + feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
                            '</button>'
                            +
                            ' <a href="/app/administrador/roles/edit/' + full['id'] + '" title="Editar información" style="color:white;">'
                            + '<button class="btn btn-info btn-sm"">' + feather.icons['file-text'].toSvg({ class: 'font-small-4' }) + '</button>' +
                            ' </a>'
                        );
                    }
                }
            ],
            order: [[1, 'desc']],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details of ' + data['name'];
                        }
                    }),
                    type: 'column',
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {

                            return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                '<td>' +
                                col.title +
                                ':' +
                                '</td> ' +
                                '<td>' +
                                col.data +
                                '</td>' +
                                '</tr>'
                                : '';
                        }).join('');

                        return data ? $('<table class="table"/>').append(data) : false;
                    }
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">DataTable with Buttons</h6>');
    }

    $(document).on("click", ".ver-rol", function () {
        animationLoading('', 'Cargando información...');
        $.ajax({
            type: 'GET',
            url: '/app/administrador/roles/show/' + $(this).attr("data-id"),
            success: function (data) {
                Swal.close();
                $("#modal-ver-rol").modal("show");
                $("#contenido-rol").html(data);
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });
});