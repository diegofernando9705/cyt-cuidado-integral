var data_resumen_edit;
var data_descripcion_edit;

$(document).ready(function () {
    $('.selector').select2();
});

var tabla_noticias = $('.datatables-noticias');
var changePicture = $('#imagen_noticia'),
    userAvatar = $('.user-avatar');

// Change user profile picture
if (changePicture.length) {
    $(changePicture).on('change', function (e) {
        var reader = new FileReader(),
            files = e.target.files;
        reader.onload = function () {
            if (userAvatar.length) {
                userAvatar.attr('src', reader.result);
            }
        };
        reader.readAsDataURL(files[0]);
    });
};

$(document).on("keyup", "#url_noticia", function () {
    let contenido = $(this).val();
    $(this).val(contenido.replace(" ", "_"));
});


// Actualizacion de servicio
$(function () {
    'use strict';

    const forms = document.querySelectorAll('.update-noticias');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {
                document.getElementById("submit").disabled = true;

                var paqueteDeDatos = new FormData();
                paqueteDeDatos.append('imagen_noticia', $('#imagen_noticia')[0].files[0]);

                paqueteDeDatos.append('fecha_registro', $('#fecha_registro').prop('value'));
                paqueteDeDatos.append('fecha_terminacion', $('#fecha_terminacion').prop('value'));

                paqueteDeDatos.append('titulo_noticia', $('#titulo_noticia').prop('value'));
                paqueteDeDatos.append('resumen_noticia', $('#resumen_noticia').prop('value'));
                paqueteDeDatos.append('descripcion_noticia', data_descripcion_edit.getData());

                paqueteDeDatos.append('type', $('#type').prop('value'));
                paqueteDeDatos.append('url_noticia', $('#url_noticia').prop('value'));
                paqueteDeDatos.append('categorias_noticia', $('#categorias_noticia').val());
                paqueteDeDatos.append('estado_noticia', $('#estado_noticia').prop('value'));

                document.getElementById("imagen_noticia").disabled = true;
                document.getElementById("fecha_registro").disabled = true;
                document.getElementById("fecha_terminacion").disabled = true;
                document.getElementById("titulo_noticia").disabled = true;
                document.getElementById("resumen_noticia").disabled = true;
                document.getElementById("type").disabled = true;
                document.getElementById("url_noticia").disabled = true;
                document.getElementById("categorias_noticia").disabled = true;
                document.getElementById("estado_noticia").disabled = true;

                document.getElementById("submit").disabled = true;

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '/app/webpage/news/edit/update/' + $("#id").prop('value'),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire({
                                title: "Actualización exitosa",
                                text: "La información se ha actualizado correctamente",
                                icon: "success"
                            });

                            setTimeout(() => {
                                $("#modal-noticias").modal('hide');
                                var opciones = tabla_noticias.DataTable();
                                opciones.ajax.reload();
                            }, 1000);
                        }
                    },
                    error: function (err) {
                        errorHttp(err.status);

                        document.getElementById("imagen_noticia").disabled = false;
                        document.getElementById("fecha_registro").disabled = false;
                        document.getElementById("fecha_terminacion").disabled = false;
                        document.getElementById("titulo_noticia").disabled = false;
                        document.getElementById("resumen_noticia").disabled = false;
                        document.getElementById("type").disabled = false;
                        document.getElementById("url_noticia").disabled = false;
                        document.getElementById("categorias_noticia").disabled = false;
                        document.getElementById("estado_noticia").disabled = false;

                        document.getElementById("submit").disabled = false;

                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});