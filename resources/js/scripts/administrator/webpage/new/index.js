$(function () {
    "use strict";

    var tabla_noticias = $(".datatables-noticias"),
        assetPath = "../../../app-assets/";

    if ($("body").attr("data-framework") === "laravel") {
        assetPath = $("body").attr("data-asset-path");
    }

    if (tabla_noticias.length) {
        var dt_basic = tabla_noticias.DataTable({
            ajax: "/app/webpage/news/listado",
            columns: [
                { data: "vacio" },
                {
                    data: "imagen_noticia",
                    render: function (data, type, row, meta) {
                        return (
                            "<img src='" + data + "' width='80px' title=''>"
                        );
                    },
                },
                { data: "titulo_noticia" },
                { data: "fecha_registro" },
                { data: "url_noticia" },
                { data: "nombre_estado" },
                { data: "" },
            ],
            columnDefs: [
                {
                    className: "control",
                    orderable: false,
                    responsivePriority: 3,
                    targets: 0,
                },
                {
                    responsivePriority: 3,
                    targets: 0,
                },
                {
                    // Actions
                    targets: -1,
                    title: "Acciones",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        if (full["codigo_estado"] == 1) {
                            return (
                                ' <button class="btn btn-success ver-noticia btn-sm" title="Ver noticia" data-id="' +
                                full["codigo_noticia"] +
                                '">' +
                                feather.icons["eye"].toSvg({ class: "font-small-4" }) +
                                "</button> " +
                                ' <button class="btn btn-info editar-noticia btn-sm" title="Editar Información" data-id="' +
                                full["codigo_noticia"] +
                                '">' +
                                feather.icons["file-text"].toSvg({ class: "font-small-4" }) +
                                "</button> " +
                                ' <button class="btn btn-danger eliminar-noticia btn-sm" title="Eliminar Información" data-id="' +
                                full["codigo_noticia"] +
                                '">' +
                                feather.icons["trash-2"].toSvg({ class: "font-small-4" }) +
                                "</button>" +
                                ' <button class="btn btn-primary asignar-membresia-noticia btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" title="Asignar a membresia" data-id="' +
                                full["codigo_noticia"] +
                                '" style="margin-top:5px;"> Asignar membresia </button>'
                            );
                        } else {
                            return (
                                '<button class="btn btn-success ver-noticia btn-sm" title="Ver Información" disabled data-id="' +
                                full["codigo_noticia"] +
                                '">' +
                                feather.icons["eye"].toSvg({ class: "font-small-4" }) +
                                "</button>"
                            );
                        }
                    },
                },
            ],
            order: [[1, "asc"]],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return "Informacion completa para " + data["titulo_noticia"];
                        },
                    }),
                    type: "column",
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                "<td>" +
                                col.title +
                                ":" +
                                "</td> " +
                                "<td>" +
                                col.data +
                                "</td>" +
                                "</tr>"
                                : "";
                        }).join("");

                        return data ? $('<table class="table"/>').append(data) : false;
                    },
                },
            },
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
            },
        });
        $("div.head-label").html('<h6 class="mb-0">DataTable with Buttons</h6>');
    }

    $(document).on("click", ".ver-noticia", function () {
        animationLoading("", "Cargando información...");

        $.ajax({
            type: "GET",
            url: "/app/webpage/news/show/" + $(this).attr("data-id"),
            success: function (data) {
                $(".dtr-bs-modal").modal("hide");
                $("#modal-noticias").modal("show");

                $("#titulo_modal").html("Ver de Información");
                $("#contenido_modal").html(data);

                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".editar-noticia", function () {
        animationLoading("", "Cargando información...");

        $.ajax({
            type: "GET",
            url: "/app/webpage/news/edit/" + $(this).attr("data-id"),
            success: function (data) {
                $(".dtr-bs-modal").modal("hide");
                $("#modal-noticias").modal("show");

                $("#titulo_modal").html("Actualizacion de Información");
                $("#contenido_modal").html(data);

                Swal.close();

                CKEDITOR.ClassicEditor.create(
                    document.getElementById("descripcion_noticia"),
                    {
                        toolbar: {
                            items: [
                                "exportPDF",
                                "exportWord",
                                "|",
                                "findAndReplace",
                                "selectAll",
                                "|",
                                "heading",
                                "|",
                                "bold",
                                "italic",
                                "strikethrough",
                                "underline",
                                "code",
                                "subscript",
                                "superscript",
                                "removeFormat",
                                "|",
                                "bulletedList",
                                "numberedList",
                                "todoList",
                                "|",
                                "outdent",
                                "indent",
                                "|",
                                "undo",
                                "redo",
                                "-",
                                "fontSize",
                                "fontFamily",
                                "fontColor",
                                "fontBackgroundColor",
                                "highlight",
                                "|",
                                "alignment",
                                "|",
                                "link",
                                "insertImage",
                                "blockQuote",
                                "insertTable",
                                "mediaEmbed",
                                "codeBlock",
                                "htmlEmbed",
                                "|",
                                "specialCharacters",
                                "horizontalLine",
                                "pageBreak",
                                "|",
                                "textPartLanguage",
                                "|",
                                "sourceEditing",
                            ],
                            shouldNotGroupWhenFull: true,
                        },
                        language: "es",
                        list: {
                            properties: {
                                styles: true,
                                startIndex: true,
                                reversed: true,
                            },
                        },
                        heading: {
                            options: [
                                {
                                    model: "paragraph",
                                    title: "Paragraph",
                                    class: "ck-heading_paragraph",
                                },
                                {
                                    model: "heading1",
                                    view: "h1",
                                    title: "Heading 1",
                                    class: "ck-heading_heading1",
                                },
                                {
                                    model: "heading2",
                                    view: "h2",
                                    title: "Heading 2",
                                    class: "ck-heading_heading2",
                                },
                                {
                                    model: "heading3",
                                    view: "h3",
                                    title: "Heading 3",
                                    class: "ck-heading_heading3",
                                },
                                {
                                    model: "heading4",
                                    view: "h4",
                                    title: "Heading 4",
                                    class: "ck-heading_heading4",
                                },
                                {
                                    model: "heading5",
                                    view: "h5",
                                    title: "Heading 5",
                                    class: "ck-heading_heading5",
                                },
                                {
                                    model: "heading6",
                                    view: "h6",
                                    title: "Heading 6",
                                    class: "ck-heading_heading6",
                                },
                            ],
                        },
                        placeholder: "Escribe acá",
                        fontFamily: {
                            options: [
                                "default",
                                "Arial, Helvetica, sans-serif",
                                "Courier New, Courier, monospace",
                                "Georgia, serif",
                                "Lucida Sans Unicode, Lucida Grande, sans-serif",
                                "Tahoma, Geneva, sans-serif",
                                "Times New Roman, Times, serif",
                                "Trebuchet MS, Helvetica, sans-serif",
                                "Verdana, Geneva, sans-serif",
                            ],
                            supportAllValues: true,
                        },
                        fontSize: {
                            options: [10, 12, 14, "default", 18, 20, 22],
                            supportAllValues: true,
                        },
                        htmlSupport: {
                            allow: [
                                {
                                    name: /.*/,
                                    attributes: true,
                                    classes: true,
                                    styles: true,
                                },
                            ],
                        },
                        htmlEmbed: {
                            showPreviews: true,
                        },
                        link: {
                            decorators: {
                                addTargetToExternalLinks: true,
                                defaultProtocol: "https://",
                                toggleDownloadable: {
                                    mode: "manual",
                                    label: "Downloadable",
                                    attributes: {
                                        download: "file",
                                    },
                                },
                            },
                        },
                        mention: {
                            feeds: [
                                {
                                    marker: "@",
                                    feed: [
                                        "@apple",
                                        "@bears",
                                        "@brownie",
                                        "@cake",
                                        "@cake",
                                        "@candy",
                                        "@canes",
                                        "@chocolate",
                                        "@cookie",
                                        "@cotton",
                                        "@cream",
                                        "@cupcake",
                                        "@danish",
                                        "@donut",
                                        "@dragée",
                                        "@fruitcake",
                                        "@gingerbread",
                                        "@gummi",
                                        "@ice",
                                        "@jelly-o",
                                        "@liquorice",
                                        "@macaroon",
                                        "@marzipan",
                                        "@oat",
                                        "@pie",
                                        "@plum",
                                        "@pudding",
                                        "@sesame",
                                        "@snaps",
                                        "@soufflé",
                                        "@sugar",
                                        "@sweet",
                                        "@topping",
                                        "@wafer",
                                    ],
                                    minimumCharacters: 1,
                                },
                            ],
                        },
                        removePlugins: [
                            "CKBox",
                            "CKFinder",
                            "EasyImage",
                            "RealTimeCollaborativeComments",
                            "RealTimeCollaborativeTrackChanges",
                            "RealTimeCollaborativeRevisionHistory",
                            "PresenceList",
                            "Comments",
                            "TrackChanges",
                            "TrackChangesData",
                            "RevisionHistory",
                            "Pagination",
                            "WProofreader",
                            "MathType",
                            "SlashCommand",
                            "Template",
                            "DocumentOutline",
                            "FormatPainter",
                            "TableOfContents",
                        ],
                    }
                ).then((editor_descripcion) => {
                    data_descripcion_edit = editor_descripcion;
                });
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".eliminar-noticia", function () {
        $(".modal").modal("hide");
        Swal.fire({
            title: "¿Desea eliminar el registro?",
            text: "No podrás revertir esto",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "Cancelar",
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/app/webpage/news/delete/" + $(this).attr("data-id"),
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire(
                                "Eliminado!",
                                "La información ha sido eliminada.",
                                "success"
                            );

                            setTimeout(() => {
                                var opciones = tabla_noticias.DataTable();
                                opciones.ajax.reload();
                            }, 2000);
                        }
                    },
                    error: function (err) {
                        errorHttp(err.status);
                    },
                });
            }
        });
    });

    $(document).on("click", ".asignar-membresia-noticia", function () {
        animationLoading("", "Cargando información...");
        $.ajax({
            type: "GET",
            url: "/app/webpage/news/form/membership/" + $(this).attr("data-id"),
            success: function (data) {
                $(".dtr-bs-modal").modal("hide");
                $("#modal-noticias").modal("show");

                $("#titulo_modal").html("Asignación de membresia");
                $("#contenido_modal").html(data);

                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });
});