$(document).ready(function () {
    $('.selector').select2();
});

var tabla_servicios = $('.datatables-servicios');
var changePicture = $('#imagen_noticia'),
    userAvatar = $('.user-avatar');

// Change user profile picture
if (changePicture.length) {
    $(changePicture).on('change', function (e) {
        var reader = new FileReader(),
            files = e.target.files;
        reader.onload = function () {
            if (userAvatar.length) {
                userAvatar.attr('src', reader.result);
            }
        };
        reader.readAsDataURL(files[0]);
    });
};

$(document).on("keyup", "#url_noticia", function () {
    let contenido = $(this).val();
    console.log(contenido);
    $(this).val(contenido.replace(" ", "_"));
});

let data_descripcion;

CKEDITOR.ClassicEditor.create(document.getElementById("descripcion_noticia"), {
    // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
    toolbar: {
        items: [
            'exportPDF', 'exportWord', '|',
            'findAndReplace', 'selectAll', '|',
            'heading', '|',
            'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript',
            'removeFormat', '|',
            'bulletedList', 'numberedList', 'todoList', '|',
            'outdent', 'indent', '|',
            'undo', 'redo',
            '-',
            'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
            'alignment', '|',
            'link', 'insertImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock', 'htmlEmbed',
            '|',
            'specialCharacters', 'horizontalLine', 'pageBreak', '|',
            'textPartLanguage', '|',
            'sourceEditing'
        ],
        shouldNotGroupWhenFull: true
    },
    // Changing the language of the interface requires loading the language file using the <script> tag.
    // language: 'es',
    list: {
        properties: {
            styles: true,
            startIndex: true,
            reversed: true
        }
    },
    // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
    heading: {
        options: [{
            model: 'paragraph',
            title: 'Paragraph',
            class: 'ck-heading_paragraph'
        },
        {
            model: 'heading1',
            view: 'h1',
            title: 'Heading 1',
            class: 'ck-heading_heading1'
        },
        {
            model: 'heading2',
            view: 'h2',
            title: 'Heading 2',
            class: 'ck-heading_heading2'
        },
        {
            model: 'heading3',
            view: 'h3',
            title: 'Heading 3',
            class: 'ck-heading_heading3'
        },
        {
            model: 'heading4',
            view: 'h4',
            title: 'Heading 4',
            class: 'ck-heading_heading4'
        },
        {
            model: 'heading5',
            view: 'h5',
            title: 'Heading 5',
            class: 'ck-heading_heading5'
        },
        {
            model: 'heading6',
            view: 'h6',
            title: 'Heading 6',
            class: 'ck-heading_heading6'
        }
        ]
    },
    // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
    placeholder: 'Escribe acá',
    // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
    fontFamily: {
        options: [
            'default',
            'Arial, Helvetica, sans-serif',
            'Courier New, Courier, monospace',
            'Georgia, serif',
            'Lucida Sans Unicode, Lucida Grande, sans-serif',
            'Tahoma, Geneva, sans-serif',
            'Times New Roman, Times, serif',
            'Trebuchet MS, Helvetica, sans-serif',
            'Verdana, Geneva, sans-serif'
        ],
        supportAllValues: true
    },
    // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
    fontSize: {
        options: [10, 12, 14, 'default', 18, 20, 22],
        supportAllValues: true
    },
    // Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
    // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
    htmlSupport: {
        allow: [{
            name: /.*/,
            attributes: true,
            classes: true,
            styles: true
        }]
    },
    // Be careful with enabling previews
    // https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
    htmlEmbed: {
        showPreviews: true
    },
    // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
    link: {
        decorators: {
            addTargetToExternalLinks: true,
            defaultProtocol: 'https://',
            toggleDownloadable: {
                mode: 'manual',
                label: 'Downloadable',
                attributes: {
                    download: 'file'
                }
            }
        }
    },
    // https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
    mention: {
        feeds: [{
            marker: '@',
            feed: [
                '@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes',
                '@chocolate', '@cookie', '@cotton', '@cream',
                '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread',
                '@gummi', '@ice', '@jelly-o',
                '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding',
                '@sesame', '@snaps', '@soufflé',
                '@sugar', '@sweet', '@topping', '@wafer'
            ],
            minimumCharacters: 1
        }]
    },
    // The "super-build" contains more premium features that require additional configuration, disable them below.
    // Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
    removePlugins: [
        // These two are commercial, but you can try them out without registering to a trial.
        // 'ExportPdf',
        // 'ExportWord',
        'CKBox',
        'CKFinder',
        'EasyImage',
        'RealTimeCollaborativeComments',
        'RealTimeCollaborativeTrackChanges',
        'RealTimeCollaborativeRevisionHistory',
        'PresenceList',
        'Comments',
        'TrackChanges',
        'TrackChangesData',
        'RevisionHistory',
        'Pagination',
        'WProofreader',
        'MathType',
        // The following features are part of the Productivity Pack and require additional license.
        'SlashCommand',
        'Template',
        'DocumentOutline',
        'FormatPainter',
        'TableOfContents'
    ]
}).then(editor_descripcion => {
    data_descripcion = editor_descripcion;
});


// Actualizacion de servicio
$(function () {
    'use strict';

    const forms = document.querySelectorAll('.store-noticias');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {

                var paqueteDeDatos = new FormData();
                paqueteDeDatos.append('imagen_noticia', $('#imagen_noticia')[0].files[0]);

                paqueteDeDatos.append('fecha_registro', $('#fecha_registro').prop('value'));
                paqueteDeDatos.append('fecha_terminacion', $('#fecha_terminacion').prop('value'));

                paqueteDeDatos.append('titulo_noticia', $('#titulo_noticia').prop('value'));
                paqueteDeDatos.append('resumen_noticia', $('#resumen_noticia').prop('value'));
                paqueteDeDatos.append('descripcion_noticia', data_descripcion.getData());

                paqueteDeDatos.append('type', $('#type').prop('value'));
                paqueteDeDatos.append('url_noticia', $('#url_noticia').prop('value'));
                paqueteDeDatos.append('categorias_noticia', $('#categorias_noticia').val());

                document.getElementById("imagen_noticia").disabled = true;
                document.getElementById("fecha_registro").disabled = true;
                document.getElementById("fecha_terminacion").disabled = true;
                document.getElementById("titulo_noticia").disabled = true;
                document.getElementById("resumen_noticia").disabled = true;
                document.getElementById("type").disabled = true;
                document.getElementById("url_noticia").disabled = true;
                document.getElementById("categorias_noticia").disabled = true;

                document.getElementById("submit").disabled = true;

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '/app/webpage/news/create/store',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire({
                                title: "Registro exitoso",
                                text: "La información se ha registrado correctamente",
                                icon: "success"
                            });

                            setTimeout(function () {
                                window.location = "/app/webpage/news";
                            }, 2000);

                        }
                    },
                    error: function (err) {
                        document.getElementById("imagen_noticia").disabled = false;
                        document.getElementById("fecha_registro").disabled = false;
                        document.getElementById("fecha_terminacion").disabled = false;
                        document.getElementById("titulo_noticia").disabled = false;
                        document.getElementById("resumen_noticia").disabled = false;
                        document.getElementById("type").disabled = false;
                        document.getElementById("url_noticia").disabled = false;
                        document.getElementById("categorias_noticia").disabled = false;

                        document.getElementById("submit").disabled = false;

                        errorHttp(err.status);

                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});