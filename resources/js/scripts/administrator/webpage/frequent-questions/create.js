$(document).ready(function () {
    $('.selector').select2();
});

var tabla_preguntas = $('.datatables-preguntasfrecuentes');

// Actualizacion de servicio
$(function () {
    'use strict';

    const forms = document.querySelectorAll('.store-preguntas');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {

                var paqueteDeDatos = new FormData();

                paqueteDeDatos.append('titulo_pregunta', $('#titulo_pregunta').prop('value'));
                paqueteDeDatos.append('respuesta_pregunta', $('#respuesta_pregunta').prop('value'));

                document.getElementById("titulo_pregunta").disabled = true;
                document.getElementById("respuesta_pregunta").disabled = true;
                document.getElementById("submit").disabled = true;

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '/app/webpage/frequent-questions/create/store',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire({
                                title: "Registro exitoso",
                                text: "La información se ha registrado correctamente",
                                icon: "success"
                            });

                            setTimeout(() => {
                                $("#modal-preguntasfrecuentes").modal(
                                    'hide');
                                var opciones = tabla_preguntas
                                    .DataTable();
                                opciones.ajax.reload();
                            }, 1000);
                        }
                    },
                    error: function (err) {
                        document.getElementById("titulo_pregunta").disabled = false;
                        document.getElementById("respuesta_pregunta").disabled = false;
                        document.getElementById("submit").disabled = false;

                        errorHttp(err.status);
                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});