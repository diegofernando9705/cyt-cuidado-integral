
$(function () {
    "use strict";

    var tabla_preguntasfrecuentes = $(".datatables-preguntasfrecuentes"),
        assetPath = "../../../app-assets/";

    if ($("body").attr("data-framework") === "laravel") {
        assetPath = $("body").attr("data-asset-path");
    }

    if (tabla_preguntasfrecuentes.length) {
        var dt_basic = tabla_preguntasfrecuentes.DataTable({
            ajax: "/app/webpage/frequent-questions/listado",
            columns: [
                { data: "vacio" },
                { data: "codigo_pregunta" },
                { data: "titulo_pregunta" },
                { data: "respuesta_pregunta" },
                { data: "nombre_estado" },
                { data: "" },
            ],
            columnDefs: [
                {
                    className: "control",
                    orderable: false,
                },
                {
                    // Actions
                    targets: -1,
                    title: "Acciones",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        if (full["codigo_estado"] == 1) {
                            return (
                                ' <button class="btn btn-success ver-preguntafrecuente btn-sm" title="Ver información" data-id="' +
                                full["codigo_pregunta"] +
                                '">' +
                                feather.icons["eye"].toSvg({ class: "font-small-4" }) +
                                "</button> " +
                                ' <button class="btn btn-info editar-preguntafrecuente btn-sm" title="Editar Información" data-id="' +
                                full["codigo_pregunta"] +
                                '">' +
                                feather.icons["file-text"].toSvg({ class: "font-small-4" }) +
                                "</button> " +
                                ' <button class="btn btn-danger eliminar-preguntafrecuente btn-sm" title="Eliminar Información" data-id="' +
                                full["codigo_pregunta"] +
                                '">' +
                                feather.icons["trash-2"].toSvg({ class: "font-small-4" }) +
                                "</button>"
                            );
                        } else {
                            return (
                                '<button class="btn btn-success ver-noticia btn-sm" title="Ver Información" disabled data-id="' +
                                full["codigo_noticia"] +
                                '">' +
                                feather.icons["eye"].toSvg({ class: "font-small-4" }) +
                                "</button>"
                            );
                        }
                    },
                },
            ],
            order: [[1, "asc"]],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return "Informacion completa para " + data["titulo_noticia"];
                        },
                    }),
                    type: "column",
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                "<td>" +
                                col.title +
                                ":" +
                                "</td> " +
                                "<td>" +
                                col.data +
                                "</td>" +
                                "</tr>"
                                : "";
                        }).join("");

                        return data ? $('<table class="table"/>').append(data) : false;
                    },
                },
            },
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
            },
        });
        $("div.head-label").html('<h6 class="mb-0">DataTable with Buttons</h6>');
    }

    $(document).on("click", ".ver-preguntafrecuente", function () {
        animationLoading("", "Cargando información...");

        $.ajax({
            type: "GET",
            url: "/app/webpage/frequent-questions/show/" + $(this).attr("data-id"),
            success: function (data) {

                $(".dtr-bs-modal").modal("hide");
                $("#modal-preguntasfrecuentes").modal("show");

                $("#titulo_modal").html("Información detallada");
                $("#contenido_modal").html(data);

                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".editar-preguntafrecuente", function () {
        animationLoading("", "Cargando información...");

        $.ajax({
            type: "GET",
            url: "/app/webpage/frequent-questions/edit/" + $(this).attr("data-id"),
            success: function (data) {

                $(".dtr-bs-modal").modal("hide");
                $("#modal-preguntasfrecuentes").modal("show");

                $("#titulo_modal").html("Información detallada");
                $("#contenido_modal").html(data);

                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".eliminar-preguntafrecuente", function () {
        $(".modal").modal("hide");
        Swal.fire({
            title: "¿Desea eliminar el registro?",
            text: "No podrás revertir esto",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "Cancelar",
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/app/preguntasfrecuentes/delete/" + $(this).attr("data-id"),
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire(
                                "Eliminado!",
                                "La información ha sido eliminada.",
                                "success"
                            );

                            setTimeout(() => {
                                var opciones = tabla_preguntasfrecuentes.DataTable();
                                opciones.ajax.reload();
                            }, 2000);
                        }
                    },
                    error: function (err) {
                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $("#success_message").fadeIn().html(err.responseJSON.message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' + i + '"]');
                                el.after(
                                    $(
                                        '<span class="errores" style="color: red;">' +
                                        error[0] +
                                        "</span>"
                                    )
                                );
                            });
                        } else {
                            $("#modal-services").modal("hide");
                            if (err.status == 403) {
                                Swal.fire({
                                    allowOutsideClick: false,
                                    icon: "error",
                                    title: "Acción restringida",
                                    html:
                                        "Usted no tiene permisos para esta acción. Comuniquese con administración. <br><br> <b>CÓD. ERROR: " +
                                        err.status +
                                        "</b>",
                                    footer:
                                        "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>",
                                });
                            } else {
                                Swal.fire({
                                    allowOutsideClick: false,
                                    icon: "error",
                                    title: "Error desconocido",
                                    html:
                                        "Comunicate con soporte. <br> <b>CÓD. ERROR: " +
                                        err.status +
                                        "</b>",
                                    footer:
                                        "<a href='https://acortar.link/U7TKJH' target='_blank'>Comunicarme con soporte.</a>",
                                });
                            }
                        }
                    },
                });
            }
        });
    });

    $(document).on("click", "#agregar_pregunta", function () {
        animationLoading("", "Cargando información...");

        $.ajax({
            type: 'GET',
            url: '/app/webpage/frequent-questions/create',
            success: function (data) {
                $(".dtr-bs-modal").modal("hide");
                $("#modal-preguntasfrecuentes").modal("show");
                $("#titulo_modal").html('Crear una nueva pregunta');
                $("#contenido_modal").html(data);

                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });
});