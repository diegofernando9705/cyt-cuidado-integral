
$(function () {
    "use strict";

    var tabla_cotizadoresdictamenes = $(".datatables-cotizadores-dictamenes"),
        assetPath = "../../../app-assets/";

    if ($("body").attr("data-framework") === "laravel") {
        assetPath = $("body").attr("data-asset-path");
    }


    /* |||||||||||||||| COTIZADORES DE DICTAMENES |||||||||||||||| */

    if (tabla_cotizadoresdictamenes.length) {
        var dt_basic = tabla_cotizadoresdictamenes.DataTable({
            ajax: "/app/webpage/quote-opinions/listado",
            columns: [
                { data: "vacio" },
                { data: "codigo_cotizacion_dictamen" },
                { data: "nombres_apellidos" },
                { data: "correo_electronico" },
                { data: "telefono_celular" },
                { data: "ciudad" },
                { data: "" },
            ],
            columnDefs: [
                {
                    className: "control",
                    orderable: false,
                },
                {
                    // Actions
                    targets: -1,
                    title: "Acciones",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return (
                            '<button class="btn btn-success ver-dictamen btn-sm" title="Ver Información" data-id="' +
                            full["codigo_cotizacion_dictamen"] +
                            '">' +
                            feather.icons["eye"].toSvg({ class: "font-small-4" }) +
                            "</button>"
                        );
                    },
                },
            ],
            order: [[1, "asc"]],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return "Informacion completa para " + data["titulo_noticia"];
                        },
                    }),
                    type: "column",
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                "<td>" +
                                col.title +
                                ":" +
                                "</td> " +
                                "<td>" +
                                col.data +
                                "</td>" +
                                "</tr>"
                                : "";
                        }).join("");

                        return data ? $('<table class="table"/>').append(data) : false;
                    },
                },
            },
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
            },
        });
        $("div.head-label").html('<h6 class="mb-0">DataTable with Buttons</h6>');
    }

    $(document).on("click", ".ver-dictamen", function () {
        animationLoading("", "Cargando información...");
        $.ajax({
            type: "GET",
            url: "/app/webpage/quote-opinions/show/" + $(this).attr("data-id"),
            success: function (data) {
                $(".dtr-bs-modal").modal("hide");
                $("#modal-cotizador-dictamen").modal("show");
                $("#titulo_modal").html("Ver Información");
                $("#contenido_modal").html(data);
                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".eliminar-dictamen", function () {
        $(".modal").modal("hide");
        Swal.fire({
            title: "¿Desea eliminar el registro?",
            text: "No podrás revertir esto",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "Cancelar",
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/app/webpage/quote-opinions/delete/" + $(this).attr("data-id"),
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire(
                                "Eliminado!",
                                "La información ha sido eliminada.",
                                "success"
                            );

                            setTimeout(() => {
                                var opciones = tabla_cotizadoresdictamenes.DataTable();
                                opciones.ajax.reload();
                            }, 2000);
                        }
                    },
                    error: function (err) {
                        errorHttp(err.status);
                    },
                });
            }
        });
    });

    /* |||||||||||||||| FIN COTIZADORES DE DICTAMENES |||||||||||||||| */

});
