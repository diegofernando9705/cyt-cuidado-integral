$(document).on("click", ".encabezado", function() {
    var contenedor = "#opciones_header_" + $(this).attr("data-slug");
    $(contenedor).html("Buscando...");
    $.ajax({
        type: 'GET',
        url: '/app/webpage/banner/general/slug/' + $(this).attr("data-slug"),
        success: function(data) {
            $(contenedor).html(data);
        },
        error: function(error) {
            console.log(error);
        },
    });
});

$(document).on("change", ".selector_encabezado", function() {
    var contenedor = "#opciones_header_" + $(this).attr("data-slug");
    $(contenedor).html("Configurando vista...");
    
    $.ajax({
        type: 'GET',
        url: '/app/webpage/banner/general/slug/update/' + $(this).attr("data-slug") + "/" + $(this).val(),
        success: function(data) {
            $(contenedor).html(data);
        },
        error: function(error) {
            console.log(error);
        },
    });

});