var tabla_categorias = $('.datatables-categorias');
var changePicture = $('#imagen_categoria'),
    userAvatar = $('.user-avatar');

// Change user profile picture
if (changePicture.length) {
    $(changePicture).on('change', function (e) {
        var reader = new FileReader(),
            files = e.target.files;
        reader.onload = function () {
            if (userAvatar.length) {
                userAvatar.attr('src', reader.result);
            }
        };
        reader.readAsDataURL(files[0]);
    });
}


$(function () {
    'use strict';

    const forms = document.querySelectorAll('.update-categorias');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {

                var paqueteDeDatos = new FormData();
                paqueteDeDatos.append('imagen_categoria', $('#imagen_categoria')[0].files[
                    0]);

                paqueteDeDatos.append('titulo_categoria', $('#titulo_categoria').prop('value'));
                paqueteDeDatos.append('descripcion_categoria', $('#descripcion_categoria').prop('value'));
                paqueteDeDatos.append('estado_categoria', $('#estado_categoria').prop('value'));

                document.getElementById("titulo_categoria").disabled = true;
                document.getElementById("descripcion_categoria").disabled = true;
                document.getElementById("estado_categoria").disabled = true;
                document.getElementById("submit").disabled = true;

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });


                $.ajax({
                    type: 'POST',
                    url: '/app/webpage/categories/edit/update/' + $('#code').prop('value'),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data == true) {

                            Swal.fire({
                                title: "Actualización exitosa",
                                text: "La información se ha actualizado correctamente",
                                icon: "success"
                            });

                            setTimeout(() => {
                                $("#modal-categorias").modal('hide');
                                var opciones = tabla_categorias.DataTable();
                                opciones.ajax.reload();
                            }, 1000);

                        }
                    },
                    error: function (err) {
                        document.getElementById("titulo_categoria").disabled = false;
                        document.getElementById("descripcion_categoria").disabled = false;
                        document.getElementById("estado_categoria").disabled = false;
                        document.getElementById("submit").disabled = false;

                        errorHttp(err.status);

                        if (err.status == 422) {
                            document.getElementById('submit').disabled = false;

                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();

            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});