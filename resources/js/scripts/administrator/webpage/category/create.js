$(function () {
    'use strict';
    $('.selector').select2();

    var changePicture = $('#imagen_categoria'),
        userAvatar = $('.user-avatar');

    // Change user profile picture
    if (changePicture.length) {
        $(changePicture).on('change', function (e) {
            var reader = new FileReader(),
                files = e.target.files;
            reader.onload = function () {
                if (userAvatar.length) {
                    userAvatar.attr('src', reader.result);
                }
            };
            reader.readAsDataURL(files[0]);
        });
    }

    const forms = document.querySelectorAll('.store-categorias');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {

                var paqueteDeDatos = new FormData();
                paqueteDeDatos.append('imagen_categoria', $('#imagen_categoria')[0].files[0]);

                paqueteDeDatos.append('titulo_categoria', $('#title').prop('value'));
                paqueteDeDatos.append('descripcion_categoria', $('#description').prop('value'));

                document.getElementById("title").disabled = true;
                document.getElementById("description").disabled = true;
                document.getElementById("submit").disabled = true;

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '/app/webpage/categories/create/store',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire({
                                title: "Registro exitoso",
                                text: "La información se ha registrado correctamente",
                                icon: "success"
                            });

                            setTimeout(function () {
                                window.location = "/app/webpage/categories";
                            }, 1000);
                        }
                    },
                    error: function (err) {
                        document.getElementById("title").disabled = false;
                        document.getElementById("description").disabled = false;
                        document.getElementById("submit").disabled = false;

                        errorHttp(err.status);
                        if (err.status == 422) {
                            document.getElementById('submit').disabled = false;

                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});