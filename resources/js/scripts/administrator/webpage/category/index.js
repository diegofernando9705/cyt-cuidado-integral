
$(function () {
    "use strict";

    var tabla_categorias = $(".datatables-categorias"),
        assetPath = "../../../app-assets/";

    if ($("body").attr("data-framework") === "laravel") {
        assetPath = $("body").attr("data-asset-path");
    }


    if (tabla_categorias.length) {
        var dt_basic = tabla_categorias.DataTable({
            ajax: "/app/webpage/categories/listado",
            columns: [
                { data: "vacio" },
                { data: "codigo_categoria" },
                {
                    data: "imagen_categoria",
                    render: function (data, type, row, meta) {
                        if (data) {
                            return (
                                "<img src='https://s3.amazonaws.com/cnp.com.co/" +
                                data +
                                "' width='80px' title=''>"
                            );
                        } else {
                            return (
                                "<img src='https://s3.amazonaws.com/cnp.com.co/" +
                                data +
                                "' width='80px' title=''>"
                            );
                        }
                    },
                },
                { data: "titulo_categoria" },
                { data: "descripcion_categoria" },
                { data: "nombre_estado" },
                { data: "" },
            ],
            columnDefs: [
                {
                    className: "control",
                    orderable: false,
                },
                {
                    // Actions
                    targets: -1,
                    title: "Acciones",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        if (full["codigo_estado"] == 1) {
                            return (
                                ' <button class="btn btn-info editar-categoria btn-sm" title="Editar Información" data-id="' +
                                full["codigo_categoria"] +
                                '">' +
                                feather.icons["file-text"].toSvg({ class: "font-small-4" }) +
                                "</button> " +
                                ' <button class="btn btn-danger eliminar-categoria btn-sm" title="Eliminar Información" data-id="' +
                                full["codigo_categoria"] +
                                '">' +
                                feather.icons["trash-2"].toSvg({ class: "font-small-4" }) +
                                "</button>"
                            );
                        } else {
                            return (
                                '<button class="btn btn-success ver-categoria btn-sm" title="Ver Información" disabled data-id="' +
                                full["codigo_categoria"] +
                                '">' +
                                feather.icons["eye"].toSvg({ class: "font-small-4" }) +
                                "</button>"
                            );
                        }
                    },
                },
            ],
            order: [[1, "asc"]],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return "Informacion completa para " + data["primer_nombre"];
                        },
                    }),
                    type: "column",
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                "<td>" +
                                col.title +
                                ":" +
                                "</td> " +
                                "<td>" +
                                col.data +
                                "</td>" +
                                "</tr>"
                                : "";
                        }).join("");

                        return data ? $('<table class="table"/>').append(data) : false;
                    },
                },
            },
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
            },
        });
        $("div.head-label").html('<h6 class="mb-0">DataTable with Buttons</h6>');
    }

    $(document).on("click", ".editar-categoria", function () {
        animationLoading("", "Cargando información...");

        $.ajax({
            type: "GET",
            url: "/app/webpage/categories/edit/" + $(this).attr("data-id"),
            success: function (data) {
                $(".dtr-bs-modal").modal("hide");
                $("#modal-categorias").modal("show");
                $("#titulo_modal").html("Actualizacion de Información");
                $("#contenido_modal").html(data);

                Swal.close();
            },
            error: function (error) {
                errorHttp(error.status);
            },
        });
    });

    $(document).on("click", ".eliminar-categoria", function () {
        $(".modal").modal("hide");
        Swal.fire({
            title: "¿Desea eliminar el registro?",
            text: "No podrás revertir esto",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "Cancelar",
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/app/webpage/categories/delete/" + $(this).attr("data-id"),
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire(
                                "Eliminado",
                                "La información ha sido eliminada.",
                                "success"
                            );

                            setTimeout(() => {
                                var opciones = tabla_categorias.DataTable();
                                opciones.ajax.reload();
                            }, 2000);
                        }
                    },
                    error: function (err) {
                        errorHttp(err.status);
                    },
                });
            }
        });
    });
});