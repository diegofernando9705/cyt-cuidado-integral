$(function () {
    'use strict';

    const forms = document.querySelectorAll('.store-services');

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {
                document.getElementById('url_unica').disabled = true;
                document.getElementById('titulo_service').disabled = true;
                document.getElementById('resena_service').disabled = true;
                document.getElementById('body_service').disabled = true;

                document.getElementById("submit").disabled = true;


                var paqueteDeDatos = new FormData();
                paqueteDeDatos.append('change-picture', $('#imagen_service')[0].files[0]);

                paqueteDeDatos.append('url_unica', $('#url_unica').prop('value'));
                paqueteDeDatos.append('titulo_service', $("#titulo_service").prop('value'));
                paqueteDeDatos.append('resena_service', $("#resena_service").prop('value'));
                paqueteDeDatos.append('body_service', data_descripcion_completa.getData());

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });


                $.ajax({
                    type: 'POST',
                    url: '/app/webpage/servicios/create/store',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data == true) {
                            Swal.fire({
                                title: "Registro exitoso",
                                text: "La información se ha registrado correctamente",
                                icon: "success"
                            });

                            setTimeout(function () {
                                window.location = "/app/webpage/servicios";
                            }, 2000);

                        }
                    },
                    error: function (err) {
                        document.getElementById('url_unica').disabled = false;
                        document.getElementById('titulo_service').disabled = false;
                        document.getElementById('resena_service').disabled = false;
                        document.getElementById('body_service').disabled = false;

                        document.getElementById("submit").disabled = false;

                        errorHttp(err.status);
                        if (err.status == 422) {
                            document.getElementById('submit').disabled = false;

                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();

            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
});