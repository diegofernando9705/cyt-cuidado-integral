@php
    $configData = Helper::applClasses();
@endphp
@extends('administrator.layouts.fullLayoutMaster')

@section('title', 'Restablecer contraseña')

@section('page-style')
    {{-- Page Css files --}}

    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">

    <style type="text/css">

    </style>
@endsection

@section('content')
    <div class="auth-wrapper auth-v2">
        <div class="auth-inner row m-0">
            <!-- Brand logo-->
            <a class="brand-logo" href="javascript:void(0);">
                <img src="https://s3.amazonaws.com/cytcuidadointegral.com/CYT-LOGO.jpeg" width="80px">
            </a>
            <!-- /Brand logo-->
            <!-- Left Text-->
            <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
                <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                    @if ($configData['theme'] === 'dark')
                        <img class="img-fluid" src="{{ asset('images/pages/forgot-password-v2-dark.svg') }}"
                            alt="Forgot password V2" />
                    @else
                        <img class="img-fluid" src="{{ asset('images/pages/forgot-password-v2.svg') }}"
                            alt="Forgot password V2" />
                    @endif
                </div>
            </div>
            <!-- /Left Text-->
            <!-- Forgot password-->
            <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
                <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                    <h2 class="card-title font-weight-bold mb-1">Olvidaste tu contraseña? 🔒</h2>
                    <p class="card-text mb-2">Ingrese su correo electrónico y le enviaremos instrucciones para restablecer
                        su contraseña</p>
                    <form class="auth-forgot-password-form mt-2" action="/reset-password-v2" method="POST">
                        @csrf

                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="form-label" for="forgot-password-email">Email</label>
                            <input class="form-control" id="forgot-password-email" type="text" name="email"
                                placeholder="john@example.com" aria-describedby="forgot-password-email" autofocus=""
                                tabindex="1" value="{{ $email ?? old('email') }}" />
                            <span class="text-danger">
                                @error('email')
                                    {{ $message }}
                                @enderror
                            </span>
                        </div>
                        <button class="btn btn-primary btn-block" tabindex="2">Enviar </button>
                    </form>
                    <p class="text-center mt-2">
                        <a href="{{ url('login') }}">
                            <i data-feather="chevron-left"></i>Regresar al inicio
                        </a>
                    </p>
                </div>
            </div>
            <!-- /Forgot password-->
        </div>
    </div>
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset(mix('js/scripts/pages/page-auth-forgot-password.js')) }}"></script>
@endsection
