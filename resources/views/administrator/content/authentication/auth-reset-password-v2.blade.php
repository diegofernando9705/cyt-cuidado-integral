@php
$configData = Helper::applClasses();
@endphp
@extends('administrator.layouts.fullLayoutMaster')

@section('title', 'Reset Password')

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
@endsection

@section('content')
<div class="auth-wrapper auth-v2">
  <div class="auth-inner row m-0">
    <!-- Brand logo-->
    <a class="brand-logo" href="javascript:void(0);">
      <img src="https://s3.amazonaws.com/cytcuidadointegral.com/CYT-LOGO.jpeg" width="80px">
    </a>
    <!-- /Brand logo-->
    <!-- Left Text-->
    <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
      <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
        @if($configData['theme'] === 'dark')
         <img src="{{asset('images/pages/reset-password-v2-dark.svg')}}" img-fluid="img-fluid" alt="Register V2" />
        @else
         <img src="{{asset('images/pages/reset-password-v2.svg')}}" img-fluid="img-fluid" alt="Register V2" />
        @endif
      </div>
    </div>
    <!-- /Left Text-->
    <!-- Reset password-->
    <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
      <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
        <h2 class="card-title font-weight-bold mb-1">Restablecer contraseña 🔒</h2>
        <p class="card-text mb-2">Su nueva contraseña debe ser diferente de las contraseñas utilizadas anteriormente.</p>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="padding-top: 5px; padding-bottom: 5px;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="auth-reset-password-form mt-2" action="/restablecer/data" method="POST">
          @csrf
          <input type="hidden" name="token" value="{{ $token }}">
          <div class="form-group">
            <div class="d-flex justify-content-between">
              <label for="reset-password-new">Nueva contraseña</label>
            </div>
            <div class="input-group input-group-merge form-password-toggle">
              <input class="form-control form-control-merge" id="reset-password-new" type="password" name="password" placeholder="············" aria-describedby="reset-password-new" autofocus="" tabindex="1" />
                <div class="input-group-append">
                  <span class="input-group-text cursor-pointer">
                    <i data-feather="eye"></i>
                  </span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="d-flex justify-content-between">
                <label for="reset-password-confirm">Confirmar contraseña</label>
              </div>
              <div class="input-group input-group-merge form-password-toggle">
                <input class="form-control form-control-merge" id="reset-password-confirm" type="password" name="c_password" placeholder="············" aria-describedby="reset-password-confirm" tabindex="2" />
                <div class="input-group-append">
                  <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary btn-block" tabindex="3">Establecer nueva contraseña</button>
          </form>
          <!--
          <p class="text-center mt-2">
            <a href="{{url('auth/login-v2')}}">
              <i data-feather="chevron-left"></i> Back to login
            </a>
          </p>-->
      </div>
    </div>
    <!-- /Reset password-->
  </div>
</div>
@endsection

@section('vendor-script')
<script src="{{asset(mix('vendors/js/forms/validation/jquery.validate.min.js'))}}"></script>
@endsection

@section('page-script')
<script src="{{asset(mix('js/scripts/pages/page-auth-reset-password.js'))}}"></script>
@endsection
