@php
$configData = Helper::applClasses();
@endphp
@extends('administrator.layouts.fullLayoutMaster')

@section('title', 'Inicio de sesión')

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
@endsection

@section('content')
<div class="auth-wrapper auth-v2">
  <div class="auth-inner row m-0">
      <!-- Brand logo-->
      <a class="brand-logo" href="javascript:void(0);">
        <img src="https://s3.amazonaws.com/cytcuidadointegral.com/CYT-LOGO.jpeg" width="80px">
      </a>
      <!-- /Brand logo-->
      <!-- Left Text-->
      <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
        <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
          @if($configData['theme'] === 'dark')
          <img class="img-fluid" src="https://s3.amazonaws.com/cytcuidadointegral.com/SISTEMA+DE+INFORMACION/LOGO-HORIZONTAL.png" alt="Login V2" />
          @else
          <img class="img-fluid" src="https://s3.amazonaws.com/cytcuidadointegral.com/SISTEMA+DE+INFORMACION/LOGO-HORIZONTAL.png" alt="Login V2" />
          @endif
        </div>
      </div>
      <!-- /Left Text-->
      <!-- Login-->
      <div class="d-flex col-lg-4 align-items-center auth-bg px-2  p-lg-5">
        <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
          <h2 class="card-title font-weight-bold mb-1">Bienvenido a C&T Cuidado integral! &#x1F44B;</h2>
          <p class="card-text mb-2">Ingresa tus credencias para acceder al sistema</p>
          @if(isset($_GET['status']) and $_GET['status'] == 'true')
            <div class="alert alert-success" style="padding-top: 10px; padding-bottom: 10px; padding-left: 10px;">
                <p>Se restablecio tu contraseña correctamente</p>
            </div>
          @elseif(isset($_GET['status']) and $_GET['status'] == 'email-send')
              <div class="alert alert-success" style="padding-top: 10px; padding-bottom: 10px; padding-left: 10px;">
                <p>Se envió un link de restablecimiento de contraseña a su correo electronico registrado en el sistema</p>
            </div>
          @elseif(isset($_GET['status']) and $_GET['status'] == 'email-no-existe')
              <div class="alert alert-danger" style="padding-top: 10px; padding-bottom: 10px; padding-left: 10px;">
                <p>El correo electronico no esta asociado con ningun usuario</p>
            </div>
          @else
          @endif
          @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="padding-top: 5px; padding-bottom: 5px;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          <form class="formulario-login" novalidate>
            @csrf
            @method('POST')
            <div id="mensajes"></div>
            <div class="form-group">
              <div class="form-outline">
                <label class="form-label" for="login-email">Correo electr&oacute;nico</label>
                <input type="email" class="form-control" id="login-email" name="email" required />
                <div class="invalid-feedback">Por favor, ingrese un correo electrónico válido.</div>
                <div class="valid-feedback"></div>
              </div>
            </div>

            <div class="form-group">
              <div class="form-outline">
                <div class="d-flex justify-content-between">
                  <label for="login-password">Contraseña</label>
                  <a href="{{route('auth-forgot-password-v2')}}">
                    <small>Olvido su contraseña?</small>
                  </a>
                </div>
                <input type="password" class="form-control" id="login-password" name="password" required />
                <div class="invalid-feedback">Por favor, ingrese su contraseña personal.</div>
                <div class="valid-feedback"></div>
              </div>
            </div>

            <!--
            <div class="form-group">
              <div div class="custom-control custom-checkbox">
                <input class="custom-control-input" id="remember-me" type="checkbox" tabindex="3" />
                <label class="custom-control-label" for="remember-me">Remember Me</label>
              </div>
            </div>-->
            <button type='submit' class="btn btn-primary btn-block" name='submit' id='submit' tabindex="4">Entrar al sistema</button>
            <div class="form-outline">
              <center>
                <label class="form-label" for="login-email">Desarrollado por <b>Softworld Colombia</b></label>
              </center>
            </div>
          </form>
          <!--
          <p class="text-center mt-2">
            <span>New on our platform?</span>
            <a href="{{url('auth/register-v2')}}"><span>&nbsp;Create an account</span></a>
          </p>

          <div class="divider my-2">
            <div class="divider-text">or</div>
          </div>
          <div class="auth-footer-btn d-flex justify-content-center">
            <a class="btn btn-facebook" href="javascript:void(0)">
              <i data-feather="facebook"></i>
            </a>
            <a class="btn btn-twitter white" href="javascript:void(0)">
              <i data-feather="twitter"></i>
            </a>
            <a class="btn btn-google" href="javascript:void(0)">
              <i data-feather="mail"></i>
            </a>
            <a class="btn btn-github" href="javascript:void(0)">
              <i data-feather="github"></i>
            </a>
          </div>-->
      </div>
    </div>
    <!-- /Login-->
  </div>
</div>
@endsection

@section('vendor-script')
<script src="{{asset(mix('vendors/js/forms/validation/jquery.validate.min.js'))}}"></script>
@endsection

@section('page-script')
<script src="{{asset(mix('js/scripts/pages/page-auth-login.js'))}}"></script>
@endsection
