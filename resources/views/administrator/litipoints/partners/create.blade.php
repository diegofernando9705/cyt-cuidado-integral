@extends('administrator.layouts.contentLayoutMaster')
@section('title', 'Registrar socio')

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
            <!-- Bootstrap Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-body">
                        <div id="mensajes"></div>
                        <form class="store-socios-premios" novalidate>
                            @csrf
                            @method('POST')
                            <div class="media mb-2">
                                <img src="https://s3.amazonaws.com/cnp.com.co/textocnp.png" alt="users avatar"
                                    class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer"
                                    width="150" />
                                <div class="media-body mt-50">
                                    <h4>Imagen o logo del socio</h4>
                                    <div class="col-12 d-flex mt-1 px-0">
                                        <label class="btn btn-primary mr-75 mb-0" for="logo_socio">
                                            <span class="d-none d-sm-block">Subir foto</span>
                                            <input class="form-control" type="file" id="logo_socio" name="logo_socio"
                                                hidden accept="image/png, image/jpeg, image/jpg" name="avatar" required />
                                            <span class="d-block d-sm-none">
                                                <i class="mr-0" data-feather="edit"></i>
                                            </span>
                                            <div class="invalid-feedback" style="color: white;">Por favor, seleccione una
                                                imagen o logo para el socio.
                                            </div>
                                        </label>

                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="partner_title"><b>(*) Nombre del socio</b></label>
                                        <input class="form-control partner_title" name="partner_title" id="partner_title"
                                            required>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="partner_description"><b>Descripción del socio</b></label>
                                        <textarea class="form-control partner_description" name="partner_description" id="partner_description"></textarea>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-12"
                                    style="padding-top: 20px; margin-top: 20px; border-top: 1px solid #b8c2cc;">

                                </div>
                                <ul class="nav nav-pills" role="tablist">
                                    <li class="nav-item btn-primary">
                                        <a class="nav-link d-flex align-items-center active btn-primary" id="account-tab"
                                            data-toggle="tab" href="#account" aria-controls="account" role="tab"
                                            aria-selected="true">
                                            <span class="d-sm-block">Persona de contacto</span>
                                        </a>
                                    </li>
                                </ul>

                                <div class="col-md-12">
                                </div>

                                <div class="row col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="form-group col">
                                        <label for="person_contact_names"><b>(*) Nombres de la persona</b></label>
                                        <input type="text" class="form-control" name="person_contact_names"
                                            id="person_contact_names" required>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                    <div class="form-group col">
                                        <label for="person_contact_last_names"><b>(*) Apellidos de la persona</b></label>
                                        <input type="text" class="form-control" name="person_contact_last_names"
                                            id="person_contact_last_names" required>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>


                                <div class="row col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="form-group col">
                                        <label for="person_contact_cellphone"><b>(*) Celular de la persona</b></label>
                                        <input type="text" class="form-control" name="person_contact_cellphone"
                                            id="person_contact_cellphone" required>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                    <div class="form-group col">
                                        <label for="person_contact_email"><b>(*) Correo de la persona</b></label>
                                        <input type="email" class="form-control" name="person_contact_email"
                                            id="person_contact_email" required>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-12 d-flex flex-sm-row flex-column mt-2">
                                    <button type="submit" id="submit"
                                        class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">Registrar
                                        socio</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset(mix('js/scripts/administrator/litipoints/partners/create.js')) }}"></script>
@endsection
