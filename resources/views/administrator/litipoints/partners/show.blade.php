
<div class="media mb-2">
    <img src="https://s3.amazonaws.com/cnp.com.co/{{ $data->partner_image }}" alt="users avatar"
        class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="150" />
    <div class="media-body mt-50">
        <h4>Imagen o logo del socio</h4>
        <div class="col-12 d-flex mt-1 px-0">

        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-12">
        <div class="form-group">
            <label for="partner_title"><b>(*) Nombre del socio</b></label>
            <input class="form-control partner_title" disabled value="{{ $data->partner_title }}">
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label for="partner_description"><b>Descripción del socio</b></label>
            <textarea class="form-control partner_description" disabled>{{ $data->partner_description }}</textarea>
        </div>
    </div>

    <div class="col-12" style="padding-top: 20px; margin-top: 20px; border-top: 1px solid #b8c2cc;">

    </div>
    <ul class="nav nav-pills" role="tablist">
        <li class="nav-item btn-primary">
            <a class="nav-link d-flex align-items-center active btn-primary" id="account-tab" data-toggle="tab"
                href="#account" aria-controls="account" role="tab" aria-selected="true">
                <span class="d-sm-block">Persona de contacto</span>
            </a>
        </li>
    </ul>

    <div class="col-md-12">
    </div>

    <div class="row col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="form-group col">
            <label for="person_contact_names"><b>Nombres de la persona</b></label>
            <input type="text" class="form-control" disabled value="{{ $data->person_contact_names }}">
        </div>
        <div class="form-group col">
            <label for="person_contact_last_names"><b>Apellidos de la persona</b></label>
            <input type="text" class="form-control" disabled value="{{ $data->person_contact_last_names }}">
        </div>
    </div>


    <div class="row col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="form-group col">
            <label for="person_contact_cellphone"><b>Celular de la persona</b></label>
            <input type="text" class="form-control" disabled value="{{ $data->person_contact_cellphone }}">
        </div>
        <div class="form-group col">
            <label for="person_contact_email"><b>Correo de la persona</b></label>
            <input type="text" class="form-control" disabled value="{{ $data->person_contact_email }}">
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label for="partner_title"><b>(*) Estado</b></label>
            <input class="form-control partner_title" disabled value="{{ $data->getStatus->description }}">
        </div>
    </div>

</div>
