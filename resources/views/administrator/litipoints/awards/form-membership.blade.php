<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">

<style>
    .select2-selection__arrow {
        display: none;
    }
</style>

<div class="row">
    <form class="update_membership_premio col-12 col-sm-12 col-md-12 col-lg-12" novalidate>
        @csrf
        @method('POST')

        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
            <label for="">Selecciona la membresia que desea asignar:</label>
            <select class="form-control selector" multiple id="membership" name="membership" required>
                @foreach ($membresias as $data_membresia)
                    @if (is_array($array_membresias_seleccionadas) && in_array($data_membresia->code, $array_membresias_seleccionadas))
                        <option value="{{ $data_membresia->code }}" selected>{{ $data_membresia->title }}</option>
                    @else
                        <option value="{{ $data_membresia->code }}">{{ $data_membresia->title }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="boton col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="text-align:center;">
            <button type="submit" class="btn btn-sm btn-primary" id="submit">Asignar membresias</button>
        </div>
    </form>
</div>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>

<script>
    tabla_premios = $(".datatables-premios");
    $(document).ready(function() {
        $(".selector").select2();
    });

    // Actualizacion de servicio
    $(function() {
        'use strict';

        const forms = document.querySelectorAll('.update_membership_premio');

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms).forEach((form) => {
            form.addEventListener('submit', (event) => {

                if (!form.checkValidity()) {
                    event.stopPropagation();
                } else {
                    document.getElementById("submit").disabled = true;

                    var paqueteDeDatos = new FormData();

                    paqueteDeDatos.append('membership', $('#membership').val());
                    document.getElementById("membership").disabled = true;


                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                        }
                    });

                    $.ajax({
                        type: 'POST',
                        url: '/app/litipoints/awards/form/membership/update/{{ $codigo_premio }}',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: paqueteDeDatos,
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            if (data == true) {
                                Swal.fire({
                                    title: "Actualización exitosa",
                                    text: "La información se ha actualizado correctamente",
                                    icon: "success"
                                });

                                setTimeout(() => {
                                    $("#modal-premios").modal('hide');
                                    var opciones = tabla_premios
                                        .DataTable();
                                    opciones.ajax.reload();
                                }, 2000);
                            }
                        },
                        error: function(err) {
                            errorHttp(err.status);
                        },
                    });
                    event.preventDefault();
                }
                event.preventDefault();
                form.classList.add('was-validated');
            }, false);
        });
    });
</script>
