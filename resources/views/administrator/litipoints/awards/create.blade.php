@extends('administrator.layouts.contentLayoutMaster')
@section('title', 'Registrar premio')

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">
@endsection

@section('content')
    <section class="bs-validation">
        <div class="row">
            <!-- Bootstrap Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-body">
                        <div id="mensajes"></div>
                        <form class="store-premios" novalidate>
                            @csrf
                            @method('POST')

                            <div class="media mb-2">
                                <img src="https://s3.amazonaws.com/cnp.com.co/textocnp.png" alt="users avatar"
                                    class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer"
                                    width="150" />
                                <div class="media-body mt-50">
                                    <h4>Imagen del premio</h4>
                                    <div class="col-12 d-flex mt-1 px-0">
                                        <label class="btn btn-primary mr-75 mb-0" for="imagen_premio">
                                            <span class="d-none d-sm-block">Subir foto</span>
                                            <input class="form-control" type="file" id="imagen_premio" name="imagen_premio"
                                                hidden accept="image/png, image/jpeg, image/jpg" name="avatar" required />
                                            <span class="d-block d-sm-none">
                                                <i class="mr-0" data-feather="edit"></i>
                                            </span>
                                            <div class="invalid-feedback" style="color: white;">Por favor, seleccione una
                                                imagen de portada.
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col">
                                    <label for="start_date"><b>(*) Fecha de inicio</b></label>
                                    <input type="date" class="form-control start_date"
                                        name="start_date" id="start_date" required>
                                    <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                </div>
                                <div class="form-group col">
                                    <label for="end_date"><b>(*) Fecha de vencimiento</b></label>
                                    <input type="date" class="form-control end_date" name="end_date"
                                        id="end_date" required>
                                    <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="award_title"><b>(*) Titulo del premio</b></label>
                                        <input class="form-control award_title" name="award_title" id="award_title"
                                            required>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="award_description"><b>(*) Descripción del premio</b></label>
                                        <textarea class="form-control award_description" name="award_description" id="award_description" required></textarea>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col">
                                    <label for="litipoint_quantity"><b>Puntos otorgados</b></label>
                                    <input type="number" class="form-control litipoint_quantity"
                                        name="litipoint_quantity" id="litipoint_quantity">
                                </div>

                                <div class="form-group col">
                                    <label for="availability"><b>Cantidad disponible</b></label>
                                    <input type="number" class="form-control availability"
                                        name="availability" id="availability">
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="partners"><b>Socios</b></label>
                                        <select class="form-control selector" multiple name="partners" id="partners">
                                            @foreach ($socios as $socio)
                                                <option value="{{ $socio->partner_code }}">{{ $socio->partner_title }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">Por favor, seleccione un estado para el socio.
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-12 d-flex flex-sm-row flex-column mt-2">
                                    <button type="submit" id="submit"
                                        class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">Registrar premio</button>
                                </div>
                            </div>
                        </form>

                        <!-- Basic toast -->
                        <div class="toast toast-basic hide position-fixed registro-exitoso" role="alert"
                            aria-live="assertive" aria-atomic="true" data-delay="5000" style="top: 1rem; right: 1rem"
                            id="registro-exitoso">
                            <div class="toast-header bg-success" style="color: white;">
                                <strong class="mr-auto">Notificación</strong>
                                <small class="text-muted">Justo ahora</small>
                                <button type="button" class="ml-1 close" data-dismiss="toast" aria-label="Close"
                                    style="color: white;">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="toast-body">¡Felicidades, registro exitoso!</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="//cdn.ckeditor.com/4.20.1/full/ckeditor.js"></script>
    <script src="https://s3.amazonaws.com/cnp.com.co/tagsinput.js"></script>
    <script src="{{ asset(mix('js/scripts/administrator/litipoints/awards/create.js')) }}"></script>
@endsection
