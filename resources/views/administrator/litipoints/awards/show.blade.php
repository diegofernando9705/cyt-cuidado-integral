<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">

<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">

<div class="media mb-2">
    <img src="https://s3.amazonaws.com/cnp.com.co/{{ $data->award_image }}" alt="users avatar"
        class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="150" />
    <div class="media-body mt-50">
        <h4>Imagen del premio</h4>
        <div class="col-12 d-flex mt-1 px-0">

        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col">
        <label for="fecha_inicio_premio"><b>(*) Fecha de inicio</b></label>
        <input type="date" class="form-control" disabled value="{{ $data->start_date }}">
    </div>
    <div class="form-group col">
        <label for="fecha_fin_premio"><b>(*) Fecha de vencimiento</b></label>
        <input type="date" class="form-control" value="{{ $data->end_date }}" disabled>
    </div>
</div>
<div class="row">

    <div class="col-md-12">
        <div class="form-group">
            <label for="titulo_premio"><b>(*) Titulo del premio</b></label>
            <input class="form-control titulo_premio" disabled value="{{ $data->award_title }}">
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label for="descripcion_premio"><b>(*) Descripción del premio</b></label>
            <textarea class="form-control descripcion_premio" disabled>{{ $data->award_description }}</textarea>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col">
        <label for="cantidad_litipuntos"><b>Puntos necesarios</b></label>
        <input type="number" class="form-control cantidad_litipuntos" value="{{ $data->litipoint_quantity }}" disabled>
    </div>

    <div class="form-group col">
        <label for="disponibles_litipuntos"><b>Cantidad disponible</b></label>
        <input type="number" class="form-control disponibles_litipuntos" value="{{ $data->availability }} puntos"
            disabled>
    </div>
</div>

<div class="form-group">
    <label for="partners"><b>Socios</b></label>
    <select class="form-control selector" multiple disabled>
        @foreach ($data->partners as $partner)
            <option value="{{ $partner->partner_code }}" selected>{{ $partner->partner_title }}
            </option>
        @endforeach
    </select>
    <div class="invalid-feedback">Por favor, seleccione un estado para el socio.
    </div>
</div>


<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
<script src="//cdn.ckeditor.com/4.20.1/full/ckeditor.js"></script>
<script src="https://s3.amazonaws.com/cnp.com.co/tagsinput.js"></script>

<script>
    $(document).ready(function() {
        $('.selector').select2();
    });
</script>
