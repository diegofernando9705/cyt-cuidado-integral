<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">

<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">
<!-- Validation -->
<section class="bs-validation">
    <div class="row">
        <!-- Bootstrap Validation -->
        <div class="col-md-12 col-12">
            <div class="card">
                <form class="update-premios" novalidate>
                    @csrf
                    @method('POST')
                    <input type="hidden" id="award_code" value="{{ $data->award_code }}"> 
                    <div class="media mb-2">
                        <img src="https://s3.amazonaws.com/cnp.com.co/{{ $data->award_image }}" alt="users avatar"
                            class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="150" />
                        <div class="media-body mt-50">
                            <h4>Imagen del premio</h4>
                            <div class="col-12 d-flex mt-1 px-0">
                                <label class="btn btn-primary mr-75 mb-0" for="imagen_premio">
                                    <span class="d-none d-sm-block">Subir foto</span>
                                    <input class="form-control" type="file" id="imagen_premio" name="imagen_premio"
                                        hidden accept="image/png, image/jpeg, image/jpg" name="avatar" />
                                    <span class="d-block d-sm-none">
                                        <i class="mr-0" data-feather="edit"></i>
                                    </span>
                                    <div class="invalid-feedback" style="color: white;">Por favor, seleccione una
                                        imagen de portada.
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col">
                            <label for="start_date"><b>(*) Fecha de inicio</b></label>
                            <input type="datetime-local" class="form-control start_date" name="start_date"
                                id="start_date" disabled value="{{ $data->start_date }}">
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                        <div class="form-group col">
                            <label for="end_date"><b>(*) Fecha de vencimiento</b></label>
                            <input type="datetime-local" class="form-control end_date" name="end_date"
                                id="end_date" required value="{{ $data->end_date }}">
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="award_title"><b>(*) Titulo del premio</b></label>
                                <input class="form-control award_title" name="award_title" id="award_title"
                                    required value="{{ $data->award_title }}">
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="award_description"><b>(*) Descripción del premio</b></label>
                                <textarea class="form-control award_description" name="award_description" id="award_description" required>{{ $data->award_description }}</textarea>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="form-group col">
                            <label for="litipoint_quantity"><b>Puntos necesarios</b></label>
                            <input type="number" class="form-control litipoint_quantity" name="litipoint_quantity"
                                id="litipoint_quantity" value="{{ $data->litipoint_quantity }}">
                        </div>

                        <div class="form-group col">
                            <label for="availability"><b>Cantidad disponible</b></label>
                            <input type="number" class="form-control availability"
                                name="availability" id="availability"
                                value="{{ $data->availability }}">
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="socios"><b>Socios</b></label>
                                <select class="form-control selector" multiple name="partner" id="partner">
                                    @foreach ($socios as $socio)
                                        @if (is_array($array_socios_premios) && in_array($socio->partner_code, $array_socios_premios))
                                            <option value="{{ $socio->partner_code }}" selected>
                                                {{ $socio->partner_title }}</option>
                                        @else
                                            <option value="{{ $socio->partner_code }}">{{ $socio->partner_title }}
                                            </option>
                                        @endif
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">Por favor, seleccione un estado para el socio.
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="status"><b>(*) Estado</b></label>
                                <select class="form-control" name="status" id="status" required>
                                    @if ($data->status == 1)
                                        <option value="1" selected>Activo</option>
                                        <option value="2">Inactivo</option>
                                    @else
                                        <option value="1">Activo</option>
                                        <option value="2" selected>Inactivo</option>
                                    @endif
                                </select>
                                <div class="invalid-feedback">Por favor, seleccione un estado para el premio.
                                </div>
                            </div>
                        </div>

                        <div class="col-12 d-flex flex-sm-row flex-column mt-2">
                            <button type="submit" id="submit"
                                class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1 col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">Actualizar premio</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/administrator/litipoints/awards/edit.js')) }}"></script>