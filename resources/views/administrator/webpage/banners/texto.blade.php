<form class="banners-inicio" novalidate>
    @csrf
    @method('POST')
    <div class="form-group">
        <label for=""><b>(*) Tipo de encabezado: </b></label>
        <select class="form-control selector selector_encabezado" data-slug="{{ $data->slug }}" required>
            <option value="texto" selected>Texto</option>
            <option value="banner">Slider con imagenes</option>
        </select>
    </div>

    <div class="form-group">
        <label for=""> <b>(*) Titulo del encabezado:</b> </label>
        <input class="form-control" type="text" name="title" id="title" value="{{ $data_header->title }}"
            required>
    </div>

    <div class="form-group">
        <label for=""> <b>(*) Descripción del encabezado:</b> </label>
        <textarea class="form-control" name="description" id="description" required>{{ $data_header->description }}</textarea>
    </div>

    <center>
        <button type="submit" class="btn btn-primary" id="actualizar_inicio">Actualizar datos</button>
    </center>
</form>

<script>
    // ===================== validacion del formulario encabezado principal ====================================  

    $(function() {
        'use strict';

        const forms = document.querySelectorAll('.banners-inicio');

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms).forEach((form) => {
            form.addEventListener('submit', (event) => {

                if (!form.checkValidity()) {
                    event.stopPropagation();
                } else {

                    var formulario = $(".banners-inicio").serialize();

                    document.getElementById("title").disabled = true;
                    document.getElementById("description").disabled = true;
                    document.getElementById("actualizar_inicio").disabled = true;

                    $.ajax({
                        type: 'POST',
                        url: '/app/webpage/banner/update/{{ $data_header->id }}',
                        data: formulario, // serializes the form's elements.
                        success: function(data) {

                            $("#mensajes").html(
                                "<div class='alert alert-success' style='padding:10px'>Verificando informacion</div>"
                            );

                            if (data == true) {
                                $("#registro-exitoso").toast('show');

                                setTimeout(function() {
                                    document.getElementById("title")
                                        .disabled = false;
                                    document.getElementById("description")
                                        .disabled = false;
                               
                                    document.getElementById(
                                            "actualizar_inicio").disabled =
                                        false;
                                }, 2000);

                            } else {
                                $("#mensajes").html(
                                    "<div class='alert alert-danger' style='padding:10px'>Error del servidor, verifica e intenta nuevamente.</div>"
                                );
                            }
                        },
                        error: function(error) {
                            errorHttp(error.status);
                            document.getElementById("title").disabled = false;
                            document.getElementById("description").disabled = false;
                            document.getElementById("actualizar_inicio").disabled =
                                false;

                            $("#mensajes").html(
                                "<div class='alert alert-danger' style='padding:10px'>" +
                                error.responseJSON.errors.nombre_rol +
                                "</div>");
                        },
                    });

                    event.preventDefault();

                }
                event.preventDefault();
                form.classList.add('was-validated');
            }, false);
        });

    });

    // =================== fin validacion del formulario encabezado principal ====================================
</script>
