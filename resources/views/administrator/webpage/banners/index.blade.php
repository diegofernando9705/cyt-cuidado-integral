@extends('administrator.layouts.contentLayoutMaster')

@section('title', 'Sección inicio')


@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <!--<p>Read full documnetation <a href="https://datatables.net/" target="_blank">here</a></p> -->
        </div>
    </div>
    <!-- Basic table -->
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">Sección banners web de CNP</h4>
                    </div>
                    <div class="container">
                        <div class="alert alert-danger" style="padding: 10px; margin-top:10px; margin-bottom:10px;">
                            <p>
                                Por favor seleccione la ubicación que desea actualizar.
                            </p>
                        </div>

                        <div id="accordion">
                            @can('paginaweb-banner-inicio')
                                <div class="componente" style="border: 1px solid rgb(201, 201, 201); margin-bottom:20px;">
                                    <div id="headingOne">
                                        <h5 class="mb-0 btn btn-success col-12 col-sm-12 encabezado" data-slug="index"
                                            data-toggle="collapse" data-target="#collapseInicio" aria-expanded="true"
                                            aria-controls="collapseInicio">
                                            Banners / Encabezado página principal
                                        </h5>
                                    </div>

                                    <div id="collapseInicio" class="collapse" aria-labelledby="headingOne"
                                        data-parent="#accordion">
                                        <div class="card-body" id="opciones_header_index">
                                        </div>

                                    </div>
                                </div>
                            @endcan

                            @can('paginaweb-banner-nosotros')
                                <div class="componente" style="border: 1px solid rgb(201, 201, 201); margin-bottom:20px;">
                                    <div id="headingOne">
                                        <h5 class="mb-0 btn btn-danger col-12 col-sm-12 encabezado" data-slug="about_us"
                                            data-toggle="collapse" data-target="#collapseNosotros" aria-expanded="true"
                                            aria-controls="collapseNosotros">
                                            Banners / Encabezado página Nosotros
                                        </h5>
                                    </div>

                                    <div id="collapseNosotros" class="collapse" aria-labelledby="headingOne"
                                        data-parent="#accordion">
                                        <div class="card-body" id="opciones_header_about_us">
                                        </div>
                                    </div>
                                </div>
                            @endcan

                            @can('paginaweb-banner-servicios')
                                <div class="componente" style="border: 1px solid rgb(201, 201, 201); margin-bottom:20px;">
                                    <div id="headingOne">
                                        <h5 class="mb-0 btn btn-info col-12 col-sm-12 encabezado" data-slug="services"
                                            data-toggle="collapse" data-target="#collapseServicios" aria-expanded="true"
                                            aria-controls="collapseServicios">
                                            Banners / Encabezado página Servicios
                                        </h5>
                                    </div>

                                    <div id="collapseServicios" class="collapse" aria-labelledby="headingOne"
                                        data-parent="#accordion">
                                        <div class="card-body" id="opciones_header_services">
                                        </div>
                                    </div>
                                </div>
                            @endcan

                            @can('paginaweb-banner-noticias')
                                <div class="componente" style="border: 1px solid rgb(201, 201, 201); margin-bottom:20px;">
                                    <div id="headingOne">
                                        <h5 class="mb-0 btn btn-primary col-12 col-sm-12 encabezado" data-slug="news"
                                            data-toggle="collapse" data-target="#collapseNoticias" aria-expanded="true"
                                            aria-controls="collapseNoticias">
                                            Banners / Encabezado página Noticias
                                        </h5>
                                    </div>

                                    <div id="collapseNoticias" class="collapse" aria-labelledby="headingOne"
                                        data-parent="#accordion">
                                        <div class="card-body" id="opciones_header_news">
                                        </div>
                                    </div>
                                </div>
                            @endcan

                            @can('paginaweb-banner-preguntasfrecuentes')
                                <div class="componente" style="border: 1px solid rgb(201, 201, 201); margin-bottom:20px;">
                                    <div id="headingOne">
                                        <h5 class="mb-0 btn btn-success col-12 col-sm-12 encabezado"
                                            data-slug="frequent_questions" data-toggle="collapse"
                                            data-target="#collapsePreguntasFrecuentes" aria-expanded="true"
                                            aria-controls="collapsePreguntasFrecuentes">
                                            Banners / Encabezado página Preguntas Frecuentes
                                        </h5>
                                    </div>

                                    <div id="collapsePreguntasFrecuentes" class="collapse" aria-labelledby="headingOne"
                                        data-parent="#accordion">
                                        <div class="card-body" id="opciones_header_frequent_questions">
                                        </div>
                                    </div>
                                </div>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>



    <!-- Modal -->
    <div class="modal fade" id="modal-agregar-imagen" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="titulo_modal">Información</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="contenido-modal">

                </div>
            </div>
        </div>
    </div>
    <!--/ Basic table -->

@endsection


@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="{{ asset(mix('js/scripts/administrator/webpage/banner/index.js')) }}"></script>
@endsection
