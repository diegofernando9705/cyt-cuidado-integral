<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap4.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">

<div class="form-group">
    <label for=""><b>(*) Tipo de encabezado: </b></label>
    <select class="form-control selector selector_encabezado" data-slug="{{ $data->slug }}" required>
        <option value="text">Texto</option>
        <option value="banner" selected>Slider con imagenes</option>
    </select>
</div>

<button class="btn btn-primary" id="agregar_imagen">Agregar un nuevo registro</button>
<div class="row col-12 col-sm-12 col-lg-12 col-xl-12">
    <table class="datatable-imagenes table table-hover col-12 col-sm-12 col-lg-12 col-xl-12">
        <thead>
            <tr>
                <th></th>
                <th>#</th>
                <th>IMAGEN</th>
                <th>TITULO</th>
                <th></th>
            </tr>
        </thead>
    </table>
</div>



<script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap4.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/tables/table-datatables-basic.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


<script>
    $("#submit_index_banners").attr("style", "display:none;");
    var tabla_banners = $('.datatable-imagenes');

    $(document).ready(function() {

        $("#submit_index_banners").attr("style", "display:none;");

        if (tabla_banners.length) {
            var dt_basic = tabla_banners.DataTable({
                ajax: "{{ route('listBannersHeader', $data->slug) }}",
                columns: [{
                        data: 'vacio'
                    }, // used for sorting so will hide this column
                    {
                        data: 'id'
                    },
                    {
                        "data": 'url',
                        "render": function(data, type, row, meta) {
                            return "<img src='" + data + "' width='80px'>";
                        }
                    },
                    {
                        data: 'title'
                    },
                    {
                        data: ''
                    }
                ],
                columnDefs: [{
                        className: 'control',
                        orderable: false,
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Acciones',
                        orderable: false,
                        render: function(data, type, full, meta) {

                            return (
                                '<button class="btn btn-info editar-imagen btn-sm" title="Editar imagen" data-id="' +
                                full['id'] + '">' +
                                feather.icons['file-text'].toSvg({
                                    class: 'font-small-4'
                                }) +
                                '</button>' +
                                ' <button class="btn btn-danger eliminar-imagen btn-sm" title="Eliminar imagen" data-id="' +
                                full['id'] + '">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-small-4'
                                }) +
                                '</button>'

                            );
                        }
                    }
                ],
                order: [
                    [2, 'asc']
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function(row) {
                                var data = row.data();
                                return 'Details of ' + data['name'];
                            }
                        }),
                        type: 'column',
                        renderer: function(api, rowIdx, columns) {
                            var data = $.map(columns, function(col, i) {

                                return col.title !==
                                    '' // ? Do not show row in modal popup if title is blank (for check box)
                                    ?
                                    '<tr data-dt-row="' +
                                    col.rowIndex +
                                    '" data-dt-column="' +
                                    col.columnIndex +
                                    '">' +
                                    '<td>' +
                                    col.title +
                                    ':' +
                                    '</td> ' +
                                    '<td>' +
                                    col.data +
                                    '</td>' +
                                    '</tr>' :
                                    '';
                            }).join('');

                            return data ? $('<table class="table"/>').append(data) : false;
                        }
                    }
                },
                language: {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                },
            });
            $('div.head-label').html('<h6 class="mb-0">DataTable with Buttons</h6>');
        }
    });


    $(document).on("click", ".editar-imagen", function() {
        $(".modal").modal("show");
        $("#titulo_modal").html("EDITAR IMAGEN");

        $.ajax({
            type: 'GET',
            url: '/app/webpage/banner/table/image/edit/' + $(this).attr("data-id"),
            success: function(data) {
                $("#contenido-modal").html(data);
            },
            error: function(err) {
                $(".error-ajax").attr("style", "display:none;");
                if (err.status == 422) {
                    $('#success_message').fadeIn().html(err.responseJSON.message);
                    console.warn(err.responseJSON.errors);
                    $.each(err.responseJSON.errors, function(i, error) {
                        var el = $(document).find('[name="' + i + '"]');
                        el.after($('<span style="color: red;" class="error-ajax">' + error[
                            0] + '</span>'));
                    });
                }
            },
        });
    });

    $(document).on("click", "#agregar_imagen", function() {
        $(".modal").modal("show");
        $("#titulo_modal").html("AGREGAR IMAGEN");

        $.ajax({
            type: 'GET',
            url: '{{ route('formAddImageToBanner', $data->slug) }}',
            success: function(data) {
                $("#contenido-modal").html(data);
            },
            error: function(err) {
                $(".error-ajax").attr("style", "display:none;");
                if (err.status == 422) {
                    $('#success_message').fadeIn().html(err.responseJSON.message);
                    console.warn(err.responseJSON.errors);
                    $.each(err.responseJSON.errors, function(i, error) {
                        var el = $(document).find('[name="' + i + '"]');
                        el.after($('<span style="color: red;" class="error-ajax">' + error[
                            0] + '</span>'));
                    });
                }
            },
        });
    });

    $(document).on("click", ".eliminar-imagen", function() {
        $(".modal").modal("hide");
        Swal.fire({
            title: '¿Desea eliminar el registro?',
            text: "No podrás revertir esto",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'GET',
                    url: '/app/webpage/banner/table/image/delete/' + $(this).attr("data-id"),
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        Swal.fire("Banner eliminado", "", "success");

                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    },
                    error: function(err) {
                        $(".modal").modal("hide");
                        errorHttp(err.status);
                        if (err.status == 422) {

                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON.message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function(i, error) {
                                var el = $(document).find('[name="' + i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                Swal.fire(
                    'Eliminado!',
                    'La información ha sido eliminada.',
                    'success'
                )
            }
        })
    });
</script>
