<form class="actualizacion-imagen-banner" novalidate>
    @csrf
    @method('POST')

    <div class="media mb-2">
        <img src="{{ config('app.AWS_BUCKET_URL') }}{{ $data->url }}" alt="users avatar"
            class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="100" />
        <div class="media-body mt-50">
            <h4>imagen del banner</h4>
            <div class="col-12 d-flex mt-1 px-0">
                <label class="btn btn-primary mr-75 mb-0" for="imagen">
                    <span class="d-none d-sm-block">Subir foto</span>
                    <input class="form-control" type="file" id="imagen" name="imagen" hidden
                        accept="image/png, image/jpeg, image/jpg" name="avatar" />
                    <span class="d-block d-sm-none">
                        <i class="mr-0" data-feather="edit"></i>
                    </span>
                    <div class="invalid-feedback" style="color: white;">Por favor, seleccione una imagen.</div>
                </label>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="title">Titulo del banner</label>
                <input type="text" class="form-control" id="title" name="title" value="{{ $data->title }}">
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="description">Descripción del banner</label>
                <textarea class="form-control" name="description" id="description">{{ $data->description }}</textarea>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="text_button">Texto del botón</label>
                <input type="text" class="form-control" id="text_button" name="text_button"
                    value="{{ $data->text_button }}">
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="link_button">Hipervinculo del botón</label>
                <input type="text" class="form-control" id="link_button" name="link_button"
                    value="{{ $data->url_button }}">
            </div>
        </div>

        <div class="col-12 d-flex flex-sm-row flex-column mt-2">
            <button type="submit" id="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">ACTUALIZAR
                INFORMACIÓN</button>
        </div>
    </div>
</form>

<!-- Basic toast -->
<div class="toast toast-basic hide position-fixed actualizacion-exitosa" role="alert" aria-live="assertive"
    aria-atomic="true" data-delay="5000" style="top: 1rem; right: 1rem" id="registro-exitoso">
    <div class="toast-header bg-success" style="color: white;">
        <strong class="mr-auto">Notificación</strong>
        <small class="text-muted">Justo ahora</small>
        <button type="button" class="ml-1 close" data-dismiss="toast" aria-label="Close" style="color: white;">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="toast-body">¡Felicidades, acción exitosa!</div>
</div>
<!-- Basic toast ends -->


<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/components/components-navs.js')) }}"></script>

<script type="text/javascript">
    var tabla_banners = $('.datatable-imagenes');

    // ================================ formulario para un nuevo registro ================================

    var changePicture = $('#imagen'),
        userAvatar = $('.user-avatar');

    if (changePicture.length) {
        $(changePicture).on('change', function(e) {
            var reader = new FileReader(),
                files = e.target.files;
            reader.onload = function() {
                if (userAvatar.length) {
                    userAvatar.attr('src', reader.result);
                }
            };
            reader.readAsDataURL(files[0]);
        });
    }


    $(function() {
        'use strict';

        const formulario_usuario_edicion = document.querySelectorAll('.actualizacion-imagen-banner');


        Array.prototype.slice.call(formulario_usuario_edicion).forEach((form) => {
            form.addEventListener('submit', (event) => {

                if (!form.checkValidity()) {
                    event.stopPropagation();
                } else {

                    var paqueteDeDatos = new FormData();
                    paqueteDeDatos.append('imagen', $('#imagen')[0].files[0]);
                    paqueteDeDatos.append('title', $('#title').prop('value'));
                    paqueteDeDatos.append('description', $('#description').prop('value'));
                    paqueteDeDatos.append('text_button', $('#text_button').prop('value'));
                    paqueteDeDatos.append('link_button', $('#link_button').prop('value'));


                    document.getElementById("title").disabled = true;
                    document.getElementById("description").disabled = true;
                    document.getElementById("text_button").disabled = true;
                    document.getElementById("link_button").disabled = true;
                    document.getElementById("submit").disabled = true;

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                        }
                    });


                    $.ajax({
                        type: 'POST',
                        url: '{{ route('updateImageBanner', $data->id) }}',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: paqueteDeDatos,
                        processData: false,
                        contentType: false,
                        success: function(data) {

                            if (data == true) {

                                $("#registro-exitoso").toast('show');

                                setTimeout(() => {
                                    location.reload();
                                }, 2000);


                            } else {
                                $("#mensajes").html(
                                    "<div class='alert alert-danger' style='padding:10px'>Error del servidor, verifica e intenta nuevamente.</div>"
                                );
                            }
                        },
                        error: function(err) {

                            document.getElementById("title").disabled =
                                false;
                            document.getElementById("description").disabled =
                                false;
                            document.getElementById("text_button").disabled =
                                false;
                            document.getElementById("link_button").disabled =
                                false;
                            document.getElementById("submit").disabled =
                                false;


                            $(".error-ajax").attr("style", "display:none;");
                            if (err.status == 422) {
                                $('#success_message').fadeIn().html(err.responseJSON
                                    .message);
                                console.warn(err.responseJSON.errors);
                                $.each(err.responseJSON.errors, function(i, error) {
                                    var el = $(document).find('[name="' +
                                        i + '"]');
                                    el.after($('<span style="color: red;" class="error-ajax">' +
                                        error[0] + '</span>'));
                                });
                            }
                        },
                    });

                    event.preventDefault();
                }

                event.preventDefault();
                form.classList.add('was-validated');
            }, false);
        });

    });

    // ================================ formulario para un nuevo registro ================================
</script>
