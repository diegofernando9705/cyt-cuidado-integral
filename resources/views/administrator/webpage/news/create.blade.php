@extends('administrator.layouts.contentLayoutMaster')
@section('title', 'Registrar noticia')

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
            <!-- Bootstrap Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-body">
                        <div id="mensajes"></div>
                        <form class="store-noticias" novalidate>
                            @csrf
                            @method('POST')
                            <div class="media mb-2">
                                <img src="https://s3.amazonaws.com/cnp.com.co/textocnp.png" alt="users avatar"
                                    class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer"
                                    width="150" />
                                <div class="media-body mt-50">
                                    <h4>Imagen de portada</h4>
                                    <div class="col-12 d-flex mt-1 px-0">
                                        <label class="btn btn-primary mr-75 mb-0" for="imagen_noticia">
                                            <span class="d-none d-sm-block">Subir foto</span>
                                            <input class="form-control" type="file" id="imagen_noticia"
                                                name="imagen_noticia" hidden accept="image/png, image/jpeg, image/jpg"
                                                name="avatar" required />
                                            <span class="d-block d-sm-none">
                                                <i class="mr-0" data-feather="edit"></i>
                                            </span>
                                            <div class="invalid-feedback" style="color: white;">Por favor, seleccione una
                                                imagen de portada.
                                            </div>
                                        </label>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fecha_registro"><b>(*) Fecha de publicación</b></label> <br>
                                        <input type="date" class="form-control" name="fecha_registro" id="fecha_registro"
                                            required value="{{ date('Y-m-d') }}">
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fecha_terminacion"><b>Fecha de fin publicación</b></label> <br>
                                        <input type="date" class="form-control" name="fecha_terminacion"
                                            id="fecha_terminacion">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="titulo_noticia"><b>(*) Titutlo de la noticia</b></label>
                                        <input type="text" class="form-control" name="titulo_noticia" id="titulo_noticia"
                                            required>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="resumen_noticia"><b>(*) Resumen de la noticia</b></label>
                                        <textarea class="form-control resumen_noticia" name="resumen_noticia" id="resumen_noticia" maxlength="190"></textarea>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="descripcion_noticia"><b>(*) Descripción de la noticia</b></label>
                                        <textarea class="form-control descripcion_noticia" name="descripcion_noticia" id="descripcion_noticia"></textarea>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="url_noticia"><b>(*) URL de la Noticia</b></label> <br>
                                        <input type="text" class="form-control" name="url_noticia" id="url_noticia"
                                            required>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="categorias_noticia"><b>(*) Categorías de la noticia</b></label>
                                        <select class="form-control selector" name="categorias_noticia" multiple
                                            id="categorias_noticia" required>
                                            @foreach ($categorias as $categoria)
                                                <option value="{{ $categoria->code }}">
                                                    {{ $categoria->title }}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">Por favor, seleccione por lo menos una categoria.
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="categorias_noticia"><b>(*) Tipo de noticia</b></label>
                                        <select class="form-control selector" name="type" id="type" required>
                                            <option value="public" selected>Público</option>
                                            <option value="subscribers">Suscriptores</option>
                                        </select>
                                        <div class="invalid-feedback">Por favor, seleccione una opción.
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-12 d-flex flex-sm-row flex-column mt-2">
                                    <button type="submit" id="submit"
                                        class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">Registrar
                                        noticia</button>
                                </div>
                            </div>
                        </form>

                        <!-- Basic toast -->
                        <div class="toast toast-basic hide position-fixed registro-exitoso" role="alert"
                            aria-live="assertive" aria-atomic="true" data-delay="5000" style="top: 1rem; right: 1rem"
                            id="registro-exitoso">
                            <div class="toast-header bg-success" style="color: white;">
                                <strong class="mr-auto">Notificación</strong>
                                <small class="text-muted">Justo ahora</small>
                                <button type="button" class="ml-1 close" data-dismiss="toast" aria-label="Close"
                                    style="color: white;">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="toast-body">¡Felicidades, registro exitoso!</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/38.0.1/super-build/ckeditor.js"></script>
    <script src="https://s3.amazonaws.com/cnp.com.co/tagsinput.js"></script>
    <script src="{{ asset(mix('js/scripts/administrator/webpage/new/create.js')) }}"></script>
@endsection
