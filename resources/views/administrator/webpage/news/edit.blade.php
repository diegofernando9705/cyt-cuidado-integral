<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">

<form class="update-noticias" novalidate>
    @csrf
    @method('POST')
    <div class="media mb-2">
        <img src="https://s3.amazonaws.com/cnp.com.co/{{ $data->image }}" alt="users avatar"
            class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="150" />
        <div class="media-body mt-50">
            <h4>Imagen de portada</h4>
            <div class="col-12 d-flex mt-1 px-0">
                <label class="btn btn-primary mr-75 mb-0" for="imagen_noticia">
                    <span class="d-none d-sm-block">Actualizar portada</span>
                    <input class="form-control" type="file" id="imagen_noticia" name="imagen_noticia" hidden
                        accept="image/png, image/jpeg, image/jpg" name="avatar" />
                    <span class="d-block d-sm-none">
                        <i class="mr-0" data-feather="edit"></i>
                    </span>
                    <div class="invalid-feedback" style="color: white;">Por favor, seleccione una
                        imagen de portada.
                    </div>
                </label>

            </div>
        </div>
    </div>

    <div class="row">
        <input type="hidden" name="id" id="id" value="{{ $data->id }}">
        <div class="col-md-6">
            <div class="form-group">
                <label for="fecha_registro"><b>(*) Fecha de publicación</b></label> <br>
                <input type="date" class="form-control" name="fecha_registro" id="fecha_registro" required
                    value="{{ $data->register_date }}">
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="fecha_terminacion"><b>Fecha de fin publicación</b></label> <br>
                <input type="date" class="form-control" name="fecha_terminacion" id="fecha_terminacion"
                    value="{{ $data->date_end }}">
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="titulo_noticia"><b>(*) Titulo de la noticia</b></label>
                <input type="text" class="form-control" name="titulo_noticia" id="titulo_noticia" required
                    value="{{ $data->title }}">
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="resumen_noticia"><b>(*) Resumen de la noticia</b></label>
                <textarea class="form-control resumen_noticia" name="resumen_noticia" id="resumen_noticia" maxlength="190">{{ $data->summary }}</textarea>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="descripcion_noticia"><b>(*) Descripción de la noticia</b></label>
                <textarea class="form-control descripcion_noticia" name="descripcion_noticia" id="descripcion_noticia">{{ $data->description }}</textarea>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="url_noticia"><b>(*) URL de la Noticia</b></label> <br>
                <input type="text" class="form-control" id="url_noticia" name="url_noticia"
                    value="{{ $data->url }}" required>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="tags_noticia"><b>Usuario creador:</b></label> <br>
                <input type="text" class="form-control" value="{{ $data->getUser->name }}" disabled>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="categorias_noticia"><b>(*) Categorías de la noticia</b></label>
                <select class="form-control selector" multiple name="categorias_noticia" id="categorias_noticia"
                    required>
                    @foreach ($categorias as $categoria)
                        <?php if (is_array($categorias_noticia) && in_array($categoria->code, $categorias_noticia)) { ?>
                        <option value="{{ $categoria->code }}" selected>
                            {{ $categoria->title }}</option>
                        <?php }else{ ?>
                        <option value="{{ $categoria->code }}">
                            {{ $categoria->title }}</option>
                        <?php } ?>
                    @endforeach
                </select>
                <div class="invalid-feedback">Por favor, seleccione por lo menos una categoria.
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="categorias_noticia"><b>(*) Tipo de noticia</b></label>
                <select class="form-control selector" name="type" id="type" required>
                    @if ($data->type == 'subscriber')
                        <option value="public">Público</option>
                        <option value="subscribers">Suscriptores</option>
                    @else
                        <option value="public">Público</option>
                        <option value="subscribers" selected>Suscriptores</option>
                    @endif
                </select>
                <div class="invalid-feedback">Por favor, seleccione una opción.
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="estado_noticia"><b>(*) Estado</b></label>
                <select class="form-control" name="estado_noticia" id="estado_noticia" required>
                    @foreach ($status as $state)
                        @if ($state->id == $data->status)
                            <option value="{{ $state->id }}" selected>{{ $state->description }}</option>
                        @else
                            <option value="{{ $state->id }}">{{ $state->description }}</option>
                        @endif
                    @endforeach
                </select>
                <div class="invalid-feedback">Por favor, seleccione un estado para la categoria.
                </div>
            </div>
        </div>

        <div class="col-12 d-flex flex-sm-row flex-column mt-2">
            <button type="submit" id="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">Actualizar
                noticia</button>
        </div>
    </div>
</form>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/38.0.1/super-build/ckeditor.js"></script>
<script src="https://s3.amazonaws.com/cnp.com.co/tagsinput.js"></script>
<script src="{{ asset(mix('js/scripts/administrator/webpage/new/edit.js')) }}"></script>
