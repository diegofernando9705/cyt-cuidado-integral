<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">


<div class="media mb-2">
    <img src="https://s3.amazonaws.com/cnp.com.co/{{ $data->image }}" alt="users avatar"
        class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="150" />
    <div class="media-body mt-50">
        <h4>Imagen de portada</h4>
        <div class="col-12 d-flex mt-1 px-0">
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <label for="fecha_registro"><b>(*) Fecha de publicación</b></label> <br>
            <input type="date" class="form-control" value="{{ $data->register_date }}" disabled>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="fecha_terminacion"><b>Fecha de fin publicación</b></label> <br>
            <input type="date" class="form-control" value="{{ $data->end_date }}" disabled>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label for="titulo_noticia"><b>(*) Titutlo de la noticia</b></label><br>
            {!! $data->title !!}
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label for="resumen_noticia"><b>(*) Resumen de la noticia</b></label> <br>
            {{ $data->summary }}
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label for="descripcion_noticia"><b>(*) Descripción de la noticia</b></label> <br>
            {!! $data->description !!}
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label for="url_noticia"><b>(*) URL de la Noticia</b></label> <br>
            <input type="text" class="form-control" value="{{ $data->url }}" disabled>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label for="url_noticia"><b>(*) Usuario que creó la noticia</b></label> <br>
            <input type="text" class="form-control" value="{{ $data->getUser->name }}" disabled>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label for="tags_noticia"><b>Categorias de la Noticia</b></label> <br>
            @foreach ($data->category as $categoria)
                <button type="button" class="btn btn-primary">{{ $categoria->title }}</button>
            @endforeach
        </div>
    </div>



</div>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
<script src="//cdn.ckeditor.com/4.20.1/full/ckeditor.js"></script>
<script src="https://s3.amazonaws.com/cnp.com.co/tagsinput.js"></script>
