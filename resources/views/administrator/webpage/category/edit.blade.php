<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">

<form class="update-categorias" novalidate>
    @csrf
    @method('POST')
    <input type="hidden" class="form-control" id="code" value="{{ $data->code }}">
    <div class="media mb-2">
        <img src="https://s3.amazonaws.com/cnp.com.co/{{ $data->image }}" title=""
            class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="150" />
        <div class="media-body mt-50">
            <h4>Imagen de portada</h4>
            <div class="col-12 d-flex mt-1 px-0">
                <label class="btn btn-primary mr-75 mb-0" for="imagen_categoria">
                    <span class="d-none d-sm-block">Subir foto</span>
                    <input class="form-control" type="file" id="imagen_categoria" name="imagen_categoria" hidden
                        accept="image/png, image/jpeg, image/jpg" name="avatar" />
                    <span class="d-block d-sm-none">
                        <i class="mr-0" data-feather="edit"></i>
                    </span>
                    <div class="invalid-feedback" style="color: white;">Por favor, seleccione una
                        imagen de portada.
                    </div>
                </label>

            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-12">
            <div class="form-group">
                <label for="titulo_Seccion"><b>(*) Titulo de la categoria</b></label>
                <input type="text" class="form-control" name="titulo_categoria" id="titulo_categoria" required
                    value="{{ $data->title }}">
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="descripcion_categoria"><b>Descripción completa de la
                        categoria</b></label>
                <textarea class="form-control descripcion_categoria" name="descripcion_categoria" id="descripcion_categoria">{{ $data->description }}</textarea>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="estado_categoria"><b>(*) Estado</b></label>
                <select class="form-control" name="estado_categoria" id="estado_categoria" required>
                    @foreach ($estados as $estado)
                        @if ($data->status == $estado->id)
                            <option value="{{ $estado->id }}">{{ $estado->description }}</option>
                        @endif
                    @endforeach
                </select>
                <div class="invalid-feedback">Por favor, seleccione un estado para la categoria.
                </div>

            </div>
        </div>

        <div class="col-12 d-flex flex-sm-row flex-column mt-2">
            <button type="submit" id="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">Actualizar</button>
        </div>
    </div>
</form>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
<script src="//cdn.ckeditor.com/4.20.1/full/ckeditor.js"></script>
<script src="{{ asset(mix('js/scripts/administrator/webpage/category/edit.js')) }}"></script>
