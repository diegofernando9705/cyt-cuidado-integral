@extends('administrator.layouts.contentLayoutMaster')
@section('title', 'Registrar categoria')

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
            <!-- Bootstrap Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-body">
                        <div id="mensajes"></div>
                        <form class="store-categorias" id="store-categorias" novalidate>
                            @csrf
                            @method('POST')
                            <div class="media mb-2">
                                <img src="https://s3.amazonaws.com/cnp.com.co/textocnp.png" alt="users avatar"
                                    class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer"
                                    width="150" />
                                <div class="media-body mt-50">
                                    <h4>Imagen de portada</h4>
                                    <div class="col-12 d-flex mt-1 px-0">
                                        <label class="btn btn-primary mr-75 mb-0" for="imagen_categoria">
                                            <span class="d-none d-sm-block">Subir foto</span>
                                            <input class="form-control" type="file" id="imagen_categoria"
                                                name="imagen_categoria" hidden accept="image/png, image/jpeg, image/jpg"
                                                name="avatar" />
                                            <span class="d-block d-sm-none">
                                                <i class="mr-0" data-feather="edit"></i>
                                            </span>
                                            <div class="invalid-feedback" style="color: white;">Por favor, seleccione una
                                                imagen de portada.
                                            </div>
                                        </label>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title"><b>(*) Titulo de la categoria</b></label>
                                        <input type="text" class="form-control" name="title" id="title" required maxlength="50">
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description"><b>Descripción completa de la
                                                categoria</b></label>
                                        <textarea class="form-control description" name="description" id="description"></textarea>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-12 d-flex flex-sm-row flex-column mt-2">
                                    <button type="submit" id="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">Registrar categoria</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="//cdn.ckeditor.com/4.20.1/full/ckeditor.js"></script>
    <script src="{{ asset(mix('js/scripts/administrator/webpage/category/create.js')) }}"></script>
@endsection
