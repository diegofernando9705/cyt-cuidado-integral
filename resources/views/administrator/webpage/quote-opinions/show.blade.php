<style>
    .form-control {
        display: block;
        padding: 0.375rem 0.75rem;
        font-size: 12px !important;
        font-weight: 400;
        line-height: 1.5;
        color: #002e66;
        background-color: #fff;
        background-clip: padding-box;
        appearance: none;
        border-radius: 4px;
        box-shadow: none;
        margin-top: 3px;
    }

    .form-control:focus {
        display: block;
        padding: 0.375rem 0.75rem;
        font-size: 12px !important;
        font-weight: 400;
        line-height: 1.5;
        color: #726D7B;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #002E66;
        appearance: none;
        border-radius: 8px;
        box-shadow: none;
        margin-top: 3px;
    }

    .label_dictamen {
        font-family: "OpenSans-Regular";
        color: #002e66 !important;
        margin-top: 10px;
        font-size: 13px;
    }

    #submit_cotizador {
        font-size: 13px;
        margin-top: 20px;
        font-family: "OpenSans-Regular" !important;
        background-color: #002e66 !important;
        color: white;
        text-decoration: none;
        border-radius: 6px;
        text-align: center;
        box-shadow: rgba(50, 50, 93, 0.25) 0px 6px 12px -2px, rgba(0, 0, 0, 0.3) 0px 3px 7px -3px;
    }
</style>

Hola! este es un mensaje automatico enviado por la plataforma CENTRO NACIONAL DE PRUEBAS indicandole el registro los
datos del formulario "Cotizador de Dictámenes"
<br>
<br>
<ul>
    <li><b>Nombre y apellidos: </b>{{ $array_informacion->nombres_apellidos }}</li>
    <li><b>Correo electrónico: </b>{{ $array_informacion->correo_electronico }}</li>
    <li><b>Telefono/Celular: </b>{{ $array_informacion->telefono_celular }}</li>
    <li><b>Ciudad: </b>{{ $array_informacion->ciudad }}</li>
    <li><b>Area de derecho: </b>{{ $array_informacion->area_derecho }}</li>
    <li><b>Valor del pretensiones: </b>${{ number_format($array_informacion->valor_pretensiones) }}</li>

    <li><b>Tipo de cliente: </b>
        @switch($array_informacion->tipo_cliente)
            @case('juridica_natural')
                P. Naturales
            @break

            @case('juridica_normal')
                P. Juridica Normal
            @break

            @case('juridica_multinacional')
                P. Juridica Multinacional
            @break

            @default
                P. Derecho Público (estado)
            @break
        @endswitch
    </li>
    <li><b>Pretensiones: </b>
        @switch($array_informacion->opcion_pretensiones)
            @case('opcion_uno')
                Menores a $1.000 Mill
            @break

            @case('opcion_dos')
                Entre $1.000 y $5.000 Mill
            @break

            @case('opcion_tres')
                Entre $5.000 y $10.000 Mill
            @break

            @default
                Más de $10.000 Mill
            @break
        @endswitch
    </li>
    <li><b>Experiencia en el litigio: </b>
        @switch($array_informacion->experiencia_litigio)
            @case('opcion_uno')
                Menos de 2 años
            @break

            @case('opcion_dos')
                Entre 2 y 5 años
            @break

            @case('opcion_tres')
                Más de 5 años
            @break

            @default
                Firma de abogados
            @break
        @endswitch
    </li>
    <li><b>Tribunal: </b>
        @switch($array_informacion->tribunal)
            @case('privado')
                Privado
            @break

            @default
                Ordinario
            @break
        @endswitch
    </li>
    <li><b>Volumen de la información: </b>
        @switch($array_informacion->volumen_informacion)
            @case('opcion_uno')
                Pocos Archivos y Organizados
            @break

            @case('opcion_dos')
                Pocos Archivos y No Organizados
            @break

            @default
                Muchos Archivos y Organizados/No Organizados
            @break
        @endswitch
    </li>
    <li><b>Servicios adicionales: </b>
        @switch($array_informacion->servicios_adicionales)
            @case('opcion_uno')
                Preparación de Audiencia
            @break

            @case('opcion_dos')
                Sustentación en Audiencia
            @break

            @default
                Elementos técnicos probatorios
            @break
        @endswitch
    </li>
    <li><b>Área del dictamen: </b>
        @switch($array_informacion->area_dictamen)
            @case('opcion_uno')
                Civil
            @break

            @case('opcion_dos')
                Médico
            @break

            @default
                Financiero
            @break
        @endswitch
    </li>
    <li>
        <b>Total dictamen: </b> ${{ number_format($array_informacion->resultado_cotizacion) }}
    </li>
</ul>

Gracias por su confianza, muy pronto uno de nuestros asesores se comunicará con usted.