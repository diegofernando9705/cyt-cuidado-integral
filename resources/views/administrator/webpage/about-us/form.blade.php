@extends('administrator.layouts.contentLayoutMaster')

@section('titulo', 'Nosotros')

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">

            <!-- Bootstrap Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-body">
                        @if (Session::has('message'))
                            <div class="alert alert-success" style="padding: 10px;">{{ Session::get('message') }}</div>
                        @endif
                        <form action="{{ route('paginaweb-nosotros-post') }}" method="POST" novalidate>
                            @csrf
                            @method('POST')
                            <div class="form-group">
                                <label class="form-label" for="titulo_nosotros"><b>(*) Titulo encabezado</b></label>
                                <input type="text" id="titulo_nosotros" class="form-control" name="titulo_nosotros"
                                    aria-describedby="basic-addon-name" value="{{ $data->title }}" required />
                                <div class="valid-feedback"></div>
                                <div class="invalid-feedback">Por favor, ingrese un titulo para el encabezado.</div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="body_nosotros"><b>(*) Descripción nosotros</b></label>
                                <textarea class="form-control" id="body_nosotros" name="body_nosotros">{{ $data->description }}</textarea>
                                <div class="valid-feedback"></div>
                                <div class="invalid-feedback">Por favor, ingrese un titulo para el encabezado.</div>
                            </div>

                            <br>
                            <div class="row">
                                <div class="col-12">
                                    <center>
                                        <button type="submit" class="btn btn-primary" id="registro-btn">Actualizar
                                            información</button>
                                    </center>
                                </div>
                            </div>
                        </form>

                        <!-- Basic toast -->
                        <div class="toast toast-basic hide position-fixed registro-exitoso" role="alert"
                            aria-live="assertive" aria-atomic="true" data-delay="5000" style="top: 1rem; right: 1rem"
                            id="registro-exitoso">
                            <div class="toast-header bg-success" style="color: white;">
                                <strong class="mr-auto">Notificación</strong>
                                <small class="text-muted">Justo ahora</small>
                                <button type="button" class="ml-1 close" data-dismiss="toast" aria-label="Close"
                                    style="color: white;">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="toast-body">¡Felicidades, registro exitoso!</div>
                        </div>
                        <!-- Basic toast ends -->
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- /Validation -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="//cdn.ckeditor.com/4.20.1/full/ckeditor.js"></script>
    <script>
        $(document).ready(function() {
            CKEDITOR.replace('body_nosotros', {
                filebrowserBrowseUrl: '{{ asset(route('ckfinder_browser')) }}',
                filebrowserImageBrowseUrl: '{{ asset(route('ckfinder_browser')) }}?type=Images',
                filebrowserFlashBrowseUrl: '{{ asset(route('ckfinder_browser')) }}?type=Flash',
                filebrowserUploadUrl: '{{ asset(route('ckfinder_connector')) }}?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl: '{{ asset(route('ckfinder_connector')) }}?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: '{{ asset(route('ckfinder_connector')) }}?command=QuickUpload&type=Flash'
            });

            CKEDITOR.config.height = 720;
        });
    </script>
@endsection
