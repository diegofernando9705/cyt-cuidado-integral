<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">

<form class="update-services" novalidate>
    @csrf
    @method('POST')
    <div class="media mb-2">
        <img src="https://s3.amazonaws.com/cnp.com.co/{{ $data_services->image }}" alt="users avatar"
            class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="150" />
        <div class="media-body mt-50">
            <h4>Imagen de portada</h4>
            <div class="col-12 d-flex mt-1 px-0">
                <label class="btn btn-primary mr-75 mb-0" for="imagen_service">
                    <span class="d-none d-sm-block">Subir foto</span>
                    <input class="form-control" type="file" id="imagen_service" name="imagen_service" hidden
                        accept="image/png, image/jpeg, image/jpg" name="avatar" />
                    <span class="d-block d-sm-none">
                        <i class="mr-0" data-feather="edit"></i>
                    </span>
                </label>

            </div>
        </div>
    </div>


    <div class="row">

        <div class="col-md-12">
            <div class="form-group">
                <label for="url_unica"><b>(*) URL única</b> <code>Ingrese la URL pública de la noticia.</code> </label>
                <input type="text" class="form-control" value="{{ $data_services->url }}" disabled />
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="titulo_service"><b>(*) Titulo del servicio</b></label>
                <input type="text" class="form-control" id="titulo_service" name="titulo_service"
                    value="{{ $data_services->title }}" required>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="resena"><b>(*) Reseña rápida del servicio</b></label>
                <textarea class="form-control" name="resena" id="resena" maxlength="190" required>{{ $data_services->resena }}</textarea>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="body_service"><b>(*) Descripción completa del
                        servicio</b></label>
                <textarea class="form-control body_service" name="body_service" id="body_service">{{ $data_services->content }}</textarea>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="estado_service"><b>(*) Estado</b></label>
                <select class="form-control select2" name="estado_service" id="estado_service" required>
                    @foreach ($status as $state)
                        @if ($state->id == $data_services->status)
                            <option value="{{ $state->id }}" selected>{{ $state->description }}</option>
                        @else
                            <option value="{{ $state->id }}">{{ $state->description }}</option>
                        @endif
                    @endforeach
                </select>
                <div class="invalid-feedback">Por favor, seleccione un estado para el usuario.</div>
            </div>
        </div>

        <div class="col-12 d-flex flex-sm-row flex-column mt-2">
            <button type="submit" id="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">Actualizar
                servicio</button>

        </div>
    </div>
</form>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/38.0.1/super-build/ckeditor.js"></script>

<script>
    let data_descripcion_completa_edit;

    var changePicture = $('#imagen_service'),
        userAvatar = $('.user-avatar');

    // Change user profile picture
    if (changePicture.length) {
        $(changePicture).on('change', function(e) {
            var reader = new FileReader(),
                files = e.target.files;
            reader.onload = function() {
                if (userAvatar.length) {
                    userAvatar.attr('src', reader.result);
                }
            };
            reader.readAsDataURL(files[0]);
        });
    }

    var tabla_servicios = $('.datatables-servicios');

    $(function() {
        'use strict';

        const forms = document.querySelectorAll('.update-services');

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms).forEach((form) => {
            form.addEventListener('submit', (event) => {

                if (!form.checkValidity()) {
                    event.stopPropagation();
                } else {
                    document.getElementById("submit").disabled = true;

                    var paqueteDeDatos = new FormData();
                    paqueteDeDatos.append('change-picture', $('#imagen_service')[0].files[0]);

                    paqueteDeDatos.append('titulo_service', $('#titulo_service').prop("value"));
                    paqueteDeDatos.append('resena_service', $('#resena').prop("value"));
                    paqueteDeDatos.append('body_service', data_descripcion_completa_edit
                        .getData());
                    paqueteDeDatos.append('estado_service', $('#estado_service').prop('value'));


                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                        }
                    });


                    $.ajax({
                        type: 'POST',
                        url: '/app/webpage/servicios/edit/update/{{ $data_services->code }}',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: paqueteDeDatos,
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            if (data == true) {
                                Swal.fire({
                                    title: "Actualización exitosa",
                                    text: "La información se ha actualizado correctamente",
                                    icon: "success"
                                });

                                setTimeout(() => {
                                    $("#modal-services").modal('hide');
                                    var opciones = tabla_servicios.DataTable();
                                    opciones.ajax.reload();
                                }, 1000);
                            }
                        },
                        error: function(err) {
                            errorHttp(err.status);
                            if (err.status == 422) {
                                document.getElementById('submit').disabled = false;

                                $(".errores").hide();
                                console.log(err.responseJSON);
                                $('#success_message').fadeIn().html(err.responseJSON
                                    .message);
                                console.warn(err.responseJSON.errors);
                                $.each(err.responseJSON.errors, function(i, error) {
                                    var el = $(document).find('[name="' +
                                        i + '"]');
                                    el.after($('<span class="errores" style="color: red;">' +
                                        error[0] + '</span>'));
                                });
                            }
                        },
                    });
                    event.preventDefault();

                }
                event.preventDefault();
                form.classList.add('was-validated');
            }, false);
        });
    });
</script>
