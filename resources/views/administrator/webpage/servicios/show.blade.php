<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">

<form novalidate>
    <div class="media mb-2">
        <img src="https://s3.amazonaws.com/cnp.com.co/{{ $data_services->image }}" alt="users avatar"
            class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="150" />
        <div class="media-body mt-50">
            <h4>Imagen de portada</h4>
        </div>
    </div>


    <div class="row">

        <div class="col-md-12">
            <div class="form-group">
                <label for="url_unica"><b>(*) URL única</b> <code>URL pública de la noticia.</code> </label>
                <input type="text" class="form-control" value="{{ $data_services->url }}" disabled />
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="titulo_service"><b>(*) Titulo del servicio</b></label>
                <br> {{  $data_services->title  }}
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="resena_service"><b>(*) Reseña rápida del servicio</b></label>
                <br> {!! $data_services->resena !!}
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="body_service"><b>(*) Descripción completa del
                        servicio</b></label>
                <br> {!! $data_services->content !!}
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="estado_service"><b>(*) Estado</b></label>
               <input type="text" class="form-control" value="{{ $data_services->getStatus->description }}" disabled>
            </div>
        </div>

    </div>
</form>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="//cdn.ckeditor.com/4.20.1/full/ckeditor.js"></script>
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
