<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="titulo_pregunta"><b>(*) Titulo de la pregunta</b></label>
            <textarea class="form-control titulo_pregunta" disabled>{{ $data->question }}</textarea>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label for="respuesta_pregunta"><b>(*) Respuesta de la pregunta</b></label>
            <br>
            <textarea class="form-control titulo_pregunta" disabled>{{ $data->answer }}</textarea>
        </div>
    </div>


    <div class="col-md-12">
        <div class="form-group">
            <label for="estado_pregunta"><b>(*) Estado</b></label>
            <input type="text" class="form-control" value="{{ $data->getStatus->description }}" disabled>
        </div>
    </div>
</div>
