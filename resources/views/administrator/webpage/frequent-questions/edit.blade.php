<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">


<form class="update-preguntas" novalidate>
    @csrf
    @method('POST')

    <div class="row">
        <input type="hidden" id="id" value="{{ $data->code }}">
        <div class="col-md-12">
            <div class="form-group">
                <label for="titulo_pregunta"><b>(*) Titulo de la pregunta</b></label>
                <textarea class="form-control titulo_pregunta" name="titulo_pregunta" id="titulo_pregunta">{{ $data->question }}</textarea>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="respuesta_pregunta"><b>(*) Respuesta de la pregunta</b></label>
                <textarea class="form-control respuesta_pregunta" name="respuesta_pregunta" id="respuesta_pregunta">{{ $data->answer }}</textarea>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>


        <div class="col-md-12">
            <div class="form-group">
                <label for="estado_pregunta"><b>(*) Estado</b></label>
                <select class="form-control" name="estado_pregunta" id="estado_pregunta" required>
                    @foreach ($status as $state)
                        @if ($data->status == $state->id)
                            <option value="{{ $state->id }}" selected>{{ $state->description }}</option>
                        @else
                            <option value="{{ $state->id }}">{{ $state->description }}</option>
                        @endif
                    @endforeach
                </select>
                <div class="invalid-feedback">Por favor, seleccione un estado para la categoria.
                </div>
            </div>
        </div>

        <div class="col-12 d-flex flex-sm-row flex-column mt-2">
            <button type="submit" id="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">Actualizar
                pregunta</button>
        </div>
    </div>
</form>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset(mix('js/scripts/administrator/webpage/frequent-questions/edit.js')) }}"></script>
