<form class="edicion-usuario" novalidate>
    @csrf
    @method('POST')
    <input type="hidden" name="id_usuario" value="{{ $usuario[0]->id_usuario }}">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="nombre_usuario">Nombre de usuarios</label>
                <input type="text" class="form-control" placeholder="Nombre de usuairo" name="nombre_usuario"
                    id="nombre_usuario" value="{{ $usuario[0]->name }}" required />
                <div class="invalid-feedback">Por favor, ingrese un nombre de usuario válido.</div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="correo_usuario">Correo electronico</label>
                <input type="email" class="form-control" placeholder="123456789" name="correo_usuario"
                    id="correo_usuario" value="{{ $usuario[0]->email }}" required="" />
                <div class="invalid-feedback">La información del correo electronico importante.</div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="password">Contraseña</label>
                <input type="password" class="form-control" placeholder="*****" name="password" id="password" />
                <div class="invalid-feedback">Por favor, ingrese la contraseña.</div>

            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="confirmar_password">Confirmar contraseña</label>
                <input type="password" class="form-control" placeholder="*****" name="confirmar_password"
                    id="confirmar_password" />
                <div class="invalid-feedback">Por favor, confirma la contraseña.</div>

            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="rol_usuario">Rol a asignar</label>
                <select class="form-control" name="rol_usuario" id="rol_usuario" required>
                    @foreach ($roles as $rol)
                        @if ($usuario[0]->id_rol == $rol->id)
                            <option value="{{ $rol->id }}" selected="">{{ $rol->name }}</option>
                        @else
                            <option value="{{ $rol->id }}">{{ $rol->name }}</option>
                        @endif
                    @endforeach
                </select>
                <div class="invalid-feedback">Es importante un rol para el usuario.</div>

            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="estado_usuario">Estado</label>
                <select class="form-control" name="estado_usuario" id="estado_usuario" required>
                    @if ($usuario[0]->condicion == '1')
                        <option value="1" selected="">Activo</option>
                        <option value="0">Inactivo</option>
                    @else
                        <option value="1">Activo</option>
                        <option value="0" selected="">Inactivo</option>
                    @endif
                </select>
                <div class="invalid-feedback">Por favor, seleccione un estado para el usuario.</div>

            </div>
        </div>

        <div class="col-12 d-flex flex-sm-row flex-column mt-2">
            <button type="submit" id="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">Actualizar
                usuario</button>
        </div>
    </div>
</form>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/components/components-navs.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/administrator/administrador/user/edit.js')) }}"></script>
