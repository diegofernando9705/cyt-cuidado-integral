
          <div id="mensajes"></div>
          <form class="actualizar-rol" novalidate>
	            @csrf
	            @method('POST')
	            <div class="row">
		            <div class="form-group col">
		              <label class="form-label" for="basic-addon-name"><b>Nombre del usuario</b></label>
		              <input type="text" id="nombre_rol" class="form-control" aria-describedby="basic-addon-name" required value="{{ $usuario[0]->name }}" readonly="" />
		            </div>
		            <div class="form-group col">
		              <label class="form-label" for="basic-addon-name"><b>Correo del usuario</b></label>
		              <input type="text" class="form-control" aria-describedby="basic-addon-name" value="{{ $usuario[0]->email }}" readonly="" name="nombre_rol" />
		            </div>
	         	</div>

                <div class="form-group">
                    <label for="rol_usuario"><b>Rol asignado</b></label>
                    <select class="form-control" disabled="">
                        @foreach($roles as $rol)
                        	@if($usuario[0]->id_rol == $rol->id)
                            <option value="{{ $rol->id }}" selected="">{{ $rol->name }}</option>
                            @else
                            <option value="{{ $rol->id }}">{{ $rol->name }}</option>
                            @endif                          
                        @endforeach
                    </select>
                    <div class="invalid-feedback">Es importante un rol para el usuario.</div>
                    
                  </div>
                  
                  <div class="form-group">
                    <label for="rol_usuario"><b>Persona asignada</b></label>
                    <select class="form-control" disabled="">
                      <option value="{{ $persona[0]->cedula_doctor }}" selected="">C.C. {{ $persona[0]->cedula_doctor }} - {{ $persona[0]->primer_nombre }} {{ $persona[0]->primer_apellido }}</option>
                    </select>
                    <div class="invalid-feedback">Es importante un rol para el usuario.</div>
                    
                  </div>

             	<div class="form-group">
                    <label for="estado_usuario"><b>Estado</b></label>
                    <select class="form-control" disabled="">
                    	@if($usuario[0]->condicion == '1')
                        <option value="1" selected="">Activo</option>
                        <option value="0">Inactivo</option>
                        @else
                        <option value="1">Activo</option>
                        <option value="0" selected="">Inactivo</option>
                        @endif
                    </select>
                    <div class="invalid-feedback">Por favor, seleccione un estado para el usuario.</div>
                    
                  </div>
          </form>
@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
@endsection