@extends('administrator.layouts.contentLayoutMaster')

@section('title', 'Perfil de Usuario')

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-profile.css')) }}">
@endsection

@section('content')
<div id="user-profile">
  <!-- profile header -->
  <div class="row">
    <div class="col-12">
      <div class="card profile-header mb-2">
        <!-- profile cover photo -->
        <img
          class="card-img-top"
          src="{{asset('images/profile/user-uploads/timeline.jpg')}}"
          alt="User Profile Image"
        />
        <!--/ profile cover photo -->

        <div class="position-relative">
          <!-- profile picture -->
          <div class="profile-img-container d-flex align-items-center">
            <div class="profile-img">
              <img
                src="https://s3.amazonaws.com/cytcuidadointegral.com/{{ auth()->user()->avatar }}"
                class="rounded img-fluid"
                alt="Card image"
              />
            </div>
            <!-- profile title -->
            <div class="profile-title ml-3">
              <h2 class="text-white"></h2>
              <p class="text-white"></p>
            </div>
          </div>
        </div>

        <!-- tabs pill -->
        <div class="profile-header-nav">
          <!-- navbar -->
          <nav class="navbar navbar-expand-md navbar-light justify-content-end justify-content-md-between w-100">
            <button
              class="btn btn-icon navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <i data-feather="align-justify" class="font-medium-5"></i>
            </button>

            <!-- collapse  -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <div class="profile-tabs d-flex justify-content-between flex-wrap mt-1 mt-md-0">
                <ul class="nav nav-pills mb-0">
                  <li class="nav-item">
                    <a class="nav-link font-weight-bold active" href="javascript:void(0)">
                      <span class="d-none d-md-block">Principal</span>
                      <i data-feather="rss" class="d-block d-md-none"></i>
                    </a>
                  </li>
                  <!--
                  <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="javascript:void(0)">
                      <span class="d-none d-md-block">About</span>
                      <i data-feather="info" class="d-block d-md-none"></i>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="javascript:void(0)">
                      <span class="d-none d-md-block">Photos</span>
                      <i data-feather="image" class="d-block d-md-none"></i>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="javascript:void(0)">
                      <span class="d-none d-md-block">Friends</span>
                      <i data-feather="users" class="d-block d-md-none"></i>
                    </a>
                  </li>
                -->
                </ul>
                <!-- edit button --
                <button class="btn btn-primary">
                  <i data-feather="edit" class="d-block d-md-none"></i>
                  <span class="font-weight-bold d-none d-md-block">Edit</span>
                </button>
              -->
              </div>
            </div>
            <!--/ collapse  -->
          </nav>
          <!--/ navbar -->
        </div>
      </div>
    </div>
  </div>
  <!--/ profile header -->

  <!-- profile info section -->
  <section id="profile-info">
    <div class="row">
      <!-- left profile info section -->
      <div class="col-lg-3 col-12 order-2 order-lg-1">
        <!-- about -->
        <div class="card">
          <div class="card-body">
            @if(auth()->user()->id == '1')
              <p>Acá se mostrará la información de los Doctores</p>
            @else
            <h5 class="mb-75">About</h5>
            <p class="card-text">
              Tart I love sugar plum I love oat cake. Sweet ⭐️ roll caramels I love jujubes. Topping cake wafer.
            </p>
            <div class="mt-2">
              <h5 class="mb-75">Joined:</h5>
              <p class="card-text">November 15, 2015</p>
            </div>
            <div class="mt-2">
              <h5 class="mb-75">Lives:</h5>
              <p class="card-text">New York, USA</p>
            </div>
            <div class="mt-2">
              <h5 class="mb-75">Email:</h5>
              <p class="card-text">bucketful@fiendhead.org</p>
            </div>
            <div class="mt-2">
              <h5 class="mb-50">Website:</h5>
              <p class="card-text mb-0">www.pixinvent.com</p>
            </div>
            @endif
          </div>
        </div>
        <!--/ about -->

        <!-- suggestion pages -->
        <div class="card">
          <div class="card-body profile-suggestion">
            @if(auth()->user()->id == '1')
              <p>Acá se mostrará la información de los Doctores</p>
            @else
            <h5 class="mb-2">Pacientes de los doctores</h5>

            @endif
          </div>
        </div>
        <!--/ suggestion pages -->

        <!-- twitter feed card -->
        <div class="card">
            <h5 style="padding-top: 10px; padding-left: 10px; padding-bottom: 10px;">Acciones en la plataforma</h5>
          <div class="card-body" style="height: 500px; overflow: scroll; overflow-x: hidden;">
           

          </div>
        </div>
        <!--/ twitter feed card -->
      </div>
      <!--/ left profile info section -->

      <!-- center profile info section -->
      <div class="col-lg-9 col-12 order-1 order-lg-2">
        
        <div class="card">
          <h5 style="padding-top: 10px; padding-left: 10px; padding-bottom: 10px;">Historias clinicas creadas por usted</h5>
          <div class="card-body">
            <table class="datatables-historiasclinicasprofile table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Codigo HC</th>
                    <th>Cedula paciente</th>
                    <th>Nombre paciente</th>
                    <th>Apellido paciente</th>
                    <th>Estado</th>
                    <th></th>
                  </tr>
                </thead>
              </table>
          </div>
        </div>


      </div>
      <!--/ center profile info section -->

    </div>

  </section>
  <!--/ profile info section -->

    <!-- MODAL HISTORIA CLINICA -->
    <div class="modal fade" id="modal-historia-clinica" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title tituloModal">Información</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" id="contenido-historia-clinica"></div>
          <div class="modal-footer">
            <button class="btn btn-danger" id="generar-pdf" data-id="">EXPORTAR PDF</button>
            <button class="btn btn-success" id="generar-pdf" data-id="">EXPORTAR CSV</button>
            <button class="btn btn-warning" id="generar-pdf" data-id="">ENVIAR POR CORREO</button>
            <button class="btn btn-secondary" id="generar-pdf" data-id="">IMPRIMIR</button>
          </div>
        </div>
        
      </div>
    </div>
  <!-- FIN MODAL CONTENIDO DEL FORMULARIO -->
</div>
@endsection

@section('vendor-script')
  {{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/tables/table-datatables-basic.js')) }}"></script>
@endsection
