@extends('administrator.layouts.contentLayoutMaster')

@section('title', 'Módulo de usuarios')

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">
@endsection

@section('content')
    <!-- users edit start -->
    <section class="app-user-edit">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-pills" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center active" id="account-tab" data-toggle="tab"
                            href="#account" aria-controls="account" role="tab" aria-selected="true">
                            <i data-feather="user"></i><span class="d-none d-sm-block">Formulario de nuevo registro de
                                usuario</span>
                        </a>
                    </li>

                </ul>
                <div class="tab-content">
                    <!-- Account Tab starts -->
                    <div id="mensajes"></div>
                    <div class="tab-pane active" id="account" aria-labelledby="account-tab" role="tabpanel">
                        <form class="registro-usuario" novalidate>
                            @csrf
                            @method('POST')
                            <div class="media mb-2">
                                <img src="{{ asset('/images/avatars/7.png') }}" alt="users avatar"
                                    class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" height="90"
                                    width="90" />
                                <div class="media-body mt-50">
                                    <h4>Foto de perfil</h4>
                                    <div class="col-12 d-flex mt-1 px-0">
                                        <label class="btn btn-primary mr-75 mb-0" for="change-picture">
                                            <span class="d-none d-sm-block">Subir foto</span>
                                            <input class="form-control" type="file" id="change-picture" hidden
                                                accept="image/png, image/jpeg, image/jpg" name="avatar" />
                                            <span class="d-block d-sm-none">
                                                <i class="mr-0" data-feather="edit"></i>
                                            </span>
                                        </label>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nombre_usuario"><b>(*) Nombre de usuario</b></label>
                                        <input type="text" class="form-control" placeholder="Nombre de usuairo"
                                            name="nombre_usuario" id="nombre_usuario" required />
                                        <div class="invalid-feedback">Por favor, ingrese un nombre de usuario válido.</div>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="correo_usuario"><b>(*) Correo electronico</b></label>
                                        <input type="email" class="form-control" placeholder="info@softworldcolombia.com"
                                            name="correo_usuario" id="correo_usuario" required />
                                        <div class="invalid-feedback">La información del correo electronico es importante.
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password"><b>(*) Contraseña</b></label>
                                        <input type="password" class="form-control" placeholder="*****" name="password"
                                            id="password" required />
                                        <div class="invalid-feedback">Por favor, ingrese la contraseña.</div>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="confirmar_password"><b>(*) Confirmar contraseña</b></label>
                                        <input type="password" class="form-control" placeholder="*****"
                                            name="confirmar_password" id="confirmar_password" required />
                                        <div class="invalid-feedback">Por favor, confirma la contraseña.</div>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="rol_usuario"><b>(*) Rol a asignar</b></label>
                                        <select class="form-control select2" name="rol_usuario" id="rol_usuario" required>
                                          <option value="" selected=""></option>
                                            @foreach ($roles as $rol)
                                                <option value="{{ $rol->id }}">{{ $rol->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">Es importante un rol para el usuario.</div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="persona_asignada"><b>(*) Asignar a</b></label>
                                        <select class="form-control select2" name="persona_asignada" id="persona_asignada"
                                            required>
                                            <option value="" selected=""></option>
                                            @foreach ($profesionales as $profesional)
                                                <option value="{{ $profesional->id }}">C.C:
                                                    {{ $profesional->id }} - {{ $profesional->first_name }}
                                                    {{ $profesional->second_name }} {{ $profesional->first_last_name }}
                                                    {{ $profesional->second_last_name }} </option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">Es importante un rol para el usuario.</div>

                                    </div>
                                </div>

                                <div class="col-12 d-flex flex-sm-row flex-column mt-2">
                                    <button type="submit" id="submit"
                                        class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">Registrar nuevo usuario</button>
                                    <a href="{{ route('usuarios-list') }}">
                                        <button type="button" class="btn btn-danger" id="cancelar">Cancelar</button>
                                    </a>
                                </div>
                            </div>
                        </form>
                        <!-- users edit account form ends -->
                    </div>
                    <!-- Account Tab ends -->

                    <!-- Basic toast -->
                    <div class="toast toast-basic hide position-fixed registro-exitoso" role="alert"
                        aria-live="assertive" aria-atomic="true" data-delay="5000" style="top: 1rem; right: 1rem"
                        id="registro-exitoso">
                        <div class="toast-header bg-success" style="color: white;">
                            <strong class="mr-auto">Notificación</strong>
                            <small class="text-muted">11 mins ago</small>
                            <button type="button" class="ml-1 close" data-dismiss="toast" aria-label="Close"
                                style="color: white;">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="toast-body">¡Felicidades, registro exitoso!</div>
                    </div>
                    <!-- Basic toast ends -->

                </div>
            </div>
        </div>
    </section>
    <!-- users edit ends -->
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset(mix('js/scripts/components/components-navs.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/administrator/administrador/user/create.js')) }}"></script>
@endsection
