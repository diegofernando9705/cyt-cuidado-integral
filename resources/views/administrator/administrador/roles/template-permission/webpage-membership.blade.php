<div class="table-responsive border rounded mt-1">
    <h6 class="py-1 mx-1 mb-0 font-medium-2">
        <i data-feather="lock" class="font-medium-3 mr-25"></i>
        <span class="align-middle">Permisos asignación membresías</span>
    </h6>
    <table class="table table-striped table-borderless table-hover">
        <thead class="thead-light">
            <tr>
                <th>Acción</th>
                <th>Pemiso adicional</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    Permitir asignar membresía a Noticias
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        @isset($permisos_asignados)
                            @if (is_array($permisos_asignados) && in_array('asignar-membresia-noticia', $permisos_asignados))
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="asignar-membresia-noticia"
                                        value="asignar-membresia-noticia" name="permisos[]" checked="" />
                                    <label class="custom-control-label" for="asignar-membresia-noticia"></label>
                                </div>
                            @else
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="asignar-membresia-noticia"
                                        value="asignar-membresia-noticia" name="permisos[]" />
                                    <label class="custom-control-label" for="asignar-membresia-noticia"></label>
                                </div>
                            @endif
                        @else
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="asignar-membresia-noticia"
                                    value="asignar-membresia-noticia" name="permisos[]" />
                                <label class="custom-control-label" for="asignar-membresia-noticia"></label>
                            </div>
                        @endisset
                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    Permitir asignar membresía a Premios
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        @isset($permisos_asignados)
                            @if (is_array($permisos_asignados) && in_array('asignar-membresia-premios', $permisos_asignados))
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="asignar-membresia-premios"
                                        value="asignar-membresia-premios" name="permisos[]" checked="" />
                                    <label class="custom-control-label" for="asignar-membresia-premios"></label>
                                </div>
                            @else
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="asignar-membresia-premios"
                                        value="asignar-membresia-premios" name="permisos[]" />
                                    <label class="custom-control-label" for="asignar-membresia-premios"></label>
                                </div>
                            @endif
                        @else
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="asignar-membresia-noticia"
                                    value="asignar-membresia-noticia" name="permisos[]" />
                                <label class="custom-control-label" for="asignar-membresia-noticia"></label>
                            </div>
                        @endisset
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Permitir asignar membresía a Conceptos
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        @isset($permisos_asignados)
                            @if (is_array($permisos_asignados) && in_array('asignar-membresia-concepto', $permisos_asignados))
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="asignar-membresia-concepto"
                                        value="asignar-membresia-concepto" name="permisos[]" checked="" />
                                    <label class="custom-control-label" for="asignar-membresia-concepto"></label>
                                </div>
                            @else
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="asignar-membresia-concepto"
                                        value="asignar-membresia-concepto" name="permisos[]" />
                                    <label class="custom-control-label" for="asignar-membresia-concepto"></label>
                                </div>
                            @endif
                        @else
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="asignar-membresia-concepto"
                                    value="asignar-membresia-concepto" name="permisos[]" />
                                <label class="custom-control-label" for="asignar-membresia-concepto"></label>
                            </div>
                        @endisset
                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    Permitir asignar membresía a Cursos
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        @isset($permisos_asignados)
                            @if (is_array($permisos_asignados) && in_array('asignar-membresia-cursos', $permisos_asignados))
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="asignar-membresia-cursos"
                                        value="asignar-membresia-cursos" name="permisos[]" checked="" />
                                    <label class="custom-control-label" for="asignar-membresia-cursos"></label>
                                </div>
                            @else
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="asignar-membresia-cursos"
                                        value="asignar-membresia-cursos" name="permisos[]" />
                                    <label class="custom-control-label" for="asignar-membresia-cursos"></label>
                                </div>
                            @endif
                        @else
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="asignar-membresia-noticia"
                                    value="asignar-membresia-noticia" name="permisos[]" />
                                <label class="custom-control-label" for="asignar-membresia-noticia"></label>
                            </div>
                        @endisset
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
