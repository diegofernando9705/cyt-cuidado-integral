<div class="table-responsive border rounded mt-1">
    <h6 class="py-1 mx-1 mb-0 font-medium-2">
        <i data-feather="lock" class="font-medium-3 mr-25"></i>
        <span class="align-middle">Otros Permisos de la plataforma</span>
    </h6>
    <table class="table table-striped table-borderless table-hover">
        <thead class="thead-light">
            <tr>
                <th>Acción</th>
                <th>Pemiso adicional</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    Permitir importar indicadores económicos
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        @isset($permisos_asignados)
                            @if (is_array($permisos_asignados) && in_array('indicadores-economicos-import', $permisos_asignados))
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="indicadores-economicos-import"
                                        value="indicadores-economicos-import" name="permisos[]" checked="" />
                                    <label class="custom-control-label" for="indicadores-economicos-import"></label>
                                </div>
                            @else
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="indicadores-economicos-import"
                                        value="indicadores-economicos-import" name="permisos[]" />
                                    <label class="custom-control-label" for="indicadores-economicos-import"></label>
                                </div>
                            @endif
                        @else
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="indicadores-economicos-import"
                                    value="indicadores-economicos-import" name="permisos[]" />
                                <label class="custom-control-label" for="indicadores-economicos-import"></label>
                            </div>
                        @endisset
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
