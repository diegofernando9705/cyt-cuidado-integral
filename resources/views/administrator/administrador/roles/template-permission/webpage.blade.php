<div class="table-responsive border rounded mt-1">
    <h6 class="py-1 mx-1 mb-0 font-medium-2">
        <i data-feather="lock" class="font-medium-3 mr-25"></i>
        <span class="align-middle">Permisos módulo Pagina Web</span>
    </h6>
    <table class="table table-striped table-borderless table-hover">
        <thead class="thead-light">
            <tr>
                <th>Acción</th>
                <th>Pemiso adicional</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    Permitir acceder al módulo principal
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        @isset($permisos_asignados)
                            @if (is_array($permisos_asignados) && in_array('paginaweb-banner', $permisos_asignados))
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner"
                                        value="paginaweb-banner" name="permisos[]" checked="" />
                                    <label class="custom-control-label" for="paginaweb-banner"></label>
                                </div>
                            @else
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner"
                                        value="paginaweb-banner" name="permisos[]" />
                                    <label class="custom-control-label" for="paginaweb-banner"></label>
                                </div>
                            @endif
                        @else
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="paginaweb-banner"
                                    value="paginaweb-banner" name="permisos[]" />
                                <label class="custom-control-label" for="paginaweb-banner"></label>
                            </div>
                        @endisset
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Permitir acceder a la actualización
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        @isset($permisos_asignados)
                            @if (is_array($permisos_asignados) && in_array('paginaweb-banner-access', $permisos_asignados))
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner-access"
                                        value="paginaweb-banner-access" name="permisos[]" checked="" />
                                    <label class="custom-control-label" for="paginaweb-banner-access"></label>
                                </div>
                            @else
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner-access"
                                        value="paginaweb-banner-access" name="permisos[]" />
                                    <label class="custom-control-label" for="paginaweb-banner-access"></label>
                                </div>
                            @endif
                        @else
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="paginaweb-banner-access"
                                    value="paginaweb-banner-access" name="permisos[]" />
                                <label class="custom-control-label" for="paginaweb-banner-access"></label>
                            </div>
                        @endisset
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Permitir actualizar banner Inicio
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        @isset($permisos_asignados)
                            @if (is_array($permisos_asignados) && in_array('paginaweb-banner-inicio', $permisos_asignados))
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner-inicio"
                                        value="paginaweb-banner-inicio" name="permisos[]" checked="" />
                                    <label class="custom-control-label" for="paginaweb-banner-inicio"></label>
                                </div>
                            @else
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner-inicio"
                                        value="paginaweb-banner-inicio" name="permisos[]" />
                                    <label class="custom-control-label" for="paginaweb-banner-inicio"></label>
                                </div>
                            @endif
                        @else
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="paginaweb-banner-inicio"
                                    value="paginaweb-banner-inicio" name="permisos[]" />
                                <label class="custom-control-label" for="paginaweb-banner-inicio"></label>
                            </div>
                        @endisset
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Permitir actualizar banner Nosotros
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        @isset($permisos_asignados)
                            @if (is_array($permisos_asignados) && in_array('paginaweb-banner-nosotros', $permisos_asignados))
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner-nosotros"
                                        value="paginaweb-banner-nosotros" name="permisos[]" checked="" />
                                    <label class="custom-control-label" for="paginaweb-banner-nosotros"></label>
                                </div>
                            @else
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner-nosotros"
                                        value="paginaweb-banner-nosotros" name="permisos[]" />
                                    <label class="custom-control-label" for="paginaweb-banner-nosotros"></label>
                                </div>
                            @endif
                        @else
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="paginaweb-banner-nosotros"
                                    value="paginaweb-banner-nosotros" name="permisos[]" />
                                <label class="custom-control-label" for="paginaweb-banner-nosotros"></label>
                            </div>
                        @endisset
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Permitir actualizar banner Servicios
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        @isset($permisos_asignados)
                            @if (is_array($permisos_asignados) && in_array('paginaweb-banner-servicios', $permisos_asignados))
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner-servicios"
                                        value="paginaweb-banner-servicios" name="permisos[]" checked="" />
                                    <label class="custom-control-label" for="paginaweb-banner-servicios"></label>
                                </div>
                            @else
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner-servicios"
                                        value="paginaweb-banner-servicios" name="permisos[]" />
                                    <label class="custom-control-label" for="paginaweb-banner-servicios"></label>
                                </div>
                            @endif
                        @else
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="paginaweb-banner-servicios"
                                    value="paginaweb-banner-servicios" name="permisos[]" />
                                <label class="custom-control-label" for="paginaweb-banner-servicios"></label>
                            </div>
                        @endisset
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Permitir actualizar banner Noticias
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        @isset($permisos_asignados)
                            @if (is_array($permisos_asignados) && in_array('paginaweb-banner-noticias', $permisos_asignados))
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner-noticias"
                                        value="paginaweb-banner-noticias" name="permisos[]" checked="" />
                                    <label class="custom-control-label" for="paginaweb-banner-noticias"></label>
                                </div>
                            @else
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner-noticias"
                                        value="paginaweb-banner-noticias" name="permisos[]" />
                                    <label class="custom-control-label" for="paginaweb-banner-noticias"></label>
                                </div>
                            @endif
                        @else
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="paginaweb-banner-noticias"
                                    value="paginaweb-banner-noticias" name="permisos[]" />
                                <label class="custom-control-label" for="paginaweb-banner-noticias"></label>
                            </div>
                        @endisset
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Permitir actualizar banner Preguntas fecuentes
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        @isset($permisos_asignados)
                            @if (is_array($permisos_asignados) && in_array('paginaweb-banner-preguntasfrecuentes', $permisos_asignados))
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner-preguntasfrecuentes"
                                        value="paginaweb-banner-preguntasfrecuentes" name="permisos[]" checked="" />
                                    <label class="custom-control-label" for="paginaweb-banner-preguntasfrecuentes"></label>
                                </div>
                            @else
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner-preguntasfrecuentes"
                                        value="paginaweb-banner-preguntasfrecuentes" name="permisos[]" />
                                    <label class="custom-control-label" for="paginaweb-banner-preguntasfrecuentes"></label>
                                </div>
                            @endif
                        @else
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="paginaweb-banner-preguntasfrecuentes"
                                    value="paginaweb-banner-preguntasfrecuentes" name="permisos[]" />
                                <label class="custom-control-label" for="paginaweb-banner-preguntasfrecuentes"></label>
                            </div>
                        @endisset
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Permitir actualizar banner Contáctanos
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        @isset($permisos_asignados)
                            @if (is_array($permisos_asignados) && in_array('paginaweb-banner-contactanos', $permisos_asignados))
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner-contactanos"
                                        value="paginaweb-banner-contactanos" name="permisos[]" checked="" />
                                    <label class="custom-control-label" for="paginaweb-banner-contactanos"></label>
                                </div>
                            @else
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="paginaweb-banner-contactanos"
                                        value="paginaweb-banner-contactanos" name="permisos[]" />
                                    <label class="custom-control-label" for="paginaweb-banner-contactanos"></label>
                                </div>
                            @endif
                        @else
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="paginaweb-banner-contactanos"
                                    value="paginaweb-banner-contactanos" name="permisos[]" />
                                <label class="custom-control-label" for="paginaweb-banner-contactanos"></label>
                            </div>
                        @endisset
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
