@extends('administrator.layouts.contentLayoutMaster')

@section('title', 'Modulo Roles')

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
            <!-- Bootstrap Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">A continuación procederá a actualizar los permisos del rol
                            <code>{{ $rol[0]->name }}</code>.
                        </h4>
                    </div>
                    <div class="card-body">
                        <div id="mensajes"></div>
                        <form class="actualizar-rol" novalidate>
                            @csrf
                            @method('POST')
                            <input type="hidden" name="codigo" value="{{ $rol[0]->id }}">
                            <div class="form-group">
                                <label class="form-label" for="basic-addon-name"><b>(*) Nombre del rol</b></label>
                                <input type="text" id="nombre_rol" class="form-control" placeholder="Nombre del rol"
                                    aria-describedby="basic-addon-name" required value="{{ $rol[0]->name }}" readonly=""
                                    name="nombre_rol" />
                                <div class="valid-feedback"></div>
                                <div class="invalid-feedback">Por favor, ingrese el nombre del rol.</div>
                            </div>
                            <div class="col-12">
                                <div class="table-responsive border rounded mt-1">
                                    <h6 class="py-1 mx-1 mb-0 font-medium-2">
                                        <i data-feather="lock" class="font-medium-3 mr-25"></i>
                                        <span class="align-middle">Permisos de la plataforma</span>
                                    </h6>
                                    <table class="table table-striped table-borderless table-hover">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>Modulo</th>
                                                <th>Permitir acceso</th>
                                                <th>Listado principal</th>
                                                <th>Crear</th>
                                                <th>Ver</th>
                                                <th>Editar</th>
                                                <th>Eliminar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($modulos as $modulo)
                                                <tr>
                                                    <td>{{ $modulo->nombre_modulo }}</td>
                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <?php if (is_array($permisos_asignados) && in_array($modulo->nombre_modulo."-access", $permisos_asignados)) { ?>
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-access"
                                                                value="{{ $modulo->nombre_modulo }}-access"
                                                                name="permisos[]" checked="" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-access"></label>
                                                            <?php }else{ ?>
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-access"
                                                                value="{{ $modulo->nombre_modulo }}-access"name="permisos[]" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-access"></label>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <?php if (is_array($permisos_asignados) && in_array($modulo->nombre_modulo."-list", $permisos_asignados)) { ?>
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-list"
                                                                value="{{ $modulo->nombre_modulo }}-list" name="permisos[]"
                                                                checked="" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-list"></label>
                                                            <?php }else{ ?>
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-list"
                                                                value="{{ $modulo->nombre_modulo }}-list"name="permisos[]" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-list"></label>
                                                            <?php } ?>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <?php if (is_array($permisos_asignados) && in_array($modulo->nombre_modulo."-create", $permisos_asignados)) { ?>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-create"
                                                                value="{{ $modulo->nombre_modulo }}-create"
                                                                name="permisos[]" checked="" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-create"></label>
                                                        </div>
                                                        <?php }else{ ?>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-create"
                                                                value="{{ $modulo->nombre_modulo }}-create"
                                                                name="permisos[]" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-create"></label>
                                                        </div>
                                                        <?php } ?>
                                                    </td>

                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <?php if (is_array($permisos_asignados) && in_array($modulo->nombre_modulo."-read", $permisos_asignados)) { ?>
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-read"
                                                                value="{{ $modulo->nombre_modulo }}-read" name="permisos[]"
                                                                checked="" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-read"></label>
                                                            <?php }else{ ?>
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-read"
                                                                value="{{ $modulo->nombre_modulo }}-read"name="permisos[]" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-read"></label>
                                                            <?php } ?>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <?php if (is_array($permisos_asignados) && in_array($modulo->nombre_modulo."-update", $permisos_asignados)) { ?>
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-update"
                                                                value="{{ $modulo->nombre_modulo }}-update" name="permisos[]"
                                                                checked="" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-update"></label>
                                                            <?php }else{ ?>
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-update"
                                                                value="{{ $modulo->nombre_modulo }}-update"
                                                                name="permisos[]" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-update"></label>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                   
                                                    <td>
                                                        <?php if (is_array($permisos_asignados) && in_array($modulo->nombre_modulo."-delete", $permisos_asignados)) { ?>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-delete"
                                                                value="{{ $modulo->nombre_modulo }}-delete"
                                                                name="permisos[]" checked="" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-delete"></label>
                                                        </div>
                                                        <?php }else{ ?>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-delete"
                                                                value="{{ $modulo->nombre_modulo }}-delete"
                                                                name="permisos[]" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-delete"></label>
                                                        </div>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                @include("administrator.administrador.roles.template-permission.index")
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-12">
                                    <center>
                                        <button type="submit" class="btn btn-primary"
                                            id="registro-btn">Actualizar</button>
                                        <a href="{{ route('roles-list') }}">
                                            <button type="button" class="btn btn-danger"
                                                id="regreso-btn">Regresar</button>
                                        </a>
                                    </center>
                                </div>
                            </div>
                        </form>

                        <!-- Basic toast -->
                        <div class="toast toast-basic hide position-fixed actualizacion-exitosa" role="alert"
                            aria-live="assertive" aria-atomic="true" data-delay="5000" style="top: 1rem; right: 1rem"
                            id="actualizacion-exitosa">
                            <div class="toast-header bg-success" style="color: white;">
                                <strong class="mr-auto">Notificación</strong>
                                <small class="text-muted">Justo ahora</small>
                                <button type="button" class="ml-1 close" data-dismiss="toast" aria-label="Close"
                                    style="color: white;">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="toast-body">¡Felicidades, actualización exitosa!</div>
                        </div>
                        <!-- Basic toast ends -->
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- /Validation -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="{{ asset(mix('js/scripts/administrator/administrador/role/edit.js')) }}"></script>
@endsection
