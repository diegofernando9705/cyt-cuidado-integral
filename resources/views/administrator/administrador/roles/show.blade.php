
          <div id="mensajes"></div>
          <form class="actualizar-rol" novalidate>
	            @csrf
	            @method('POST')
	            <input type="hidden" name="codigo" value="{{ $rol[0]->id }}">
	            <div class="form-group">
	              <label class="form-label" for="basic-addon-name"><b>(*) Nombre del rol</b></label>
	              <input type="text" id="nombre_rol" class="form-control" placeholder="Nombre del rol" aria-describedby="basic-addon-name" required value="{{ $rol[0]->name }}" readonly="" name="nombre_rol" />
	              <div class="valid-feedback"></div>
	              <div class="invalid-feedback">Por favor, ingrese el nombre del rol.</div>
	            </div>
	            <div class="col-12">
	                <div class="table-responsive border rounded mt-1">
	                  <h6 class="py-1 mx-1 mb-0 font-medium-2">
	                    <i data-feather="lock" class="font-medium-3 mr-25"></i>
	                    <span class="align-middle">Permisos de la plataforma</span>
	                  </h6>
	                  <table class="table table-striped table-borderless table-hover">
	                    <thead class="thead-light">
	                      <tr>
	                        <th>Modulo</th>
	                        <th>Permitir acceso</th>
                        	<th>Listado principal</th>
	                        <th>Leer</th>
	                        <th>Editar</th>
	                        <th>Crear</th>
	                        <th>Eliminar</th>
	                      </tr>
	                    </thead>
	                    <tbody>
	                      @foreach($modulos as $modulo)
	                        <tr>
	                          <td>{{ $modulo->nombre_modulo }}</td>
	                          <td>
	                              <div class="custom-control custom-checkbox">
	                      			<?php if (is_array($permisos_asignados) && in_array($modulo->nombre_modulo."-access", $permisos_asignados)) { ?>
	                                	<input type="checkbox" class="custom-control-input" id="{{ $modulo->nombre_modulo }}-access" value="{{ $modulo->nombre_modulo }}-access" name="permisos[]" checked=""  disabled/>
	                                	<label class="custom-control-label" for="{{ $modulo->nombre_modulo }}-access"></label>
	                                <?php }else{ ?> 
	                                	<input type="checkbox" class="custom-control-input" id="{{ $modulo->nombre_modulo }}-access" value="{{ $modulo->nombre_modulo }}-access"name="permisos[]"  disabled/>
	                                	<label class="custom-control-label" for="{{ $modulo->nombre_modulo }}-access"></label>
	                                <?php } ?>
	                              </div>
	                            </td>
	                            <td>
	                              <div class="custom-control custom-checkbox">
	                                <?php if (is_array($permisos_asignados) && in_array($modulo->nombre_modulo."-list", $permisos_asignados)) { ?>
	                                  <input type="checkbox" class="custom-control-input" id="{{ $modulo->nombre_modulo }}-list" value="{{ $modulo->nombre_modulo }}-list" name="permisos[]" checked="" disabled="" />
	                                  <label class="custom-control-label" for="{{ $modulo->nombre_modulo }}-list"></label>
	                                <?php }else{ ?> 
	                                  <input type="checkbox" class="custom-control-input" id="{{ $modulo->nombre_modulo }}-list" value="{{ $modulo->nombre_modulo }}-list"name="permisos[]" disabled="" />
	                                  <label class="custom-control-label" for="{{ $modulo->nombre_modulo }}-list"></label>
	                                <?php } ?>
	                              </div>
	                            </td>
	                            
	                            <td>
	                              <div class="custom-control custom-checkbox">
	                                <?php if (is_array($permisos_asignados) && in_array($modulo->nombre_modulo."-show", $permisos_asignados)) { ?>
	                                  <input type="checkbox" class="custom-control-input" id="{{ $modulo->nombre_modulo }}-show" value="{{ $modulo->nombre_modulo }}-show" name="permisos[]" checked="" disabled="" />
	                                  <label class="custom-control-label" for="{{ $modulo->nombre_modulo }}-show"></label>
	                                <?php }else{ ?> 
	                                  <input type="checkbox" class="custom-control-input" id="{{ $modulo->nombre_modulo }}-show" value="{{ $modulo->nombre_modulo }}-show"name="permisos[]" disabled="" />
	                                  <label class="custom-control-label" for="{{ $modulo->nombre_modulo }}-show"></label>
	                                <?php } ?>
	                              </div>
	                            </td>

	                            <td>
	                              <div class="custom-control custom-checkbox">
	                              	<?php if (is_array($permisos_asignados) && in_array($modulo->nombre_modulo."-edit", $permisos_asignados)) { ?>
	                                	<input type="checkbox" class="custom-control-input" id="{{ $modulo->nombre_modulo }}-edit" value="{{ $modulo->nombre_modulo }}-edit" name="permisos[]" checked=""  disabled/>
	                                	<label class="custom-control-label" for="{{ $modulo->nombre_modulo }}-edit"></label>
	                                <?php }else{ ?> 
	                                	<input type="checkbox" class="custom-control-input" id="{{ $modulo->nombre_modulo }}-edit" value="{{ $modulo->nombre_modulo }}-edit" name="permisos[]" disabled/>
	                                	<label class="custom-control-label" for="{{ $modulo->nombre_modulo }}-edit"></label>
	                                <?php } ?>
	                              </div>
	                            </td>
	                            <td>
	                            	<?php if (is_array($permisos_asignados) && in_array($modulo->nombre_modulo."-create", $permisos_asignados)) { ?>
	                            		<div class="custom-control custom-checkbox">
	                                		<input type="checkbox" class="custom-control-input" id="{{ $modulo->nombre_modulo }}-create" value="{{ $modulo->nombre_modulo }}-create" name="permisos[]" checked="" disabled />
	                                		<label class="custom-control-label" for="{{ $modulo->nombre_modulo }}-create"></label>
	                                	</div>
	                                <?php }else{ ?> 
	                                	<div class="custom-control custom-checkbox">
	                                		<input type="checkbox" class="custom-control-input" id="{{ $modulo->nombre_modulo }}-create" value="{{ $modulo->nombre_modulo }}-create" name="permisos[]" disabled />
	                                		<label class="custom-control-label" for="{{ $modulo->nombre_modulo }}-create"></label>
	                                	</div>
	                                <?php } ?>
	                            </td>
	                            <td>
	                            	<?php if (is_array($permisos_asignados) && in_array($modulo->nombre_modulo."-delete", $permisos_asignados)) { ?>
	                            		<div class="custom-control custom-checkbox">
	                            			<input type="checkbox" class="custom-control-input" id="{{ $modulo->nombre_modulo }}-delete" value="{{ $modulo->nombre_modulo }}-delete" name="permisos[]" checked="" disabled />
	                            			<label class="custom-control-label" for="{{ $modulo->nombre_modulo }}-delete"></label>
	                            		</div>
	                            	<?php }else{ ?> 
	                            		<div class="custom-control custom-checkbox">
	                            			<input type="checkbox" class="custom-control-input" id="{{ $modulo->nombre_modulo }}-destroy" value="{{ $modulo->nombre_modulo }}-delete" name="permisos[]" disabled/>
	                            			<label class="custom-control-label" for="{{ $modulo->nombre_modulo }}-delete"></label>
	                            		</div>
	                            	<?php } ?>
	                          </td>
	                        </tr>
	                      @endforeach
	                    </tbody>
	                  </table>
	                </div>
	              </div>
	              <br>
          </form>

        

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
@endsection