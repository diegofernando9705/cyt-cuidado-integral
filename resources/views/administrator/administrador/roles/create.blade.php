@extends('administrator.layouts.contentLayoutMaster')

@section('title', 'Nuevo registro')

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
            <!-- Bootstrap Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Para crear un nuevo rol en la plataforma, con sus respectivos permisos.</h4>
                    </div>
                    <div class="card-body">
                        <div id="mensajes"></div>
                        <form class="registro-rol" novalidate>
                            @csrf
                            @method('POST')
                            <div class="form-group">
                                <label class="form-label" for="basic-addon-name"><b>(*) Nombre del rol</b></label>
                                <input type="text" id="nombre_rol" class="form-control" placeholder="Nombre del rol"
                                    name="nombre_rol" aria-describedby="basic-addon-name" required />
                                <div class="valid-feedback"></div>
                                <div class="invalid-feedback">Por favor, ingrese el nombre del rol.</div>
                            </div>
                            <div class="col-12">
                                <div class="table-responsive border rounded mt-1">
                                    <h6 class="py-1 mx-1 mb-0 font-medium-2">
                                        <i data-feather="lock" class="font-medium-3 mr-25"></i>
                                        <span class="align-middle">Permisos de la plataforma</span>
                                    </h6>
                                    <table class="table table-striped table-borderless table-hover">
                                        <thead class="thead-light ">
                                            <tr>
                                                <th>Modulo</th>
                                                <th>Permitir acceso</th>
                                                <th>Listado principal</th>
                                                <th>Leer</th>
                                                <th>Editar</th>
                                                <th>Crear</th>
                                                <th>Eliminar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($modulos as $modulo)
                                                <tr>
                                                    <td>{{ $modulo->nombre_modulo }}</td>

                                                    <td style="text-align: center; align-content: center;">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-access"
                                                                value="{{ $modulo->nombre_modulo }}-access"name="permisos[]" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-access"></label>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-list"
                                                                value="{{ $modulo->nombre_modulo }}-list"name="permisos[]" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-list"></label>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-show"
                                                                value="{{ $modulo->nombre_modulo }}-show"
                                                                name="permisos[]" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-show"></label>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-edit"
                                                                value="{{ $modulo->nombre_modulo }}-edit"
                                                                name="permisos[]" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-edit"></label>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-create"
                                                                value="{{ $modulo->nombre_modulo }}-create"
                                                                name="permisos[]" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-create"></label>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="{{ $modulo->nombre_modulo }}-delete"
                                                                value="{{ $modulo->nombre_modulo }}-delete"
                                                                name="permisos[]" />
                                                            <label class="custom-control-label"
                                                                for="{{ $modulo->nombre_modulo }}-delete"></label>
                                                        </div>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @include("administrator.administrador.roles.template-permission.index")
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-12">
                                    <center>
                                        <button type="submit" class="btn btn-primary" id="registro-btn">Registrar</button>
                                    </center>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- /Validation -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="{{ asset(mix('js/scripts/administrator/administrador/role/create.js')) }}"></script>

@endsection
