<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
<style>
    .select2-selection__arrow {
        display: none;
    }

    .modal-body {
        max-height: calc(100vh - 210px);
        overflow-y: auto;
    }
</style>

<section class="bs-validation">
    <div class="row">
        <!-- Bootstrap Validation -->
        <div class="col-md-12 col-12">
            <div class="card">
                <input type="hidden" id="id_course" value="{{ $course->code }}">
                @if (count($course->getModules) <= 0)
                    <div class="alert alert-danger" style="padding:8px;">
                        <p>El curso <b><i>{{ $course->title }}</i></b> no tiene módulos asignados temporalmente.</p>
                    </div>
                @endif

                @foreach ($course->getModules as $module)
                    <div class="accordion btn btn-primary" style="margin-bottom: 10px;">
                        <div class="row">
                            <div class="titulo col-10 col-md-10 col-lg-10 col-xl-10" data-toggle="collapse"
                                href="#collapse{{ $module->code }}" aria-controls="collapse{{ $module->code }}"
                                id="title_seccion_{{ $module->code }}">
                                Módulo: {{ $module->title }}
                            </div>
                            <div class="botones_opciones col-2 col-md-2 col-lg-2 col-xl-2">
                                <button class="btn btn-info btn-sm edit_option" data-id="{{ $module->code }}"
                                    code-course="{{ $course->code }}"><i class="bi bi-pencil-square"></i></button>
                                <button class="btn btn-danger btn-sm delete_option" data-id="{{ $module->code }}"
                                    code-course="{{ $course->code }}"><i class="bi bi-trash3"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="collapse" id="collapse{{ $module->code }}">
                        <div class="card card-body">
                            <div class="row">
                                <div class="image col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <img src="{{ config('app.AWS_BUCKET_URL') . $module->image }}"
                                        alt="Imagen del modulo" width="100%;">
                                </div>
                                <div class="description  col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <label for=""> <b>Descripcion del módulo:</b> </label>
                                    <p id="descripcion_seccion_{{ $module->code }}">{{ $module->summary }}
                                    </p>
                                </div>
                                <hr>
                                <b>Lecciones del módulo:</b>
                                <hr>
                                <table class="table tavle-striped">
                                    <thead>
                                        <tr>
                                            <th>
                                                Imagen
                                            </th>
                                            <th>
                                                Titulo
                                            </th>
                                            <th>
                                                Tipo
                                            </th>
                                            <th>

                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($module->lessons as $lessons)
                                            <tr>
                                                <td>
                                                    <img src="{{ config('app.AWS_BUCKET_URL') . $lessons->image }}"
                                                        alt="Imagen de la leccion" width="150px">
                                                </td>
                                                <td>
                                                    {{ $lessons->title }}
                                                </td>
                                                <td>
                                                    {{ $lessons->type }}
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-sm edit_lesson"
                                                        data-code="{{ $lessons->code }}">EDIT</button>
                                                    <button type="button" class="btn btn-danger btn-sm delete_lesson"
                                                        data-code="{{ $lessons->code }}">DEL</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <button type="button"
                                    class="btn btn-warning btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 button_add_new_lessons"
                                    data-module="{{ $module->code }}" style="margin-top:5px;">Agregar una nueva
                                    lección</button>
                            </div>
                        </div>
                    </div>
                @endforeach
                <button type="button" class="btn btn-danger btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
                    id="add_module">Agregar módulo
                    nuevo</button>
            </div>
        </div>
    </div>
</section>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset(mix('js/scripts/administrator/learning/courses/edit.js')) }}"></script>
