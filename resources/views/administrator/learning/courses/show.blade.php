<div class="row">
    <div class="imagen_curso col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <img src="{{ config('app.AWS_BUCKET_URL') . $course->cover_image }}" alt="" style="width:100%">
    </div>
    @if ($course->video_image)
        <div class="imagen_curso col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <video src="{{ config('app.AWS_BUCKET_URL') . $course->video_image }}" style="width:100%" controls></video>
        </div>
    @endif
</div>

<div class="row">
    <div class="informacion_curso col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <table class="table table-hovered">
            <tr>
                <td>
                    <b>Titulo del curso: </b> {{ $course->title }}
                </td>
            </tr>
            <tr>
                <td>
                    <b>Resumen del curso: </b> {!! $course->summary !!}
                </td>
            </tr>
            <tr>
                <td>
                    <b>Descripción del curso: </b> {!! $course->description !!}
                </td>
            </tr>
            <tr>
                <td>
                    <b>Fecha inicial del curso: </b> {{ $course->start_date }}
                </td>
            </tr>
            <tr>
                <td>
                    <b>Fecha fin del curso: </b> {{ $course->end_date }}
                </td>
            </tr>
            <tr>
                <td>
                    <b>Categoria del curso: </b> {{ $course->getCategory->title }}
                </td>
            </tr>
            <tr>
                <td>
                    <b>Nivel del curso: </b> {{ $course->getLevel->title }}
                </td>
            </tr>
            <tr>
                <td>
                    <b>Limite de matriculados: </b> {{ $course->student_limit }}
                </td>
            </tr>
            <tr>
                <td>
                    <b>Profesor del curso: </b>
                    {{ $course->getInstructor->first_name . ' ' . $course->getInstructor->first_last_name }}
                </td>
            </tr>
            <tr>
                <td>
                    <b>Archivos del curso: </b>
                    @foreach ($course->getArchives as $archive)
                        <a class="btn btn-info" href="{{ config('app.AWS_BUCKET_URL') . $archive->url }}"
                            target="_blank">Abrir documento</a> &nbsp;
                    @endforeach
                </td>
            </tr>
            <tr>
                <td>
                    <b>Estado del curso: </b> {{ $course->getStatus->description }}
                </td>
            </tr>
        </table>

    </div>
</div>
