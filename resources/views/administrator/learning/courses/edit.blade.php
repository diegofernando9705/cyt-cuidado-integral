<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">

<style>
    .select2-selection__arrow {
        display: none;
    }

    .modal-body {
        max-height: calc(100vh - 210px);
        overflow-y: auto;
    }
</style>

<section class="bs-validation">
    <div class="row">
        <!-- Bootstrap Validation -->
        <div class="col-md-12 col-12">
            <div class="card">
                <form class="update-course" novalidate>
                    @csrf
                    @method('POST')

                    <input type="hidden" id="code" name="code" value="{{ $course->code }}">

                    <div class="media mb-2">
                        <img src="{{ config('app.AWS_BUCKET_URL') . $course->cover_image }}" alt="users avatar"
                            class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="150" />
                        <div class="media-body mt-50">
                            <h4>Imagen de portada</h4>
                            <div class="col-12 d-flex mt-1 px-0">
                                <label class="btn btn-primary mr-75 mb-0" for="imagen_curso">
                                    <span class="d-none d-sm-block">Subir foto</span>
                                    <input class="form-control" type="file" id="imagen_curso" name="imagen_curso"
                                        hidden accept="image/png, image/jpeg, image/jpg" name="avatar" />
                                    <span class="d-block d-sm-none">
                                        <i class="mr-0" data-feather="edit"></i>
                                    </span>
                                    <div class="invalid-feedback" style="color: white;">Por favor, seleccione una
                                        imagen de portada.
                                    </div>
                                </label>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>(*) Titulo del curso</b></label>
                                <input type="text" class="form-control" id="title" name="title" required
                                    value="{{ $course->title }}">
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="summary"><b>(*) Resumen del curso</b></label>
                                <textarea class="form-control" name="summary" id="summary">{!! $course->summary !!}</textarea>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description"><b>(*) Descripción del curso</b></label>
                                <textarea class="form-control description" name="description" id="description">{!! $course->description !!}</textarea>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <label for="video_image"><b>Video presentación del curso</b></label>
                            <div class="form-group">
                                <input type="file" class="form-control" id="video_image" name="video_image"
                                    accept="video/*">
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group col">
                                <label for="start_date"> <b>(*) Fecha de inicio del curso: </b></label>
                                <input type="datetime-local" class="form-control" id="start_date" name="start_date"
                                    required value="{{ $course->start_date }}">
                            </div>

                            <div class="form-group col">
                                <label for="end_date"> <b>(*) Fecha de fin del curso: </b></label>
                                <input type="datetime-local" class="form-control" id="end_date" name="end_date" value="{{ $course->end_date }}">

                            </div>
                        </div>

                        <div class="row col12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group col">
                                <label for="category_id"> <b>(*) Categoria del curso: </b></label>
                                <select class="form-control selector" name="category_id" id="category_id">
                                    @foreach ($categories as $category)
                                        @if ($course->category_id == $category->code)
                                            <option value="{{ $category->code }}" selected>{{ $category->title }}
                                            </option>
                                        @else
                                            <option value="{{ $category->code }}">{{ $category->title }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col">
                                <label for="professional_id"> <b>(*) Responsable del curso: </b></label>
                                <select class="form-control selector" name="professional_id" id="professional_id" required>
                                    @foreach ($professionals as $professional)
                                        @if ($course->instructor_id == $professional->id)
                                            <option value="{{ $professional->id }}" selected>
                                                C.C. {{ $professional->id }} -
                                                {{ $professional->first_name }}
                                                {{ $professional->second_name }}
                                                {{ $professional->first_last_name }}</option>
                                        @else
                                            <option value="{{ $professional->id }}">
                                                C.C. {{ $professional->id }} -
                                                {{ $professional->first_name }}
                                                {{ $professional->second_name }}
                                                {{ $professional->first_last_name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group col">
                            <label for="level"> <b>(*) Nivel del curso: </b></label>
                            <select class="form-control selector" name="level" id="level">
                                @foreach ($levels as $level)
                                    @if ($course->level == $level->id)
                                        <option value="{{ $level->id }}" selected>{{ $level->title }}</option>
                                    @else
                                        <option value="{{ $level->id }}">{{ $level->title }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col">
                            <label for="student_limit"> <b>Limite de estudiantes del curso:
                                </b></label>
                            <input type="number" class="form-control" id="student_limit" name="student_limit"
                                value="{{ $course->student_limit }}">
                            </select>
                        </div>

                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <label for="archivos"> <b> Archivos del curso: </b></label>
                                <input class="form-control" type="file" name="archivos[]" id="archivos" multiple
                                    placeholder="Seleccione archivos para el concepto" accept="application/pdf">
                                <code><small>Solo se permiten archivos PDF</small></code>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="status"><b>(*) Estado</b></label>
                                <select class="form-control" name="status" id="status" required>
                                    @foreach ($status as $state)
                                        @if ($course->status == $state->id)
                                            <option value="{{ $state->id }}" selected>
                                                {{ $state->description }}</option>
                                        @else
                                            <option value="{{ $state->id }}">{{ $state->description }}
                                            </option>
                                        @endif
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">Por favor, seleccione un estado.
                                </div>
                            </div>
                        </div>

                        <button type="submit" id="submit"
                            class="btn btn-primary col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 btn-sm">Actualizar
                            curso</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset(mix('js/scripts/administrator/learning/courses/edit.js')) }}"></script>
