@extends('administrator.layouts.contentLayoutMaster')
@section('title', 'Crear curso virtual')

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
            <!-- Bootstrap Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-body">

                        <form class="store-cursos" novalidate>
                            @csrf
                            @method('POST')

                            <div class="media mb-2">
                                <img src="https://s3.amazonaws.com/cnp.com.co/textocnp.png" alt="users avatar"
                                    class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer"
                                    width="150" />
                                <div class="media-body mt-50">
                                    <h4>Imagen de portada</h4>
                                    <div class="col-12 d-flex mt-1 px-0">
                                        <label class="btn btn-primary mr-75 mb-0" for="imagen_curso">
                                            <span class="d-none d-sm-block">Subir foto</span>
                                            <input class="form-control" type="file" id="imagen_curso" name="imagen_curso"
                                                hidden accept="image/png, image/jpeg, image/jpg" name="avatar" required />
                                            <span class="d-block d-sm-none">
                                                <i class="mr-0" data-feather="edit"></i>
                                            </span>
                                            <div class="invalid-feedback" style="color: white;">Por favor, seleccione una
                                                imagen de portada.
                                            </div>
                                        </label>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                    <label for="start_date"> <b>(*) Fecha de inicio del curso: </b></label>
                                    <input type="datetime-local" class="form-control" id="start_date" name="start_date"
                                        required>
                                </div>

                                <div class="form-group col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                    <label for="end_date"> <b> Fecha de fin del curso: </b></label>
                                    <input type="datetime-local" class="form-control" id="end_date" name="end_date">
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title"><b>(*) Titulo del curso</b></label>
                                        <input type="text" class="form-control" id="title" name="title" required>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="summary"><b>(*) Resumen del curso</b></label>
                                        <textarea class="form-control summary" name="summary" id="summary"></textarea>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description"><b>(*) Descripción del curso</b></label>
                                        <textarea class="form-control description" name="description" id="description"></textarea>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <label for="cover_video"><b>Video presentación del curso</b></label>
                                    <div class="form-group">
                                        <input type="file" class="form-control" id="video_image" name="video_image" accept="video/*">
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="row col12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="form-group col">
                                        <label for="category_id"> <b>(*) Categoria del curso: </b></label>
                                        <select class="form-control selector" name="category_id" id="category_id">
                                            <option value="" selected disabled></option>
                                            @foreach ($categories as $categoria)
                                                <option value="{{ $categoria->code }}">
                                                    {{ $categoria->title }}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                    <div class="form-group col">
                                        <label for="professional_id"> <b>(*) Instructor del curso: </b></label>
                                        <select class="form-control selector" name="professional_id" id="professional_id">
                                            <option value selected disabled></option>
                                            @foreach ($professionals as $professional)
                                                <option value="{{ $professional->id }}">
                                                    C.C. {{ $professional->id }} |
                                                    {{ $professional->first_name }}
                                                    {{ $professional->second_name }}
                                                    {{ $professional->first_last_name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="form-group col">
                                    <label for="nivel_curso"> <b>(*) Nivel del curso: </b></label>
                                    <select class="form-control selector" name="nivel_curso" id="nivel_curso">
                                        <option value="" selected disabled></option>
                                        @foreach ($levels as $level)
                                            <option value="{{ $level->id }}">{{ $level->title }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col">
                                    <label for="student_limit"> <b>Limite de estudiantes del curso:
                                        </b></label>
                                    <input type="number" class="form-control" id="student_limit"
                                        name="student_limit">
                                    </select>
                                </div>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="form-group">
                                        <label for="archivos"> <b> Archivos del curso: </b></label>
                                        <input class="form-control" type="file" name="archivos[]" id="archivos"
                                            multiple placeholder="Seleccione archivos para el concepto"
                                            accept="application/pdf">
                                    </div>
                                </div>
                                <div class="col-12 d-flex flex-sm-row flex-column mt-2">
                                    <button type="submit" id="submit"
                                        class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">Registrar
                                        curso</button>
                                </div>
                            </div>
                        </form>

                        <!-- Basic toast -->
                        <div class="toast toast-basic hide position-fixed registro-exitoso" role="alert"
                            aria-live="assertive" aria-atomic="true" data-delay="5000" style="top: 1rem; right: 1rem"
                            id="registro-exitoso">
                            <div class="toast-header bg-success" style="color: white;">
                                <strong class="mr-auto">Notificación</strong>
                                <small class="text-muted">Justo ahora</small>
                                <button type="button" class="ml-1 close" data-dismiss="toast" aria-label="Close"
                                    style="color: white;">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="toast-body">¡Felicidades, registro exitoso!</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="https://cdn.ckeditor.com/4.20.1/basic/ckeditor.js"></script>
    <script src="https://s3.amazonaws.com/cnp.com.co/tagsinput.js"></script>
    <script src="{{ asset(mix('js/scripts/administrator/learning/courses/create.js')) }}"></script>
    <script>
     
    </script>
@endsection
