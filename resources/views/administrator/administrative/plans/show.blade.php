<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">

<style>
    .select2-selection__arrow {
        display: none;
    }
</style>

<!-- Validation -->
<section class="bs-validation">
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="card">

                <div class="media mb-2">
                    <img src="https://s3.amazonaws.com/cnp.com.co/{{ $plan->image }}" alt="users avatar"
                        class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="150" />
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="fecha_inicio"><b>(*) Fecha de inicio</b></label> <br>
                            <input type="date" class="form-control" value="{{ $plan->start_date }}" disabled>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="fecha_fin"><b>(*) Fecha de fin</b></label> <br>
                            <input type="date" class="form-control" value="{{ $plan->end_date }}" disabled>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nombre_plan"><b>(*) Titulo del plan</b></label>
                            <input type="text" class="form-control" value="{{ $plan->title }}" disabled>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="descripcion_plan"><b>(*) Descripción del plan</b></label>
                            <textarea class="form-control descripcion_plan" disabled>{{ $plan->description }}</textarea>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                        <div class="form-group">
                            <label for="valor_del_plan"><b>(*) Valor del plan</b></label>
                            <input class="form-control" type="text" value="{{ '$ ' . number_format($plan->price) }}"
                                disabled>
                        </div>
                    </div>
                    {{-- 
                            <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label for="tiempo_frecuencia"><b>(*) Tiempo de Frecuencia en el pago</b></label>
                                    <input type="text"  value="{{ $plan->tiempo_fecuencia }}" disabled>
                                    <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                </div>
                            </div> 

                            <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label for="valor_del_plan"><b>(*) Frecuencia en el pago</b></label>
                                    <input type="text"  value="{{ $plan->pago_frecuencia }}" disabled>
                                    <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                </div>
                            </div>
                            --}}
                    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                        <div class="form-group">
                            <label for="codigo_plataforma_suscripcion"><b>(*) Membresías asignadas</b></label>
                            <select class="form-control selector" multiple disabled>
                                @foreach ($plan->memberships as $membresia)
                                    <option value="{{ $membresia->code }}" selected>
                                        {{ $membresia->title }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="form-group">
                            <label for="platform_code"><b>(*) Código en la plataforma</b></label>
                            <input type="text" class="form-control" value="{{ $plan->platform_code }}" disabled>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
<script src="https://cdn.ckeditor.com/4.20.1/basic/ckeditor.js"></script>
<script src="https://s3.amazonaws.com/cnp.com.co/tagsinput.js"></script>

<script>
    $(document).ready(function() {
        $('.selector').select2();
    });
</script>
