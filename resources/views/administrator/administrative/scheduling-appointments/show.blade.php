<div class="row">
    <div class="col-12 col-sm-4 col-md-4 col-xl-4 col-lg-4">
        <div class="form-group">
            <label for="titulo_Seccion"><b>(*) Fecha de agendamiento</b></label>
            <input type="text" class="form-control" required value="{{ $agendamiento->date }}" disabled>
        </div>
    </div>
    <div class="col-12 col-sm-4 col-md-4 col-xl-4 col-lg-4">
        <div class="form-group">
            <label for="titulo_Seccion"><b>(*) Hora inicial de agendamiento</b></label>
            <input type="text" class="form-control" required value="{{ $agendamiento->start_hour }}" disabled>
        </div>
    </div>
    <div class="col-12 col-sm-4 col-md-4 col-xl-4 col-lg-4">
        <div class="form-group">
            <label for="titulo_Seccion"><b>(*) Hora final de agendamiento</b></label>
            <input type="text" class="form-control" required value="{{ $agendamiento->end_hour }}" disabled>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6">
        <div class="form-group">
            <label for="titulo_Seccion"><b>(*) Nombres</b></label>
            <input type="text" class="form-control" required value="{{ $agendamiento->names }}" disabled>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6">
        <div class="form-group">
            <label for="titulo_Seccion"><b>(*) Nombres</b></label>
            <input type="text" class="form-control" required value="{{ $agendamiento->last_names }}" disabled>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6">
        <div class="form-group">
            <label for="titulo_Seccion"><b>(*) Celular</b></label>
            <input type="text" class="form-control" required value="{{ $agendamiento->celphone }}" disabled>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6">
        <div class="form-group">
            <label for="titulo_Seccion"><b>(*) Email</b></label>
            <input type="text" class="form-control" required value="{{ $agendamiento->email }}" disabled>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-12 col-xl-12 col-lg-12">
        <div class="form-group">
            <label for="titulo_Seccion"><b>(*) Profesional</b></label>
            <input type="text" class="form-control" required value="{{ $agendamiento->profesional ? $agendamiento->profesional->id . ' | ' . $agendamiento->profesional->first_name . ' ' . $agendamiento->profesional->second_name . ' ' . $agendamiento->profesional->first_last_name . ' ' . $agendamiento->profesional->second_last_name : 'Profesional no asignado' }}" disabled>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-12 col-xl-12 col-lg-12">
        <div class="form-group">
            <label for="titulo_Seccion"><b>(*) Estado actual</b></label>
            <input type="text" class="form-control" required value="{{ $agendamiento->getStatus->description }}" disabled>
        </div>
    </div>
</div>
