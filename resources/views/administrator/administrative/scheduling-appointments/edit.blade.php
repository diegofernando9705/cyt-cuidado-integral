<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">

<form class="update-agendamiento" novalidate>
    @csrf
    @method('POST')
    <input type="hidden" class="form-control" id="code" value="{{ $agendamiento->code }}">

    <div class="row">
        <div class="col-12 col-sm-4 col-md-4 col-xl-4 col-lg-4">
            <div class="form-group">
                <label for="titulo_Seccion"><b>(*) Fecha de agendamiento</b></label>
                <input type="date" class="form-control" required value="{{ $agendamiento->date }}" id="date"
                    name="date" required>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-4 col-xl-4 col-lg-4">
            <div class="form-group">
                <label for="titulo_Seccion"><b>(*) Hora inicial de agendamiento</b></label>
                <input type="time" class="form-control" required value="{{ $agendamiento->start_hour }}"
                    id="start_hour" name="start_hour" required>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-4 col-xl-4 col-lg-4">
            <div class="form-group">
                <label for="titulo_Seccion"><b>(*) Hora final de agendamiento</b></label>
                <input type="time" class="form-control" required value="{{ $agendamiento->end_hour }}"
                    id="end_hour" name="end_hour" required>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6">
            <div class="form-group">
                <label for="titulo_Seccion"><b>(*) Nombres</b></label>
                <input type="text" class="form-control" required value="{{ $agendamiento->names }}" disabled>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6">
            <div class="form-group">
                <label for="titulo_Seccion"><b>(*) Nombres</b></label>
                <input type="text" class="form-control" required value="{{ $agendamiento->last_names }}" disabled>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6">
            <div class="form-group">
                <label for="titulo_Seccion"><b>(*) Celular</b></label>
                <input type="text" class="form-control" required value="{{ $agendamiento->celphone }}" disabled>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6">
            <div class="form-group">
                <label for="titulo_Seccion"><b>(*) Email</b></label>
                <input type="text" class="form-control" required value="{{ $agendamiento->email }}" disabled>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-xl-12 col-lg-12">
            <div class="form-group">
                <label for="titulo_Seccion"><b>(*) Profesional</b></label>
                <select class="form-control selector" name="profesional_id" id="profesional_id" required>
                    <option value="" disabled selected>Seleccione un profesional</option>
                    @foreach ($profesionales as $profesion)
                        @isset($agendamiento->profesional)
                            @if ($profesion->id == $agendamiento->profesional->id)
                                <option value="{{ $profesion->id }}" selected>
                                    {{ $agendamiento->profesional->id }} | {{ $agendamiento->profesional->first_name }}
                                    {{ $agendamiento->profesional->second_name }}
                                    {{ $agendamiento->profesional->first_last_name }}
                                </option>
                            @else
                                <option value="{{ $profesion->id }}">
                                    {{ $profesion->id }} | {{ $profesion->first_name }} {{ $profesion->second_name }}
                                    {{ $profesion->first_last_name }}
                                </option>
                            @endif
                        @else
                            <option value="{{ $profesion->id }}">
                                {{ $profesion->id }} | {{ $profesion->first_name }} {{ $profesion->second_name }}
                                {{ $profesion->first_last_name }}
                            </option>
                        @endisset
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-xl-12 col-lg-12">
            <div class="form-group">
                <label for="titulo_Seccion"><b>(*) Estado actual</b></label>
                <select class="form-control" name="status" id="status" required>
                    @foreach ($status as $state)
                        @if ($state->id == $agendamiento->getStatus->id)
                            <option value="{{ $state->id }}" selected>{{ $state->description }}</option>
                        @else
                            <option value="{{ $state->id }}">{{ $state->description }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-12 d-flex flex-sm-row flex-column mt-2">
            <button type="submit" id="submit"
                class="btn btn-primary col-12 col-md-12 col-sm-12 col-lg-12">Actualizar agendamiento</button>
        </div>
    </div>
</form>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
<script src="//cdn.ckeditor.com/4.20.1/full/ckeditor.js"></script>
<script src="{{ asset(mix('js/scripts/administrator/administrative/scheduling-appointments/edit.js')) }}"></script>
