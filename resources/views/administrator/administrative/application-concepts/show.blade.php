<style>
    .informacion_respuestas {
        border: 1px solid rgba(128, 128, 128, 0.397);
        text-align: left;
        font-size: 13px;
        border-radius: 10px;
        margin-bottom: 10px;
        padding-top: 10px;
        padding-left: 15px;
        padding-right: 15px;
        color: black !important;
    }

    .informacion_registro_respuesta {
        border-top: 1px solid rgba(128, 128, 128, 0.075);
        font-size: 11px;
        padding: 5px;

    }

    .descripcion_answer {
        text-align: justify !important;
    }

    .contenedor_formulario .form-group {
        margin-bottom: 10px;
    }

    .documentos ul li {
        color: black;
        font-size: 12px;
    }

    .separador {
        border-bottom: 3px solid rgba(128, 128, 128, 0.329);
        height: 10px;
        margin-bottom: 10px;
    }
</style>
<div class="title_calculadora col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
    <center>
        <h3>
            {{ $array_solicitudes[0]['titulo_solicitud'] }}
        </h3>
        <p> <b>Fecha:</b> {{ $array_solicitudes[0]['fecha_solicitud'] }} | <b>Por:
            </b>{{ $array_solicitudes[0]['usuario_solicitud'] }} | <b>Estado actual:
            </b>{{ $array_solicitudes[0]['nombre_estado'] }} </p>
    </center>
    <hr>
    <div class="descripcion">
        <div class="solicitud_register">
            <div class="row" style="text-align: center; font-size:12px;">
                <div class="fecha col">

                </div>

            </div>
        </div>
        <p style="font-size: 14px; text-align:justify !important; color:black;">{!! $array_solicitudes[0]['descripcion_solicitud'] !!}</p>
    </div>
    <hr>
    <div class="botones col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="row">
            @if ($array_solicitudes[0]['estado_solicitud'] == 11)
                @can('solicitudes-conceptos-aprobar')
                    <div class="boton_aprobada col">
                        <button type="button" class="btn btn-success aprobar-solicitud">Aprobar solicitud</button>
                    </div>
                @endcan
                @can('solicitudes-conceptos-rechazar')
                    <div class="boton_rechazada col">
                        <button type="button" class="btn btn-warning rechazar-solicitud">Rechazar solicitud</button>
                    </div>
                @endcan
                @can('solicitudes-generar-concepto')
                    <div class="boton_rechazada col">
                        <button type="button" class="btn btn-danger eliminar-solicitud">Eliminar solicitud</button>
                    </div>
                @endcan
            @elseif($array_solicitudes[0]['estado_solicitud'] == 12)
                @can('solicitudes-generar-concepto')
                    <div class="boton_aprobada col">
                        <button type="button" class="btn btn-success vincular-concepto">Vincular concepto</button>
                    </div>
                @endcan
                @can('solicitudes-conceptos-delete')
                    <div class="boton_rechazada col">
                        <button type="button" class="btn btn-danger eliminar-solicitud">Eliminar solicitud</button>
                    </div>
                @endcan
            @elseif($array_solicitudes[0]['estado_solicitud'] == 13)
                <div class="boton_rechazada">
                    <button type="button" class="btn btn-warning">Solicitud rechazada</button>
                </div>
            @elseif($array_solicitudes[0]['estado_solicitud'] == 14)
                <div class="boton_rechazada">
                    <button type="button" class="btn btn-danger">Solicitud eliminada</button>
                </div>
            @else
                <div class="boton_aprobada">
                    <button type="button" class="btn btn-success">Concepto creado - {{ $array_solicitudes[0]["concepto_asociado_solicitud"]}}</button>
                </div>
            @endif
        </div>
    </div>

</div>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdn.ckeditor.com/4.20.1/basic/ckeditor.js"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    var tabla_solicitudes_conceptos = $('.datatables-solicitudes-conceptos');

    @if ($array_solicitudes[0]['estado_solicitud'] == 11)

        $(document).on("click", ".aprobar-solicitud", function() {
            $(".modal-conceptos").off('focusin.modal');
            $.fn.modal.Constructor.prototype._enforceFocus = function() {};

            const containerClaimReqModal = document.getElementById("modal-conceptos");
            const claimReqModal = new bootstrap.Modal(containerClaimReqModal, {
                focus: false
            });


            Swal.fire({
                html: '<label><b> (*) Fecha aprobación: <b></label> <input type="date" id="fecha_aprobacion" class="form-control" value="{{ date('Y-m-d') }}">' +
                    '<br> <b> <label> (*) Observación generales: <b></label>  <textarea id="descripcion" class="form-control"></textarea>' +
                    '<br> <b> <label> (*) Documentos de soporte: <b></label>  <input type="file" multiple id="documentos" name="documentos[]" class="form-control"></label>',
                showCancelButton: true,
                confirmButtonText: 'Aprobar solicitud',
                cancelButtonText: 'Cancelar',
                cancelButtonColor: '#d33',
                focusConfirm: false,
                allowOutsideClick: false,
                preConfirm: () => {
                    const fecha_aprobacion = Swal.getPopup().querySelector('#fecha_aprobacion')
                        .value
                    const descripcion = Swal.getPopup().querySelector('#descripcion').value
                    const documentos = Swal.getPopup().querySelector('#documentos').value
                    if (!descripcion || !documentos || !fecha_aprobacion) {
                        Swal.showValidationMessage(
                            `Los campos marcados con asteriscos son importantes`)
                    }
                    return {
                        descripcion: descripcion,
                        documentos: documentos,
                        fecha_aprobacion: fecha_aprobacion,
                    }
                }
            }).then((result) => {
                if (result.isConfirmed) {

                    const fecha_aprobacion = Swal.getPopup().querySelector('#fecha_aprobacion').value
                    const descripcion = Swal.getPopup().querySelector('#descripcion').value

                    var paqueteDeDatos = new FormData();

                    var inpFiles = document.getElementById("documentos");
                    for (const file of inpFiles.files) {
                        paqueteDeDatos.append("archivos[]", file);
                    }


                    paqueteDeDatos.append('_token', "{{ csrf_token() }}");
                    paqueteDeDatos.append('codigo_solicitud',
                        "{{ $array_solicitudes[0]['codigo_solicitud'] }}");
                    paqueteDeDatos.append('fecha_aprobacion', fecha_aprobacion);
                    paqueteDeDatos.append('descripcion', descripcion);


                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                        }
                    });


                    $.ajax({
                        type: 'POST',
                        url: "{{ route('solicitudes-update_conceptos', 'aprobar') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: paqueteDeDatos,
                        processData: false,
                        contentType: false,
                        success: function(data) {

                            if (data == true) {
                                var opciones = tabla_solicitudes_conceptos.DataTable();
                                opciones.ajax.reload();

                                Swal.fire({
                                    allowOutsideClick: false,
                                    icon: 'success',
                                    title: 'Solicitud aprobada',
                                    html: 'Se ha aprobado la solicitud correctamente, ahora puede generar el concepto'
                                });


                                setTimeout(() => {
                                    $(".modal-conceptos").modal('hide');
                                }, 2000);

                            }
                        },
                        error: function(error) {

                        },
                    });
                }
            });
        });

        $(document).on("click", ".rechazar-solicitud", function() {
            $(".modal-conceptos").off('focusin.modal');
            $.fn.modal.Constructor.prototype._enforceFocus = function() {};

            const containerClaimReqModal = document.getElementById("modal-conceptos");
            const claimReqModal = new bootstrap.Modal(containerClaimReqModal, {
                focus: false
            });


            Swal.fire({
                html: '<label><b> (*) Fecha rechazo: <b></label> <input type="date" id="fecha_rechazo" class="form-control" value="{{ date('Y-m-d') }}">' +
                    '<br> <b> <label> (*) Motivo del rechazo: <b></label>  <textarea id="descripcion" class="form-control"></textarea>',
                showCancelButton: true,
                confirmButtonText: 'Rechazar solicitud',
                cancelButtonText: 'Cancelar',
                cancelButtonColor: '#d33',
                focusConfirm: false,
                allowOutsideClick: false,
                preConfirm: () => {
                    const fecha_rechazo = Swal.getPopup().querySelector('#fecha_rechazo')
                        .value
                    const descripcion = Swal.getPopup().querySelector('#descripcion').value

                    if (!descripcion || !fecha_rechazo) {
                        Swal.showValidationMessage(
                            `Los campos marcados con asteriscos son importantes`)
                    }
                    return {
                        descripcion: descripcion,
                        fecha_rechazo: fecha_rechazo,
                    }
                }
            }).then((result) => {
                if (result.isConfirmed) {

                    const fecha_rechazo = Swal.getPopup().querySelector('#fecha_rechazo').value
                    const descripcion = Swal.getPopup().querySelector('#descripcion').value

                    var paqueteDeDatos = new FormData();

                    paqueteDeDatos.append('_token', "{{ csrf_token() }}");
                    paqueteDeDatos.append('codigo_solicitud',
                        "{{ $array_solicitudes[0]['codigo_solicitud'] }}");
                    paqueteDeDatos.append('fecha_rechazo', fecha_rechazo);
                    paqueteDeDatos.append('descripcion', descripcion);


                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                        }
                    });


                    $.ajax({
                        type: 'POST',
                        url: "{{ route('solicitudes-update_conceptos', 'rechazar') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: paqueteDeDatos,
                        processData: false,
                        contentType: false,
                        success: function(data) {

                            if (data == true) {
                                var opciones = tabla_solicitudes_conceptos.DataTable();
                                opciones.ajax.reload();

                                Swal.fire({
                                    allowOutsideClick: false,
                                    icon: 'warning',
                                    title: 'Solicitud rechazada',
                                    html: 'La solicitud ha sido rechazada.'
                                });


                                setTimeout(() => {
                                    $(".modal-conceptos").modal('hide');
                                }, 2000);

                            }
                        },
                        error: function(error) {

                        },
                    });
                }
            });
        });

        $(document).on("click", ".eliminar-solicitud", function() {
            $(".modal-conceptos").off('focusin.modal');
            $.fn.modal.Constructor.prototype._enforceFocus = function() {};

            const containerClaimReqModal = document.getElementById("modal-conceptos");
            const claimReqModal = new bootstrap.Modal(containerClaimReqModal, {
                focus: false
            });


            Swal.fire({
                html: '<label><b> (*) Fecha eliminación: <b></label> <input type="date" id="fecha_eliminacion" class="form-control" value="{{ date('Y-m-d') }}">' +
                    '<br> <b> <label> (*) Motivo del eliminación: <b></label>  <textarea id="descripcion" class="form-control"></textarea>',
                showCancelButton: true,
                confirmButtonText: 'Eliminar solicitud',
                cancelButtonText: 'Cancelar',
                cancelButtonColor: '#d33',
                focusConfirm: false,
                allowOutsideClick: false,
                preConfirm: () => {
                    const fecha_eliminacion = Swal.getPopup().querySelector('#fecha_eliminacion')
                        .value
                    const descripcion = Swal.getPopup().querySelector('#descripcion').value

                    if (!descripcion || !fecha_eliminacion) {
                        Swal.showValidationMessage(
                            `Los campos marcados con asteriscos son importantes`)
                    }
                    return {
                        descripcion: descripcion,
                        fecha_eliminacion: fecha_eliminacion,
                    }
                }
            }).then((result) => {
                if (result.isConfirmed) {

                    const fecha_eliminacion = Swal.getPopup().querySelector('#fecha_eliminacion').value
                    const descripcion = Swal.getPopup().querySelector('#descripcion').value

                    var paqueteDeDatos = new FormData();

                    paqueteDeDatos.append('_token', "{{ csrf_token() }}");
                    paqueteDeDatos.append('codigo_solicitud',
                        "{{ $array_solicitudes[0]['codigo_solicitud'] }}");
                    paqueteDeDatos.append('fecha_eliminacion', fecha_eliminacion);
                    paqueteDeDatos.append('descripcion', descripcion);


                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                        }
                    });


                    $.ajax({
                        type: 'POST',
                        url: "{{ route('solicitudes-update_conceptos', 'eliminar') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: paqueteDeDatos,
                        processData: false,
                        contentType: false,
                        success: function(data) {

                            if (data == true) {
                                var opciones = tabla_solicitudes_conceptos.DataTable();
                                opciones.ajax.reload();

                                Swal.fire({
                                    allowOutsideClick: false,
                                    icon: 'warning',
                                    title: 'Solicitud eliminada',
                                    html: 'La solicitud ha sido eliminada.'
                                });


                                setTimeout(() => {
                                    $(".modal-conceptos").modal('hide');
                                }, 2000);

                            }
                        },
                        error: function(error) {

                        },
                    });
                }
            });
        });
    @elseif ($array_solicitudes[0]['estado_solicitud'] == 12)
        $(document).on("click", ".vincular-concepto", function() {

            $.ajax({
                type: 'GET',
                url: "{{ route('solicitudes-generar-concepto', $array_solicitudes[0]['codigo_solicitud']) }}",
                success: function(data) {
                    Swal.fire({
                        html: data,
                        showCancelButton: true,
                        confirmButtonText: 'Vincular concepto',
                        cancelButtonText: 'Cancelar',
                        cancelButtonColor: '#d33',
                        focusConfirm: false,
                        allowOutsideClick: false,
                        preConfirm: () => {
                            const concepto_vincular = $("#concepto_vincular").val();

                            if (!concepto_vincular) {
                                Swal.showValidationMessage(`Los campos marcados con asteriscos son importantes`)
                            }
                            return {
                                concepto_vincular: concepto_vincular,
                            }
                        }
                    }).then((result) => {
                        if (result.isConfirmed) {

                            var paqueteDeDatos = new FormData();

                            paqueteDeDatos.append('_token', "{{ csrf_token() }}");
                            paqueteDeDatos.append('codigo_solicitud',"{{ $array_solicitudes[0]['codigo_solicitud'] }}");
                            paqueteDeDatos.append('concepto_vincular', $("#concepto_vincular").val());


                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="token"]').attr(
                                        'value')
                                }
                            });


                            $.ajax({
                                type: 'POST',
                                url: "{{ route('solicitudes-update_conceptos', 'vincular') }}",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]')
                                        .attr('content')
                                },
                                data: paqueteDeDatos,
                                processData: false,
                                contentType: false,
                                success: function(data) {

                                    if (data == true) {
                                        var opciones =
                                            tabla_solicitudes_conceptos
                                            .DataTable();
                                        opciones.ajax.reload();

                                        Swal.fire({
                                            allowOutsideClick: false,
                                            icon: 'success',
                                            title: 'Concepto vinculado',
                                            html: 'Se ha vinculado correctamente el concepto.'
                                        });


                                        setTimeout(() => {
                                            $(".modal-conceptos").modal(
                                                'hide');
                                        }, 2000);

                                    }
                                },
                                error: function(error) {

                                },
                            });
                        }
                    });
                },
                error: function(error) {

                },
            });
        });
    @elseif ($array_solicitudes[0]['estado_solicitud'] == 13)
    @elseif ($array_solicitudes[0]['estado_solicitud'] == 14)
    @else
    @endif
</script>
