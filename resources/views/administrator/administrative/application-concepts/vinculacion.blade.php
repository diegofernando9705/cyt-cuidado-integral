
    <div class="form-group">
        <label for=""> <b>(*) Concepto a vincular </b></label>
        <select class="form-control selector" name="concepto_vincular" id="concepto_vincular" required>
            <option selected disabled></option>
            @foreach ($conceptos as $concepto)
                <option value="{{ $concepto->codigo_concepto }}">{{ $concepto->codigo_concepto }} -
                    {{ $concepto->titulo_concepto }}</option>
            @endforeach
        </select>
    </div>
 