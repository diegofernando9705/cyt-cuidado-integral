<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">

<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">

<div class="row">
    <!-- Bootstrap Validation -->
    <div class="col-md-12 col-12">
        <div class="card">
            <form class="update-membresias" novalidate>
                @csrf
                @method('POST')
                <div class="media mb-2">
                    <img src="https://s3.amazonaws.com/cnp.com.co/{{ $membership->image }}" alt="users avatar"
                        class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="150" />
                    <div class="media-body mt-50">
                        <h4>Imagen de portada</h4>
                        <div class="col-12 d-flex mt-1 px-0">
                            <label class="btn btn-primary mr-75 mb-0" for="image">
                                <span class="d-none d-sm-block">Subir foto</span>
                                <input class="form-control" type="file" id="image" name="image" hidden
                                    accept="image/png, image/jpeg, image/jpg" name="avatar" />
                                <span class="d-block d-sm-none">
                                    <i class="mr-0" data-feather="edit"></i>
                                </span>
                                <div class="invalid-feedback" style="color: white;">Por favor, seleccione una
                                    imagen de portada.
                                </div>
                            </label>

                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="start_date"><b>(*) Fecha de publicación</b></label> <br>
                            <input type="datetime-local" class="form-control" name="start_date" id="start_date" required
                                value="{{ $membership->start_date }}">
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="end_date"><b>(*) Fecha de terminación</b></label> <br>
                            <input type="datetime-local" class="form-control" name="end_date" id="end_date"
                                value="{{ $membership->end_date }}">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title"><b>(*) Titulo de la membresía</b></label>
                            <input class="form-control" name="title" id="title" value="{{ $membership->title }}"
                                required>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="description"><b>(*) Descripción de la membresía</b></label>
                            <textarea class="form-control description" name="description" id="description">{!! $membership->description !!}</textarea>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="code_platform_subscription"><b>(*) Código de la
                                    Plataforma</b></label>
                            <input type="text" class="form-control"
                                value="{{ $membership->code_platform_subscription }}" disabled>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="status"><b>(*) Estado</b></label>
                            <select class="form-control selector" name="status" id="status" required>
                                @foreach ($estados as $estado)
                                    @if ($membership->status == $estado->id)
                                        <option value="{{ $estado->id }}" selected>
                                            {{ $estado->description }}</option>
                                    @else
                                        <option value="{{ $estado->id }}">
                                            {{ $estado->description }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Por favor, seleccione un estado para la categoria.
                            </div>
                        </div>
                    </div>

                    <div class="col-12 d-flex flex-sm-row flex-column mt-2">
                        <button type="submit" id="submit"
                            class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1 col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">Actualizar
                            membresia</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
<script src="https://cdn.ckeditor.com/4.20.1/basic/ckeditor.js"></script>
<script src="https://s3.amazonaws.com/cnp.com.co/tagsinput.js"></script>

<script>
    $(document).ready(function() {
        $('.selector').select2();
    });


    var tabla_membresias = $('.datatables-membresias');
    var changePicture = $('#image'),
        userAvatar = $('.user-avatar');

    // Change user profile picture
    if (changePicture.length) {
        $(changePicture).on('change', function(e) {
            var reader = new FileReader(),
                files = e.target.files;
            reader.onload = function() {
                if (userAvatar.length) {
                    userAvatar.attr('src', reader.result);
                }
            };
            reader.readAsDataURL(files[0]);
        });
    };

    CKEDITOR.replace('description');

    // Actualizacion de servicio
    $(function() {
        'use strict';

        const forms = document.querySelectorAll('.update-membresias');

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms).forEach((form) => {
            form.addEventListener('submit', (event) => {

                if (!form.checkValidity()) {
                    event.stopPropagation();
                } else {
                    document.getElementById("start_date").disabled = true;
                    document.getElementById("end_date").disabled = true;
                    document.getElementById("title").disabled = true;
                    document.getElementById("description").disabled = true;
                    document.getElementById("status").disabled = true;
                    document.getElementById("submit").disabled = true;

                    var paqueteDeDatos = new FormData();
                    paqueteDeDatos.append('image', $('#image')[0].files[0]);

                    paqueteDeDatos.append('start_date', $('#start_date').prop('value'));
                    paqueteDeDatos.append('code', '{{ $membership->code }}');
                    paqueteDeDatos.append('end_date', $('#end_date').prop('value'));
                    paqueteDeDatos.append('title', $('#title').prop('value'));
                    paqueteDeDatos.append('description', CKEDITOR.instances["description"]
                        .getData());

                    paqueteDeDatos.append('status', $('#status').val());



                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                        }
                    });

                    $.ajax({
                        type: 'POST',
                        url: '{{ route('membresias-update') }}',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: paqueteDeDatos,
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            if (data == true) {
                                Swal.fire({
                                    title: "Actualización exitosa",
                                    text: "Se actualizó la información en la plataforma",
                                    icon: "success",
                                    allowOutsideClick: false,
                                    allowEscapeKey: false,
                                });

                                $("#modal-membresias").modal('hide');

                                setTimeout(() => {
                                    var opciones = tabla_membresias
                                        .DataTable();
                                    opciones.ajax.reload();
                                    Swal.close();
                                }, 2000);
                            }
                        },
                        error: function(err) {
                            document.getElementById("start_date").disabled = false;
                            document.getElementById("end_date").disabled = false;
                            document.getElementById("title").disabled = false;
                            document.getElementById("description").disabled = true;
                            document.getElementById("status").disabled = false;
                            document.getElementById("submit").disabled = false;

                            errorHttp(err.status);
                            if (err.status == 422) {
                                $(".errores").hide();
                                console.log(err.responseJSON);
                                $('#success_message').fadeIn().html(err.responseJSON
                                    .message);
                                console.warn(err.responseJSON.errors);
                                $.each(err.responseJSON.errors, function(i, error) {
                                    var el = $(document).find('[name="' +
                                        i + '"]');
                                    el.after($('<span class="errores" style="color: red;">' +
                                        error[0] + '</span>'));
                                });
                            }
                        },
                    });
                    event.preventDefault();
                }
                event.preventDefault();
                form.classList.add('was-validated');
            }, false);
        });
    });
</script>
