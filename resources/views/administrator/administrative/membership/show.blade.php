<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">


<section class="bs-validation">
    <div class="row">
        <!-- Bootstrap Validation -->
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-body">
                    <div class="media mb-2">
                        <img src="https://s3.amazonaws.com/cnp.com.co/{{ $membership->image }}" alt="users avatar"
                            class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="150" />
                        <div class="media-body mt-50">
                            <h4>Imagen de portada</h4>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fecha_inicio"><b>(*) Fecha de publicación</b></label> <br>
                                <input type="datetime" class="form-control" value="{{ $membership->start_date }}"
                                    disabled>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fecha_fin"><b>(*) Fecha de terminación</b></label> <br>
                                <input type="datetime" class="form-control" value="{{ $membership->end_date }}"
                                    disabled>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="titulo_membresia"><b>(*) Titulo de la membresía</b></label>
                                <input type="text" class="form-control" value="{{ $membership->title }}" disabled>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="descripcion_membresia"><b>(*) Descripción de la membresía</b></label> <br>
                                {!! $membership->description !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="codigo_plataforma_suscripcion"><b>(*) Código de la
                                        membresia en PayPal</b></label>
                                <input type="text" class="form-control"
                                    value="{{ $membership->code_platform_subscription }}" disabled>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="estado_membresia"><b>(*) Estado</b></label>
                                <input type="text" class="form-control"
                                    value="{{ $membership->getStatus->description }}" disabled>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
