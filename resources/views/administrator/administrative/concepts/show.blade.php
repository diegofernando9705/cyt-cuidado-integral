<style>
    .informacion_respuestas {
        border: 1px solid rgba(128, 128, 128, 0.397);
        text-align: left;
        font-size: 13px;
        border-radius: 10px;
        margin-bottom: 10px;
        padding-top: 10px;
        padding-left: 15px;
        padding-right: 15px;
        color: black !important;
    }

    .informacion_registro_respuesta {
        border-top: 1px solid rgba(128, 128, 128, 0.075);
        font-size: 11px;
        padding: 5px;

    }

    .descripcion_answer {
        text-align: justify !important;
    }

    .contenedor_formulario .form-group {
        margin-bottom: 10px;
    }

    .documentos ul li {
        color: black;
        font-size: 12px;
    }

    .separador {
        border-bottom: 3px solid rgba(128, 128, 128, 0.329);
        height: 10px;
        margin-bottom: 10px;
    }
</style>
<div class="title_calculadora col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
    <center>
        <h3>
            CONCEPTO - {{ $concept->title }}
        </h3>
    </center>
    <hr>
    <div class="descripcion">
        <div class="data_register">
            <div class="row" style="text-align: center; font-size:12px;">
                <div class="fecha col">
                    <p> {{ $concept->register_date }}</p>
                </div>
                <div class="user col" style="border-left:1px solid rgba(128, 128, 128, 0.308);">
                    <p> {{ $concept->privacy }}</p>
                </div>
            </div>
        </div>
        <p style="font-size: 13px; text-align:justify !important;">{!! $concept->description !!}</p>
        <div class="documentos">
            <h5>Documentos adjuntos</h5>
            <ul>
                @foreach ($concept->archives as $archive)
                    <li>
                        <a href="https://s3.amazonaws.com/cnp.com.co/{{ $archive->url }}" target="_blank">Abrir
                            documento</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <form class="form_answer" novalidate>
        @csrf
        @method('POST')

        <div class="form-group">
            <label for="descripcion_respuesta" style="color:black; font-size:12px;">Escriba su respuesta acá...</label>
            <textarea class="form-control" id="descripcion_respuesta" name="descripcion_respuesta"></textarea>
            <div class="invalid-feedback">Por favor, este campo es importante.</div>
        </div>
        <div class="contendor_submit" style="text-align: right; margin-top:5px;">
            <button type="submit" class="btn btn-success btn-sm" id="register_button">Registrar</button>
        </div>
    </form>
    <hr>
    <h5>Respuestas..</h5>
    <div class="contenedor_respuestas" id="contenedor_respuestas">

    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdn.ckeditor.com/4.20.1/basic/ckeditor.js"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    setTimeout(() => {
        loadingAnswer();
    }, 1000);

    $(function() {
        'use strict';

        const forms = document.querySelectorAll('.form_answer');

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms).forEach((form) => {
            form.addEventListener('submit', (event) => {

                if (!form.checkValidity()) {
                    event.stopPropagation();
                } else {

                    document.getElementById("register_button").disabled = true;

                    var DatosAnswer = new FormData();

                    DatosAnswer.append('_token', "{{ csrf_token() }}");
                    DatosAnswer.append('data', "{{ $concept->code }}");
                    DatosAnswer.append('descripcion_respuesta', CKEDITOR.instances[
                        "descripcion_respuesta"].getData());

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                        }
                    });

                    $.ajax({
                        type: 'POST',
                        url: '{{ route('answers-store') }}',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: DatosAnswer, // serializes the form's elements.
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            if (data == true) {
                                storeSuccess();
                                document.getElementById("register_button")
                                    .disabled = false;

                                setTimeout(() => {
                                    CKEDITOR.instances["descripcion_respuesta"].setData('');
                                    loadingAnswer();
                                }, 1000);
                            }
                        },
                        error: function(err) {
                            document.getElementById("register_button").disabled =
                                false;

                            errorHttp(err.status);
                        },
                    });
                    event.preventDefault();

                }
                event.preventDefault();
                form.classList.add('was-validated');
            }, false);
        });

    });

    function loadingAnswer() {
        $.ajax({
            type: "GET",
            url: "{{ route('answers-list', $concept->code) }}",
            success: function(data) {
                $("#contenedor_respuestas").html(data);
            },
            error: function(err) {},
        });
    }
</script>
