<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">

<style>
    .select2-selection__arrow {
        display: none;
    }
</style>

<div class="row">
    <form class="update_membership_conceptos col-12 col-sm-12 col-md-12 col-lg-12" novalidate>
        @csrf
        @method('POST')

        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
            <label for="">Selecciona la membresia que desea asignar:</label>
            <select class="form-control selector" multiple id="membership" name="membership" required>
                @foreach ($membresias as $membresia)
                    @if (is_array($array_membresias) && in_array($membresia->code, $array_membresias))
                        <option value="{{ $membresia->code }}" selected>
                            {{ $membresia->title }}</option>
                    @else
                        <option value="{{ $membresia->code }}">{{ $membresia->title }}
                        </option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="boton col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="text-align:center;">
            <button type="submit" class="btn btn-sm btn-primary" id="submit">Asignar membresias</button>
        </div>
    </form>
</div>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>

<script>
    tabla_conceptos = $(".datatables-conceptos");

    $(document).ready(function() {
        $(".selector").select2();
    });

    // Actualizacion de servicio
    $(function() {
        'use strict';

        const forms = document.querySelectorAll('.update_membership_conceptos');

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms).forEach((form) => {
            form.addEventListener('submit', (event) => {

                if (!form.checkValidity()) {
                    event.stopPropagation();
                } else {
                    document.getElementById("submit").disabled = true;

                    var paqueteDeDatos = new FormData();

                    paqueteDeDatos.append('membership', $('#membership').val());
                    document.getElementById("membership").disabled = true;


                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                        }
                    });

                    $.ajax({
                        type: 'POST',
                        url: '{{ route('concepto-membresia-update', $id) }}',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: paqueteDeDatos,
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            updateSuccess();
                            
                            setTimeout(() => {
                                $("#modal-conceptos").modal("hide");
                            }, 1000);

                        },
                        error: function(err) {
                            errorHttp(err.status);
                            if (err.status == 422) {
                                $(".errores").hide();
                                console.log(err.responseJSON);
                                $('#success_message').fadeIn().html(err.responseJSON
                                    .message);
                                console.warn(err.responseJSON.errors);
                                $.each(err.responseJSON.errors, function(i, error) {
                                    var el = $(document).find('[name="' +
                                        i + '"]');
                                    el.after($('<span class="errores" style="color: red;">' +
                                        error[0] + '</span>'));
                                });
                            }
                        },
                    });
                    event.preventDefault();
                }
                event.preventDefault();
                form.classList.add('was-validated');
            }, false);
        });
    });
</script>
