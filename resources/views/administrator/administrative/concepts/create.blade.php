@extends('administrator.layouts.contentLayoutMaster')
@section('title', 'Registrar concepto')

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
            <!-- Bootstrap Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-body">

                        <form class="store-conceptos" novalidate>
                            @csrf
                            @method('POST')

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title"><b>(*) Titutlo del concepto</b></label>
                                        <input type="text" class="form-control" id="title" name="title" required>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description"><b>(*) Descripción del concepto</b></label>
                                        <textarea class="form-control description" name="description" id="description"></textarea>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="register_date"><b>(*) Fecha de registro</b></label> <br>
                                        <input type="date" class="form-control" name="register_date" id="register_date"
                                            required value="{{ date('Y-m-d') }}">
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="end_date"><b>Fecha de caducidad</b></label> <br>
                                        <input type="date" class="form-control" name="end_date" id="end_date">
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="privacy"> <b> Privacidad del concepto: </b></label>
                                        <select class="form-control selector" name="privacy" id="privacy" required>
                                            <option value selected disabled></option>
                                            <option value="public">Público</option>
                                            <option value="private">Privado</option>
                                        </select>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6" id="div_profesionales"
                                    style="display: none;">
                                    <div class="form-group">
                                        <label for="profesionales">
                                            <b>(*) Asigar a la solicitud de concepto:</b>
                                        </label>
                                        <select class="form-control selector" name="application_concept"
                                            id="application_concept">
                                        </select>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="form-group">
                                        <label for="archivos"> <b> Archivos del concepto: </b></label>
                                        <input class="form-control" type="file" name="archivos[]" id="archivos" multiple
                                            placeholder="Seleccione archivos para el concepto" accept="application/pdf">
                                    </div>
                                </div>
                                <div class="container">
                                    <hr>
                                    <button type="submit" id="submit"
                                        class="btn btn-primary btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">Registrar
                                        concepto</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="https://cdn.ckeditor.com/4.20.1/basic/ckeditor.js"></script>
    <script src="https://s3.amazonaws.com/cnp.com.co/tagsinput.js"></script>
    <script src="{{ asset(mix('js/scripts/administrator/administrative/concepts/create.js')) }}"></script>
@endsection
