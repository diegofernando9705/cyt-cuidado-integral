
<style>
    .informacion_respuestas {
        border: 1px solid rgba(128, 128, 128, 0.397);
        text-align: left;
        font-size: 13px;
        border-radius: 10px;
        margin-bottom: 10px;
        padding-top: 10px;
        padding-left: 15px;
        padding-right: 15px;
        color: black !important;
    }

    .informacion_registro_respuesta {
        border-top: 1px solid rgba(128, 128, 128, 0.075);
        font-size: 11px;
        padding: 5px;

    }

    .descripcion_answer {
        text-align: justify !important;
    }

    .contenedor_formulario .form-group {
        margin-bottom: 10px;
    }

    .documentos ul li{
        color: black;
        font-size: 12px;
    }
    .separador {
        border-bottom: 3px solid rgba(128, 128, 128, 0.329);
        height: 10px;
        margin-bottom: 10px;
    }
</style>
<div class="title_calculadora col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
    <center>
        <h3>
            CONCEPTO - {{ $data->title }}
        </h3>
    </center>
    <hr>
    <div class="descripcion">
        <div class="data_register">
            <div class="row" style="text-align: center; font-size:12px;">
                <div class="fecha col">
                    <p> {{ $data->date_start }}</p>
                </div>
                <div class="user col" style="border-left:1px solid rgba(128, 128, 128, 0.308);">
                    <p> {{ $data->privacy }}</p>
                </div>
            </div>
        </div>
        <p style="font-size: 13px; text-align:justify !important;">{!! $data->description !!}</p>
        <div class="documentos">
            <h5>Documentos adjuntos</h5>
            <ul>
                @foreach ($data->archives as $archive)
                    <li>
                        <a href="https://s3.amazonaws.com/cnp.com.co/{{ $archive->url }}" target="_blank">Abrir
                            documento</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <hr>
    <h5>Respuestas..</h5>
    <div class="contenedor_respuestas">
        @foreach ($data->answers as $data_respuesta)
            <div class="contenedor_respuestas col-12 col-md-12 col-sm-12 col-lg-12 col-xl-12">
                <div class="informacion_respuestas">
                    <div class="descripcion_answer">
                        <a>
                            {!! $data_respuesta->description !!}
                        </a>
                    </div>

                    <div class="informacion_registro_respuesta">
                        <div class="row">
                            <div class="fecha_registro col">
                                <b>Fecha:</b> {{ $data_respuesta->created_at }}
                            </div>
                            <div class="usuario_registro col">
                                <b>Por:</b> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdn.ckeditor.com/4.20.1/basic/ckeditor.js"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
