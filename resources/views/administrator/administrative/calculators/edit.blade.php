<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">

<style>
    .select2-selection__arrow {
        display: none;
    }
</style>

<section class="bs-validation">
    <div class="row">
        <!-- Bootstrap Validation -->
        <div class="col-md-12 col-12">
            <div class="card">
                <form class="update-calculadoras" novalidate>
                    @csrf
                    @method('POST')
                    <div class="media mb-2">
                        <img src="https://s3.amazonaws.com/cnp.com.co/{{ $calculator->image }}" alt="users avatar"
                            class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="150" />
                        <div class="media-body mt-50">
                            <h4>Imagen de fondo</h4>
                            <div class="col-12 d-flex mt-1 px-0">
                                <label class="btn btn-primary mr-75 mb-0" for="image">
                                    <span class="d-none d-sm-block">Cambiar foto</span>
                                    <input class="form-control" type="file" id="image" name="image" hidden
                                        accept="image/png, image/jpeg, image/jpg" name="avatar" />
                                    <span class="d-block d-sm-none">
                                        <i class="mr-0" data-feather="edit"></i>
                                    </span>
                                    <div class="invalid-feedback" style="color: white;">Por favor, seleccione una
                                        imagen de portada.
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <input type="hidden" id="code" value="{{ $calculator->code }}">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>(*) Titulo de la calculadora</b></label>
                                <input type="text" class="form-control" name="title" id="title"
                                    value="{{ $calculator->title }}" required>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description"><b>Descripción de la calculadora</b></label>
                                <textarea class="form-control description" name="description" id="description">{{ $calculator->description }}</textarea>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="codigo_plataforma_suscripcion"><b>(*) Membresías asignadas</b></label>
                                <select class="form-control selector" multiple name="membresias" id="membresias"
                                    required>
                                    @foreach ($membresias as $membresia)
                                        @if (is_array($array_membresias) && in_array($membresia->code, $array_membresias))
                                            <option value="{{ $membresia->code }}" selected>
                                                {{ $membresia->title }}</option>
                                        @else
                                            <option value="{{ $membresia->code }}">
                                                {{ $membresia->title }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="status"><b>(*) Estado</b></label>
                                <select class="form-control selector" name="status" id="status" required>
                                    @foreach ($estados as $estado)
                                        @if ($calculator->status == $estado->id)
                                            <option value="{{ $estado->id }}" selected>
                                                {{ $estado->description }}</option>
                                        @else
                                            <option value="{{ $estado->id }}">
                                                {{ $estado->description }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">Por favor, seleccione un estado para la categoria.
                                </div>
                            </div>
                        </div>

                        <div class="container">
                            <button type="submit" id="submit"
                                class="btn btn-primary btn-sm col-12 col-sm-12 col-md-12 col-lg-12">Actualizar
                                calculadora</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
<script src="https://cdn.ckeditor.com/4.20.1/basic/ckeditor.js"></script>
<script src="https://s3.amazonaws.com/cnp.com.co/tagsinput.js"></script>
<script src="{{ asset(mix('js/scripts/administrator/administrative/calculators/edit.js')) }}"></script>