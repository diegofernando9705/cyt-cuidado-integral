{{-- Vendor Css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css" />

<style>
    .select2-selection__arrow {
        display: none;
    }
</style>


<form class="update-human-talent" novalidate>
    @csrf
    @method('POST')

    <div class="media mb-2">
        <img src="https://s3.amazonaws.com/cnp.com.co/{{ $data->getUser->avatar }}" title=""
            class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="150" />
        <div class="media-body mt-50">
            <h4>Foto de perfil</h4>
            <div class="col-12 d-flex mt-1 px-0">
                <label class="btn btn-primary mr-75 mb-0" for="user-avatar">
                    <span class="d-none d-sm-block">Subir foto</span>
                    <input class="form-control" type="file" id="user-avatar" hidden
                        accept="image/png, image/jpeg, image/jpg" name="avatar" />
                    <span class="d-block d-sm-none">
                        <i class="mr-0" data-feather="edit"></i>
                    </span>
                </label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="tarjeta_profesional"><b>Tarjeta profesional</b></label> <br>
                <input type="text" class="form-control" name="tarjeta_profesional" id="tarjeta_profesional"
                    value="{{ $data->professional_card }}" required>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="tipo_de_documento_profesional"><b>(*) Tipo de documento</b></label> <br>
                <input type="text" class="form-control" value="{{ $data->documentType->description }}" disabled>
            </div>
        </div>


        <div class="col-md-6">
            <div class="form-group">
                <label for="identificacion_profesional"><b>(*) Número de identificación</b></label>
                <br>
                <input type="text" class="form-control" id="identificacion_profesional" value="{{ $data->id }}"
                    disabled>

            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="primer_nombre_profesional"><b>(*) Primer nombre</b></label> <br>
                <input type="text" class="form-control" name="primer_nombre_profesional"
                    id="primer_nombre_profesional" value="{{ $data->first_name }}" required>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="segundo_nombre_profesional"><b>Segundo nombre</b></label> <br>
                <input type="text" class="form-control" name="segundo_nombre_profesional"
                    id="segundo_nombre_profesional" value="{{ $data->second_name }}">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="primer_apellido_profesional"><b>(*) Primer apellido</b></label> <br>
                <input type="text" class="form-control" name="primer_apellido_profesional"
                    id="primer_apellido_profesional" value="{{ $data->first_last_name }}" required>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="segundo_apellido_profesional"><b>Segundo apellido</b></label> <br>
                <input type="text" class="form-control" name="segundo_apellido_profesional"
                    id="segundo_apellido_profesional" value="{{ $data->second_last_name }}">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="correo_profesional"><b>(*) Correo electrónico</b></label> <br>
                <input type="text" class="form-control" name="correo_profesional" id="correo_profesional"
                    value="{{ $data->email }}" required>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="celular_profesional"><b>(*) Celular</b></label> <br>
                <input type="tel" class="form-control" name="celular_profesional" id="celular_profesional"
                    value="{{ $data->cellphone }}" required>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
            <div class="alert alert-info" style="display: none"></div>
            <div class="alert alert-error" style="display: none"></div>
        </div>


        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <div class="form-group">
                <label for="type"><b>(*) Tipo</b></label>
                <select class="form-control selector" name="type" id="type" required>
                    @if ($data->type == 'subscriber')
                        <option value="user">Talento humano</option>
                        <option value="subscriber" selected>Suscriptor/Estudiante</option>
                    @else
                        <option value="user" selected>Talento humano</option>
                        <option value="subscriber">Suscriptor/Estudiante</option>
                    @endif
                </select>
                <div class="invalid-feedback">Por favor, seleccione un tipo.
                </div>
            </div>
        </div>


        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <div class="form-group">
                <label for="estado"><b>(*) Estado</b></label>
                <select class="form-control selector" name="estado" id="estado" required>
                    @foreach ($status as $state)
                        @if ($state->id == $data->status)
                            <option value="{{ $state->id }}" selected>{{ $state->description }}</option>
                        @else
                            <option value="{{ $state->id }}">{{ $state->description }}</option>
                        @endif
                    @endforeach
                </select>
                <div class="invalid-feedback">Por favor, seleccione un estado.
                </div>
            </div>
        </div>

        <div class="col-12 d-flex flex-sm-row flex-column mt-2">
            <button type="submit" id="submit"
                class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1 col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">Actualizar
            </button>
        </div>
    </div>
</form>


<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset(mix('js/scripts/administrator/administrative/people/edit.js')) }}"></script>
