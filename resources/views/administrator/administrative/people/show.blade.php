<div class="row">
    <div class="media mb-2">
        <img src="https://s3.amazonaws.com/cnp.com.co/{{ $data->getUser->avatar }}" title=""
            class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" width="150" />
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label for="tarjeta_profesional"><b>Tarjeta profesional</b></label> <br>
            <input type="text" class="form-control" value="{{ $data->professional_card }}" disabled>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="tipo_de_documento_profesional"><b>(*) Tipo de documento</b></label> <br>
            <input type="text" class="form-control" value="{{ $data->documentType->description }}" disabled>
        </div>
    </div>


    <div class="col-md-6">
        <div class="form-group">
            <label for="identificacion_profesional"><b>(*) Número de identificación</b></label>
            <br>
            <input type="text" class="form-control" value="{{ $data->id }}" disabled>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="primer_nombre_profesional"><b>(*) Primer nombre</b></label> <br>
            <input type="text" class="form-control" value="{{ $data->first_name }}" disabled>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="segundo_nombre_profesional"><b>Segundo nombre</b></label> <br>
            <input type="text" class="form-control" value="{{ $data->second_name }}" disabled>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="primer_apellido_profesional"><b>(*) Primer apellido</b></label> <br>
            <input type="text" class="form-control" value="{{ $data->first_last_name }}" disabled>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="segundo_apellido_profesional"><b>Segundo apellido</b></label> <br>
            <input type="text" class="form-control" value="{{ $data->second_last_name }}" disabled>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="correo_profesional"><b>(*) Correo electrónico</b></label> <br>
            <input type="text" class="form-control" value="{{ $data->email }}" disabled>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="celular_profesional"><b>(*) Celular</b></label> <br>
            <input type="text" class="form-control" value="{{ $data->cellphone }}" disabled>
        </div>
    </div>


    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="form-group">
            <label for="type"><b>(*) Tipo de persona</b></label>
            @if ($data->type == 'suscriber')
                <input type="text" class="form-control" value="Suscriptor" disabled>
            @else
                <input type="text" class="form-control" value="Talento humano" disabled>
            @endif
        </div>
        <hr>
    </div>

    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="form-group">
            <label for="type"><b>(*) Nombre de usuario</b></label>
            <input type="text" class="form-control" value="{{ $data->getUser->name }}" disabled>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="form-group">
            <label for="type"><b>(*) Correo de usuario</b></label>
            <input type="text" class="form-control" value="{{ $data->getUser->email }}" disabled>
        </div>
    </div>
</div>
