@foreach ($all_answers as $answer)
    <div class="contenedor_respuestas col-12 col-md-12 col-sm-12 col-lg-12 col-xl-12">
        <div class="informacion_respuestas">
            <div class="row">
                <div class="descripcion_answer col-12 col-sm-10 col-md-12 col-lg-10 col-xl-10"
                    style="overflow: hidden; word-wrap: break-word;">
                    <p>
                        {!! $answer->description !!}
                    </p>
                </div>
                <div class="usuario_registro col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                    <button type="button" class="btn btn-danger btn-sm del-answers"
                        data-code="{{ $answer->id }}">X</button>
                </div>
            </div>

            <div class="informacion_registro_respuesta">
                <div class="row">
                    <div class="fecha_registro col">
                        <b>Fecha:</b> {{ $answer->created_at }}
                    </div>
                    <div class="usuario_registro col">
                        <b>Por:</b> {{ $answer->created_by }}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endforeach

<script>
    $(".del-answers").click(function() {
        var code = $(this).attr("data-code");

        $.ajax({
            type: "GET",
            url: "/app/answers/delete/" + code,
            success: function(data) {
                deleteSuccess();
                loadingAnswer();
            },
            error: function(err) {},
        });
    });
</script>
