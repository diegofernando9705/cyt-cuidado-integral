<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">

<style>
    .select2-selection__arrow {
        display: none;
    }
</style>

<form class="formulario_importacion" enctype="multipart/form-data" novalidate>
    @csrf
    @method('POST')
    <div class="form-group">
        <div class="alert alert-success" style="padding: 8px;">
            <p>
                Inicie desde la fila 1 indicando: Fecha inicial del indicador (YYYY-MM-DD), Fecha final del indicador
                (YYYY-MM-DD), Valor del indicador (si no posee, coloque 0), Porcentaje del indicador (si no posee,
                coloque 0)
            </p>
        </div>
        <a class="btn btn-success btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
            href="{{ asset('plantillas/plantillaIndicadoresEconomicos.csv') }}"
            download="plantillaIndicadoresEconomicos.csv">
            Descargar plantilla
        </a>
    </div>
    <div class="form-group">
        <label for="">(*) Seleccione el archivo en formato excel</label>
        <input type="file" class="form-control" id="file" name="file" required>
        <div class="invalid-feedback">Por favor, este campo es importante.</div>
    </div>
    <div class="form-group">
        <label for="indicador">(*) Seleccione el indicador</label>
        <select class="form-control selector" name="indicador_economico" id="indicador_economico" required>
            <option selected value disabled>Seleccione un indicador</option>
            <option value="UVR">UVR</option>
            <option value="TRM">TRM</option>
            <option value="DTF">DTF</option>
            <option value="IPC">IPC</option>
            <option value="IBC">IBC</option>
            <option value="USURA">USURA</option>
        </select>
        <div class="invalid-feedback">Por favor, este campo es importante.</div>
    </div>
    <br>
    <div class="boton-registro col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="text-align: center;">
        <button type="submit" class="btn btn-primary" id="submit">Actualizar indicadores</button>
    </div>
</form>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset(mix('js/scripts/administrator/administrative/economic-indicators/create.js')) }}"></script>

<script>
    $('.selector').select2();
</script>
