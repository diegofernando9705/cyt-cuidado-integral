@extends('administrator.layouts.contentLayoutMaster')
@section('title', 'Indicadores económicos')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('content')

    <style>
        .modal-open .modal {
            outline: none;
        }
    </style>

    <div class="row">
        <div class="col-12">
        </div>
    </div>

    <!-- Basic table -->
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card" style="padding: 10px 10px 10px 10px;">
                    <div class="card-header border-bottom">
                        <table class="col-12 col-sm-12 col-md-12 col-xl-12 col-lg-12">
                            <tr>
                                <td>
                                    <h4 class="card-title">Listado de los indicadores económicos</h4>
                                </td>
                                @can('indicadores-economicos-import')
                                    <td>
                                        <button class="btn btn-success btn-sm  ml-auto" id="actualizar_indicadores_excel">
                                            ACTUALIZAR INDICADORES EXCEL
                                        </button>
                                    </td>
                                @endcan
                            </tr>
                        </table>

                    </div>
                    <table class="datatables-indicadores-economicos table table-hover">
                        <thead>
                            <tr>
                                <th></th>
                                <th>FECHA INICIO</th>
                                <th>FECHA FIN</th>
                                <th>VALOR INDICADOR</th>
                                <th>PORCENTAJE</th>
                                <th>TIPO INDICADOR</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>


    <div class="modal fade" id="modal-cursos" role="dialog" data-bs-focus="false" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="titulo_modal">Información</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="contenido_modal">
                </div>
            </div>
        </div>
    </div>


    <div class="toast toast-basic hide position-fixed actualizacion-exitosa" role="alert" aria-live="assertive"
        aria-atomic="true" data-delay="5000" style="top: 1rem; right: 1rem" id="registro-exitoso">
        <div class="toast-header bg-success" style="color: white;">
            <strong class="mr-auto">Notificación</strong>
            <small class="text-muted">Justo ahora</small>
            <button type="button" class="ml-1 close" data-dismiss="toast" aria-label="Close" style="color: white;">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">¡Felicidades, actualización exitosa!</div>
    </div>
@endsection


@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="https://cdn.ckeditor.com/4.20.1/basic/ckeditor.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset(mix('js/scripts/administrator/administrative/economic-indicators/index.js')) }}"></script>
@endsection
