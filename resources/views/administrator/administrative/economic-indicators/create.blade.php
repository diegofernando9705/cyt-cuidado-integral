@extends('administrator.layouts.contentLayoutMaster')

@section('title', 'Nuevo registro')

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="https://s3.amazonaws.com/cnp.com.co/bootstrap-tagsinput.css">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
            <!-- Bootstrap Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-body">

                        <form class="store-indicador-economico" novalidate>
                            @csrf
                            @method('POST')


                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                    <div class="form-group">
                                        <label for="date_start_indicator"><b>(*) Fecha inicio del indicador</b></label>
                                        <input type="date" class="form-control" id="date_start_indicator"
                                            name="date_start_indicator" required>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                    <div class="form-group">
                                        <label for="date_end_indicator"><b> Fecha fin del indicador</b></label>
                                        <input type="date" class="form-control" id="date_end_indicator"
                                            name="date_end_indicator">
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <label for="value_start_indicator"><b>(*) Valor del indicador</b></label>
                                        <input type="text" class="form-control" id="value_start_indicator"
                                            name="value_start_indicator" value="0" required>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <label for="percentage_start_indicator"><b>(*) Porcentaje del indicador</b></label>
                                        <input type="text" class="form-control" id="percentage_start_indicator"
                                            name="percentage_start_indicator" value="0" required>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>


                                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <label for="description_start_indicator"><b>(*) Descripcion del
                                                indicador</b></label>
                                        <select class="form-control selector" name="description_start_indicator"
                                            id="description_start_indicator" required>
                                            <option selected disabled></option>
                                            <option value="UVR">UVR</option>
                                            <option value="TRM">TRM</option>
                                            <option value="DTF">DTF</option>
                                            <option value="IPC">IPC</option>
                                            <option value="IBC">IBC</option>
                                        </select>
                                        <div class="invalid-feedback">Por favor, este campo es importante.</div>
                                    </div>
                                </div>

                                <div class="col-12 d-flex flex-sm-row flex-column mt-2">
                                    <button type="submit" id="submit"
                                        class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">Registrar Indicador</button>
                                </div>
                            </div>
                        </form>

                        <!-- Basic toast -->
                        <div class="toast toast-basic hide position-fixed registro-exitoso" role="alert"
                            aria-live="assertive" aria-atomic="true" data-delay="5000" style="top: 1rem; right: 1rem"
                            id="registro-exitoso">
                            <div class="toast-header bg-success" style="color: white;">
                                <strong class="mr-auto">Notificación</strong>
                                <small class="text-muted">Justo ahora</small>
                                <button type="button" class="ml-1 close" data-dismiss="toast" aria-label="Close"
                                    style="color: white;">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="toast-body">¡Felicidades, registro exitoso!</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset(mix('js/scripts/administrator/administrative/economic-indicators/create.js')) }}"></script>
    <script>
        var input = document.getElementById("value_start_indicator");
        var porcentaje = document.getElementById("percentage_start_indicator");

        input.addEventListener("input", function() {
            var valor = input.value;
            var esNumeroDecimal = /^\d*\.?\d*$/.test(valor);

            if (!esNumeroDecimal) {
                input.value = valor.substring(0, valor.length - 1);
            }
        });

        porcentaje.addEventListener("input", function() {
            var valor = porcentaje.value;
            var esNumeroDecimal = /^\d*\.?\d*$/.test(valor);

            if (!esNumeroDecimal) {
                porcentaje.value = valor.substring(0, valor.length - 1);
            }
        });
    </script>
@endsection
