@extends('administrator.layouts.contentLayoutMaster')

@section('title', 'Panel Principal')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap.min.css')) }}">
@endsection
@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-invoice-list.css')) }}">
@endsection

@section('content')
    <!-- Dashboard Analytics Start -->
    <section id="dashboard-analytics">
        <div class="row match-height">
            <!-- Greetings Card starts -->
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card card-congratulations">
                    <div class="card-body text-center">
                        <img src="{{ asset('images/elements/decore-left.png') }}" class="congratulations-img-left"
                            alt="card-img-left" />
                        <img src="{{ asset('images/elements/decore-right.png') }}" class="congratulations-img-right"
                            alt="card-img-right" />
                        <div class="avatar avatar-xl bg-primary shadow">
                            <div class="avatar-content">
                                <i data-feather="award" class="font-large-1"></i>
                            </div>
                        </div>
                        <div class="text-center">
                            <h1 class="mb-1 text-white">Bienvenido {{ auth()->user()->name }},</h1>
                            <p class="card-text m-auto w-75">
                                Disfrute del sistema desarrollado para su comodidad y seguridad

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Greetings Card ends -->

            <!-- data generales -->
            <div class="col-lg-6 col-sm-6 col-12">
                <div class="row">

                    <div class="col-lg-6 col-md-6 col-12">
                        <a href="" style="text-decoration: none; color:black;">
                            <div class="card card-user-timeline">
                                <div class="card-header">
                                    <div class="d-flex align-items-center">
                                        <i data-feather="heart" class="user-timeline-title-icon"></i>
                                        <h4 class="card-title">Talento humano</h4>
                                    </div>
                                    <h4 class="card-title" style="font-weight: bold"></h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <a href="" style="text-decoration: none; color:black;">
                            <div class="card card-user-timeline">
                                <div class="card-header">
                                    <div class="d-flex align-items-center">
                                        <i data-feather="users" class="user-timeline-title-icon"></i>
                                        <h4 class="card-title">Suscriptores</h4>
                                    </div>
                                    <h4 class="card-title" style="font-weight: bold"></h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <a href="" style="text-decoration: none; color:black;">
                            <div class="card card-user-timeline">
                                <div class="card-header">
                                    <div class="d-flex align-items-center">
                                        <i data-feather="package" class="user-timeline-title-icon"></i>
                                        <h4 class="card-title">Programas</h4>
                                    </div>
                                    <h4 class="card-title" style="font-weight: bold"></h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <a href="" style="text-decoration: none; color:black;">
                            <div class="card card-user-timeline">
                                <div class="card-header">
                                    <div class="d-flex align-items-center">
                                        <i data-feather="briefcase" class="user-timeline-title-icon"></i>
                                        <h4 class="card-title">Profesiones</h4>
                                    </div>
                                    <h4 class="card-title" style="font-weight: bold"></h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <a href="" style="text-decoration: none; color:black;">
                            <div class="card card-user-timeline">
                                <div class="card-header">
                                    <div class="d-flex align-items-center">
                                        <i data-feather="home" class="user-timeline-title-icon"></i>
                                        <h4 class="card-title">EPS</h4>
                                    </div>
                                    <h4 class="card-title" style="font-weight: bold">1</h4>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- fin data generales -->

            <!-- Analisis de patologias -->
            <div class="col-lg-6 col-sm-6 col-12">
                <div class="card card-app-design">
                    <div class="card-body">
                        <div class="card-header flex-column align-items-start pb-0">
                            <h2 class="font-weight-bolder mt-1">Análisis de Patologías</h2>
                            <p class="card-text">Promedio de Patologias en la IPS</p>
                        </div>
                        <div class="card-header flex-column align-items-start pb-0">
                            <canvas id="patologias-ips"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Dashboard Analytics end -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js"></script>
@endsection
