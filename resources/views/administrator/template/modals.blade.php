<style>
    #modal-two {
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        background-color: rgba(0, 0, 0, 0.575);
        z-index: 9999 !important;
    }

    .swal2-container {
        z-index: 10000; /* Un z-index alto para asegurarse de que esté por encima de la modal */
    }
</style>

<div class="modal fade" id="modal" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static"
    data-keyboard="false" style="overflow-y: scroll;">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document" data-backdrop="static" data-keyboard="false">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-content"></div>
            <div class="modal-body" id="modal-content-additional"></div>
            <div class="modal-footer" id="modal-footer"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-two" tabindex="2" role="dialog" aria-hidden="true" data-backdrop="static"
    data-keyboard="false" style="overflow-y: scroll;">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document" data-backdrop="static"
        data-keyboard="false">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal-title-two"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-content-two">

            </div>
        </div>
    </div>
</div>
