<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">

<form class="{{ isset($data) ? 'form-lesson-update' : 'form-lesson' }}" novalidate>
    @csrf
    @method('POST')
    <input type="hidden" id="code_module" value="{{ isset($code_module) ? $code_module : '' }}">
    <input type="hidden" id="code_lesson" value="{{ isset($data) && is_object($data) ? $data->code : '' }}">

    <div class="form-group" style="text-align:left;">
        <label><b>Imagen de la lección</b></label>
        <input type="file" class="form-control" id="image" name="image" accept="image/*"
            {{ isset($data) ? '' : 'required' }}>
    </div>

    <div class="form-group" style="text-align:left;">
        <label><b>(*) Titulo de la lección</b></label>
        <input type="text" class="form-control" id="title" name="title"
            value="{{ isset($data) && is_object($data) ? $data->title : '' }}">
    </div>

    <div class="form-group" style="text-align:left;">
        <label><b>Descripción de la lección</b></label>
        <textarea id="summary" class="form-control" name="summary" required>{{ isset($data) && is_object($data) ? $data->summary : '' }}</textarea>
    </div>

    <div class="form-group" style="text-align:left;">
        <label><b>(*) Tipo de la lección</b></label>
        <select class="form-control" id="type" name="type" required>
            <option value="pdf" {{ isset($data->type) && $data->type === 'pdf' ? 'selected' : '' }}>Documentos PDF
            </option>
            <option value="mp4" {{ isset($data->type) && $data->type === 'mp4' ? 'selected' : '' }}>Video MP4
            </option>
            <option value="image" {{ isset($data->type) && $data->type === 'image' ? 'selected' : '' }}>Imagenes
            </option>
        </select>
    </div>

    <div class="form-group" id="container_type">
    </div>
    <button class="btn btn-primary btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" type="submit"
        id="submit-lessons">Registrar</button>
</form>
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>

<script>
    var formLesson = document.querySelectorAll('.form-lesson');

    // Loop over them and prevent submission
    Array.prototype.slice.call(formLesson).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {

                document.getElementById("image").disabled = true;
                document.getElementById("title").disabled = true;
                document.getElementById("summary").disabled = true;
                document.getElementById("type").disabled = true;
                document.getElementById("submit-lessons").disabled = true;

                var paqueteDeDatos = new FormData();

                var code_module = $("#code_module").prop('value');

                paqueteDeDatos.append('image', $('#image')[0].files[0]);
                paqueteDeDatos.append('title', $("#title").prop('value'));
                paqueteDeDatos.append('summary', $("#summary").prop('value'));
                paqueteDeDatos.append('type', $("#type").prop('value'));


                var inpFiles = document.getElementById("archives");
                for (const file of inpFiles.files) {
                    paqueteDeDatos.append("archivos[]", file);
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '/app/learning/courses/edit/module/lessons/store/' + code_module,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data[0] === false && data[1] == "file_error") {
                            document.getElementById("image").disabled = false;
                            document.getElementById("title").disabled = false;
                            document.getElementById("summary").disabled = false;
                            document.getElementById("type").disabled = false;
                            document.getElementById("submit-lessons").disabled = false;

                            errorFile();
                        } else {
                            storeSuccess();

                            setTimeout(() => {
                                $("#modal-two").modal("hide");
                                loadingModules(data[1])
                            }, 1000);
                        }
                    },
                    error: function(err) {
                        errorHttp(err.status);

                        document.getElementById("image").disabled = false;
                        document.getElementById("title").disabled = false;
                        document.getElementById("summary").disabled = false;
                        document.getElementById("type").disabled = false;
                        document.getElementById("submit-lessons").disabled = false;

                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function(i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });


    var formLessonUpdate = document.querySelectorAll('.form-lesson-update');

    // Loop over them and prevent submission
    Array.prototype.slice.call(formLessonUpdate).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {

                document.getElementById("image").disabled = true;
                document.getElementById("title").disabled = true;
                document.getElementById("summary").disabled = true;
                document.getElementById("type").disabled = true;
                document.getElementById("submit-lessons").disabled = true;

                var paqueteDeDatos = new FormData();

                var code_lesson = $("#code_lesson").val();

                paqueteDeDatos.append('code_module', $("#code_module").val());
                paqueteDeDatos.append('image', $('#image')[0].files[0]);
                paqueteDeDatos.append('title', $("#title").prop('value'));
                paqueteDeDatos.append('summary', $("#summary").prop('value'));
                paqueteDeDatos.append('type', $("#type").prop('value'));

                var inpFiles = document.getElementById("archives");

                if (inpFiles) {
                    for (const file of inpFiles.files) {
                        paqueteDeDatos.append("archivos[]", file);
                    }
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: "/app/learning/courses/module/lessons/update/" + code_lesson,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data[0] === false && data[1] == "file_error") {
                            document.getElementById("image").disabled = false;
                            document.getElementById("title").disabled = false;
                            document.getElementById("summary").disabled = false;
                            document.getElementById("type").disabled = false;
                            document.getElementById("submit-lessons").disabled = false;

                            errorFile();
                        } else {
                            storeSuccess();

                            setTimeout(() => {
                                $("#modal-two").modal("hide");
                                loadingModules(data[1])
                            }, 1000);
                        }
                    },
                    error: function(err) {
                        errorHttp(err.status);

                        document.getElementById("image").disabled = false;
                        document.getElementById("title").disabled = false;
                        document.getElementById("summary").disabled = false;
                        document.getElementById("type").disabled = false;
                        document.getElementById("submit-lessons").disabled = false;

                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function(i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });

    $(document).on("change", "#type", function() {
        var value = $(this).val();
        var container = document.getElementById("container_type");
        container.innerHTML = "";

        switch (value) {
            case 'pdf':
                var pdf = document.createElement("input");
                pdf.setAttribute("type", "file");
                pdf.setAttribute("class", "form-control");
                pdf.setAttribute("name", "archives");
                pdf.setAttribute("id", "archives");
                pdf.setAttribute("multiple", "");
                pdf.setAttribute("accept", ".pdf");
                pdf.setAttribute("required", true);
                container.appendChild(pdf);

                break;
            case 'mp4':
                var video = document.createElement("input");
                video.setAttribute("type", "file");
                video.setAttribute("class", "form-control");
                video.setAttribute("name", "archives");
                video.setAttribute("id", "archives");
                video.setAttribute("accept", "video/*");
                video.setAttribute("required", true);
                container.appendChild(video);

                break;
            case 'image':
                var image = document.createElement("input");
                image.setAttribute("type", "file");
                image.setAttribute("class", "form-control");
                image.setAttribute("name", "archives");
                image.setAttribute("id", "archives");
                image.setAttribute("multiple", "");
                image.setAttribute("accept", "image/png, image/jpeg, image/jpg");
                image.setAttribute("required", true);
                container.appendChild(image);

                break;
            default:
                break;
        }
    });


    function loadingModules(courseId) {
        Swal.close();

        $.ajax({
            type: "GET",
            url: "/app/learning/courses/edit/modules/" + courseId,
            success: function(data) {
                $("#modal-content").html(data);
                Swal.close();
            },
            error: function(error) {
                errorHttp(error.status);
            },
        });
    }
</script>
