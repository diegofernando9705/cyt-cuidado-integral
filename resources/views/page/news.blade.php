@extends('page-layouts.template.app')

@section('title', 'Noticias')

@section('style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link href="{{ asset('page/assets/css/style_pagina_servicios.css') }}" rel="stylesheet">
    <style>
        .titleNew{
            line-height: 18px !important;
            color: black;
        }

        .service-item {
            text-align: justify;
            font-size: "Roboto";
            border: 1px solid #b3b3b3;
            padding: 0px;
        }

        .service-item:hover {
            cursor: pointer;
            background-color: #002E66;
            color: white;
            font-size: "Roboto";
        }

        .service-item h5 {
            padding-top: 15px;
            margin-bottom: -10px;
            color: black;
            text-align: center !important;
        }

        .service-item p {
            padding-left: 20px;
            padding-right: 20px;
        }

        .espacio {
            height: 100px;
        }

        .body_servicios {
            padding-right: 40px;
        }

        .body_lateral {
            border-left: 1px solid rgb(168, 168, 168);
            ;
        }

        .titulo:hover {
            padding: 0px;
            cursor: pointer;
            text-decoration-line: underline;
        }

        .noticia a {
            color: #000000;
        }

        .portfolio-item .portfolio-overlay {
            position: absolute;
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            background: #002e6680 !important;
            transition: .5s;
            opacity: 0;

        }

        .mx-2 {
            color: #002E66 !important;
        }

        .active {
            border-color: black !important;
        }

        .mx-2:hover {
            border-color: black !important;
        }
    </style>
@endsection

@section('navegacion')
    {!! $subview !!}
@endsection

@section('container')
    <div class="container">
        <div class="row">
            <div class="contenedor-news col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8" style="margin-top:20px; margin-bottom:20px; ">
                <div class="categories col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"></div>
                <div class="news col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="row" id="container-news"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('codigo')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar/index.global.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            var container = document.getElementById("container-news");

            $.ajax({
                type: "GET",
                url: "{{ route('news-all') }}",
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        var newData = data[i];

                        var portafolioItem = document.createElement("div");
                        portafolioItem.setAttribute("class",
                            "portfolio-item wow fadeInUp col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4"
                            );
                        portafolioItem.setAttribute("data-wow-delay", "0.1s");
                        container.appendChild(portafolioItem);

                        var rounded = document.createElement("div");
                        rounded.setAttribute("class", "rounded overflow-hidden");
                        rounded.setAttribute("style", "border:1px solid rgb(168, 168, 168);");
                        portafolioItem.appendChild(rounded);

                        var positionRelative = document.createElement("div");
                        positionRelative.setAttribute("class", "position-relative overflow-hidden");
                        rounded.appendChild(positionRelative);

                        var image = document.createElement("img");
                        image.setAttribute("class", "img-fluid w-100");
                        image.setAttribute("src", "https://s3.amazonaws.com/cnp.com.co/" + newData.image);
                        positionRelative.appendChild(image);

                        var portafolioOverlay = document.createElement("img");
                        portafolioOverlay.setAttribute("class", "portfolio-overlay");
                        positionRelative.appendChild(portafolioOverlay);

                        var bgLight = document.createElement("div");
                        bgLight.setAttribute("class", "bg-light p-4");
                        bgLight.innerHTML = "<h5 class='lh-base mb-0 titleNew'>"+newData.title+"</h5>";
                        rounded.appendChild(bgLight);

                        var link = document.createElement("a");
                        link.setAttribute("class", "btn btn-dark btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12");
                        link.setAttribute("href", "{{ url('news') }}" + "/" + newData.url);
                        link.innerHTML = "Ver noticia";
                        rounded.appendChild(link);
                    }
                }
            });
        });
    </script>
@endsection
