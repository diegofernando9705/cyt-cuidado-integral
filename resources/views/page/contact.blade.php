@extends('page-layouts.template.app')

@section('title', 'Contáctanos')

@section('style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link href="{{ asset('page/assets/css/style_pagina_inicio.css') }}" rel="stylesheet">

    <style>
        .btn-application {
            background-color: #002e66 !important;
            color: white;
        }
    </style>
@endsection

@section('navegacion')
    {!! $subview !!}
@endsection

@section('container')
    <div class="container-xxl py-5">
        <div class="container px-lg-5">
            <div class="wow fadeInUp col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" data-wow-delay="0.1s">
                <div class="icons-social" style="float: left; position: relative;">
                    <svg fill="#002e66" width="40px" height="40px" viewBox="0 0 24 24"
                        xmlns="http://www.w3.org/2000/svg">
                        <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                        <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                        <g id="SVGRepo_iconCarrier">
                            <path
                                d="M12 2.03998C6.5 2.03998 2 6.52998 2 12.06C2 17.06 5.66 21.21 10.44 21.96V14.96H7.9V12.06H10.44V9.84998C10.44 7.33998 11.93 5.95998 14.22 5.95998C15.31 5.95998 16.45 6.14998 16.45 6.14998V8.61998H15.19C13.95 8.61998 13.56 9.38998 13.56 10.18V12.06H16.34L15.89 14.96H13.56V21.96C15.9164 21.5878 18.0622 20.3855 19.6099 18.57C21.1576 16.7546 22.0054 14.4456 22 12.06C22 6.52998 17.5 2.03998 12 2.03998Z">
                            </path>
                        </g>
                    </svg>
                    <svg fill="#002e66" height="35px" width="35px" version="1.1" id="Layer_1"
                        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                        viewBox="-143 145 512 512" xml:space="preserve">
                        <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                        <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                        <g id="SVGRepo_iconCarrier">
                            <g>
                                <path
                                    d="M113,446c24.8,0,45.1-20.2,45.1-45.1c0-9.8-3.2-18.9-8.5-26.3c-8.2-11.3-21.5-18.8-36.5-18.8s-28.3,7.4-36.5,18.8 c-5.3,7.4-8.5,16.5-8.5,26.3C68,425.8,88.2,446,113,446z">
                                </path>
                                <polygon points="211.4,345.9 211.4,308.1 211.4,302.5 205.8,302.5 168,302.6 168.2,346 ">
                                </polygon>
                                <path
                                    d="M183,401c0,38.6-31.4,70-70,70c-38.6,0-70-31.4-70-70c0-9.3,1.9-18.2,5.2-26.3H10v104.8C10,493,21,504,34.5,504h157 c13.5,0,24.5-11,24.5-24.5V374.7h-38.2C181.2,382.8,183,391.7,183,401z">
                                </path>
                                <path
                                    d="M113,145c-141.4,0-256,114.6-256,256s114.6,256,256,256s256-114.6,256-256S254.4,145,113,145z M241,374.7v104.8 c0,27.3-22.2,49.5-49.5,49.5h-157C7.2,529-15,506.8-15,479.5V374.7v-52.3c0-27.3,22.2-49.5,49.5-49.5h157 c27.3,0,49.5,22.2,49.5,49.5V374.7z">
                                </path>
                            </g>
                        </g>
                    </svg>
                    <svg fill="#002e66" width="35px" height="35px" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"
                        stroke="#002e66">
                        <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                        <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                        <g id="SVGRepo_iconCarrier">
                            <path
                                d="M10 .4C4.698.4.4 4.698.4 10s4.298 9.6 9.6 9.6 9.6-4.298 9.6-9.6S15.302.4 10 .4zm3.905 7.864c.004.082.005.164.005.244 0 2.5-1.901 5.381-5.379 5.381a5.335 5.335 0 0 1-2.898-.85c.147.018.298.025.451.025.886 0 1.701-.301 2.348-.809a1.895 1.895 0 0 1-1.766-1.312 1.9 1.9 0 0 0 .853-.033 1.892 1.892 0 0 1-1.517-1.854v-.023c.255.141.547.227.857.237a1.89 1.89 0 0 1-.585-2.526 5.376 5.376 0 0 0 3.897 1.977 1.891 1.891 0 0 1 3.222-1.725 3.797 3.797 0 0 0 1.2-.459 1.9 1.9 0 0 1-.831 1.047 3.799 3.799 0 0 0 1.086-.299 3.834 3.834 0 0 1-.943.979z">
                            </path>
                        </g>
                    </svg>
                </div>
                <h1 class="text-center mb-5">¡Contáctanos ahora!
                    <hr>
                </h1>
            </div>

            <div class="row g-4 fadeInUp" data-wow-delay="0.1s">
                <form class="contact-page" id="contact-page" novalidate>
                    @csrf
                    @method('POST')
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="form-group col">
                            <label for="nombres">(*) Nombres:</label>
                            <input type="text" class="form-control" id="nombres" name="nombres" required>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                        <div class="form-group col">
                            <label for="apellidos">(*) Apellidos:</label>
                            <input type="text" class="form-control" id="apellidos" name="apellidos" required>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 10px;">
                        <div class="form-group col">
                            <label for="correos">(*) Correo:</label>
                            <input type="email" class="form-control" id="correos" name="correos" required>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                        <div class="form-group col">
                            <label for="celular">(*) Celular:</label>
                            <input type="text" class="form-control" id="celular" name="celular" required>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col">
                            <label for="mensaje">(*) Mensaje:</label>
                            <textarea class="form-control" id="mensaje" name="mensaje" required></textarea>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="boton col-12 col-sm-12 col-md-12 col-xl-12 col-lg-12"
                        style="text-align: center; margin-top:30px;">
                        <button type="submit" class="btn btn-application" id="envio_submit">Enviar formulario</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

@section('codigo')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar/index.global.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(function() {
            'use strict';

            const formulario_usuario = document.querySelectorAll('.contact-page');

            Array.prototype.slice.call(formulario_usuario).forEach((form) => {
                form.addEventListener('submit', (event) => {

                    if (!form.checkValidity()) {
                        event.stopPropagation();
                    } else {

                        var paqueteDeDatos = new FormData(document.getElementById("contact-page"));

                        document.getElementById("nombres").disabled = true;
                        document.getElementById("apellidos").disabled = true;
                        document.getElementById("correos").disabled = true;
                        document.getElementById("celular").disabled = true;
                        document.getElementById("mensaje").disabled = true;
                        document.getElementById("envio_submit").disabled = true;


                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                            }
                        });

                        $.ajax({
                            type: 'POST',
                            contentType: false,
                            url: "{{ route('contact-store') }}",
                            headers: {
                                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: paqueteDeDatos,
                            processData: false,
                            cache: false,
                            success: function(data) {

                                Swal.fire({
                                    title: '¡Recibimos su mensaje!',
                                    text: "Te informamos que nuestro sistema registro sus datos. En unos instantes uno de nuestros colabores se pondrá en contacto con nosotros",
                                    icon: 'success',
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Yes, delete it!'
                                }).then((result) => {

                                });


                                setTimeout(function() {
                                    window.location = "/contact";
                                }, 3000);

                            },
                            error: function(err) {
                                errorHttp(err.status);
                                document.getElementById("nombres").disabled = false;
                                document.getElementById("apellidos").disabled = false;
                                document.getElementById("correos").disabled = false;
                                document.getElementById("celular").disabled = false;
                                document.getElementById("mensaje").disabled = false;
                                document.getElementById("envio_submit").disabled =
                                    false;

                                if (err.status == 422) {
                                    console.log(err.responseJSON);
                                    $('#success_message').fadeIn().html(err.responseJSON
                                        .message);
                                    console.warn(err.responseJSON.errors);
                                    $.each(err.responseJSON.errors, function(i, error) {
                                        var el = $(document).find('[name="' +
                                            i + '"]');
                                        el.after($('<span style="color: red;">' +
                                            error[0] + '</span>'));
                                    });
                                }
                            },
                        });


                        event.preventDefault();
                    }

                    event.preventDefault();
                    form.classList.add('was-validated');
                }, false);
            });
        });
    </script>
@endsection
