@extends('page-layouts.template.app')

@section('title', 'Preguntas fecuentes')

@section('style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link href="{{ asset('page/assets/css/style_pagina_preguntasfrecuentes.css') }}" rel="stylesheet">
@endsection

@section('navegacion')
    {!! $subview !!}
@endsection

@section('container')
    <div class="container-xxl" style="margin-top: 10px !important; margin-bottom:30px;">
        <div class="acordeon">
            <div class='wrapper'>
                @foreach ($questions as $question)
                    <input id='{{ $question->code }}' type='checkbox'>
                    <label for='{{ $question->code }}'>
                        <p>{{ $question->question }}</p>
                        <div class='lil_arrow'></div>
                        <div class='content'>
                            {!! $question->answer !!}
                        </div>
                        <span></span>
                    </label>
                @endforeach
            </div>
        </div>
    </div>
    @include('page-layouts.template.banner-membership');

    @include('page-layouts.template.slider.customers');
@endsection

@section('codigo')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar/index.global.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endsection
