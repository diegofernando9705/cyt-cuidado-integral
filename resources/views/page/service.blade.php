@extends('page-layouts.template.app')

@section('title', $data->title)

@section('navegacion')
    <div class="encabezado-slider owl-carousel">
        <div class="single-box-encabezado text-center-encabezado">
            <div class="imagen_de_fondo_encabezado"
                style="background-image: url(https://s3.amazonaws.com/cnp.com.co/{{ $data->image }})">
            </div>
            <div class="contenedor_informacion_slider_encabezado">
                <div class="contenedor_titulo_slider_encabezado">
                </div>
                <div class="contenedor_descripcion_slider_encabezado">
                    <div class="row">
                        <div class="titulo_contenedor_slider col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            {{ $data->title }}
                        </div>
                        <div class="descripcion_contenedor_slider col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <p>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('container')

    <!-- Team Start -->
    <div class="container-xxl">
        <div class="container-xxl py-5 px-lg-5">
            <div class="row">
                <div class="body_servicios col-12 col-sm-10 col-md-10 col-xl-10">
                    <div class="resena row">
                        <div class="texto_resena col-12 col-sm-8 col-xl-8 col-md-8">
                            {!! $data->resena !!}
                        </div>
                    </div>
                    <hr>
                    <div class="contenido_noticia">
                        {!! $data->content !!}
                    </div>
                </div>

            </div>
        </div>
        @include('page-layouts.template.share-options')
    </div>

@endsection


@section('codigo')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
    <script>
        $('.encabezado-slider').owlCarousel({
            loop: true,
            nav: false,
            autoplay: true,
            autoplayTimeout: 5000,
            smartSpeed: 450,
            margin: 0,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 1
                },
                991: {
                    items: 1
                },
                1200: {
                    items: 1
                },
                1920: {
                    items: 1
                }
            }
        });
    </script>
@endsection
