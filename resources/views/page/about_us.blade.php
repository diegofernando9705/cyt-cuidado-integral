@extends('page-layouts.template.app')

@section('title', $data->title)

@section('style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link href="{{ asset('page/assets/css/style_pagina_inicio.css') }}" rel="stylesheet">
@endsection

@section('navegacion')
    {!! $subview !!}
@endsection

@section('container')
    <div class="container-xxl" style="margin-top: 10px !important;">
        <div class="container py-5 px-lg-5" style="margin-top: 10px !important;">
            <div class="row g-4">
                {!! $data->description !!}
                @include('page-layouts.template.share-options')
            </div>
        </div>
    </div>
@endsection

@section('codigo')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar/index.global.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endsection
