<style>
    .form-comments-course {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .button-submit {
        display: flex;
        justify-content: flex-end;
    }

    .alert {
        display: none;
        margin-top: 10px;
        padding-top: 10px;
        padding-left: 10px;
        padding-bottom: 10px;
    }

    .comment {
        margin-bottom: 15px;
        padding-bottom: 15px;
        border-bottom: 1px solid rgba(128, 128, 128, 0.322);
    }

    .avatar-user {
        text-align: center;
        align-content: center;
    }

    .avatar-user img {
        height: 50px;
    }

    .info-user {
        padding: 0px !important;
    }

    .info-user b {
        color: black;
    }
</style>

<div class="alert alert-info col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="notification-comment">
    <p>Comentarios realizado...</p>
</div>

<form class="form-comments-lesson col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" novalidate>
    @csrf
    @method('POST')
    <input type="hidden" id="lesson" value="{{ $lesson->code }}">
    <div class="form-group">
        <label for=""><b>Realizar comentario</b></label>
        <textarea class="form-control" name="comment" id="comment" required></textarea>
    </div>
    <div class="button-submit">
        <button type="submit" class="btn btn-sm btn-application" id="submit">Realizar comentario</button>
    </div>
</form>
<hr>
<b>
    Nuestros suscriptores dicen....
</b>
<div class="comments col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
    <hr>
    @foreach ($lesson->getComments as $comment)
        <div class="comment col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="row">
                <div class="avatar-user col-12 col-sm-1 col-md-1 col-lg-1 col-xl-1">
                    <img src="{{ config('app.AWS_BUCKET_URL') . $comment->getUser->avatar }}" alt="Avatar"
                        class="rounded-circle">
                </div>
                <div class="info-user col-12 col-sm-10 col-md-10 col-lg-10 col-xl-10">
                    <b><i>({{ $comment->created_at }}) | {{ $comment->getUser->name }} comentó:</i></b>
                    <div class="container-comment">
                        {{ $comment->comment }}
                    </div>
                </div>
            </div>

        </div>
    @endforeach
</div>

<script>
    var form_comments_course = document.querySelectorAll('.form-comments-lesson');

    // Loop over them and prevent submission
    Array.prototype.slice.call(form_comments_course).forEach((form) => {
        form.addEventListener('submit', (event) => {

            if (!form.checkValidity()) {
                event.stopPropagation();
            } else {
                document.getElementById("comment").disabled = true;
                document.getElementById("submit").disabled = true;

                var paqueteDeDatos = new FormData();

                paqueteDeDatos.append('lesson', $('#lesson').val());
                paqueteDeDatos.append('comment', $('#comment').val());

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: "/subscriber/course/lesson/comments/send",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: paqueteDeDatos,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        $("#notification-comment").fadeIn();

                        setTimeout(() => {
                            document.getElementById("comment").disabled = false;
                            document.getElementById("submit").disabled = false;

                            document.getElementById("comment").value = "";

                            $("#notification-comment").fadeOut();
                        }, 1500);

                        setTimeout(() => {
                            var id = $('#lesson').val();
                            $.ajax({
                                type: 'GET',
                                url: "/subscriber/course/lesson/selected/comments/" + id,
                                success: function(data) {
                                    $("#general-store").html(data);
                                },
                                error: function(error) {
                                    errorHttp(error.status);
                                },
                            });
                        }, 2000);
                    },
                    error: function(err) {
                        document.getElementById("comment").disabled = false;
                        document.getElementById("submit").disabled = false;

                        errorHttp(err.status);
                        if (err.status == 422) {
                            $(".errores").hide();
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON
                                .message);
                            console.warn(err.responseJSON.errors);
                            $.each(err.responseJSON.errors, function(i, error) {
                                var el = $(document).find('[name="' +
                                    i + '"]');
                                el.after($('<span class="errores" style="color: red;">' +
                                    error[0] + '</span>'));
                            });
                        }
                    },
                });
                event.preventDefault();
            }
            event.preventDefault();
            form.classList.add('was-validated');
        }, false);
    });
</script>
