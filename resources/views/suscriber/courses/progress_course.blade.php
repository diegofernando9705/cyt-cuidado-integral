<div class="general-course col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8 col-lg-8" id="general-store">
    @if ($course->video_image != null)
        <video src="{{ config('app.AWS_BUCKET_URL') . $course->video_image }}" style="width: 100%"></video>
    @else
        <img src="{{ config('app.AWS_BUCKET_URL') . $course->cover_image }}" alt="Imagen del curso" width="100%">
    @endif
    <hr>
    <div class="summary">
        <b>
            <h4>Resumen: </h4>
        </b>
        <p>
            {{ $course->summary }}
        </p>
    </div>
    <div class="description">
        <b>
            <h4>Descripción: </h4>
        </b>
        <p>
            {!! $course->description !!}
        </p>
    </div>
</div>
<div class="general-modules col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 col-lg-4">
    <div class="title-section">
        <p>Contenido del curso</p>
    </div>
    <div class="accordion" id="accordionExample">
        <div class="row">
            @php
                $i = 1;
            @endphp
            @foreach ($course->getModules as $module)
                <div class="container-module col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="row row-container-module" data-toggle="collapse"
                        data-target="#collapse{{ $module->code }}" aria-expanded="false"
                        aria-controls="collapse{{ $module->code }}">
                        <div class="image-module col-12 col-sm-1 col-md-1 col-lg-1 col-xl-1 rounded-circle"
                            style="background-image: url('{{ config('app.AWS_BUCKET_URL') . $module->image }}')">
                        </div>
                        <div class="title-module col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <p class="m-0">
                                Lección {{ $i++ }}: {{ $module->title }} <br>
                                <small>
                                    {{ count($module->lessons) }} Lección
                                </small>
                            </p>
                        </div>
                    </div>
                    <div id="collapse{{ $module->code }}" class="collapse" aria-labelledby="headingOne"
                        data-parent="#accordionExample">
                        <div class="card-body" style="padding: 0px !important;">
                            <div class="row">
                                @foreach ($module->lessons as $lesson)
                                    <div class="container-lesson col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
                                        data-code="{{ $lesson->code }}">
                                        <div class="row">
                                            <div id="lesson-svg-{{ $lesson->code }}" class="svg-lesson col-12 col-sm-1 col-md-1 col-lg-1 col-xl-1">
                                                @if (!$lesson->getUserQualification(auth()->user()->id))
                                                    <svg fill="#002e66" width="24px" height="24px"
                                                        viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <g id="SVGRepo_bgCarrier" stroke-width="0">
                                                        </g>
                                                        <g id="SVGRepo_tracerCarrier" stroke-linecap="round"
                                                            stroke-linejoin="round"></g>
                                                        <g id="SVGRepo_iconCarrier">
                                                            <title>box</title>
                                                            <path
                                                                d="M96 448Q82 448 73 439 64 430 64 416L64 96Q64 82 73 73 82 64 96 64L416 64Q430 64 439 73 448 82 448 96L448 416Q448 430 439 439 430 448 416 448L96 448ZM400 400L400 112 112 112 112 400 400 400Z">
                                                            </path>
                                                        </g>
                                                    </svg>
                                                @else
                                                    <svg width="26px" height="26px" viewBox="0 0 24 24"
                                                        fill="none"px xmlns="http:/px/www.w3.org/2000/svg">
                                                        <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                                        <g id="SVGRepo_tracerCarrier" stroke-linecap="round"
                                                            stroke-linejoin="round"></g>
                                                        <g id="SVGRepo_iconCarrier">
                                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                                d="M6 3C4.34315 3 3 4.34315 3 6V18C3 19.6569 4.34315 21 6 21H18C19.6569 21 21 19.6569 21 18V6C21 4.34315 19.6569 3 18 3H6ZM17.8 8.6C18.1314 8.15817 18.0418 7.53137 17.6 7.2C17.1582 6.86863 16.5314 6.95817 16.2 7.4L10.8918 14.4776L8.70711 12.2929C8.31658 11.9024 7.68342 11.9024 7.29289 12.2929C6.90237 12.6834 6.90237 13.3166 7.29289 13.7071L10.2929 16.7071C10.4979 16.9121 10.7817 17.018 11.0709 16.9975C11.3601 16.9769 11.6261 16.8319 11.8 16.6L17.8 8.6Z"
                                                                fill="#002e66"></path>
                                                        </g>
                                                    </svg>
                                                @endif
                                            </div>
                                            <div class="title-lesson col-12 col-sm-10 col-md-10 col-lg-10 col-xl-10">
                                                <p class="m-0">
                                                    {{ $lesson->title }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
