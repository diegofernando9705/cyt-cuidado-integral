<style>
    table {
        border-collapse: collapse;
    }

    td,
    th {
        border: 1px solid black;
        padding: 8px;
        width: 33.33%;
    }
</style>

<table class="tale table-bordered">
    <tr>
        <td rowspan="3">
            <img src="{{ config('app.AWS_BUCKET_URL') . $course->cover_image }}" alt="Imagen del curso"
                style="width: 100%;">
        </td>
        <td>
            <b>Fecha de inicio:</b> <br>
            {{ $start_date }}
        </td>
        <td>
            <b>Fecha de terminación:</b> <br>
            {{ $end_date }}
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <b>Titulo: </b> <br>
            {{ $course->title }}
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <b>Resumen: </b> <br>
            {{ $course->summary }}
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <b>Descripción: </b> <br>
            {!! $course->description !!}
        </td>
    </tr>
    <tr>
        <td>
            <b>Instructor: </b> <br>
            {{ $course->getInstructor->first_name . ' ' . $course->getInstructor->second_name . ' ' . $course->getInstructor->first_last_name }}
        </td>
        <td>
            <b>Nivel: </b> <br>
            {{ $course->getLevel->title }}
        </td>
        <td>
            <b>Categoria actual: </b> <br>
            {{ $course->getCategory->title }}
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <b>Módulos: </b> <br><br>
            <div class="row">
                @foreach ($course->getModules as $module)
                    <div class="module col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                        <button type="button" class="bnt btn-primary btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">{{ $module->title }}</button>
                        <ul style="margin-top: 8px;">
                            @foreach ($module->lessons as $lesson)
                                <li>
                                    {{ $lesson->title }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>
        </td>
    </tr>
</table>
