@extends('suscriber.layouts.contentLayoutMaster')

@section('title_nav', 'Cursos CNP')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset('page/assets/css/style_panel_de_control.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.css">
    <link href="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.css" rel="stylesheet" />

@endsection
@section('page-style')
    <link href="{{ asset('page/assets/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">

    <style>
        .header-section {
            padding-top: 15px;
            color: white !important;
            border-radius: 5px;
            background-color: #002e66 !important;
        }

        .header-section h1 {
            color: white;
            padding-bottom: 1% !important;
            padding-top: 1% !important;
        }

        .header-section ul {
            margin-top: 20px;
        }

        .header-section ul li {
            list-style: none;
            display: inline;
            padding-right: 20px;
            padding-left: 20px;
            padding-bottom: 7px;
        }

        .active_li {
            border-bottom: 8px solid white;
        }

        .filter-search {
            margin-bottom: 15px;
            margin-top: 15px;
        }

        .card-img-top {
            height: 200px;
            background-size: cover;
        }

        .card-title {
            font-weight: bold;
        }

        .image-hover {
            background-color: rgba(0, 0, 0, 0.308);
            height: 200px;
            width: 100%;
            position: absolute;
            display: none;

            display: flex;
            justify-content: center;
            /* Centrar horizontalmente */
            align-items: center;
            /* Centrar verticalmente */
        }

        .course-div:hover {
            cursor: pointer;
        }

        .list-group-item {
            font-size: 12px;
        }

        .general-course {
            padding-top: 20px;
        }

        .container-module {
            padding: 5px 5px 5px 5px;
            border-bottom: 1px solid rgba(128, 128, 128, 0.479);
        }

        .row-container-module:hover {
            background-color: rgba(128, 128, 128, 0.082);
            cursor: pointer;
        }

        .image-module {
            padding-top: 10px;
            height: 40px;
            background-size: cover;
        }

        .title-module p {
            font-weight: bold;
            color: #002e66;
        }

        .title-section {
            color: black;
            font-weight: bold;
            font-size: 18px;
            padding-top: 15px;
            padding-bottom: 8px;
            border-bottom: 1px solid rgba(128, 128, 128, 0.479);
        }

        .container-lesson {
            padding-top: 14px;
            padding-bottom: 14px;
        }

        .container-lesson:hover {
            cursor: pointer;
            background-color: rgba(128, 128, 128, 0.137);
        }

        .active-lesson{
            cursor: pointer;
            background-color: rgba(128, 128, 128, 0.137);
        }

        .options_menu_course:hover {
            cursor: pointer;
        }
    </style>
@endsection


@section('content')
    <!-- Dashboard Analytics Start -->
    <section id="dashboard-analytics" class="col-lg-12 col-md-12 col-sm-12 col-12 col-xl-12">
        <div class="row">
            <div class="header-section col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h1>
                    {{ $course->title }}
                </h1>
                <ul>
                    <li class="options_menu_course major_course active_li" id="major_course" data-id="{{ $course->code }}">Principal</li>
                    <li class="options_menu_course comments_course" id="comments_course" title="Comentarios del curso"
                        data-id="{{ $course->code }}">Comentarios</li>
                   {{--  <li>Información general</li>  --}}
                </ul>
            </div>
            <div class="container-section col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="container">
                        <div class="row" id="content-data">
                            @include('suscriber.courses.progress_course')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Dashboard Analytics end -->
    @include('administrator.template.modals')
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.js"></script>
    <script src="{{ asset(mix('js/scripts/subscriber/courses/index.js')) }}"></script>
@endsection
