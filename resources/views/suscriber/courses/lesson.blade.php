<style>
    .title-lesson h4 {
        color: black;
    }


    .rating {
        margin: 5%;
        display: flex;
        align-items: center;

        .stars {
            position: relative;

            .filled {
                position: absolute;
                left: 0;
            }
        }
    }

    .button-qualification {
        margin-top: 5%;
    }

    /* Checkbox */

    .checkbox-wrapper-10 .tgl {
        display: none;
    }

    .checkbox-wrapper-10 .tgl,
    .checkbox-wrapper-10 .tgl:after,
    .checkbox-wrapper-10 .tgl:before,
    .checkbox-wrapper-10 .tgl *,
    .checkbox-wrapper-10 .tgl *:after,
    .checkbox-wrapper-10 .tgl *:before,
    .checkbox-wrapper-10 .tgl+.tgl-btn {
        box-sizing: border-box;
    }

    .checkbox-wrapper-10 .tgl::-moz-selection,
    .checkbox-wrapper-10 .tgl:after::-moz-selection,
    .checkbox-wrapper-10 .tgl:before::-moz-selection,
    .checkbox-wrapper-10 .tgl *::-moz-selection,
    .checkbox-wrapper-10 .tgl *:after::-moz-selection,
    .checkbox-wrapper-10 .tgl *:before::-moz-selection,
    .checkbox-wrapper-10 .tgl+.tgl-btn::-moz-selection,
    .checkbox-wrapper-10 .tgl::selection,
    .checkbox-wrapper-10 .tgl:after::selection,
    .checkbox-wrapper-10 .tgl:before::selection,
    .checkbox-wrapper-10 .tgl *::selection,
    .checkbox-wrapper-10 .tgl *:after::selection,
    .checkbox-wrapper-10 .tgl *:before::selection,
    .checkbox-wrapper-10 .tgl+.tgl-btn::selection {
        background: none;
    }

    .checkbox-wrapper-10 .tgl+.tgl-btn {
        outline: 0;
        display: block;
        width: 8em;
        height: 2em;
        position: relative;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .checkbox-wrapper-10 .tgl+.tgl-btn:after,
    .checkbox-wrapper-10 .tgl+.tgl-btn:before {
        position: relative;
        display: block;
        content: "";
        width: 50%;
        height: 100%;
    }

    .checkbox-wrapper-10 .tgl+.tgl-btn:after {
        left: 0;
    }

    .checkbox-wrapper-10 .tgl+.tgl-btn:before {
        display: none;
    }

    .checkbox-wrapper-10 .tgl:checked+.tgl-btn:after {
        left: 50%;
    }

    .checkbox-wrapper-10 .tgl-flip+.tgl-btn {
        padding: 2px;
        transition: all 0.2s ease;
        font-family: sans-serif;
        perspective: 100px;
    }

    .checkbox-wrapper-10 .tgl-flip+.tgl-btn:after,
    .checkbox-wrapper-10 .tgl-flip+.tgl-btn:before {
        display: inline-block;
        transition: all 0.4s ease;
        width: 100%;
        text-align: center;
        position: absolute;
        line-height: 2em;
        font-weight: bold;
        color: #fff;
        position: absolute;
        top: 0;
        left: 0;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        border-radius: 4px;
    }

    .checkbox-wrapper-10 .tgl-flip+.tgl-btn:after {
        content: attr(data-tg-on);
        background: #004727;
        transform: rotateY(-180deg);
    }

    .checkbox-wrapper-10 .tgl-flip+.tgl-btn:before {
        background: #be1d00;
        content: attr(data-tg-off);
    }


    .checkbox-wrapper-10 .tgl-flip+.tgl-btn:active:before {
        transform: rotateY(-20deg);
    }

    .checkbox-wrapper-10 .tgl-flip:checked+.tgl-btn:before {
        transform: rotateY(180deg);
    }

    .checkbox-wrapper-10 .tgl-flip:checked+.tgl-btn:after {
        transform: rotateY(0);
        left: 0;
        background: #004727;
    }

    .checkbox-wrapper-10 .tgl-flip:checked+.tgl-btn:active:after {
        transform: rotateY(20deg);
    }


    .container-checkbox {
        text-align: center;
        align-content: center;
    }

    div.valoracion {
        position: relative;
        overflow: hidden;
        display: inline-block;
    }

    div.valoracion input {
        position: absolute;
        top: -100px;
    }

    div.valoracion label {
        float: right;
        color: rgb(184, 184, 184);
        width: 53px;
        font-size: 55px;
        cursor: pointer;
    }

    div.valoracion label:hover,
    div.valoracion label:hover~label,
    div.valoracion input:checked~label {
        color: #002e66;
    }
</style>

<input type="hidden" id="code_lesson_index" value="{{ $lesson->code }}">
<div class="row">
    @if ($lesson->type == 'pdf')
        @foreach ($lesson->getFiles as $file)
            <div class="file col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                <embed src="{{ config('app.AWS_BUCKET_URL') . $file->url }}" type="application/pdf" width="100%"
                    height="500px" toolbar="0" />
            </div>
        @endforeach
    @endif
    <hr>
    <div class="description-lesson col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="row">
            <div class="image-lesson col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                <img src="{{ config('app.AWS_BUCKET_URL') . $lesson->image }}" alt="Imagen de la lección"
                    width="100%">
                <button
                    class="btn btn-application col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 btn-sm button-qualification">Calificar</button>
                <div class="rating" data-rating="{{ $promedio }}">
                    <div class="stars">
                        <img alt=""
                            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTcwIiBoZWlnaHQ9IjMyIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA0NC45NzkxNjYgOC40NjY2NjY2IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNjZmNmY2YiIHN0cm9rZS13aWR0aD0iLjIzOTc0NiI+PHBhdGggZD0ibTQuMjMzMjcwMyAwLjQyMzMzMzAxYy0wLjI1ODA2NzggMC0wLjUxNjA4NTUgMC4xNDkwNTI2LTAuNjEwMTUzIDAuNDQ2ODI2NmwtMC41MDMzMDYxIDEuNTkyNjcwNmMtMC4wNzM0OTEzIDAuMjMyNjI1Ny0wLjI4OTQzNDcgMC4zOTExNDktMC41MzM3NjYzIDAuMzkxMTQ5aC0xLjUyMTYzNDFjLTAuNjE5NzA0MDQgMC0wLjg3NzUyMjU0IDAuNzkxMzM1OC0wLjM3NjMwODA0IDEuMTU1MjAwNGwxLjM1NTI3MTUgMC45ODM5NTUzYzAuMjA0OTgzNCAwLjE0ODgxMzIgMC4yODQwNTc5IDAuNDE2NTIwNCAwLjE5MzA3MzcgMC42NTI2OTU3bC0wLjU4NzY1ODUgMS41MjU3NjI4Yy0wLjIyODgyNTQgMC41OTQwMDA5IDAuNDU3ODMyMSAxLjEyMDYwMjYgMC45NzMzMzk5IDAuNzQ2MjcxbDEuMjgxNjk1OS0wLjkzMTA4NTFjMC4xOTYzMTQ5LTAuMTQyNTU1NSAwLjQ2MjU3NTktMC4xNDI1NTU1IDAuNjU4ODkwOCAwbDEuMjgxNjk1OSAwLjkzMTA4NTFjMC41MTU1MDY5IDAuMzc0MzMxNiAxLjIwMjE3MzUtMC4xNTIyNzAxIDAuOTczMzM5LTAuNzQ2MjcxbC0wLjU4NzY1ODYtMS41MjU3NjI4Yy0wLjA5MTAxMTQtMC4yMzYxNzUzLTAuMDExNDQ0NC0wLjUwMzg4MjUgMC4xOTM1NDM1LTAuNjUyNjk1N2wxLjM1NDgwMTctMC45ODM5NTUzYzAuNTAxMjI0OS0wLjM2Mzg2NDYgMC4yNDMzOTY2LTEuMTU1MjAwNC0wLjM3NjMwNzItMS4xNTUyMDA0aC0xLjUyMTYzNGMtMC4yNDQzMDUyIDAtMC40NjAzMTIzLTAuMTU4NTIzMy0wLjUzMzc2NzEtMC4zOTExNDlsLTAuNTAzMzA2MS0xLjU5MjY3MDZjLTAuMDk0MDgxMi0wLjI5Nzc3NC0wLjM1MjA4NTMtMC40NDY4MjY2LTAuNjEwMTUyOS0wLjQ0NjgyNjZ6Ii8+PHBhdGggZD0ibTQwLjUzMTk4NCAwLjQyMzMzMzA4Yy0wLjI1ODA2OCAwLTAuNTE2MDg2IDAuMTQ5MDUyNi0wLjYxMDE1MyAwLjQ0NjgyNjZsLTAuNTAzMzA2IDEuNTkyNjcwNmMtMC4wNzM0OSAwLjIzMjYyNTctMC4yODk0MzUgMC4zOTExNDktMC41MzM3NjcgMC4zOTExNDloLTEuNTIxNjMzYy0wLjYxOTcwNCAwLTAuODc3NTIzIDAuNzkxMzM1OC0wLjM3NjMwOCAxLjE1NTIwMDRsMS4zNTUyNzEgMC45ODM5NTU2YzAuMjA0OTgzIDAuMTQ4ODEzMiAwLjI4NDA1OCAwLjQxNjUyIDAuMTkzMDczIDAuNjUyNjk1M2wtMC41ODc2NTggMS41MjU3NjI4Yy0wLjIyODgyNSAwLjU5NDAwMDkgMC40NTc4MzMgMS4xMjA2MDI2IDAuOTczMzQgMC43NDYyNzFsMS4yODE2OTYtMC45MzEwODUxYzAuMTk2MzE1LTAuMTQyNTU1NSAwLjQ2MjU3Ni0wLjE0MjU1NTUgMC42NTg4OTEgMGwxLjI4MTY5NiAwLjkzMTA4NTFjMC41MTU1MDcgMC4zNzQzMzE2IDEuMjAyMTc0LTAuMTUyMjcwMSAwLjk3MzMzOS0wLjc0NjI3MWwtMC41ODc2NTktMS41MjU3NjI4Yy0wLjA5MTAxMS0wLjIzNjE3NTMtMC4wMTE0NDgtMC41MDM4ODIxIDAuMTkzNTQzLTAuNjUyNjk1M2wxLjM1NDgwMi0wLjk4Mzk1NTZjMC41MDEyMjUtMC4zNjM4NjQ2IDAuMjQzMzk3LTEuMTU1MjAwNC0wLjM3NjMwNy0xLjE1NTIwMDRoLTEuNTIxNjM0Yy0wLjI0NDMwNSAwLTAuNDYwMzEyLTAuMTU4NTIzMy0wLjUzMzc2Ny0wLjM5MTE0OWwtMC41MDMzMDYtMS41OTI2NzA2Yy0wLjA5NDA4LTAuMjk3Nzc0LTAuMzUyMDg2LTAuNDQ2ODI2Ni0wLjYxMDE1My0wLjQ0NjgyNjZ6Ii8+PHBhdGggZD0ibTIyLjM4MjYyOCAwLjQyMzMzMzUzYy0wLjI1ODA2OCAwLTAuNTE2MDg2IDAuMTQ5MDUyNi0wLjYxMDE1MyAwLjQ0NjgyNjZsLTAuNTAzMzA2IDEuNTkyNjcwNmMtMC4wNzM0OSAwLjIzMjYyNTctMC4yODk0MzUgMC4zOTExNDktMC41MzM3NjcgMC4zOTExNDloLTEuNTIxNjMzYy0wLjYxOTcwNCAwLTAuODc3NTI0IDAuNzkxMzM1OC0wLjM3NjMwOCAxLjE1NTIwMDRsMS4zNTUyNzEgMC45ODM5NTQ3YzAuMjA0OTgzIDAuMTQ4ODEzMiAwLjI4NDA1OCAwLjQxNjUyIDAuMTkzMDczIDAuNjUyNjk1M2wtMC41ODc2NTggMS41MjU3NjI4Yy0wLjIyODgyNSAwLjU5NDAwMDkgMC40NTc4MzIgMS4xMjA2MDI2IDAuOTczMzQgMC43NDYyNzFsMS4yODE2OTYtMC45MzEwODUxYzAuMTk2MzE0LTAuMTQyNTU1NSAwLjQ2MjU3Ni0wLjE0MjU1NTUgMC42NTg4OTEgMGwxLjI4MTY5NSAwLjkzMTA4NTFjMC41MTU1MDggMC4zNzQzMzE2IDEuMjAyMTc0LTAuMTUyMjcwMSAwLjk3MzM0LTAuNzQ2MjcxbC0wLjU4NzY1OS0xLjUyNTc2MjhjLTAuMDkxMDEyLTAuMjM2MTc1My0wLjAxMTQ0OC0wLjUwMzg4MjEgMC4xOTM1NDMtMC42NTI2OTUzbDEuMzU0ODAxLTAuOTgzOTU0N2MwLjUwMTIyNS0wLjM2Mzg2NDYgMC4yNDMzOTgtMS4xNTUyMDA0LTAuMzc2MzA3LTEuMTU1MjAwNGgtMS41MjE2MzRjLTAuMjQ0MzA1IDAtMC40NjAzMTItMC4xNTg1MjMzLTAuNTMzNzY3LTAuMzkxMTQ5bC0wLjUwMzMwNi0xLjU5MjY3MDZjLTAuMDk0MDgtMC4yOTc3NzQtMC4zNTIwODUtMC40NDY4MjY2LTAuNjEwMTUyLTAuNDQ2ODI2NnoiLz48cGF0aCBkPSJtMzEuNDU3MzA2IDAuNDIzMzMzMDhjLTAuMjU4MDY4IDAtMC41MTYwODYgMC4xNDkwNTI2LTAuNjEwMTUzIDAuNDQ2ODI2NmwtMC41MDMzMDYgMS41OTI2NzA2Yy0wLjA3MzQ5IDAuMjMyNjI1Ny0wLjI4OTQzNSAwLjM5MTE0OS0wLjUzMzc2NyAwLjM5MTE0OWgtMS41MjE2MzNjLTAuNjE5NzA0IDAtMC44Nzc1MjQgMC43OTEzMzU4LTAuMzc2MzA4IDEuMTU1MjAwNGwxLjM1NTI3MSAwLjk4Mzk1NTZjMC4yMDQ5ODMgMC4xNDg4MTMyIDAuMjg0MDU4IDAuNDE2NTIgMC4xOTMwNzMgMC42NTI2OTUzbC0wLjU4NzY1OCAxLjUyNTc2MjhjLTAuMjI4ODI1IDAuNTk0MDAwOSAwLjQ1NzgzMyAxLjEyMDYwMjYgMC45NzMzNCAwLjc0NjI3MWwxLjI4MTY5Ni0wLjkzMTA4NTFjMC4xOTYzMTUtMC4xNDI1NTU1IDAuNDYyNTc2LTAuMTQyNTU1NSAwLjY1ODg5IDBsMS4yODE2OTcgMC45MzEwODUxYzAuNTE1NTA3IDAuMzc0MzMxNiAxLjIwMjE3My0wLjE1MjI3MDEgMC45NzMzMzktMC43NDYyNzFsLTAuNTg3NjU5LTEuNTI1NzYyOGMtMC4wOTEwMTEtMC4yMzYxNzUzLTAuMDExNDQ4LTAuNTAzODgyMSAwLjE5MzU0My0wLjY1MjY5NTNsMS4zNTQ4MDItMC45ODM5NTU2YzAuNTAxMjI1LTAuMzYzODY0NiAwLjI0MzM5Ny0xLjE1NTIwMDQtMC4zNzYzMDYtMS4xNTUyMDA0aC0xLjUyMTYzM2MtMC4yNDQzMDYgMC0wLjQ2MDMxNC0wLjE1ODUyMzMtMC41MzM3NjgtMC4zOTExNDlsLTAuNTAzMzA3LTEuNTkyNjcwNmMtMC4wOTQwOC0wLjI5Nzc3NC0wLjM1MjA4Ni0wLjQ0NjgyNjYtMC42MTAxNTMtMC40NDY4MjY2eiIvPjxwYXRoIGQ9Im0xMy4zMDc5NDkgMC40MjMzMzM1M2MtMC4yNTgwNjggMC0wLjUxNjA4NiAwLjE0OTA1MjYtMC42MTAxNTMgMC40NDY4MjY2bC0wLjUwMzMwNiAxLjU5MjY3MDZjLTAuMDczNDkgMC4yMzI2MjU3LTAuMjg5NDM1IDAuMzkxMTQ5LTAuNTMzNzY3IDAuMzkxMTQ5aC0xLjUyMTYzM2MtMC42MTk3MDQxIDAtMC44Nzc1MjM1IDAuNzkxMzM1OC0wLjM3NjMwODEgMS4xNTUyMDA0bDEuMzU1MjcxMSAwLjk4Mzk1NDdjMC4yMDQ5ODMgMC4xNDg4MTMyIDAuMjg0MDU4IDAuNDE2NTIgMC4xOTMwNzMgMC42NTI2OTUzbC0wLjU4NzY1OCAxLjUyNTc2MjhjLTAuMjI4ODI1IDAuNTk0MDAwOSAwLjQ1NzgzMyAxLjEyMDYwMjYgMC45NzMzNCAwLjc0NjI3MWwxLjI4MTY5Ni0wLjkzMTA4NTFjMC4xOTYzMTUtMC4xNDI1NTU1IDAuNDYyNTc2LTAuMTQyNTU1NSAwLjY1ODg5IDBsMS4yODE2OTcgMC45MzEwODUxYzAuNTE1NTA3IDAuMzc0MzMxNiAxLjIwMjE3My0wLjE1MjI3MDEgMC45NzMzMzktMC43NDYyNzFsLTAuNTg3NjU5LTEuNTI1NzYyOGMtMC4wOTEwMTEtMC4yMzYxNzUzLTAuMDExNDQ4LTAuNTAzODgyMSAwLjE5MzU0My0wLjY1MjY5NTNsMS4zNTQ4MDItMC45ODM5NTQ3YzAuNTAxMjI1LTAuMzYzODY0NiAwLjI0MzM5Ny0xLjE1NTIwMDQtMC4zNzYzMDctMS4xNTUyMDA0aC0xLjUyMTYzNGMtMC4yNDQzMDUgMC0wLjQ2MDMxMi0wLjE1ODUyMzMtMC41MzM3NjctMC4zOTExNDlsLTAuNTAzMzA2LTEuNTkyNjcwNmMtMC4wOTQwOC0wLjI5Nzc3NC0wLjM1MjA4Ni0wLjQ0NjgyNjYtMC42MTAxNTMtMC40NDY4MjY2eiIvPjwvZz48L3N2Zz4K" />
                        <img class="filled" alt=""
                            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTcwIiBoZWlnaHQ9IjMyIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA0NC45NzkxNjYgOC40NjY2NjY2IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNmZmIwMDAiIHN0cm9rZS13aWR0aD0iLjIzOTc0NiI+PHBhdGggZD0ibTQuMjMzMjcwMyAwLjQyMzMzMzAyYy0wLjI1ODA2NzggMC0wLjUxNjA4NTUgMC4xNDkwNTI2LTAuNjEwMTUzIDAuNDQ2ODI2NmwtMC41MDMzMDYxIDEuNTkyNjcwN2MtMC4wNzM0OTEzIDAuMjMyNjI1Ny0wLjI4OTQzNDcgMC4zOTExNDktMC41MzM3NjYzIDAuMzkxMTQ5aC0xLjUyMTYzNDFjLTAuNjE5NzA0MDMgMC0wLjg3NzUyMjUzIDAuNzkxMzM1OC0wLjM3NjMwODAzIDEuMTU1MjAwNGwxLjM1NTI3MTUgMC45ODM5NTUyYzAuMjA0OTgzNCAwLjE0ODgxMzIgMC4yODQwNTc5IDAuNDE2NTIwNSAwLjE5MzA3MzcgMC42NTI2OTU4bC0wLjU4NzY1ODUgMS41MjU3NjI4Yy0wLjIyODgyNTQgMC41OTQwMDA5IDAuNDU3ODMyMSAxLjEyMDYwMjYgMC45NzMzMzk5IDAuNzQ2MjcxbDEuMjgxNjk1OS0wLjkzMTA4NTFjMC4xOTYzMTQ5LTAuMTQyNTU1NSAwLjQ2MjU3NTktMC4xNDI1NTU1IDAuNjU4ODkwOCAwbDEuMjgxNjk1OSAwLjkzMTA4NTFjMC41MTU1MDY5IDAuMzc0MzMxNiAxLjIwMjE3MzUtMC4xNTIyNzAxIDAuOTczMzM5LTAuNzQ2MjcxbC0wLjU4NzY1ODYtMS41MjU3NjI4Yy0wLjA5MTAxMTQtMC4yMzYxNzUzLTAuMDExNDQ0NC0wLjUwMzg4MjYgMC4xOTM1NDM1LTAuNjUyNjk1OGwxLjM1NDgwMTctMC45ODM5NTUyYzAuNTAxMjI0OS0wLjM2Mzg2NDYgMC4yNDMzOTY2LTEuMTU1MjAwNC0wLjM3NjMwNzItMS4xNTUyMDA0aC0xLjUyMTYzNGMtMC4yNDQzMDUyIDAtMC40NjAzMTIzLTAuMTU4NTIzMy0wLjUzMzc2NzEtMC4zOTExNDlsLTAuNTAzMzA2MS0xLjU5MjY3MDdjLTAuMDk0MDgxMi0wLjI5Nzc3NC0wLjM1MjA4NTMtMC40NDY4MjY2LTAuNjEwMTUyOS0wLjQ0NjgyNjZ6Ii8+PHBhdGggZD0ibTQwLjUzMTk4NCAwLjQyMzMzMzA3Yy0wLjI1ODA2OCAwLTAuNTE2MDg2IDAuMTQ5MDUyNi0wLjYxMDE1MyAwLjQ0NjgyNjZsLTAuNTAzMzA2IDEuNTkyNjcwNmMtMC4wNzM0OSAwLjIzMjYyNTctMC4yODk0MzUgMC4zOTExNDktMC41MzM3NjcgMC4zOTExNDloLTEuNTIxNjMzYy0wLjYxOTcwNCAwLTAuODc3NTIzIDAuNzkxMzM1OC0wLjM3NjMwOCAxLjE1NTIwMDRsMS4zNTUyNzEgMC45ODM5NTU2YzAuMjA0OTgzIDAuMTQ4ODEzMiAwLjI4NDA1OCAwLjQxNjUyIDAuMTkzMDczIDAuNjUyNjk1M2wtMC41ODc2NTggMS41MjU3NjI4Yy0wLjIyODgyNSAwLjU5NDAwMDkgMC40NTc4MzMgMS4xMjA2MDI2IDAuOTczMzQgMC43NDYyNzFsMS4yODE2OTYtMC45MzEwODUxYzAuMTk2MzE1LTAuMTQyNTU1NSAwLjQ2MjU3Ni0wLjE0MjU1NTUgMC42NTg4OTEgMGwxLjI4MTY5NiAwLjkzMTA4NTFjMC41MTU1MDcgMC4zNzQzMzE2IDEuMjAyMTc0LTAuMTUyMjcwMSAwLjk3MzMzOS0wLjc0NjI3MWwtMC41ODc2NTktMS41MjU3NjI4Yy0wLjA5MTAxMS0wLjIzNjE3NTMtMC4wMTE0NDgtMC41MDM4ODIxIDAuMTkzNTQzLTAuNjUyNjk1M2wxLjM1NDgwMi0wLjk4Mzk1NTZjMC41MDEyMjUtMC4zNjM4NjQ2IDAuMjQzMzk3LTEuMTU1MjAwNC0wLjM3NjMwNy0xLjE1NTIwMDRoLTEuNTIxNjM0Yy0wLjI0NDMwNSAwLTAuNDYwMzEyLTAuMTU4NTIzMy0wLjUzMzc2Ny0wLjM5MTE0OWwtMC41MDMzMDYtMS41OTI2NzA2Yy0wLjA5NDA4LTAuMjk3Nzc0LTAuMzUyMDg2LTAuNDQ2ODI2Ni0wLjYxMDE1My0wLjQ0NjgyNjZ6Ii8+PHBhdGggZD0ibTIyLjM4MjYyOCAwLjQyMzMzMzUzYy0wLjI1ODA2OCAwLTAuNTE2MDg2IDAuMTQ5MDUyNi0wLjYxMDE1MyAwLjQ0NjgyNjZsLTAuNTAzMzA2IDEuNTkyNjcwNmMtMC4wNzM0OSAwLjIzMjYyNTctMC4yODk0MzUgMC4zOTExNDktMC41MzM3NjcgMC4zOTExNDloLTEuNTIxNjMzYy0wLjYxOTcwNCAwLTAuODc3NTI0IDAuNzkxMzM1OC0wLjM3NjMwOCAxLjE1NTIwMDRsMS4zNTUyNzEgMC45ODM5NTQ3YzAuMjA0OTgzIDAuMTQ4ODEzMiAwLjI4NDA1OCAwLjQxNjUyIDAuMTkzMDczIDAuNjUyNjk1M2wtMC41ODc2NTggMS41MjU3NjI4Yy0wLjIyODgyNSAwLjU5NDAwMDkgMC40NTc4MzIgMS4xMjA2MDI2IDAuOTczMzQgMC43NDYyNzFsMS4yODE2OTYtMC45MzEwODUxYzAuMTk2MzE0LTAuMTQyNTU1NSAwLjQ2MjU3Ni0wLjE0MjU1NTUgMC42NTg4OTEgMGwxLjI4MTY5NSAwLjkzMTA4NTFjMC41MTU1MDggMC4zNzQzMzE2IDEuMjAyMTc0LTAuMTUyMjcwMSAwLjk3MzM0LTAuNzQ2MjcxbC0wLjU4NzY1OS0xLjUyNTc2MjhjLTAuMDkxMDEyLTAuMjM2MTc1My0wLjAxMTQ0OC0wLjUwMzg4MjEgMC4xOTM1NDMtMC42NTI2OTUzbDEuMzU0ODAxLTAuOTgzOTU0N2MwLjUwMTIyNS0wLjM2Mzg2NDYgMC4yNDMzOTgtMS4xNTUyMDA0LTAuMzc2MzA3LTEuMTU1MjAwNGgtMS41MjE2MzRjLTAuMjQ0MzA1IDAtMC40NjAzMTItMC4xNTg1MjMzLTAuNTMzNzY3LTAuMzkxMTQ5bC0wLjUwMzMwNi0xLjU5MjY3MDZjLTAuMDk0MDgtMC4yOTc3NzQtMC4zNTIwODUtMC40NDY4MjY2LTAuNjEwMTUyLTAuNDQ2ODI2NnoiLz48cGF0aCBkPSJtMzEuNDU3MzA2IDAuNDIzMzMzMDhjLTAuMjU4MDY4IDAtMC41MTYwODYgMC4xNDkwNTI2LTAuNjEwMTUzIDAuNDQ2ODI2NmwtMC41MDMzMDYgMS41OTI2NzA2Yy0wLjA3MzQ5IDAuMjMyNjI1Ny0wLjI4OTQzNSAwLjM5MTE0OS0wLjUzMzc2NyAwLjM5MTE0OWgtMS41MjE2MzNjLTAuNjE5NzA0IDAtMC44Nzc1MjQgMC43OTEzMzU4LTAuMzc2MzA4IDEuMTU1MjAwNGwxLjM1NTI3MSAwLjk4Mzk1NTZjMC4yMDQ5ODMgMC4xNDg4MTMyIDAuMjg0MDU4IDAuNDE2NTIgMC4xOTMwNzMgMC42NTI2OTUzbC0wLjU4NzY1OCAxLjUyNTc2MjhjLTAuMjI4ODI1IDAuNTk0MDAwOSAwLjQ1NzgzMyAxLjEyMDYwMjYgMC45NzMzNCAwLjc0NjI3MWwxLjI4MTY5Ni0wLjkzMTA4NTFjMC4xOTYzMTUtMC4xNDI1NTU1IDAuNDYyNTc2LTAuMTQyNTU1NSAwLjY1ODg5IDBsMS4yODE2OTcgMC45MzEwODUxYzAuNTE1NTA3IDAuMzc0MzMxNiAxLjIwMjE3My0wLjE1MjI3MDEgMC45NzMzMzktMC43NDYyNzFsLTAuNTg3NjU5LTEuNTI1NzYyOGMtMC4wOTEwMTEtMC4yMzYxNzUzLTAuMDExNDQ4LTAuNTAzODgyMSAwLjE5MzU0My0wLjY1MjY5NTNsMS4zNTQ4MDItMC45ODM5NTU2YzAuNTAxMjI1LTAuMzYzODY0NiAwLjI0MzM5Ny0xLjE1NTIwMDQtMC4zNzYzMDYtMS4xNTUyMDA0aC0xLjUyMTYzM2MtMC4yNDQzMDYgMC0wLjQ2MDMxNC0wLjE1ODUyMzMtMC41MzM3NjgtMC4zOTExNDlsLTAuNTAzMzA3LTEuNTkyNjcwNmMtMC4wOTQwOC0wLjI5Nzc3NC0wLjM1MjA4Ni0wLjQ0NjgyNjYtMC42MTAxNTMtMC40NDY4MjY2eiIvPjxwYXRoIGQ9Im0xMy4zMDc5NDkgMC40MjMzMzM1MmMtMC4yNTgwNjggMC0wLjUxNjA4NiAwLjE0OTA1MjYtMC42MTAxNTMgMC40NDY4MjY2bC0wLjUwMzMwNiAxLjU5MjY3MDZjLTAuMDczNDkgMC4yMzI2MjU3LTAuMjg5NDM1IDAuMzkxMTQ5LTAuNTMzNzY3IDAuMzkxMTQ5aC0xLjUyMTYzM2MtMC42MTk3MDQxIDAtMC44Nzc1MjM1IDAuNzkxMzM1OC0wLjM3NjMwODEgMS4xNTUyMDA0bDEuMzU1MjcxMSAwLjk4Mzk1NDdjMC4yMDQ5ODMgMC4xNDg4MTMyIDAuMjg0MDU4IDAuNDE2NTIgMC4xOTMwNzMgMC42NTI2OTUzbC0wLjU4NzY1OCAxLjUyNTc2MjhjLTAuMjI4ODI1IDAuNTk0MDAwOSAwLjQ1NzgzMyAxLjEyMDYwMjYgMC45NzMzNCAwLjc0NjI3MWwxLjI4MTY5Ni0wLjkzMTA4NTFjMC4xOTYzMTUtMC4xNDI1NTU1IDAuNDYyNTc2LTAuMTQyNTU1NSAwLjY1ODg5IDBsMS4yODE2OTcgMC45MzEwODUxYzAuNTE1NTA3IDAuMzc0MzMxNiAxLjIwMjE3My0wLjE1MjI3MDEgMC45NzMzMzktMC43NDYyNzFsLTAuNTg3NjU5LTEuNTI1NzYyOGMtMC4wOTEwMTEtMC4yMzYxNzUzLTAuMDExNDQ4LTAuNTAzODgyMSAwLjE5MzU0My0wLjY1MjY5NTNsMS4zNTQ4MDItMC45ODM5NTQ3YzAuNTAxMjI1LTAuMzYzODY0NiAwLjI0MzM5Ny0xLjE1NTIwMDQtMC4zNzYzMDctMS4xNTUyMDA0aC0xLjUyMTYzNGMtMC4yNDQzMDUgMC0wLjQ2MDMxMi0wLjE1ODUyMzMtMC41MzM3NjctMC4zOTExNDlsLTAuNTAzMzA2LTEuNTkyNjcwNmMtMC4wOTQwOC0wLjI5Nzc3NC0wLjM1MjA4Ni0wLjQ0NjgyNjYtMC42MTAxNTMtMC40NDY4MjY2eiIvPjwvZz48L3N2Zz4K" />
                    </div>
                </div>
            </div>
            <div class="title-lesson col-12 col-sm-9 col-md-9 col-lg-9 col-xl-9">
                <div class="row">
                    <div class="container-h4 col-12 col-sm-10 col-md-10 col-lg-10 col-xl-10">
                        <h4>
                            {{ $lesson->title }}
                        </h4>
                    </div>
                    <div class="container-checkbox col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                        <div class="checkbox-wrapper-10">
                            <input class="tgl tgl-flip  @if (!$lesson->getUserQualification(auth()->user()->id)) lesson-finished @endif"
                                id="cb5" type="checkbox"
                                @if ($lesson->getUserQualification(auth()->user()->id)) checked data-option="true" @else data-option="false" @endif
                                data-code="{{ $lesson->code }}" />

                            <label class="tgl-btn" data-tg-off="Incompleta" data-tg-on="Completada!"
                                for="cb5"></label>
                        </div>
                    </div>
                </div>
                <p style="text-align: justify; margin-top:1em;">
                    {{ $lesson->summary }}
                </p>
            </div>
        </div>
    </div>
</div>

<script>
    document.querySelectorAll(".rating").forEach((x) => {
        const rating = Number(x.dataset.rating)
        const percentage = Math.floor(((5 - rating) / 5) * 100)

        // Apply clip-path
        const filled = x.children[0].children[1]
        filled.style.clipPath = `inset(0 ${percentage}% 0 0)`

        // Update label
        const label = x.children[1]
        //label.innerText = `${rating}/5`
    })


    $(document).on("change", ".lesson-finished", function() {
        var option = $(this).attr("data-option");
        var code = $(this).attr("data-code");

        var title = "";
        var html = "";

        if (option == true) {
            title = "¿Esta seguro que desea reiniciar la lección?"
            title = "No modificará su progreso actual."
        } else {
            title = "¿Esta seguro que desea terminar la lección?"
            title = "Esta acción se registrará en su progreso actual en el curso."
        }

        Swal.fire({
            icon: "question",
            title: title,
            html: html,
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonColor: "#003f48",
            confirmButtonText: "Si, estoy seguro",
            denyButtonText: `No, cancelar`,
            allowOutsideClick: false,
            allowEscapeKey: false,
            focusConfirm: true,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    type: 'GET',
                    url: "/subscriber/course/lesson/selected/" + code + "/finished",
                    success: function(data) {
                        if (data == true) {
                            $(".checkbox-wrapper-10 .tgl-flip:checked+.tgl-btn").addClass(
                                "checked-styles");
                            $("#lesson-svg-" + code).html(
                                '<svg width="26px" height="26px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">' +
                                '<g id="SVGRepo_bgCarrier" stroke-width="0"></g>' +
                                '<g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>' +
                                '<g id="SVGRepo_iconCarrier">' +
                                '<path fill-rule="evenodd" clip-rule="evenodd" d="M6 3C4.34315 3 3 4.34315 3 6V18C3 19.6569 4.34315 21 6 21H18C19.6569 21 21 19.6569 21 18V6C21 4.34315 19.6569 3 18 3H6ZM17.8 8.6C18.1314 8.15817 18.0418 7.53137 17.6 7.2C17.1582 6.86863 16.5314 6.95817 16.2 7.4L10.8918 14.4776L8.70711 12.2929C8.31658 11.9024 7.68342 11.9024 7.29289 12.2929C6.90237 12.6834 6.90237 13.3166 7.29289 13.7071L10.2929 16.7071C10.4979 16.9121 10.7817 17.018 11.0709 16.9975C11.3601 16.9769 11.6261 16.8319 11.8 16.6L17.8 8.6Z" fill="#002e66"></path>' +
                                '</g>' +
                                '</svg>'
                            );

                        }
                    },
                    error: function(err) {
                        errorHttp(err.status);
                    },
                });
            } else if (result.isDenied) {
                $('#cb5').prop('checked', false);
            }
        });

        return false;
    });
</script>
