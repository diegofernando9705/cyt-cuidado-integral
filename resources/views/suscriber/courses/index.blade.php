@extends('suscriber.layouts.contentLayoutMaster')

@section('title_nav', 'Cursos virtuales')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset('page/assets/css/style_panel_de_control.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.css">
    <link href="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.css" rel="stylesheet" />

@endsection
@section('page-style')
    <link href="{{ asset('page/assets/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">

    <style>
        .header-section {
            padding-top: 15px;
            color: white !important;
            border-radius: 5px;
            background-color: #002e66 !important;
        }

        .header-section h1 {
            color: white;
            padding-bottom: 1% !important;
            padding-top: 1% !important;
        }

        .header-section ul {
            margin-top: 20px;
        }

        .header-section ul li {
            list-style: none;
            display: inline;
            padding-right: 20px;
            padding-left: 20px;
            padding-bottom: 7px;
        }

        .active_li {
            border-bottom: 8px solid white;
        }

        .filter-search {
            margin-bottom: 15px;
            margin-top: 15px;
        }

        .card-img-top {
            height: 200px;
            background-size: cover;
        }

        .card-title {
            font-weight: bold;
        }

        .image-hover {
            background-color: rgba(0, 0, 0, 0.308);
            height: 200px;
            width: 100%;
            position: absolute;
            display: none;

            display: flex;
            justify-content: center;
            /* Centrar horizontalmente */
            align-items: center;
            /* Centrar verticalmente */
        }

        .course-div:hover {
            cursor: pointer;
        }

        .list-group-item {
            font-size: 12px;
        }

    </style>
@endsection


@section('content')
    <!-- Dashboard Analytics Start -->
    <section id="dashboard-analytics" class="col-lg-12 col-md-12 col-sm-12 col-12 col-xl-12">
        <div class="row">
            <div class="header-section col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h1>
                    Cursos virtuales
                </h1>
                <ul>
                    <li class="active_li">Todos</li>
                    {{-- <li>Mis cursos</li>
                    <li>Pŕoximos cursos</li> --}}
                </ul>
            </div>
            <div class="container-section col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="container">
                    <div class="row">
                        <div class="filter-search col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="row">
                                <div class="form-group col-12 col-sm-3 col-md-3 col-xl-3 col-lg-3">
                                    <label for=""><b>Ordenar por: </b></label>
                                    <select class="form-control" disabled>
                                        <option value="new_courses" selected>Agregados recientementes</option>
                                        <option value="title_a_z">Titulo: de la A a la Z</option>
                                        <option value="title_z_a">Titulo: de la Z a la A</option>
                                    </select>
                                </div>
                                <div class="form-group col-12 col-sm-3 col-md-3 col-xl-3 col-lg-3">
                                    <label for=""><b>Categorias: </b></label>
                                    <select class="form-control" disabled>
                                        <option value="new_courses" selected>Agregados recientementes</option>
                                    
                                    </select>
                                </div>
                                <div class="form-group col-12 col-sm-3 col-md-3 col-xl-3 col-lg-3">
                                    <label for=""><b>Instructor: </b></label>
                                    <select class="form-control" disabled>
                                     
                                    </select>
                                </div>
                                <div class="form-group col-12 col-sm-3 col-md-3 col-xl-3 col-lg-3">
                                    <label for=""><b>Búsqueda: </b></label>
                                    <input type="text" class="form-control" placeholder="Buscar cursos..." disabled>
                                </div>
                            </div>
                        </div>
                        @foreach ($courses as $course)
                            <div class="course-div col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3"
                                data-id="{{ $course->code }}">
                                <div class="card">
                                    <div class="image-hover">
                                        <svg width="64px" height="64px" viewBox="0 0 24 24" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                            <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round">
                                            </g>
                                            <g id="SVGRepo_iconCarrier">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22ZM10.6935 15.8458L15.4137 13.059C16.1954 12.5974 16.1954 11.4026 15.4137 10.941L10.6935 8.15419C9.93371 7.70561 9 8.28947 9 9.21316V14.7868C9 15.7105 9.93371 16.2944 10.6935 15.8458Z"
                                                    fill="#ffffff"></path>
                                            </g>
                                        </svg>
                                    </div>
                                    <img class="card-img-top"
                                        style="background-image: url('{{ config('app.AWS_BUCKET_URL') . $course->cover_image }}');">
                                    <div class="card-body">
                                        <h5 class="card-title">
                                            {{ Illuminate\Support\Str::limit($course->title, 25, '...') }} </h5>
                                        <p class="card-text">
                                            {{ Illuminate\Support\Str::limit($course->summary, 100, '...') }} </p>
                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">Prof.
                                            {{ $course->getInstructor->first_name . ' ' . $course->getInstructor->first_last_name }}
                                            | {{ $course->getLevel->title }}</li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Dashboard Analytics end -->
    @include('administrator.template.modals')
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.js"></script>
    <script src="{{ asset(mix('js/scripts/subscriber/courses/index.js')) }}"></script>
@endsection
