@extends('suscriber.layouts.contentLayoutMaster')

@section('title_nav', 'Panel Principal')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset('page/assets/css/style_panel_de_control.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.css">

@endsection
@section('page-style')
    <link href="{{ asset('page/assets/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
@endsection

<style>
    .todo_alta {
        border-radius: 4px;
        border-top: 1px solid rgba(255, 0, 0, 0.199);
        border-bottom: 1px solid rgb(255, 0, 0.199);
        border-left: 4px solid rgb(255, 0, 0);
        border-right: 1px solid rgb(255, 0, 0.199);
        background-color: rgba(255, 0, 0, 0.199);
    }

    .todo_media {
        border-radius: 4px;
        border-top: 1px solid rgb(255, 255, 0);
        border-bottom: 1px solid rgb(255, 255, 0);
        border-left: 4px solid rgb(255, 255, 0);
        border-right: 1px solid rgb(255, 255, 0);
        background-color: rgba(255, 255, 0, 0.199);
    }

    .todo_baja {
        border-radius: 4px;
        border-top: 1px solid rgb(0, 128, 0);
        border-bottom: 1px solid rgb(0, 128, 0);
        border-left: 4px solid rgb(0, 128, 0);
        border-right: 1px solid rgb(5, 129, 5);
        background-color: rgba(0, 128, 0, 0.199);
    }

    .table-task-home>thead>tr>th {
        background-color: #002e66 !important;
        color: white;
    }

    .table-task-home>tbody>tr>td {
        font-size: 13px;
        color: black;
        padding-left: 10px;
        padding-right: 10px;
    }

    .titulo_slider_new {
        margin-top: 10px;
        margin-bottom: 10px;
        color: black;
        font-size: 14px;
        font-weight: bold;
    }

    .descripcion_slider_new {
        text-align: justify;
        font-size: 12px;
        line-height: 12px;
    }

    .contenedor-new-most {
        margin-bottom: 8px;
        padding-bottom: 8px;
        border-bottom: 1px solid #002e66;
    }

    .image-new {
        padding: 0px;
    }

    .title-new {
        padding: 0px;
    }
</style>

@section('content')
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal-title">Información</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-content">
                </div>
            </div>
        </div>
    </div>

    <!-- Dashboard Analytics Start -->
    <section id="dashboard-analytics" class="col-lg-12 col-md-12 col-sm-12 col-12 col-xl-12">
        {{-- =========================== CONTENEDOR INDICADORES =========================== --}}
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <a style="text-decoration: none; color:black;">
                    <div class="card card-user-timeline">
                        <div class="indicadores-slider owl-carousel">

                            @if (count($array_indicators['TRM']) >= 1)
                                <div class="single-box-indicador text-center">
                                    <div class="row">
                                        <div class="texto col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                            <div class="valor_indicador">
                                                $ {{ $indicador_trm[0]->valor_indicador }}
                                                <br>
                                                <small>
                                                    {{ $indicador_trm[0]->fecha_indicador_fin }}
                                                </small>
                                                </td>
                                            </div>
                                        </div>
                                        <div class="info-area-indicador col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                            <div class="texto_indicador">
                                                Dólar TRM <br>
                                                @if ($indicador_trm[0]->valor_indicador > $indicador_trm[1]->valor_indicador)
                                                    <small class="positive">
                                                        +
                                                        ${{ $indicador_trm[0]->valor_indicador - $indicador_trm[1]->valor_indicador }}
                                                    </small>
                                                @else
                                                    <small class="negative">
                                                        -
                                                        ${{ $indicador_trm[0]->valor_indicador - $indicador_trm[1]->valor_indicador }}
                                                    </small>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if (count($array_indicators['IPC']) >= 1)
                                <div class="single-box-indicador text-center">
                                    <div class="row">
                                        <div class="texto col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                            <div class="valor_indicador">
                                                $ {{ $indicador_ipc[0]->porcentaje }}
                                                <br>
                                                <small>
                                                    {{ $indicador_ipc[0]->fecha_indicador_fin }}
                                                </small>
                                                </td>
                                            </div>
                                        </div>
                                        <div class="info-area-indicador col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                            <div class="texto_indicador">
                                                IPC <br>
                                                @if ($indicador_ipc[0]->porcentaje > $indicador_ipc[1]->porcentaje)
                                                    <small class="positive">
                                                        +
                                                        {{ $indicador_ipc[0]->porcentaje - $indicador_ipc[1]->porcentaje }}
                                                        %
                                                    </small>
                                                @else
                                                    <small class="negative">
                                                        -
                                                        {{ $indicador_ipc[0]->porcentaje - $indicador_ipc[1]->porcentaje }}
                                                        %
                                                    </small>
                                                @endif

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            @endif

                            @if (count($array_indicators['UVR']) >= 1)
                                <div class="single-box-indicador text-center">
                                    <div class="row">
                                        <div class="texto col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                            <div class="valor_indicador">
                                                $
                                                {{ Illuminate\Support\Str::limit($indicador_uvr[0]->valor_indicador, 7, '') }}
                                                <br>
                                                <small>
                                                    {{ $indicador_uvr[0]->fecha_indicador_inicio }}
                                                </small>
                                                </td>
                                            </div>
                                        </div>
                                        <div class="info-area-indicador col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                            <div class="texto_indicador">
                                                UVR <br>
                                                @if ($indicador_uvr[0]->valor_indicador > $indicador_uvr[1]->valor_indicador)
                                                    <small class="positive">
                                                        +
                                                        {{ Illuminate\Support\Str::limit($indicador_uvr[0]->valor_indicador - $indicador_uvr[1]->valor_indicador, 7, '') }}
                                                        %
                                                    </small>
                                                @else
                                                    <small class="negative">
                                                        @php
                                                            $operacion =
                                                                $indicador_uvr[0]->valor_indicador -
                                                                $indicador_uvr[1]->valor_indicador;
                                                        @endphp
                                                        {{ Illuminate\Support\Str::limit($operacion, 7, '') }} %
                                                    </small>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <div class="single-box-indicador text-center">
                                <div class="row">
                                    <div class="texto col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                        <div class="valor_indicador">
                                            $ ---
                                            <br>
                                            <small>
                                                ---
                                            </small>
                                            </td>
                                        </div>
                                    </div>
                                    <div class="info-area-indicador col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                        <div class="texto_indicador">
                                            PIB <br>
                                            ---
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if (count($array_indicators['UVR']) >= 1)
                                <div class="single-box-indicador text-center">
                                    <div class="row">
                                        <div class="texto col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                            <div class="valor_indicador">
                                                $ {{ $indicador_dtf[0]->porcentaje }}
                                                <br>
                                                <small>
                                                    {{ $indicador_dtf[0]->fecha_indicador_fin }}
                                                </small>
                                                </td>
                                            </div>
                                        </div>
                                        <div class="info-area-indicador col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                            <div class="texto_indicador">
                                                DTF <br>
                                                @if ($indicador_dtf[0]->porcentaje > $indicador_dtf[1]->porcentaje)
                                                    <small class="positive">
                                                        +
                                                        {{ $indicador_dtf[0]->porcentaje - $indicador_dtf[1]->porcentaje }}
                                                        %
                                                    </small>
                                                @else
                                                    <small class="negative">
                                                        -
                                                        {{ $indicador_dtf[0]->valor_indicador - $indicador_dtf[1]->valor_indicador }}
                                                        %
                                                    </small>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </a>
            </div>
        </div>

        {{-- =========================== CONTENEDOR INFORMACION DE USUARIOS =========================== --}}
        <div class="row">
            <div class="informacion_usuario col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                <p>
                    {{ $jornada }}, <br>
                    <b>
                        Dr(a). {{ auth()->user()->name }},
                    </b>
                    <br>
                    Tienes {{ count($data_profesional->getUnusedPoints) }} litipuntos
                </p>
            </div>
            <div class="informacion_fecha col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                <p>
                    <br>
                    {{ $fecha_hoy }}
                </p>
            </div>
        </div>

        {{-- =========================== CONTENEDOR QUE QUIERES HACER HOY =========================== --}}
        <div class="row">
            <div class=" col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                <a style="text-decoration: none; color:black;">
                    <div class="card card-user-timeline">
                        <div class="titulo_que_quieres_hoy col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <p>
                                ¿Qué quieres hacer hoy?
                            </p>
                        </div>
                        <div class="opciones_que_quieres_hoy col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="hacer_hoy-slider owl-carousel">
                                <div class="single-box-hacer_hoy text-center">
                                    <img src="https://s3.amazonaws.com/cnp.com.co/Publico/medallas.gif" alt=""
                                        class="rounded-circle">
                                    <b>Redimir litipuntos</b>
                                </div>

                                <div class="single-box-hacer_hoy text-center">
                                    <img src="https://s3.amazonaws.com/cnp.com.co/Publico/clase-en-linea.gif
                                    "
                                        alt="" class="rounded-circle" width="50%">
                                    <b>Ver mis cursos</b>
                                </div>

                                <div class="single-box-hacer_hoy text-center">
                                    <img src="https://s3.amazonaws.com/cnp.com.co/Publico/editar.gif" alt=""
                                        class="rounded-circle" width="50%">
                                    <b>Cotizar dictamen</b>
                                </div>

                                <div class="single-box-hacer_hoy text-center">
                                    <img src="https://s3.amazonaws.com/cnp.com.co/Publico/asesor.gif" alt=""
                                        class="rounded-circle" width="50%">
                                    <b>Hablar con un asesor</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="informacion_membresia col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                <div id="circle_membresia"></div>
                @foreach ($data_profesional->getSubscription as $subscription)
                    @foreach ($subscription->getPlan->memberships as $membership)
                        <img src="https://s3.amazonaws.com/cnp.com.co/{{ $membership->image }}">
                    @endforeach
                @endforeach
            </div>
        </div>

        {{-- =========================== CONTENEDOR BANNER VIVE UNA EXPERIENCIA =========================== --}}
        <div class="row">
            <div class="texto_banner col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <form action="{{ route('form-subscription') }}" class="card card-user-timeline" method="POST">
                    @csrf
                    @method('POST')
                    <input type="hidden" name="codigo_membresia" id="codigo_membresia" value="CNP_MEMBERSHIP_MAGISTER"
                        required>
                    <input type="submit" style="border:0px; background-color:white;"
                        title="¡Quiero mi membresia Magister!" class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
                        value="Vive una experiencia extraordinaria en el litigio sin límites suscribiéndote a nuestra membresía magíster.">
                </form>
            </div>
        </div>

        {{-- =========================== CONTENEDOR TUS DICTAMENES =========================== --}}
        <div class="row">
            <div class="contenedor_mis_dictamenes col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="titulo_general_dictamen">
                    <p>
                        Tus dictámenes <a href="">Ver todos</a>
                    </p>
                </div>
                <div class="contenedor_slider_dictamenes col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="dictamenes-slider owl-carousel">
                        @for ($i = 0; $i < 10; $i++)
                            <div class="single-box-dictamen text-center">
                                <div class="row">
                                    <div class="img-area-dictamen col-12 col-md-5 col-sm-5 col-lg-5 col-xl-5">
                                        <img alt="" class="move-animation"
                                            src="https://images.pexels.com/photos/1097456/pexels-photo-1097456.jpeg">
                                    </div>
                                    <div class="info-area-dictamen col-12 col-md-7 col-sm-7 col-lg-7 col-xl-7">
                                        <div class="row">

                                            <div
                                                class="titulo_slider_dictamen  col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                Dictamen Bancolombia
                                            </div>
                                            <div
                                                class="descripcion_slider_servicio col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                Dictamen Bancolombia
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>

        {{-- =========================== CONTENEDOR TAREAS =========================== --}}
        <div class="row">
            <div class="contenedor_tareas col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card card-user-timeline">
                    <div class="titulo_tareas col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <p>
                            Tareas
                        </p>
                    </div>
                    <div class="input_buscador col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="row">

                            <div class="input-with-icon col-8 col-md-3 col-sm-3 col-lg-3 col-xl-3">
                                <i data-feather="search"></i>
                                <input type="text" class="form-control" placeholder="Buscar" id="input-task-search"
                                    title="Desactivado temporalmente">
                            </div>
                            <div class="contenedor_icono_agregar col-6 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                <div class="icono_agregar">
                                    <i data-feather="plus"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="contenedores_todo col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="contenedores_todo">
                        <div class="row">
                            <table class="table table-bordered table-hover table-striped table-task-home">
                                <thead>
                                    <tr>
                                        <th>
                                            CÓDIGO
                                        </th>
                                        <th>
                                            TITULO
                                        </th>
                                        <th>
                                            FECHA VENCIMIENTO
                                        </th>
                                        <th>
                                            PRIORIDAD
                                        </th>
                                        <th>
                                            ESTADO ACTUAL
                                        </th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="table-task-tbody">
                                    @include('suscriber.template.task.list')
                                </tbody>
                            </table>

                        </div>
                        <div class="contenedor_boton_more col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <p>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- =========================== CONTENEDOR FAVORITOS =========================== --}}
        <div class="row">
            <div class="contenedor_favoritos col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="titulo_favoritos col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <p>
                        Tus favoritos <a href="">Volver a leer</a>
                    </p>
                </div>
                <div class="body_favoritos col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="row">
                        <div class="left_favoritos col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                            <div class="contenedor_slider_dictamenes col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="favoritos-slider owl-carousel">
                                    @foreach ($favorites as $favorite_data)
                                        @foreach ($favorite_data->getNews as $favorite)
                                            <a href="{{ url('news') . '/' . $favorite->url }}" target="_blank"
                                                title="Leer {{ $favorite->title }}">
                                                <div class="single-box-dictamen text-center">
                                                    <div class="row">
                                                        <div
                                                            class="img-area-new col-12 col-md-5 col-sm-5 col-lg-5 col-xl-5">
                                                            <img alt="" class="move-animation"
                                                                src="{{ config('app.AWS_BUCKET_URL') . $favorite->image }}">
                                                        </div>
                                                        <div
                                                            class="info-area-dictamen col-12 col-md-7 col-sm-7 col-lg-7 col-xl-7">
                                                            <div class="row">
                                                                <div
                                                                    class="titulo_slider_new  col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                                    {{ $favorite->title }}
                                                                </div>
                                                                <div
                                                                    class="descripcion_slider_new col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                                    {{ $favorite->summary }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        @endforeach
                                    @endforeach
                                </div>
                            </div>
                            <div class="banners_imagenes col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
                                style="padding: 0px;">
                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        @php
                                            $i = 1;
                                        @endphp
                                        BANNER PUBLICIDAD
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                        data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                        data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="right_favoritos col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <div class="card card-user-timeline">
                                <div class="listado_lo_mas_leido col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <p class="titulo_lo_mas_leido">
                                        Las 3 más leídas...
                                    </p>
                                    <div class="informacion_listado col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
                                        style="padding: 0px;">
                                        @foreach ($mostViewedNews as $mostNew)
                                            <div
                                                class="contenedor-new-most col-12 col-sm-12 col-md-12 col-lg-12 col-sl-12">
                                                <a href="{{ $mostNew->url }}">
                                                    <div class="row">
                                                        <div class="image-new col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                                            <img src="{{ config('app.AWS_BUCKET_URL') . $mostNew->image }}"
                                                                alt="" width="100%">
                                                        </div>
                                                        <div class="title-new col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                                            <b>{{ $mostNew->title }}</b>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach

                                        <p style="text-align: right;">
                                            <a href="{{ route('news') }}" target="_blank">
                                                <b>Ver más...</b>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- Dashboard Analytics end -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="https://cdn.ckeditor.com/4.20.1/basic/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js"></script>
    <script src="{{ asset('page/assets/lib/owlcarousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset(mix('js/scripts/subscriber/home/index.js')) }}"></script>
@endsection
