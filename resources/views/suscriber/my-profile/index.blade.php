@extends('suscriber.layouts.contentLayoutMaster')

@section('title_nav', 'Mi perfil')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset('page/assets/css/style_panel_de_control.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.css">
    <link href="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.css" rel="stylesheet" />

@endsection
@section('page-style')
    <link href="{{ asset('page/assets/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <style>
        .icono_usuario {
            text-align: center;
            align-content: center;
        }

        .icono_usuario img {
            width: 50%;
        }

        .botones_opciones {
            margin-top: 10px;
            text-align: center;
            margin-bottom: 10px;
        }

        .btn-validated {
            margin-bottom: 16px;
        }
    </style>
@endsection


@section('content')
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal-title">Información</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-content">
                </div>
            </div>
        </div>
    </div>

    <!-- Dashboard Analytics Start -->
    <section id="dashboard-analytics" class="col-lg-12 col-md-12 col-sm-12 col-12 col-xl-12">
        <div class="row">
            <div class="contenedor_foto_usuario col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                <a style="text-decoration: none; color:black;">
                    <div class="card card-user-timeline">
                        <div class="icono_usuario col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="icono_usuario">
                            @if ($profile->getUser->avatar == 'vacio' or $profile->getUser->avatar == null)
                                <img src="https://s3.amazonaws.com/cnp.com.co/icons_user.jpg" alt="Imagen del usuario"
                                    title="AVatar" class="rounded-circle">
                            @else
                                <img src="{{ config('app.AWS_BUCKET_URL') . $profile->getUser->avatar }}"
                                    alt="Imagen del usuario" title="Avatar" class="rounded-circle">
                            @endif
                        </div>
                        <div class="botones_opciones col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <input type="file" class="btn btn-primary btn-sm" id="image" value="Cambiar imagen">
                        </div>
                    </div>
                    <div class="card card-user-timeline">
                        <div class="card-header">
                            <div class="titulo_seccion_usuario col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <p>
                                    Información del usuario <button
                                        class="btn btn-sm btn-primary habilitar_campos_form_usuario"
                                        title="Editar información"><i data-feather="edit"></i></button> <button
                                        class="btn btn-sm btn-danger inhabilitar_campos_form_usuario"
                                        id="inhabilitar_campos_form_usuario" title="Cancelar actualización" disabled><i
                                            data-feather="x-circle"></i></button>
                                </p>
                            </div>
                            <div class="contenedor_formulario col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <form class="formulario_usuario" novalidate>
                                    @csrf
                                    @method('POST')

                                    <div class="row">
                                        <div class="form-group col">
                                            <label for="name"> <b>(*) Nombre de usuario</b></label>
                                            <input type="text" class="form-control input_form_usuario"
                                                value="{{ $profile->getUser->name }}" id="name" name="name" disabled
                                                required>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col">
                                            <label for=""> <b>(*) Correo acceso</b></label>
                                            <input type="email" class="form-control input_form_usuario"
                                                value="{{ $profile->getUser->email }}" id="correo_usuario"
                                                name="correo_usuario" disabled required>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col">
                                            <label for=""> <b>(*) Clave de acceso</b></label>
                                            <input type="password" class="form-control input_form_usuario" value=""
                                                id="password" name="password" disabled>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col">
                                            <label for="password_confirmation"> <b>(*) Repite clave de acceso</b></label>
                                            <input type="password" class="form-control input_form_usuario" value=""
                                                id="password_confirmation" name="password_confirmation" disabled>
                                        </div>
                                    </div>

                                    <center>
                                        <button type="submit" class="btn btn-primary btn-sm"
                                            id="actualizar_informacion_button_usuario" disabled>Actualizar
                                            información</button>
                                    </center>
                                </form>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="informacion_contacto col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                @if ($profile->getUser->email_verified_at == null)
                    <button type="button"
                        class="btn btn-danger btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 btn-validated">Por
                        favor verifique su correo electrónico para
                        acceder a más funciones </button>
                @endif
                <a style="text-decoration: none; color:black;">
                    <div class="card card-user-timeline">
                        <div class="card-header">
                            <div class="titulo_seccion col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <p>
                                    Información del profesional <button
                                        class="btn btn-sm btn-primary habilitar_campos_form_professional"
                                        title="Editar información"><i data-feather="edit"></i></button> <button
                                        class="btn btn-sm btn-danger inhabilitar_campos_form_professional"
                                        id="inhabilitar_campos_form_professional" title="Cancelar actualización"
                                        disabled><i data-feather="x-circle"></i></button>
                                </p>
                            </div>
                            <div class="contenedor_formulario col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <form class="formulario_profesional" novalidate>
                                    @csrf
                                    @method('POST')
                                    <div class="row">

                                        <div class="form-group col">
                                            <label for=""> <b> Tarjeta profesional</b></label>
                                            <input type="text" class="form-control"
                                                value="{{ $profile->professional_card }}" disabled>
                                        </div>
                                        <div class="form-group col">
                                            <label for=""> <b> Número de identificación</b></label>
                                            <input type="text" class="form-control" id="id"
                                                value="{{ $profile->id }}" disabled>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col">
                                            <label for=""> <b>(*) Primer nombre</b></label>
                                            <input type="text" class="form-control input_form"
                                                value="{{ $profile->first_name }}" id="first_name" name="first_name"
                                                required disabled>
                                        </div>
                                        <div class="form-group col">
                                            <label for=""> <b> Segundo nombre</b></label>
                                            <input type="text" class="form-control input_form"
                                                value="{{ $profile->second_name }}" id="second_name" name="second_name"
                                                disabled>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col">
                                            <label for=""> <b>(*) Primer apellido</b></label>
                                            <input type="text" class="form-control input_form"
                                                value="{{ $profile->first_last_name }}" id="first_last_name"
                                                name="first_last_name" required disabled>
                                        </div>
                                        <div class="form-group col">
                                            <label for=""> <b> Segundo apellido</b></label>
                                            <input type="text" class="form-control input_form"
                                                value="{{ $profile->second_last_name }}" id="second_last_name"
                                                name="second_last_name" disabled>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col">
                                            <label for=""> <b>(*) Celular</b></label>
                                            <input type="text" class="form-control input_form"
                                                value="{{ $profile->cellphone }}" id="cellphone" name="cellphone"
                                                required disabled>
                                        </div>
                                        <div class="form-group col">
                                            <label for=""> <b> (*) Correo electrónico</b></label>
                                            <input type="email" class="form-control input_form"
                                                value="{{ $profile->email }}" id="email" name="email" required
                                                disabled>
                                        </div>
                                    </div>
                                    <center>
                                        <button type="submit"
                                            class="btn btn-primary btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
                                            id="actualizar_informacion_button_professional" disabled>Actualizar
                                            información</button>
                                    </center>
                                </form>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>
    <!-- Dashboard Analytics end -->

    <!-- Modal -->
    <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel"
        aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="imageModalLabel">Subir y Recortar Imagen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-mody">
                    <div id="editor"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="cropImage">Recortar y enviar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.js"></script>
    <script src="{{ asset(mix('js/scripts/subscriber/profile/index.js')) }}"></script>
@endsection
