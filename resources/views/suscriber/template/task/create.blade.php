<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">

<form class="create-task" novalidate>
    @method('POST')
    @csrf

    <div class="row">
        <div class="form-group col col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
            <label for=""><b>(*) Fecha de inicio</b></label>
            <input type="date" class="form-control" id="start_task" name="start_task" required
                min="{{ date('Y-m-d') }}">
            <div class="invalid-feedback">Por favor, este campo es importante.</div>
        </div>
        <div class="form-group col col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
            <label for=""><b>Fecha de entrega</b></label>
            <input type="date" class="form-control" id="end_task" name="end_task">
            <div class="invalid-feedback">Por favor, este campo es importante.</div>
        </div>
        <div class="form-group col col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
            <label for=""><b>(*) Prioridad</b> </label>
            <select class="form-control" name="priority" id="priority" required>
                <option selected disabled></option>
                @foreach ($priorities as $priority)
                    <option value="{{ $priority->id }}">{{ $priority->description }}</option>
                @endforeach
            </select>
            <div class="invalid-feedback">Por favor, este campo es importante.</div>
        </div>
        <div class="form-group col col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <label for="referred"><b>(*) Asignar a</b> </label>
            <select class="form-control selector" name="referred" id="referred" multiple required>
                <option value="for_my" selected>A mi</option>
                @foreach ($referrers as $referrer)
                @endforeach
            </select>
            <div class="invalid-feedback">Por favor, este campo es importante.</div>
        </div>
        <div class="form-group col col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <label for="status"><b>(*) Estado</b> </label>
            <select class="form-control" name="status" id="status" required>
                <option value="" selected disabled></option>
                @foreach ($status as $state)
                    <option value="{{ $state->id }}">{{ $state->description }}</option>
                @endforeach
            </select>
            <div class="invalid-feedback">Por favor, este campo es importante.</div>
        </div>
        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <label for=""><b>(*) Titulo</b> </label>
            <input type="text" class="form-control" id="title" name="title" required>
            <div class="invalid-feedback">Por favor, este campo es importante.</div>
        </div>
        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <label for=""><b>(*) Descripción</b> </label>
            <textarea class="form-control" name="description" id="description" required></textarea>
            <div class="invalid-feedback">Por favor, este campo es importante.</div>
        </div>
        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <label for=""><b>Archivos</b> </label>
            <input type="file" class="form-control" id="files" name="files" multiple>
        </div>
        <button type="submit"
            class="btn btn-primary btn-application btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
            id="submit">Registrar</button>
    </div>
</form>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/subscriber/home/task.js')) }}"></script>
<script>
    $('.selector').select2();
</script>
