@if (!$tasks)
    <tr>
        <td colspan="5" style="text-align: center; color:black;">
            Sin tareas registradas
        </td>
    </tr>
@endif

@foreach ($tasks as $task)
    <tr>
        <td>
            {{ $task->getTask->code }}
        </td>
        <td>
            {{ $task->getTask->title }}
        </td>
        <td>
            {{ $task->getTask->end_date }}
        </td>
        <td>
            {{ $task->getTask->getPriority->description }}
        </td>
        <td>
            {{ $task->getTask->getStatus->description }}
        </td>
        <td style="text-align: center; align-content:center;">
            <button type="button" class="btn btn-success btn-sm edit-task" title="Actualizar"
                data-id="{{ $task->getTask->code }}">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                    class="feather feather-edit-3">
                    <path d="M12 20h9"></path>
                    <path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path>
                </svg>
            </button>
        </td>
    </tr>
@endforeach

<div class="paginado col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="text-align: center; align-content:center;">
    <hr>
    {{ $tasks->links() }}
</div>
