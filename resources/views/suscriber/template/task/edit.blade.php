<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">

<form class="update-task" novalidate>
    @method('POST')
    @csrf

    <input type="hidden" id="code" value="{{ $task->code }}">
    <div class="row">
        <div class="form-group col col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
            <label for=""><b>(*) Fecha de inicio</b></label>
            <input type="datetime-local" class="form-control" id="start_task" name="start_task" required
                value="{{ $task->start_task }}">
            <div class="invalid-feedback">Por favor, este campo es importante.</div>
        </div>

        <div class="form-group col col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
            <label for=""><b>Fecha de entrega</b></label>
            <input type="datetime-local" class="form-control" id="end_task" name="end_task"
                value="{{ $task->end_date }}">
            <div class="invalid-feedback">Por favor, este campo es importante.</div>
        </div>

        <div class="form-group col col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
            <label for=""><b>(*) Prioridad</b> </label>
            <select class="form-control" name="priority" id="priority" required>
                <option selected disabled></option>
                @foreach ($priorities as $priority)
                    @if ($task->priority == $priority->id)
                        <option value="{{ $priority->id }}" selected>{{ $priority->description }}</option>
                    @else
                        <option value="{{ $priority->id }}">{{ $priority->description }}</option>
                    @endif
                @endforeach
            </select>
            <div class="invalid-feedback">Por favor, este campo es importante.</div>
        </div>

        <div class="form-group col col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <label for="referred"><b>(*) Asignar a</b> </label>
            <select class="form-control selector" name="referred" id="referred" multiple required>
                <option value="for_my" selected>A mi</option>
                @foreach ($referrers as $referrer)
                    @if (is_array($array_assigness) && in_array($referrer->id . '-access', $permisos_asignados))
                        <input type="checkbox" class="custom-control-input" id="{{ $referrer->id }}-access"
                            value="{{ $referrer->id }}-access" name="permisos[]" checked="" disabled />
                        <label class="custom-control-label" for="{{ $referrer->id }}-access"></label>
                    @else
                        <input type="checkbox" class="custom-control-input" id="{{ $referrer->id }}-access"
                            value="{{ $referrer->id }}-access"name="permisos[]" disabled />
                        <label class="custom-control-label" for="{{ $referrer->id }}-access"></label>
                    @endif
                @endforeach
            </select>
            <div class="invalid-feedback">Por favor, este campo es importante.</div>
        </div>

        <div class="form-group col col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <label for="status"><b>(*) Estado</b> </label>
            <select class="form-control" name="status" id="status" required>
                <option value="" selected disabled></option>
                @foreach ($status as $state)
                    @if ($task->status == $state->id)
                        <option value="{{ $state->id }}" selected>{{ $state->description }}</option>
                    @else
                        <option value="{{ $state->id }}">{{ $state->description }}</option>
                    @endif
                @endforeach
            </select>
            <div class="invalid-feedback">Por favor, este campo es importante.</div>
        </div>

        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <label for=""><b>(*) Titulo</b> </label>
            <input type="text" class="form-control" id="title" name="title" required
                value="{{ $task->title }}">
            <div class="invalid-feedback">Por favor, este campo es importante.</div>
        </div>

        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <label for=""><b>(*) Descripción</b> </label>
            <textarea class="form-control" name="description" id="description" required>{{ $task->description }}</textarea>
            <div class="invalid-feedback">Por favor, este campo es importante.</div>
        </div>

        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <label for=""><b>Archivos</b> </label>
            <div class="data-files">
                <ul style="list-style: none;">
                    @foreach ($task->getFiles as $file)
                        <li id="file-{{ $file->id }}">
                            <a href="{{ config('app.AWS_BUCKET_URL') . $file->url }}" target="_blank">
                                <button type="button" class="btn btn-primary btn-sm"
                                    style="margin-bottom: 6px !important;">{{ $file->title }}</button>
                            </a>
                            <button type="button" class="btn btn-danger btn-sm delete-file"
                                data-id="{{ $file->id }}" title="Eliminar">X</button>
                        </li>
                    @endforeach
                </ul>
            </div>

            <input type="file" class="form-control" id="files" name="files">
        </div>
        <button type="submit"
            class="btn btn-primary btn-application btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
            id="submit">Actualizar</button>
    </div>
</form>

<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/subscriber/home/task.js')) }}"></script>
<script>
    $('.selector').select2();
</script>
