@extends('page-layouts.template.app')
@section('title', 'Información no encontrada')

@section('navegacion')
    <div class="container-xxl py-5 bg-primary hero-header">
    </div>
@endsection

@section('container')
    <!-- Service Start -->
    <div class="container-xxl py-5">
        <div class="container px-lg-5">
            <div class="wow fadeInUp" data-wow-delay="0.1s">
                <h1 class="text-center mb-5">PROFESIONAL NO ENCONTRADO
                    <hr>
                </h1>
            </div>
            <div class="row g-4 fadeInUp" data-wow-delay="0.1s">
               <h3>Hola {{ auth()->user()->name }}. usted no tiene información de contacto. Por favor comuniquese con el administrador.</h3>
            </div>
        </div>
    </div>
@endsection