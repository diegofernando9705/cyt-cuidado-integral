
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset('page/assets/js/errorPlatfotm.js') }}"></script>

<!-- BEGIN: Footer-->
<footer class="footer {{ $configData['footerType'] === 'footer-hidden' ? 'd-none' : '' }} footer-light">
    <p class="clearfix mb-0">
        <span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2023<a class="ml-25"
                href="https://softworldcolombia.com/" target="_blank">Softworld Colombia</a>
            <span class="d-none d-sm-inline-block">, Todos los derechos reservados</span>
        </span>
        <span class="float-md-right d-none d-md-block">CNP - Centro Nacional de Pruebas<i data-feather="heart"></i></span>
    </p>
</footer>
<button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
<!-- END: Footer-->
