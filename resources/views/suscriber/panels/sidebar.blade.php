@php
    $configData = Helper::applClasses();
@endphp


<style>
    .hamburger-menu {
        width: 100px;
        cursor: pointer;
        height: 100%;
        top: -13px !important;
        position: absolute;
        right: -57px;
    }

    .bar,
    .bar:after,
    .bar:before {
        width: 25px;
        height: 3px;
    }

    .bar {
        position: relative;
        transform: translateY(25px);
        background: white;
        transition: all 0ms 300ms;
    }

    .bar.animate {
        background: rgba(255, 255, 255, 0);
    }

    .bar:before {
        height: 3px;
        content: "";
        position: absolute;
        left: 0;
        bottom: 8px;
        background: white;
        transition: bottom 300ms 300ms cubic-bezier(0.23, 1, 0.32, 1), transform 300ms cubic-bezier(0.23, 1, 0.32, 1);
    }

    .bar:after {
        content: "";
        position: absolute;
        left: 0;
        height: 3px;
        top: 8px;
        background: white;
        transition: top 300ms 300ms cubic-bezier(0.23, 1, 0.32, 1), transform 300ms cubic-bezier(0.23, 1, 0.32, 1);
    }

    .bar.animate:after {
        top: 0;
        transform: rotate(45deg);
        transition: top 300ms cubic-bezier(0.23, 1, 0.32, 1), transform 300ms 300ms cubic-bezier(0.23, 1, 0.32, 1);
    }

    .bar.animate:before {
        bottom: 0;
        transform: rotate(-45deg);
        transition: bottom 300ms cubic-bezier(0.23, 1, 0.32, 1), transform 300ms 300ms cubic-bezier(0.23, 1, 0.32, 1);
    }
</style>

<div class="main-menu menu-fixed {{ $configData['theme'] === 'dark' ? 'menu-dark' : 'menu-light' }} menu-accordion menu-shadow"
    data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="{{ route('dashboard') }}">
                    <span class="brand-logo" style="margin-top: -5px; margin-left:5px; ">
                        <img src="https://s3.amazonaws.com/cnp.com.co/ICONS+CNP.png" width="120px" alt="">
                    </span>
                    <h2 class="brand-text">
                        <img src="https://s3.amazonaws.com/cnp.com.co/text-cnp.png" width="120px">
                    </h2>
                </a>
            </li>
            <li
                class="nav-item nav-toggle"style="z-index:999 !important; margin-left:8px; margin-right:2px; width:19px;">
                <div class="hamburger-menu nav-link modern-nav-toggle pr-0 " data-toggle="collapse">
                    <div class="bar animate"></div>
                </div>
            </li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation">

            <li class="nav-item  {{ Route::currentRouteName() === 'dashboard-subscriber' ? ' open' : '' }}">
                <a href="{{ route('dashboard-subscriber') }}" class="d-flex align-items-center">
                    <i data-feather="grid"></i>
                    <span class="menu-title text-truncate">Panel de control</span>
                </a>
            </li>

            <li class="nav-item  {{ Route::currentRouteName() === 'profile-subscriber' ? ' open' : '' }}">
                <a href="{{ route('profile-subscriber') }}" class="d-flex align-items-center">
                    <i data-feather="user"></i>
                    <span class="menu-title text-truncate">Mi perfil</span>
                </a>
            </li>

            <li class="nav-item {{ Route::currentRouteName() === 'courses-index' || Route::currentRouteName() === 'courses-progress' ? ' open' : '' }}">
                <a href="{{ route('courses-index') }}" class="d-flex align-items-center">
                    <i data-feather="book-open"></i>
                    <span class="menu-title text-truncate">Mis cursos</span>
                </a>
            </li>
            

            <li class="nav-item  {{ Route::currentRouteName() === 'calculators-index' ? ' open' : '' }}">
                <a href="{{ route('calculators-index') }}" class="d-flex align-items-center">
                    <i data-feather="percent"></i>
                    <span class="menu-title text-truncate">Calculadoras</span>
                </a>
            </li>

            <li class="nav-item  {{ Route::currentRouteName() === 'diaries-index' ? ' open' : '' }}">
                <a href="{{ route('diaries-index') }}" class="d-flex align-items-center">
                    <i data-feather="file-text"></i>
                    <span class="menu-title text-truncate">Diario</span>
                </a>
            </li>


            <li class="nav-item  {{ Route::currentRouteName() === 'litipoints-index' ? ' open' : '' }}">
                <a href="{{ route('litipoints-index') }}" class="d-flex align-items-center">
                    <i data-feather="briefcase"></i>
                    <span class="menu-title text-truncate">Mis litipuntos</span>
                </a>
            </li>

            <li class="nav-item  {{ Route::currentRouteName() === 'concepts-index' ? ' open' : '' }}">
                <a href="{{ route('concepts-index') }}" class="d-flex align-items-center">
                    <i data-feather="book"></i>
                    <span class="menu-title text-truncate">Conceptos</span>
                </a>
            </li>

            <li class="nav-item  {{ Route::currentRouteName() === '' ? ' open' : '' }}">
                <a href="{{ route('dashboard-subscriber') }}" class="d-flex align-items-center">
                    <i data-feather="headphones"></i>
                    <span class="menu-title text-truncate">Servicio al cliente</span>
                </a>
            </li>
            {{-- 
            <li class="nav-item  {{ Route::currentRouteName() === '' ? ' open' : '' }}">
                <a href="{{ route('dashboard-subscriber') }}" class="d-flex align-items-center">
                    <i data-feather="award"></i>
                    <span class="menu-title text-truncate">Mis casos</span>
                </a>
            </li>
 --}}
            {{-- Foreach menu item starts 
            @if (isset($menuData[0]))

                @foreach ($menuData[0]->menu as $menu_usuarios)
                    @if (isset($menu_usuarios->navheader))
                    @else
                        @php
                            $custom_classes = '';
                            if (isset($menu_usuarios->classlist)) {
                                $custom_classes = $menu_usuarios->classlist;
                            }
                        @endphp

                        <li
                            class="nav-item {{ Route::currentRouteName() === $menu_usuarios->slug ? 'active' : '' }} {{ $custom_classes }}">
                            <a href="{{ isset($menu_usuarios->url) ? url($menu_usuarios->url) : 'javascript:void(0)' }}"
                                class="d-flex align-items-center"
                                target="{{ isset($menu_usuarios->newTab) ? '_blank' : '_self' }}">
                                <i data-feather="{{ $menu_usuarios->icon }}"></i>
                                <span class="menu-title text-truncate">{{ $menu_usuarios->name }}</span>
                                @if (isset($menu_usuarios->badge))
                                    <?php $badgeClasses = 'badge badge-pill badge-light-primary ml-auto mr-1'; ?>
                                    <span
                                        class="{{ isset($menu_usuarios->badgeClass) ? $menu_usuarios->badgeClass : $badgeClasses }} ">{{ $menu_usuarios->badge }}</span>
                                @endif
                            </a>
                            @if (isset($menu_usuarios->submenu))
                                @include('suscriber.panels.submenu', [
                                    'menu' => $menu_usuarios->submenu,
                                ])
                            @endif
                        </li>
                    @endif
                @endforeach

            @endif
            {{-- Foreach menu item ends --}}
        </ul>
    </div>
</div>
<!-- END: Main Menu-->
