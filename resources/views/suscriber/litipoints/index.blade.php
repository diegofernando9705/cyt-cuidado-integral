@extends('suscriber.layouts.contentLayoutMaster')

@section('title_nav', 'Mis litipuntos')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset('page/assets/css/style_panel_de_control.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.css">
    <link href="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.css" rel="stylesheet" />

@endsection
@section('page-style')
    <link href="{{ asset('page/assets/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">

    <style>
        .header-section {
            padding-top: 15px;
            color: white !important;
            border-radius: 5px;
            background-color: #002e66 !important;
        }

        .header-section h1 {
            color: white;
            padding-bottom: 1% !important;
            padding-top: 1% !important;
        }

        .header-section ul {
            margin-top: 20px;
        }

        .header-section ul li {
            list-style: none;
            display: inline;
            padding-right: 20px;
            padding-left: 20px;
            padding-bottom: 7px;
        }

        .active_li {
            border-bottom: 8px solid white;
        }

        .filter-search {
            margin-bottom: 15px;
            margin-top: 15px;
        }

        .card-img-top {
            height: 200px;
            background-size: cover;
        }

        .card-title {
            font-weight: bold;
        }

        .image-hover {
            background-color: rgba(0, 0, 0, 0.308);
            height: 200px;
            width: 100%;
            position: absolute;
            display: none;

            display: flex;
            justify-content: center;
            /* Centrar horizontalmente */
            align-items: center;
            /* Centrar verticalmente */
        }

        .diary-div:hover {
            cursor: pointer;
        }

        .list-group-item {
            font-size: 12px;
        }
    </style>
@endsection


@section('content')
    <!-- Dashboard Analytics Start -->
    <section id="dashboard-analytics" class="col-lg-12 col-md-12 col-sm-12 col-12 col-xl-12">
        <div class="row">
            <div class="header-section col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h1>
                    Mis litipuntos en CNP
                </h1>
                <ul>
                    <li class="active_li">Todos</li>
                    {{-- <li>Según membresia</li>
                     <li>Pŕoximos cursos</li> --}}
                </ul>
            </div>
            <div class="container-section col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="container">
                    <div class="row">
                        <div class="filter-search col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="row">
                                <div class="form-group col-12 col-sm-3 col-md-3 col-xl-3 col-lg-3">
                                    <label for=""><b>Ordenar por: </b></label>
                                    <select class="form-control" disabled>
                                        <option value="new_diarys" selected>Agregados recientementes</option>
                                        <option value="title_a_z">Titulo: de la A a la Z</option>
                                        <option value="title_z_a">Titulo: de la Z a la A</option>
                                    </select>
                                </div>
                                <div class="form-group col-12 col-sm-3 col-md-3 col-xl-3 col-lg-3">
                                    <label for=""><b>Categorias: </b></label>
                                    <select class="form-control" disabled>
                                        <option value="new_diarys" selected>Agregados recientementes</option>

                                    </select>
                                </div>
                                <div class="form-group col-12 col-sm-3 col-md-3 col-xl-3 col-lg-3">
                                    <label for=""><b>Instructor: </b></label>
                                    <select class="form-control" disabled>

                                    </select>
                                </div>
                                <div class="form-group col-12 col-sm-3 col-md-3 col-xl-3 col-lg-3">
                                    <label for=""><b>Búsqueda: </b></label>
                                    <input type="text" class="form-control" placeholder="Buscar cursos..." disabled>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Dashboard Analytics end -->
    @include('administrator.template.modals')
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.js"></script>
    <script src="{{ asset(mix('js/scripts/subscriber/diaries/index.js')) }}"></script>
@endsection
