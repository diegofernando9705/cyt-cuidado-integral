@extends('suscriber.layouts.contentLayoutMaster')

@section('title_nav', 'Calculadoras virtuales')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset('page/assets/css/style_panel_de_control.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.css">
    <link href="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.css" rel="stylesheet" />

@endsection
@section('page-style')
    <link href="{{ asset('page/assets/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">

    <style>
        .header-section {
            padding-top: 15px;
            color: white !important;
            border-radius: 5px;
            background-color: #002e66 !important;
        }

        .header-section h1 {
            color: white;
            padding-bottom: 1% !important;
            padding-top: 1% !important;
            padding-left: 1em;
        }

        .header-section ul {
            margin-top: 20px;
        }

        .header-section ul li {
            list-style: none;
            display: inline;
            padding-right: 20px;
            padding-left: 20px;
            padding-bottom: 7px;
        }

        .active_li {
            border-bottom: 8px solid white;
        }

        .container-section {
            margin-top: 2em;
        }

        .filter-search {
            margin-bottom: 15px;
            margin-top: 15px;
        }

        .card-img-top {
            height: 280px;
            background-size: 100% 100%;
        }

        .card-title {
            text-align: center;
            color: #002e66;
            font-weight: bold;
        }

        .image-hover {
            background-color: rgba(0, 0, 0, 0.308);
            height: 280px;
            width: 100%;
            position: absolute;
            display: none;

            display: flex;
            justify-content: center;
            /* Centrar horizontalmente */
            align-items: center;
            /* Centrar verticalmente */
        }

        .calculator-div:hover {
            cursor: pointer;
        }

        .list-group-item {
            font-size: 12px;
        }
    </style>
@endsection


@section('content')
    <!-- Dashboard Analytics Start -->
    <section id="dashboard-analytics" class="col-lg-12 col-md-12 col-sm-12 col-12 col-xl-12">
        <div class="row">
            <div class="header-section col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h1 style="padding-left: 1em;">
                    Calculadoras virtuales
                </h1>
                <ul>
                    <li class="active_li">Habilitadas</li>
                    {{-- <li>Mis cursos</li>
                    <li>Pŕoximos cursos</li> --}}
                </ul>
            </div>
            <div class="container-section col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="container">
                    <div class="row">
                        @foreach ($calculators as $calculator)
                            <div class="calculator-div col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3"
                                data-id="{{ $calculator->getCalculator->code }}">
                                <div class="card">
                                    <div class="image-hover">
                                        <svg width="64px" height="64px" viewBox="0 0 20 20" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                            <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round">
                                            </g>
                                            <g id="SVGRepo_iconCarrier">
                                                <path
                                                    d="M10.7071 10.7071C10.3166 11.0976 9.68342 11.0976 9.29289 10.7071C8.90237 10.3166 8.90237 9.68342 9.29289 9.29289L15.2929 3.29289C15.6834 2.90237 16.3166 2.90237 16.7071 3.29289C17.0976 3.68342 17.0976 4.31658 16.7071 4.70711L10.7071 10.7071Z"
                                                    fill="#ffffff"></path>
                                                <path
                                                    d="M15 15V11.5C15 10.9477 15.4477 10.5 16 10.5C16.5523 10.5 17 10.9477 17 11.5V16C17 16.5523 16.5523 17 16 17H4C3.44772 17 3 16.5523 3 16V4C3 3.44772 3.44772 3 4 3H8.5C9.05228 3 9.5 3.44772 9.5 4C9.5 4.55228 9.05228 5 8.5 5H5V15H15Z"
                                                    fill="#ffffff"></path>
                                                <path
                                                    d="M17 8C17 8.55228 16.5523 9 16 9C15.4477 9 15 8.55228 15 8V4C15 3.44772 15.4477 3 16 3C16.5523 3 17 3.44772 17 4V8Z"
                                                    fill="#ffffff"></path>
                                                <path
                                                    d="M12 5C11.4477 5 11 4.55228 11 4C11 3.44772 11.4477 3 12 3H16C16.5523 3 17 3.44772 17 4C17 4.55228 16.5523 5 16 5H12Z"
                                                    fill="#ffffff"></path>
                                            </g>
                                        </svg>
                                    </div>
                                    <img class="card-img-top"
                                        style="background-image: url('{{ config('app.AWS_BUCKET_URL') . $calculator->getCalculator->image }}');">
                                    <div class="card-body">
                                        <h5 class="card-title">
                                            {{ Illuminate\Support\Str::limit($calculator->getCalculator->title, 25, '...') }}
                                        </h5>
                                        <p class="card-text">
                                            {{ Illuminate\Support\Str::limit($calculator->getCalculator->description, 100, '...') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Dashboard Analytics end -->
    @include('administrator.template.modals')
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.js"></script>
    <script src="{{ asset(mix('js/scripts/subscriber/calculators/index.js')) }}"></script>
@endsection
