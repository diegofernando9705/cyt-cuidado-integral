<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css" />
<style>
    .contenedor_formulario {
        text-align: left;
        font-size: 14px;
    }

    .contenedor_formulario .form-group {
        margin-bottom: 10px;
    }

    .separador {
        border-bottom: 3px solid rgba(128, 128, 128, 0.329);
        height: 10px;
        margin-bottom: 10px;
    }

    .table_title {
        background-color: #1f1b6b;
        color: white;
        text-align: center;
        padding-bottom: 0px;
    }

    .td_valor_reintegral {
        text-align: right;
    }

    .valores_td {
        text-align: right !important;
    }
</style>

<div class="descripcion">
    <p style="font-size: 13px;">Todos los cálculos realizados por usted, quedarán almacenados en la base de datos</p>
</div>

<form class="store_calculator" novalidate>
    @csrf
    @method('POST')
    <input type="hidden" value="calculadora_indexacion" id="code_calculator">

    <div class="form-group">
        <label for="capital"> <b> Valor reintegrar: </b></label>
        <input type="text" class="form-control" name="capital" id="capital">
        <div class="invalid-feedback">Por favor, este campo es importante.</div>
    </div>
    <div class="container">
        <div class="row" style="overflow: hidden;">
            <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <label for="fecha_actual"> <b> Fecha actual: </b></label>
                <input type="date" class="form-control" name="fecha_actual" id="fecha_actual" required>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
            <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <label for="fecha_inicial"> <b> Fecha Inicial: </b></label>
                <input type="date" class="form-control" name="fecha_inicial" id="fecha_inicial" required>
                <div class="invalid-feedback">Por favor, este campo es importante.</div>
            </div>
        </div>
    </div>



    <div class="separador"></div>

    <div class="row">
        <div class="contenedor_ipc col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <table class="table table-hovered table_title">
                <tr>
                    <th>
                        INDEXACIÓN IPC
                    </th>
                </tr>
            </table>

            <table class="table table-bordered ">
                <tr>
                    <th>
                        Valor a reintegrar
                    </th>
                    <td class="td_valor_reintegral">
                        0
                    </td>
                </tr>
                <tr>
                    <th>
                        IPC Actual
                    </th>
                    <td class="valores_td" id="ipc_actual">
                        0
                    </td>
                </tr>
                <tr>
                    <th>
                        IPC Inicial
                    </th>
                    <td class="valores_td" id="ipc_inicial">
                        0
                    </td>
                </tr>
                <tr class="alert alert-success">
                    <th>Valor indexado</th>
                    <td class="valores_td" id="ipc_indexado">
                        0
                    </td>
                </tr>
            </table>
        </div>

        <div class="contenedor_uvr col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <table class="table table-hovered table_title">
                <tr>
                    <th>
                        INDEXACIÓN UVR
                    </th>
                    <td class="valores_td"></td>
                </tr>
            </table>
            <table class="table table-bordered" style="margin-top:-15px;">
                <tr>
                    <th>
                        Valor a reintegrar
                    </th>
                    <td class="td_valor_reintegral" id="uvr_capital">
                        0
                    </td>
                </tr>
                <tr>
                    <th>
                        UVR Actual
                    </th>
                    <td class="valores_td" id="uvr_actual">
                        0
                    </td>
                </tr>
                <tr>
                    <th>
                        UVR Inicial
                    </th>
                    <td class="valores_td" id="uvr_inicial">
                        0
                    </td>
                </tr>
                <tr class="alert alert-success">
                    <th>Valor indexado</th>
                    <td class="valores_td" id="uvr_indexado">
                        0
                    </td>
                </tr>
            </table>
        </div>

        <div class="contenedor_ibc col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <table class="table table-hovered table_title">
                <tr>
                    <th>
                        INDEXACIÓN IBC
                    </th>

                </tr>
            </table>
            <table class="table table-bordered" style="margin-top:-15px;">
                <tr>
                    <th>
                        Valor a reintegrar
                    </th>
                    <td class="td_valor_reintegral">
                        0
                    </td>
                </tr>
                <tr>
                    <th>
                        IBC Actual
                    </th>
                    <td class="valores_td" id="ibc_actual">
                        0
                    </td>
                </tr>
                <tr>
                    <th>
                        IBC Inicial
                    </th>
                    <td class="valores_td" id="ibc_inicial">
                        0
                    </td>
                </tr>
                <tr class="alert alert-success">
                    <th>Valor indexado</th>
                    <td class="valores_td" id="ibc_indexado">
                        0
                    </td>
                </tr>
            </table>
        </div>

        <div class="contenedor_usura col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <table class="table table-hovered table_title">
                <tr>
                    <th>
                        INDEXACIÓN USURA
                    </th>
                </tr>
            </table>
            <table class="table table-bordered" style="margin-top:-15px;">
                <tr>
                    <th>
                        Valor a reintegrar
                    </th>
                    <td class="td_valor_reintegral">
                        0
                    </td>
                </tr>
                <tr>
                    <th>
                        Tasa USURA Actual
                    </th>
                    <td class="valores_td" id="usura_actual">
                        0
                    </td>
                </tr>
                <tr>
                    <th>
                        Tasa USURA Inicial
                    </th>
                    <td class="valores_td" id="usura_inicial">
                        0
                    </td>
                </tr>
                <tr class="alert alert-success">
                    <th>Valor indexado</th>
                    <td class="valores_td" id="usura_indexado">
                        0
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <table class="table table-hovered table_title">
        <tr>
            <th>
                INDEXACIÓN DÓLAR
            </th>
        </tr>
    </table>
    <table class="table table-bordered" style="margin-top:-15px;">
        <tr>
            <th>
                Valor a reintegrar
            </th>
            <td class="td_valor_reintegral">
                0
            </td>
        </tr>
        <tr>
            <th>
                TRM Actual
            </th>
            <td class="valores_td" id="trm_actual">
                0
            </td>
        </tr>
        <tr>
            <th>
                TRM Inicial
            </th>
            <td class="valores_td" id="trm_inicial">
                0
            </td>
        </tr>
        <tr class="alert alert-success">
            <th>Valor indexado</th>
            <td class="valores_td" id="trm_indexado">
                0
            </td>
        </tr>
    </table>
    <hr>
    <div class="buttons-options col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="text-align: center;">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close"
            id="button_cancel">Cancelar</button>
        <button type="submit" class="btn btn-application">Generar cálculo</button>
    </div>
</form>

<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/subscriber/calculators/calculadora_indexacion.js')) }}"></script>
