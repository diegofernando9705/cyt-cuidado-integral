<style>
    .contentText {
        padding: 5rem 0;
        background: rgb(2, 0, 36);
        background: linear-gradient(0deg, rgba(2, 0, 36, 1) 0%, rgba(9, 9, 121, 1) 35%, rgba(10, 48, 56, 1) 100%);
    }
</style>

@extends('page-layouts.template.app')

@section('title', 'Contáctanos')

@section('style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link href="{{ asset('page/assets/css/style_pagina_inicio.css') }}" rel="stylesheet">
@endsection

@section('navegacion')
    <div class="container-xxl bg-primary" style="padding-top: 6rem; background-color:#002E66 !important;">
        <div class="container">
            <div class="row g-5 align-items-end">
                <div class="col-lg-12 text-center text-lg-start">
                    <h1 class="text-white mb-4 animated slideInDown">
                        {{ $title }}
                        <p class="text-white pb-3 animated slideInDown">
                        </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('container')
    <div class="container-xxl py-5">
        <div class="container px-lg-5">
            <div class="wow fadeInUp" data-wow-delay="0.1s">
                <h1 class="text-center mb-5">{{ $alert }}
                    <hr>
                </h1>
            </div>
        </div>
    </div>
@endsection

@section('codigo')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar/index.global.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endsection
