<style>
    .contentText {
        padding: 5rem 0;
        background: rgb(2, 0, 36);
        background: linear-gradient(0deg, rgba(2, 0, 36, 1) 0%, rgba(9, 9, 121, 1) 35%, rgba(10, 48, 56, 1) 100%);
    }
</style>

<div class="container-xxl bg-primary contentText">
    <div class="container px-lg-5">
        <div class="row g-5 align-items-end">
            <div class="col-lg-6 text-center text-lg-start">
                <h1 class="text-white mb-4 animated slideInDown">
                    {{ $content->title }}</h1>
                <p class="text-white pb-3 animated slideInDown">
                    {{ $content->description }}
                </p>
            </div>
        </div>
    </div>
</div>
