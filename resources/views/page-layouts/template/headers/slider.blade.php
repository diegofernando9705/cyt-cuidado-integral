<div class="encabezado-slider owl-carousel">
    @foreach ($content as $data)
        <div class="single-box-encabezado text-center-encabezado">
            <div class="imagen_de_fondo_encabezado"
                style="background-image: url({{ config("app.AWS_BUCKET_URL") }}{{ $data->url }})">
            </div>
            <div class="contenedor_informacion_slider_encabezado">
                <div class="contenedor_titulo_slider_encabezado">

                </div>
                <div class="contenedor_descripcion_slider_encabezado">
                    <div class="row">
                        <div class="titulo_contenedor_slider col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            {{ $data->title }}
                        </div>
                        <div class="descripcion_contenedor_slider col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <p>
                                {{ $data->description }}
                            </p>
                        </div>
                        <div class="boton_slider_encabezado col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <a class="btn btn-application" href="{{ $data->url_button }}" target="_blank">{{ $data->text_button }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
