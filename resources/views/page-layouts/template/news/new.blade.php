<style>
    .contentText {
        padding: 5rem 0;
        background: rgb(2, 0, 36);
        background: linear-gradient(0deg, rgba(2, 0, 36, 1) 0%, rgba(9, 9, 121, 1) 35%, rgba(10, 48, 56, 1) 100%);
    }
</style>

@extends('page-layouts.template.app')

@section('title', $data->title)

@section('style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link href="{{ asset('page/assets/css/style_pagina_inicio.css') }}" rel="stylesheet">
    <style>
        .contenedor-new-most {
            margin-bottom: 20px;
        }

        .contenedor-new-most>a>b {
            color: black;
        }

        .contenedor-new-most>a>table {
            margin-top: 5px;
            margin-bottom: 5px;
            color: grey;
            font-size: 12px;
        }

        .social-network {
            text-align: right;
            font-size: 12px;
        }

        .all-description {
            margin-top: 15px;
        }

        .metadatos>table {
            font-size: 12px;
        }

        .option-favorite {
            width: 50px;
            height: 50px;
            background-color: #002E66;
            border-radius: 50%;
            position: absolute;
            right: 20px;
            top: 8px;
            margin-left: 5px;
            border: 1px solid whitesmoke;


            display: flex;
            /* Usar flexbox */
            justify-content: center;
            /* Centrar horizontalmente */
            align-items: center;
            /* Centrar verticalmente */
        }

        .content-new-all {
            position: relative;
            /* Hacer que el posicionamiento absoluto sea relativo a este contenedor */
        }

        .icon-favorite-new:hover {
            cursor: pointer;
        }
    </style>
@endsection

@section('navegacion')
    <div class="container-xxl bg-primary" style="padding-top: 6rem; background-color:#002E66 !important;">
        <div class="container">
            <div class="row g-5 align-items-end">
                <div class="col-lg-12 text-center text-lg-start">
                    <h1 class="text-white mb-4 animated slideInDown">
                        {{ $data->title }}
                        <p class="text-white pb-3 animated slideInDown">
                        </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('container')
    <div class="container-xxl py-5">
        <div class="container px-lg-5">
            <div class="wow fadeInUp" data-wow-delay="0.1s">
                <div class="row">
                    <div class="content-new-all col-12 col-sm-9 col-md-9 col-lg-9 col-xl-9">
                        <div class="option-favorite" style="text-align: center;">
                            @if ($favorite)
                                <svg fill="#ffffff" width="30px" height="30px" viewBox="0 0 1000 1000" class="icon-favorite-new"
                                    xmlns="http://www.w3.org/2000/svg" stroke="#ffffff" data-id="{{ $data->id }}"> 
                                    <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                    <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                                    <g id="SVGRepo_iconCarrier">
                                        <path
                                            d="M476 801l-181 95q-18 10-36.5 4.5T229 879t-7-36l34-202q2-12-1.5-24T242 596L95 453q-15-14-15.5-33.5T91 385t32-18l203-30q12-2 22-9t16-18l90-184q10-18 28-25t36 0 28 25l90 184q6 11 16 18t22 9l203 30q20 3 32 18t11.5 34.5T905 453L758 596q-8 9-12 21t-2 24l34 202q4 20-7 36t-29.5 21.5T705 896l-181-95q-11-6-24-6t-24 6z">
                                        </path>
                                    </g>
                                </svg>
                            @else
                                <svg width="30px" height="30px" viewBox="0 0 1024 1024" fill="#ffffff"
                                    class="icon icon-favorite-new" data-id="{{ $data->id }}" version="1.1"
                                    xmlns="http://www.w3.org/2000/svg" stroke="#ffffff">
                                    <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                    <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                                    <g id="SVGRepo_iconCarrier">
                                        <path
                                            d="M802.4 967.2c-7.2 0-15.2-1.6-21.6-4.8l-258.4-128.8-252.8 140c-18.4 10.4-41.6 5.6-56-9.6-8.8-9.6-12.8-23.2-11.2-36.8l43.2-285.6L33.6 444C20.8 432 16 414.4 21.6 397.6c4.8-16.8 18.4-28.8 36-31.2l285.6-48L464.8 56c7.2-15.2 22.4-25.6 39.2-26.4 17.6-0.8 33.6 8.8 41.6 24l133.6 256.8 287.2 35.2c17.6 2.4 31.2 13.6 36.8 30.4 5.6 16 1.6 34.4-10.4 46.4L790.4 629.6l55.2 284c2.4 12.8-0.8 26.4-8.8 36.8-8.8 10.4-21.6 16.8-34.4 16.8zM520.8 784.8c7.2 0 15.2 1.6 21.6 4.8l255.2 127.2-54.4-280c-3.2-14.4 1.6-29.6 12-40l200-203.2L672 358.4c-14.4-1.6-28-11.2-34.4-24L506.4 81.6 385.6 340c-6.4 13.6-19.2 23.2-33.6 25.6L70.4 412l208 194.4c11.2 10.4 16 24.8 13.6 40L249.6 928l249.6-137.6c7.2-3.2 14.4-4.8 21.6-5.6z"
                                            fill=""></path>
                                    </g>
                                </svg>
                            @endif
                        </div>
                        <img src="{{ config('app.AWS_BUCKET_URL') . $data->image }}" alt="{{ $data->title }}"
                            width="100%">
                        <div class="footer-image" style="margin-top: 10px;">
                            <div class="row">
                                <div class="metadatos col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <table class="col-12 col-sm-2 col-md-12 col-lg-12 col-xl-12">
                                        <tr>
                                            <td>
                                                Por {{ $data->getUser->name }}
                                            </td>
                                            <td>
                                                {{ $data->created_at->locale('es')->isoFormat('dddd, D [de] MMMM [a las] H:mm') }}

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="social-network col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <svg fill="#002e66" height="33px" width="33px" version="1.1" id="Layer_1"
                                        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        viewBox="-143 145 512 512" xml:space="preserve">
                                        <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                        <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                                        <g id="SVGRepo_iconCarrier">
                                            <path
                                                d="M113,145c-141.4,0-256,114.6-256,256s114.6,256,256,256s256-114.6,256-256S254.4,145,113,145z M169.5,357.6l-2.9,38.3h-39.3 v133H77.7v-133H51.2v-38.3h26.5v-25.7c0-11.3,0.3-28.8,8.5-39.7c8.7-11.5,20.6-19.3,41.1-19.3c33.4,0,47.4,4.8,47.4,4.8l-6.6,39.2 c0,0-11-3.2-21.3-3.2c-10.3,0-19.5,3.7-19.5,14v29.9H169.5z">
                                            </path>
                                        </g>
                                    </svg>
                                    <svg width="40px" height="40px" viewBox="0 0 30 30" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                        <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                                        <g id="SVGRepo_iconCarrier">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M16 31C23.732 31 30 24.732 30 17C30 9.26801 23.732 3 16 3C8.26801 3 2 9.26801 2 17C2 19.5109 2.661 21.8674 3.81847 23.905L2 31L9.31486 29.3038C11.3014 30.3854 13.5789 31 16 31ZM16 28.8462C22.5425 28.8462 27.8462 23.5425 27.8462 17C27.8462 10.4576 22.5425 5.15385 16 5.15385C9.45755 5.15385 4.15385 10.4576 4.15385 17C4.15385 19.5261 4.9445 21.8675 6.29184 23.7902L5.23077 27.7692L9.27993 26.7569C11.1894 28.0746 13.5046 28.8462 16 28.8462Z"
                                                fill="#BFC8D0"></path>
                                            <path
                                                d="M28 16C28 22.6274 22.6274 28 16 28C13.4722 28 11.1269 27.2184 9.19266 25.8837L5.09091 26.9091L6.16576 22.8784C4.80092 20.9307 4 18.5589 4 16C4 9.37258 9.37258 4 16 4C22.6274 4 28 9.37258 28 16Z"
                                                fill="url(#paint0_linear_87_7264)"></path>
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M16 30C23.732 30 30 23.732 30 16C30 8.26801 23.732 2 16 2C8.26801 2 2 8.26801 2 16C2 18.5109 2.661 20.8674 3.81847 22.905L2 30L9.31486 28.3038C11.3014 29.3854 13.5789 30 16 30ZM16 27.8462C22.5425 27.8462 27.8462 22.5425 27.8462 16C27.8462 9.45755 22.5425 4.15385 16 4.15385C9.45755 4.15385 4.15385 9.45755 4.15385 16C4.15385 18.5261 4.9445 20.8675 6.29184 22.7902L5.23077 26.7692L9.27993 25.7569C11.1894 27.0746 13.5046 27.8462 16 27.8462Z"
                                                fill="white"></path>
                                            <path
                                                d="M12.5 9.49989C12.1672 8.83131 11.6565 8.8905 11.1407 8.8905C10.2188 8.8905 8.78125 9.99478 8.78125 12.05C8.78125 13.7343 9.52345 15.578 12.0244 18.3361C14.438 20.9979 17.6094 22.3748 20.2422 22.3279C22.875 22.2811 23.4167 20.0154 23.4167 19.2503C23.4167 18.9112 23.2062 18.742 23.0613 18.696C22.1641 18.2654 20.5093 17.4631 20.1328 17.3124C19.7563 17.1617 19.5597 17.3656 19.4375 17.4765C19.0961 17.8018 18.4193 18.7608 18.1875 18.9765C17.9558 19.1922 17.6103 19.083 17.4665 19.0015C16.9374 18.7892 15.5029 18.1511 14.3595 17.0426C12.9453 15.6718 12.8623 15.2001 12.5959 14.7803C12.3828 14.4444 12.5392 14.2384 12.6172 14.1483C12.9219 13.7968 13.3426 13.254 13.5313 12.9843C13.7199 12.7145 13.5702 12.305 13.4803 12.05C13.0938 10.953 12.7663 10.0347 12.5 9.49989Z"
                                                fill="white"></path>
                                            <defs>
                                                <linearGradient id="paint0_linear_87_7264" x1="26.5" y1="7"
                                                    x2="4" y2="28" gradientUnits="userSpaceOnUse">
                                                    <stop stop-color="#5BD066"></stop>
                                                    <stop offset="1" stop-color="#27B43E"></stop>
                                                </linearGradient>
                                            </defs>
                                        </g>
                                    </svg>
                                    <svg width="30px" height="30px" viewBox="0 0 1024 1024"
                                        xmlns="http://www.w3.org/2000/svg" fill="#000000">
                                        <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                        <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                                        <g id="SVGRepo_iconCarrier">
                                            <circle cx="512" cy="512" r="512" style="fill:#1da1f2"></circle>
                                            <path
                                                d="M778 354.8c-18.8 8.3-38.9 13.9-60.1 16.5 21.6-13 38.2-33.5 46-57.9-20.2 11.8-42.7 20.4-66.5 25.2-19.1-20.4-46.2-33.2-76.4-33.2-57.8 0-104.7 46.9-104.7 104.6 0 8.3 1 16.3 2.7 23.9-87-4.1-164.2-45.9-215.8-109.1-9.1 15.4-14.2 33.2-14.2 52.7 0 36.4 18.5 68.4 46.6 87.2-17.2-.6-33.3-5.3-47.4-13.1v1.3c0 50.8 36 93.1 84 102.7-8.8 2.4-18.1 3.6-27.6 3.6-6.7 0-13.1-.6-19.5-1.8 13.4 41.6 52 71.9 98 72.7-35.7 28.1-81.1 44.8-129.8 44.8-8.3 0-16.6-.5-24.9-1.4 46.6 29.7 101.5 47 160.8 47C621.7 720.5 727 561 727 422.9c0-4.4 0-8.9-.3-13.4 20.4-14.7 38.3-33.2 52.3-54.2l-1-.5z"
                                                style="fill:#fff"></path>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div class="all-description">
                            {!! $data->description !!}
                        </div>
                    </div>
                    <div class="all-news col-12 col-sm-3 col-md-3 col-xl-3 col-lg-3">
                        <h3>
                            Lo más visto
                        </h3>
                        <hr>
                        @foreach ($mostViewedNews as $mostNew)
                            <div class="contenedor-new-most col-12 col-sm-12 col-md-12 col-lg-12 col-sl-12">
                                <a href="{{ $mostNew->url }}">
                                    <img src="{{ config('app.AWS_BUCKET_URL') . $mostNew->image }}" alt=""
                                        width="100%">
                                    <table class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <tbody>
                                            <td><i>{{ $mostNew->getUser->name }}</i></td>
                                            <td style="text-align: left;"><i>{{ $mostNew->created_at }}</i></td>
                                        </tbody>
                                    </table>
                                    <b>{{ $mostNew->title }}</b>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('codigo')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        $(document).on("click", ".icon-favorite-new", function() {
            var id = $(this).attr("data-id");
            $.ajax({
                type: "GET",
                url: "/subscriber/myNews/favorite/" + id,
                success: function(data) {
                    if(data=="delete_favorite"){
                        alert("Eliminado de tus favoritos");
                    }else if(data=="add_favorite"){
                        alert("Agregado a tus favoritos");
                    }
                },
                error: function(error) {
                    console.log(error);
                },
            });
        });
    </script>
@endsection
