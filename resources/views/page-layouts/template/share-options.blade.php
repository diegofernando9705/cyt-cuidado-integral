<style>
    #lista1 {
        font-family: Arial;
        font-weight: bold;
        font-size: 13px;
        border-top: 1px solid rgb(167, 167, 167);
        padding-top: 10px;

    }

    #lista1 li {
        display: inline;
    }

    a {
        color: white;
        text-decoration: none;
    }
</style>

<ul id="lista1">
    Comparte en tus redes favoritas: <br>
    <li>
        <a class="share_fb">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="40px" height="40px">
                <path fill="#039be5" d="M24 5A19 19 0 1 0 24 43A19 19 0 1 0 24 5Z" />
                <path fill="#fff"
                    d="M26.572,29.036h4.917l0.772-4.995h-5.69v-2.73c0-2.075,0.678-3.915,2.619-3.915h3.119v-4.359c-0.548-0.074-1.707-0.236-3.897-0.236c-4.573,0-7.254,2.415-7.254,7.917v3.323h-4.701v4.995h4.701v13.729C22.089,42.905,23.032,43,24,43c0.875,0,1.729-0.08,2.572-0.194V29.036z" />
            </svg>
        </a>
    </li>
    <li>
        <a class="share_twitter">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="40px" height="40px">
                <path fill="#03a9f4" d="M24,4C12.954,4,4,12.954,4,24s8.954,20,20,20s20-8.954,20-20S35.046,4,24,4z" />
                <path fill="#fff"
                    d="M36,17.12c-0.882,0.391-1.999,0.758-3,0.88c1.018-0.604,2.633-1.862,3-3	c-0.951,0.559-2.671,1.156-3.793,1.372C29.789,13.808,24,14.755,24,20v2c-4,0-7.9-3.047-10.327-6c-2.254,3.807,1.858,6.689,2.327,7	c-0.807-0.025-2.335-0.641-3-1c0,0.016,0,0.036,0,0.057c0,2.367,1.661,3.974,3.912,4.422C16.501,26.592,16,27,14.072,27	c0.626,1.935,3.773,2.958,5.928,3c-2.617,2.029-7.126,2.079-8,1.977c8.989,5.289,22.669,0.513,21.982-12.477	C34.95,18.818,35.342,18.104,36,17.12" />
            </svg>
        </a>
    </li>
    <li>
        <a class="share_linkedin">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="40px" height="40px">
                <path fill="#0288d1" d="M24,4C13,4,4,13,4,24s9,20,20,20s20-9,20-20S35,4,24,4z" />
                <rect width="4" height="15" x="14" y="19" fill="#fff" />
                <path fill="#fff"
                    d="M16,17L16,17c-1.2,0-2-0.9-2-2c0-1.1,0.8-2,2-2c1.2,0,2,0.9,2,2C18,16.1,17.2,17,16,17z" />
                <path fill="#fff"
                    d="M35,24.5c0-3-2.5-5.5-5.5-5.5c-1.9,0-3.5,0.9-4.5,2.3V19h-4v15h4v-8c0-1.7,1.3-3,3-3s3,1.3,3,3v8h4	C35,34,35,24.9,35,24.5z" />
            </svg>
        </a>
    </li>
    <li>
        <a class="share_email">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="40px" height="40px">
                <circle cx="24" cy="24" r="20" fill="#669df6" />
                <path fill="#317be4"
                    d="M42,27v5.73C38.76,39.4,31.92,44,24,44C12.95,44,4,35.05,4,24c0-2.09,0.32-4.1,0.92-6H33	C37.97,18,42,22.03,42,27z" />
                <path fill="#fff"
                    d="M22.976,24l9.976-6.234C32.835,16.773,32,16,30.976,16h-16c-1.024,0-1.859,0.773-1.976,1.766	L22.976,24z" />
                <path fill="#fff"
                    d="M33,19.75v5.79C32.41,25.2,31.73,25,31,25c-2.21,0-4,1.79-4,4c0,1.2,0.53,2.27,1.36,3H15	c-1.1,0-2-0.9-2-2V19.75L23,26L33,19.75z" />
                <circle cx="31" cy="29" r="2" fill="#fff" />
                <path fill="#fff" d="M35,36h-8v-1c0-1.33,2.665-2,4-2s4,0.67,4,2V36z" />
            </svg>
        </a>
    </li>
    <li>
        <a class="share_whatsapp">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="40px" height="40px"
                fill-rule="evenodd" clip-rule="evenodd">
                <path fill="#fff"
                    d="M4.868,43.303l2.694-9.835C5.9,30.59,5.026,27.324,5.027,23.979C5.032,13.514,13.548,5,24.014,5c5.079,0.002,9.845,1.979,13.43,5.566c3.584,3.588,5.558,8.356,5.556,13.428c-0.004,10.465-8.522,18.98-18.986,18.98c-0.001,0,0,0,0,0h-0.008c-3.177-0.001-6.3-0.798-9.073-2.311L4.868,43.303z" />
                <path fill="#fff"
                    d="M4.868,43.803c-0.132,0-0.26-0.052-0.355-0.148c-0.125-0.127-0.174-0.312-0.127-0.483l2.639-9.636c-1.636-2.906-2.499-6.206-2.497-9.556C4.532,13.238,13.273,4.5,24.014,4.5c5.21,0.002,10.105,2.031,13.784,5.713c3.679,3.683,5.704,8.577,5.702,13.781c-0.004,10.741-8.746,19.48-19.486,19.48c-3.189-0.001-6.344-0.788-9.144-2.277l-9.875,2.589C4.953,43.798,4.911,43.803,4.868,43.803z" />
                <path fill="#cfd8dc"
                    d="M24.014,5c5.079,0.002,9.845,1.979,13.43,5.566c3.584,3.588,5.558,8.356,5.556,13.428c-0.004,10.465-8.522,18.98-18.986,18.98h-0.008c-3.177-0.001-6.3-0.798-9.073-2.311L4.868,43.303l2.694-9.835C5.9,30.59,5.026,27.324,5.027,23.979C5.032,13.514,13.548,5,24.014,5 M24.014,42.974C24.014,42.974,24.014,42.974,24.014,42.974C24.014,42.974,24.014,42.974,24.014,42.974 M24.014,42.974C24.014,42.974,24.014,42.974,24.014,42.974C24.014,42.974,24.014,42.974,24.014,42.974 M24.014,4C24.014,4,24.014,4,24.014,4C12.998,4,4.032,12.962,4.027,23.979c-0.001,3.367,0.849,6.685,2.461,9.622l-2.585,9.439c-0.094,0.345,0.002,0.713,0.254,0.967c0.19,0.192,0.447,0.297,0.711,0.297c0.085,0,0.17-0.011,0.254-0.033l9.687-2.54c2.828,1.468,5.998,2.243,9.197,2.244c11.024,0,19.99-8.963,19.995-19.98c0.002-5.339-2.075-10.359-5.848-14.135C34.378,6.083,29.357,4.002,24.014,4L24.014,4z" />
                <path fill="#40c351"
                    d="M35.176,12.832c-2.98-2.982-6.941-4.625-11.157-4.626c-8.704,0-15.783,7.076-15.787,15.774c-0.001,2.981,0.833,5.883,2.413,8.396l0.376,0.597l-1.595,5.821l5.973-1.566l0.577,0.342c2.422,1.438,5.2,2.198,8.032,2.199h0.006c8.698,0,15.777-7.077,15.78-15.776C39.795,19.778,38.156,15.814,35.176,12.832z" />
                <path fill="#fff" fill-rule="evenodd"
                    d="M19.268,16.045c-0.355-0.79-0.729-0.806-1.068-0.82c-0.277-0.012-0.593-0.011-0.909-0.011c-0.316,0-0.83,0.119-1.265,0.594c-0.435,0.475-1.661,1.622-1.661,3.956c0,2.334,1.7,4.59,1.937,4.906c0.237,0.316,3.282,5.259,8.104,7.161c4.007,1.58,4.823,1.266,5.693,1.187c0.87-0.079,2.807-1.147,3.202-2.255c0.395-1.108,0.395-2.057,0.277-2.255c-0.119-0.198-0.435-0.316-0.909-0.554s-2.807-1.385-3.242-1.543c-0.435-0.158-0.751-0.237-1.068,0.238c-0.316,0.474-1.225,1.543-1.502,1.859c-0.277,0.317-0.554,0.357-1.028,0.119c-0.474-0.238-2.002-0.738-3.815-2.354c-1.41-1.257-2.362-2.81-2.639-3.285c-0.277-0.474-0.03-0.731,0.208-0.968c0.213-0.213,0.474-0.554,0.712-0.831c0.237-0.277,0.316-0.475,0.474-0.791c0.158-0.317,0.079-0.594-0.04-0.831C20.612,19.329,19.69,16.983,19.268,16.045z"
                    clip-rule="evenodd" />
            </svg>
        </a>
    </li>
</ul>
<script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
    crossorigin="anonymous"></script>

<script>
    $(document).on("click", ".share_fb", function() {
        window.open(
            "http://www.facebook.com/sharer.php?u={{ $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI'] }}",
            'Comparte en Facebook',
            'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
    });

    $(document).on("click", ".share_twitter", function() {
        window.open(
            "https://twitter.com/share?url={{ $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI'] }}",
            'Comparte en Facebook',
            'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
    });

    $(document).on("click", ".share_linkedin", function() {
        window.open(
            "http://www.linkedin.com/shareArticle?mini=true&url={{ $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI'] }}",
            'Comparte en Facebook',
            'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
    });

    $(document).on("click", ".share_email", function() {
        window.open(
            "mailto:?subject={{ $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI'] }}",
            'Comparte en Facebook',
            'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
    });

    $(document).on("click", ".share_whatsapp", function() {
        window.open(
            "https://api.whatsapp.com/send?text={{ $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI'] }}",
            'Comparte en Facebook',
            'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
    });
</script>
