<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">

<style>
    .password-container {
        position: relative;
    }

    .password-icon {
        position: absolute;
        right: 10px;
        bottom: 0%;
        transform: translateY(-50%);
        cursor: pointer;
        z-index: 1;
        z-index: 99999;
    }
</style>

<div class="card-body">
    <a href="javascript:void(0);" class="brand-logo">
        <img src="https://s3.amazonaws.com/cnp.com.co/textocnp.png" alt="" width="100%">
    </a>
    <hr>
    <div class="alert alert-danger" id="alertDanger" style="display: none;">

    </div>
    <form class="formulario-login" novalidate>
        @csrf
        @method('POST')
        <input type="hidden" id="csrf_token" value="{{ csrf_token() }}">

        <div id="mensajes"></div>
        <div class="form-group">
            <div class="form-outline">
                <label for="email">Correo electr&oacute;nico</label>
                <input type="email" class="form-control" id="email" name="email" required />
                <div class="invalid-feedback">Por favor, ingrese un correo electrónico válido.</div>
                <div class="valid-feedback"></div>
            </div>
        </div>

        <div class="form-group" style="margin-top:15px; position:relative;">
            <div class="form-outline">
                <div class="d-flex justify-content-between">
                    <label for="login-password">Contraseña</label>
                    <a href="#" class="reset_password" style="color: black !important;">
                        <small>¿Olvidó su contraseña?</small>
                    </a>
                </div>
                <input type="password" class="form-control" id="password" name="password" required />
                <i class="password-icon fas fa-eye-slash" id="togglePassword"></i>
                <div class="invalid-feedback">Por favor, ingrese su contraseña personal.</div>
                <div class="valid-feedback"></div>
            </div>
        </div>
        <center>
            <button type='submit' class="btn btn-dark btn-block btn-sm" name='submit' id='submit' tabindex="4"
                style="margin-top: 20px; margin-bottom: 10px;">Entrar
                al sistema</button>
        </center>
    </form>

    <p class="text-center mt-2" style="color: black !important; font-size:14px;">
        <span>¿Nuevo en la plataforma?</span> |
        <a href="javascript::void(0)" class="boton_ir_membresia" style="color:#002e66;">
            <span>Cree una cuenta</span>
        </a>
    </p>
    <div class="form-outline" style="font-size:10px; color:gray;">
        <center>
            <label class="form-label" for="login-email">Desarrollado por <b>Softworld
                    Colombia</b></label>
        </center>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/js/all.min.js"
    integrity="sha512-1qG4dmz9VhD0nW0tqsKdGjKCbXoExGqvfo9M9MAMbVH8OgVV5l9Z7+uM6CmLeGKSDZKtPBRRQU0zWftDAh2FjA=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/page/login.js')) }}"></script>
