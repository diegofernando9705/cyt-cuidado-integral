@php
    $configData = Helper::applClasses();
@endphp
@extends('page-layouts.template.app')

@section('title', 'Restablecer contraseña')

<style>
    html .content.app-content {
        padding: 0 !important;
    }

    html .content {
        margin-left: 0px !important;
    }

    #submit_button {
        background-color: black;
    }
</style>

@section('page-style')
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('container')
    <div class="row">
        <div class="images col-12 col-md-8 col-sm-8 col-lg-8 col-xl-8">
            <img src="https://s3.amazonaws.com/cnp.com.co/Publico/pexels-sora-shimazaki-5668772.jpg" style="width: 100%;"
                alt="">
        </div>
        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                <h2 class="card-title font-weight-bold mb-1">Reiniciar contraseña 🔒</h2>
                <hr>
                <form class="update-reset-password-form" novalidate>
                    @csrf
                    @method('POST')

                    <input type="hidden" id="token" name="token" value="{{ $codigo }}">

                    <div class="form-group">
                        <label for="">Nueva contraseña:</label>
                        <input type="password" class="form-control" id="password" type="password" name="password" required>
                    </div>

                    <div class="form-group">
                        <label for="">Confirmar contraseña:</label>
                        <input type="password" class="form-control" id="c_password" type="c_password" name="c_password"
                            required>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary btn-sm col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
                        id="submit_button">Actualizar contraseña.</button>
                </form>
                <div class="form-outline" style="font-size:10px; color:gray; margin-top:4%;">
                    <center>
                        <label class="form-label" for="login-email">Desarrollado por <b>Softworld
                                Colombia</b></label>
                    </center>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('codigo')

    <script src="{{ asset('vendors/js/forms/validation/jquery.validate.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        const update_password = document.querySelectorAll('.update-reset-password-form');

        // Loop over them and prevent submission
        Array.prototype.slice.call(update_password).forEach((form) => {
            form.addEventListener('submit', (event) => {
                if (!form.checkValidity()) {
                    event.stopPropagation();
                } else {

                    var paqueteDeDatos = new FormData();

                    paqueteDeDatos.append('_token', "{{ csrf_token() }}");
                    paqueteDeDatos.append('codigo', $('#token').prop('value'));
                    paqueteDeDatos.append('password', $('#password').prop('value'));
                    paqueteDeDatos.append('c_password', $('#c_password').prop('value'));

                    document.getElementById("password").disabled = true;
                    document.getElementById("c_password").disabled = true;

                    document.getElementById("submit_button").disabled = true;

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    var csrfToken = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({
                        type: "POST",
                        contentType: false,
                        url: "/update-recovery-password",
                        headers: {
                            "X-CSRF-Token": csrfToken,
                        },
                        data: paqueteDeDatos, // serializes the form's elements.
                        processData: false,
                        cache: false,
                        success: function(data) {
                            if (data == true) {
                                Swal.fire({
                                    title: "Actualización exitosa!",
                                    text: "La contraseña se actualizó con éxito.",
                                    allowOutsideClick: false, // Evita que se cierre con clic fuera del modal
                                    allowEscapeKey: false, // Evita que se cierre con la tecla ESC
                                    showCloseButton: false, // No muestra el botón de cierre
                                    icon: "success"
                                });

                                setTimeout(() => {
                                    location.reload('');    
                                }, 1000);

                            } else {
                                Swal.fire({
                                    title: "Error",
                                    text: "Lo siento, no se puedo realizar el envio de contraseña.",
                                    footer: 'Estamos trabajando para dar solución al inconveniente presentado',
                                    icon: "error"
                                });

                                setTimeout(() => {
                                    $("#modalLogin").modal("hide");
                                }, 1000);
                            }
                        },
                        error: function(err) {
                            if (err.status == 422) {
                                $(".errores").attr("style", "display:none;");
                                $("#success_message").fadeIn().html(err.responseJSON.message);

                                $.each(err.responseJSON.errors, function(i, error) {
                                    var el = $(document).find('[name="' + i + '"]');
                                    el.after(
                                        $(
                                            '<span class="errores" style="color: red;">' +
                                            error[0] +
                                            "</span>"
                                        )
                                    );
                                });
                            } else {
                                alert("Error en el proceso");
                            }

                            document.getElementById("password").disabled = false;
                            document.getElementById("c_password").disabled = false;

                            document.getElementById("submit_button").disabled = false;
                        },
                    });

                    event.preventDefault();

                }
                event.preventDefault();
                form.classList.add('was-validated');
            }, false);
        });
    </script>
@endsection
