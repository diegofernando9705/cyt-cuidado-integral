<!-- Brand logo-->
<a class="brand-logo" href="javascript:void(0);">
    <img src="https://s3.amazonaws.com/cnp.com.co/textocnp.png" alt="" width="100%">
</a>
<!-- /Brand logo-->

<div class="d-flex col-12 col-sm-12 col-lg-12 col-xl-12 align-items-center auth-bg px-2 p-lg-12">
    <div class="col-12 col-sm-12 col-md-12 col-lg-12 px-xl-12 mx-auto">
        <hr>
        <p class="card-text mb-2">Ingrese su correo electrónico y le enviaremos instrucciones para restablecer
            su contraseña</p>

        <form class="auth-forgot-password" novalidate>
            @csrf
            @method('POST')
            <div class="form-group">
                <input class="form-control" type="email" id="email" name="email" placeholder="info@cnp.com.co"
                    required />
                <div class="invalid-feedback">Por favor, ingrese un correo electrónico válido.</div>
                <div class="valid-feedback"></div>
            </div>
            <center>
                <button type="submit" class="btn btn-dark btn-block btn-sm" style="margin-top: 20px;" tabindex="2"
                    id="submit">Enviar instrucciones</button>
            </center>
        </form>
        <p class="text-center mt-2">
            <a href="#" class="abrir-modal-login">
                <i data-feather="chevron-left"></i>Regresar al inicio
            </a>
        </p>
        <div class="form-outline" style="font-size:10px;">
            <center>
                <label class="form-label" for="login-email">Desarrollado por <b>Softworld
                        Colombia</b></label>
            </center>
        </div>
    </div>

</div>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/page/login.js')) }}"></script>
