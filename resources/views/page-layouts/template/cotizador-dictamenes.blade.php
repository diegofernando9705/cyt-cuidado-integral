<style>
    .form-control {
        display: block;
        padding: 0.375rem 0.75rem;
        font-size: 12px !important;
        font-weight: 400;
        line-height: 1.5;
        color: #002e66;
        background-color: #fff;
        background-clip: padding-box;
        appearance: none;
        border-radius: 4px;
        box-shadow: none;
        margin-top: 3px;
    }

    .form-control:focus {
        display: block;
        padding: 0.375rem 0.75rem;
        font-size: 12px !important;
        font-weight: 400;
        line-height: 1.5;
        color: #726D7B;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #002E66;
        appearance: none;
        border-radius: 8px;
        box-shadow: none;
        margin-top: 3px;
    }

    .label_dictamen {
        font-family: "OpenSans-Regular";
        color: #002e66 !important;
        margin-top: 10px;
        font-size: 13px;
    }

    #submit_cotizador {
        font-size: 13px;
        margin-top: 20px;
        font-family: "OpenSans-Regular" !important;
        background-color: #002e66 !important;
        color: white;
        text-decoration: none;
        border-radius: 6px;
        text-align: center;
        box-shadow: rgba(50, 50, 93, 0.25) 0px 6px 12px -2px, rgba(0, 0, 0, 0.3) 0px 3px 7px -3px;
    }
</style>

<div class="contenedor_cuarto_principal col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
    <div class="capa_superpuesta col-12 col-sm-12 col-md-12 col-lg-12" id="cotizador_dictamen"></div>
    <div class="container-xxl py-5 px-lg-5" style="box-shadow: none !important;">
        <div class="row">
            <div class="contenedor_izquierdo_cuarto col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6">
                <div class="row">

                    <div class="titulo_izquierdo_cuarto col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <h1>
                            Cotizador de Dictámenes
                        </h1>
                    </div>
                    <div class="descripcion_izquierdo_cuarto col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <p>
                            Realiza tu cotización online para conocer de manera sencilla y ágil el valor de sus
                            dictámenes
                        </p>
                    </div>
                </div>
            </div>

            <div class="contenedor_derecho_cuarto col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6">
                <div class="contenedor_formulario col-12 col-sm-12 col-md-12 col-lg-12">
                    <form class="register_dictamen_form" id="register_dictamen_form" novalidate>
                        @csrf
                        @method('POST')
                        <div class="row">

                            <div
                                class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 col-lg-12 div_nombres_apellidos_persona">
                                <label for="" class="label_dictamen">(*) Nombres y apellidos:</label>
                                <input type="text" class="form-control" name="nombres_apellidos_persona"
                                    id="nombres_apellidos_persona" required>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>

                            </div>

                            <div class="form-group col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 col-lg-6">
                                <label for="" class="label_dictamen">(*) Correo electrónico:</label>
                                <input type="email" class="form-control" name="correo_persona" id="correo_persona"
                                    required>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>

                            </div>

                            <div class="form-group col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                <label for="" class="label_dictamen">(*) Teléfono / Celular:</label>
                                <input type="text" class="form-control" name="celular_persona" id="celular_persona"
                                    required>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>

                            </div>

                            {{-- 
                            <div class="form-group col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                <label for="" class="label_dictamen">(*) Ciudad:</label>
                                 --}}
                            <input type="hidden" class="form-control" name="ciudad_persona" id="ciudad_persona"
                                required value="Sin información">
                            {{-- </div> --}}

                            {{-- 
                            <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <label for="" class="label_dictamen">Área de derecho</label> --}}
                            <textarea class="form-control" name="area_derecho_persona" id="area_derecho_persona" style="display: none;">Sin información</textarea>
                            {{--   </div>
                            --}}
                            <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <label for="" class="label_dictamen">(*) Pretensiones $</label>
                                <input type="number" class="form-control" name="valor_pretensiones"
                                    id="valor_pretensiones" required>
                            </div>

                            <div class="form-group col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                <label for="" class="label_dictamen">(*) Tipo de Cliente</label>
                                <select class="form-control selector" name="tipo_cliente" id="tipo_cliente" required>
                                    <option selected disabled></option>
                                    <option value="juridica_natural">P. Naturales</option>
                                    <option value="juridica_normal">P. Jurídica Normal</option>
                                    <option value="juridica_multinacional">P. Jurídica Multinacional</option>
                                    <option value="derecho_publico">P. Derecho Público (estado)</option>
                                </select>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>

                            </div>

                            <div class="form-group col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                <label for="" class="label_dictamen">(*) Pretensiones</label>
                                <select class="form-control selector" name="pretensiones" id="pretensiones" required>
                                    <option selected disabled></option>
                                    <option value="opcion_uno">Menores a $1.000 Mill</option>
                                    <option value="opcion_dos">Entre $1.000 y $5.000 Mill</option>
                                    <option value="opcion_tres">Entre $5.000 y $10.000 Mill</option>
                                    <option value="opcion_cuatro">Más de $10.000 Mill</option>
                                </select>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>

                            </div>

                            <div class="form-group col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                <label for="" class="label_dictamen">(*) Experiencia en el litigio</label>
                                <select class="form-control selector" name="experiencia_litigio"
                                    id="experiencia_litigio" required>
                                    <option selected disabled></option>
                                    <option value="opcion_uno">Menos de 2 años</option>
                                    <option value="opcion_dos">Entre 2 y 5 años</option>
                                    <option value="opcion_tres">Más de 5 años</option>
                                    <option value="opcion_cuatro">Firma de abogados</option>
                                </select>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>

                            </div>

                            <div class="form-group col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                <label for="" class="label_dictamen">(*) Tribunal</label>
                                <select class="form-control selector" name="tribunal" id="tribunal" required>
                                    <option selected disabled></option>
                                    <option value="privado">Privado</option>
                                    <option value="ordinario">Ordinario</option>
                                </select>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>

                            </div>

                            <div class="form-group col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                <label for="" class="label_dictamen">(*) Volumen de la información</label>
                                <select class="form-control selector" name="volumen_informacion"
                                    id="volumen_informacion" required>
                                    <option selected disabled></option>
                                    <option value="opcion_uno">Pocos Archivos y Organizados</option>
                                    <option value="opcion_dos">Pocos Archivos y No Organizados</option>
                                    <option value="opcion_tres">Muchos Archivos y Organizados/No Organizados</option>
                                </select>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>

                            </div>

                            <div class="form-group col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                <label for="" class="label_dictamen">(*) Servicios adicionales</label>
                                <select class="form-control selector" name="servicios_adicionales"
                                    id="servicios_adicionales">
                                    <option selected disabled></option>
                                    <option value="opcion_uno">Preparación de Audiencia</option>
                                    <option value="opcion_dos">Sustentación en Audiencia</option>
                                    <option value="opcion_tres">Elementos técnicos probatorios</option>
                                </select>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>

                            </div>

                            <div class="form-group col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                <label for="" class="label_dictamen">(*) Área del dictamen</label>
                                <select class="form-control selector" name="area_dictamen" id="area_dictamen"
                                    required>
                                    <option selected disabled></option>
                                    <option value="opcion_uno">Civil</option>
                                    <option value="opcion_dos">Médico</option>
                                    <option value="opcion_tres">Financiero</option>
                                </select>
                                <div class="invalid-feedback">Por favor, este campo es importante.</div>

                            </div>

                            <div class="contenedor_bottom">
                                <button type="submit" class="btn col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
                                    id="submit_cotizador">ENVIAR
                                    COTIZADOR</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        "use strict";

        const forms = document.querySelectorAll(".register_dictamen_form");

        Array.prototype.slice.call(forms).forEach((form) => {
            form.addEventListener(
                "submit",
                (event) => {
                    if (!form.checkValidity()) {
                        event.stopPropagation();
                    } else {
                        var formulario = $(".register_dictamen_form").serialize();

                        document.getElementById("nombres_apellidos_persona").disabled = true;
                        document.getElementById("correo_persona").disabled = true;
                        document.getElementById("celular_persona").disabled = true;
                        document.getElementById("ciudad_persona").disabled = true;
                        document.getElementById("area_derecho_persona").disabled = true;
                        document.getElementById("tipo_cliente").disabled = true;
                        document.getElementById("pretensiones").disabled = true;
                        document.getElementById("experiencia_litigio").disabled = true;
                        document.getElementById("tribunal").disabled = true;
                        document.getElementById("volumen_informacion").disabled = true;
                        document.getElementById("servicios_adicionales").disabled = true;
                        document.getElementById("area_dictamen").disabled = true;
                        document.getElementById("valor_pretensiones").disabled = true;
                        document.getElementById("submit_cotizador").disabled = true;

                        $.ajax({
                            type: "POST",
                            url: "{{ route('quote-opinion') }}",
                            data: formulario, // serializes the form's elements.
                            success: function(data) {
                                if (data == true) {
                                    Swal.fire({
                                        title: "<strong>Registro exitoso</strong>",
                                        icon: "success",
                                        html: "El formulario se ha registrado exitosamente. Muy pronto uno de nuestros asesores se comunicará con usted.",
                                        showCloseButton: false,
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        focusConfirm: false,
                                    });
                                }
                            },
                            error: function(err) {
                                document.getElementById(
                                    "nombres_apellidos_persona"
                                ).disabled = false;
                                document.getElementById("correo_persona").disabled =
                                    false;
                                document.getElementById("celular_persona").disabled =
                                    false;
                                document.getElementById("ciudad_persona").disabled =
                                    false;
                                document.getElementById("area_derecho_persona")
                                    .disabled = false;
                                document.getElementById(
                                    "register_dictamen_form"
                                ).disabled = false;
                                document.getElementById("tipo_cliente").disabled =
                                    false;
                                document.getElementById("pretensiones").disabled =
                                    false;
                                document.getElementById("experiencia_litigio")
                                    .disabled = false;
                                document.getElementById("tribunal").disabled = false;
                                document.getElementById("volumen_informacion")
                                    .disabled = false;
                                document.getElementById("servicios_adicionales")
                                    .disabled = false;
                                document.getElementById("area_dictamen").disabled =
                                    false;
                                document.getElementById("valor_pretensiones").disabled =
                                    false;

                                document.getElementById("submit_cotizador").disabled =
                                    false;

                                if (err.status == 403) {
                                    Swal.fire({
                                        title: "<strong>ACCIÓN RESTRINGIDA</strong>",
                                        icon: "error",
                                        html: "Usted no tiene permisos para esta acción. Comuniquese con soporte. <br>" +
                                            "<b>CÓD. ERROR: " +
                                            err.status +
                                            "</b> <br>" +
                                            "<small>" +
                                            err.statusText +
                                            "</small> <br>",
                                        showCloseButton: false,
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        focusConfirm: false,
                                    });
                                } else {
                                    Swal.fire({
                                        title: "<strong>ERROR</strong>",
                                        icon: "error",
                                        html: "Ocurrio un error en el proceso. <br>" +
                                            "<b>CÓD. ERROR: " +
                                            err.status +
                                            "</b> <br>" +
                                            "<small>" +
                                            err.statusText +
                                            "</small> <br>",
                                        showCloseButton: false,
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        focusConfirm: false,
                                    });

                                    $(".errores").hide();
                                    console.log(err.responseJSON);
                                    $("#success_message").fadeIn().html(err.responseJSON
                                        .message);
                                    console.warn(err.responseJSON.errors);
                                    $.each(err.responseJSON.errors, function(i, error) {
                                        var el = $(document).find('[name="' +
                                            i + '"]');
                                        el.after(
                                            $(
                                                '<span class="errores" style="color: red;">' +
                                                error[0] +
                                                "</span>"
                                            )
                                        );
                                    });
                                }
                            },
                        });
                        event.preventDefault();
                    }
                    event.preventDefault();
                    form.classList.add("was-validated");
                },
                false
            );
        });
    });
</script>
