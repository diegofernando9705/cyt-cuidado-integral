<div class="contenedor_cinco_principal col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
    {{-- 
    <div class="capa_superpuesta col-12 col-sm-12 col-md-12 col-lg-12"></div>
     --}}
    <div class="container-xxl py-5 px-lg-5" style="padding-bottom: 0px !important;">
        <div class="row">
            <div class="contenedor_izquierdo_cinco col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6" id="lifelaw_memberships">
                <div class="row">
                    <div class="titulo_izquierdo_cinco col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <img src="https://s3.amazonaws.com/cnp.com.co/CELULAR+MEMBRESIA-01.png">
                    </div>
                </div>
            </div>
            <div class="contenedor_derecho_cinco col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6">
                <div class="row">
                    <div class="titulo_derecho_cinco col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <h1>
                            Lifelaw Memberships
                        </h1>
                    </div>
                    <div class="descripcion_derecho_cinco col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <p>
                            Únete a la primera comunidad exclusiva de abogados en Colombia que resuelve todas tus
                            preguntas.
                        </p>
                    </div>
                    <div class="boton_contenedor_derecho_cinco col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <i class="btn boton_ir_membresia">
                            Conocer más
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
