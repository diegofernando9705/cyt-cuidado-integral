<style>
    .submit_button {
        margin-top: 10px;
        color: white;
        font-family: "OpenSans-Regular";
        background-color: #1f335e;
    }

    .store-profesional-client {
        padding-left: 10px;
        padding-right: 10px;
        text-align: left;
        font-size: 12px;
    }

    .store-profesional-client .form-group {
        margin-bottom: 10px;
    }

    #swal2-html-container {
        margin: 20px;
        overflow: hidden;
    }

    .contenedor_boton_continuar {
        text-align: center;
        margin-top: 10px;
    }

    .form-control {
        display: block;
        padding: 0.375rem 0.75rem;
        font-size: 12px !important;
        font-weight: 400;
        line-height: 1.5;
        color: #726D7B;
        background-color: #fff;
        background-clip: padding-box;
        appearance: none;
        border-radius: 4px;
        box-shadow: none;
        margin-top: 3px;
    }

    .form-control:focus {
        display: block;
        padding: 0.375rem 0.75rem;
        font-size: 12px !important;
        font-weight: 400;
        line-height: 1.5;
        color: #726D7B;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #002E66;
        appearance: none;
        border-radius: 8px;
        box-shadow: none;
        margin-top: 3px;
    }
</style>

<div class="row">
    <div class="container">
        <div class="formulario_registro col-12 col-sm-12 col-md-12 col-xl-12 col-lg-12">
            <form class="store-profesional-client" id="store-profesional-client" novalidate>
                @csrf
                @method('POST')

                <div class="row">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tarjeta_profesional"><b>(*) Tarjeta profesional</b></label> <br>
                            <input type="text" class="form-control" name="tarjeta_profesional"
                                id="tarjeta_profesional" required>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tipo_de_documento_profesional"><b>(*) Tipo de documento</b></label>
                            <br>
                            <select class="form-control" name="tipo_de_documento_profesional"
                                id="tipo_de_documento_profesional" required>
                                <option selected disabled></option>
                                @foreach ($documentType as $type)
                                    <option value="{{ $type->code }}">{{ $type->description }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="identificacion_profesional"><b>(*) Número de
                                    identificación</b></label>
                            <br>
                            <input type="number" class="form-control" name="identificacion_profesional"
                                id="identificacion_profesional" required>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="primer_nombre_profesional"><b>(*) Primer nombre</b></label> <br>
                            <input type="text" class="form-control" name="primer_nombre_profesional"
                                id="primer_nombre_profesional" required>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="segundo_nombre_profesional"><b>Segundo nombre</b></label> <br>
                            <input type="text" class="form-control" name="segundo_nombre_profesional"
                                id="segundo_nombre_profesional">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="primer_apellido_profesional"><b>(*) Primer apellido</b></label> <br>
                            <input type="text" class="form-control" name="primer_apellido_profesional"
                                id="primer_apellido_profesional" required>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="segundo_apellido_profesional"><b>Segundo apellido</b></label> <br>
                            <input type="text" class="form-control" name="segundo_apellido_profesional"
                                id="segundo_apellido_profesional">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="correo_profesional"><b>(*) Correo electrónico</b></label> <br>
                            <input type="text" class="form-control" name="correo_profesional" id="correo_profesional"
                                required>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="celular_profesional"><b>(*) Celular</b></label> <br>
                            <input type="tel" class="form-control" name="celular_profesional"
                                id="celular_profesional" required>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                        <div class="alert alert-info" style="display: none"></div>
                        <div class="alert alert-error" style="display: none"></div>
                    </div>

                    <div class="alert alert-warning">
                        Registre su usuario para entrar a la plataforma
                    </div>

                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="form-group">
                            <label for="correo_profesional_usuario"><b>(*) Correo electrónico usuario</b></label> <br>
                            <input type="text" class="form-control" name="correo_profesional_usuario"
                                id="correo_profesional_usuario" readonly>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password"><b>(*) Contraseña</b></label> <br>
                            <input type="password" class="form-control" name="password" id="password" required>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                        <div class="alert alert-info" style="display: none"></div>
                        <div class="alert alert-error" style="display: none"></div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password_confirmation"><b>(*) Repite la contraseña</b></label> <br>
                            <input type="password" class="form-control" name="password_confirmation"
                                id="password_confirmation" required>
                            <div class="invalid-feedback">Por favor, este campo es importante.</div>
                        </div>
                        <div class="alert alert-info" style="display: none"></div>
                        <div class="alert alert-error" style="display: none"></div>
                    </div>

                    <div class="boton_suscripcion">
                        <button type="submit"
                            class="btn submit_button col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
                            id="submit">Continuar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).on("keyup", "#correo_profesional", function() {
        $("#correo_profesional_usuario").val($(this).val());
    });


    $(function() {
        "use strict";

        const forms = document.querySelectorAll(".store-profesional-client");

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms).forEach((form) => {
            form.addEventListener(
                "submit",
                (event) => {
                    if (!form.checkValidity()) {
                        event.stopPropagation();
                    } else {
                        var formulario = $(".store-profesional-client").serialize();

                        document.getElementById("identificacion_profesional").disabled = true;
                        document.getElementById("tarjeta_profesional").disabled = true;
                        document.getElementById("tipo_de_documento_profesional").disabled = true;
                        document.getElementById("primer_nombre_profesional").disabled = true;
                        document.getElementById("segundo_nombre_profesional").disabled = true;
                        document.getElementById("primer_apellido_profesional").disabled = true;
                        document.getElementById("segundo_apellido_profesional").disabled = true;
                        document.getElementById("celular_profesional").disabled = true;
                        document.getElementById("correo_profesional").disabled = true;

                        document.getElementById("password").disabled = true;
                        document.getElementById("password_confirmation").disabled = true;

                        document.getElementById("submit").disabled = true;

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            type: "POST",
                            url: "{{ route('registerSubscriptor') }}",
                            data: formulario, // serializes the form's elements.
                            success: function(data) {
                                if (data) {
                                    location.reload();
                                }
                            },
                            error: function(err) {
                                errorHttp(err.status);

                                document.getElementById("identificacion_profesional")
                                    .disabled = false;
                                document.getElementById("tarjeta_profesional")
                                    .disabled = false;
                                document.getElementById("tipo_de_documento_profesional")
                                    .disabled = false;
                                document.getElementById("primer_nombre_profesional")
                                    .disabled = false;
                                document.getElementById("segundo_nombre_profesional")
                                    .disabled = false;
                                document.getElementById("primer_apellido_profesional")
                                    .disabled = false;
                                document.getElementById("segundo_apellido_profesional")
                                    .disabled = false;
                                document.getElementById("celular_profesional")
                                    .disabled = false;
                                document.getElementById("correo_profesional").disabled =
                                    false;

                                document.getElementById("password").disabled = false;
                                document.getElementById("password_confirmation")
                                    .disabled =
                                    false;

                                document.getElementById("submit").disabled = false;

                                if (err.status == 422) {
                                    $(".errores").hide();
                                    console.log(err.responseJSON);
                                    $("#success_message").fadeIn().html(err.responseJSON
                                        .message);
                                    console.warn(err.responseJSON.errors);
                                    $.each(err.responseJSON.errors, function(i, error) {
                                        var el = $(document).find('[name="' +
                                            i + '"]');
                                        el.after(
                                            $(
                                                '<span class="errores" style="color: red;">' +
                                                error[0] +
                                                "</span>"
                                            )
                                        );
                                    });
                                }
                            },
                        });
                        event.preventDefault();
                    }
                    event.preventDefault();
                    form.classList.add("was-validated");
                },
                false
            );
        });
    });
</script>
