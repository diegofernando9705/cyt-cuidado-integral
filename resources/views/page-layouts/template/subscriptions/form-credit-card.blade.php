<link href="{{ asset('page/assets/css/style-credit-card.css') }}" rel="stylesheet">

<div class="container">

    <div class="row">
        <form id="formulario-tarjeta" class="formulario-tarjeta" novalidate>
            @csrf
            @method('POST')
            <div class="container_customer col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                <div class="row">
                    <div class="titulo_customer col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                        <p>Información de la suscripción:</p>
                    </div>
                    <div class="data_suscripcion col-12 col-sm-10 col-md-10 col-lg-10 col-xl-10">
                        <div class="row">
                            <div class="form-group">
                                <input type="hidden" name="codigo_plan" id="codigo_plan" value="{{ $plan->code }}">
                                <b> {{ $plan->title }}</b>
                            </div>
                            <div class="form-group">
                                <ul>
                                    @foreach (explode(',', $plan->description) as $item)
                                        <li>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-check">
                                                <polyline points="20 6 9 17 4 12"></polyline>
                                            </svg> {{ $item }}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                            <div class="form-group valor_plan">
                                $ {{ number_format($plan->price) }}
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row">
                        <div class="titulo_payment col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <p>Información de pago:</p>
                        </div>
                        <div class="data_payment col-12 col-sm-10 col-md-10 col-lg-10 col-xl-10">
                            <div class="row">
                                <div class="contenedor_tarjeta">
                                    <div class="row">
                                        <!-- Tarjeta -->
                                        <div class="tarjeta col-12 col-sm-7 col-md-7 col-lg-7 col-xl-7" id="tarjeta">
                                            <div class="delantera">
                                                <div class="imagenes">
                                                    <div class="row">
                                                        <div class="logo-marca col" id="logo-marca">
                                                        </div>
                                                        <div class="img_chip col">
                                                            <img src="{{ asset('page/assets/img_targets_credit/chip-tarjeta.png') }}"
                                                                class="chip" alt="">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="datos">
                                                    <div class="grupo" id="numero">
                                                        <p class="label">Número Tarjeta</p>
                                                        <p class="numero">#### #### #### ####</p>
                                                    </div>
                                                    <div class="flexbox">
                                                        <div class="grupo" id="nombre">
                                                            <p class="label">Nombre Tarjeta</p>
                                                            <p class="nombre">Centro Nacional de Pruebas</p>
                                                        </div>
                                                        <div class="grupo" id="expiracion">
                                                            <p class="label">Expiracion</p>
                                                            <p class="expiracion">
                                                                <span class="mes">MM</span> / <span
                                                                    class="year">AA</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="trasera">
                                                <div class="barra-magnetica"></div>
                                                <div class="datos">
                                                    <div class="grupo" id="firma">
                                                        <p class="label">Firma</p>
                                                        <div class="firma">
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                    <div class="grupo" id="ccv">
                                                        <p class="label">CCV</p>
                                                        <p class="ccv"></p>
                                                    </div>
                                                </div>
                                                <p class="leyenda">Los datos ingresados se validarán directamente con
                                                    la pasarela de pago seleccionada
                                                </p>
                                                <a href="#" class="link-banco">www.cnp.com.co</a>
                                            </div>
                                        </div>

                                        <div class="formulario_tarjeta col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
                                            <!-- Formulario -->

                                            <div class="grupo form-group">
                                                <label for="inputNumero">Número Tarjeta</label>
                                                <input class="form-control" type="text" id="inputNumero"
                                                    maxlength="19" autocomplete="off" required>
                                                <div class="invalid-feedback">Por favor, este campo es importante.
                                                </div>

                                            </div>
                                            <div class="grupo form-group">
                                                <label for="inputNombre">Nombre</label>
                                                <input class="form-control" type="text" id="inputNombre"
                                                    maxlength="19" autocomplete="off" required>
                                                <div class="invalid-feedback">Por favor, este campo es importante.
                                                </div>
                                            </div>
                                            <div class="grupo-select grupoinputExpiracion">
                                                <label for="selectMes">Expiración</label>
                                                <div class="row">
                                                    <div class="form-group col">
                                                        <select name="mes" class="form-control" id="selectMes"
                                                            required>
                                                            <option disabled selected>Mes</option>
                                                        </select>
                                                        <div class="invalid-feedback">Por favor, este campo es
                                                            importante.</div>
                                                    </div>
                                                    <div class="form-group col">
                                                        <select name="year" class="form-control" id="selectYear"
                                                            required>
                                                            <option disabled selected>Año</option>
                                                        </select>
                                                        <div class="invalid-feedback">Por favor, este campo es
                                                            importante.</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="flexbox">

                                                <div class="grupo-select form-group col">
                                                    <label for="inputCCV">CCV</label>
                                                    <input type="text" class="form-control" id="inputCCV"
                                                        maxlength="3" required>
                                                    <div class="invalid-feedback">Por favor, este campo es
                                                        importante.
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 30px;">
                    <div class="titulo_customer col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">

                    </div>
                    <div class="data_suscripcion col-12 col-sm-10 col-md-10 col-lg-10 col-xl-10">
                        <div class="checkbox_politicas">
                            <label class="container_check">Autorizo a CNP (Centro Nacional de Pruebas) para que el
                                número de celular y el correo
                                electrónico, sean tratados para contactarme y/o enviarme la información relacionada con
                                la
                                solicitud del producto. Igualmente para que me consulten ante Operadores de Información
                                y
                                Riesgo con el fin de verificar mi información personal.
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>

                </div>

                <div class="row" style="margin-top: 30px;">
                    <div class="boton_siguiente col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <button class="btn btn-dark">Suscribirme a CNP (Centro Nacional de Pruebas)</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="{{ asset('page/assets/js/form-payment-subscription.js') }}" type=""></script>

<script>
    $(function() {
        'use strict';

        const forms = document.querySelectorAll('.formulario-tarjeta');

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms).forEach((form) => {
            form.addEventListener('submit', (event) => {

                if (!form.checkValidity()) {
                    event.stopPropagation();
                } else {

                    let timerInterval

                    Swal.fire({
                        title: 'Realizando la suscripción.',
                        html: 'Se paciente, ya estamos terminando...',
                        timerProgressBar: true,
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        didOpen: () => {
                            Swal.showLoading()
                            const b = Swal.getHtmlContainer().querySelector('b')
                        },
                        willClose: () => {
                            console.log("Llego");
                        }
                    });

                    $("body").addClass("body_inactive");
                    $(".contenedor_bloqueo").addClass("active_contenedor_bloqueo");

                    var paqueteDeDatos = new FormData();

                    paqueteDeDatos.append('_token', "{{ csrf_token() }}");

                    paqueteDeDatos.append('code_plan', $("#codigo_plan").prop('value'));
                    paqueteDeDatos.append('numero', $("#inputNumero").prop('value'));
                    paqueteDeDatos.append('nombre', $("#inputNombre").prop('value'));
                    paqueteDeDatos.append('mes', $("#selectMes").prop('value'));
                    paqueteDeDatos.append('year', $("#selectYear").prop('value'));
                    paqueteDeDatos.append('ccv', $("#inputCCV").prop('value'));

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
                        }
                    });

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('subscriptionPayment') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: paqueteDeDatos,
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            if (data == true) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Suscripción exitosa.',
                                    html: 'Su suscripción se ha realizaco con éxito',
                                    allowOutsideClick: false,
                                    showConfirmButton: false,
                                });

                                setTimeout(() => {
                                    window.location.href =
                                        "{{ route('dashboard-subscriber') }}";
                                }, 1000);

                            } else if (data === "CREDIT_CARD_INCORRECT") {
                                Swal.fire({
                                    title: "Error de información",
                                    text: "La tarjeta ingresada no pudo ser verificada.",
                                    footer: "El número de tarjeta ingresado no corresponde con ningún banco.",
                                    icon: "error"
                                });
                            }
                        },
                        error: function(err) {

                            errorHttp(err.status);

                            $("body").removeClass("body_inactive");
                            $(".contenedor_bloqueo").removeClass(
                                "active_contenedor_bloqueo");

                            if (err.status == 422) {
                                $(".errores").hide();
                                console.log(err.responseJSON);
                                $('#success_message').fadeIn().html(err.responseJSON
                                    .message);
                                console.warn(err.responseJSON.errors);
                                $.each(err.responseJSON.errors, function(i, error) {
                                    var el = $(document).find('[name="' +
                                        i + '"]');
                                    el.after($('<span class="errores" style="color: red;">' +
                                        error[0] + '</span>'));
                                });
                            }
                        },
                    });
                    event.preventDefault();
                }
                event.preventDefault();
                form.classList.add('was-validated');
            }, false);
        });
    });
</script>
