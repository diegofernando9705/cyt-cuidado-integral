<style>
    .imagen_membresia_modal {
        height: 250px
    }

    .imagen_membresia_modal img {
        width: 100% !important;
        height: auto;
    }

    .titulo_membresia_modal {
        font-size: 18px;
        color: black;
        font-weight: bold;
    }

    .titulo_seccion_plan {
        font-size: 18px;
        color: black;
        font-weight: bold;
    }


    .contenedor_data_planes {
        margin-top: 10px;
    }

    .data_plan {
        border-radius: 10px;
        background-image: linear-gradient(rgb(0 0 0 / 70%), rgb(0 0 0 / 59%)), url("https://s3.amazonaws.com/cnp.com.co/{{ $membership->imagen_membresia }}");
        background-size: 150% 150%;
        padding: 10px;
        background-repeat: no-repeat;
        background-position-y: -22px;
    }

    .data_plan:hover {
        cursor: pointer;
        transition: 0.5s;
        transform: translateY(-5px);
    }

    .title_plan_modal {
        color: white !important;
        text-align: center;
        line-height: 20px !important;
    }

    .info_general_data_modal {
        border-top: 1px solid white;
        font-size: 14px;
        color: white;
        margin-top: 10px;
        padding-top: 10px;
        padding-left: 10px;
        padding-left: 10px;
    }

    .value_membresia_modal {
        color: white;
        text-align: center;
        font-weight: bold;
        padding-left: 10px;
        padding-left: 10px;
    }
</style>

<div class="alert alert-info">
    Por favor, seleccione un plan de la lista.
</div>

<div class="info_membresias" style="margin-bottom: 4%; border-bottom:1px solid rgba(128, 128, 128, 0.445);">
    <div class="row">
        <div class="imagen_membresia_modal col-12 col-sm-4 col-md-4 col-xl-4 col-lg-4">
            <img src="https://s3.amazonaws.com/cnp.com.co/{{ $membership->image }}">
        </div>
        <div class="info_membresia_modal col-12 col-sm-8 col-md-8 col-xl-8 col-lg-8">
            <div class="titulo_membresia_modal col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                {{ $membership->title }}
            </div>
            <div class="descripciom_membresia col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                {!! $membership->description !!}
            </div>
        </div>
    </div>

</div>

<div class="container">
    <div class="row">
        <div class="info_planes">
            <div class="titulo_seccion_plan">
                Planes de la Membresia:
            </div>
            <div class="contenedor_data_planes col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="row">
                    @foreach ($membership->plans as $data_plan)
                        <a class="contenedor_plan col-6 col-md-3 col-sm-3 col-lg-3 col-xl-3"
                            data-code="{{ $data_plan->code }}">
                            <div class="detallado_plan_suscripcion">
                                <div class="valor_plan_suscripcion">
                                    ${{ number_format($data_plan->price) }}
                                </div>
                                <div class="imagen_plan_suscripcion"
                                    style="background-image: url('https://s3.amazonaws.com/cnp.com.co/{{ $data_plan->image }}');">
                                </div>
                                <div
                                    class="informacion_detallada_plan_suscripcion col-112 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="titulo_plan_suscripcion">
                                        <p> {{ $data_plan->title }} </p>
                                    </div>
                                    <div class="descripcion_plan_suscripcion">
                                        <ul>
                                            @foreach (explode(',', $data_plan->description) as $item)
                                                <li>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16"
                                                        height="16" viewBox="0 0 24 24" fill="none"
                                                        stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                        stroke-linejoin="round" class="feather feather-check">
                                                        <polyline points="20 6 9 17 4 12"></polyline>
                                                    </svg> {{ $item }}
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".contenedor_plan").click(function() {
        

        Swal.fire({
            title: 'Configurando método de pago.',
            html: 'Se paciente, este proceso no demorará...',
            timer: 1000,
            timerProgressBar: true,
            allowOutsideClick: false,
            showConfirmButton: false,
            didOpen: () => {
                Swal.showLoading()
                const b = Swal.getHtmlContainer().querySelector('b')
            }
        });
        
        var planSeleccionado = new FormData();

        planSeleccionado.append('codigo_plan', $(this).attr("data-code"));
        planSeleccionado.append('_token', "{{ csrf_token() }}");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: "/form-subscription/plans/membership/selected",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: planSeleccionado,
            processData: false,
            contentType: false,
            success: function(data) {
                $(".opcion_login").removeClass("active");
                $(".opcion_login").addClass("inactive");

                $(".opcion_plan_membresia").removeClass("active");
                $(".opcion_plan_membresia").addClass("inactive");

                $(".opcion_pago_membresia").removeClass("inactive");
                $(".opcion_pago_membresia").addClass("active");

                $("#contenedor_resultado_linea_tiempo").html(data);
            },
            error: function(err) {
                errorHttp(err.status);
            },
        });
    });
</script>
