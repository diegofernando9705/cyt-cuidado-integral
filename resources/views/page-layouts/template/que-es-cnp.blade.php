<div class="contenedor_segundario_principal col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
    <div class="container-xxl py-5 px-lg-5">
        <div class="row">
            <div class="contenedor_izquierdo_segundario col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6">
                <h1>
                    Estos son los beneﬁcios que obtienes al adquirir nuestros servicios
                </h1>
            </div>
            <div class="contenedor_derecho_segundario col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6">
                <video src="https://s3.amazonaws.com/cnp.com.co/VIDEO-CNP.mp4" controls></video>
            </div>
            <div class="contenedor_beneficios_segundario">
                <div class="row">
                    <div class="beneficio_uno col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                        <div class="contenedor_beneficio">
                            <div class="row">
                                <div class="imagen_beneficio col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <img src="https://s3.amazonaws.com/cnp.com.co/CALCULADORA-ACTUALIZADA-01.png"
                                        alt="">
                                </div>
                                <div class="descripcion_beneficio col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                    <p>
                                        Expertos en la precisión de pretensiones y liquidaciones.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="beneficio_dos col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                        <div class="contenedor_beneficio">
                            <div class="row">
                                <div class="imagen_beneficio col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <img src="https://s3.amazonaws.com/cnp.com.co/MALETIN+BENEFICIO-01.png"
                                        alt="">
                                </div>
                                <div class="descripcion_beneficio col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <p>
                                        Fortalecemos la estrategia probatoria para permitir ventajas competitivas
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="beneficio_tres col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                        <div class="contenedor_beneficio">
                            <div class="row">
                                <div class="imagen_beneficio col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <img src="https://s3.amazonaws.com/cnp.com.co/CAPACITACION+BENEFICIO-01.png"
                                        alt="">
                                </div>
                                <div class="descripcion_beneficio col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <p>
                                        Capacitamos de manera técnica, ﬁnanciera abogados y operadores de justicia.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="beneficio_cuatro col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                        <div class="contenedor_beneficio">
                            <div class="row">
                                <div class="imagen_beneficio col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <img src="https://s3.amazonaws.com/cnp.com.co/ATENCION-EFECTIVA-01.png"
                                        alt="">
                                </div>
                                <div class="descripcion_beneficio col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <p>
                                        Atención efectiva, oportuna en dictámenes y análisis que acompañen las demanda o
                                        su contestación.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="beneficio_cinco col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                        <div class="contenedor_beneficio">
                            <div class="row">
                                <div class="imagen_beneficio col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <img src="https://s3.amazonaws.com/cnp.com.co/METODOLOGIA+BENEFICIO-01.png"
                                        alt="">
                                </div>
                                <div class="descripcion_beneficio col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <p>
                                        Te apoyamos con metodología en la preparación de audiencias e interrogatorios.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="beneficio_seis col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                        <div class="contenedor_beneficio">
                            <div class="row">
                                <div class="imagen_beneficio col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <img src="https://s3.amazonaws.com/cnp.com.co/JURAMENTO+BENEFICIOS-01.png"
                                        alt="">
                                </div>
                                <div class="descripcion_beneficio col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <p>
                                        Soportamos el juramento estimatorio.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="clear: both"></div>
