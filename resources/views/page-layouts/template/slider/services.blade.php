<div class="contenedor_tercero_principal">
    <div class="container-xxl py-5 px-lg-5">
        <div class="wow fadeInUp titulo_seccion_servicios" data-wow-delay="0.1s">
            <h1>
                Nuestro portafolio de servicios
            </h1>
        </div>
        <div class="row" id="services-slider">
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

<script>
    $(document).ready(function() {
        var content = document.getElementById("services-slider");
        content.innerHTML = "<center><p style='color:#002e66 !important;'>Cargando servicios...</p></center>";

        $.ajax({
            type: "GET",
            url: "{{ route('services-all') }}",
            success: function(data) {
                var content = document.getElementById("services-slider");
                content.innerHTML = "<center>Cargando servicios...</center>";

                var owlCarousel = document.createElement('div');
                owlCarousel.setAttribute("class", "services-slider owl-carousel");

                setTimeout(() => {
                content.innerHTML = "";


                    for (var i = 0; i < data.length; i++) {
                    var newData = data[i];

                    var singleBoxNoticia = document.createElement('div');
                    singleBoxNoticia.setAttribute("class","single-box-servicio text-center")

                    var imageAreaNews = document.createElement("div");
                    imageAreaNews.setAttribute("class", "img-area-servicio");
                    imageAreaNews.setAttribute("style","background-image: url('https://s3.amazonaws.com/cnp.com.co/" +newData.image + "')");
                    singleBoxNoticia.appendChild(imageAreaNews);


                    var infoArea = document.createElement("div");
                    infoArea.setAttribute("class", "info-area");
                    singleBoxNoticia.appendChild(infoArea);


                    var titleNew = document.createElement("div");
                    titleNew.setAttribute("class", "titulo_slider_servicio");
                    titleNew.innerHTML = newData.title;
                    infoArea.appendChild(titleNew);

                    var descriptionNew = document.createElement("div");
                    descriptionNew.setAttribute("class", "descripcion_slider_servicio");
                    descriptionNew.innerHTML = newData.resena.slice(0, 150) + "...";
                    infoArea.appendChild(descriptionNew);

                    var link = document.createElement("a");
                    link.setAttribute("href", "{{ url('services') }}" + "/" + newData
                        .url);
                    link.innerHTML = "Leer más";
                    singleBoxNoticia.appendChild(link);

                    content.appendChild(owlCarousel);
                    owlCarousel.appendChild(singleBoxNoticia);
                }
                }, 1000);
            }
        });
    });

    setTimeout(() => {
        $('.services-slider').owlCarousel({
            loop: true,
            autoplay: true,
            autoplayTimeout: 5000,
            smartSpeed: 450,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 2
                },
                991: {
                    items: 4
                },
                1200: {
                    items: 5
                },
                1920: {
                    items: 5
                }
            }
        });
    }, 2000);
</script>
