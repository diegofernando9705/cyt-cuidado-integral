<div class="contenedor_slider_noticias">
    <div class="container-xxl py-5 px-lg-5" id="noticias">
        <div class="wow fadeInUp titulo_seccion_noticia" data-wow-delay="0.1s">
            <p>
                Noticias
            </p>
        </div>
        <div class="row" id="noticia-slider">

        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

<script>
    $(document).ready(function() {
        var contentData = document.getElementById("noticia-slider");
        contentData.innerHTML = "<center><p style='color:#002e66 !important;'>Cargando noticias...</p></center>";

        $.ajax({
            type: "GET",
            url: "{{ route('news-all') }}",
            success: function(data) {
                var content = document.getElementById("noticia-slider");
                content.innerHTML = "";

                var owlCarousel = document.createElement('div');
                owlCarousel.setAttribute("class", "noticia-slider owl-carousel");

                for (var i = 0; i < data.length; i++) {
                    var newData = data[i];

                    var singleBoxNoticia = document.createElement('div');
                    singleBoxNoticia.setAttribute("class","single-box-noticia text-center")

                    var imageAreaNews = document.createElement("div");
                    imageAreaNews.setAttribute("class", "img-area-noticia");
                    imageAreaNews.setAttribute("style","background-image: url('https://s3.amazonaws.com/cnp.com.co/" +newData.image + "')");
                    singleBoxNoticia.appendChild(imageAreaNews);


                    var infoArea = document.createElement("div");
                    infoArea.setAttribute("class", "info-area");
                    singleBoxNoticia.appendChild(infoArea);


                    var titleNew = document.createElement("div");
                    titleNew.setAttribute("class", "titulo_slider_noticia");
                    titleNew.innerHTML = newData.title;
                    infoArea.appendChild(titleNew);

                    var descriptionNew = document.createElement("div");
                    descriptionNew.setAttribute("class", "descripcion_slider_noticia");
                    descriptionNew.innerHTML = newData.description.slice(0, 150) + "...";
                    infoArea.appendChild(descriptionNew);

                    var link = document.createElement("a");
                    link.setAttribute("href", "{{ url('news') }}" + "/" + newData
                        .url);
                    link.innerHTML = "Leer más";
                    singleBoxNoticia.appendChild(link);

                    content.appendChild(owlCarousel);
                    owlCarousel.appendChild(singleBoxNoticia);
                }
            }
        });
    });

    setTimeout(() => {
        $('.noticia-slider').owlCarousel({
            loop: true,
            autoplay: true,
            autoplayTimeout: 5000,
            smartSpeed: 450,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 2
                },
                991: {
                    items: 4
                },
                1200: {
                    items: 5
                },
                1920: {
                    items: 5
                }
            }
        });
    }, 2000);
</script>
