<div class="container-xxl py-5 px-lg-5">

    <div class="contenedor_nuestros_clientes fadeInUp">
        <div class="wow fadeInUp titulo_seccion_clientes" data-wow-delay="0.1s">
            <p>
                Algunos de nuestros clientes
            </p>
        </div>
        <div class="row">
            <div class="container">
                <div class="clientes-slider owl-carousel">
                    <div class="single-box  unico-single-box-cliente text-center" style="background-image: url('')">
                        <img src="https://s3.amazonaws.com/cnp.com.co/Publico/fiduciaria_corficolombia.png"
                            alt="">
                    </div>

                    <div class="single-box unico-single-box-cliente text-center">
                        <img alt="" class="img-fluid move-animation"
                            src="https://s3.amazonaws.com/cnp.com.co/Publico/bancolombia.png">
                    </div>

                    <div class="single-box unico-single-box-cliente  text-center">
                        <img alt="" class="img-fluid move-animation"
                            src="https://s3.amazonaws.com/cnp.com.co/Publico/davivienda.png">
                    </div>

                    <div class="single-box unico-single-box-cliente text-center">

                        <img alt="" class="img-fluid move-animation"
                            src="https://s3.amazonaws.com/cnp.com.co/Publico/fortox.png">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.clientes-slider').owlCarousel({
            loop: true,
            autoplay: true,
            autoplayTimeout: 4000,
            smartSpeed: 300,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 2
                },
                991: {
                    items: 3
                },
                1200: {
                    items: 4
                },
                1920: {
                    items: 4
                }
            }
        });
        
</script>
