<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="container-xxl py-5 px-lg-5">
    <div class="row">
        <div class="contenedor_izquierdo_seis col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6">
            <div class="row">

                <div class="titulo_izquierdo_seis col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <h1>
                        Agenda con un experto
                    </h1>
                </div>
                <div class="descripcion_izquierdo_seis col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <p>
                        Programa una cita si necesitas acompañamiento o asesoría con un experto en algún tema específico
                    </p>
                </div>
            </div>
        </div>

        <div class="contenedor_derecho_seis col-12 col-sm-6 col-md-6 col-xl-6 col-lg-6">
            <div class="contenedor_formulario col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="calendar_table row">
                    <table class="table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th class="title_mmonth" colspan="7">
                                    <div class="row">
                                        <div
                                            class="contenido_header_calendario col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                                            <div
                                                class="calendario_izquierdo col-3 col-md-3 col-sm-3 col-lg-3 col-xl-3 col-lg-3">
                                                <span id="prev-month">
                                                    <i class="fa-solid fa-greater-than fa-rotate-180"></i>
                                                </span>
                                            </div>

                                            <div
                                                class="calendario_centro col-6 col-md-6 col-sm-6 col-lg-6 col-xl-6 col-lg-6">
                                                <span id="current-month"></span>
                                            </div>

                                            <div
                                                class="calendario_derecha col-3 col-md-3 col-sm-3 col-lg-3 col-xl-3 col-lg-3">
                                                <span id="next-month">
                                                    <i class="fa-solid fa-greater-than"></i>
                                                </span>
                                            </div>
                                        </div>

                                    </div>
                                </th>
                            </tr>
                            <tr class="fila_dias">
                                <th class="title_day_calendar">L</th>
                                <th class="title_day_calendar">M</th>
                                <th class="title_day_calendar">M</th>
                                <th class="title_day_calendar">J</th>
                                <th class="title_day_calendar">V</th>
                                <th class="title_day_calendar">S</th>
                                <th class="title_day_calendar">D</th>
                            </tr>
                        </thead>
                        <tbody id="calendar-body">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_calendar" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <center>
                    <h3>
                        Agenda tu cita
                    </h3>
                </center>
                <hr>
                <form class="form_agenda_cita" novalidate>
                    @csrf
                    @method('POST')

                    <div class="row">
                        <div class="form-group col">
                            <label for="">Fecha</label>
                            <input type="date" class="form-control" name="fecha_seleccionada"
                                id="fecha_seleccionada" required value="{{ date("Y-m-d") }}">
                        </div>

                        <div class="form-group col">
                            <label for="">Hora inicial</label>
                            <input type="time" class="form-control" name="hora_inicial" id="hora_inicial"
                                value="08:00">
                        </div>

                        <div class="form-group col">
                            <label for="">Hora final</label>
                            <input type="time" class="form-control" name="hora_final" id="hora_final" value="07:00">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="">(*) Nombre</label>
                            <input type="text" class="form-control" name="nombre" id="nombre" required>
                        </div>
                        <div class="form-group">
                            <label for="">(*) Apellidos</label>
                            <input type="text" class="form-control" name="apellidos" id="apellidos" required>
                        </div>
                        <div class="form-group">
                            <label for="">(*) Celular</label>
                            <input type="number" class="form-control" name="celular" id="celular" required>
                        </div>
                        <div class="form-group">
                            <label for="">(*) E-mail</label>
                            <input type="email" class="form-control" name="correo" id="correo" required>
                        </div>
                    </div>
                    <hr>
                    <center>
                        <button type="submit" class="btn btn-dark" id="button_agendar">Agendar cita</button>
                        <a class="btn btn-danger" id="cancelar_agenda">Cancelar cita</a>
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset('vendors/js/forms/validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset(mix('js/scripts/page/appointment-scheduling.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/page/calendar.js')) }}"></script>

<script>

    $(document).on("click", "#cancelar_agenda", function() {
        $("#modal_calendar").modal("hide");
    });

    // Actualizacion de servicio
</script>
