<html>

<head>
    <meta charset="utf-8">
    <title>@yield('title') - Centro Nacional de Pruebas</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

    <meta content="" name="keywords">
    <meta content="" name="description">
    <!-- Favicon -->
    <link href="https://s3.amazonaws.com/cnp.com.co/ICONO+CNP+WEB-03.png" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500&family=Jost:wght@500;600;700&display=swap"
        rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />

    <!-- Libraries Stylesheet -->
    <link href="{{ asset('page/assets/lib/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('page/assets/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('page/assets/lib/lightbox/css/lightbox.min.css') }}" rel="stylesheet">

    <!-- ICONS -->
    <link href="{{ asset('page/assets/iconos/css/fontawesome.css') }}" rel="stylesheet">
    <link href="{{ asset('page/assets/iconos/css/brands.css') }}" rel="stylesheet">
    <link href="{{ asset('page/assets/iconos/css/solid.css') }}" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('page/assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="{{ asset('page/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('page/assets/css/style_personalizados.css') }}" rel="stylesheet">
    <link href="{{ asset('page/assets/css/style_menu_navegacion.css') }}" rel="stylesheet">
    <link href="{{ asset('page/assets/css/style_panel_de_control.css') }}" rel="stylesheet">
    <link href="{{ asset('page/assets/css/style_pagina_inicio.css') }}" rel="stylesheet">

    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?render={{ config('app.recaptcha_site_key') }}"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

    @yield('style')

    <style>
        @font-face {
            font-family: "OpenSans-Bold";
            src: url("{{ asset('page/assets/fonts/OpenSans/OpenSans-Bold.ttf') }} ") format("truetype");
        }

        @font-face {
            font-family: "OpenSans-BoldItalic";
            src: url("{{ asset('page/assets/fonts/OpenSans/OpenSans-BoldItalic.ttf') }} ") format("truetype");
        }

        @font-face {
            font-family: "OpenSans-ExtraBold";
            src: url("{{ asset('page/assets/fonts/OpenSans/OpenSans-ExtraBold.ttf') }} ") format("truetype");
        }

        @font-face {
            font-family: "OpenSans-ExtraBoldItalic";
            src: url("{{ asset('page/assets/fonts/OpenSans/OpenSans-ExtraBoldItalic.ttf') }} ") format("truetype");
        }

        @font-face {
            font-family: "OpenSans-Italic";
            src: url("{{ asset('page/assets/fonts/OpenSans/OpenSans-Italic.ttf') }} ") format("truetype");
        }

        @font-face {
            font-family: "OpenSans-Light";
            src: url("{{ asset('page/assets/fonts/OpenSans/OpenSans-Light.ttf') }} ") format("truetype");
        }

        @font-face {
            font-family: "OpenSans-LightItalic";
            src: url("{{ asset('page/assets/fonts/OpenSans/OpenSans-LightItalic.ttf') }} ") format("truetype");
        }

        @font-face {
            font-family: "OpenSans-Regular";
            src: url("{{ asset('page/assets/fonts/OpenSans/OpenSans-Regular.ttf') }} ") format("truetype");
        }

        @font-face {
            font-family: "OpenSans-Semibold";
            src: url("{{ asset('page/assets/fonts/OpenSans/OpenSans-Semibold.ttf') }} ") format("truetype");
        }

        @font-face {
            font-family: "OpenSans-SemiboldItalic";
            src: url("{{ asset('page/assets/fonts/OpenSans/OpenSans-SemiboldItalic.ttf') }} ") format("truetype");
        }

        .active_page {
            border-bottom: 2px solid white;
        }


        .whatsapp img {
            z-index: 2;
            position: relative;
            width: 60px;
        }

        .whatsapp {
            position: fixed;
            bottom: 10px;
            left: 20px;
            z-index: 9999;
        }

        .whatsapp:before,
        .whatsapp:after {
            content: "";
            position: absolute;
            z-index: -1;
            top: 1px;
            left: 2px;
            width: 60px;
            height: 60px;
            background-color: #00e676;
            border-radius: 50%;
            opacity: 0;
            animation: ondas_whatsapp 1.7s infinite;
        }

        .whatsapp:before {
            animation-delay: 1s;
        }

        .whatsapp:after {
            animation-delay: 1.3s;
        }

        @keyframes ondas_whatsapp {
            0% {
                transform: scale(1);
            }

            15% {
                opacity: 1;
            }

            100% {
                opacity: 0;
                transform: scale(2.5);
            }
        }
    </style>

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-8YV8FW8EHN"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-8YV8FW8EHN');
    </script>
</head>

<body>
    {{-- 
    <a href="https://api.whatsapp.com/send/?phone=5731640071992&text=Hola%2C+deseo+mas+informacion+de+la+plataforma&type=phone_number&app_absent=0"
        target="_blank" class="whatsapp" title="Contactarme por WhatsApp">
        <img src="https://s3.amazonaws.com/softworldcolombia.com/WhatsApp_icon.png" alt="Contactar por WhatsApp">
    </a> --}}


    <!-- Spinner Start --
    <div id="spinner"
        class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
        <div class="spinner-grow text-primary" style="width: 3rem; height: 3rem;" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
     Spinner End -->


    <!-- Navbar & Hero Start -->
    <div class="row position-relative p-0">
        <!-- Header Section -->
        <header class="header" id="header">
            <nav class="navbar container">
                <div class="menu-icon" onclick="toggleMenu()">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <a href="#" class="brand">
                    <img src="https://s3.amazonaws.com/cnp.com.co/logo-white-cnp.png"
                        alt="Logo centro nacional de pruebas">
                </a>
                <div class="menu" id="menu">
                    <ul class="menu-inner">
                        <li class="menu-item"><a href="{{ route('index') }}"
                                class="menu-link {{ Route::currentRouteName() === 'index' ? ' active_page' : '' }}">Inicio</a>
                        </li>
                        <li class="menu-item"><a href="{{ route('about_us') }}"
                                class="menu-link {{ Route::currentRouteName() === 'about_us' ? ' active_page' : '' }}">Nosotros</a>
                        </li>
                        <li class="menu-item"><a href="{{ route('services') }}"
                                class="menu-link {{ Route::currentRouteName() === 'services' ? ' active_page' : '' }}">Servicios</a>
                        </li>
                        <li class="menu-item"><a href="{{ route('news') }}"
                                class="menu-link {{ Route::currentRouteName() === 'news' ? ' active_page' : '' }}"">Noticias</a>
                        </li>
                        <li class="menu-item"><a href="{{ route('frequent_questions') }}"
                                class="menu-link {{ Route::currentRouteName() === 'frequent_questions' ? ' active_page' : '' }}"">Preguntas
                                frecuentes</a></li>
                        <li class="menu-item"><a href="{{ route('contact') }}"
                                class="menu-link {{ Route::currentRouteName() === 'contact' ? ' active_page' : '' }}"">Contáctanos</a>
                        </li>
                    </ul>
                </div>
                @if (Auth::guest())
                    <div class="menu_boton abrir-modal-login">
                        <a href="#" class="opcion_entrar">Entrar</a>
                    </div>
                @elseif (Auth::user()->hasRole('subscriber'))
                    <div class="menu_boton">
                        <a class="opcion_entrar" href="{{ route('dashboard-subscriber') }}">Mi plataforma</a>
                    </div>
                @else
                    <div class="menu_boton">
                        <a class="opcion_entrar" href="{{ route('dashboard') }}">Mi plataforma</a>
                    </div>
                @endif
            </nav>
        </header>

        @yield('navegacion')
    </div>
    <!-- Navbar & Hero End -->

    @yield('container')

    <div style="clear: both"></div>

    <div class="contenedor_principal_footer col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="secciones_contenido_principal_footer col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="container-xxl py-5 px-lg-5">
                <div class="row">
                    <div class="opcion_uno_principal_footer col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                        <img src="https://s3.amazonaws.com/cnp.com.co/logo-white-cnp.png" alt="">
                        <div class="descripcion_cnp_principal_footer">

                        </div>
                    </div>

                    <div class="opcion_dos_principal_footer col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                        <div class="titulo_opcion_principal_footer">
                            QUIÉNES SOMOS
                        </div>
                        <ul>
                            <li><a href="{{ route('about_us') }}" style="color: white;">Nosotros</a></li>
                            <li><a href="{{ route('frequent_questions') }}" style="color: white;">Preguntas frecuentes</a></li>
                            <li><a href="{{ route('contact') }}" style="color: white;">Expertos</a></li>
                        </ul>
                    </div>

                    <div class="opcion_tres_principal_footer col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                        <div class="titulo_opcion_principal_footer">
                            SERVICIOS
                        </div>
                        <ul>
                            <li><a href="#cotizador_dictamen" style="color:white;">Cotizador de dictamen</a></li>
                            <li><a href="#agenca_con_experto" style="color:white;">Agenda con un experto</a></li>
                            <li><a href="#lifelaw_memberships" style="color:white;">Lifelaw Memberships</a></li>
                            <li><a href="#noticias" style="color:white;">Noticias</a></li>
                        </ul>
                    </div>

                    <div class="opcion_cuatro_principal_footer col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                        <div class="titulo_opcion_principal_footer">
                        </div>

                    </div>

                    <div class="opcion_cinco_principal_footer col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                        <div class="titulo_opcion_principal_footer">
                            CONTÁCTANOS
                        </div>
                        <ul>
                            <li>
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px"
                                    viewBox="0,0,256,256">
                                    <g transform="translate(44,44) scale(0.65625,0.65625)">
                                        <g fill-opacity="0" fill="#100e0e" fill-rule="nonzero" stroke="none"
                                            stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter"
                                            stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0"
                                            font-family="none" font-weight="none" font-size="none"
                                            text-anchor="none" style="mix-blend-mode: normal">
                                            <path d="M-67.04762,323.04762v-390.09524h390.09524v390.09524z"
                                                id="bgRectangle"></path>
                                        </g>
                                        <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1"
                                            stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10"
                                            stroke-dasharray="" stroke-dashoffset="0" font-family="none"
                                            font-weight="none" font-size="none" text-anchor="none"
                                            style="mix-blend-mode: normal">
                                            <path
                                                d="M128,323.04762c-107.72183,0 -195.04762,-87.32579 -195.04762,-195.04762v0c0,-107.72183 87.32579,-195.04762 195.04762,-195.04762v0c107.72183,0 195.04762,87.32579 195.04762,195.04762v0c0,107.72183 -87.32579,195.04762 -195.04762,195.04762z"
                                                id="shape"></path>
                                        </g>
                                        <g fill="#000000" fill-rule="nonzero" stroke="none" stroke-width="1"
                                            stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10"
                                            stroke-dasharray="" stroke-dashoffset="0" font-family="none"
                                            font-weight="none" font-size="none" text-anchor="none"
                                            style="mix-blend-mode: normal">
                                            <g transform="scale(4,4)">
                                                <path
                                                    d="M57,10h-50c-3.86421,0.0043 -6.9957,3.13579 -7,7v30c0.0043,3.86421 3.13579,6.9957 7,7h50c3.86421,-0.0043 6.9957,-3.13579 7,-7v-30c-0.0043,-3.86421 -3.13579,-6.9957 -7,-7zM59.6763,48.2124c-0.35492,0.42306 -0.98558,0.47835 -1.4087,0.1235l-19.115,-16.0286l-6.51,5.4588c-0.37169,0.31173 -0.91351,0.31173 -1.2852,0l-6.51,-5.4588l-19.115,16.0286c-0.27297,0.23594 -0.65187,0.30653 -0.99149,0.18474c-0.33962,-0.1218 -0.5873,-0.4171 -0.64812,-0.77274c-0.06082,-0.35564 0.07466,-0.71647 0.35451,-0.9442l18.8438,-15.8014l-17.8839,-14.996c-0.41498,-0.35701 -0.46578,-0.98132 -0.11399,-1.40072c0.35179,-0.41941 0.97542,-0.47801 1.39919,-0.13148l25.3076,21.2202l25.3076,-21.22c0.42377,-0.34653 1.04739,-0.28793 1.39919,0.13148c0.35179,0.41941 0.30099,1.04372 -0.11399,1.40072l-17.8839,14.9958l18.8438,15.8014c0.42309,0.35489 0.47843,0.98555 0.1236,1.4087z">
                                                </path>
                                            </g>
                                        </g>
                                    </g>
                                </svg> contacto@cnp.com.co
                            </li>
                            <li>
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px"
                                    viewBox="0,0,256,256">
                                    <g transform="">
                                        <g fill-opacity="0" fill="#dddddd" fill-rule="nonzero" stroke="none"
                                            stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter"
                                            stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0"
                                            font-family="none" font-weight="none" font-size="none"
                                            text-anchor="none" style="mix-blend-mode: normal">
                                            <path d="M0,256v-256h256v256z" id="bgRectangle"></path>
                                        </g>
                                        <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1"
                                            stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10"
                                            stroke-dasharray="" stroke-dashoffset="0" font-family="none"
                                            font-weight="none" font-size="none" text-anchor="none"
                                            style="mix-blend-mode: normal">
                                            <path
                                                d="M128,256c-70.69245,0 -128,-57.30755 -128,-128v0c0,-70.69245 57.30755,-128 128,-128v0c70.69245,0 128,57.30755 128,128v0c0,70.69245 -57.30755,128 -128,128z"
                                                id="shape"></path>
                                        </g>
                                        <g fill="#000000" fill-rule="nonzero" stroke="none" stroke-width="1"
                                            stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10"
                                            stroke-dasharray="" stroke-dashoffset="0" font-family="none"
                                            font-weight="none" font-size="none" text-anchor="none"
                                            style="mix-blend-mode: normal">
                                            <g transform="scale(10.66667,10.66667)">
                                                <path
                                                    d="M20.023,15.488c-0.63,-0.015 -1.423,-0.052 -1.924,-0.14c-0.542,-0.096 -1.194,-0.282 -1.653,-0.425c-0.36,-0.112 -0.752,-0.013 -1.019,0.253l-2.217,2.204c-1.532,-0.807 -2.759,-1.747 -3.798,-2.792c-1.045,-1.039 -1.985,-2.266 -2.792,-3.798l2.204,-2.218c0.266,-0.267 0.365,-0.659 0.253,-1.019c-0.142,-0.458 -0.329,-1.11 -0.424,-1.652c-0.089,-0.501 -0.125,-1.294 -0.141,-1.924c-0.013,-0.545 -0.457,-0.977 -1.002,-0.977h-3.51c-0.438,0 -1,0.328 -1,1c0,4.539 1.84,8.874 4.966,12.034c3.16,3.126 7.495,4.966 12.034,4.966c0.672,0 1,-0.562 1,-1v-3.51c0,-0.545 -0.432,-0.989 -0.977,-1.002z">
                                                </path>
                                            </g>
                                        </g>
                                    </g>
                                </svg> +57 316 4007 1992
                            </li>
                            <li>
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px"
                                    viewBox="0,0,256,256">
                                    <g fill-opacity="0" fill="#dddddd" fill-rule="nonzero" stroke="none"
                                        stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter"
                                        stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0"
                                        font-family="none" font-weight="none" font-size="none" text-anchor="none"
                                        style="mix-blend-mode: normal">
                                        <path d="M0,256v-256h256v256z" id="bgRectangle"></path>
                                    </g>
                                    <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1"
                                        stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10"
                                        stroke-dasharray="" stroke-dashoffset="0" font-family="none"
                                        font-weight="none" font-size="none" text-anchor="none"
                                        style="mix-blend-mode: normal">
                                        <path
                                            d="M128,256c-70.69245,0 -128,-57.30755 -128,-128v0c0,-70.69245 57.30755,-128 128,-128v0c70.69245,0 128,57.30755 128,128v0c0,70.69245 -57.30755,128 -128,128z"
                                            id="shape"></path>
                                    </g>
                                    <g fill="#000000" fill-rule="nonzero" stroke="none" stroke-width="1"
                                        stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10"
                                        stroke-dasharray="" stroke-dashoffset="0" font-family="none"
                                        font-weight="none" font-size="none" text-anchor="none"
                                        style="mix-blend-mode: normal">
                                        <g transform="scale(5.12,5.12)">
                                            <path
                                                d="M25,1c-8.82031,0 -16,7.17969 -16,16c0,14.11328 14.62891,30.94531 15.25,31.65625c0.19141,0.21875 0.46094,0.34375 0.75,0.34375c0.30859,-0.01953 0.55859,-0.125 0.75,-0.34375c0.62109,-0.72266 15.25,-17.84375 15.25,-31.65625c0,-8.82031 -7.17969,-16 -16,-16zM25,12c3.3125,0 6,2.6875 6,6c0,3.3125 -2.6875,6 -6,6c-3.3125,0 -6,-2.6875 -6,-6c0,-3.3125 2.6875,-6 6,-6z">
                                            </path>
                                        </g>
                                    </g>
                                </svg> Cali -Valle del cauca
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright_principal_footer">
            <a href="https://softworldcolombia.com/">
                Copyright © 2023 Centro Nacional de Pruebas All rights reserved. Design and Developed by
                SoftWorld Colombia </a>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body" id="content-login">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade " id="membership_modal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="titulo_modal_membresia col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <h2>
                            Membresías Centro Nacional de Pruebas
                        </h2>
                    </div>
                    <div class="row" id="body_membership_modal"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- JavaScript Libraries -->

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('page/assets/lib/easing/easing.min.js') }}"></script>
    <script src="{{ asset('page/assets/lib/waypoints/waypoints.min.js') }}"></script>
    <script src="{{ asset('page/assets/lib/counterup/counterup.min.js') }}"></script>
    <script src="{{ asset('page/assets/lib/owlcarousel/owl.carousel.min.js') }}"></script>
    <!-- Template Javascript -->
    <script src="{{ asset('page/assets/js/main.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('page/assets/js/code_personalizado.js') }}"></script>
    <script src="{{ asset('page/assets/js/errorPlatfotm.js') }}"></script>



    <script>
        $(document).ready(function() {
            console.log('%cTe damos la bienvenida a Centro Nacional de Pruebas',
                'font-weight:bold; font-size: 24px; color: #002e66;');
            console.log(
                '%cEsta función del navegador está pensada para desarrolladores. Si alguien te indicó que copiaras y pegaras algo aquí para habilitar una función de CNP o para "hackear" la cuenta de alguien, se trata de un fraude. Si lo haces, esta persona podrá acceder a tu cuenta.',
                'font-size: 20px; color: black; margin-bottom:30px;');
            console.log(
                '%cProgramado y desarrollado por: Softworld Colombia SAS. Conoce más en: https://softworldcolombia.com/',
                'font-size: 16px; color: #002e66;');
        });

        (function() {
            $('.hamburger-menu').on('click', function() {
                $('.bar').toggleClass('animate');
                $('.contenedor_menu_navegacion').toggleClass('inactive');
                $('.text').toggleClass('inactive');
                $('.icon-container').toggleClass('inactive');
                $(".contenedor_acciones_plataforma").toggleClass('active');
            })
        })();

        function toggleMenu() {
            var menu = document.querySelector('.menu');
            var menuIcon = document.querySelector('.menu-icon');
            menu.classList.toggle('open');
            menuIcon.classList.toggle('open');
        }
    </script>

    <script>
        $(document).on("click", ".abrir-modal-login", function() {
            $("#modalLogin").modal("show");
            $.ajax({
                type: 'GET',
                url: '{{ route('login-form') }}',
                success: function(data) {
                    $("#content-login").html(data);
                },
                error: function(error) {
                    console.log(error);
                },
            });
        });

        $(document).on("click", ".reset_password", function() {
            $.ajax({
                type: 'GET',
                url: '{{ route('reset-form') }}',
                success: function(data) {
                    $("#content-login").html(data);
                },
                error: function(error) {
                    console.log(error);
                },
            });
        });
    </script>


    <script>
        $('.encabezado-slider').owlCarousel({
            loop: true,
            nav: false,
            autoplay: true,
            autoplayTimeout: 5000,
            smartSpeed: 450,
            margin: 0,
            autoHeight: true,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 1
                },
                991: {
                    items: 1
                },
                1200: {
                    items: 1
                },
                1920: {
                    items: 1
                }
            }
        });

        /* funciones membership */

        $(document).on("click", ".boton_ir_membresia", function() {

            animationLoading('', 'Cargando membresias...');


            $.ajax({
                type: 'GET',
                url: '/membership',
                success: function(data) {


                    setTimeout(() => {

                        $(document).off('focusin.modal');
                        $.fn.modal.Constructor.prototype._enforceFocus = function() {};

                        const containerClaimReqModal = document.getElementById(
                            "membership_modal");
                        const claimReqModal = new bootstrap.Modal(containerClaimReqModal, {
                            focus: false
                        });

                        var code = $(this).attr("data-code");

                        $("#membership_modal").modal("show");

                        var content = document.getElementById("body_membership_modal");

                        Swal.close();

                        for (var i = 0; i < data.length; i++) {
                            var membership = data[i];

                            var contenedorModalMembresia = document.createElement('div');
                            contenedorModalMembresia.setAttribute("class",
                                "contenedor_modal_membresia col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3"
                            );
                            contenedorModalMembresia.setAttribute("style",
                                "margin-bottom: 20px;");

                            var contenedorMembresiaUnico = document.createElement('div');
                            contenedorMembresiaUnico.setAttribute("class",
                                "contenedor_membresia_unico col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3"
                            );

                            var contenedorMembresiaUnico = document.createElement('div');
                            contenedorMembresiaUnico.setAttribute("class",
                                "contenedor_membresia_unico col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
                            );

                            var imagenMembresiaModal = document.createElement('div');
                            imagenMembresiaModal.setAttribute("class",
                                "imagen_membresia_modal col-12 col-sm-12 col-md-12 col-lg-12"
                            );
                            imagenMembresiaModal.setAttribute("style",
                                "background-image: url('https://s3.amazonaws.com/cnp.com.co/" +
                                membership.image + "')");
                            contenedorMembresiaUnico.appendChild(imagenMembresiaModal);

                            var informacionMembresiaModal = document.createElement('div');
                            informacionMembresiaModal.setAttribute("class",
                                "informacion_membresia_modal col-12 col-sm-12 col-md-12 col-lg-12"
                            );
                            contenedorMembresiaUnico.appendChild(informacionMembresiaModal);

                            var tituloMembresiaModal = document.createElement('div');
                            tituloMembresiaModal.setAttribute("class",
                                "titulo_membresia_modal");
                            tituloMembresiaModal.innerHTML = "<p>" + membership.title + "</p>";
                            informacionMembresiaModal.appendChild(tituloMembresiaModal);

                            var descripcionMembresiaModal = document.createElement('div');
                            descripcionMembresiaModal.setAttribute("class",
                                "descripcion_membresia");
                            descripcionMembresiaModal.innerHTML = membership.description;
                            informacionMembresiaModal.appendChild(descripcionMembresiaModal);

                            var divFormMemberhis = document.createElement('div');
                            divFormMemberhis.setAttribute('class',
                                "formulario_membresia col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
                            );
                            informacionMembresiaModal.appendChild(divFormMemberhis);

                            var formMembership = document.createElement('form');
                            formMembership.setAttribute("method", "POST")
                            formMembership.setAttribute("action",
                                "{{ route('form-subscription') }}")
                            divFormMemberhis.appendChild(formMembership);

                            var inputHidden = document.createElement("input");
                            inputHidden.setAttribute("type", "hidden");
                            inputHidden.setAttribute("name", "codigo_membresia");
                            inputHidden.setAttribute("id", "codigo_membresia");
                            inputHidden.setAttribute("value", membership
                                .code_platform_subscription);
                            inputHidden.setAttribute("required", true);
                            formMembership.appendChild(inputHidden);

                            var divButton = document.createElement("div");
                            divButton.setAttribute("class",
                                "botones_membership col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
                            );
                            formMembership.appendChild(divButton);

                            var inputToken = document.createElement("input");
                            inputToken.setAttribute("type", "hidden");
                            inputToken.setAttribute("name", "_token");
                            inputToken.setAttribute("value", "{{ csrf_token() }}");
                            formMembership.appendChild(inputToken);

                            var buttonForm = document.createElement("button");
                            buttonForm.setAttribute("type", "submit");
                            buttonForm.setAttribute("class", "btn info_btn_membreship");
                            buttonForm.innerHTML = "Seleccionar plan";
                            divButton.appendChild(buttonForm);


                            contenedorModalMembresia.appendChild(contenedorMembresiaUnico);
                            content.appendChild(contenedorModalMembresia);
                        }
                    }, 1000);
                },
                error: function(error) {
                    $("#modal-todo").modal("hide");
                    errorHttp(error.status);
                },
            });
        });

        /* fin funciones membership */
    </script>

    @yield('codigo')
</body>

</html>
