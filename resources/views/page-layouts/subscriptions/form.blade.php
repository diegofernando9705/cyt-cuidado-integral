@extends('page-layouts.template.app')

@section('title', 'Suscribete a nuestra plataforma')

@section('style')
    <link href="{{ asset('page/assets/css/style_pagina_suscripcion.css') }}" rel="stylesheet">
@endsection

@section('navegacion')
    <div class="container-xxl py-5 bg-primary hero-header">
    </div>
@endsection

@section('container')

    <!-- Service Start -->
    <div class="container-xxl py-5">
        <div class="container px-lg-5">
            <div class="wow fadeInUp" data-wow-delay="0.1s">
                <h2 class="text-center mb-5">
                    Suscríbete a nuestra plataforma - {{ $membership->title }}
                </h2>
            </div>
            <div class="row g-4 fadeInUp" data-wow-delay="0.1s">
                <div class="contenedor_linea_tiempo col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="menu_navegacion_linea_tiempo">
                        <div class="row">
                            <div class="opcion_login active col">1. Inicia sesión o registrate</div>
                            <div class="opcion_plan_membresia inactive col">2. Selecciona el plan</div>
                            <div class="opcion_pago_membresia inactive col">3. Completa el pago</div>
                        </div>
                    </div>
                </div>

                <div class="contenedor_resultado_linea_tiempo col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
                    id="contenedor_resultado_linea_tiempo">
                    @if (auth()->check())
                    @else
                        <div class="alert alert-info">
                            Completa el registro para continuar con la suscripción
                        </div>
                        @include('page-layouts.template.subscriptions.register-form')
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection


@if (auth()->check())
    @section('codigo')
        <script>
            var timerInterval;

            Swal.fire({
                icon: 'warning',
                title: 'Usted ya inició sesión.',
                html: 'Redireccionando al siguiente paso...',
                timer: 20000,
                timerProgressBar: true,
                allowOutsideClick: false,
                showConfirmButton: false,
                didOpen: () => {
                    Swal.showLoading()
                    const b = Swal.getHtmlContainer().querySelector('b')
                }
            });

            setTimeout(() => {
                Swal.close();

                var paqueteMembresia = new FormData();
                paqueteMembresia.append('_token', "{{ csrf_token() }}");
                paqueteMembresia.append('codigo_membresia', "{{ $membership->code }}");

                $.ajax({
                    type: "POST",
                    url: "{{ route('plansMembership') }}",
                    headers: {
                        'X-CSRF-TOKEN': $(
                            'meta[name="csrf-token"]'
                        ).attr('content')
                    },
                    data: paqueteMembresia,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        $(".opcion_login").removeClass(
                            "active");
                        $(".opcion_login")
                            .addClass(
                                "inactive");

                        $(".opcion_plan_membresia")
                            .removeClass(
                                "inactive");
                        $(".opcion_plan_membresia")
                            .addClass("active");

                        $("#contenedor_resultado_linea_tiempo").html(data);
                    },
                    error: function(err) {
                        errorHttp(err.status)
                    },
                });
            }, 1000);
        </script>
    @endsection
@else
    <script src="{{ asset('page/assets/js/form-register-subscription.js') }}"></script>
@endif
