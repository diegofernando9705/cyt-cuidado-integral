<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'avatar' => 'logo-cnp.png',
        	'name' => 'Administrador',
        	'email' => 'info@softworldcolombia.com',
        	'password' => Hash::make('qwerty*'),
            'condicion' => '1',
        ])->assignRole('administrador');
        

    }
}
