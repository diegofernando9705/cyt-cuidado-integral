<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\softworld_modulos as Modulos;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create(['name' => 'administrador']);

        Permission::create(['name' => 'usuarios-access'])->assignRole($role);
        Permission::create(['name' => 'usuarios-create'])->assignRole($role);
        Permission::create(['name' => 'usuarios-read'])->assignRole($role);
        Permission::create(['name' => 'usuarios-update'])->assignRole($role);
        Permission::create(['name' => 'usuarios-delete'])->assignRole($role);
        Permission::create(['name' => 'usuarios-list'])->assignRole($role);

        Permission::create(['name' => 'roles-access'])->assignRole($role);
        Permission::create(['name' => 'roles-create'])->assignRole($role);
        Permission::create(['name' => 'roles-read'])->assignRole($role);
        Permission::create(['name' => 'roles-update'])->assignRole($role);
        Permission::create(['name' => 'roles-delete'])->assignRole($role);
        Permission::create(['name' => 'roles-list'])->assignRole($role);

        Modulos::create(['nombre_modulo' => 'usuarios', 'descripcion_modulo' => 'Modulo de los usuarios', 'estado_modulo' => '1']);
        Modulos::create(['nombre_modulo' => 'roles', 'descripcion_modulo' => 'Modulo de los roles', 'estado_modulo' => '1']);
    }
}
