<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_subscriptions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("user_id");
            $table->string("plan_id", 50);
            $table->dateTime("start_date");
            $table->dateTime("end_date");
            $table->boolean("renewal")->default(1);
            $table->dateTime("normal_end_date")->nullable();
            $table->dateTime("date_renewal_cancelled")->nullable();
            $table->longText("code_subscription_pay")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_subscriptions');
    }
}
