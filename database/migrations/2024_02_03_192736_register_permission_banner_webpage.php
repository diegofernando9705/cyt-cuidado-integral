<?php

use App\softworld_modulos;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RegisterPermissionBannerWebpage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = Role::where(['name' => 'administrador'])->get();

        Permission::create(['name' => 'paginaweb-banner'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-banner-access'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-banner-inicio'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-banner-nosotros'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-banner-servicios'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-banner-noticias'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-banner-preguntasfrecuentes'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-banner-contactanos'])->assignRole($role);    


        Permission::create(['name' => 'paginaweb-nosotros-access'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-nosotros-list'])->assignRole($role);
        
        Permission::create(['name' => 'paginaweb-nosotros-create'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-nosotros-read'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-nosotros-update'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-nosotros-delete'])->assignRole($role);

        softworld_modulos::create(['nombre_modulo' => 'paginaweb-nosotros', 'descripcion_modulo' => 'Modulo de pagina web nosotros', 'estado_modulo' => '1']);


        Permission::create(['name' => 'paginaweb-servicios-access'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-servicios-list'])->assignRole($role);

        Permission::create(['name' => 'paginaweb-servicios-create'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-servicios-read'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-servicios-update'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-servicios-delete'])->assignRole($role);

        softworld_modulos::create(['nombre_modulo' => 'paginaweb-servicios', 'descripcion_modulo' => 'Modulo de pagina web noticias', 'estado_modulo' => '1']);
   
        Permission::create(['name' => 'paginaweb-categorias-access'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-categorias-list'])->assignRole($role);
        
        Permission::create(['name' => 'paginaweb-categorias-create'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-categorias-read'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-categorias-update'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-categorias-delete'])->assignRole($role);

        softworld_modulos::create(['nombre_modulo' => 'paginaweb-categorias', 'descripcion_modulo' => 'Modulo de pagina web categorias', 'estado_modulo' => '1']);
   


        Permission::create(['name' => 'paginaweb-noticias-access'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-noticias-list'])->assignRole($role);

        Permission::create(['name' => 'paginaweb-noticias-create'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-noticias-read'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-noticias-update'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-noticias-delete'])->assignRole($role);

        softworld_modulos::create(['nombre_modulo' => 'paginaweb-noticias', 'descripcion_modulo' => 'Modulo de pagina web noticias', 'estado_modulo' => '1']);




        Permission::create(['name' => 'paginaweb-preguntasfrecuentes-access'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-preguntasfrecuentes-list'])->assignRole($role);
        
        Permission::create(['name' => 'paginaweb-preguntasfrecuentes-create'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-preguntasfrecuentes-read'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-preguntasfrecuentes-update'])->assignRole($role);
        Permission::create(['name' => 'paginaweb-preguntasfrecuentes-delete'])->assignRole($role);

        softworld_modulos::create(['nombre_modulo' => 'paginaweb-preguntasfrecuentes', 'descripcion_modulo' => 'Modulo de pagina web preguntas frecuentes', 'estado_modulo' => '1']);
 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
