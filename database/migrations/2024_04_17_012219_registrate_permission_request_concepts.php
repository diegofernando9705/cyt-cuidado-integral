<?php

use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\softworld_modulos as Modulos;

class RegistratePermissionRequestConcepts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = Role::where(['id' => '1'])->get();
        
        Permission::create(['name' => 'solicitud-conceptos-access'])->assignRole($role);
        Permission::create(['name' => 'solicitud-conceptos-list'])->assignRole($role);

        Permission::create(['name' => 'solicitud-conceptos-create'])->assignRole($role);
        Permission::create(['name' => 'solicitud-conceptos-read'])->assignRole($role);
        Permission::create(['name' => 'solicitud-conceptos-update'])->assignRole($role);
        Permission::create(['name' => 'solicitud-conceptos-delete'])->assignRole($role);

        Modulos::create(['nombre_modulo' => 'solicitud-conceptos', 'descripcion_modulo' => 'Modulo de indicadores economicos', 'estado_modulo' => '1']);
 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
