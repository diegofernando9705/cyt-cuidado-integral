<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStartTaskToSoftworldTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('softworld_tasks', function (Blueprint $table) {
            $table->dateTime('start_task')->after('description')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('softworld_tasks', function (Blueprint $table) {
            $table->dropColumn('start_task');
        });
    }
}
