<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_plans', function (Blueprint $table) {
            $table->string("code", 50)->primary();
            $table->string("image");
            $table->string("title");
            $table->longText("description");
            $table->string("platform_code")->unique()->nullable();
            $table->date("start_date");
            $table->date("end_date");
            $table->integer("price")->nullable();
            $table->unsignedBigInteger("status");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_plans');
    }
}
