<?php

use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\softworld_modulos as Modulos;

class RegistratePermissionEconomicIndicator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = Role::where(['id' => '1'])->get();

        Permission::create(['name' => 'indicadores-economicos-access'])->assignRole($role);
        Permission::create(['name' => 'indicadores-economicos-list'])->assignRole($role);

        Permission::create(['name' => 'indicadores-economicos-create'])->assignRole($role);
        Permission::create(['name' => 'indicadores-economicos-read'])->assignRole($role);
        Permission::create(['name' => 'indicadores-economicos-update'])->assignRole($role);
        Permission::create(['name' => 'indicadores-economicos-delete'])->assignRole($role);

        Permission::create(['name' => 'indicadores-economicos-import'])->assignRole($role);

        Modulos::create(['nombre_modulo' => 'indicadores-economicos', 'descripcion_modulo' => 'Modulo de indicadores economicos', 'estado_modulo' => '1']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
