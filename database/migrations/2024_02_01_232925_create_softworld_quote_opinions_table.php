<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldQuoteOpinionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_quote_opinions', function (Blueprint $table) {
            $table->id();
            $table->string('nombres_apellidos');
            $table->string('correo_electronico');
            $table->string('telefono_celular');
            $table->string('ciudad');
            $table->string('area_derecho');
            $table->string('valor_pretensiones');
            $table->string('tipo_cliente');
            $table->string('opcion_pretensiones');
            $table->string('experiencia_litigio');
            $table->string('tribunal');
            $table->string('volumen_informacion');
            $table->string('servicios_adicionales');
            $table->string('area_dictamen');
            $table->string('resultado_cotizacion');
            $table->unsignedBigInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_quote_opinions');
    }
}
