<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldAwardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_awards', function (Blueprint $table) {
            $table->string("award_code", 50)->primary();
            $table->string("award_image")->nullable();
            $table->string("award_title");
            $table->longText("award_description");
            $table->integer("litipoint_quantity")->nullable();
            $table->integer("availability")->nullable();
            $table->dateTime("start_date");
            $table->dateTime("end_date")->nullable();
            $table->integer("status");
            $table->dateTime('date_deleted')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_awards');
    }
}
