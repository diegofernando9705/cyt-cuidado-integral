<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\softworld_modulos;

class RegistratePermissionCustomerBanner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = Role::where(['name' => 'administrador'])->get();

        Permission::create(['name' => 'banner-publicidad-cliente-access'])->assignRole($role);
        Permission::create(['name' => 'banner-publicidad-cliente-list'])->assignRole($role);

        Permission::create(['name' => 'banner-publicidad-cliente-create'])->assignRole($role);
        Permission::create(['name' => 'banner-publicidad-cliente-read'])->assignRole($role);
        Permission::create(['name' => 'banner-publicidad-cliente-update'])->assignRole($role);
        Permission::create(['name' => 'banner-publicidad-cliente-delete'])->assignRole($role);
        
        softworld_modulos::create(['nombre_modulo' => 'banner-publicidad-cliente', 'descripcion_modulo' => 'Modulo de agendamiento citas', 'estado_modulo' => '1']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
