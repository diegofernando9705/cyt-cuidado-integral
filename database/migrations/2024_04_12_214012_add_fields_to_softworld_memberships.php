<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToSoftworldMemberships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('softworld_memberships', function (Blueprint $table) {
            $table->dateTime('date_deleted')->after('status')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->after('date_deleted')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('softworld_memberships', function (Blueprint $table) {
            $table->dropColumn('date_deleted');
            $table->dropColumn('deleted_by');
        });
    }
}
