<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_people', function (Blueprint $table) {
            $table->bigInteger("id")->primary();
            $table->unsignedBigInteger("document_type");
            $table->string("professional_card");
            $table->string("first_name");
            $table->string("second_name")->nullable();
            $table->string("first_last_name");
            $table->string("second_last_name")->nullable();
            $table->string("cellphone")->nullable();
            $table->string("email")->unique();
            $table->string("profession_id", 50)->nullable();
            $table->enum("type", ['subscriber', 'user']);
            $table->string("user")->nullable();
            $table->unsignedBigInteger("status");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_people');
    }
}
