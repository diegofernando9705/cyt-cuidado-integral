<?php

use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\softworld_modulos as Modulos;

class RegistratePermissionConcepts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = Role::where(['id' => '1'])->get();

        Permission::create(['name' => 'conceptos-access'])->assignRole($role);
        Permission::create(['name' => 'conceptos-list'])->assignRole($role);

        Permission::create(['name' => 'conceptos-create'])->assignRole($role);
        Permission::create(['name' => 'conceptos-read'])->assignRole($role);
        Permission::create(['name' => 'conceptos-update'])->assignRole($role);
        Permission::create(['name' => 'conceptos-delete'])->assignRole($role);

        Modulos::create(['nombre_modulo' => 'conceptos', 'descripcion_modulo' => 'Modulo de indicadores economicos', 'estado_modulo' => '1']);
 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
