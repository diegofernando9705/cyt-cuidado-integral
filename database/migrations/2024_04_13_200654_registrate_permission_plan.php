<?php

use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\softworld_modulos as Modulos;

class RegistratePermissionPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = Role::where(['id' => '1'])->get();

        Permission::create(['name' => 'planes-access'])->assignRole($role);
        Permission::create(['name' => 'planes-list'])->assignRole($role);

        Permission::create(['name' => 'planes-create'])->assignRole($role);
        Permission::create(['name' => 'planes-read'])->assignRole($role);
        Permission::create(['name' => 'planes-update'])->assignRole($role);
        Permission::create(['name' => 'planes-delete'])->assignRole($role);

        Modulos::create(['nombre_modulo' => 'planes', 'descripcion_modulo' => 'Modulo de indicadores economicos', 'estado_modulo' => '1']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
