<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\softworld_modulos;

class RegistratePermissionPeople extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = Role::where(['name' => 'administrador'])->get();

        Permission::create(['name' => 'personas-access'])->assignRole($role);
        Permission::create(['name' => 'personas-list'])->assignRole($role);

        Permission::create(['name' => 'personas-create'])->assignRole($role);
        Permission::create(['name' => 'personas-read'])->assignRole($role);
        Permission::create(['name' => 'personas-update'])->assignRole($role);
        Permission::create(['name' => 'personas-delete'])->assignRole($role);
        
        softworld_modulos::create(['nombre_modulo' => 'personas', 'descripcion_modulo' => 'Modulo de agendamiento citas', 'estado_modulo' => '1']);
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
