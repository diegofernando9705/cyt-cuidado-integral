<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;

class RegistratePermissionMembershipModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => 'asignar-membresia-noticia']);
        Permission::create(['name' => 'asignar-membresia-premios']);
        Permission::create(['name' => 'asignar-membresia-banner-publicidad']);
        Permission::create(['name' => 'asignar-membresia-calculadoras']);
        Permission::create(['name' => 'asignar-membresia-conceptos']);
        Permission::create(['name' => 'asignar-membresia-cursos']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
