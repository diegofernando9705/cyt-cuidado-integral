<?php

use App\Models\SoftworldStatusPlatforms;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RegistrateStatusOfCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        SoftworldStatusPlatforms::create([
            'description' => 'Activo',
            'module' => 'courses',
        ]);

        SoftworldStatusPlatforms::create([
            'description' => 'En progreso',
            'module' => 'courses',
        ]);

        SoftworldStatusPlatforms::create([
            'description' => 'En pausa',
            'module' => 'courses',
        ]);

        SoftworldStatusPlatforms::create([
            'description' => 'Finalizado',
            'module' => 'courses',
        ]);

        SoftworldStatusPlatforms::create([
            'description' => 'Cancelado',
            'module' => 'courses',
        ]);

        SoftworldStatusPlatforms::create([
            'description' => 'Borrador',
            'module' => 'courses',
        ]);

        SoftworldStatusPlatforms::create([
            'description' => 'Eliminado',
            'module' => 'courses',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
