<?php

use App\Models\SoftworldStatusPlatforms;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldStatusPlatformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_status_platforms', function (Blueprint $table) {
            $table->id();
            $table->string("description");
            $table->string("module")->nullable()->default("general");
            $table->timestamps();
        });

        SoftworldStatusPlatforms::create([
            'description' => "Activo",
            'module' => "general"
        ]);

        SoftworldStatusPlatforms::create([
            'description' => "Inactivo",
            'module' => "general"
        ]);

        SoftworldStatusPlatforms::create([
            'description' => "Eliminado",
            'module' => "general"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_status_platforms');
    }
}
