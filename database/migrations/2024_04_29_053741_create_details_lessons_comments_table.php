<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailsLessonsCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details_lessons_comments', function (Blueprint $table) {
            $table->id();
            $table->string('lesson_id');
            $table->unsignedBigInteger('user_id');
            $table->longText('comment');
            $table->dateTime('date_deleted')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details_lessons_comments');
    }
}
