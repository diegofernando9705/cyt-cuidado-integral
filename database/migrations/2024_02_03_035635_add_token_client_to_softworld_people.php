<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTokenClientToSoftworldPeople extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('softworld_people', function (Blueprint $table) {
            $table->string('token_client', 200)->after('user')->unique()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('softworld_people', function (Blueprint $table) {
            $table->dropColumn('token_client');
        });
    }
}
