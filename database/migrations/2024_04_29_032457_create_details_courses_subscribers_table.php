<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailsCoursesSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details_courses_subscribers', function (Blueprint $table) {
            $table->id();
            $table->string('course_id');
            $table->unsignedBigInteger('user_id');
            $table->decimal('qualification', 5, 1);
            $table->dateTime('start_date');
            $table->dateTime('end_date')->nullable()->default(NULL);
            $table->dateTime('cancelled_date')->nullable()->default(NULL);
            $table->dateTime('updated_date')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details_courses_subscribers');
    }
}
