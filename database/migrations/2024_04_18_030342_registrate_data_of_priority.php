<?php

use App\Models\SoftworldPriority;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RegistrateDataOfPriority extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        SoftworldPriority::create([
            'description' => 'Alta',
            'module' => 'task'
        ]);

        SoftworldPriority::create([
            'description' => 'Media',
            'module' => 'task'
        ]);

        SoftworldPriority::create([
            'description' => 'Baja',
            'module' => 'task'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
