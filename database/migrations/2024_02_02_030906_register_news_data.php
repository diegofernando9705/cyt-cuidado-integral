<?php

use App\Models\SoftworldNews;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RegisterNewsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        SoftworldNews::create([
            'image' => "noticias/2NPwkSsACDIcigcj5P92brysynUemxxigcALbUaO.jpg",
            'title' => "Pagos por Nequi y Daviplata si permiten reconocimiento de costos y deducciones",
            'summary' => "<p>La Direccion de Gestion Juridica de la DIAN afirmo que los depositos de bajo monto como Nequi y Daviplata, no hacen parte de los medios de pag</p>",
            'description' => "<p>La Direccion de Gestion Juridica de la DIAN afirmo que los depositos de bajo monto como Nequi y Daviplata, no hacen parte de los medios de pago expresamente permitido por el art 771-5 del Estatuto Tributario, para efectos del reconocimiento fiscal de costos, deduciones, pasivos e impuestos descontables, ya que no corresponden a ninguno de los listados por esta disposicion (en particular los depositos en cuentas bancarias)</p>",
            'url' => "Pagos_por_Nequi_y_Daviplata_si_permiten_reconocimiento_de_costos_y_deducciones_",
            'type' => "subscribers",
            'register_date' => now(),
            'date_end' => date("2024-05-15"),
            'user_creator_id' => 1,
            'status' => 1,
        ]);

        SoftworldNews::create([
            'image' => "noticias/2NPwkSsACDIcigcj5P92brysynUemxxigcALbUaO.jpg",
            'title' => "Pagos por Nequi y Daviplata si permiten reconocimiento de costos y deducciones",
            'summary' => "<p>La Direccion de Gestion Juridica de la DIAN afirmo que los depositos de bajo monto como Nequi y Daviplata, no hacen parte de los medios de pag</p>",
            'description' => "<p>La Direccion de Gestion Juridica de la DIAN afirmo que los depositos de bajo monto como Nequi y Daviplata, no hacen parte de los medios de pago expresamente permitido por el art 771-5 del Estatuto Tributario, para efectos del reconocimiento fiscal de costos, deduciones, pasivos e impuestos descontables, ya que no corresponden a ninguno de los listados por esta disposicion (en particular los depositos en cuentas bancarias)</p>",
            'url' => "Pagos_por_Nequi_y_Daviplata_si_permiten_reconocimiento_de_costos_y_deducciones_",
            'type' => "subscribers",
            'register_date' => now(),
            'date_end' => date("2024-05-15"),
            'user_creator_id' => 1,
            'status' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
