<?php

use App\Models\SoftworldStatusPlatforms;
use App\softworld_modulos;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RegisterPermissionAppointmentSheduling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = Role::where(['name' => 'administrador'])->get();

        Permission::create(['name' => 'agendamiento-citas-access'])->assignRole($role);
        Permission::create(['name' => 'agendamiento-citas-list'])->assignRole($role);

        Permission::create(['name' => 'agendamiento-citas-create'])->assignRole($role);
        Permission::create(['name' => 'agendamiento-citas-read'])->assignRole($role);
        Permission::create(['name' => 'agendamiento-citas-update'])->assignRole($role);
        Permission::create(['name' => 'agendamiento-citas-delete'])->assignRole($role);

        softworld_modulos::create(['nombre_modulo' => 'agendamiento-citas', 'descripcion_modulo' => 'Modulo de agendamiento citas', 'estado_modulo' => '1']);
    
        SoftworldStatusPlatforms::create([
            'description' => "Agendada",
            'module' => "agendamiento-citas"
        ]);

        SoftworldStatusPlatforms::create([
            'description' => "En espera por agendar",
            'module' => "agendamiento-citas"
        ]);

        SoftworldStatusPlatforms::create([
            'description' => "No agendada",
            'module' => "agendamiento-citas"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
