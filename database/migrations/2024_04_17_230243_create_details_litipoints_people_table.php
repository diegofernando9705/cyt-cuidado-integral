<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailsLitipointsPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details_litipoints_people', function (Blueprint $table) {
            $table->id();
            $table->string('description_point');
            $table->unsignedBigInteger('people_id');
            $table->dateTime('register_date');
            $table->double('point');
            $table->dateTime('date_use')->nullable()->default(NULL);
            $table->dateTime('date_deleted')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details_litipoints_people');
    }
}
