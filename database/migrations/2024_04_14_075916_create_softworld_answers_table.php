<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_answers', function (Blueprint $table) {
            $table->id();
            $table->longText('description');
            $table->string('created_by');
            $table->dateTime('date_deleted')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_answers');
    }
}
