<?php

use App\Models\SoftworldAboutUs;
use App\Models\SoftworldDescriptionsHeadersPages;
use App\Models\SoftworldTextoBanners;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RegistrateDataTypeHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        SoftworldDescriptionsHeadersPages::create([
            'type' => 'texto',
            'slug' => 'inicio',
        ]);

        SoftworldDescriptionsHeadersPages::create([
            'type' => 'texto',
            'slug' => 'nosotros',
        ]);

        SoftworldDescriptionsHeadersPages::create([
            'type' => 'texto',
            'slug' => 'servicios',
        ]);

        SoftworldDescriptionsHeadersPages::create([
            'type' => 'texto',
            'slug' => 'noticias',
        ]);

        SoftworldDescriptionsHeadersPages::create([
            'type' => 'texto',
            'slug' => 'preguntas_frecuentes',
        ]);

        SoftworldTextoBanners::create([
            'title' => "Inicio",
            'description' => "Inicio",
            'section' => "Inicio",
            'css' => "padding: 5rem 0; background: rgb(2, 0, 36); background: linear-gradient(0deg, rgba(2, 0, 36, 1) 0%, rgba(9, 9, 121, 1) 35%, rgba(10, 48, 56, 1) 100%);",
            'page' => 1,
            'status' => 1,
        ]);

        SoftworldTextoBanners::create([
            'title' => "Nosotros",
            'description' => "nosotros",
            'section' => "nosotros",
            'css' => "padding: 5rem 0; background: rgb(2, 0, 36); background: linear-gradient(0deg, rgba(2, 0, 36, 1) 0%, rgba(9, 9, 121, 1) 35%, rgba(10, 48, 56, 1) 100%);",
            'page' => 2,
            'status' => 1,
        ]);

        SoftworldTextoBanners::create([
            'title' => "servicios",
            'description' => "servicios",
            'section' => "servicios",
            'css' => "padding: 5rem 0; background: rgb(2, 0, 36); background: linear-gradient(0deg, rgba(2, 0, 36, 1) 0%, rgba(9, 9, 121, 1) 35%, rgba(10, 48, 56, 1) 100%);",
            'page' => 3,
            'status' => 1,
        ]);

        SoftworldTextoBanners::create([
            'title' => "noticias",
            'description' => "noticias",
            'section' => "noticias",
            'css' => "padding: 5rem 0; background: rgb(2, 0, 36); background: linear-gradient(0deg, rgba(2, 0, 36, 1) 0%, rgba(9, 9, 121, 1) 35%, rgba(10, 48, 56, 1) 100%);",
            'page' => 4,
            'status' => 1,
        ]);

        SoftworldTextoBanners::create([
            'title' => "preguntas_frecuentes",
            'description' => "preguntas frecuentes",
            'section' => "preguntas frecuentes",
            'css' => "padding: 5rem 0; background: rgb(2, 0, 36); background: linear-gradient(0deg, rgba(2, 0, 36, 1) 0%, rgba(9, 9, 121, 1) 35%, rgba(10, 48, 56, 1) 100%);",
            'page' => 5,
            'status' => 1,
        ]);

        SoftworldAboutUs::create([
            'title' => "title",
            'description' => "description",
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
