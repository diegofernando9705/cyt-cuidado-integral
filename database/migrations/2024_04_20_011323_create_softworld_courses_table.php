<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_courses', function (Blueprint $table) {
            $table->string("code")->primary();
            $table->string("title");
            $table->string("summary");
            $table->longText("description")->nullable();
            $table->string("cover_image")->nullable()->default(NULL);
            $table->string("video_image")->nullable()->default(NULL);
            $table->dateTime("start_date")->default(now());
            $table->dateTime("end_date")->nullable()->default(NULL);
            $table->string("category_id");
            $table->unsignedBigInteger("instructor_id");
            $table->unsignedBigInteger("level");
            $table->integer("student_limit")->nullable()->default(NULL);
            $table->unsignedBigInteger("status");
            $table->dateTime('date_deleted')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_courses');
    }
}
