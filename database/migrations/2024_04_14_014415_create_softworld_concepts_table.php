<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldConceptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_concepts', function (Blueprint $table) {
            $table->string('code', 50);
            $table->string('title');
            $table->longText('description');
            $table->date('register_date')->default(now());
            $table->date('end_date')->nullable();
            $table->enum('privacy', ['private', 'public'])->default('public');
            $table->unsignedBigInteger('application_concept_id')->nullable();
            $table->unsignedBigInteger('status');
            $table->unsignedBigInteger('created_by');
            $table->dateTime('date_deleted')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_concepts');
    }
}
