<?php

use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\softworld_modulos as Modulos;

class RegistratePermissionCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = Role::where(['id' => '1'])->get();
        
        Permission::create(['name' => 'cursos-access'])->assignRole($role);
        Permission::create(['name' => 'cursos-list'])->assignRole($role);

        Permission::create(['name' => 'cursos-create'])->assignRole($role);
        Permission::create(['name' => 'cursos-read'])->assignRole($role);
        Permission::create(['name' => 'cursos-update'])->assignRole($role);
        Permission::create(['name' => 'cursos-delete'])->assignRole($role);

        Modulos::create(['nombre_modulo' => 'cursos', 'descripcion_modulo' => 'Modulo de indicadores economicos', 'estado_modulo' => '1']);
 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
