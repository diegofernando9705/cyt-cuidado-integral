<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldModulesLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_modules_lessons', function (Blueprint $table) {
            $table->string("code");
            $table->string("image")->nullable()->default(NULL);
            $table->string("title");
            $table->longText("summary")->nullable()->default(NULL);
            $table->enum("type", ['pdf', 'mp4', 'image'])->default('pdf');
            $table->unsignedBigInteger("status");
            $table->dateTime('date_deleted')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_modules_lessons');
    }
}
