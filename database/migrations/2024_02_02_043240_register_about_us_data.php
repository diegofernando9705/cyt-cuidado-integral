<?php

use App\Models\SoftworldAboutUs;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RegisterAboutUsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        SoftworldAboutUs::create([
            "title" => "Sobre Nosotros",
            "description" => '<p><span style="color:#000000"><span style="font-size:48px"><span style="font-family:Verdana,Geneva,sans-serif"><strong>NUESTRA PROPUESTA DE VALOR</strong></span></span></span></p>
        
            <p><span style="font-size:18px"><span style="font-family:Georgia,serif">Con nuestro equipo de expertos en temas econ&oacute;micos, auditor&iacute;a, contable y financieros, hemos dise&ntilde;ado un portafolio de servicios de alta categor&iacute;a para apoyar la labor de abogados y jueces en la elaboraci&oacute;n, controversia y valoraci&oacute;n de la prueba financiera, a instancias de los procesos judiciales.</span></span></p>
            
            <p>&nbsp;</p>
            
            <p><span style="color:#000000"><span style="font-size:48px"><span style="font-family:Verdana,Geneva,sans-serif"><strong>NECESIDADES DE ABOGADOS Y JUECES</strong></span></span></span></p>
            
            <ol>
                <li><span style="font-size:18px"><span style="font-family:Georgia,serif">Precisi&oacute;n en pretensiones y liquidaciones.</span></span></li>
                <li><span style="font-size:18px"><span style="font-family:Georgia,serif">Atenci&oacute;n efectiva y oportuna de dict&aacute;menes y an&aacute;lisis que acompa&ntilde;en las demandas o su contestaci&oacute;n.</span></span></li>
                <li><span style="font-size:18px"><span style="font-family:Georgia,serif">Valoraci&oacute;n del juramento estimatorio.</span></span></li>
                <li><span style="font-size:18px"><span style="font-family:Georgia,serif">Preparaci&oacute;n de audiencias e interrogatorios.</span></span></li>
                <li><span style="font-size:18px"><span style="font-family:Georgia,serif">Definici&oacute;n de estrategias probatorias que permitan ventajas competitivas.</span></span></li>
                <li><span style="font-size:18px"><span style="font-family:Georgia,serif">Estructuraci&oacute;n de argumentos para persuadir.</span></span></li>
                <li><span style="font-size:18px"><span style="font-family:Georgia,serif">Capacitaci&oacute;n t&eacute;cnica y financiera de abogados.</span></span></li>
            </ol>
            
            <p>&nbsp;</p>',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
