<?php

use App\Models\SoftworldCalculators;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RegistrateDataCalculators extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        SoftworldCalculators::create([
            "code" => "calculo_ir_im",
            "image" => "calculadoras/img_calculo_ir_im.png",
            "title" => "CÁLCULO IR Y IM",
            "description" => "CÁLCULO IR Y IM",
            "status" => 1,
        ]);

        SoftworldCalculators::create([
            "code" => "calculadora_indicadores",
            "image" => "calculadoras/img_indicadores.png",
            "title" => "INDICADORES",
            "description" => "INDICADORES",
            "status" => 1,
        ]);

        SoftworldCalculators::create([
            "code" => "calculadora_redenominacion",
            "image" => "calculadoras/img_redenominacion.png",
            "title" => "REDENOMINACIÓN",
            "description" => "REDENOMINACIÓN",
            "status" => 1,
        ]);

        SoftworldCalculators::create([
            "code" => "calculadora_liquidaciones",
            "image" => "calculadoras/img_liquidaciones.png",
            "title" => "LIQUIDACIONES",
            "description" => "LIQUIDACIONES",
            "status" => 1,
        ]);

        SoftworldCalculators::create([
            "code" => "calculadora_indexacion",
            "image" => "calculadoras/img_indexacion.png",
            "title" => "INDEXACIÓN",
            "description" => "INDEXACIÓN",
            "status" => 1,
        ]);

        SoftworldCalculators::create([
            "code" => "sancion_pretensiones",
            "image" => "calculadoras/img_sanciones.png",
            "title" => "SANCIÓN DE PRETENSIONES",
            "description" => "SANCIÓN DE PRETENSIONES",
            "status" => 1,
        ]);

        SoftworldCalculators::create([
            "code" => "conversiones_tasas",
            "image" => "calculadoras/img_conversiones.png",
            "title" => "CONVERSIONES DE TASAS",
            "description" => "CONVERSIONES",
            "status" => 1,
        ]);

        SoftworldCalculators::create([
            "code" => "juramento_estimatorio",
            "image" => "calculadoras/img_juramento.png",
            "title" => "JURAMENTO ESTIMATORIO",
            "description" => "JURAMENTO",
            "status" => 1,
        ]);

        SoftworldCalculators::create([
            "code" => "calculo_interes_rem",
            "image" => "calculadoras/img_calculo_rem.png",
            "title" => "CÁLCULO DE INTERÉS REM",
            "description" => "CÁLCULO DE INTERÉS REM",
            "status" => 1,
        ]);

        SoftworldCalculators::create([
            "code" => "calculo_perjuicios",
            "image" => "calculadoras/img_calculo_perjuicios.png",
            "title" => "CÁLCULO DE PERJUICIOS",
            "description" => "CÁLCULO DE PERJUICIOS",
            "status" => 1,
        ]);

        SoftworldCalculators::create([
            "code" => "calculo_valor_presente",
            "image" => "calculadoras/img_calculo_rem.png",
            "title" => "CÁLCULO VALOR PRESENTE",
            "description" => "CÁLCULO VALOR PRESENTE",
            "status" => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
