<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Spatie\Permission\Models\Permission;
use App\softworld_modulos as Modulos;
use Spatie\Permission\Models\Role;


class RegistratePermissionCotizador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = Role::where(['id' => '1'])->get();

        Permission::create(['name' => 'cotizadordictamen-access'])->assignRole($role);
        Permission::create(['name' => 'cotizadordictamen-list'])->assignRole($role);
        
        Permission::create(['name' => 'cotizadordictamen-create'])->assignRole($role);
        Permission::create(['name' => 'cotizadordictamen-read'])->assignRole($role);
        Permission::create(['name' => 'cotizadordictamen-update'])->assignRole($role);
        Permission::create(['name' => 'cotizadordictamen-delete'])->assignRole($role);
        
        Modulos::create(['nombre_modulo' => 'cotizadordictamen', 'descripcion_modulo' => 'Modulo de cotizadoress', 'estado_modulo' => '1']);
   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
