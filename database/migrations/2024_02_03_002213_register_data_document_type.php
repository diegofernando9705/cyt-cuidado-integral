<?php

use App\Models\SoftworldDocumentTypes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RegisterDataDocumentType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        SoftworldDocumentTypes::create([
            'code' => 11,
            'description' => "Registro civil de nacimiento",
            'status' => 1,
        ]);

        SoftworldDocumentTypes::create([
            'code' => 12,
            'description' => "Tarjeta de identidad ",
            'status' => 1,
        ]);

        SoftworldDocumentTypes::create([
            'code' => 13,
            'description' => "Cédula de ciudadanía",
            'status' => 1,
        ]);

        SoftworldDocumentTypes::create([
            'code' => 21,
            'description' => "Tarjeta de extranjería",
            'status' => 1,
        ]);

        SoftworldDocumentTypes::create([
            'code' => 22,
            'description' => "Cédula de extranjería",
            'status' => 1,
        ]);

        SoftworldDocumentTypes::create([
            'code' => 31,
            'description' => "NIT",
            'status' => 1,
        ]);

        SoftworldDocumentTypes::create([
            'code' => 41,
            'description' => "Pasaporte",
            'status' => 1,
        ]);

        SoftworldDocumentTypes::create([
            'code' => 42,
            'description' => "Tipo de documento extranjero",
            'status' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
