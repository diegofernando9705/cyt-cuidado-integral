<?php

use App\Models\SoftworldMembership;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RegisterMembershipData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        SoftworldMembership::create([
            'code' => "CNP_MEMBERSHIP_START",
            'image' => "membresias/eOKTw7FMR8S2n9fOiKxR4f3LUyYw3VJNBiBYkVr0.jpg",
            'title' => "Membresía START",
            'description' => "<p>2 Cursos de la app, M&oacute;dulo de vivienda ilimitado, Diario CNP, Calculadoras limitadas, Conceptos - tipo limitado (CT), Descuento en Concepto a la medida (CM)</p>",
            'start_date' => NOW(),
            'code_platform_subscription' => "CNP_MEMBERSHIP_START",
            'status' => 1,
        ]);

        SoftworldMembership::create([
            'code' => "CNP_MEMBERSHIP_JUNIOR",
            'image' => "membresias/XruYx7OzzojQv76zCUjYWU90VhIyJfHJrVUYnwFe.jpg",
            'title' => "Membresía JUNIOR",
            'description' => "<p>5 Liquidaciones, 1 Capacitaci&oacute;n, M&oacute;dulo vivienda ilimitado, 5 Consultas, Calculadoras uso limitado, Conceptos - tipo limitado (CT), Descuento en Conceptos - medida (CM)</p>",
            'start_date' => NOW(),
            'code_platform_subscription' => "CNP_MEMBERSHIP_JUNIOR",
            'status' => 1,
        ]);

        SoftworldMembership::create([
            'code' => "CNP_MEMBERSHIP_SENIOR",
            'image' => "membresias/bBpEj4he9tYhs8Gk7XlosNbj0skjjolVvbAtz4AR.png",
            'title' => "Membresía SENIOR",
            'description' => "<p>11 Liquidaciones, 2 Capacitaciones, M&oacute;dulo vivienda ilimitado, 8 Consultas, Calculadoras uso ilimitado, Conceptos - tipo ilimitado (CT), Descuento en Conceptos - medida (CM)</p>",
            'start_date' => NOW(),
            'code_platform_subscription' => "CNP_MEMBERSHIP_SENIOR",
            'status' => 1,
        ]);

        SoftworldMembership::create([
            'code' => "CNP_MEMBERSHIP_MAGISTER",
            'image' => "membresias/hoXmVzEC8JTPjpS6Lvg2LVjJpuYiweiwlQUK5Dym.png",
            'title' => "Membresía MAGISTER",
            'description' => "<p>20 Liquidaciones, Capacitaciones ilimitadas, 1 Seminario con descuento, M&oacute;dulo vivienda ilimitado, 12 Consultas, Calculadoras uso ilimitado, Conceptos - tipo ilimitado (CT), Descuento en Conceptos - medida (CM)</p>",
            'start_date' => NOW(),
            'code_platform_subscription' => "CNP_MEMBERSHIP_MAGISTER",
            'status' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
