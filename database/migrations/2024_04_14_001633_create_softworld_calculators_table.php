<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldCalculatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_calculators', function (Blueprint $table) {
            $table->string('code')->primary();
            $table->string('image');
            $table->string('title');
            $table->longText('description')->nullable();
            $table->unsignedBigInteger('status');
            $table->dateTime('date_deleted')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_calculators');
    }
}
