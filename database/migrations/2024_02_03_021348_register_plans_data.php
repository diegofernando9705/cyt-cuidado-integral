<?php

use App\Models\DetailsPlansMembership;
use App\Models\SoftworldPlans;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RegisterPlansData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        date_default_timezone_set("America/Bogota");
        $start_date = date("Y-m-d");
        $end_date = "2040-12-31";

        /* Plan free */
        SoftworldPlans::create([
            'code' => "PLAN_START",
            'image' => "membresias/eOKTw7FMR8S2n9fOiKxR4f3LUyYw3VJNBiBYkVr0.jpg",
            'title' => "Plan Start",
            'description' => "2 Cursos de la app, Módulo de vivienda ilimitado, Diario CNP, Calculadoras limitadas, Conceptos - tipo limitado (CT), Descuento en Concepto a la medida (CM)",
            'start_date' => $start_date,
            'end_date' => $end_date,
            'status' => 1,
        ]);

        DetailsPlansMembership::create([
            'plan_id' => 'PLAN_START',
            'membership_id' => 'CNP_MEMBERSHIP_START',
        ]);


        /* Regitro del plan membresia Junior */
        SoftworldPlans::create([
            'code' => "PLAN_MENSUAL_JUNIOR",
            'image' => "membresias/XruYx7OzzojQv76zCUjYWU90VhIyJfHJrVUYnwFe.jpg",
            'title' => "Plan Mensual Junior",
            'description' => "5 Liquidaciones, 1 Capacitación, Módulo vivienda ilimitado, 5 Consultas, Calculadoras uso limitado, Conceptos - tipo limitado (CT), Descuento en Conceptos - medida (CM)",
            'start_date' => $start_date,
            'end_date' => $end_date,
            'platform_code' => "Membresia_Junior_Mensual",
            'price' => 51700,
            'status' => 1,
        ]);

        SoftworldPlans::create([
            'code' => "PLAN_TRIMESTRAL_JUNIOR",
            'image' => "membresias/XruYx7OzzojQv76zCUjYWU90VhIyJfHJrVUYnwFe.jpg",
            'title' => "Plan Trimestral Junior",
            'description' => "5 Liquidaciones, 1 Capacitación, Módulo vivienda ilimitado, 5 Consultas, Calculadoras uso limitado, Conceptos - tipo limitado (CT), Descuento en Conceptos - medida (CM)",
            'start_date' => $start_date,
            'end_date' => $end_date,
            'platform_code' => "Membresia_Junior_Trimestral",
            'price' => 155100,
            'status' => 1,
        ]);

        SoftworldPlans::create([
            'code' => "PLAN_SEMESTRAL_JUNIOR",
            'image' => "membresias/XruYx7OzzojQv76zCUjYWU90VhIyJfHJrVUYnwFe.jpg",
            'title' => "Plan Semestral Junior",
            'description' => "5 Liquidaciones, 1 Capacitación, Módulo vivienda ilimitado, 5 Consultas, Calculadoras uso limitado, Conceptos - tipo limitado (CT), Descuento en Conceptos - medida (CM)",
            'start_date' => $start_date,
            'end_date' => $end_date,
            'platform_code' => "Membresia_Junior_Semestral",
            'price' => 310200,
            'status' => 1,
        ]);


        SoftworldPlans::create([
            'code' => "PLAN_ANUAL_JUNIOR",
            'image' => "membresias/XruYx7OzzojQv76zCUjYWU90VhIyJfHJrVUYnwFe.jpg",
            'title' => "Plan Anual Junior",
            'description' => "5 Liquidaciones, 1 Capacitación, Módulo vivienda ilimitado, 5 Consultas, Calculadoras uso limitado, Conceptos - tipo limitado (CT), Descuento en Conceptos - medida (CM)",
            'start_date' => $start_date,
            'end_date' => $end_date,
            'platform_code' => "Membresia_Junior_Anual",
            'price' => 620400,
            'status' => 1,
        ]);

        DetailsPlansMembership::create([
            'plan_id' => 'PLAN_MENSUAL_JUNIOR',
            'membership_id' => 'CNP_MEMBERSHIP_JUNIOR',
        ]);

        DetailsPlansMembership::create([
            'plan_id' => 'PLAN_TRIMESTRAL_JUNIOR',
            'membership_id' => 'CNP_MEMBERSHIP_JUNIOR',
        ]);

        DetailsPlansMembership::create([
            'plan_id' => 'PLAN_SEMESTRAL_JUNIOR',
            'membership_id' => 'CNP_MEMBERSHIP_JUNIOR',
        ]);

        DetailsPlansMembership::create([
            'plan_id' => 'PLAN_ANUAL_JUNIOR',
            'membership_id' => 'CNP_MEMBERSHIP_JUNIOR',
        ]);




        /* Regitro del plan membresia Senior */

        SoftworldPlans::create([
            'code' => "PLAN_MENSUAL_SENIOR",
            'image' => "membresias/bBpEj4he9tYhs8Gk7XlosNbj0skjjolVvbAtz4AR.png",
            'title' => "Plan Mensual Senior",
            'description' => "11 Liquidaciones, 2 Capacitaciones, Módulo vivienda ilimitado, 8 Consultas, Calculadoras uso ilimitado, Conceptos - tipo ilimitado (CT), Descuento en Conceptos - medida (CM)",
            'start_date' => $start_date,
            'end_date' => $end_date,
            'platform_code' => "Membresia_Senior_Mensual",
            'price' => 62500,
            'status' => 1,
        ]);

        SoftworldPlans::create([
            'code' => "PLAN_TRIMESTRAL_SENIOR",
            'image' => "membresias/bBpEj4he9tYhs8Gk7XlosNbj0skjjolVvbAtz4AR.png",
            'title' => "Plan Trimestral Senior",
            'description' => "11 Liquidaciones, 2 Capacitaciones, Módulo vivienda ilimitado, 8 Consultas, Calculadoras uso ilimitado, Conceptos - tipo ilimitado (CT), Descuento en Conceptos - medida (CM)",
            'start_date' => $start_date,
            'end_date' => $end_date,
            'platform_code' => "Membresia_Senior_Trimestral",
            'price' => 187500,
            'status' => 1,
        ]);

        SoftworldPlans::create([
            'code' => "PLAN_SEMESTRAL_SENIOR",
            'image' => "membresias/bBpEj4he9tYhs8Gk7XlosNbj0skjjolVvbAtz4AR.png",
            'title' => "Plan Semestral Senior",
            'description' => "11 Liquidaciones, 2 Capacitaciones, Módulo vivienda ilimitado, 8 Consultas, Calculadoras uso ilimitado, Conceptos - tipo ilimitado (CT), Descuento en Conceptos - medida (CM)",
            'start_date' => $start_date,
            'end_date' => $end_date,
            'platform_code' => "Membresia_Senior_Semestral",
            'price' => 375000,
            'status' => 1,
        ]);


        SoftworldPlans::create([
            'code' => "PLAN_ANUAL_SENIOR",
            'image' => "membresias/bBpEj4he9tYhs8Gk7XlosNbj0skjjolVvbAtz4AR.png",
            'title' => "Plan Anual Senior",
            'description' => "11 Liquidaciones, 2 Capacitaciones, Módulo vivienda ilimitado, 8 Consultas, Calculadoras uso ilimitado, Conceptos - tipo ilimitado (CT), Descuento en Conceptos - medida (CM)",
            'start_date' => $start_date,
            'end_date' => $end_date,
            'platform_code' => "Membresia_Senior_Anual",
            'price' => 750000,
            'status' => 1,
        ]);

        DetailsPlansMembership::create([
            'plan_id' => 'PLAN_MENSUAL_SENIOR',
            'membership_id' => 'CNP_MEMBERSHIP_SENIOR',
        ]);

        DetailsPlansMembership::create([
            'plan_id' => 'PLAN_TRIMESTRAL_SENIOR',
            'membership_id' => 'CNP_MEMBERSHIP_SENIOR',
        ]);

        DetailsPlansMembership::create([
            'plan_id' => 'PLAN_SEMESTRAL_SENIOR',
            'membership_id' => 'CNP_MEMBERSHIP_SENIOR',
        ]);

        DetailsPlansMembership::create([
            'plan_id' => 'PLAN_ANUAL_SENIOR',
            'membership_id' => 'CNP_MEMBERSHIP_SENIOR',
        ]);





        /* Regitro del plan membresia Magister */
        SoftworldPlans::create([
            'code' => "PLAN_MENSUAL_MAGISTER",
            'image' => "membresias/hoXmVzEC8JTPjpS6Lvg2LVjJpuYiweiwlQUK5Dym.png",
            'title' => "Plan Mensual Magister",
            'description' => "20 Liquidaciones, Capacitaciones ilimitadas, 1 Seminario con descuento, Módulo vivienda ilimitado, 12 Consultas, Calculadoras uso ilimitado, Conceptos - tipo ilimitado (CT), Descuento en Conceptos - medida (CM)",
            'start_date' => $start_date,
            'end_date' => $end_date,
            'platform_code' => "Membresia_Magister_Mensual",
            'price' => 79200,
            'status' => 1,
        ]);

        SoftworldPlans::create([
            'code' => "PLAN_TRIMESTRAL_MAGISTER",
            'image' => "membresias/hoXmVzEC8JTPjpS6Lvg2LVjJpuYiweiwlQUK5Dym.png",
            'title' => "Plan Trimestral Magister",
            'description' => "20 Liquidaciones, Capacitaciones ilimitadas, 1 Seminario con descuento, Módulo vivienda ilimitado, 12 Consultas, Calculadoras uso ilimitado, Conceptos - tipo ilimitado (CT), Descuento en Conceptos - medida (CM)",
            'start_date' => $start_date,
            'end_date' => $end_date,
            'platform_code' => "Membresia_Magister_Trimestral",
            'price' => 237600,
            'status' => 1,
        ]);

        SoftworldPlans::create([
            'code' => "PLAN_SEMESTRAL_MAGISTER",
            'image' => "membresias/hoXmVzEC8JTPjpS6Lvg2LVjJpuYiweiwlQUK5Dym.png",
            'title' => "Plan Semestral Magister",
            'description' => "20 Liquidaciones, Capacitaciones ilimitadas, 1 Seminario con descuento, Módulo vivienda ilimitado, 12 Consultas, Calculadoras uso ilimitado, Conceptos - tipo ilimitado (CT), Descuento en Conceptos - medida (CM)",
            'start_date' => $start_date,
            'end_date' => $end_date,
            'platform_code' => "Membresia_Magister_Semestral",
            'price' => 475200,
            'status' => 1,
        ]);


        SoftworldPlans::create([
            'code' => "PLAN_ANUAL_MAGISTER",
            'image' => "membresias/hoXmVzEC8JTPjpS6Lvg2LVjJpuYiweiwlQUK5Dym.png",
            'title' => "Plan Anual Magister",
            'description' => "20 Liquidaciones, Capacitaciones ilimitadas, 1 Seminario con descuento, Módulo vivienda ilimitado, 12 Consultas, Calculadoras uso ilimitado, Conceptos - tipo ilimitado (CT), Descuento en Conceptos - medida (CM)",
            'start_date' => $start_date,
            'end_date' => $end_date,
            'platform_code' => "Membresia_Magister_Anual",
            'price' => 950400,
            'status' => 1,
        ]);

        DetailsPlansMembership::create([
            'plan_id' => 'PLAN_MENSUAL_MAGISTER',
            'membership_id' => 'CNP_MEMBERSHIP_MAGISTER',
        ]);

        DetailsPlansMembership::create([
            'plan_id' => 'PLAN_TRIMESTRAL_MAGISTER',
            'membership_id' => 'CNP_MEMBERSHIP_MAGISTER',
        ]);

        DetailsPlansMembership::create([
            'plan_id' => 'PLAN_SEMESTRAL_MAGISTER',
            'membership_id' => 'CNP_MEMBERSHIP_MAGISTER',
        ]);

        DetailsPlansMembership::create([
            'plan_id' => 'PLAN_ANUAL_MAGISTER',
            'membership_id' => 'CNP_MEMBERSHIP_MAGISTER',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
