<?php

use App\Models\DetailsTypeContent;
use App\Models\SoftworldContentTextHeaders;
use App\Models\SoftworldHeadersTypes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RegisterDataHeaders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        SoftworldContentTextHeaders::create([
            'title' => "Inicio",
            'description' => "Pagina de inicio",
            'status' => 1,
        ]);

        SoftworldContentTextHeaders::create([
            'title' => "Nosotros",
            'description' => "Pagina de Nosotros",
            'status' => 1,
        ]);

        SoftworldContentTextHeaders::create([
            'title' => "Servicios",
            'description' => "Pagina de servicios",
            'status' => 1,
        ]);

        SoftworldContentTextHeaders::create([
            'title' => "Noticias",
            'description' => "Pagina de Noticias",
            'status' => 1,
        ]);

        SoftworldContentTextHeaders::create([
            'title' => "Preguntas frecuentes",
            'description' => "Pagina de Preguntas frecuentes",
            'status' => 1,
        ]);

        SoftworldContentTextHeaders::create([
            'title' => "Contactanos",
            'description' => "Pagina de Contactanos",
            'status' => 1,
        ]);

        DetailsTypeContent::create([
            'slug_id' => 1,
            'content_id' => 1,
        ]);

        DetailsTypeContent::create([
            'slug_id' => 2,
            'content_id' => 2,
        ]);

        DetailsTypeContent::create([
            'slug_id' => 3,
            'content_id' => 3,
        ]);

        DetailsTypeContent::create([
            'slug_id' => 4,
            'content_id' => 4,
        ]);

        DetailsTypeContent::create([
            'slug_id' => 5,
            'content_id' => 5,
        ]);

        DetailsTypeContent::create([
            'slug_id' => 6,
            'content_id' => 6,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
