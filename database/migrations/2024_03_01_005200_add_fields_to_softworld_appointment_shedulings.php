<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToSoftworldAppointmentShedulings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('softworld_sheduling_appointments', function (Blueprint $table) {
            $table->unsignedBigInteger('profesional_id')->after('status')->nullable()->default(NULL);
            $table->dateTime('date_deleted')->after('profesional_id')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->after('date_deleted')->nullable()->default(NULL);
        });

        Schema::table('softworld_categories', function (Blueprint $table) {
            $table->dateTime('date_deleted')->after('status')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->after('date_deleted')->nullable()->default(NULL);
        });

        Schema::table('softworld_modulos', function (Blueprint $table) {
            $table->dateTime('date_deleted')->after('estado_modulo')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->after('date_deleted')->nullable()->default(NULL);
        });

        Schema::table('softworld_news', function (Blueprint $table) {
            $table->dateTime('date_deleted')->after('status')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->after('date_deleted')->nullable()->default(NULL);
        });

        Schema::table('softworld_people', function (Blueprint $table) {
            $table->dateTime('date_deleted')->after('status')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->after('date_deleted')->nullable()->default(NULL);
        });


        Schema::table('softworld_services', function (Blueprint $table) {
            $table->dateTime('date_deleted')->after('status')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->after('date_deleted')->nullable()->default(NULL);
        });

        Schema::table('softworld_status_platforms', function (Blueprint $table) {
            $table->dateTime('date_deleted')->after('module')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->after('date_deleted')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('softworld_sheduling_appointments', function (Blueprint $table) {
            $table->dropColumn('profesional_id');
            $table->dropColumn('date_deleted');
            $table->dropColumn('deleted_by');
        });

        Schema::table('softworld_categories', function (Blueprint $table) {
            $table->dropColumn('date_deleted');
            $table->dropColumn('deleted_by');
        });

        Schema::table('softworld_modulos', function (Blueprint $table) {
            $table->dropColumn('date_deleted');
            $table->dropColumn('deleted_by');
        });

        Schema::table('softworld_news', function (Blueprint $table) {
            $table->dropColumn('date_deleted');
            $table->dropColumn('deleted_by');
        });

        Schema::table('softworld_people', function (Blueprint $table) {
            $table->dropColumn('date_deleted');
            $table->dropColumn('deleted_by');
        });

        Schema::table('softworld_services', function (Blueprint $table) {
            $table->dropColumn('date_deleted');
            $table->dropColumn('deleted_by');
        });

        Schema::table('softworld_status_platforms', function (Blueprint $table) {
            $table->dropColumn('date_deleted');
            $table->dropColumn('deleted_by');
        });
    }
}
