<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_partners', function (Blueprint $table) {
            $table->string("partner_code", 50)->primary();
            $table->string("partner_image")->nullable();
            $table->string("partner_title");
            $table->longText("partner_description")->nullable();
            $table->string("person_contact_names");
            $table->string("person_contact_last_names");
            $table->string("person_contact_cellphone");
            $table->string("person_contact_email");
            $table->string("status");
            $table->dateTime('date_deleted')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_partners');
    }
}
