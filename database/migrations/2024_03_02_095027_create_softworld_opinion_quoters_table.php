<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldOpinionQuotersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_opinion_quoters', function (Blueprint $table) {
            $table->string("code");
            $table->string("nombres_apellidos");
            $table->string("correo_electronico");
            $table->string("telefono_calular");
            $table->string("ciudad");
            $table->string("area_derecho");
            $table->string("valor_pretensiones");
            $table->string("tipo_cliente");
            $table->string("opcion_pretensiones");
            $table->string("experiencia_litigio");
            $table->string("tribunal");
            $table->string("volumen_informacion");
            $table->string("servicios_adicionales");
            $table->string("area_dictamen");
            $table->string("resultado_cotizacion");
            $table->dateTime("date_deleted")->default(NULL)->nullable();
            $table->string("deleted_by")->default(NULL)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_opinion_quoters');
    }
}
