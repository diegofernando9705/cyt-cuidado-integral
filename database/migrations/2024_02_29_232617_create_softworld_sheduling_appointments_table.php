<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldShedulingAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_sheduling_appointments', function (Blueprint $table) {
            $table->string('code', 50)->primary();
            $table->date('date');
            $table->time('start_hour');
            $table->time('end_hour');
            $table->string('names');
            $table->string('last_names');
            $table->string('celphone');
            $table->string('email');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_sheduling_appointments');
    }
}
