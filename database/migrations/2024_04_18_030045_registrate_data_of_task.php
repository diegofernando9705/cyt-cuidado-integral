<?php

use App\Models\SoftworldStatusPlatforms;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RegistrateDataOfTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        SoftworldStatusPlatforms::create([
            'description' => 'Abierta',
            'module' => 'task',
        ]);

        SoftworldStatusPlatforms::create([
            'description' => 'Pendiente',
            'module' => 'task',
        ]);

        SoftworldStatusPlatforms::create([
            'description' => 'En progreso',
            'module' => 'task',
        ]);

        SoftworldStatusPlatforms::create([
            'description' => 'Completada',
            'module' => 'task',
        ]);

        SoftworldStatusPlatforms::create([
            'description' => 'Pausada',
            'module' => 'task',
        ]);

        SoftworldStatusPlatforms::create([
            'description' => 'Cancelada',
            'module' => 'task',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
