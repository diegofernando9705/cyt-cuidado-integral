<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\softworld_modulos;

class RegistratePermissionPartners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = Role::where(['name' => 'administrador'])->get();

        Permission::create(['name' => 'socios-access'])->assignRole($role);
        Permission::create(['name' => 'socios-list'])->assignRole($role);

        Permission::create(['name' => 'socios-create'])->assignRole($role);
        Permission::create(['name' => 'socios-read'])->assignRole($role);
        Permission::create(['name' => 'socios-update'])->assignRole($role);
        Permission::create(['name' => 'socios-delete'])->assignRole($role);
        
        softworld_modulos::create(['nombre_modulo' => 'socios', 'descripcion_modulo' => 'Modulo de agendamiento citas', 'estado_modulo' => '1']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
