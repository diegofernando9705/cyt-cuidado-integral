<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldRequestsConceptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_requests_concepts', function (Blueprint $table) {
            $table->string("code")->primary();
            $table->string("title");
            $table->longText("description");
            $table->date("date_register");
            $table->date("date_approved")->nullable();
            $table->date("date_rejection")->nullable();
            $table->unsignedBigInteger("created_by");
            $table->unsignedBigInteger("status");
            $table->dateTime('date_deleted')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_requests_concepts');
    }
}
