<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCamposToDetailsLitipointsPeople extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('details_litipoints_people', function (Blueprint $table) {
            $table->longText('code_litipoint')->after('id')->nullable()->default(NULL);
            $table->enum('action', ['add', 'subtract'])->after('point')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('details_litipoints_people', function (Blueprint $table) {
            $table->dropColumn('code_litipoint');
            $table->dropColumn('action');
        });
    }
}
