<?php

use App\Models\SoftworldHeadersTypes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RegisterHeadersType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        SoftworldHeadersTypes::create([
            'type' => "text",
            'slug' => "index",
        ]);

        SoftworldHeadersTypes::create([
            'type' => "text",
            'slug' => "about_us",
        ]);

        SoftworldHeadersTypes::create([
            'type' => "text",
            'slug' => "services",
        ]);

        SoftworldHeadersTypes::create([
            'type' => "text",
            'slug' => "news",
        ]);

        SoftworldHeadersTypes::create([
            'type' => "text",
            'slug' => "frequent_questions",
        ]);

        SoftworldHeadersTypes::create([
            'type' => "text",
            'slug' => "contact",
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
