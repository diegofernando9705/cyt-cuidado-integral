<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_news', function (Blueprint $table) {
            $table->id();
            $table->string("image");
            $table->string("title");
            $table->string("summary");
            $table->longText("description");
            $table->string("url");
            $table->string("type");
            $table->date("register_date");
            $table->date("date_end")->nullable();
            $table->unsignedBigInteger("user_creator_id");
            $table->unsignedBigInteger("status")->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_news');
    }
}
