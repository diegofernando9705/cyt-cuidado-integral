<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_categories', function (Blueprint $table) {
            $table->string("code", 50)->primary();
            $table->string("image")->nullable();
            $table->string("title", 50);
            $table->longText("description")->nullable();
            $table->unsignedBigInteger("status");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_categories');
    }
}
