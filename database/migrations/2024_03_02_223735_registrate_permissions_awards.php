<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\softworld_modulos;

class RegistratePermissionsAwards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = Role::where(['name' => 'administrador'])->get();

        Permission::create(['name' => 'premios-access'])->assignRole($role);
        Permission::create(['name' => 'premios-list'])->assignRole($role);

        Permission::create(['name' => 'premios-create'])->assignRole($role);
        Permission::create(['name' => 'premios-read'])->assignRole($role);
        Permission::create(['name' => 'premios-update'])->assignRole($role);
        Permission::create(['name' => 'premios-delete'])->assignRole($role);
        
        softworld_modulos::create(['nombre_modulo' => 'premios', 'descripcion_modulo' => 'Modulo de agendamiento citas', 'estado_modulo' => '1']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
