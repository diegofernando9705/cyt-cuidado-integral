<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_tasks', function (Blueprint $table) {
            $table->string('code', 50)->primary();
            $table->string('title');
            $table->longText('description')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->unsignedBigInteger('priority');
            $table->unsignedBigInteger('status');
            $table->dateTime('date_deleted')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_tasks');
    }
}
