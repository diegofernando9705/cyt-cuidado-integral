<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldImagesHeadersPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_images_headers_pages', function (Blueprint $table) {
            $table->id();
            $table->string("title");
            $table->longText("description");
            $table->longText("url");
            $table->string("text_button");
            $table->string("link_button");
            $table->unsignedBigInteger("page");
            $table->unsignedBigInteger("status");
            $table->timestamps();

            $table->foreign('page')->references('id')->on('softworld_descriptions_headers_pages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_images_headers_pages');
    }
}
