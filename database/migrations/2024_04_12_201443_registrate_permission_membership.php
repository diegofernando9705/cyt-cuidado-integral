<?php

use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\softworld_modulos as Modulos;

class RegistratePermissionMembership extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = Role::where(['id' => '1'])->get();

        Permission::create(['name' => 'membresias-access'])->assignRole($role);
        Permission::create(['name' => 'membresias-list'])->assignRole($role);
        
        Permission::create(['name' => 'membresias-create'])->assignRole($role);
        Permission::create(['name' => 'membresias-read'])->assignRole($role);
        Permission::create(['name' => 'membresias-update'])->assignRole($role);
        Permission::create(['name' => 'membresias-delete'])->assignRole($role);
        
        Modulos::create(['nombre_modulo' => 'membresias', 'descripcion_modulo' => 'Modulo de indicadores economicos', 'estado_modulo' => '1']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
