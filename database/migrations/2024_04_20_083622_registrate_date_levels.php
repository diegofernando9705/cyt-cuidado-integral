<?php

use App\Models\SoftworldLevels;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RegistrateDateLevels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        SoftworldLevels::create([
            'title' => 'Básico'
        ]);

        SoftworldLevels::create([
            'title' => 'Intermedio',
        ]);

        SoftworldLevels::create([
            'title' => 'Avanzado',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
