<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoftworldEconomicIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('softworld_economic_indicators', function (Blueprint $table) {
            $table->id();
            $table->date("date_start_indicator");
            $table->date("date_end_indicator");
            $table->string("value_start_indicator");
            $table->string("description_start_indicator");
            $table->string("percentage_start_indicator");
            $table->unsignedBigInteger("status_start_indicator");
            $table->dateTime('date_deleted')->nullable()->default(NULL);
            $table->unsignedBigInteger('deleted_by')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('softworld_economic_indicators');
    }
}
