<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => false]);

$prefixWeb = "page";
Route::get('', $prefixWeb . '\HomeController@index')->name('index');
Route::get('about', $prefixWeb . '\HomeController@about')->name('about_us');
Route::get('services', $prefixWeb . '\HomeController@services')->name('services');
Route::get('news', $prefixWeb . '\HomeController@news')->name('news');
Route::get('frequent_questions', $prefixWeb . '\HomeController@frequentQuestions')->name('frequent_questions');
Route::get('contact', $prefixWeb . '\HomeController@contact')->name('contact');
Route::post('contact/store', $prefixWeb . '\HomeController@contactStore')->name('contact-store');
Route::get('access-denied', $prefixWeb . '\HomeController@accessDenied')->name('access-denied');

Route::get('login-form', $prefixWeb . '\HomeController@loginForm')->name('login-form');
Route::post('login-post', $prefixWeb . '\HomeController@loginPost')->name('login-post');
Route::get('login-logout', $prefixWeb . '\HomeController@loginLogout')->name('login-logout');

Route::get('reset-form', $prefixWeb . '\HomeController@resetForm')->name('reset-form');

Route::post('send-recovery-password', $prefixWeb . '\HomeController@sendRecoveryPassword')->name('send-recovery-password');
Route::get('form-recovery-password/{token}', $prefixWeb . '\HomeController@formRecoveryPassword')->name('reset_password_form');
Route::post('update-recovery-password', $prefixWeb . '\HomeController@updateRecoveryPassword')->name('update_password_form');

Route::get('services/all', $prefixWeb . '\GlobalController@services')->name('services-all');
Route::get('services/{url}', $prefixWeb . '\HomeController@service')->name('service');

Route::get('news/all', $prefixWeb . '\GlobalController@news')->name('news-all');
Route::get('news/{url}', $prefixWeb . '\HomeController@new')->name('new');


Route::post('quote-opinion', $prefixWeb . '\HomeController@quoteOpinion')->name('quote-opinion');
Route::get('membership', $prefixWeb . '\GlobalController@membership')->name('membership');
Route::post('register-appointment-scheduling', $prefixWeb . '\HomeController@registerAppointmentScheduling')->name('register-appointment-scheduling');

/* ------------------------------ ROUTES FOR SUBSCRIPTION ----------------------------- */
Route::post('form-subscription', $prefixWeb . '\GlobalController@formSubscription')->name('form-subscription');
Route::post('form-subscription/store', $prefixWeb . '\SubscriptionController@registerSubscriptor')->name('registerSubscriptor');
Route::post('form-subscription/plans/membership', $prefixWeb . '\SubscriptionController@plansMembership')->name('plansMembership');
Route::post('form-subscription/plans/membership/selected', $prefixWeb . '\SubscriptionController@plansMembershipSelected')->name('plansMembershipSelected');
Route::post('form-subscription/payment/store', $prefixWeb . '\SubscriptionController@subscriptionPayment')->name('subscriptionPayment');
/* ---------------------------- FIN ROUTES FOR SUBSCRIPTION ---------------------------- */

/* ---------------------------- RUTAS LOGUEADO ---------------------------- */
$prefixApp = "Administrator";
Route::group(['prefix' => 'app', 'middleware' => ['auth:web', 'checkSubscriberRole']], function () use ($prefixApp) {

    // ==================== Respuestas ==================== \\
    Route::group(['prefix' => 'answers'], function () use ($prefixApp) {
        Route::get('all/{data}', $prefixApp . '\Administrative\AnswersController@listado')->name('answers-list');
        Route::post('create/store', $prefixApp . '\Administrative\AnswersController@store')->name('answers-store');
        Route::get('delete/{code}', $prefixApp . '\Administrative\AnswersController@destroy')->name('answers-delete');
    });
    // ==================== Fin Respuestas ==================== \\

    /* ROUTE CAPTURE FORMS ALL */
    Route::any('/templates/form/{type}/{moduleId}', 'Administrator\TemplatesController@getForm')->name('get_form');


    /* ROUTE CKFINDER */
    Route::any('/ckfinder/connector', '\CKSource\CKFinderBridge\Controller\CKFinderController@requestAction')
        ->name('ckfinder_connector');

    Route::any('/ckfinder/browser', '\CKSource\CKFinderBridge\Controller\CKFinderController@browserAction')
        ->name('ckfinder_browser');


    Route::get('', $prefixApp . '\HomeController@index')->name('dashboard');

    Route::group(['prefix' => 'administrador', 'middleware' => ['can:roles-access']], function () use ($prefixApp) {
        Route::get('roles/listado', ['middleware' => 'can:roles-list', 'uses' => $prefixApp . '\RoleController@listado'])->name('roles-listado');

        Route::get('roles', ['middleware' => 'can:roles-list', 'uses' => $prefixApp . '\RoleController@index'])->name('roles-list');
        Route::get('roles/create', ['middleware' => 'can:roles-create', 'uses' => $prefixApp . '\RoleController@create'])->name('roles-create');
        Route::post('roles/create/store', ['middleware' => 'can:roles-create', 'uses' => $prefixApp . '\RoleController@store'])->name('roles-store');
        Route::get('roles/show/{id?}', ['middleware' => 'can:roles-read', 'uses' => $prefixApp . '\RoleController@show'])->name('roles-read');
        Route::get('roles/edit/{id?}', ['middleware' => 'can:roles-update', 'uses' => $prefixApp . '\RoleController@edit'])->name('roles-edit');
        Route::post('roles/edit/update', ['middleware' => 'can:roles-update', 'uses' => $prefixApp . '\RoleController@update'])->name('roles-update');
    });

    Route::group(['prefix' => 'administrador', 'middleware' => ['can:usuarios-access']], function () use ($prefixApp) {
        Route::get('usuarios/listado', ['middleware' => 'can:usuarios-list', 'uses'  => $prefixApp . '\UserController@listado'])->name('usuarios-listado');

        Route::get('usuarios', ['middleware' => 'can:usuarios-list', 'uses'  => $prefixApp . '\UserController@index'])->name('usuarios-list');
        Route::get('usuarios/create', ['middleware' => 'can:usuarios-create', 'uses'  => $prefixApp . '\UserController@create'])->name('usuarios-create');

        Route::get('usuarios/edit/{id?}', ['middleware' => 'can:usuarios-update', 'uses'  => $prefixApp . '\UserController@edit'])->name('usuarios-edit');
        Route::post('usuarios/edit/update', ['middleware' => 'can:usuarios-update', 'uses'  => $prefixApp . '\UserController@update'])->name('usuarios-update');

        Route::post('usuarios/create/store', ['middleware' => 'can:usuarios-create', 'uses'  => $prefixApp . '\UserController@store']);

        Route::get('usuarios/delete/{id?}', ['middleware' => 'can:usuarios-delete', 'uses'  => $prefixApp . '\UserController@destroy'])->name('usuarios-delete');
    });

    Route::group(['prefix' => 'webpage/banner', 'middleware' => ['can:paginaweb-banner-access']], function () use ($prefixApp) {
        Route::get('general', ['middleware' => 'can:paginaweb-banner-access', 'uses' => $prefixApp . '\Webpage\BannersController@banners_general'])->name('paginaweb-banner-access');
        Route::get('general/slug/{code}', ['middleware' => 'can:paginaweb-banner-access', 'uses' => $prefixApp . '\Webpage\BannersController@banners_general_slug'])->name('banners_general_slug');

        Route::get('general/slug/update/{sk}/{type}', ['middleware' => 'can:paginaweb-banner-access', 'uses' => $prefixApp . '\Webpage\BannersController@updateBannerType'])->name('updateBannerType');
        Route::post('update/{id}', $prefixApp . '\Webpage\BannersController@update')->name('updateBanner');
        Route::get('form/addImagesBanner/{slug}', $prefixApp . '\Webpage\BannersController@formAddImageToBanner')->name('formAddImageToBanner');
        Route::get('table/{slug}', $prefixApp . '\Webpage\BannersController@listBannersHeader')->name('listBannersHeader');
        Route::get('table/image/edit/{code}', $prefixApp . '\Webpage\BannersController@formEditImageBanner')->name('formEditImageBanner');
        Route::post('table/image/edit/update/{code}', $prefixApp . '\Webpage\BannersController@updateImageBanner')->name('updateImageBanner');
        Route::get('table/image/delete/{code}', $prefixApp . '\Webpage\BannersController@deleteImageBanner')->name('deleteImageBanner');
    });


    Route::group(['prefix' => 'webpage/about-us', 'middleware' => ['can:paginaweb-nosotros-access']], function () use ($prefixApp) {
        Route::get('form', ['middleware' => 'can:paginaweb-nosotros-read', 'uses' => $prefixApp . '\Webpage\AboutusController@formUpdate'])->name('paginaweb-nosotros-read');
        Route::post('form-update/update', ['middleware' => 'can:paginaweb-nosotros-update', 'uses' => $prefixApp . '\Webpage\AboutusController@update'])->name('paginaweb-nosotros-post');
    });

    // ==================== servicios ==================== \\
    Route::group(['prefix' => 'webpage/servicios', 'middleware' => ['can:paginaweb-servicios-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:paginaweb-servicios-list', 'uses' => $prefixApp . '\Webpage\ServiciosController@tabla'])->name('table');
        Route::get('', ['middleware' => 'can:paginaweb-servicios-access', 'uses' => $prefixApp . '\Webpage\ServiciosController@index'])->name('paginaweb-servicios-list');

        Route::get('create', ['middleware' => 'can:paginaweb-servicios-create', 'uses' => $prefixApp . '\Webpage\ServiciosController@create'])->name('paginaweb-servicios-create');
        Route::post('create/store', ['middleware' => 'can:paginaweb-servicios-create', 'uses' => $prefixApp . '\Webpage\ServiciosController@store'])->name('store_services');

        Route::get('show/{code}', ['middleware' => 'can:paginaweb-servicios-read', 'uses' => $prefixApp . '\Webpage\ServiciosController@show'])->name('show_services');

        Route::get('edit/{code}', ['middleware' => 'can:paginaweb-servicios-update', 'uses' => $prefixApp . '\Webpage\ServiciosController@edit'])->name('edit_services');
        Route::post('edit/update/{code}', ['middleware' => 'can:paginaweb-servicios-update', 'uses' => $prefixApp . '\Webpage\ServiciosController@update'])->name('update_services');

        Route::get('delete/{code}', ['middleware' => 'can:paginaweb-servicios-delete', 'uses' => $prefixApp . '\Webpage\ServiciosController@delete'])->name('delete_services');
    });
    // ==================== Fin servicios ==================== \\


    // ==================== categorias ==================== \\
    Route::group(['prefix' => 'webpage/categories', 'middleware' => ['can:paginaweb-categorias-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:paginaweb-categorias-list', 'uses' => $prefixApp . '\Webpage\CategoriesController@tabla'])->name('table_category');
        Route::get('', ['middleware' => 'can:paginaweb-categorias-access', 'uses' => $prefixApp . '\Webpage\CategoriesController@index'])->name('paginaweb-categorias-list');

        Route::get('create', ['middleware' => 'can:paginaweb-categorias-create', 'uses' => $prefixApp . '\Webpage\CategoriesController@create'])->name('paginaweb-categorias-create');
        Route::post('create/store', ['middleware' => 'can:paginaweb-categorias-create', 'uses' => $prefixApp . '\Webpage\CategoriesController@store'])->name('store_category');

        Route::get('show/{code}', ['middleware' => 'can:paginaweb-categorias-read', 'uses' => $prefixApp . '\Webpage\CategoriesController@show'])->name('show_category');

        Route::get('edit/{code}', ['middleware' => 'can:paginaweb-categorias-update', 'uses' => $prefixApp . '\Webpage\CategoriesController@edit'])->name('edit_category');
        Route::post('edit/update/{code}', ['middleware' => 'can:paginaweb-categorias-update', 'uses' => $prefixApp . '\Webpage\CategoriesController@update'])->name('update_category');

        Route::get('delete/{code}', ['middleware' => 'can:paginaweb-categorias-delete', 'uses' => $prefixApp . '\Webpage\CategoriesController@delete'])->name('delete_category');
    });
    // ==================== Fin categorias ==================== \\


    // ==================== noticias ==================== \\
    Route::group(['prefix' => 'webpage/news', 'middleware' => ['can:paginaweb-noticias-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:paginaweb-noticias-list', 'uses' => $prefixApp . '\Webpage\NewsController@tabla'])->name('table_noticia');
        Route::get('', ['middleware' => 'can:paginaweb-noticias-access', 'uses' => $prefixApp . '\Webpage\NewsController@index'])->name('paginaweb-noticias-list');

        Route::get('create', ['middleware' => 'can:paginaweb-noticias-create', 'uses' => $prefixApp . '\Webpage\NewsController@create'])->name('paginaweb-noticias-create');
        Route::post('create/store', ['middleware' => 'can:paginaweb-noticias-create', 'uses' => $prefixApp . '\Webpage\NewsController@store'])->name('store_noticies');

        Route::get('show/{code}', ['middleware' => 'can:paginaweb-noticias-read', 'uses' => $prefixApp . '\Webpage\NewsController@show'])->name('show_noticia');

        Route::get('edit/{code}', ['middleware' => 'can:paginaweb-noticias-update', 'uses' => $prefixApp . '\Webpage\NewsController@edit'])->name('edit_noticia');
        Route::post('edit/update/{code}', ['middleware' => 'can:paginaweb-noticias-update', 'uses' => $prefixApp . '\Webpage\NewsController@update'])->name('update_noticia');

        Route::get('delete/{code}', ['middleware' => 'can:paginaweb-noticias-delete', 'uses' => $prefixApp . '\Webpage\NewsController@delete'])->name('delete_noticia');

        /* Asignar membresia a la noticia */
        Route::get('form/membership/{codigo?}', ['middleware' => 'can:asignar-membresia-noticia', 'uses' => $prefixApp . '\Webpage\NewsController@form_membresia_noticia'])->name('form_membresia_noticia');
        Route::post('post/membership/{codigo?}', ['middleware' => 'can:asignar-membresia-noticia', 'uses' => $prefixApp . '\Webpage\NewsController@post_membresia_noticia'])->name('post_membresia_noticia');
    });
    // ==================== Fin noticias ==================== \\



    // ==================== preguntas frecuentes ==================== \\
    Route::group(['prefix' => 'webpage/frequent-questions', 'middleware' => ['can:paginaweb-preguntasfrecuentes-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:paginaweb-preguntasfrecuentes-list', 'uses' => $prefixApp . '\Webpage\FrequentquestionsController@tabla'])->name('table_preguntasfrecuentes');
        Route::get('', ['middleware' => 'can:paginaweb-preguntasfrecuentes-access', 'uses' => $prefixApp . '\Webpage\FrequentquestionsController@index'])->name('paginaweb-preguntasfrecuentes-list');

        Route::get('create', ['middleware' => 'can:paginaweb-preguntasfrecuentes-create', 'uses' => $prefixApp . '\Webpage\FrequentquestionsController@create'])->name('create_preguntasfrecuentes');
        Route::post('create/store', ['middleware' => 'can:paginaweb-preguntasfrecuentes-create', 'uses' => $prefixApp . '\Webpage\FrequentquestionsController@store'])->name('store_preguntasfrecuentes');

        Route::get('show/{code}', ['middleware' => 'can:paginaweb-preguntasfrecuentes-read', 'uses' => $prefixApp . '\Webpage\FrequentquestionsController@show'])->name('show_preguntasfrecuentes');

        Route::get('edit/{code}', ['middleware' => 'can:paginaweb-preguntasfrecuentes-update', 'uses' => $prefixApp . '\Webpage\FrequentquestionsController@edit'])->name('edit_preguntasfrecuentes');
        Route::post('edit/update/{code}', ['middleware' => 'can:paginaweb-preguntasfrecuentes-update', 'uses' => $prefixApp . '\Webpage\FrequentquestionsController@update'])->name('update_preguntasfrecuentes');

        Route::get('delete/{code}', ['middleware' => 'can:paginaweb-preguntasfrecuentes-delete', 'uses' => $prefixApp . '\Webpage\FrequentquestionsController@delete'])->name('delete_preguntasfrecuentes');
    });
    // ==================== Fin preguntas frecuentes ==================== \\


    // ==================== cotizador dictamen ==================== \\
    Route::group(['prefix' => 'webpage/quote-opinions', 'middleware' => ['can:cotizadordictamen-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:cotizadordictamen-list', 'uses' => $prefixApp . '\Webpage\QuoteOpinionsController@tabla'])->name('table_cotizador-dictamen');
        Route::get('', ['middleware' => 'can:cotizadordictamen-access', 'uses' => $prefixApp . '\Webpage\QuoteOpinionsController@index'])->name('cotizadordictamen-list');
        Route::get('show/{code}', ['middleware' => 'can:cotizadordictamen-read', 'uses' => $prefixApp . '\Webpage\QuoteOpinionsController@show'])->name('show_cotizador-dictamen');
        Route::get('delete/{code}', ['middleware' => 'can:cotizadordictamen-delete', 'uses' => $prefixApp . '\Webpage\QuoteOpinionsController@delete'])->name('delete_cotizador-dictamen');
    });
    // ==================== Fin cotizador dictamen ==================== \\


    // ==================== Agendamiento citas ==================== \\
    Route::group(['prefix' => 'administrative/scheduling-appointments', 'middleware' => ['can:agendamiento-citas-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:agendamiento-citas-list', 'uses' => $prefixApp . '\Administrative\SchedulingAppointmentsController@listado'])->name('agendamiento-citas-listado');

        Route::get('', ['middleware' => 'can:agendamiento-citas-list', 'uses' => $prefixApp . '\Administrative\SchedulingAppointmentsController@index'])->name('agendamiento-citas-list');
        Route::get('show/{id?}', ['middleware' => 'can:agendamiento-citas-read', 'uses' => $prefixApp . '\Administrative\SchedulingAppointmentsController@show'])->name('agendamiento-citas-read');
        Route::get('edit/{id?}', ['middleware' => 'can:agendamiento-citas-update', 'uses' => $prefixApp . '\Administrative\SchedulingAppointmentsController@edit'])->name('agendamiento-citas-edit');
        Route::post('edit/update', ['middleware' => 'can:agendamiento-citas-update', 'uses' => $prefixApp . '\Administrative\SchedulingAppointmentsController@update'])->name('agendamiento-citas-update');

        Route::get('delete/{id?}', ['middleware' => 'can:agendamiento-citas-update', 'uses' => $prefixApp . '\Administrative\SchedulingAppointmentsController@destroy'])->name('agendamiento-citas-destroy');
    });
    // ==================== Fin Agendamiento citas ==================== \\


    // ==================== Personas ==================== \\
    Route::group(['prefix' => 'administrative/people', 'middleware' => ['can:personas-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:personas-list', 'uses' => $prefixApp . '\Administrative\PeopleController@listado'])->name('personas-listado');

        Route::get('', ['middleware' => 'can:personas-list', 'uses' => $prefixApp . '\Administrative\PeopleController@index'])->name('personas-list');
        Route::get('create', ['middleware' => 'can:personas-create', 'uses' => $prefixApp . '\Administrative\PeopleController@create'])->name('personas-create');
        Route::post('create/store', ['middleware' => 'can:personas-create', 'uses' => $prefixApp . '\Administrative\PeopleController@store'])->name('personas-store');
        Route::get('show/{id?}', ['middleware' => 'can:personas-read', 'uses' => $prefixApp . '\Administrative\PeopleController@show'])->name('personas-read');
        Route::get('edit/{id?}', ['middleware' => 'can:personas-update', 'uses' => $prefixApp . '\Administrative\PeopleController@edit'])->name('personas-edit');
        Route::post('edit/update', ['middleware' => 'can:personas-update', 'uses' => $prefixApp . '\Administrative\PeopleController@update'])->name('personas-update');

        Route::get('delete/{code}', ['middleware' => 'can:personas-delete', 'uses' => $prefixApp . '\\Administrative\PeopleController@destroy'])->name('personas-delete');
    });
    // ==================== Fin personas ==================== \\


    // ==================== Premios ==================== \\
    Route::group(['prefix' => 'litipoints/awards', 'middleware' => ['can:premios-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:premios-list', 'uses' => $prefixApp . '\Litipoints\AwardsController@listado'])->name('premios-table');
        Route::get('', ['middleware' => 'can:premios-access', 'uses' => $prefixApp . '\Litipoints\AwardsController@index'])->name('premios-list');

        Route::get('create', ['middleware' => 'can:premios-create', 'uses' => $prefixApp . '\Litipoints\AwardsController@create'])->name('premios-create');
        Route::post('create/store', ['middleware' => 'can:premios-create', 'uses' => $prefixApp . '\Litipoints\AwardsController@store'])->name('premios-store');

        Route::get('show/{code}', ['middleware' => 'can:premios-read', 'uses' => $prefixApp . '\Litipoints\AwardsController@show'])->name('premios-show');

        Route::get('edit/{code}', ['middleware' => 'can:premios-update', 'uses' => $prefixApp . '\Litipoints\AwardsController@edit'])->name('premios-edit');
        Route::post('edit/update/{code}', ['middleware' => 'can:premios-update', 'uses' => $prefixApp . '\Litipoints\AwardsController@update'])->name('premios-update');

        Route::get('delete/{code}', ['middleware' => 'can:premios-delete', 'uses' => $prefixApp . '\Litipoints\AwardsController@destroy'])->name('premios-delete');

        Route::get('form/membership/{code}', ['middleware' => 'can:asignar-membresia-premios', 'uses' => $prefixApp . '\Litipoints\AwardsController@form_membresia_premio'])->name('premios-update-membership');
        Route::post('form/membership/update/{code}', ['middleware' => 'can:asignar-membresia-premios', 'uses' => $prefixApp . '\Litipoints\AwardsController@updateMembershipAward'])->name('premios-update-membership-up');
    });
    // ==================== Fin Premios ==================== \\


    // ==================== Socios ==================== \\
    Route::group(['prefix' => 'litipoints/partners', 'middleware' => ['can:socios-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:socios-list', 'uses' => $prefixApp . '\Litipoints\PartnersController@listado'])->name('socios-table');
        Route::get('', ['middleware' => 'can:socios-access', 'uses' => $prefixApp . '\Litipoints\PartnersController@index'])->name('socios-list');

        Route::get('create', ['middleware' => 'can:socios-create', 'uses' => $prefixApp . '\Litipoints\PartnersController@create'])->name('socios-create');
        Route::post('create/store', ['middleware' => 'can:socios-create', 'uses' => $prefixApp . '\Litipoints\PartnersController@store'])->name('socios-store');

        Route::get('show/{code}', ['middleware' => 'can:socios-read', 'uses' => $prefixApp . '\Litipoints\PartnersController@show'])->name('socios-show');

        Route::get('edit/{code}', ['middleware' => 'can:socios-update', 'uses' => $prefixApp . '\Litipoints\PartnersController@edit'])->name('socios-edit');
        Route::post('edit/update/{code}', ['middleware' => 'can:socios-update', 'uses' => $prefixApp . '\Litipoints\PartnersController@update'])->name('socios-update');

        Route::get('delete/{code}', ['middleware' => 'can:socios-delete', 'uses' => $prefixApp . '\Litipoints\PartnersController@destroy'])->name('socios-delete');
    });
    // ==================== Fin Socios ==================== \\


    // ==================== Banner publicidad clientes ==================== \\
    Route::group(['prefix' => 'administrative/banner-customer', 'middleware' => ['can:banner-publicidad-cliente-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:banner-publicidad-cliente-list', 'uses' => $prefixApp . '\Litipoints\BannerCustomerController@listado'])->name('banner-publicidad-cliente-table');
        Route::get('', ['middleware' => 'can:banner-publicidad-cliente-access', 'uses' => $prefixApp . '\Litipoints\BannerCustomerController@index'])->name('banner-publicidad-cliente-list');

        Route::get('create', ['middleware' => 'can:banner-publicidad-cliente-create', 'uses' => $prefixApp . '\Litipoints\BannerCustomerController@create'])->name('banner-publicidad-cliente-create');
        Route::post('create/store', ['middleware' => 'can:banner-publicidad-cliente-create', 'uses' => $prefixApp . '\Litipoints\BannerCustomerController@store'])->name('banner-publicidad-cliente-store');

        Route::get('show/{code}', ['middleware' => 'can:banner-publicidad-cliente-read', 'uses' => $prefixApp . '\Litipoints\BannerCustomerController@show'])->name('banner-publicidad-cliente-show');

        Route::get('edit/{code}', ['middleware' => 'can:banner-publicidad-cliente-update', 'uses' => $prefixApp . '\Litipoints\BannerCustomerController@edit'])->name('banner-publicidad-cliente-edit');
        Route::post('edit/update/{code}', ['middleware' => 'can:banner-publicidad-cliente-update', 'uses' => $prefixApp . '\Litipoints\BannerCustomerController@update'])->name('banner-publicidad-cliente-update');

        Route::get('delete/{code}', ['middleware' => 'can:banner-publicidad-cliente-delete', 'uses' => $prefixApp . '\Litipoints\BannerCustomerController@destroy'])->name('banner-publicidad-cliente-delete');
    });
    // ==================== Fin Banner publicidad clientes ==================== \\


    // ==================== Indicadores economicos ==================== \\
    Route::group(['prefix' => 'administrative/economic-indicator', 'middleware' => ['can:indicadores-economicos-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:indicadores-economicos-list', 'uses' => $prefixApp . '\Administrative\EconomicIndicatorsController@listado'])->name('indicadores-economicos-listado');

        Route::get('', ['middleware' => 'can:indicadores-economicos-list', 'uses' => $prefixApp . '\Administrative\EconomicIndicatorsController@index'])->name('indicadores-economicos-list');
        Route::get('create', ['middleware' => 'can:indicadores-economicos-create', 'uses' => $prefixApp . '\Administrative\EconomicIndicatorsController@create'])->name('indicadores-economicos-create');
        Route::post('create/store', ['middleware' => 'can:indicadores-economicos-create', 'uses' => $prefixApp . '\Administrative\EconomicIndicatorsController@store'])->name('indicadores-economicos-store');
        Route::get('show/{id?}', ['middleware' => 'can:indicadores-economicos-read', 'uses' => $prefixApp . '\Administrative\EconomicIndicatorsController@show'])->name('indicadores-economicos-read');
        Route::get('edit/{id?}', ['middleware' => 'can:indicadores-economicos-update', 'uses' => $prefixApp . '\Administrative\EconomicIndicatorsController@edit'])->name('indicadores-economicos-edit');
        Route::post('edit/update', ['middleware' => 'can:indicadores-economicos-update', 'uses' => $prefixApp . '\Administrative\EconomicIndicatorsController@update'])->name('indicadores-economicos-update');

        Route::get('delete/{code}', ['middleware' => 'can:indicadores-economicos-delete', 'uses' => $prefixApp . '\Administrative\EconomicIndicatorsController@destroy'])->name('indicadores-economicos-delete');

        Route::get('update/formulario', ['middleware' => 'can:indicadores-economicos-import', 'uses' => $prefixApp . '\Administrative\EconomicIndicatorsController@formImport'])->name('formulario-indicadores-update');
        Route::post('update/formulario/data', ['middleware' => 'can:indicadores-economicos-import', 'uses' => $prefixApp . '\Administrative\EconomicIndicatorsController@updateImportEconomicIndicators'])->name('formulario-indicadores-update-excel');
    });
    // ==================== Fin Indicadores economicos ==================== \\


    // ==================== Membresia ==================== \\
    Route::group(['prefix' => 'administrative/membership', 'middleware' => ['can:membresias-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:membresias-list', 'uses' => $prefixApp . '\Administrative\MembershipController@listado'])->name('membresias-listado');

        Route::get('', ['middleware' => 'can:membresias-list', 'uses' => $prefixApp . '\Administrative\MembershipController@index'])->name('membresias-list');
        //Route::get('create', ['middleware' => 'can:membresias-create', 'uses' => $prefixApp . '\Administrative\MembershipController@create'])->name('membresias-create');
        //Route::post('create/store', ['middleware' => 'can:membresias-create', 'uses' => $prefixApp . '\Administrative\MembershipController@store'])->name('membresias-store');
        Route::get('show/{id?}', ['middleware' => 'can:membresias-read', 'uses' => $prefixApp . '\Administrative\MembershipController@show'])->name('membresias-read');
        Route::get('edit/{id?}', ['middleware' => 'can:membresias-update', 'uses' => $prefixApp . '\Administrative\MembershipController@edit'])->name('membresias-edit');
        Route::post('edit/update', ['middleware' => 'can:membresias-update', 'uses' => $prefixApp . '\Administrative\MembershipController@update'])->name('membresias-update');

        Route::get('delete/{code}', ['middleware' => 'can:membresias-delete', 'uses' => $prefixApp . '\Administrative\MembershipController@destroy'])->name('membresias-delete');
    });
    // ==================== Fin Membresia ==================== \\


    // ==================== Planes ==================== \\
    Route::group(['prefix' => 'administrative/plans', 'middleware' => ['can:planes-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:planes-list', 'uses' => $prefixApp . '\Administrative\PlansController@listado'])->name('planes-listado');

        Route::get('', ['middleware' => 'can:planes-list', 'uses' => $prefixApp . '\Administrative\PlansController@index'])->name('planes-list');
        //Route::get('create', ['middleware' => 'can:planes-create', 'uses' => $prefixApp . '\Administrative\PlansController@create'])->name('planes-create');
        //Route::post('create/store', ['middleware' => 'can:planes-create', 'uses' => $prefixApp . '\Administrative\PlansController@store'])->name('planes-store');
        Route::get('show/{id?}', ['middleware' => 'can:planes-read', 'uses' => $prefixApp . '\Administrative\PlansController@show'])->name('planes-read');
        Route::get('edit/{id?}', ['middleware' => 'can:planes-update', 'uses' => $prefixApp . '\Administrative\PlansController@edit'])->name('planes-edit');
        Route::post('edit/update', ['middleware' => 'can:planes-update', 'uses' => $prefixApp . '\Administrative\PlansController@update'])->name('planes-update');

        Route::get('delete/{code}', ['middleware' => 'can:planes-delete', 'uses' => $prefixApp . '\Administrative\PlansController@destroy'])->name('planes-delete');
    });
    // ==================== Fin Planes ==================== \\



    // ==================== Calculadoras ==================== \\
    Route::group(['prefix' => 'administrative/calculators', 'middleware' => ['can:calculadoras-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:calculadoras-list', 'uses' => $prefixApp . '\Administrative\CalculatorsController@listado'])->name('calculadoras-listado');

        Route::get('', ['middleware' => 'can:calculadoras-list', 'uses' => $prefixApp . '\Administrative\CalculatorsController@index'])->name('calculadoras-list');
        //Route::get('create', ['middleware' => 'can:calculadoras-create', 'uses' => $prefixApp . '\Administrative\CalculatorsController@create'])->name('calculadoras-create');
        //Route::post('create/store', ['middleware' => 'can:calculadoras-create', 'uses' => $prefixApp . '\Administrative\CalculatorsController@store'])->name('calculadoras-store');
        Route::get('show/{id?}', ['middleware' => 'can:calculadoras-read', 'uses' => $prefixApp . '\Administrative\CalculatorsController@show'])->name('calculadoras-read');
        Route::get('edit/{id?}', ['middleware' => 'can:calculadoras-update', 'uses' => $prefixApp . '\Administrative\CalculatorsController@edit'])->name('calculadoras-edit');
        Route::post('edit/update', ['middleware' => 'can:calculadoras-update', 'uses' => $prefixApp . '\Administrative\CalculatorsController@update'])->name('calculadoras-update');

        Route::get('delete/{code}', ['middleware' => 'can:calculadoras-delete', 'uses' => $prefixApp . '\Administrative\CalculatorsController@destroy'])->name('calculadoras-delete');
    });
    // ==================== Fin Calculadoras ==================== \\


    // ==================== Conceptos ==================== \\
    Route::group(['prefix' => 'administrative/concepts', 'middleware' => ['can:conceptos-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:conceptos-list', 'uses' => $prefixApp . '\Administrative\ConceptsController@listado'])->name('conceptos-listado');

        Route::get('', ['middleware' => 'can:conceptos-list', 'uses' => $prefixApp . '\Administrative\ConceptsController@index'])->name('conceptos-list');
        Route::get('create', ['middleware' => 'can:conceptos-create', 'uses' => $prefixApp . '\Administrative\ConceptsController@create'])->name('conceptos-create');
        Route::post('create/store', ['middleware' => 'can:conceptos-create', 'uses' => $prefixApp . '\Administrative\ConceptsController@store'])->name('conceptos-store');
        Route::get('show/{id?}', ['middleware' => 'can:conceptos-read', 'uses' => $prefixApp . '\Administrative\ConceptsController@show'])->name('conceptos-read');
        Route::get('edit/{id?}', ['middleware' => 'can:conceptos-update', 'uses' => $prefixApp . '\Administrative\ConceptsController@edit'])->name('conceptos-edit');
        Route::post('edit/update', ['middleware' => 'can:conceptos-update', 'uses' => $prefixApp . '\Administrative\ConceptsController@update'])->name('conceptos-update');

        Route::get('delete/{code}', ['middleware' => 'can:conceptos-delete', 'uses' => $prefixApp . '\Administrative\ConceptsController@destroy'])->name('conceptos-delete');
        Route::get('pdf/{code}', $prefixApp . '\Administrative\ConceptsController@pdf')->name('conceptos-pdf');

        /* Asignar membresia a la noticia */
        Route::get('form/membership/{codigo?}', ['middleware' => 'can:asignar-membresia-concepto', 'uses' => $prefixApp . '\Administrative\ConceptsController@formMembresia'])->name('concepto-membresia');
        Route::post('post/membership/{codigo?}', ['middleware' => 'can:asignar-membresia-concepto', 'uses' => $prefixApp . '\Administrative\ConceptsController@postMembresia'])->name('concepto-membresia-update');
    });
    // ==================== Fin Conceptos ==================== \\


    // ==================== Conceptos ==================== \\
    Route::group(['prefix' => 'administrative/application-concepts', 'middleware' => ['can:solicitud-conceptos-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:solicitud-conceptos-list', 'uses' => $prefixApp . '\Administrative\ApplicationsController@listado'])->name('solicitud-conceptos-listado');

        Route::get('', ['middleware' => 'can:solicitud-conceptos-list', 'uses' => $prefixApp . '\Administrative\ApplicationsController@index'])->name('solicitud-conceptos-list');
        Route::get('show/{id?}', ['middleware' => 'can:solicitud-conceptos-read', 'uses' => $prefixApp . '\Administrative\ApplicationsController@show'])->name('solicitud-conceptos-read');
        Route::get('edit/{id?}', ['middleware' => 'can:solicitud-conceptos-update', 'uses' => $prefixApp . '\Administrative\ApplicationsController@edit'])->name('solicitud-conceptos-edit');
        Route::post('edit/update', ['middleware' => 'can:solicitud-conceptos-update', 'uses' => $prefixApp . '\Administrative\ApplicationsController@update'])->name('solicitud-conceptos-update');

        Route::get('delete/{code}', ['middleware' => 'can:solicitud-conceptos-delete', 'uses' => $prefixApp . '\Administrative\ApplicationsController@destroy'])->name('solicitud-conceptos-delete');
    });
    // ==================== Fin Conceptos ==================== \\



    // ==================== Conceptos ==================== \\
    Route::group(['prefix' => 'learning/courses', 'middleware' => ['can:cursos-access']], function () use ($prefixApp) {
        Route::get('listado', ['middleware' => 'can:cursos-list', 'uses' => $prefixApp . '\Administrative\CoursesController@listado'])->name('cursos-listado');
        Route::get('memberships/{code}', ['middleware' => 'can:asignar-membresia-cursos', 'uses' => $prefixApp . '\Administrative\CoursesController@form_membership_course'])->name('cursos-membresia');
        Route::post('memberships/post/{code}', ['middleware' => 'can:asignar-membresia-cursos', 'uses' => $prefixApp . '\Administrative\CoursesController@store_membership_course'])->name('cursos-membresia-store');

        Route::get('', ['middleware' => 'can:cursos-list', 'uses' => $prefixApp . '\Administrative\CoursesController@index'])->name('cursos-list');
        Route::get('create', ['middleware' => 'can:cursos-create', 'uses' => $prefixApp . '\Administrative\CoursesController@create'])->name('cursos-create');
        Route::post('create/store', ['middleware' => 'can:cursos-create', 'uses' => $prefixApp . '\Administrative\CoursesController@store'])->name('cursos-store');
        Route::get('show/{id?}', ['middleware' => 'can:cursos-read', 'uses' => $prefixApp . '\Administrative\CoursesController@show'])->name('cursos-read');
        Route::get('edit/{id?}', ['middleware' => 'can:cursos-update', 'uses' => $prefixApp . '\Administrative\CoursesController@edit'])->name('cursos-edit');
        Route::post('edit/update', ['middleware' => 'can:cursos-update', 'uses' => $prefixApp . '\Administrative\CoursesController@update'])->name('cursos-update');

        Route::get('edit/modules/{courseid}', ['middleware' => 'can:cursos-update', 'uses' => $prefixApp . '\Administrative\CoursesController@modules'])->name('cursos-modules');
        Route::post('edit/modules/{courseid}/save', ['middleware' => 'can:cursos-update', 'uses' => $prefixApp . '\Administrative\CoursesController@moduleStore'])->name('cursos-modules-store');
        Route::get('edit/modules/{moduleId}/delete', ['middleware' => 'can:cursos-update', 'uses' => $prefixApp . '\Administrative\CoursesController@moduleDelete'])->name('cursos-modules-delete');

        Route::get('edit/module/{moduleId}', ['middleware' => 'can:cursos-update', 'uses' => $prefixApp . '\Administrative\CoursesController@moduleEdit'])->name('cursos-module-edit');
        Route::post('edit/module/update/{moduleId}', ['middleware' => 'can:cursos-update', 'uses' => $prefixApp . '\Administrative\CoursesController@moduleUpdate'])->name('cursos-module-update');

        Route::post('edit/module/lessons/store/{moduleId}', ['middleware' => 'can:cursos-update', 'uses' => $prefixApp . '\Administrative\CoursesController@lessonStore'])->name('cursos-lesson-store');
        Route::get('module/lessons/edit/{code}', ['middleware' => 'can:cursos-update', 'uses' => $prefixApp . '\Administrative\CoursesController@lessonEdit'])->name('cursos-lesson-edit');
        Route::post('module/lessons/update/{code}', ['middleware' => 'can:cursos-update', 'uses' => $prefixApp . '\Administrative\CoursesController@lessonUpdate'])->name('cursos-lesson-update');
        Route::get('module/lessons/delete/{code}', ['middleware' => 'can:cursos-update', 'uses' => $prefixApp . '\Administrative\CoursesController@lessonDelete'])->name('cursos-lesson-delete');
    });
    // ==================== Fin Conceptos ==================== \\
});

$prefixSubscriber = "Subscriber";
Route::group(['prefix' => 'subscriber', 'middleware' => ['auth:web', 'role:subscriber']], function () use ($prefixSubscriber) {
    Route::get('', $prefixSubscriber . '\HomeController@index')->name('dashboard-subscriber');

    /* ----------------------- My News -----------------------*/
    Route::get('myNews/favorite/{id?}', $prefixSubscriber . '\NewsController@favorite')->name('news-subscriber-favorite');
    Route::get('myNews/favorites/all', $prefixSubscriber . '\NewsController@allFavorits')->name('news-subscriber-favorites-all');
    /* ----------------------- End My News -----------------------*/

    /* ----------------------- My Task -----------------------*/
    Route::get('myTask', $prefixSubscriber . '\TaskController@listado')->name('task-subscriber');
    Route::get('myTask/create', $prefixSubscriber . '\TaskController@create')->name('task-subscriber-create');
    Route::get('myTask/edit/{code}', $prefixSubscriber . '\TaskController@edit')->name('task-subscriber-edit');
    Route::post('myTask/edit/update', $prefixSubscriber . '\TaskController@update')->name('task-subscriber-update');

    Route::post('myTask/create/store', $prefixSubscriber . '\TaskController@store')->name('task-subscriber-store');
    Route::get('myTask/search/{data?}', $prefixSubscriber . '\TaskController@search')->name('task-subscriber-search');
    /* ----------------------- End My Task -----------------------*/

    /* ----------------------- My Files ----------------------- */
    Route::get('myFiles/delete/{data?}', $prefixSubscriber . '\FilesController@destroy')->name('files-subscriber-delete');
    /* ----------------------- EndMy Task ----------------------- */

    /* ----------------------- My Profile ----------------------- */
    Route::get('profile', $prefixSubscriber . '\ProfileController@index')->name('profile-subscriber');
    Route::post('profile/edit/update/{type}', $prefixSubscriber . '\ProfileController@update')->name('profile-subscriber-update');
    Route::get('profile/validation/email', $prefixSubscriber . '\ProfileController@validation')->name('profile-subscriber-validation');
    Route::get('profile/validation/email/token/{token}', $prefixSubscriber . '\ProfileController@validationConfirmed')->name('profile-subscriber-validation-token');
    /* ----------------------- End My profile -----------------------*/

    /* ----------------------- My Courses -----------------------*/
    Route::get('courses', $prefixSubscriber . '\CoursesController@index')->name('courses-index');
    Route::get('courses/show/{id}', $prefixSubscriber . '\CoursesController@show')->name('courses-show');
    Route::post('courses/progress', $prefixSubscriber . '\CoursesController@start')->name('courses-progress');
    Route::get('course/selected/{id}', $prefixSubscriber . '\CoursesController@course')->name('courses-course');
    Route::get('course/selected/comments/{id}', $prefixSubscriber . '\CoursesController@comments')->name('courses-comments');
    Route::post('course/comments/send', $prefixSubscriber . '\CoursesController@commentsStoreCourse')->name('courses-comments-course');

    Route::get('course/lesson/selected/{code}', $prefixSubscriber . '\CoursesController@lesson')->name('courses-lesson');
    Route::get('course/lesson/selected/{code}/finished', $prefixSubscriber . '\CoursesController@lessonFinished')->name('courses-lesson-finished');
    Route::get('course/lesson/selected/comments/{id}', $prefixSubscriber . '\CoursesController@commentsLesson')->name('courses-lesson-comments');
    Route::post('course/lesson/comments/send', $prefixSubscriber . '\CoursesController@commentsStoreLesson')->name('courses-comments-lesson');

    Route::get('course/lesson/assessment/{value}/{id}', $prefixSubscriber . '\CoursesController@assessmentLesson')->name('courses-lesson-assessment');
    /* ----------------------- End My Courses -----------------------*/


    /* ----------------------- My Calculators -----------------------*/
    Route::get('calculators', $prefixSubscriber . '\CalculatorsController@index')->name('calculators-index');
    Route::get('calculators/show/{code}', $prefixSubscriber . '\CalculatorsController@show')->name('calculators-show');
    Route::post('calculators/store/{code}', $prefixSubscriber . '\CalculatorsController@store')->name('calculators-store');
    /* ----------------------- End My Calculators -----------------------*/


    /* ----------------------- My Diaries -----------------------*/
    Route::get('diary', $prefixSubscriber . '\DiariesController@index')->name('diaries-index');
    /* ----------------------- End My Diaries -----------------------*/

    /* ----------------------- My Diaries -----------------------*/
    Route::get('litipoints', $prefixSubscriber . '\LitipointsController@index')->name('litipoints-index');
    /* ----------------------- End My Diaries -----------------------*/

    /* ----------------------- My Concepts -----------------------*/
    Route::get('concepts', $prefixSubscriber . '\ConceptsController@index')->name('concepts-index');
    /* ----------------------- End My Concepts -----------------------*/
});

/* ------------------------- FIN RUTAS LOGUEADO ---------------------------- */
