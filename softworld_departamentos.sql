-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-09-2022 a las 07:25:06
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cytcuidadointegral.com`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `softworld_departamentos`
--
-- Volcado de datos para la tabla `softworld_departamentos`
--

INSERT INTO `softworld_departamentos` (`codigo_dane`, `region_departamento`, `nombre_departamento`, `estado_departamento`, `created_at`, `updated_at`) VALUES
('11', 'Region Centro Oriente', 'Bogota D.C.', '1', NULL, NULL),
('13', 'Region Caribe', 'Bolivar', '1', NULL, NULL),
('15', 'Region Centro Oriente', 'Boyaca', '1', NULL, NULL),
('17', 'Region Eje Cafetero - Antioquia', 'Caldas', '1', NULL, NULL),
('18', 'Region Centro Sur', 'Caqueta', '1', NULL, NULL),
('19', 'Region Pacifico', 'Cauca', '1', NULL, NULL),
('20', 'Region Caribe', 'Cesar', '1', NULL, NULL),
('23', 'Region Caribe', 'Cordoba', '1', NULL, NULL),
('25', 'Region Centro Oriente', 'Cundinamarca', '1', NULL, NULL),
('27', 'Region Pacifico', 'Choco', '1', NULL, NULL),
('41', 'Region Centro Sur', 'Huila', '1', NULL, NULL),
('44', 'Region Caribe', 'La Guajira', '1', NULL, NULL),
('47', 'Region Caribe', 'Magdalena', '1', NULL, NULL),
('5', 'Region Eje Cafetero - Antioquia', 'Antioquia', '1', NULL, NULL),
('50', 'Region Llano', 'Meta', '1', NULL, NULL),
('52', 'Region Pacifico', 'Narino', '1', NULL, NULL),
('54', 'Region Centro Oriente', 'Norte de Santander', '1', NULL, NULL),
('63', 'Region Eje Cafetero - Antioquia', 'Quindio', '1', NULL, NULL),
('66', 'Region Eje Cafetero - Antioquia', 'Risaralda', '1', NULL, NULL),
('68', 'Region Centro Oriente', 'Santander', '1', NULL, NULL),
('70', 'Region Caribe', 'Sucre', '1', NULL, NULL),
('73', 'Region Centro Sur', 'Tolima', '1', NULL, NULL),
('76', 'Region Pacifico', 'Valle del Cauca', '1', NULL, NULL),
('8', 'Region Caribe', 'Atlantico', '1', NULL, NULL),
('81', 'Region Llano', 'Arauca', '1', NULL, NULL),
('85', 'Region Llano', 'Casanare', '1', NULL, NULL),
('86', 'Region Centro Sur', 'Putumayo', '1', NULL, NULL),
('88', 'Region Caribe', 'Archipielago de San Andres, Providencia y Santa Catalina', '1', NULL, NULL),
('94', 'Region Llano', 'Guainia', '1', NULL, NULL),
('95', 'Region Llano', 'Guaviare', '1', NULL, NULL),
('97', 'Region Centro Sur', 'Vaupes', '1', NULL, NULL),
('99', 'Region Llano', 'Vichada', '1', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `softworld_departamentos`
--
ALTER TABLE `softworld_departamentos`
  ADD PRIMARY KEY (`codigo_dane`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
