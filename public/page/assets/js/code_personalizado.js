const navbarMenu = document.getElementById("menu");
const burgerMenu = document.getElementById("burger");
const headerMenu = document.getElementById("header");

// Open Close Navbar Menu on Click Burger
if (burgerMenu && navbarMenu) {
  burgerMenu.addEventListener("click", () => {
    burgerMenu.classList.toggle("is-active");
    navbarMenu.classList.toggle("is-active");
  });
}

// Close Navbar Menu on Click Menu Links
document.querySelectorAll(".menu-link").forEach((link) => {
  link.addEventListener("click", () => {
    burgerMenu.classList.remove("is-active");
    navbarMenu.classList.remove("is-active");
  });
});

// Change Header Background on Scrolling
window.addEventListener("scroll", () => {
  if (this.scrollY >= 85) {
    headerMenu.classList.add("on-scroll");
  } else {
    headerMenu.classList.remove("on-scroll");
  }
});

// Fixed Navbar Menu on Window Resize
window.addEventListener("resize", () => {
  if (window.innerWidth > 768) {
    if (navbarMenu.classList.contains("is-active")) {
      navbarMenu.classList.remove("is-active");
    }
  }
});

$(document).on("click", ".nombre-usuario-nav", function () {
  var option = $(this).attr("data-option");

  if (option == "closed") {
    $(this).attr("data-option", "open");
    $(".menu_nav_user").attr("style", "height:70px !important; transition:1s;");
  } else {
    $(this).attr("data-option", "closed");
    $(".menu_nav_user").attr("style", "height:0px !important; transition:1s;");
  }
});

$(document).on("keyup", "#search_todo", function () {
  var valor = 0;
  if ($(this).val() == "") {
    valor = "null";
  } else {
    valor = $(this).val();
  }
  $.ajax({
    type: "GET",
    url: "/app/todo/search/like" + "/" + valor,
    success: function (data) {
      $("#listado_tareas_general").html(data);
    },
    error: function (error) {},
  });
});

$(document).on("click", ".btn_expandir", function () {
  $(this).attr("style", "transform: translateX(-100vh); transition:2s");
  $(".contenedor_menu_navegacion").attr(
    "style",
    "transform: translateX(0vh); transition:1s"
  );
});

$(document).on("click", ".svg_btn_expandir", function () {
  $(".btn_expandir").attr(
    "style",
    "transform: translateX(-100vh); transition:2s"
  );
  $(".contenedor_menu_navegacion").attr(
    "style",
    "transform: translateX(0vh); transition:0.5s"
  );
});

$(document).on("click", ".option_close_nav", function () {
  $(".btn_expandir").attr("style", "transform: translateX(0vh); transition:1s");
  $(".contenedor_menu_navegacion").attr(
    "style",
    "transform: translateX(-100vh); transition:1s"
  );
});
