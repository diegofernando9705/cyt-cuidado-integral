
function errorHttp(codigo) {
  var alert;
  switch (codigo) {
    case 301:
      alert = Swal.fire({
        allowOutsideClick: false,
        confirmButtonText: "Cerrar",
        confirmButtonColor: "#05555b",
        icon: "error",
        html: "El recurso solicitado ha sido trasladado permanentemente.",
        footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico. <code>Cód. error: " + codigo + "</code></i>",
      });
      break;
    case 302:
      alert = Swal.fire({
        allowOutsideClick: false,
        confirmButtonText: "Cerrar",
        confirmButtonColor: "#05555b",
        icon: "error",
        html: "El recurso solicitado se ha movido, pero fue encontrado.",
        footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico. <code>Cód. error: " + codigo + "</code></i>",
      });
      break;
    case 400:
      alert = Swal.fire({
        allowOutsideClick: false,
        confirmButtonText: "Cerrar",
        confirmButtonColor: "#05555b",
        icon: "error",
        html: "Lo siento, hubo un error en el proceso.",
        footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico. <code>Cód. error: " + codigo + "</code></i>",
      });
      break;
    case 401:
      alert = Swal.fire({
        allowOutsideClick: false,
        confirmButtonText: "Cerrar",
        confirmButtonColor: "#05555b",
        icon: "error",
        html: "Lo siento, usted no esta autorizado.",
        footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico. <code>Cód. error: " + codigo + "</code></i>",
      });
      break;
    case 403:
      alert = Swal.fire({
        allowOutsideClick: false,
        confirmButtonText: "Cerrar",
        confirmButtonColor: "#05555b",
        icon: "error",
        html: "Lo siento, no tienes los permisos necesarios para acceder a esta funcionalidad.",
        footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico. <code>Cód. error: " + codigo + "</code></i>",
      });
      break;
    case 404:
      alert = Swal.fire({
        allowOutsideClick: false,
        confirmButtonText: "Cerrar",
        confirmButtonColor: "#05555b",
        icon: "error",
        html: "Lo siento, No se encontró el recurso solicitado.",
        footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico. <code>Cód. error: " + codigo + "</code></i>",
      });
      break;
    case 405:
      alert = Swal.fire({
        allowOutsideClick: false,
        confirmButtonText: "Cerrar",
        confirmButtonColor: "#05555b",
        icon: "error",
        html: "Lo siento, método no permitido.",
        footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico. <code>Cód. error: " + codigo + "</code></i>",
      });
      break;
    case 406:
      alert = Swal.fire({
        allowOutsideClick: false,
        confirmButtonText: "Cerrar",
        confirmButtonColor: "#05555b",
        icon: "error",
        html: "Lo siento, error en el proceso (Respuesta no aceptable).",
        footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico. <code>Cód. error: " + codigo + "</code></i>",
      });
      break;
    case 407:
      alert = Swal.fire({
        allowOutsideClick: false,
        confirmButtonText: "Cerrar",
        confirmButtonColor: "#05555b",
        icon: "error",
        html: "Lo siento, Se requiere autenticación de proxy.",
        footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico. <code>Cód. error: " + codigo + "</code></i>",
      });
      break;
    case 408:
      alert = Swal.fire({
        allowOutsideClick: false,
        confirmButtonText: "Cerrar",
        confirmButtonColor: "#05555b",
        icon: "error",
        html: "Lo siento, El servidor se agotó esperando el resto de la petición del navegador.",
        footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico. <code>Cód. error: " + codigo + "</code></i>",
      });
      break;
    case 409:
      alert = Swal.fire({
        allowOutsideClick: false,
        confirmButtonText: "Cerrar",
        confirmButtonColor: "#05555b",
        icon: "error",
        html: "Lo siento, Conflicto en el servidor.",
        footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico. <code>Cód. error: " + codigo + "</code></i>",
      });
      break;
    case 410:
      alert = Swal.fire({
        allowOutsideClick: false,
        confirmButtonText: "Cerrar",
        confirmButtonColor: "#05555b",
        icon: "error",
        html: "Lo siento, El recurso solicitado se ha ido y no volverá.",
        footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico. <code>Cód. error: " + codigo + "</code></i>",
      });
      break;
    case 422:
      alert = Swal.fire({
        allowOutsideClick: false,
        confirmButtonText: "Cerrar",
        confirmButtonColor: "#05555b",
        icon: "error",
        html: "Lo siento, por favor verifique la información ingresada e intente nuevamente.",
        footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico. <code>Cód. error: " + codigo + "</code></i>",
      });
      break;
    case 500:
      alert = Swal.fire({
        allowOutsideClick: false,
        confirmButtonText: "Cerrar",
        confirmButtonColor: "#05555b",
        icon: "error",
        html: "Lo siento, Hubo un error en el servidor y la solicitud no pudo ser completada.",
        footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico. <code>Cód. error: " + codigo + "</code></i>",
      });
      break;
    default:
      alert = Swal.fire({
        allowOutsideClick: false,
        confirmButtonText: "Cerrar",
        confirmButtonColor: "#05555b",
        icon: "error",
        html: "Lo siento, el error no fue identificado.",
        footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico.  <code>Cód. error: " + codigo + "</code></i>",
      });
  }

  return alert;
}


function animationLoading(title, description) {
  var timerInterval;
  return Swal.fire({
    title: title,
    html: description,
    timer: 50000,
    timerProgressBar: true,
    allowOutsideClick: false,
    allowEscapeKey: false,
    didOpen: () => {
      Swal.showLoading();
    },
    willClose: () => {
      clearInterval(timerInterval);
    }
  });
}

function storeSuccess() {
  Swal.fire({
    allowOutsideClick: false,
    confirmButtonText: "Cerrar",
    confirmButtonColor: "#05555b",
    icon: "success",
    title: "Registro exitoso",
    html: "La información se ha registrado en la plataforma.",
    footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico.</i>",
  });
}

function updateSuccess() {
  Swal.fire({
    allowOutsideClick: false,
    confirmButtonText: "Cerrar",
    confirmButtonColor: "#05555b",
    icon: "success",
    title: "Actualización exitosa",
    html: "La información se ha actualizado en la plataforma.",
    footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico.</i>",
  });
}


function deleteSuccess() {
  Swal.fire({
    allowOutsideClick: false,
    confirmButtonText: "Cerrar",
    confirmButtonColor: "#05555b",
    icon: "success",
    title: "Eliminación exitosa",
    html: "La información se ha elimnado completamente en la plataforma.",
    footer: "<i style='line-height:4px; font-size:12px;'><code>Ten en cuenta!</code> la información ya no se podrá recuperar manualmente. Para mayor información, comunicate con soporte.</i>",
  });
}

function errorFile() {
  Swal.fire({
    allowOutsideClick: false,
    confirmButtonText: "Cerrar",
    confirmButtonColor: "#05555b",
    icon: "error",
    title: "Error de archivo(s)",
    html: "Uno o más archivos que se adjuntaron no coinciden con el formato permitido, por favor, corrígelo.",
    footer: "<i style='line-height:4px; font-size:12px;'>Si crees que esto es un error, por favor contacta al soporte técnico.</i>",
  });
}